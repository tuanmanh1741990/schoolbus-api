﻿using DeMasterProCloud.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class StringExtensionsTest
    {
        [Fact]
        public void FirstCharToUpper_When_InputNotEmpty()
        {
            string input = "duali and 3s intersoft";
            var result = StringExtensions.FirstCharToUpper(input);
            Assert.True(result == "Duali and 3s intersoft");
        }

        [Fact]
        public void FirstCharToUpper_When_InputIsEmpty()
        {
            string input = "";
            var result = StringExtensions.FirstCharToUpper(input);
            Assert.True(result == "");
        }

        [Fact]
        public void MaskString_When_InputNotEmpty()
        {
            string input = "duali";
            var mark = 'p';
            var result = StringExtensions.Mask(input, mark);
            Assert.True(result == "ppppp");
        }

        [Fact]
        public void MaskString_When_InputIsEmpty()
        {
            string input = "";
            var mark = 'p';
            var result = StringExtensions.Mask(input, mark);
            Assert.True(string.IsNullOrEmpty(result));
        }

        [Fact]
        public void Check_IsNumeric_Return_True()
        {
            string theValue = "32335";
            var result = StringExtensions.IsNumeric(theValue);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsNumeric_Return_False()
        {
            string theValue = "Duali";
            var result = StringExtensions.IsNumeric(theValue);
            Assert.False(result);
        }

        [Fact]
        public void Convert_ToCamelCase_When_InputNotEmpty()
        {
            string str = "Duali";
            var result = StringExtensions.ToCamelCase(str);
            Assert.True(result == "duali");
        }

        [Fact]
        public void Convert_ToCamelCase_When_InputIsEmpty()
        {
            string str = string.Empty;
            var result = StringExtensions.ToCamelCase(str);
            Assert.True(string.IsNullOrEmpty(result));
        }
    }

}
