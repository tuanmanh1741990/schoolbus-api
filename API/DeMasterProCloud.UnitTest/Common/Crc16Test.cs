﻿using DeMasterProCloud.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class Crc16Test
    {
        private readonly Crc16 crc16;
        public Crc16Test()
        {
            crc16 = new Crc16();
        }

        [Fact]
        public void GetCrca_Test()
        {
            var data = new byte[] { 2, 4, 6, 8, 10 };
            var result = crc16.GetCrca(data);
            Assert.True(result == 29139);
        }

        [Fact]
        public void UpdateCrc_Test()
        {
            byte ch = 1;
            var lpwCrcC = new byte[] { 2, 4, 6, 8, 10 };
            crc16.UpdateCrc(ch, lpwCrcC);
        }
    }
}
