﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class JsonSerializerTest
    {
        private JsonSerializer jsonSerializer;
        public JsonSerializerTest()
        {
            jsonSerializer = new JsonSerializer();
        }

        [Fact]
        public void Serialize_Return_ArrayByte()
        {
            var account = new Account()
            {
                Username = "phuongcn",
                Password = "123456"
            };
            var result = jsonSerializer.Serialize(account);
            Assert.True(result is byte[]);
        }

        [Fact]
        public void Serialize_Return_Null()
        {
            Account account = null;
            var result = jsonSerializer.Serialize(account);
            Assert.True(result is null);
        }

        [Fact]
        public void DeSerialize_Return_Account()
        {
            var account = new Account()
            {
                Username = "phuongcn",
                Password = "123456"
            };
            var json = jsonSerializer.Serialize(account);
            var result = jsonSerializer.DeSerialize<Account>(json);
            Assert.True(result is Account);
        }
    }
}
