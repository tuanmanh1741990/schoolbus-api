﻿using DeMasterProCloud.Common.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using static DeMasterProCloud.Common.Infrastructure.Encryptor;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Options;

namespace DeMasterProCloud.UnitTest.Common
{
    public class EncryptorTest
    {
        private readonly Encryptor encryptor;
        public EncryptorTest()
        {
            encryptor = new Encryptor();
        }

        [Fact]
        public void Encrypt_Test()
        {
            var plainText = "BYLAIVrSVK+P8q5DNsCjeA==";
            var encryptKey = "duali123";
            var result = Encryptor.Encrypt(plainText, encryptKey);
            Assert.True(result == "dxMA9kFT7PJHKjR0uVHhzKAdAKdXBlXSO1z+r5q11eo=");
        }

        [Fact]
        public void Encrypt2_Test()
        {
            var plainText = "BYLAIVrSVK+P8q5DNsCjeA==";
            var encryptKey = "duali123duali123duali123duali123duali123duali123duali123duali123";
            var initVector = "ad9f26e7273c55c0ad9f26e7273c55c0";
            var result = encryptor.Encrypt(plainText, encryptKey, initVector);
            Assert.True(result == "i3xZURNpdWS81lVaV9dXw3xQPjIJTRzhICEpvXOwDFM=");
        }

        [Fact]
        public void Decrypt_Test()
        {
            var plainText = "BYLAIVrSVK+P8q5DNsCjeA==";
            var encryptKey = "duali123";
            var result = Encryptor.Decrypt(plainText, encryptKey);
            Assert.True(result == "89954769");
        }

        [Fact]
        public void EncryptDecrypt_Test()
        {
            var inputText = "BYLAIVrSVK+P8q5DNsCjeA==";
            var encryptionKey = "ad9f26e7273c55c0f7eb91b514ea654c";
            var initVector = "ad9f26e7273c55c0";
            var result = encryptor.EncryptDecrypt(inputText, encryptionKey, EncryptMode.Encrypt, initVector);
            Assert.True(result == "dxMA9kFT7PJHKjR0uVHhzKAdAKdXBlXSO1z+r5q11eo=");
        }

        [Theory]
        [InlineData("phuongcn", 8, "74ec2a80")]
        [InlineData("phuongcn", 100, "74ec2a80c6e3b7a22502148aa2863a4fc5074ca72a926f699fae64fc8386a344")]
        public void GetHashSha256_Test(string text, int length, string expected)
        {
            var result = Encryptor.GetHashSha256(text, length);
            Assert.True(result == expected);
        }

        [Fact]
        public void GenerateRandomIv_Test()
        {
            var length = 10;
            var result = Encryptor.GenerateRandomIv(length);
            Assert.True(result != null);
        }

    }
}
