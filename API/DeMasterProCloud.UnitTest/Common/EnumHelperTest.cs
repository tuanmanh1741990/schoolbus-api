﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class EnumHelperTest
    {
      

        [Fact]
        public void ToSelectList_When_SelectedNotNull()
        {
            short? _short = 1;
            var result = EnumHelper.ToSelectList<WeekDays>(_short);
            Assert.True(result != null);
        }

        [Fact]
        public void ToSelectList_When_SelectedIsNull()
        {
            short? _short = null;
            var result = EnumHelper.ToSelectList<WeekDays>(_short);
            Assert.True(result != null);
        }

        [Fact]
        public void ToSelectList_When_StructNotEnum()
        {
            short? _short = null;
            var result = EnumHelper.ToSelectList<Struct>(_short);
            Assert.True(result == null);
        }

        [Fact]
        public void ToStringSelectList()
        {
            var result = EnumHelper.ToStringSelectList<WeekDays>();
            Assert.True(result != null);
        }

        [Fact]
        public void ToStringSelectList_When_StructNotEnum()
        {
            var result = EnumHelper.ToStringSelectList<Struct>();
            Assert.True(result == null);
        }

        [Fact]
        public void ToStringSelectList2_()
        {
            short _short = 1;
            var result = EnumHelper.ToSelectList<WeekDays>(_short);
            Assert.True(result != null);
        }

        [Fact]
        public void ToStringSelectList2_When_StructNotEnum()
        {
            short _short = 1;
            var result = EnumHelper.ToSelectList<Struct>(_short);
            Assert.True(result == null);
        }

        [Fact]
        public void ToEnumList()
        {
            var result = EnumHelper.ToEnumList<WeekDays>();
            Assert.True(result != null);
        }

        [Fact]
        public void ToEnumList_When_StructNotEnum()
        {
            var result = EnumHelper.ToEnumList<Struct>();
            Assert.True(result != null);
        }

        [Fact]
        public void ToEnumListText()
        {
            var result = EnumHelper.ToEnumListText<WeekDays>();
            Assert.True(result != null);
        }

        [Fact]
        public void ToEnumListText_When_StructNotEnum()
        {
            var result = EnumHelper.ToEnumListText<Struct>();
            Assert.True(result != null);
        }

        [Fact]
        public void GetName_Test()
        {
            WeekDays weekDays = new WeekDays();    
            var result = EnumHelper.GetName<WeekDays>(weekDays);
            Assert.True(result =="Monday");
        }

        //[Fact]
        //public void GetDisplayName_Test()
        //{
        //    Status status = new Status();
        //    var result = EnumHelper.GetDisplayName<Status>(status);
        //    Assert.True(result == "0");
        //}

        [Fact]
        public void Check_IsEnum_Return_True()
        {
            var selected = 1;
            var result = EnumHelper.IsEnum<WeekDays>(selected);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsEnum_Return_False()
        {
            var result = EnumHelper.IsEnum<Status>(1);
            Assert.False(result);
        }

        //[Fact]
        //public void Check_IsEnum_mutiSelected_Return_Fasle()
        //{
        //    var multiSelected = "";
        //    var result = EnumHelper.IsEnum<WeekDays>(multiSelected);
        //    Assert.False(result);
        //}

        //[Fact]
        //public void Check_IsEnum_mutiSelected_Return_Fasle2()
        //{
        //    var multiSelected = " ,";
        //    var result = EnumHelper.IsEnum<WeekDays>(multiSelected);
        //    Assert.False(result);
        //}

        //[Fact]
        //public void Check_IsEnum_mutiSelected_Return_Fasle3()
        //{
        //    var multiSelected = "1,2,3";
        //    var result = EnumHelper.IsEnum<WeekDays>(multiSelected);
        //    Assert.False(result);
        //}
        //[Fact]
        //public void Check_IsEnum_mutiSelected_Return_True()
        //{
        //    var multiSelected = "1,2,1";
        //    var result = EnumHelper.IsEnum<WeekDays>(multiSelected);
        //    Assert.True(result);
        //}

        [Fact]
        public void InitData()
        {
            var enumModel = new EnumModel()
            {
                Id = 1,
                Name = "enum1"
            };
            enumModel.Id = 2;
            enumModel.Name = "enum2";
            Assert.True(enumModel.Id == 2);
            Assert.True(enumModel.Name == "enum2");

            var autoComplete = new AutoComplete()
            {
                Label = "label",
                Value=1              
            };

            autoComplete.Label = "label2";
            autoComplete.Value = 2;
            Assert.True(autoComplete.Label == "label2");
            Assert.True(autoComplete.Value == 2);
            
        }
        
        enum WeekDays
        {
            Monday = 0,
            Tuesday = 1,
            Wednesday = 2,
            Thursday = 3,
            Friday = 4,
            Saturday = 5,
            Sunday = 6
        }
        enum Status
        {

        }
        public struct Struct
        {
            public int x, y;

            public Struct(int p1, int p2)
            {
                x = p1;
                y = p2;
            }
        }
    }
}
