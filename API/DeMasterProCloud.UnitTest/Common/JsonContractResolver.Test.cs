﻿using DeMasterProCloud.Common.Infrastructure;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class JsonContractResolver : LowercaseContractResolver
    {
        private readonly PropertyRenameAndIgnoreSerializerContractResolver contractResolver;
        public JsonContractResolver()
        {
            contractResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
        }

        [Fact]
        public void ResolvePropertyName_Test()
        {
            var propertyName = "Phuongcn";
            var result = new JsonContractResolver().ResolvePropertyName(propertyName);
            Assert.True(result == "phuongcn");
        }

        [Fact]
        public void IgnoreProperty_Test()
        {
            var jsonPropertyNames = new string[] { "phuong", "ngoc" };
            contractResolver.IgnoreProperty(typeof(string), jsonPropertyNames);
        }

        [Fact]
        public void RenameProperty_Test()
        {
            var propertyName = "name";
            var newJsonPropertyName = "phuong ngoc";
            contractResolver.RenameProperty(typeof(string), propertyName, newJsonPropertyName);
        }

        //[Fact]
        //public void CreateProperty_Test()
        //{
        //    MemberInfo member;
        //    MemberSerialization memberSerialization;
        //    var a = new CreatePropertyTest();
        //   new CreatePropertyTest().CreateProperty(member, memberSerialization);
        //}
        //[Fact]
        //public void Check_IsIgnored_Return_False()
        //{
        //    var jsonPropertyName = "";
        //    var result = contractResolver.IsIgnored(typeof(string), jsonPropertyName);
        //    Assert.False(result);
        //}

        //[Fact]
        //public void Check_IsRenamed_Return_False()
        //{
        //    var jsonPropertyName = "phuong ngoc";
        //    var newJsonPropertyName = "phuong ngoc";
        //    var result = contractResolver.IsRenamed(typeof(string), jsonPropertyName,out newJsonPropertyName);
        //    Assert.False(result);
        //}

    }
    public class CreatePropertyTest : PropertyRenameAndIgnoreSerializerContractResolver
    {
        public CreatePropertyTest()
        {

        }
    }
}
