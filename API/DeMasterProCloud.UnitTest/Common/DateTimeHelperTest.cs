﻿using DeMasterProCloud.Common.Infrastructure;
using System;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class DateTimeHelperTest
    {
        [Theory]
        [InlineData("10:15:10", 10.25)]
        [InlineData("30:90:00", 23.983333333333331)]
        public void TimeSpanToDouble_Test(string timeSpan, double expected)
        {
            var result = DateTimeHelper.TimeSpanToDouble(timeSpan);
            Assert.True(result == expected);
        }
        [Fact]
        public void TimeSpanToDouble_Return_Format_Exception()
        {
            var timeSpan = "::50";
           Assert.Throws<FormatException>(() => DateTimeHelper.TimeSpanToDouble(timeSpan));         
         }

        [Theory]
        [InlineData(12.3, "12.18")]
        [InlineData(42.3, "23.59")]
        public void DoubleToTimeSpan_Test(double dou, string expected)
        {
            var result = DateTimeHelper.DoubleToTimeSpan(dou);
            Assert.True(result == expected);
        }

        [Theory]
        [InlineData("1995/04/30")]
        [InlineData("")]
        public void Check_IsDateTime_Return_True(string dateTime)
        {
            var result = DateTimeHelper.IsDateTime(dateTime);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsDateTime_Return_False()
        {
            var dateTime = "phuongdragon";
            var result = DateTimeHelper.IsDateTime(dateTime);
            Assert.False(result);
        }

        [Theory]
        [InlineData("30/04/1995", "dd/MM/yyyy")]
        [InlineData("", "dd/MM/yyyy")]
        public void Check_IsDateTime_With_Format_Return_True(string dateTime,string format)
        {
            var result = DateTimeHelper.IsDateTime(dateTime, format);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsDateTime_With_Format_Return_False()
        {
            var dateTime = "phuongdragon";
            var format = "yyyy/MM/dd";
            var result = DateTimeHelper.IsDateTime(dateTime, format);
            Assert.False(result);
        }
    }
}
