﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class HelperTest
    {
        private readonly IConfigurationRoot _configuration;
        public HelperTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            var culture = new CultureInfo("en-US");
            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;
        }

        [Fact]
        public void Check_IsValidImage_Return_True()
        {
            var extension = ".png";
            string[] validExtensions = new string[] { ".png", ".jpg" };
            var result = Helpers.IsValidImage(extension, validExtensions);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsValidImage_When_ExtensionIsEmpty_Return_False()
        {
            var extension = string.Empty;
            string[] validExtensions = new string[] { ".png", ".jpg" };
            var result = Helpers.IsValidImage(extension, validExtensions);
            Assert.False(result);
        }

        [Fact]
        public void Check_IsValidImage_Return_False()
        {
            var extension = ".mp4";
            string[] validExtensions = new string[] { ".png", ".jpg" };
            var result = Helpers.IsValidImage(extension, validExtensions);
            Assert.False(result);
        }

        [Fact]
        public void Check_ParseToByArrayAsync_Success()
        {
            var fileMock = new Mock<IFormFile>();


            var result = Helpers.ParseToByArrayAsync(fileMock.Object);
            Assert.True(result is byte[]);
        }

        //[Fact]
        //public void Check_ToSettingDateString_Success()
        //{
        //    var datetime = new DateTime(2018, 04, 30);
        //    var result = Helpers.ToSettingDateString(datetime);
        //    Assert.True(result == "04.30.2018");
        //}

        //[Fact]
        //public void Check_ToSettingDateTimeString_Success()
        //{
        //    var datetime = new DateTime(2018, 04, 30, 11, 35, 10);
        //    var result = Helpers.ToSettingDateTimeString(datetime);
        //    Assert.True(result == "04.30.2018 11:35:10");
        //}

        //[Fact]
        //public void Check_ToSettingDateTimeWithoutSecString_Success()
        //{
        //    var datetime = new DateTime(2018, 04, 30, 11, 35, 10);
        //    var result = Helpers.ToSettingDateTimeWithoutSecString(datetime);
        //    Assert.True(result == "04.30.2018 11:35");
        //}

        //[Fact]
        //public void Check_ToSettingDateTimeString2_Success()
        //{
        //    DateTime? datetime = new DateTime(2018, 04, 30, 11, 35, 10);
        //    var result = Helpers.ToSettingDateTimeString(datetime);
        //    Assert.True(result == "04.30.2018 11:35:10");
        //}

        [Fact]
        public void Check_ToSettingDateTimeString3_Success()
        {
            DateTime? datetime = null;
            var result = Helpers.ToSettingDateTimeString(datetime);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Check_ToSettingDateString2_Success()
        //{
        //    DateTime? datetime = new DateTime(2018, 04, 30);
        //    var result = Helpers.ToSettingDateString(datetime);
        //    Assert.True(result == "04.30.2018");
        //}

        [Fact]
        public void Check_ToSettingDateString3_Success()
        {
            DateTime? datetime = null;
            var result = Helpers.ToSettingDateString(datetime);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Check_ToEndDateTime_Success()
        //{
        //    var dateOnly = "04.30.2018";
        //    var result = Helpers.ToEndDateTime(dateOnly);
        //    var expected = new DateTime(2018, 04, 30, 23, 59, 59);
        //    Assert.True(result == expected);
        //}

        //[Fact]
        //public void Check_GetDateServerFormat_Success()
        //{
        //    var result = Helpers.GetDateServerFormat();
        //    Assert.True(result == "MM.dd.yyyy");
        //}

        [Fact]
        public void Check_CompanyCode_Success()
        {
            var result = Helpers.GetCompanyCode(GetHttpContext().User);
            Assert.True(result == "d000001");
        }

        [Fact]
        public void Check_CompanyCode_When_UserIsNull()
        {
            var result = Helpers.GetCompanyCode(GetHttpContext2().User);
            Assert.True(result == null);
        }

        [Fact]
        public void Check_GetCompanyId_Success()
        {
            var result = Helpers.GetCompanyId(GetHttpContext().User);
            Assert.True(result == 1);
        }

        [Fact]
        public void Check_GetCompanyId_When_UserIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => Helpers.GetCompanyId(GetHttpContext2().User));
        }

        [Fact]
        public void Check_GetCompanyName_Success()
        {
            var result = Helpers.GetCompanyName(GetHttpContext().User);
            Assert.True(result == "duali");
        }

        [Fact]
        public void Check_GetCompanyName_When_UserIsNull()
        {
            var result = Helpers.GetCompanyName(GetHttpContext2().User);
            Assert.True(result == null);
        }

        [Fact]
        public void Check_GetAccountType_Success()
        {
            var result = Helpers.GetAccountType(GetHttpContext().User);
            Assert.True(result == 2);
        }

        [Fact]
        public void Check_GetAccountType_When_UserIsNull()
        {
            var result = Helpers.GetAccountType(GetHttpContext2().User);
            Assert.True(result == 0);
        }

        [Fact]
        public void Check_GetAccountId_Success()
        {
            var result = Helpers.GetAccountId(GetHttpContext().User);
            Assert.True(result == 1);
        }

        [Fact]
        public void Check_GetAccountId_When_UserIsNull()
        {
            var result = Helpers.GetAccountId(GetHttpContext2().User);
            Assert.True(result == 0);
        }

        [Fact]
        public void Check_GetBearerToken_Success()
        {
            var result = Helpers.GetBearerToken(GetHttpContext().User);
            Assert.True(result == "123456");
        }

        [Fact]
        public void Check_GetBearerToken_When_UserIsNull()
        {
            var result = Helpers.GetBearerToken(GetHttpContext2().User);
            Assert.True(result == null);
        }
        [Fact]
        public void Check_ToAscii_Success()
        {
            var text = "phuong";
            var result = Helpers.ToAscii(text);
            Assert.True(result == "112104117111110103");
        }

        [Fact]
        public void Check_GenerateCompanyCode_Success()
        {
            var result = Helpers.GenerateCompanyCode();
            Assert.True(result.Length == 7);
        }

        [Fact]
        public void Check_GenerateRandomString_Success()
        {
            var length = 10;
            var result = Helpers.GenerateRandomString(length);
            Assert.True(result.Length == length);
        }

        //[Fact]
        //public void Convert_GetStatusList_Success()
        //{
        //    var result = Helpers.GetStatusList(null);
        //    foreach (var item in result)
        //    {
        //        Assert.True(item.Value != Status.Deleted.ToString());
        //    }
        //}

        [Fact]
        public void Convert_GenerateEncryptKey()
        {
            var result = Helpers.GenerateEncryptKey("T46T5U");
            Assert.True(result == "322A3A9A309B362A349A98AA992A199A");
        }

        [Fact]
        public void Convert_Hex2Ascii()
        {
            var hex = "7068756f6e67";
            var result = Helpers.Hex2Ascii(hex);
            Assert.True(result == "phuong");
        }

        [Fact]
        public void Convert_AsciiToHex()
        {
            var ascii = "phuong";
            var result = Helpers.AsciiToHex(ascii);
            Assert.True(result == "7068756f6e67");
        }

        [Theory]
        [InlineData("phuong dragon", "phuong cn")]
        [InlineData("phuong cn", "phuong ngoc")]
        [InlineData("phuong cn", "phuong gd")]
        public void Check_EnsureMapLengthString(string str1, string str2)
        {
            Helpers.EnsureMapLengthString(ref str1, ref str2);
        }

        [Fact]
        public void Convert_CombineString_Success()
        {
            var str1 = "phuong cn";
            var str2 = "phuong dragon";
            var result = Helpers.CombineString(str1, str2);
            Assert.True(result == "pphhuuoonngg  cdnr");
        }

        [Fact]
        public void Convert__CombineString_Fail()
        {
            var str1 = "";
            var str2 = "";
            var result = Helpers.CombineString(str1, str2);
            Assert.True(string.IsNullOrEmpty(result));
        }

        [Fact]
        public void Convert_StringToBinaryString_Success()
        {
            var data = "3si";
            var result = Helpers.StringToBinaryString(data);
            Assert.True(result == "001100110111001101101001");
        }

        [Fact]
        public void Convert_BinaryStringToHexString_Success()
        {
            var data = "0011001101110011011010011";
            var result = Helpers.BinaryStringToHexString(data);
            Assert.True(result == "0066E6D3");
        }

        [Fact]
        public void Convert_HexStringToByteArray()
        {
            var data = "337369";
            var result = Helpers.HexStringToByteArray(data);
            var expected = new byte[] { 51, 115, 105 };
            var resultNotExpected = result.Select(x => x).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void Convert_ByteArrayToIntBigEndian()
        {
            byte[] data = new byte[10];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)i;
            }
            var startIndex = 2;
            var result = Helpers.ByteArrayToIntBigEndian(data, startIndex);
            Assert.True(result == 33752069);
        }

        [Fact]
        public void Convert_ByteArrayToIntLittleEndian()
        {
            byte[] data = new byte[10];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = (byte)i;
            }
            var startIndex = 3;
            var result = Helpers.ByteArrayToIntLittleEndian(data, startIndex);
            Assert.True(result == 100992003);
        }

        [Fact]
        public void Check_GenerateRandomPassword()
        {
            var result = Helpers.GenerateRandomPassword();
        }

        [Fact]
        public void Check_GenerateRandomPassword_when_PasswordOptions_IsNotDefaut()
        {
            var opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = true,
                RequireUppercase = true,
            };
            var result = Helpers.GenerateRandomPassword(opts);
        }

        [Theory]
        [InlineData("2018/04/03", "2018/04/04")]
        [InlineData("3si", "2018/04/04")]
        public void Check_CompareDate_Return_True(string startDate, string endDate)
        {
            var result = Helpers.CompareDate(startDate, endDate);
            Assert.True(result);
        }

        [Fact]
        public void Check_CompareDate_Return_False()
        {
            var startDate = "2018/04/03";
            var endDate = "2018/04/02";
            var result = Helpers.CompareDate(startDate, endDate);
            Assert.False(result);
        }

        [Fact]
        public void Check_IsUnicode_Return_True()
        {
            var input = "字&))";
            var result = Helpers.IsUnicode(input);
            Assert.True(result);
        }

        [Theory]
        [InlineData("")]
        [InlineData("phuongcn")]
        public void Check_IsUnicode_Return_False(string input)
        {
            var result = Helpers.IsUnicode(input);
            Assert.False(result);
        }

        [Fact]
        public void Check_IsValidUserName_True()
        {
            var userName = "phuongcn";
            var result = Helpers.IsValidUserName(userName);
            Assert.True(result);
        }

        [Theory]
        [InlineData("")]
        [InlineData("()*(*")]
        public void Check_IsValidUserName_False(string userName)
        {
            var result = Helpers.IsValidUserName(userName);
            Assert.False(result);
        }

        [Fact]
        public void Check_IsIpAddress_True()
        {
            var ipAddress = "192.168.1.2";
            var result = Helpers.IsIpAddress(ipAddress);
            Assert.True(result);
        }

        [Theory]
        [InlineData("")]
        [InlineData("()*(*")]
        public void Check_IsIpAddress_False(string userName)
        {
            var result = Helpers.IsIpAddress(userName);
            Assert.False(result);
        }

        //[Theory]
        //[InlineData("02.04.1995", "5:50 AM", true, "1995-02-04 5:50:00 AM")]
        //[InlineData("02.04.1995", "chingio", false, "1995-02-04 12:00:00 AM")]
        //[InlineData("02.04.1995", "chingio", true, "1995-02-04 11:59:59 PM")]
        //public void Get_FromToDateTime_Return_Datetime(string fromToDate, string fromToTime, bool isToDateTime, string expected)
        //{
        //    var result = Helpers.GetFromToDateTime(fromToDate, fromToTime, isToDateTime);
        //    Assert.True(result.ToString(Constants.DateTimeFormat.YyyyMMddHHmmss) == Convert.ToDateTime(expected).ToString(Constants.DateTimeFormat.YyyyMMddHHmmss));
        //}

        [Theory]
        [MemberData(nameof(DataTest_Get_MinMissingNumberFromList_Return_MaxRange))]
        public void Get_MinMissingNumberFromList_Return_MaxRange(List<int> curUserCodes, int expected)
        {
            var result = Helpers.GetMinMissingNumberFromList(curUserCodes);
            Assert.True(result == expected);
        }

        [Fact]
        public void Get_MinMissingNumberFromList_When_ListEmpty()
        {
            var curUserCodes = new List<int>();
            var result = Helpers.GetMinMissingNumberFromList(curUserCodes);
            Assert.True(result == 1);
        }

        [Fact]
        public void Get_MinMissingNumberFromList_When_ListEmpty2()
        {
            var curUserCodes = new List<int>() { 1, 4, 14, 34 };
            var result = Helpers.GetMinMissingNumberFromList(curUserCodes);
            Assert.True(result == 2);
        }

        [Theory]
        [InlineData("0031234", "31234")]
        [InlineData("phuongcn", "phuongcn")]
        public void Get_RealValueOfNumberString(string numberString, string expected)
        {
            var result = Helpers.GetRealValueOfNumberString(numberString);
            Assert.True(result == expected);
        }

        [Fact]
        public void GetLoginPath()
        {
            var result = Helpers.GetLoginPath(GetHttpContext(), "d000001");
            Assert.True(result == ":///account/login?companycode=d000001");
        }

        [Fact]
        public void Check_ReplaceQueryStringParam_True()
        {
            var currentPageUrl = "http://api.dev.demasterprocloud.3si.vn/timezones?pageNumber=1&pageSize=2&sortColumn=0&sortDirection=desc";
            var paramToReplace = "pageSize";
            var newValue = "10";
            var result = Helpers.ReplaceQueryStringParam(currentPageUrl, paramToReplace, newValue);
            Assert.True(result == "http://api.dev.demasterprocloud.3si.vn/timezones?pageNumber=1&pageSize=10&sortColumn=0&sortDirection=desc");
        }

        [Fact]
        public void Check_ReplaceQueryStringParam_True2()
        {
            var currentPageUrl = "http://api.dev.demasterprocloud.3si.vn/timezones";
            var paramToReplace = "pageSize";
            var newValue = "10";
            var result = Helpers.ReplaceQueryStringParam(currentPageUrl, paramToReplace, newValue);
            Assert.True(result == "http://api.dev.demasterprocloud.3si.vn/timezones?pageSize=10");
        }
        [Fact]
        public void Check_BuildQueryStringParam_Success()
        {
            var currentPageUrl = "http://api.dev.demasterprocloud.3si.vn/timezones";
            Dictionary<string, object> parameters = new Dictionary<string, object>() {
                { "1",new Account(){ Username="phuongcn",Password="123456"} } ,
                { "2",new Account(){ Username="phuongdragon",Password="12345678"} } ,
            };
            var result = Helpers.BuildQueryStringParam(currentPageUrl, parameters);
            Assert.True(result == "http://api.dev.demasterprocloud.3si.vn/timezones?1=DeMasterProCloud.DataAccess.Models.Account&2=DeMasterProCloud.DataAccess.Models.Account");
        }

        [Fact]
        public void Check_FileEquals_Return_True()
        {
            var file1 = new byte[] { 1, 23, 4 };
            var file2 = new byte[] { 1, 23, 4 };
            var result = Helpers.FileEquals(file1, file2);
            Assert.True(result);
        }

        [Fact]
        public void Check_FileEquals_Return_False()
        {
            var file1 = new byte[] { 1, 23, 4 };
            var file2 = new byte[] { 23, 1, 4 };
            var result = Helpers.FileEquals(file1, file2);
            Assert.False(result);
        }

        [Fact]
        public void Check_FileEquals_Return_False2()
        {
            var file1 = new byte[] { 1, 23, 4, 6 };
            var file2 = new byte[] { 23, 1, 4 };
            var result = Helpers.FileEquals(file1, file2);
            Assert.False(result);
        }

        [Fact]
        public void Check_FileEquals2_Return_True()
        {
            var directory = Directory.GetCurrentDirectory();
            var path = directory + "\\Test1.txt";
            var path2 = directory + "\\Test2.txt";
            if (!File.Exists(path))
            {
                using (var writer = new StreamWriter(path))
                {
                    writer.Write("Test");
                }
            }
            if (!File.Exists(path2))
            {
                using (var writer = new StreamWriter(path2))
                {
                    writer.Write("Test");
                }
            }
            var result = Helpers.FileEquals(path, path2);
            Assert.True(result);
        }


        [Fact]
        public void SplitList_Test()
        {
            var locations = new List<Account>();
            locations.Add(new Account()
            {
                Username = "phuongcn",
                Password = "123456",
            });
            locations.Add(new Account()
            {
                Username = "phuongngoc",
                Password = "12345678",
            });
            var size = 300;
            var result = Helpers.SplitList(locations, size);
            Assert.True(result.First().Count == 2);
        }

        [Fact]
        public void SplitList_Success()
        {
            var locations = new List<Account>();
            locations.Add(new Account()
            {
                Username = "phuongcn",
                Password = "123456",
            });
            locations.Add(new Account()
            {
                Username = "phuongngoc",
                Password = "12345678",
            });
            var result = Helpers.SplitList(locations);
            Assert.True(result.First().Count == 2);
        }

        [Theory]
        [InlineData("ivbor", ".png")]
        [InlineData("/9j/4", ".jpg")]
        [InlineData("aaaaf", ".mp4")]
        [InlineData("aaaba", ".ico")]
        [InlineData("jvber", ".pdf")]
        [InlineData("umfyi", ".rar")]
        [InlineData("e1xyd", ".rtf")]
        [InlineData("77u/m", ".srt")]
        [InlineData("mqowm", ".srt")]
        [InlineData("u1pkc", ".txt")]
        [InlineData("data:", "")]
        [InlineData("data:u1pkc", "")]
        [InlineData("", "")]
        [InlineData("abcd:", "")]
        [InlineData("data:image/png;base64,", ".png")]
        public void GetFileExtension_Success(string base64String, string expected)
        {
            var result = Helpers.GetFileExtension(base64String);
            Assert.True(result == expected);
        }

        //[Theory]
        //[InlineData("online", "Connected")]
        //[InlineData("offline", "Disconneted")]
        //public void GetIcuStatus_Test(string status, string expected)
        //{
        //    var result = Helpers.GetIcuStatus(status);
        //    Assert.True(result.ToString() == expected);
        //}

        [Fact]
        public void GetStringFromValueSetting_Test()
        {
            string value = @"[ 'Small','Medium','Large']";
            var result = Helpers.GetStringFromValueSetting(value);
            Assert.True(result == "Small");
        }

        [Fact]
        public void GetStringFromValueSetting_When_FormatWrong()
        {
            string value = @"[]";
            var result = Helpers.GetStringFromValueSetting(value);
            Assert.True(result == string.Empty);
        }
        private DefaultHttpContext GetHttpContext()
        {
            var claims = new[]
           {
                new Claim(ClaimTypes.Name, "phong.ld@3si.vn"),
                new Claim(Constants.ClaimName.AccountId, "1"),
                new Claim(Constants.ClaimName.CompanyId, "1"),
                new Claim(Constants.ClaimName.CompanyCode, "d000001"),
                new Claim(Constants.ClaimName.CompanyName, "duali"),
                new Claim(Constants.ClaimName.AccountType, "2"),
                new Claim(Constants.ClaimName.BearerToken, "123456")
            };
            var appIdentity = new ClaimsIdentity(claims);
            return new DefaultHttpContext { User = new ClaimsPrincipal(appIdentity) };
        }
        private DefaultHttpContext GetHttpContext2()
        {
            var claims = new[]
              {
                new Claim(Constants.ClaimName.UserCode, "123456")
            };
            var appIdentity = new ClaimsIdentity(claims);
            return new DefaultHttpContext { User = new ClaimsPrincipal(appIdentity) };
        }
        public static IEnumerable<object[]> DataTest_Get_MinMissingNumberFromList_Return_MaxRange()
        {
            return new List<object[]>
            {
            new object[] { new List<int>{ 1,2,3,4,5 },6 },
            new object[] { new List<int> { 1, 4, 14, 34 },2 },
            };
        }
    }
}
