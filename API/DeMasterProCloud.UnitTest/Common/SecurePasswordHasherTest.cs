﻿using DeMasterProCloud.Common.Infrastructure;
using Xunit;

namespace DeMasterProCloud.UnitTest.Common
{
    public class SecurePasswordHasherTest
    {

        [Fact]
        public void CreatesHashFromPassword_Test()
        {
            var password = "123456";
            var iterations = 10;
            var result = SecurePasswordHasher.Hash(password, iterations);
            Assert.True(result != password);
        }

        [Fact]
        public void Creates_HashFromPassword_With10000Iterations()
        {
            var password = "123456";
            var result = SecurePasswordHasher.Hash(password);
            Assert.True(result != password);
        }

        [Fact]
        public void Verify_Password_Return_True()
        {
            var password = "123456";
            var hashedPassword = SecurePasswordHasher.Hash(password);
            var result = SecurePasswordHasher.Verify(password, hashedPassword);
            Assert.True(result);
        }

        [Fact]
        public void Verify_Password_Return_False()
        {
            var password = "123456";
            var hashedPassword = SecurePasswordHasher.Hash("1234567");
            var result = SecurePasswordHasher.Verify(password, hashedPassword);
            Assert.False(result);
        }
    }
}
