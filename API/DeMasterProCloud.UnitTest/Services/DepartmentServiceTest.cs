﻿using System;
using System.Collections.Generic;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;
using DeMasterProCloud.Common.Infrastructure;

namespace DeMasterProCloud.UnitTest.Services
{
    public class DepartmentServiceTest
    {
        private readonly Mock<DepartmentService> _mockDepartmentService;
        private readonly List<Department> _departments;
        private readonly Mock<UnitOfWork> _mockUnitOfWork;

        public DepartmentServiceTest()
        {
            var mockLogger = new Mock<ILogger<DepartmentService>>();

            //Setup environment
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);

            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();
            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            _departments = TestHelper.GetTestDepartments();
            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock UnitOfWork
            var appDbContext = TestHelper.GetAppDbContext();
            if (!appDbContext.Department.Any())
            {
                foreach (var department in _departments)
                {
                    department.Id = 0;
                    appDbContext.Department.Add(department);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(department).State = EntityState.Detached;
                }
            }

            _mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
            _mockDepartmentService =
                new Mock<DepartmentService>(_mockUnitOfWork.Object, mockHttpContext.Object, mockLogger.Object);
        }

        [Fact]
        public void InitData_Test()
        {
            var departmentModel = Mapper.Map<DepartmentModel>(_departments.First());
            _mockDepartmentService.Object.InitDepartment(departmentModel);
        }

        [Fact]
        public void IsDepartmentNumberExist_Test()
        {
            var departmentModel = Mapper.Map<DepartmentModel>(_departments.First());
            departmentModel.Id = 10;
            var result = _mockDepartmentService.Object.IsDepartmentNumberExist(departmentModel);
            Assert.True(result);
        }

        [Fact]
        public void Add_Department_Success()
        {
            var departmentModel = new DepartmentModel
            {
                Name = "department name test 1",
                Number = "2",
                ParentId = 1
            };
            _mockDepartmentService.Object.Add(departmentModel);
        }

        [Fact]
        public void Add_Department_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDepartmentService.Object.Add(null));
        }

        [Fact]
        public void Update_Department_Susscess()
        {
            var model = new DepartmentModel
            {
                Id = _departments.First().Id,
                Name = "department name is updated",
                Number = "2",
                ParentId = 1
            };
            _mockDepartmentService.Object.Update(model);
            var departmentUpdated = _mockUnitOfWork.Object.AppDbContext.Department.Find(_departments.First().Id);
            Assert.True(departmentUpdated.DepartName == model.Name);
            Assert.True(departmentUpdated.DepartNo == model.Number);
            Assert.True(departmentUpdated.ParentId == model.ParentId);
        }

        [Fact]
        public void Update_Department_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDepartmentService.Object.Update(null));
        }

        [Fact]
        public void DeleteRange_Department_Success()
        {
            _mockDepartmentService.Object.DeleteRange(_departments.ToList());
            var listDepartmentDeleted = _mockUnitOfWork.Object.AppDbContext.Department.ToList();
            foreach (var item in listDepartmentDeleted)
            {
                Assert.True(item.IsDeleted);
            }
        }

        [Fact]
        public void DeleteRange_Department_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDepartmentService.Object.DeleteRange(null));
        }

        [Fact]
        public void Delete_Department_Succcess()
        {
            _mockDepartmentService.Object.Delete(_departments.First());
            var deviceUpdated = _mockUnitOfWork.Object.AppDbContext.Department.Find(_departments.First().Id);
            Assert.True(deviceUpdated.IsDeleted);
        }

        [Fact]
        public void DepartmentExist_Test()
        {
            var department = _departments.First();
            department.Id = 5;
            var departmentModel1 = Mapper.Map<DepartmentModel>(department);
            var result1 = _mockDepartmentService.Object.IsDepartmentNumberExist(departmentModel1);
            Assert.True(result1);

            var departmentModel2 = Mapper.Map<DepartmentModel>(_departments.Last());
            var result2 = _mockDepartmentService.Object.IsDepartmentNumberExist(departmentModel2);
            Assert.True(result2 == false);
        }

        [Fact]
        public void GetIdAndCompany_Test()
        {
            var result1 = _mockDepartmentService.Object.GetByIdAndCompany(_departments.First().Id, _departments.First().CompanyId);
            Assert.True(result1 != null);
            var result2 = _mockDepartmentService.Object.GetByIdAndCompany(_departments.First().Id, 2);
            Assert.True(result2 == null);
        }

        [Fact]
        public void GetByIdsAndCompany_Test()
        {
            var department1 = _departments.First();
            var department2 = _departments.ElementAt(1);
            var result1 =
                _mockDepartmentService.Object.GetByIdsAndCompany(new List<int> { department1.Id, department2.Id }, 1);
            Assert.True(result1[0].DepartName == department1.DepartName);
            Assert.True(result1[1].DepartName == department2.DepartName);
        }

        [Fact]
        public void GetDeparmentHierarchy_Test()
        {
            var result1 = _mockDepartmentService.Object.GetDeparmentHierarchy();
            Assert.True(result1 != null);
        }

        [Fact]
        public void GetDescendantsOrself_Test()
        {
            var result = _mockDepartmentService.Object.GetDescendantsOrself(_departments.First().Id, 1);
            Assert.True(result.Any());
        }

        [Fact]
        public void GetDescendantsOrself_Test_ReturnNull()
        {
            var result = _mockDepartmentService.Object.GetDescendantsOrself(10, 10);
            Assert.True(result == null);
        }

        [Fact]
        public void IsDepartmentNameExist_Test()
        {
            var departmentModel = Mapper.Map<DepartmentModel>(_departments.First());
            departmentModel.Id = 10;
            var result = _mockDepartmentService.Object.IsDepartmentNameExist(departmentModel);
            Assert.True(result);
        }

        [Fact]
        public void IsDepartmentNameExist_Test_ReturnNull()
        {
            var departmentModel = Mapper.Map<DepartmentModel>(_departments.First());
            var result = _mockDepartmentService.Object.IsDepartmentNameExist(departmentModel);
            Assert.True(result == false);
        }

        [Fact]
        public void Delete_Department_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDepartmentService.Object.Delete(null));
        }

        [Theory]
        [InlineData("", 1, 10, 1, "desc")]
        [InlineData("", 1, 10, 11, "desc")]
        public void Get_Paginated_Department_Return_ListDepartment_Success(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection)
        {

            var result = _mockDepartmentService.Object.GetPaginated(filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
            var expected = _departments.Select(x => x.Id).ToList();
            var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
            Assert.True(!resultNotExpected.Any() && expectedNotResult.Any());
        }

        [Fact]
        public void Get_PaginatedDevice_Return_ListIcuDevice_Fail()
        {
            var result = _mockDepartmentService.Object.GetPaginated("1000", 1, 10, 1, "desc", out _, out _);
            Assert.True(!result.Any());
        }
    }
}