using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class AccessGroupDeviceServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly Mock<AccessGroupService> _mockAccessGroupService;
        private readonly List<AccessGroup> _accessGroups;
        private readonly AppDbContext _appDbContext;

        public AccessGroupDeviceServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;

            _accessGroups = TestHelper.GetTestAccessGroups();

            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock UnitOfWork
            _appDbContext = TestHelper.GetAppDbContext();

            if (!_appDbContext.AccessGroup.Any())
            {
                foreach (var accessGroup in _accessGroups)
                {
                    accessGroup.Id = 0;
                    _appDbContext.AccessGroup.Add(accessGroup);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(accessGroup).State = EntityState.Detached;
                }
            }
            _mockUnitOfWork = new Mock<UnitOfWork>(_appDbContext, mockHttpContext.Object);
            _mockAccessGroupService = new Mock<AccessGroupService>(_mockUnitOfWork.Object, _configuration);
        }

        //[Fact]
        //public void Assign_AccessLevel_Success()
        //{
        //    var testAccessGroupId = 1;
        //    var testUserIds = new List<int> {1, 2, 3};
        //    _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, testUserIds);
        //}
    }
}
