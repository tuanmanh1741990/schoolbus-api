﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataModel.Building;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class BuildingServiceTest
    {
        //private readonly IConfigurationRoot _configuration;
        //private readonly Mock<BuildingService> _mockBuildingService;
        //private readonly List<Building> _buildings;
        //public BuildingServiceTest()
        //{
        //    _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
        //    ApplicationVariables.Configuration = _configuration;
        //    _buildings = TestHelper.GetTestBuildings();
        //    var mockEnviroment = new Mock<IHostingEnvironment>();
        //    mockEnviroment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
        //    //var mockUserLogService = new Mock<IUserLogService>();
        //    var mockLogger = new Mock<ILogger<BuildingService>>();
        //    var mockQueueService = new Mock<IQueueService>();

        //    //Initial mapping
        //    AutoMapperConfig.Initialize();

        //    //Mock HttpContext
        //    var mockHttpContext = new Mock<IHttpContextAccessor>();

        //    mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
        //    //Mock UnitOfWork

        //    var appDbContext = TestHelper.GetAppDbContext();

        //    var mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
        //    _mockBuildingService = new Mock<BuildingService>(mockUnitOfWork.Object, mockQueueService.Object, mockHttpContext.Object,
        //        mockLogger.Object);
        //}


        ////void Add(BuildingModel model);
        ////void AddDoors(int id, List<int> doorIds);
        ////List<Building> GetBuildings();
        ////List<BuildingDoorModel> GetDoors(int id);
        ////IQueryable<BuildingDoorModel> GetPaginatedDoors(int id, string filter, int pageNumber, int pageSize, int sortColumn,
        ////    string sortDirection, out int totalRecords, out int recordsFiltered);
        ////void Delete(int id);
        ////void DeleteDoors(int id, List<int> doorIds);
        ////void Update(int id, BuildingModel model);
        ////bool IsExistedBuildingName(int id, string name);
        //[Fact]
        //public void Add_Test()
        //{
        //    BuildingModel model = new BuildingModel()
        //    {
        //        Id = 1,
        //        Name = "Duali",

        //    };
        //    _mockBuildingService.Object.Add(model);
        //}

        //[Fact]
        //public void AddDoors_Test()
        //{
        //    var doorIds = new List<int>();
        //    doorIds.Add(1);
        //    _mockBuildingService.Object.AddDoors(1, doorIds);
        //}


        //[Fact]
        //public void Delete_Test()
        //{
        //    BuildingModel model = new BuildingModel()
        //    {
        //        Id = 1,
        //        Name = "Duali",

        //    };
        //    _mockBuildingService.Object.Delete(1);
        //}

        //[Fact]
        //public void DeleteDoors_Test()
        //{
        //    var doorIds = new List<int>();
        //    doorIds.Add(1);
        //    _mockBuildingService.Object.DeleteDoors(1, doorIds);
        //}


        //[Fact]
        //public void GetBuildings_Test()
        //{
            
        //    _mockBuildingService.Object.GetBuildings();
        //}

        //[Fact]
        //public void GetDoors_Test()
        //{
        //    _mockBuildingService.Object.GetDoors(1);
        //}


        //[Fact]
        //public void GetPaginated_Test()
        //{
        //    _mockBuildingService.Object.GetPaginated("",1,1,1,"desc",out var totalRecords, out var recordsFiltered);
        //}

        //[Fact]
        //public void GetPaginated_Doors_Test()
        //{
        //    _mockBuildingService.Object.GetPaginatedDoors(1, "", 1, 1, 1, "desc", out var totalRecords, out var recordsFiltered);
        //}

        //[Fact]
        //public void IsExistedBuildingName_Test()
        //{
        //    _mockBuildingService.Object.IsExistedBuildingName(1, "test");
        //}

    }
}
