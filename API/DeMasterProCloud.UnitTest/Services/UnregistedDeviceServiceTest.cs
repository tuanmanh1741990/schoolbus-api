﻿using System;
using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using EfCore.InMemoryHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Moq;

namespace DeMasterProCloud.UnitTest.Services
{
    public class UnregistedDeviceServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly Mock<IUnregistedDeviceService> _mockUnregistedDevice;
        private readonly List<UnregistedDevice> _unregistedDevices;
        private readonly AppDbContext _appDbContext;

        public UnregistedDeviceServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;

            _unregistedDevices = TestHelper.GetTestUnregistedDevices();
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            //Mock HttpContext
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Init database
            _unregistedDevices = TestHelper.GetTestUnregistedDevices();
           //Mock UnitOfWork
           var builder = TestHelper.GetDbContextOptionsBuilderOptions();
            _appDbContext = InMemoryContextBuilder.Build<AppDbContext>(builder);

            ////Other mock
            var deviceService = new Mock<IDeviceService>();
            //if (!_appDbContext.UnregistedDevice.Any())
            //{
            //    foreach (var unregistedDevice in _unregistedDevices)
            //    {
            //        unregistedDevice.Id = 0;
            //        _appDbContext.UnregistedDevice.Add(unregistedDevice);
            //        _appDbContext.SaveChanges();
            //        _appDbContext.Entry(unregistedDevice).State = EntityState.Detached;
            //    }
            //}

            //Initial mapping
            AutoMapperConfig.Initialize();
            _mockUnitOfWork = new Mock<UnitOfWork>(_appDbContext, _mockHttpContext.Object);
            _mockUnregistedDevice = new Mock<IUnregistedDeviceService>(_mockUnitOfWork.Object, _mockHttpContext.Object, deviceService.Object);
        }

        //[Fact]
        //public void Add_MissingDevice_Return_Success()
        //{
        //    _mockUnregistedDevice.Object.AddMissingDevice(_unregistedDevices);
        //    var missingDeviceUpdated = _mockUnitOfWork.Object.AppDbContext.UnregistedDevice.Find(_unregistedDevices.First().Id);
        //    Assert.True(missingDeviceUpdated.Id == _unregistedDevices.First().Id);
        //    Assert.True(missingDeviceUpdated.DeviceAddress == _unregistedDevices.First().DeviceAddress);
        //    Assert.True(missingDeviceUpdated.IpAddress == _unregistedDevices.First().IpAddress);
        //    Assert.True(missingDeviceUpdated.Status == _unregistedDevices.First().Status);
        //    Assert.True(missingDeviceUpdated.CompanyId == _unregistedDevices.First().CompanyId);
        //}
    }
}
