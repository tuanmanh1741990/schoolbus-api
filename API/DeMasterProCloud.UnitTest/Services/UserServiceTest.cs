using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class UserServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly Mock<UserService> _mockUserService;
        private readonly Mock<IHttpContextAccessor> mockHttpContext;
        private Mock<IHostingEnvironment> mockEnvironment;
        private readonly Mock<ILogger<UserService>> mockLogger;
        private readonly Mock<IDepartmentService> mockDepartmentService;
        private readonly Mock<IQueueService> mockQueueService;
        private readonly Mock<IAccessGroupService> mockIAccessGroupService;
        private readonly List<User> _users;

        public UserServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;

            _users = TestHelper.GetTestUsers();

            mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);

            mockDepartmentService = new Mock<IDepartmentService>();
            mockQueueService = new Mock<IQueueService>();
            mockLogger = new Mock<ILogger<UserService>>();
            mockIAccessGroupService = new Mock<IAccessGroupService>();

            //Mock HttpContext 
            mockHttpContext = new Mock<IHttpContextAccessor>();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock UnitOfWork

            var appDbContext = TestHelper.GetAppDbContext();
            List<User> list = new List<User>();

            if (!appDbContext.User.Any())
            {
                foreach (var user in _users)
                {
                    user.Id = 0;
                    appDbContext.User.Add(user);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(user).State = EntityState.Detached;
                }
            }

            //_mockUnitOfWork.Setup(x => x.AppDbContext).Returns(appDbContext);
            _mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);

            _mockUserService = new Mock<UserService>(_mockUnitOfWork.Object, mockHttpContext.Object,
                mockEnvironment.Object, mockDepartmentService.Object, mockIAccessGroupService.Object,
                _configuration, mockLogger.Object);
        }

        //[Theory]
        //[MemberData(nameof(DataTest_InitData_Susscess))]
        //public void Check_InitData_Suscces(UserDataModel userDataModel)
        //{
        //    List<Department> departments = new List<Department>()
        //    {
        //        new Department()
        //        {
        //            Id=1,
        //            CompanyId=1,
        //            Status=(short)Status.Valid,
        //            DepartName="Duali",

        //        },
        //        new Department()
        //        {
        //            Id=2,
        //            CompanyId=2,
        //            Status=(short)Status.Valid,
        //            DepartName="Duali",

        //        }
        //   };

        //    mockDepartmentService.Setup(x => x.GetDescendantsOrself(It.IsAny<int>(), It.IsAny<int>())).Returns(departments);
        //    var mockUserService = new Mock<UserService>(_mockUnitOfWork.Object, mockHttpContext.Object, mockEnvironment.Object,
        //        mockUserLogService.Object, mockDepartmentService.Object, mockQueueService.Object, _configuration,
        //        mockLogger.Object);
        //    _mockUserService.Object.InitData(userDataModel);
        //}
        //[Fact]
        //public void Add_User_Susscess()
        //{
        //    var userModel = new UserModel()
        //    {
        //        FirstName = "phuong",
        //        LastName = "Cao",
        //        //UserCode = 1,
        //        CardId = "KQM6374KCCKL7I",
        //        DepartmentId = 1,
        //        KeyPadPassword = "IafzyvOubzdkwKOfDRvHjA==",
        //    };
        //    _mockUserService.Object.Add(userModel);
        //    var newUser = _mockUnitOfWork.Object.AppDbContext.User.ToList().Last();
        //    Assert.True(newUser.FirstName == userModel.FirstName);
        //    Assert.True(newUser.LastName == userModel.LastName);
        //    //Assert.True(newUser.UserCode == userModel.UserCode);
        //    Assert.True(newUser.CardId == userModel.CardId);
        //    Assert.True(newUser.DepartmentId == userModel.DepartmentId);
        //    Assert.True(newUser.CompanyId == 1);
        //}

        [Fact]
        public void Add_User_Return_NullException()
        {
            UserModel userModel = null;
            Assert.Throws<NullReferenceException>(() => _mockUserService.Object.Add(userModel));
        }

        //[Fact]
        //public void Update_User_Success()
        //{
        //    var userModel = new UserModel()
        //    {
        //        Id = _users.First().Id,
        //        FirstName = "phuong",
        //        LastName = "Cao",
        //        //UserCode = 1,
        //        CardId = "KQM6374KCCKL7I",
        //        DepartmentId = 1,
        //        KeyPadPassword = "IafzyvOubzdkwKOfDRvHjA==",
        //    };
        //    _mockUserService.Object.Update(userModel);
        //    var userUpdated = _mockUserService.Object.GetById(userModel.Id);
        //    Assert.True(userUpdated.FirstName == userModel.FirstName);
        //    Assert.True(userUpdated.LastName == userModel.LastName);
        //    //Assert.True(userUpdated.UserCode == userModel.UserCode);
        //    Assert.True(userUpdated.CardId == userModel.CardId);
        //    Assert.True(userUpdated.DepartmentId == userModel.DepartmentId);
        //}

        [Fact]
        public void Edit_User_Return_NullException()
        {
            UserModel userModel = null;
            Assert.Throws<NullReferenceException>(() => _mockUserService.Object.Update(userModel));
        }

        //[Fact]
        //public void Delete_User_Success()
        //{
        //    var user = _users.First();
        //    _mockUserService.Object.Delete(user);
        //    Assert.True(user == null);
        //}

        //[Fact]
        //public void Delete_User_Return_NullException()
        //{
        //    User user = null;
        //    Assert.Throws<NullReferenceException>(() => _mockUserService.Object.Delete(user));
        //}

        [Fact]
        public void DeleteRange_User_Success()
        {
            var users = _users.Where(c => c.CompanyId != 2).ToList();
            _mockUserService.Object.DeleteRange(users);

            var listUserDeleted = _mockUnitOfWork.Object.AppDbContext.User.Where(c => c.CompanyId != 2).ToList();
            foreach (var item in listUserDeleted)
            {
                Assert.True(item.IsDeleted = true);
            }
        }

        //[Fact]
        //public void DeleteRange_Return_NullException()
        //{
        //    Assert.Throws<ArgumentNullException>(() => _mockUserService.Object.DeleteRange(null));
        //}

        [Fact]
        public void Get_ByIdAndCompany_ReturnUser()
        {
            int id = _users.First().Id;
            int companyId = 1;
            var result = _mockUserService.Object.GetByIdAndCompany(id, companyId);
            Assert.True(result.Id == id);
            Assert.True(result.CompanyId == 1);
            Assert.True(result.DepartmentId == 1);
            //Assert.True(result.CardId == "A");
        }

        [Fact]
        public void Get_ByIdAndCompany_Return_NonExist()
        {
            int id = 1;
            int companyId = 5;
            var result = _mockUserService.Object.GetByIdAndCompany(id, companyId);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Get_ById_Return_User()
        //{
        //    int id = _users.First().Id;
        //    var result = _mockUserService.Object.GetById(id);
        //    Assert.True(result.Id == id);
        //    Assert.True(result.CompanyId == 1);
        //    Assert.True(result.DepartmentId == 1);
        //    Assert.True(!result.IsDeleted);
        //    Assert.True(result.CardId == "A");
        //}

        [Fact]
        public void Get_ById_Return_Non_Exist()
        {
            int id = -1;
            var result = _mockUserService.Object.GetById(id);
            Assert.True(result == null);

        }

        [Fact]
        public void CheckData_Return_True()
        {
            var result = _mockUserService.Object.CheckData();
            Assert.True(result);
        }

        //[Fact]
        //public void Get_ByIdsAndCompany_Return_ListUser()
        //{
        //    List<int> userIds = _users.Select(x => x.Id).Take(3).ToList();
        //    int companyId = 1;
        //    var result = _mockUserService.Object.GetByIdsAndCompany(userIds, companyId);
        //    var expected = new List<string> { "A", "B", "C" };
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.CardId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_ByIdsAndCompany_Return_Fail()
        {
            List<int> userIds = new List<int>() { 1, 2, 3 };
            int companyId = 3;
            var result = _mockUserService.Object.GetByIdsAndCompany(userIds, companyId);
            Assert.True(result.Count == 0);
        }

        //[Fact]
        //public void Get_Paginated_Return_ListUser()
        //{
        //    var result = _mockUserService.Object.GetPaginated("", 1, 10, 0, "desc", out _, out _);
        //    var expected = new List<string> { "A", "B", "D" };
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.CardId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_Paginated_Fail()
        {
            Assert.Throws<NullReferenceException>(() => _mockUserService.Object.GetPaginated("-1", 1, 10, 0, "desc", out _, out _, true, 0));
        }
        //[Fact]
        //public void Check_HasChange_Return_True()
        //{
        //    var userModel = new UserModel()
        //    {
        //        Id = 1,
        //        FirstName = "phuong",
        //        LastName = "Cao",
        //        //UserCode = 1,
        //        CardId = "KQM6374KCCKL7I",
        //        ExpiredDate = DateTime.Now.ToString(),
        //        EffectiveDate = DateTime.Now.AddDays(1).ToString(),
        //        DepartmentId = 1,
        //        KeyPadPassword = "IafzyvOubzdkwKOfDRvHjA==",
        //    };

        //    var user = _users.First();
        //    var result = _mockUserService.Object.HasChange(user, userModel);
        //    Assert.True(result);
        //}

        //[Fact]
        //public void FilterDataWithOrder_Return_ListUser()
        //{
        //    var result = _mockUserService.Object.FilterDataWithOrder("", 0, "desc", out _, out _,true);
        //    var expected = new List<string> { "A", "B", "D" };
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.CardId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void FilterDataWithOrder_Fail()
        {
            Assert.Throws<NullReferenceException>(() => _mockUserService.Object.FilterDataWithOrder("-1", 0, "desc", out _, out _, true, 0));
        }

        //[Fact]
        //public void Check_IsCardIdExist_Form_UserModel_Return_True()
        //{
        //    var userModel = Mapper.Map<UserModel>(_users.First());
        //    userModel.CardId = "B";
        //    var result = _mockUserService.Object.IsCardIdExist(userModel);
        //    Assert.True(result);
        //}

        //[Fact]
        //public void Check_IsCardIdExist_Form_UserModel_Return_False()
        //{
        //    var userModel = new UserModel()
        //    {
        //        Id = 1,
        //        FirstName = "phuong",
        //        LastName = "Cao",
        //        //UserCode = 1,
        //        CardId = "KQM6374KCCKL7I",
        //        ExpiredDate = DateTime.Now.ToString(CultureInfo.CurrentCulture),
        //        EffectiveDate = DateTime.Now.AddDays(1).ToString(CultureInfo.CurrentCulture),
        //        DepartmentId = 1,
        //        KeyPadPassword = "IafzyvOubzdkwKOfDRvHjA==",
        //    };
        //    var result = _mockUserService.Object.IsCardIdExist(userModel);
        //    Assert.False(result);
        //}

        [Fact]
        public void Check_IsCardIdExist_Return_True()
        {
            var cardId = "A";
            var result = _mockUserService.Object.IsCardIdExist(cardId);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsCardIdExist_Return_False()
        {
            var cardId = "Z";
            var result = _mockUserService.Object.IsCardIdExist(cardId);
            Assert.False(result);
        }
        //[Fact]
        //public void Check_IsUserCodeExist_Return_True()
        //{
        //    var userModel = Mapper.Map<UserModel>(_users.First());
        //    userModel.UserCode = 2;
        //    var result = _mockUserService.Object.IsUserCodeExist(userModel);
        //    Assert.True(result);
        //}

        //[Fact]
        //public void Check_IsUserCodeExist_Return_False()
        //{
        //    var userModel = new UserModel()
        //    {
        //        Id = 1,
        //        FirstName = "phuong",
        //        LastName = "Cao",
        //        UserCode = 1000,
        //        CardId = "KQM6374KCCKL7I",
        //        ExpiredDate = DateTime.Now.ToString(CultureInfo.CurrentCulture),
        //        EffectiveDate = DateTime.Now.AddDays(1).ToString(CultureInfo.CurrentCulture),
        //        DepartmentId = 1,
        //        KeyPadPassword = "IafzyvOubzdkwKOfDRvHjA==",
        //    };
        //    var result = _mockUserService.Object.IsUserCodeExist(userModel);
        //    Assert.False(result);
        //}

        [Fact]
        public void Check_IsKeyPadPasswordExist_Return_True()
        {
            int userId = _users.First().Id;
            string enctypedKeyPadPassword = _users.Skip(1).First().KeyPadPw;
            var result = _mockUserService.Object.IsKeyPadPasswordExist(userId, enctypedKeyPadPassword);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsKeyPadPasswordExist_Return_False()
        {
            int userId = _users.First().Id;
            string enctypedKeyPadPassword = _users.First().KeyPadPw;
            var result = _mockUserService.Object.IsKeyPadPasswordExist(userId, enctypedKeyPadPassword);
            Assert.False(result);
        }

        //[Fact]
        //public void Check_GetValidUsers_Success()
        //{
        //    var result = _mockUserService.Object.GetValidUsers();
        //    var expected = new List<string> { "A", "B"};
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.CardId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}
       
        //[Fact]
        //public void Get_ChangedContents_True()
        //{
        //    var userModel = new UserModel()
        //    {
        //        Id = 1,
        //        FirstName = "phuong",
        //        LastName = "Cao",
        //        UserCode = 1,
        //        CardId = "KQM6374KCCKL7I",
        //        ExpiredDate = DateTime.Now.ToString(),
        //        EffectiveDate = DateTime.Now.AddDays(1).ToString(),
        //        DepartmentId = 1,
        //        KeyPadPassword = "IafzyvOubzdkwKOfDRvHjA==",

        //    };

        //    var user = _users.First();
        //    var result = _mockUserService.Object.GetChangedContents(user, userModel);
        //    Assert.True(result.Count == 2);
        //    Assert.True(result.First() == string.Format(UserResource.msgChangeUserInfor, UserResource.lblCardId, user.UserCode));
        //    Assert.True(result.Skip(1).First() == string.Format(UserResource.msgChangeUserInfor, UserResource.lblKeyPadPassword, user.UserCode));
        //}

        //[Fact]
        //public void Get_ChangedContents_False()
        //{
        //    var user = _users.First();
        //    var userModel = Mapper.Map<UserModel>(user);
        //    var result = _mockUserService.Object.GetChangedContents(user, userModel);
        //    Assert.True(result.Count == 1);
        //    Assert.True(result.First() == string.Format(string.Format(UserResource.msgChangeUserInforUserCode, user.UserCode)));
        //}

        //[Fact]
        //public void GenerateTestData_Test()
        //{
        //    _mockUserService.Object.GenerateTestData(1);
        //}

        //public static IEnumerable<object[]> DataTest_InitData_Susscess()
        //{
        //    return new List<object[]>
        //    {
        //    new object[] {
        //      new UserDataModel() {
        //        Id = 1,
        //        Address = "ToaNhaKimAnh",
        //        CardId = "089574T7PMS5YM",
        //        DepartmentId = 1,
        //        FirstName = "Phuong",
        //        LastName = "cn",
        //        //UserCode = 66,

        //    } },
        //     new object[] {
        //      new UserDataModel() {
        //        Id = 0,
        //        Address = "ToaNhaKimAnh",
        //        CardId = "089574T7PMS5YM",
        //        DepartmentId = 1,
        //        FirstName = "Phuong",
        //        LastName = "cn",
        //        //UserCode = 0,
        //    } },
        //      new object[] {
        //      new UserDataModel() {
        //        Id = 0,
        //        Address = "ToaNhaKimAnh",
        //        CardId = "",
        //        DepartmentId = 1,
        //        FirstName = "Phuong",
        //        LastName = "cn",
        //        //UserCode = 66,
        //    } }
        //    };
        //}
    }
}
