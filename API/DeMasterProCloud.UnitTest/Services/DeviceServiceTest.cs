﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataModel.Device;
using Microsoft.AspNetCore.Mvc.Rendering;
using Xunit;
using IHolidayService = DeMasterProCloud.Service.IHolidayService;
using QueueHelper = DeMasterProCloud.Common.Infrastructure.QueueHelper;

namespace DeMasterProCloud.UnitTest.Services
{
    public class DeviceServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<DeviceService> _mockDeviceService;
        private readonly List<IcuDevice> _devices;
        private readonly Mock<UnitOfWork> mockUnitOfWork;
        private readonly Mock<IHttpContextAccessor> mockHttpContext;
        private readonly Mock<ILogger<DeviceService>> mockLogger;
        private readonly Mock<IQueueService> mockQueueService;
        private readonly Mock<IConsumerService> mockConsumerService;
        public DeviceServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _devices = TestHelper.GetTestIcuDevice();
            var mockEnviroment = new Mock<IHostingEnvironment>();
            mockEnviroment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            mockLogger = new Mock<ILogger<DeviceService>>();
            mockQueueService = new Mock<IQueueService>();
            mockConsumerService = new Mock<IConsumerService>();
            var mockAccessGroupService = new Mock<IAccessGroupService>();
            var mockTimezoneService = new Mock<ITimezoneService>();
            var mockHolidayService = new Mock<IHolidayService>();
            var mockNotificationService = new Mock<INotificationService>();
            var mockSettingService = new Mock<ISettingService>();
            //Initial mapping
            AutoMapperConfig.Initialize();

            var factory = QueueHelper.GetConnectionFactory(_configuration);
            ApplicationVariables.RabbitMqConsumerConnection = factory.CreateConnection();

            //Mock HttpContext
            mockHttpContext = new Mock<IHttpContextAccessor>();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            //Mock UnitOfWork

            var appDbContext = TestHelper.GetAppDbContext();
            var accessGroups = TestHelper.GetTestAccessGroups();
            var users = TestHelper.GetTestUsers();
            var accessGroupDevices = TestHelper.GetTestAccessGroupDevices();
            if (!appDbContext.IcuDevice.Any())
            {
                foreach (var device in _devices)
                {
                    device.Id = 0;
                    appDbContext.IcuDevice.Add(device);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(device).State = EntityState.Detached;
                }
            }

            if (!appDbContext.AccessGroup.Any())
            {
                foreach (var group in accessGroups)
                {
                    group.Id = 0;
                    appDbContext.AccessGroup.Add(group);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(group).State = EntityState.Detached;
                }
            }

            if (!appDbContext.User.Any())
            {
                foreach (var user in users)
                {
                    user.Id = 0;
                    appDbContext.User.Add(user);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(user).State = EntityState.Detached;
                }
            }

            if (!appDbContext.AccessGroupDevice.Any())
            {
                foreach (var agdv in accessGroupDevices)
                {
                    appDbContext.AccessGroupDevice.Add(agdv);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(agdv).State = EntityState.Detached;
                }
            }

            mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
            _mockDeviceService = new Mock<DeviceService>(mockUnitOfWork.Object, mockHttpContext.Object,
                _configuration, mockLogger.Object, mockQueueService.Object, mockAccessGroupService.Object,
                mockTimezoneService.Object, mockHolidayService.Object,
                mockNotificationService.Object);
        }

        [Fact]
        public void Check_InitData_When_IdEqualZero()
        {
            DeviceDataModel deviceData = new DeviceDataModel()
            {
                Id = 0,
                Passback = 1,
                VerifyMode = 1,
                SensorType = 1,
                MPRCount = 1,

            };
            mockUnitOfWork.Object.Save();
            _mockDeviceService.Object.InitData(deviceData);
        }

        //[Fact]
        //public void Check_InitData_When_IdNotEqualZero()
        //{
        //    DeviceDataModel deviceData = new DeviceDataModel()
        //    {
        //        Id = 1,
        //        Passback = 1,
        //        VerifyMode = 1,
        //        SensorType = 1,
        //        Condition = 2,
        //    };
        //    var result = _mockDeviceService.Object.InitData(deviceData);
        //    var Id = mockUnitOfWork.Object.AppDbContext.Timezone.Where(x => x.CompanyId == 1).ToList().First().Id;
        //    Assert.True(result.ActiveTimezoneId == Id);
        //    Assert.True(result.SensorType == 1);
        //    Assert.True(result.VerifyMode == 1);
        //    Assert.True(result.Passback == 1);
        //    Assert.True(result.LockOpenDuration == 0);
        //    Assert.True(result.Condition == 2);
        //}

        [Fact]
        public void Add_Device_Success()
        {
            var deviceModel = new DeviceModel
            {
                Alarm = true,
                VerifyMode = 0,
                ActiveTimezoneId = 0,
                PassageTimezoneId = 2,
                SensorType = 1,
                CloseReverseLock = true,
                LedReader0 = 1,
                LedReader1 = 1,
                MPRCount = 2,
                LockOpenDuration = 1,
                Passback = 1,
                SensorDuration = 1,
                DeviceAddress = "196E69",
                DoorName = "Door 196E69",
                //NetworkEnable = false,
                IpAddress = "192.168.1.1",
                MPRInterval = 1,
                DeviceType = 0,
                BackupPeriod = 1
            };
            
            _mockDeviceService.Object.Add(deviceModel);

            // TODO update device attribute
            var newDevice = mockUnitOfWork.Object.AppDbContext.IcuDevice.ToList().Last();
            Assert.True(newDevice.DeviceAddress == deviceModel.DeviceAddress);
            Assert.True(newDevice.IpAddress == deviceModel.IpAddress);
            Assert.True(newDevice.PassbackRule == deviceModel.Passback);
            Assert.True(newDevice.IpAddress == deviceModel.IpAddress);
            Assert.True(newDevice.CompanyId == 1);
            Assert.True(newDevice.Status == (short)Status.Valid);
            Assert.True(newDevice.ActiveTzId == deviceModel.ActiveTimezoneId);
            Assert.True(newDevice.PassageTzId == deviceModel.PassageTimezoneId);
            Assert.True(newDevice.LedReader0 == deviceModel.LedReader0);
            Assert.True(newDevice.LedReader1 == deviceModel.LedReader1);
            Assert.True(newDevice.SensorType == deviceModel.SensorType);
            Assert.True(newDevice.MPRCount == deviceModel.MPRCount);
            Assert.True(newDevice.MPRInterval == deviceModel.MPRInterval);
            Assert.True(newDevice.SensorDuration == deviceModel.SensorDuration);
            //Assert.True(newDevice.DeviceType == deviceModel.DeviceType);
            Assert.True(newDevice.BackupPeriod == deviceModel.BackupPeriod);
            Assert.True(newDevice.Name == deviceModel.DoorName);
        }

        [Fact]
        public void Add_Device_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDeviceService.Object.Add(null));
        }

        [Fact]
        public void Update_Device_Susscess()
        {
            var deviceModel = new DeviceModel
            {
                Id = _devices.First().Id,
                Alarm = true,
                VerifyMode = 1,
                ActiveTimezoneId = 0,
                PassageTimezoneId = 2,
                SensorType = 1,
                CloseReverseLock = true,
                LedReader0 = 1,
                LedReader1 = 1,
                MPRCount = 1,
                LockOpenDuration = 1,
                Passback = 1,
                SensorDuration = 2,
                DeviceAddress = "196E61",
                //NetworkEnable = false,
                IpAddress = "192.168.1.2",
                MPRInterval = 1,
                DeviceType = 0,
                BackupPeriod = 1,
                DoorName = "Door 196E69"
            };
            _mockDeviceService.Object.Update(deviceModel);
            var deviceUpdated = mockUnitOfWork.Object.AppDbContext.IcuDevice.Find(_devices.First().Id);
            Assert.True(deviceUpdated.DeviceAddress == deviceModel.DeviceAddress);
            Assert.True(deviceUpdated.IpAddress == deviceModel.IpAddress);
            Assert.True(deviceUpdated.PassbackRule == deviceModel.Passback);
            Assert.True(deviceUpdated.IpAddress == deviceModel.IpAddress);
            Assert.True(deviceUpdated.CompanyId == 1);
            Assert.True(deviceUpdated.Status == (short)Status.Valid);
            Assert.True(deviceUpdated.ActiveTzId == deviceModel.ActiveTimezoneId);
            Assert.True(deviceUpdated.PassageTzId == deviceModel.PassageTimezoneId);
            Assert.True(deviceUpdated.LedReader0 == deviceModel.LedReader0);
            Assert.True(deviceUpdated.LedReader1 == deviceModel.LedReader1);
            Assert.True(deviceUpdated.SensorType == deviceModel.SensorType);
            Assert.True(deviceUpdated.MPRCount == deviceModel.MPRCount);
            Assert.True(deviceUpdated.SensorDuration == deviceModel.SensorDuration);
            //Assert.True(deviceUpdated.DeviceType == deviceModel.DeviceType);
            Assert.True(deviceUpdated.BackupPeriod == deviceModel.BackupPeriod);
            Assert.True(deviceUpdated.Name == deviceModel.DoorName);
        }

        [Fact]
        public void Update_Device_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDeviceService.Object.Update(null));
        }

        [Fact]
        public void Delete_Device_Succcess()
        {
            _mockDeviceService.Object.Delete(_devices.First());
            var deviceUpdated = mockUnitOfWork.Object.AppDbContext.IcuDevice.Find(_devices.First().Id);
            Assert.True(deviceUpdated.IsDeleted = true);
        }

        [Fact]
        public void Delete_Device_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDeviceService.Object.Delete(null));
        }

        //[Fact]
        //public void DeleteRange_Device_Succcess()
        //{
        //    _mockDeviceService.Object.DeleteRange(_devices.Take(2).ToList());
        //    var listDeviceDeleted = mockUnitOfWork.Object.AppDbContext.IcuDevice.Take(2).ToList();
        //    foreach (var item in listDeviceDeleted)
        //    {
        //        Assert.True(item == null);
        //    }
        //}

        //[Theory]
        //[InlineData("", 1, 10, 1, "desc")]
        //[InlineData("", 1, 10, 11, "desc")]
        //public void Get_PaginatedDevice_Return_ListIcuDevice_Success(string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockDeviceService.Object.GetPaginated(filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = _devices.Select(x => x.Id).Take(4).ToList();
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_PaginatedDevice_Return_ListIcuDevice_Fail()
        {
            var result = _mockDeviceService.Object.GetPaginated("1000", 1, 10, 1, "desc", out _, out _);
            Assert.True(!result.Any());
        }

        [Fact]
        public void Check_ToggleStatus_FromValidToInvalid()
        {
            _mockDeviceService.Object.UpdateDeviceStatus(_devices.First(), false);
            var device = mockUnitOfWork.Object.AppDbContext.IcuDevice.Find(_devices.First().Id);
            Assert.True(device.Status == (short)Status.Invalid);
        }

        //[Fact]
        //public void Check_ToggleStatus_FromInValidToValid()
        //{
        //    _mockDeviceService.Object.ToggleStatus(_devices.Skip(1).FirstOrDefault());
        //    var device = mockUnitOfWork.Object.AppDbContext.IcuDevice.Find(_devices.Skip(1).FirstOrDefault().Id);
        //    Assert.True(device.Status == (short)Status.Valid);
        //}

        [Fact]
        public void Check_ToggleStatus_Return_NullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockDeviceService.Object.UpdateDeviceStatus(null, true));
        }

        //[Fact]
        //public void Get_ActiveDeviceByCompanyAndAddress_Return_IcuDevice()
        //{
        //    var result = _mockDeviceService.Object.GetActiveDeviceByCompanyAndAddress(1, "196E69");
        //    Assert.True(result != null);
        //}

        [Fact]
        public void Get_ActiveDeviceByCompanyAndAddress_Return_Non_Exist()
        {
            var result = _mockDeviceService.Object.GetActiveDeviceByCompanyAndAddress(1, "196E68");
            Assert.True(result == null);
        }

        [Fact]
        public void Get_ByIdAndCompany_Return_IcuDevice()
        {
            var result = _mockDeviceService.Object.GetByIdAndCompany(_devices.First().Id, 1);
            Assert.True(result != null);
        }

        [Fact]
        public void Get_ByIdAndCompany_Return_Non_Exist()
        {
            var result = _mockDeviceService.Object.GetByIdAndCompany(_devices.First().Id, 3);
            Assert.True(result == null);
        }

        [Fact]
        public void Check_IsDeviceAddressExist_Return_True()
        {
            var deviceModel1 = new DeviceModel()
            {
                Id = 1,
                Alarm = true,
                VerifyMode = 0,
                ActiveTimezoneId = 0,
                PassageTimezoneId = 1,
                SensorType = 1,
                CloseReverseLock = true,
                LedReader0 = 1,
                LedReader1 = 1,
                MPRCount = 1,
                LockOpenDuration = 1,
                Passback = 2,
                SensorDuration = 1,
                DeviceAddress = "196E69"
            };
            var result = _mockDeviceService.Object.IsDeviceAddressExist(deviceModel1);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsDeviceAddressExist_Return_False()
        {
            var deviceModel = new DeviceModel()
            {
                Id = 1,
                Alarm = true,
                VerifyMode = 0,
                ActiveTimezoneId = 0,
                PassageTimezoneId = 1,
                SensorType = 1,
                CloseReverseLock = true,
                LedReader0 = 1,
                LedReader1 = 1,
                MPRCount = 1,
                LockOpenDuration = 1,
                Passback = 1,
                SensorDuration = 1,
                DeviceAddress = "196E67"
            };
            var result = _mockDeviceService.Object.IsDeviceAddressExist(deviceModel);
            Assert.False(result);
        }

        [Fact]
        public void Get_DeviceByCompanyAndAddress_Return_IcuDevice()
        {
            var result = _mockDeviceService.Object.GetDeviceByCompanyAndAddress(1, "196E69");
            Assert.True(result != null);
        }

        [Fact]
        public void Get_DeviceByCompanyAndAddress_Return_NonExist()
        {
            var result = _mockDeviceService.Object.GetDeviceByCompanyAndAddress(1, "196E68");
            Assert.True(result == null);
        }

        //[Fact]
        //public void Get_ByIdsAndCompany_Return_ListIcuDevice_Success()
        //{
        //    var result = _mockDeviceService.Object.GetByIdsAndCompany(_devices.Select(x => x.Id).ToList(), 1);
        //    var expected = _devices.Select(x => x.Id).Take(4).ToList();
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_ByIdsAndCompany_Return_ListIcuDevice_Fail()
        {
            var result = _mockDeviceService.Object.GetByIdsAndCompany(_devices.Select(x => x.Id).ToList(), 0);
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void Get_GenerateTestData_Return_ListIcuDevice_Success()
        {
            var result = _mockDeviceService.Object.GenerateTestData(4);
            Assert.True(result.Any());
        }

        [Fact]
        public void Get_GenerateTestData_Return_ListIcuDevice_Fail()
        {
            var result = _mockDeviceService.Object.GenerateTestData(-1);
            Assert.True(result.Count == 0); ;
        }

        [Fact]
        public void RegisterConsumerService_IcuDevcie()
        {
            var icuDevice = _devices.First();
            _mockDeviceService.Object.RegisterConsumerService(icuDevice);
            _mockDeviceService.Verify();
        }

        [Fact]
        public void Get_DoorList_Return_ListDoor_Success()
        {
            var result = _mockDeviceService.Object.GetDoorList().ToList();
            Assert.True(result.Count == 4);
        }

        [Fact]
        public void Check_HasTimezone_Return_True()
        {
            var result = _mockDeviceService.Object.HasTimezone(1, 1);
            Assert.True(result);
        }

        [Fact]
        public void Check_HasTimezone_Return_False()
        {
            var result = _mockDeviceService.Object.HasTimezone(5, 3);
            Assert.False(result);
        }

        //[Fact]
        //public void GetListIcuDevicesByUserId_ReturnList()
        //{
        //    var devices = _mockDeviceService.Object.GetListIcuDevicesByUserId(1);
        //    Assert.True(devices.Count == 2);
        //}
    }
}
