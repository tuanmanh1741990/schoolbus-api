﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class HolidayServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<HolidayService> _mockHolidayService;
        private readonly List<Holiday> _holidays;
        public HolidayServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _holidays = TestHelper.GetTestHolidays();
            var mockEnviroment = new Mock<IHostingEnvironment>();
            mockEnviroment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            var mockLogger = new Mock<ILogger<HolidayService>>();
            var mockQueueService = new Mock<IQueueService>();

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            //Mock UnitOfWork

            var appDbContext = TestHelper.GetAppDbContext();

            if (!appDbContext.Holiday.Any())
            {
                var newHoliday = new List<Holiday>();
                foreach (var holiday in _holidays)
                {
                    holiday.Id = 0;
                    newHoliday.Add(holiday);
                }
                appDbContext.Holiday.AddRange(newHoliday);
                appDbContext.SaveChanges();
            }
            
            var mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
            _mockHolidayService = new Mock<HolidayService>(mockUnitOfWork.Object, mockQueueService.Object, mockHttpContext.Object,
                mockLogger.Object);
        }

        //[Fact]
        //public void InitData_Test()
        //{
        //    HolidayModel holidayData = new HolidayModel()
        //    {
        //        Id = 1,
        //        Name = "Duali",
        //        CompanyId = 1,
        //        Type = 1,
        //        StartDate = DateTime.Now.ToString(),
        //        EndDate = DateTime.Now.AddYears(1).ToString(),
        //        Recursive = false,
        //        Remarks = "Duali",
        //        Status = 0,
        //        //HolidayTypes = new List<SelectListItem>(),
        //    };
        //    _mockHolidayService.Object.InitData(holidayData);
        //    //_mockHolidayService.Object.
        //}

        [Fact]
        public void Add_Test()
        {
            //HolidayModel holidayData = new HolidayModel();
            //holidayData.Id = 0;
            //holidayData.Name = "Duali";
            //holidayData.CompanyId = 1;
            //holidayData.Type = 1;
            //holidayData.StartDate = "2018.12.10";
            //holidayData.EndDate = "2020.12.10";
            //holidayData.Recursive = false;
            //holidayData.Remarks = "Duali";
            //holidayData.Status = 0;
            HolidayModel holidayData = new HolidayModel()
            {
                Id = 0,
                Name = "Duali",
                CompanyId = 1,
                Type = 1,
                StartDate = "2018.12.10",
                EndDate = "2019.12.11",
                Recursive = false,
                Remarks = "Duali",
            };
            _mockHolidayService.Object.Add(holidayData);

        }

        [Fact]
        public void Update_Test()
        {

            var holidayModel = Mapper.Map<HolidayModel>(_holidays.First());
            var company = new Company()
            {
                Id = 1,
                Code = "p000001",
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                ExpiredFrom = DateTime.Now,
                ExpiredTo = DateTime.Now.AddDays(10),
                Name = "3si",
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                Timezone = TestHelper.GetTestTimezone()
            };
            Holiday holiday = new Holiday()
            {
                Id = 1,
                CompanyId = 1,
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                EndDate = DateTime.Now,
                Name = "Duali",
                Recursive = true,
                Remarks = "Duali",
                StartDate = DateTime.Now,
                Type = 1,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                Company = company,
            };
            _mockHolidayService.Object.Update(holidayModel, holiday);

        }

        [Fact]
        public void IsOverLapDurationTime_Test()
        {
            _mockHolidayService.Object.IsOverLapDurationTime(1, DateTime.Now, DateTime.Now.AddDays(3));
        }

        [Fact]
        public void GetHolidayByIdAndCompany_Test()
        {
            _mockHolidayService.Object.GetHolidayByIdAndCompany(1, 1);
        }


        [Fact]
        public void Delete_Test()
        {
            _mockHolidayService.Object.Delete(_holidays.First());

        }





        [Fact]
        public void GetByIds_Test()
        {
            List<int> ids = new List<int>();
            ids.Add(1);
            ids.Add(2);
            ids.Add(3);
            var holidays = _mockHolidayService.Object.GetByIds(ids);
        }


        [Fact]
        public void DeleteRange_Test()
        {
            _mockHolidayService.Object.DeleteRange(_holidays.Take(3).ToList());

        }

        //GetHolidayByNameAndCompany
        [Fact]
        public void GetHolidayByNameAndCompany_Test()
        {
            var holiday = _mockHolidayService.Object.GetHolidayByNameAndCompany(1, "Duali");
        }

        //GetHolidayByCompany
        [Fact]
        public void GetHolidayByCompany_Test()
        {
            var holiday = _mockHolidayService.Object.GetHolidayByCompany(1);
        }


        //GetHolidayCount
        [Fact]
        public void GetHolidayCount_Test()
        {
            var cnt = _mockHolidayService.Object.GetHolidayCount(1);
        }

        //IsExistedName
        [Fact]
        public void IsExistedName_Test()
        {
            _mockHolidayService.Object.IsExistedName(1, "Duali");
        }
    }
}
