﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.SystemLog;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class SystemLogServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<SystemLogService> _mockSystemLogService;
        private readonly List<SystemLog> _systemLogs;
        public SystemLogServiceTest()
        {

            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _systemLogs = TestHelper.GetTestSystemLogs();
            var mockEnviroment = new Mock<IHostingEnvironment>();
            mockEnviroment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            var mockUserLogService = new Mock<ISystemLogService>();
            var mockLogger = new Mock<ILogger<SystemLogService>>();
            var mockQueueService = new Mock<IQueueService>();

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            //Mock UnitOfWork

            var appDbContext = TestHelper.GetAppDbContext();

            if (!appDbContext.SystemLog.Any())
            {
                var newSystemLog = new List<SystemLog>();
                foreach (var systemLog in _systemLogs)
                {
                    systemLog.Id = 0;
                    newSystemLog.Add(systemLog);
                }
                appDbContext.SystemLog.AddRange(newSystemLog);
                appDbContext.SaveChanges();
            }




            var mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
            _mockSystemLogService = new Mock<SystemLogService>(mockUnitOfWork.Object, mockHttpContext.Object, _configuration);



        }

        [Fact]
        public void Add_Test()
        {
            
            //_mockSystemLogService.Object.Add(13, SystemLogType.Login, ActionLogType.Login, "Hello", "Hello");
            
        }

        [Fact]
        public void AddAsync_Test()
        {
            //_mockSystemLogService.Object.AddAsync(1, SystemLogType.Login, ActionLogType.Login);
        }

        [Fact]
        public void GetPaginated_Test()
        {

            var model = new SystemLogModel
            {
                //public string OpeDateFrom { get; set; }
                //public string OpeDateTo { get; set; }
                //public string OpeTimeFrom { get; set; }
                //public string OpeTimeTo { get; set; }
                //public int? ObjectType { get; set; }
                //public int? Action { get; set; }
                //public int? Company { get; set; }
                //public List<SelectListItem> ObjectTypeItems { get; set; }
                //public List<SelectListItem> ActionItems { get; set; }
                //public List<SelectListItem> CompanyItems { get; set; }

                OpeDateFrom = "01.09.2019",
                OpeDateTo = "01.10.2019",
                OpeTimeFrom = "000000",
                OpeTimeTo = "235959",
                ObjectType = 1,
                Action = 1,
                Company = 1,
            };
            _mockSystemLogService.Object.InitData(model);
            //_mockSystemLogService.Object.GetPaginated("", 1, 1, "desc", 1, out var totalRecords, out var recordsFiltered, "", "", "9:00 AM", "10:00 AM", 1, 1, 1);

            //_mockSystemLogService.Object.GetPaginated("", 1, 1, "desc", 1, out var totalRecords , out var recordFilter, "01.09.2019", "01.10.2019", "000000", "000000", 1, 1, 1);
        }

        [Fact]
        public void Export_Test()
        {

            var model = new SystemLogModel
            {
                //public string OpeDateFrom { get; set; }
                //public string OpeDateTo { get; set; }
                //public string OpeTimeFrom { get; set; }
                //public string OpeTimeTo { get; set; }
                //public int? ObjectType { get; set; }
                //public int? Action { get; set; }
                //public int? Company { get; set; }
                //public List<SelectListItem> ObjectTypeItems { get; set; }
                //public List<SelectListItem> ActionItems { get; set; }
                //public List<SelectListItem> CompanyItems { get; set; }

                OpeDateFrom = "01.09.2019",
                OpeDateTo = "01.10.2019",
                OpeTimeFrom = "000000",
                OpeTimeTo = "235959",
                ObjectType = 1,
                Action = 1,
                Company = 1,
            };
            _mockSystemLogService.Object.InitData(model);
            _mockSystemLogService.Object.ExportPDF(1, "desc", out var totalRecords, out var recordsFiltered, "", "", "9:00 AM", "10:00 AM", 1, 1, 1);
            //_mockSystemLogService.Object.Export("Login", 1, "desc", out var totalRecords, out var recordsFiltered, "01.09.2019", "01.10.2019", "000000", "000000", 1, 1, 1);
        }

        //[Fact]
        //public void ExportPDF_Test()
        //{
        //    var model = new SystemLogModel
        //    {
        //        //public string OpeDateFrom { get; set; }
        //        //public string OpeDateTo { get; set; }
        //        //public string OpeTimeFrom { get; set; }
        //        //public string OpeTimeTo { get; set; }
        //        //public int? ObjectType { get; set; }
        //        //public int? Action { get; set; }
        //        //public int? Company { get; set; }
        //        //public List<SelectListItem> ObjectTypeItems { get; set; }
        //        //public List<SelectListItem> ActionItems { get; set; }
        //        //public List<SelectListItem> CompanyItems { get; set; }

            //    OpeDateFrom = "01.09.2019",
            //    OpeDateTo = "01.10.2019",
            //    OpeTimeFrom = "000000",
            //    OpeTimeTo = "235959",
            //    ObjectType = 1,
            //    Action = 1,
            //    Company = 1,
            //};
        //    _mockSystemLogService.Object.InitData(model);
        //    _mockSystemLogService.Object.ExportPDF(1, "desc", out var totalRecords, out var recordsFiltered, "", "", "9:00 AM", "10:00 AM", 1, 1, 1);
        //}

        [Fact]
        public void HasData_Test()
        {
            _mockSystemLogService.Object.HasData(1);
        }

        [Fact]
        public void GetActionListItems()
        {
            _mockSystemLogService.Object.GetActionListItems(1);
        }

        [Fact]
        public void InitData_Test()
        {
            var model = new SystemLogModel
            {
                //public string OpeDateFrom { get; set; }
                //public string OpeDateTo { get; set; }
                //public string OpeTimeFrom { get; set; }
                //public string OpeTimeTo { get; set; }
                //public int? ObjectType { get; set; }
                //public int? Action { get; set; }
                //public int? Company { get; set; }
                //public List<SelectListItem> ObjectTypeItems { get; set; }
                //public List<SelectListItem> ActionItems { get; set; }
                //public List<SelectListItem> CompanyItems { get; set; }
               
                OpeDateFrom = "01.09.2019",
                OpeDateTo = "01.10.2019",
                OpeTimeFrom = "000000",
                OpeTimeTo = "235959",
                ObjectType = 1,
                Action = 1,
                Company = 1,
            };
            _mockSystemLogService.Object.InitData(model);
        }

        [Fact]
        public void Save_Test()
        {
            _mockSystemLogService.Object.Save();
        }
    }
}