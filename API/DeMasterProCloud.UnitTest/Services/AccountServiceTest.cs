﻿using AutoMapper;
using AutoMapper.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Account;
using DeMasterProCloud.DataModel.Login;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class AccountServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<AccountService> _mockAccountService;
        private readonly List<Account> _accounts;
        private readonly Mock<UnitOfWork> mockUnitOfWork;
        private readonly Mock<IHttpContextAccessor> mockHttpContext;
        private readonly Mock<ICompanyService> mockCompanyService;

        public AccountServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _accounts = TestHelper.GetTestAccounts();

            var mockEnviroment = new Mock<IHostingEnvironment>();
            mockEnviroment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            mockCompanyService = new Mock<ICompanyService>();
            var mockLogger = new Mock<ILogger<AccountService>>();
            var mockMailService = new Mock<IMailService>();
            //Mock HttpContext
            mockHttpContext = new Mock<IHttpContextAccessor>();

            //Initial mapping
            AutoMapperConfig.Initialize();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            //Mock UnitOfWork

            var appDbContext = TestHelper.GetAppDbContext();
            if (!appDbContext.Account.Any())
            {
                var newAccount = new List<Account>();
                foreach (var account in _accounts)
                {
                    account.Id = 0;
                    newAccount.Add(account);
                }
                appDbContext.Account.AddRange(newAccount);
                appDbContext.SaveChanges();
            }


            mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
            _mockAccountService = new Mock<AccountService>(mockUnitOfWork.Object, mockHttpContext.Object, mockCompanyService.Object, mockMailService.Object, mockLogger.Object);

        }

        [Fact]
        public void Add_Account_Success()
        {
            var accountModel = new AccountModel()
            {
                Username = "phuongngoc",
                Password = "123456789",
                Status = (short)Status.Valid,
                //CompanyCode = "p000001",
                Role = 1,
            };
            _mockAccountService.Object.Add(accountModel);
            var newAccount = mockUnitOfWork.Object.AppDbContext.Account.ToList().Last();
            Assert.True(newAccount.Username == accountModel.Username);
            Assert.True(SecurePasswordHasher.Verify(accountModel.Password, newAccount.Password));
            //Assert.True(newAccount.Status == accountModel.Status);
            Assert.True(newAccount.RootFlag == accountModel.RootFlag);
            Assert.True(newAccount.CompanyId == 1);
            Assert.True(newAccount.RootFlag == false);
            Assert.True(newAccount.Type == accountModel.Role);
        }

        [Fact]
        public void Add_Account_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccountService.Object.Add(null));
        }

        [Fact]
        public void Update_Account_Success()
        {
            var accountModel = Mapper.Map<AccountModel>(_accounts.First());
            accountModel.Password = _accounts.First().Password;
            accountModel.Username = "phuongcn";
            accountModel.Status = (short)Status.Invalid;

            _mockAccountService.Object.Update(accountModel);

            var accountUpdated = mockUnitOfWork.Object.AppDbContext.Account.Find(_accounts.First().Id);
            Assert.True(SecurePasswordHasher.Verify(accountModel.Password, accountUpdated.Password));
            //Assert.True(accountUpdated.Status == accountModel.Status);
        }

        [Fact]
        public void Update_Account_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccountService.Object.Update(null));
        }

        [Fact]
        public void Delete_Account_Success()
        {
            _mockAccountService.Object.Delete(_accounts.First());
            var accountDeleted = mockUnitOfWork.Object.AppDbContext.Account.Find(_accounts.First().Id);
            Assert.True(accountDeleted.IsDeleted);
        }

        [Fact]
        public void Delete_Account_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccountService.Object.Delete(null));
        }

        [Fact]
        public void DeleteRange_Account_Suscess()
        {
            _mockAccountService.Object.DeleteRange(_accounts.Take(2).ToList());
            var listAccountDeleted = mockUnitOfWork.Object.AppDbContext.Account.Take(2).ToList();
            foreach (var item in listAccountDeleted)
            {
                Assert.True(item.IsDeleted);
            }
        }

        [Fact]
        public void DeleteRange_Delete_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccountService.Object.DeleteRange(null));
        }

        [Fact]
        public void Get_ById_Return_Account()
        {
            var result = _mockAccountService.Object.GetById(_accounts.First().Id);
            Assert.True(result != null);
        }

        [Fact]
        public void Get_ById_Return_NonExit()
        {
            var result = _mockAccountService.Object.GetById(6);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_AccountsById_Return_ListAccount_Success()
        {
            var idArr = _accounts.Select(x => x.Id).ToList();
            var result = _mockAccountService.Object.GetAccountsByIds(idArr, 1);
            var expected = _accounts.Select(x => x.Id).ToList();
            var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void Get_AccountsById_Return_ListAccount_Fail()
        {
            var idArr = _accounts.Select(x => x.Id).ToList();
            var result = _mockAccountService.Object.GetAccountsByIds(idArr, 10);
            Assert.True(result.Count == 0);
        }

        //[Theory]
        //[MemberData(nameof(DataTest_Get_AccountsById_Return_ListAccount_Success))]
        //public void Get_Paginated_Return_ListAccount_Success(string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection, List<string> expected)
        //{
        //    var result = _mockAccountService.Object.GetPaginated(filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var resultNotExpected = result.Select(x => x.Username).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Username)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_Paginated_Return_ListAccount_Fail()
        {
            var result = _mockAccountService.Object.GetPaginated("phuongdragon@gmail.com", 1, 10, 0, "desc", out _, out _);
            Assert.True(!result.Any());
        }

        [Fact]
        public void Check_IsUserExist_Return_True()
        {
            var result = _mockAccountService.Object.IsExist(_accounts.First().Id, "b@gmail.com", 1);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsUserExist_Return_False()
        {
            var result = _mockAccountService.Object.IsExist(_accounts.First().Id, "a@gmail.com", null);
            Assert.False(result);
        }

        [Fact]
        public void Get_AccountByCompanyCodeAndUsername_Return_Account()
        {
            var result = _mockAccountService.Object.GetAccountByCompanyCodeAndUsername("p000001", "a@gmail.com");
            Assert.True(result != null && result.Username.Equals("a@gmail.com"));
        }

        [Fact]
        public void Get_AccountByCompanyCodeAndUsername_Return_NonExit()
        {
            var result = _mockAccountService.Object.GetAccountByCompanyCodeAndUsername("p000002", "a@gmail.com");
            Assert.True(result == null);
        }

        [Fact]
        public void Get_AuthenticatedAccount_Return_Account()
        {
            var loginModel = new LoginModel()
            {
                Username = "a@gmail.com",
                //CompanyCode = "p000001",
                Password = "123456"
            };
            var result = _mockAccountService.Object.GetAuthenticatedAccount(loginModel);
            Assert.True(result != null && result.Username.Equals("a@gmail.com"));
        }

        [Theory]
        [MemberData(nameof(DataTest_Get_AuthenticatedAccount_Return_NonExist))]
        public void Get_AuthenticatedAccount_Return_NonExist(LoginModel loginModel)
        {
            var result = _mockAccountService.Object.GetAuthenticatedAccount(loginModel);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_AccountRoles_Return_ListSelectItem_Return_RootAdmin()
        {
            var result = _mockAccountService.Object.GetAccountRoles(1).ToList();
            foreach (var item in result)
            {
                Assert.True(item.Value == "1" || item.Value == "2" || item.Value == "3");
            }
        }

        [Fact]
        public void Get_AccountRoles_Return_ListSelectItem_Return_Not_RootAdmin()
        {
            short selectedValue = 1;
            mockHttpContext.Setup(x => x.HttpContext).Returns(GetHttpContext());
            var result = _mockAccountService.Object.GetAccountRoles(selectedValue).ToList();
            foreach (var item in result)
            {
                Assert.True(item.Value != "1");
            }
        }

        [Fact]
        public void Get_AccountRoles_Return_GetValidAccount_Success()
        {
            mockCompanyService.Setup(x => x.IsValidCompany(_accounts.First().Company)).Returns(true);
            var result = _mockAccountService.Object.GetValidAccount("a@gmail.com", 1);
            Assert.True(result.Id == _accounts.First().Id);
        }

        [Fact]
        public void Get_AccountRoles_Return_GetValidAccount_Fail()
        {
            var result = _mockAccountService.Object.GetValidAccount("a@gmail.com", 2);
            Assert.True(result == null);
        }

        [Fact]
        public void Check_IsAllowDelete_Return_True()
        {
            var accountLogin = new Account()
            {
                Id = 1,
                Username = "a@gmail.com",
                Password = "10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                CompanyId = 1,
                RootFlag = true,
                //Status = (short)Status.Valid,
            };
            var result3 = _mockAccountService.Object.IsAllowDelete(TestHelper.GetTestAccounts().Last(), accountLogin);
            Assert.True(result3);
        }

        [Theory]
        [MemberData(nameof(DataTest_Check_IsAllowDelete_Return_False))]
        public void Check_IsAllowDelete_Return_False(Account accountLogin)
        {
            var result = _mockAccountService.Object.IsAllowDelete(TestHelper.GetTestAccounts().First(), accountLogin);
            Assert.False(result);
        }

        [Fact]
        public void Get_AccountLogin_Return_Fail()
        {
            var result = _mockAccountService.Object.GetAccountLogin(TestHelper.GetTestHttpContext().User);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_AccountLogin_GetRootAccountByCompany_Return_Account()
        {
            var result = _mockAccountService.Object.GetRootAccountByCompany(1);
            Assert.True(result.Id == _accounts.First().Id);
        }

        [Fact]
        public void Get_AccountLogin_GetRootAccountByCompany_Return_Non_Exist()
        {
            var result = _mockAccountService.Object.GetRootAccountByCompany(100);
            Assert.True(result == null);
        }

        [Fact]
        public void ChangePassword_Return_Success()
        {
            var account = _accounts.First();
            account.Password = _accounts.Last().Password;
            _mockAccountService.Object.ChangePassword(account);
            Assert.True(account.Password == _accounts.Last().Password);
        }

        [Fact]
        public void ChangePassword_Return_Return_Null_Exception()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccountService.Object.ChangePassword(null));
        }
        [Fact]
        public void Check_SendResetAccountMail_Success()
        {
            _mockAccountService.Object.SendResetAccountMail(new Account()
            {
                Username = "duali@duali.com"
            }, 1);
        }

        [Fact]
        public void Get_AccountByCurrentCompany_Return_Account()
        {
            var result = _mockAccountService.Object.GetAccountByCurrentCompany(_accounts.First().Id);
            Assert.True(result != null);
        }

        [Fact]
        public void Get_AccountByCurrentCompany_Return_NonExist()
        {
            var result = _mockAccountService.Object.GetAccountByCurrentCompany(0);
            Assert.True(result == null);
        }

        [Fact]
        public void Check_InitData_Return_AccountDataModel()
        {
            AccountDataModel accountDataModel = new AccountDataModel()
            {
                Status = (short)Status.Valid,
                Role = (short)Role.Admin
            };
            var result = _mockAccountService.Object.InitData(accountDataModel);
            Assert.True(result.RoleList.Any());
            Assert.True(result.StatusList.Any());
        }

        [Fact]
        public void Check_SendAccountMail_Success()
        {
            //_mockAccountService.Object.SendAccountMail("phuong.cn@3si.vn", "123456", "");
        }

        [Fact]
        public void Check_HasChangeAccount_Return_True()
        {
            var accountModel = new AccountModel()
            {
                Id = 1,
                Username = "phuongcn",
                Password = "12345678",
                ConfirmPassword = "12345678",
                Status = (short)Status.Valid,
                RootFlag = true,
                //CompanyCode = "p000001",
                Role = 1
            };
            List<string> changes = new List<string>();
            var result = _mockAccountService.Object.HasChange(TestHelper.GetTestAccounts().First(), accountModel, ref changes);
            Assert.True(result);
        }

        [Fact]
        public void Check_HasChangeAccount_Return_False()
        {
            var accountModel = new AccountModel()
            {
                Id = 1,
                Username = "phuongcn",
                Password = "123456",
                Status = (short)Status.Valid,
                RootFlag = true,
                //CompanyCode = "p000001",
                Role = 1
            };
            List<string> changes = new List<string>();
            var result = _mockAccountService.Object.HasChange(TestHelper.GetTestAccounts().First(), accountModel, ref changes);
            Assert.False(result);
        }

        [Fact]
        public void Check_HasChangeAccount_When_ConfirmPasswordIsEmpty_Return_False()
        {
            var accountModel = new AccountModel()
            {
                Id = 0,
                Username = "phuongcn",
                Password = "123456",
                Status = (short)Status.Valid,
                RootFlag = true,
                //CompanyCode = "p000001",
                Role = 1
            };
            List<string> changes = new List<string>();
            var result = _mockAccountService.Object.HasChange(TestHelper.GetTestAccounts().First(), accountModel, ref changes);
            Assert.False(result);
        }

        [Fact]
        public void Check_HasChangeAccount_When_PasswordIsEmpty_Return_False()
        {
            var accountModel = new AccountModel()
            {
                Id = 1,
                Username = "phuongcn",
                Password = string.Empty,
                ConfirmPassword = "123456",
                Status = (short)Status.Valid,
                RootFlag = true,
                //CompanyCode = "p000001",
                Role = 1
            };
            List<string> changes = new List<string>();
            var result = _mockAccountService.Object.HasChange(TestHelper.GetTestAccounts().First(), accountModel, ref changes);
            Assert.False(result);
        }
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void Check_IsValidAccountType_Return_True(short accountType)
        {
            var result = _mockAccountService.Object.IsValidAccountType(accountType);
            Assert.True(result);           
        }

        [Fact]
        public void Check_IsValidAccountType_Return_False()
        {
            short accountType = 0;
            var result = _mockAccountService.Object.IsValidAccountType(accountType);
            Assert.False(result);
        }
        public static IEnumerable<object[]> DataTest_Get_AccountsById_Return_ListAccount_Success()
        {
            return new List<object[]>
            {
            new object[] { "", 1, 10, 0, "desc", new List<string> { "a@gmail.com", "b@gmail.com", "c@gmail.com", "d@gmail.com" }  },
            new object[] { "a@gmail.com", 1, 10, 10, "desc", new List<string> { "a@gmail.com" } },

            };
        }

        
        public static IEnumerable<object[]> DataTest_Check_IsAllowDelete_Return_False()
        {
            return new List<object[]>
            {
            new object[] {
                new Account() {
                Id = 1,
                Username = "a@gmail.com",
                Password = "10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                CompanyId = 1,
                RootFlag = true,
                /*Status = (short)Status.Valid*/ }
            },
            new object[] {
                new Account(){
                Id = 2,
                Username = "b@gmail.com",
                Password = "10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                CompanyId = 1,
                RootFlag = true,
                //Status = (short)Status.Valid,
             }
            },
            };
        }

        public static IEnumerable<object[]> DataTest_Get_AuthenticatedAccount_Return_NonExist()
        {
            var loginModel = new LoginModel()
            {
                Username = "a@gmail.com",
                //CompanyCode = "p000001",
                Password = "12345678"
            };

            return new List<object[]>
            {
            new object[] {
               new LoginModel() {
                Username = "a@gmail.com",
                //CompanyCode = "p000001",
                Password = "12345678"
            }
        },
            new object[] {
                  new LoginModel(){
                Username = "a@gmail.com",
                //CompanyCode = "p000001",
                Password = "12345678"
                 }
            },
            };
        }

        private DefaultHttpContext GetHttpContext()
        {
            var claims = new[]
           {
                new Claim(ClaimTypes.Name, "phong.ld@3si.vn"),
                new Claim(Constants.ClaimName.AccountId, "1"),
                new Claim(Constants.ClaimName.CompanyId, "1"),
                new Claim(Constants.ClaimName.CompanyCode, "d000001"),
                new Claim(Constants.ClaimName.CompanyName, "duali"),
                new Claim(Constants.ClaimName.AccountType, "2")
            };
            var appIdentity = new ClaimsIdentity(claims);

            return new DefaultHttpContext { User = new ClaimsPrincipal(appIdentity) }; // or mock a `HttpContext`
        }
    }
}
