using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using EfCore.InMemoryHelpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class AccessGroupServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly Mock<AccessGroupService> _mockAccessGroupService;
        private readonly Mock<IAccessGroupDeviceService> _mockAccessGroupServiceDevice;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly List<AccessGroup> _accessGroups;
        private readonly List<IcuDevice> _devices;
        private readonly List<AccessGroupDevice> _accessGroupDevices;
        private readonly Mock<IQueueService> _mockQueueService; 
        private readonly Mock<ILogger<AccessGroupService>> mockLogger;
        private readonly AppDbContext _appDbContext;

        public AccessGroupServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;

            _accessGroups = TestHelper.GetTestAccessGroups();
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            //Mock HttpContext
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock UnitOfWork
            var builder = TestHelper.GetDbContextOptionsBuilderOptions();
            _appDbContext = InMemoryContextBuilder.Build<AppDbContext>(builder);

            if (!_appDbContext.AccessGroup.Any())
            {
                foreach (var accessGroup in _accessGroups)
                {
                    accessGroup.Id = 0;
                    _appDbContext.AccessGroup.Add(accessGroup);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(accessGroup).State = EntityState.Detached;
                }
            }

            if (!_appDbContext.User.Any())
            {
                var users = TestHelper.GetTestUsers();
                foreach (var user in users)
                {
                    user.Id = 0;
                    _appDbContext.User.Add(user);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(user).State = EntityState.Detached;
                }
            }

            if (!_appDbContext.IcuDevice.Any())
            {
                _devices = TestHelper.GetTestIcuDevice();
                foreach (var device in _devices)
                {
                    device.Id = 0;
                    _appDbContext.IcuDevice.Add(device);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(device).State = EntityState.Detached;
                }
            }

            if (!_appDbContext.AccessGroupDevice.Any())
            {
                _accessGroupDevices = TestHelper.GetTestAccessGroupDevices().Take(2).ToList();
                foreach (var accessGroupDevice in _accessGroupDevices)
                {
                    _appDbContext.AccessGroupDevice.Add(accessGroupDevice);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(accessGroupDevice).State = EntityState.Detached;
                }
            }

            if (!_appDbContext.Timezone.Any())
            {
                var timezones = TestHelper.GetTestTimezone().ToList();
                foreach (var timezone in timezones)
                {
                    _appDbContext.Timezone.Add(timezone);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(timezone).State = EntityState.Detached;
                }
            }

            _mockUnitOfWork = new Mock<UnitOfWork>(_appDbContext, _mockHttpContext.Object);
            _mockAccessGroupServiceDevice = new Mock<IAccessGroupDeviceService>();
            _mockQueueService = new Mock<IQueueService>();
            mockLogger = new Mock<ILogger<AccessGroupService>>();
            var mockSettingService = new Mock<ISettingService>();
            _mockAccessGroupService = new Mock<AccessGroupService>(_mockUnitOfWork.Object,
                _mockQueueService.Object, _configuration, mockLogger.Object, _mockHttpContext.Object);
        }

        [Fact]
        public void Get_PaginatedAccessGroup_Return_ListAccessGroup_Fail()
        {
            var result = _mockAccessGroupService.Object.GetPaginated("1000", 1, 10, 1, "desc", out _, out _);
            Assert.True(!result.Any());
        }

        //[Theory]
        //[InlineData("", 1, 10, 1, "desc")]
        //[InlineData("", 1, 10, 11, "desc")]
        //public void Get_PaginatedAccessGroup_Return_ListAccessGroup_Success(string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockAccessGroupService.Object.GetPaginated(filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = _accessGroups.Where(x => !x.IsDeleted).Select(x => x.Id).ToList();
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        //[Theory]
        //[InlineData(1, true, "", 1, 10, 1, "desc")]
        //[InlineData(1, true, "0D9038", 1, 10, 1, "desc")]
        //public void Get_PaginatedAssignForDoors_Return_DoorList_Success(int accessgroupId, bool isAssign, string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockAccessGroupService.Object.GetPaginatedForDoors(accessgroupId, isAssign, filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = new List<string> {"0D9038"};
        //    var resultNotExpected = result.Select(x => x.DeviceAddress).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.DeviceAddress)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        //[Theory]
        //[InlineData(1, "", 1, 10, 1, "desc")]
        //[InlineData(1, "196E69", 1, 10, 1, "desc")]
        //public void Get_PaginatedUnAssignForDoors_Return_DoorList_Success(int accessgroupId, string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockAccessGroupService.Object.GetPaginatedForDoors(accessgroupId, filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = new List<string> { "196E69" };
        //    var resultNotExpected = result.Select(x => x.DeviceAddress).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.DeviceAddress)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        //[Theory]
        //[InlineData(1, "", 1, 10, 1, "desc")]
        //[InlineData(1, "", 1, 10, 2, "desc")]
        //[InlineData(1, "A", 1, 10, 1, "desc")]
        //public void Get_PaginatedAssignForUsers_Return_UserList_Success(int accessgroupId, string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockAccessGroupService.Object.GetPaginatedForUsers(accessgroupId, filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = new List<string> { "A" };
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.CardId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}
        
        //[Theory]
        //[InlineData(1, "", 1, 10, 1, "desc")]
        ////[InlineData(1, "B", 1, 10, 1, "desc")]
        //public void Get_PaginatedUnAssignForUsers_Return_UserList_Success(int accessgroupId, string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockAccessGroupService.Object.GetPaginatedForUnAssignUsers(accessgroupId, filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = new List<string> { "B" };
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    Assert.True(resultNotExpected.Any());
        //}

        //[Theory]
        //[InlineData(1, "", 1, 10, 2, "desc")]
        //public void Get_PaginatedUnAssignForUsers2_Return_UserList_Success(int accessgroupId, string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection)
        //{
        //    var result = _mockAccessGroupService.Object.GetPaginatedForUsers(accessgroupId, filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _);
        //    var expected = new List<string> { "A" };
        //    var resultNotExpected = result.Select(x => x.CardId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.CardId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_ByIds_Return_ListAccessGroup_Success()
        {
            var result = _mockAccessGroupService.Object.GetByIds(_accessGroups.Select(x => x.Id).ToList());
            var expected = _accessGroups.Where(x => !x.IsDeleted).Select(x => x.Id).ToList();
            var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void Get_ByIds_Return_ListAccessGroup_Fail()
        {
            var result = _mockAccessGroupService.Object.GetByIds(new List<int> { 5, 6 });
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void Get_ById_Return_AccessGroup()
        {
            var result = _mockAccessGroupService.Object.GetById(_accessGroups.First().Id);
            Assert.True(result != null);
        }

        [Fact]
        public void Get_ById_Return_Non_Exist()
        {
            var result = _mockAccessGroupService.Object.GetById(_accessGroups.Last().Id + 1);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_ListAccessGroup()
        {
            var result = _mockAccessGroupService.Object.GetListAccessGroups();
            Assert.True(result.Count == _accessGroups.Where(x => !x.IsDeleted).ToList().Count());
        }


        [Fact]
        public void Add_AccessGroup_Success()
        {
            var accessGroupModel = new AccessGroupModel
            {
                Name = "Access group name 1",
                IsDefault = false
            };

            _mockAccessGroupService.Object.Add(accessGroupModel);
            var newDevice = _mockUnitOfWork.Object.AppDbContext.AccessGroup.ToList().Last();
            Assert.True(newDevice.Name == accessGroupModel.Name);
            Assert.True(newDevice.IsDefault == accessGroupModel.IsDefault);
            Assert.True(newDevice.CompanyId == 1);
        }

        [Fact]
        public void Add_AccessGroup_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccessGroupService.Object.Add(null));
        }

        [Fact]
        public void Update_AccessGroup_Susscess()
        {
            var accessGroupModel = new AccessGroupModel()
            {
                Id = _accessGroups.First().Id,
                Name = "Access group updated",
                IsDefault = true
            };
            _mockAccessGroupService.Object.Update(accessGroupModel);
            var deviceUpdated = _mockUnitOfWork.Object.AppDbContext.AccessGroup.Find(_accessGroups.First().Id);
            Assert.True(deviceUpdated.Name == accessGroupModel.Name);
            Assert.True(deviceUpdated.IsDefault == accessGroupModel.IsDefault);
        }

        [Fact]
        public void Update_AccessGroup_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccessGroupService.Object.Update(null));
        }

        //[Fact]
        //public void Delete_AccessGroup_Succcess()
        //{
        //    var accessGroup = _accessGroups.FirstOrDefault(x =>
        //        !x.IsDeleted && x.Type == (short)AccessGroupType.NormalAccess &&
        //        x.CompanyId == Constants.DefaultCompanyId);
        //    _mockAccessGroupService.Object.Delete(accessGroup);
        //    Assert.True(accessGroup != null && accessGroup.IsDeleted);
        //}

        [Fact]
        public void Delete_AccessGroup_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccessGroupService.Object.Delete(null));
        }

        [Fact]
        public void DeleteRange_AccessGroup_Succcess()
        {
            var accessGroups = _accessGroups.Where(x =>
                !x.IsDeleted && x.Type == (short) AccessGroupType.NormalAccess &&
                x.CompanyId == Constants.DefaultCompanyId).ToList();
            _mockAccessGroupService.Object.DeleteRange(accessGroups);
            foreach (var item in accessGroups)
            {
                Assert.True(item.IsDeleted);
            }
        }

        [Fact]
        public void HasExistsName_ReturnTrue()
        {
            var result = _mockAccessGroupService.Object.HasExistName(5, _accessGroups.First().Name);
            Assert.True(result);
        }

        [Fact]
        public void HasExistsName_ReturnFalse()
        {
            var result = _mockAccessGroupService.Object.HasExistName(_accessGroups.First().Id, _accessGroups.First().Name);
            Assert.True(result == false);
        }

        [Fact]
        public void DeleteRange_AccessGroup_ReturnNullException()
        {
            Assert.Throws<NullReferenceException>(() => _mockAccessGroupService.Object.DeleteRange(null));
        }

        [Fact]
        public void SetAllAccessGroupToNotDefault_ReturnSuccess()
        {
            _mockAccessGroupService.Object.SetAllAccessGroupToNotDefault(Constants.DefaultCompanyId, _accessGroups.First().Id);
            //Check result
            var listToSetNotDefault = _accessGroups.Except(new List<AccessGroup>{_accessGroups.First()});
            foreach (var item in listToSetNotDefault)
            {
                Assert.True(item.IsDefault == false);
            }
        }

        [Fact]
        public void GetNameAccessGroupCouldNotDelete_ReturnName()
        {
            var listIds = new List<int> {1};
            var result = _mockAccessGroupService.Object.GetNameAccessGroupCouldNotUpdateOrDelete(listIds);
            Assert.True(result == _accessGroups.First().Name);
        }

        [Fact]
        public void GetNameAccessGroupCouldNotDelete_ReturnEmpty()
        {
            var listIds = new List<int> { 4 };
            var result = _mockAccessGroupService.Object.GetNameAccessGroupCouldNotUpdateOrDelete(listIds);
            Assert.True(result == "");
        }

        [Fact]
        public void AssignUsers_UserIdList_ReturnUserLogCount()
        {
            var testAccessGroupId = 1;
            var testUserIds = new List<int> { 1, 2 };

            _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, testUserIds);
            //Check result
            //var userAddCount =
            //    _mockUnitOfWork.Object.AppDbContext.UserLog.Count(c => c.Action == 1);
            //Assert.True(userAddCount == 2);
            //var userDeleteCount =
            //    _mockUnitOfWork.Object.AppDbContext.UserLog.Count(c => c.Action == 3);
            //Assert.True(userDeleteCount == 4);
        }

        [Fact]
        public void AssignUsers_UserIdList_NotDefineSetting()
        {
            var testAccessGroupId = 1;
            var testUserIds = new List<int> { 1, 2 };

            _configuration[Constants.Settings.MaxIcuUser] = null;
            _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, testUserIds);
            ////Check result
            //var userAddCount =
            //    _mockUnitOfWork.Object.AppDbContext.UserLog.Count(c => c.Action == 1);
            //Assert.True(userAddCount == 2);
            //var userDeleteCount =
            //    _mockUnitOfWork.Object.AppDbContext.UserLog.Count(c => c.Action == 3);
            //Assert.True(userDeleteCount == 4);
        }

        [Fact]
        public void AssignUsers_UserIdList_ReturnMessageOverMaxUser()
        {
            var testAccessGroupId = 1;
            var testUserIds = new List<int> { 1, 2 };
            _configuration[Constants.Settings.MaxIcuUser] = "1";
            var result = _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, testUserIds);
            Assert.True(!string.IsNullOrEmpty(result));
        }

        [Fact]
        public void AssignUsers_UserIdList_ThrowException()
        {
            var testAccessGroupId = 1;
            //Check result
            Assert.Throws<ArgumentNullException>(() => _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, null));
        }


        [Fact]
        public void AssignDoors_AccessGroupAssignDoor_ReturnMessageOverMaxUser()
        {
            var testAccessGroupId = 1;
            var listAccessGroupAssignDoorDetail = new List<AccessGroupAssignDoorDetail>
            {
                new AccessGroupAssignDoorDetail
                {
                    DoorId = 3,
                    TzId = 1
                }
            };
            _configuration[Constants.Settings.MaxIcuUser] = "1";
            var testAccessGroupAssignDoor = new AccessGroupAssignDoor { Doors = listAccessGroupAssignDoorDetail };
            var result = _mockAccessGroupService.Object.AssignDoors(testAccessGroupId, testAccessGroupAssignDoor);
            Assert.True(!string.IsNullOrEmpty(result));
        }

        [Fact]
        public void AssignDoors_AccessGroupAssignDoor_ThrowException()
        {
            var testAccessGroupId = 1;
            //Check result
            Assert.Throws<NullReferenceException>(() => _mockAccessGroupService.Object.AssignDoors(testAccessGroupId, null));
        }

        [Fact]
        public void MakeUserProtocolData_UserLogList_ReturnUserProtocolData()
        {
            var testAccessGroupId = 1;
            var testUserIds = new List<int> { 1, 2 };
            _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, testUserIds);
            //Check result
            //var userLogs = _mockUnitOfWork.Object.AppDbContext.UserLog.ToList();
            //var result = _mockAccessGroupService.Object.MakeUserProtocolData(userLogs, Constants.Protocol.DeleteUser);
            //Assert.True(result != null && result.Data.Total == 6);
        }

        [Fact]
        public void UnAssignUsersFromList_UserLogList_ReturnUserProtocolData()
        {
            var testUsers = _appDbContext.User.Include(c => c.AccessGroup).ThenInclude(c => c.AccessGroupDevice)
                .ThenInclude(c => c.Icu).Take(2).ToList();
            var result = _mockAccessGroupService.Object.UnAssignUsersFromUserList(testUsers);
            //Check result
            Assert.True(result != null && result.Count == 2);
        }

        [Fact]
        public void UnAssignUsersFromAccessGroup_UserLogList_ReturnUserProtocolData()
        {
            var testAccessGroupId = 1;
            var testUsers = _appDbContext.User.Include(c => c.AccessGroup).ThenInclude(c => c.AccessGroupDevice)
                .ThenInclude(c => c.Icu).Take(2).ToList();
            var result = _mockAccessGroupService.Object.UnAssignUsersFromAccessGroup(testAccessGroupId, testUsers);
            //Check result
            Assert.True(result != null && result.Count == 2);
        }

        //[Fact]
        //public void UnAssignUsers_UserIdList_Success()
        //{
        //    var testAccessGroupId = 1;
        //    var noAccessGroup = _mockUnitOfWork.Object.AccessGroupRepository.GetNoAccessGroup(Constants.DefaultCompanyId);
        //    var testUserIds = _appDbContext.User.Where(c => c.AccessGroupId == testAccessGroupId).Select(c => c.Id)
        //        .ToList();
        //    _mockAccessGroupService.Object.UnAssignUsers(testAccessGroupId, testUserIds);
        //    //Check result
        //    var result = _appDbContext.User.Count(c => c.AccessGroupId == noAccessGroup.Id);
        //    Assert.True(result == 2);
        //}

        [Fact]
        public void UnAssignUsers_UserIdList_ThrowException()
        {
            var testAccessGroupId = 1;
            Assert.Throws<ArgumentNullException>(() => _mockAccessGroupService.Object.UnAssignUsers(testAccessGroupId, null));
        }

        [Fact]
        public void UnAssignDoors_DoorIdList_Success()
        {
            var testAccessGroupId = 1;
            var testDoorIds = _mockUnitOfWork.Object.AccessGroupDeviceRepository
                .GetByAccessGroupId(Constants.DefaultCompanyId, testAccessGroupId)
                .Select(c => c.IcuId).ToList();
            _mockAccessGroupService.Object.UnAssignDoors(testAccessGroupId, testDoorIds);
            //Check result
            var result = _mockUnitOfWork.Object.AccessGroupDeviceRepository
                .GetByAccessGroupId(Constants.DefaultCompanyId, testAccessGroupId);
            Assert.False(result.Any());
        }

        [Fact]
        public void ChangeTimezone_AccessGroupAssignDoor_Success()
        {
            var testAccessGroupId = 1;
            var accessGroupAssignDoorDetail = new AccessGroupAssignDoorDetail
            {
                DoorId = 1,
                TzId = 2
            };
            var accessGroupAssignDoor = new AccessGroupAssignDoor
            {
                Doors = new List<AccessGroupAssignDoorDetail> { accessGroupAssignDoorDetail }
            };
            _mockAccessGroupService.Object.ChangeTimezone(testAccessGroupId, accessGroupAssignDoor);
            //Check result
            var result = _appDbContext.AccessGroupDevice.First(c =>
                c.AccessGroupId == testAccessGroupId && c.IcuId == accessGroupAssignDoorDetail.DoorId);
            Assert.True(result != null && result.TzId == 2);
        }

        [Fact]
        public void ChangeTimezone_AccessGroupAssignDoor_ThrowException()
        {
            var testAccessGroupId = 1;
            Assert.Throws<NullReferenceException>(() => _mockAccessGroupService.Object.ChangeTimezone(testAccessGroupId, null));
        }

        [Fact]
        public void UnAssignDoors_DoorIdList_ThrowException()
        {
            var testAccessGroupId = 1;
            Assert.Throws<ArgumentNullException>(() => _mockAccessGroupService.Object.UnAssignUsers(testAccessGroupId, null));
        }

        [Fact]
        public void SendUserDataToQueue_UserLogProtocolData_Return()
        {
            _mockAccessGroupService.Object.SendUserDataToQueue(null,0,0,new DateTime(), new DateTime(), new DateTime(), new DateTime());
        }

        [Fact]
        public void SendUserDataToQueue_UserLogProtocolData_SendToQueue()
        {
            var testAccessGroupId = 1;
            var testUserIds = new List<int> { 1, 2 };
            _mockAccessGroupService.Object.AssignUsers(testAccessGroupId, testUserIds);
            //Check result
            //var userLogs = _mockUnitOfWork.Object.AppDbContext.UserLog.ToList();
            //var userLogProtocol = new UserLogProtocolData
            //{
            //    IcuAddress = "ICU001",
            //    UserLogs = userLogs,
            //    ProtocolType = Constants.Protocol.AddUser
            //};
            //_mockAccessGroupService.Object.SendUserDataToQueue(new List<UserLogProtocolData> { userLogProtocol });
        }

        [Fact]
        public void HasData_AccessGroupId_ReturnTrue()
        {
            var testAccessGroupId = 1;
            var result = _mockAccessGroupService.Object.HasData(testAccessGroupId);
            Assert.True(result);
        }

        [Fact]
        public void HasData_AccessGroupId_ReturnFalse()
        {
            var testAccessGroupId = 0;
            var result = _mockAccessGroupService.Object.HasData(testAccessGroupId);
            Assert.True(result == false);
        }
    }
}
