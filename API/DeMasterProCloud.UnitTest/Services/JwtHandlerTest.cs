﻿using System.Security.Claims;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.Extensions.Options;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class JwtHandlerTest
    {
        private readonly JwtHandler _mockJwtService;
        public JwtHandlerTest()
        {
            var mockOptions = Options.Create(new JwtOptionsModel
            {
                Issuer = "duali",
                SecretKey = "F7peYX7825YkwztCxgjzZGF4yExvu4TK4mN8DLUtsVHMpnGa3V5jabYjFhGf",
                ExpiryMinutes = 14 * 24 * 60 //14 days
            });
            _mockJwtService = new JwtHandler(mockOptions);
        }

        [Fact]
        public void BuildToken_Test()
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, "phong.ld@3si.vn"),
                new Claim(Constants.ClaimName.AccountId, "1"),
                new Claim(Constants.ClaimName.CompanyId, "1"),
                new Claim(Constants.ClaimName.CompanyCode, "d000001"),
                new Claim(Constants.ClaimName.CompanyName, "duali"),
                new Claim(Constants.ClaimName.AccountType, "1")
            };
            var token = _mockJwtService.BuilToken(claims);
            Assert.NotEmpty(token);
        }
    }
}
