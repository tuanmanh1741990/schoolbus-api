﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using MoreLinq.Extensions;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services.RabbitMq
{
    public class DeviceConnectionStatusConsumerTest
    {
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<DeviceConnectionStatusConsumer> _connectionStatusConsumer;
        private readonly List<IcuDevice> _icuDevices;
        private readonly List<Setting> settings;

        public DeviceConnectionStatusConsumerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);

            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();
            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            _icuDevices = TestHelper.GetTestIcuDevice();
            settings = TestHelper.GetTestSettings();
            //Initial
            AutoMapperConfig.Initialize();
            var appDbContext = TestHelper.GetAppDbContext();

            if (!appDbContext.IcuDevice.Any())
            {
                var listDevice = new List<IcuDevice>();
                foreach (var icuDevice in _icuDevices)
                {
                    icuDevice.Id = 0;
                    listDevice.Add(icuDevice);
                    appDbContext.IcuDevice.Add(icuDevice);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(icuDevice).State = EntityState.Detached;
                }
                appDbContext.IcuDevice.AddRange(listDevice);
                appDbContext.SaveChanges();
            }

            _mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, mockHttpContext.Object);
            var mockMailLogger = new Mock<ILogger<MailService>>();
            var mockSettingService = new Mock<SettingService>(_mockUnitOfWork.Object, _configuration);
            var mockMailService = new Mock<MailService>(_configuration, mockMailLogger.Object);
            _connectionStatusConsumer = new Mock<DeviceConnectionStatusConsumer>(_configuration, mockSettingService.Object, mockMailService.Object);
        }

        //[Fact]
        //public void ParseData_LengthLessThan13_ReturnNull()
        //{
        //    var result = _connectionStatusConsumer.Object.ParseData(GetConnectStatusBytes().Slice(0, 12).ToArray());
        //    Assert.Null(result);
        //}

        //[Fact]
        //public void ParseData_ReturnEventCount()
        //{
        //    var result = _connectionStatusConsumer.Object.ParseData(GetConnectStatusBytes());
        //    Assert.True(result.DeviceAddress == "0D9038");
        //    Assert.True(result.Status == "online");
        //}

        //[Fact]
        //public void DoWork_ByteZero_ReturnFalse()
        //{
        //    var result = _connectionStatusConsumer.Object.DoWork(new byte[] { 0 });
        //    Assert.True(result == false);
        //}

        //[Fact]
        //public void DoWork_DeviceNotFound_ReturnNull()
        //{
        //    var testData = GetConnectStatusBytes();
        //    testData[1] = 99;
        //    var result = _connectionStatusConsumer.Object.DoWork(testData);
        //    Assert.True(result == false);
        //}

        //[Fact]
        //public void DoWork_Return_True()
        //{
        //    var testData = GetConnectStatusBytes();
        //    var result = _connectionStatusConsumer.Object.DoWork(testData);
        //    Assert.True(result);
        //}

        private byte[] GetConnectStatusBytes()
        {
            return new byte[]
            {
                48,68,57,48,51,56,32,111,110,108,105,110,101
            };
        }
    }
}

