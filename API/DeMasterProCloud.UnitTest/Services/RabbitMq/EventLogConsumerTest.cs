using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using EfCore.InMemoryHelpers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using MoreLinq;
using RabbitMQ.Client;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services.RabbitMq
{
    public class EventLogConsumerTest
    {
        //private readonly List<IcuDevice> _icuDevices;

        //private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        //private readonly IConfigurationRoot _configuration;
        //private readonly Mock<EventLogConsumer> _eventLogConsumer;
        //private readonly IConnection _connection;

        //public EventLogConsumerTest()
        //{
        //    _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
        //    ApplicationVariables.Configuration = _configuration;
        //    _icuDevices = GetTestDevices();
        //    var users = GetListUsers();

        //    //Initial mapping
        //    AutoMapperConfig.Initialize();

        //    //Mock Unit Of Work
        //    _mockUnitOfWork = new Mock<IUnitOfWork>();
            
        //    var appDbContext = TestHelper.GetAppDbContext();
        //    appDbContext.IcuDevice = TestHelper.GetMockDbSet(_icuDevices).Object;
        //    _mockUnitOfWork.Setup(x => x.IcuDeviceRepository).Returns(new IcuDeviceRepository(appDbContext, null));
        //    appDbContext.User = TestHelper.GetMockDbSet(users).Object;
        //    _mockUnitOfWork.Setup(x => x.UserRepository).Returns(new UserRepository(appDbContext, null));
        //    var mockQueueService = new Mock<IQueueService>();
        //    _eventLogConsumer = new Mock<EventLogConsumer>(_configuration, mockQueueService.Object);
        //}

        //[Fact]
        //public void ParseData_LengthLessThan99_ReturnNull()
        //{
        //    var result = _eventLogConsumer.Object.ParseData(GetEventLogsBytes().Slice(0, 99).ToArray());
        //    Assert.Null(result);
        //}


        //[Fact]
        //public void ParseData_ReturnEventCount()
        //{
        //    var result = _eventLogConsumer.Object.ParseData(GetEventLogsBytes());
        //    Assert.True(result.Data != null);
        //    Assert.True(result.MsgId == "36871D_0f8fad5b-d9cb-469f-a165-70867728950e");
        //    Assert.True(result.Type == "EVENT_LOG");
        //    Assert.True(result.Data.Total == 1);
        //    Assert.True(result.Data.Events[0].DeviceAddress == "36871D");
        //    Assert.True(result.Data.Events[0].AccessTime == "08012019180817");
        //    Assert.True(result.Data.Events[0].CardId == "T06B9QHWJ0N28773");
        //    Assert.True(result.Data.Events[0].IssueCount == 2);
        //    Assert.True(result.Data.Events[0].UserName == null);
        //    Assert.True(result.Data.Events[0].UpdateTime == "08012019180817111111");
        //    Assert.True(result.Data.Events[0].InOut == "In");
        //    Assert.True(result.Data.Events[0].EventType == 1);
        //}

        //[Fact]
        //public async Task DoWork_ByteZero_ReturnNull()
        //{
        //    var result = await _eventLogConsumer.Object.DoWork(new byte[] { 0 });
        //    Assert.Null(result);
        //}

        //[Fact]
        //public async Task DoWork_DeviceNotFound_ReturnNull()
        //{
        //    var testData = GetEventLogsBytes();
        //    testData[1] = 99; //try to change icu address that not existed in the database
        //    var result = await _eventLogConsumer.Object.DoWork(testData);
        //    Assert.True(!result.Any());
        //}

        //[Fact]
        //public async Task DoWork_OnPerfomanceLog_Single()
        //{
        //    var reg = new Regex(_csvFile);
        //    var inputFilePaths = Directory.GetFiles(Environment.CurrentDirectory, "*.csv")
        //        .Where(path => reg.IsMatch(path))
        //        .ToList();
        //    foreach (var filePath in inputFilePaths)
        //    {
        //        File.Delete(filePath);
        //    }
        //    _configuration[Constants.Settings.EnablePerformanceLog] = "true";
        //    var testData = GetEventLogsBytes();
        //    await _eventLogConsumer.Object.DoWork(testData);

        //    inputFilePaths = Directory.GetFiles(Environment.CurrentDirectory, "*.csv")
        //        .Where(path => reg.IsMatch(path))
        //        .ToList();
        //    Assert.True(inputFilePaths.Any());
        //}

        //[Fact]
        //public async Task DoWork_OnPerfomanceLog_Multiple()
        //{
        //    var reg = new Regex(_csvFile);
        //    var inputFilePaths = Directory.GetFiles(Environment.CurrentDirectory, "*.csv")
        //        .Where(path => reg.IsMatch(path))
        //        .ToList();
        //    foreach (var filePath in inputFilePaths)
        //    {
        //        File.Delete(filePath);
        //    }
        //    _configuration[Constants.Settings.EnablePerformanceLog] = "true";
        //    var testData = GetEventLogsBytes2();
        //    await _eventLogConsumer.Object.DoWork(testData);

        //    inputFilePaths = Directory.GetFiles(Environment.CurrentDirectory, "*.csv")
        //        .Where(path => reg.IsMatch(path))
        //        .ToList();
        //    Assert.True(inputFilePaths.Any());
        //}

        //[Fact]
        //public async Task DoWork_OffPerfomanceLog_NonOutputLog()
        //{
        //    var reg = new Regex(_csvFile);
        //    var inputFilePaths = Directory.GetFiles(Environment.CurrentDirectory, "*.csv")
        //        .Where(path => reg.IsMatch(path))
        //        .ToList();
        //    foreach (var filePath in inputFilePaths)
        //    {
        //        File.Delete(filePath);
        //    }
        //    _configuration[Constants.Settings.EnablePerformanceLog] = "false";
        //    var testData = GetEventLogsBytes();
        //    _mockUnitOfWork
        //        .Setup(x => x.IcuDeviceRepository.GetDeviceByCompanyAndAddress(It.IsAny<int>(), It.IsAny<string>()))
        //        .Returns(_icuDevices.First());
        //    await _eventLogConsumer.Object.DoWork(testData);

        //    var isExisted = File.Exists(_csvFile);
        //    Assert.False(isExisted);
        //}

        //[Fact]
        //public async Task Dowork_Device_Null()
        //{
        //    var testData = GetEventLogBytesTest();
        //    var eventlogs = await _eventLogConsumer.Object.DoWork(testData);
        //    Assert.True(eventlogs == null);
        //}

        //[Fact]
        //public void SendDataToFe_EventLogs()
        //{
        //    var testEventLogs = GetEventLogs();
        //    var receiData = _eventLogConsumer.Object.ParseData(GetEventLogsBytes());
        //    _eventLogConsumer.Object.SendDataToFe(_mockUnitOfWork.Object,testEventLogs, receiData, GetTestDevices().First());
        //}

        //[Fact]
        //public async Task DoWork_ReturnEventLogs()
        //{
        //    _mockUnitOfWork
        //        .Setup(x => x.IcuDeviceRepository.GetDeviceByCompanyAndAddress(It.IsAny<int>(), It.IsAny<string>()))
        //        .Returns(_icuDevices.First());
        //    _mockUnitOfWork
        //        .Setup(x => x.UserRepository.GetByCardId(It.IsAny<int>(), It.IsAny<string>()))
        //        .Returns(GetTestUser());
        //    _mockUnitOfWork
        //        .Setup(x => x.UserRepository.GetByKeyPadPw(It.IsAny<int>(), It.IsAny<string>()))
        //        .Returns(GetTestUser());
        //    _mockUnitOfWork
        //        .Setup(x => x.EventLogRepository.Add(It.IsAny<EventLog>()));
        //    var result = await _eventLogConsumer.Object.DoWork(GetEventLogsBytes());
        //    Assert.True(result.Count == 1);
        //}

        //private byte[] GetEventLogsBytes()
        //{

        //    return new byte[]
        //    {
        //        123, 34, 109, 115, 103, 73, 100, 34, 58, 32, 34, 51, 54, 56, 55, 49, 68, 95, 48, 102, 56, 102, 97, 100,
        //        53, 98, 45, 100, 57, 99, 98, 45, 52, 54, 57, 102, 45, 97, 49, 54, 53, 45, 55, 48, 56, 54, 55, 55, 50,
        //        56, 57, 53, 48, 101, 34, 44, 34, 116, 121, 112, 101, 34, 58, 32, 34, 69, 86, 69, 78, 84, 95, 76, 79, 71,
        //        34, 44, 34, 100, 97, 116, 97, 34, 58, 32, 123, 34, 116, 111, 116, 97, 108, 34, 58, 32, 49, 44, 34, 101,
        //        118, 101, 110, 116, 115, 34, 58, 32, 91, 123, 34, 100, 101, 118, 105, 99, 101, 65, 100, 100, 114, 101,
        //        115, 115, 34, 58, 32, 34, 51, 54, 56, 55, 49, 68, 34, 44, 34, 97, 99, 99, 101, 115, 115, 84, 105, 109,
        //        101, 34, 58, 32, 34, 48, 56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56, 49, 55, 34, 44, 34, 99, 97, 114,
        //        100, 73, 100, 34, 58, 32, 34, 84, 48, 54, 66, 57, 81, 72, 87, 74, 48, 78, 50, 56, 55, 55, 51, 34, 44,
        //        34, 105, 115, 115, 117, 101, 67, 111, 117, 110, 116, 34, 58, 32, 34, 50, 34, 44, 34, 117, 115, 101, 114,
        //        110, 97, 109, 101, 34, 58, 32, 110, 117, 108, 108, 44, 34, 117, 112, 100, 97, 116, 101, 84, 105, 109,
        //        101, 34, 58, 32, 34, 48, 56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56, 49, 55, 49, 49, 49, 49, 49, 49, 34,
        //        44, 34, 105, 110, 79, 117, 116, 34, 58, 32, 34, 73, 110, 34, 44, 34, 101, 118, 101, 110, 116, 84, 121,
        //        112, 101, 34, 58, 49, 125, 93, 125, 125
        //    };
        //}

        //private byte[] GetEventLogBytesTest()
        //{
        //    return new byte[]
        //    {
        //        123, 34, 109, 115, 103, 73, 100, 34, 58, 32, 34, 48, 48, 48, 49, 49, 49, 95, 48, 102, 56, 102, 97, 100,
        //        53, 98, 45, 100, 57, 99, 98, 45, 52, 54, 57, 102, 45, 97, 49, 54, 53, 45, 55, 48, 56, 54, 55, 55, 50,
        //        56, 57, 53, 48, 101, 34, 44, 34, 116, 121, 112, 101, 34, 58, 32, 34, 69, 86, 69, 78, 84, 95, 76, 79, 71,
        //        34, 44, 34, 100, 97, 116, 97, 34, 58, 32, 123, 34, 116, 111, 116, 97, 108, 34, 58, 32, 49, 44, 34, 101,
        //        118, 101, 110, 116, 115, 34, 58, 32, 91, 123, 34, 100, 101, 118, 105, 99, 101, 65, 100, 100, 114, 101,
        //        115, 115, 34, 58, 32, 34, 48, 48, 48, 49, 49, 49, 34, 44, 34, 97, 99, 99, 101, 115, 115, 84, 105, 109,
        //        101, 34, 58, 32, 34, 48, 56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56, 49, 55, 34, 44, 34, 99, 97, 114,
        //        100, 73, 100, 34, 58, 32, 34, 84, 48, 54, 66, 57, 81, 72, 87, 74, 48, 78, 50, 56, 55, 55, 51, 34, 44,
        //        34, 105, 115, 115, 117, 101, 67, 111, 117, 110, 116, 34, 58, 32, 34, 50, 34, 44, 34, 117, 115, 101, 114,
        //        110, 97, 109, 101, 34, 58, 32, 110, 117, 108, 108, 44, 34, 117, 112, 100, 97, 116, 101, 84, 105, 109,
        //        101, 34, 58, 32, 34, 48, 56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56, 49, 55, 49, 49, 49, 49, 49, 49, 34,
        //        44, 34, 105, 110, 79, 117, 116, 34, 58, 32, 34, 73, 110, 34, 44, 34, 101, 118, 101, 110, 116, 84, 121,
        //        112, 101, 34, 58, 49, 125, 93, 125, 125
        //    };
        //}

        //private byte[] GetEventLogsBytes2()
        //{
        //    return new byte[]
        //    {
        //        123, 34, 109, 115, 103, 73, 100, 34, 58, 34, 51, 54, 56, 55, 49, 68, 95, 48, 102, 56, 102, 97, 100, 53,
        //        98, 45, 100, 57, 99, 98, 45, 52, 54, 57, 102, 45, 97, 49, 54, 53, 45, 55, 48, 56, 54, 55, 55, 50, 56,
        //        57, 53, 48, 101, 34, 44, 34, 116, 121, 112, 101, 34, 58, 34, 69, 86, 69, 78, 84, 95, 76, 79, 71, 34, 44,
        //        34, 100, 97, 116, 97, 34, 58, 123, 34, 116, 111, 116, 97, 108, 34, 58, 49, 44, 34, 101, 118, 101, 110,
        //        116, 115, 34, 58, 91, 123, 34, 100, 101, 118, 105, 99, 101, 65, 100, 100, 114, 101, 115, 115, 34, 58,
        //        34, 51, 54, 56, 55, 49, 68, 34, 44, 34, 97, 99, 99, 101, 115, 115, 84, 105, 109, 101, 34, 58, 34, 48,
        //        56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56, 49, 55, 34, 44, 34, 99, 97, 114, 100, 73, 100, 34, 58, 34,
        //        84, 48, 54, 66, 57, 81, 72, 87, 74, 48, 78, 50, 56, 55, 55, 51, 34, 44, 34, 105, 115, 115, 117, 101, 67,
        //        111, 117, 110, 116, 34, 58, 34, 50, 34, 44, 34, 117, 115, 101, 114, 110, 97, 109, 101, 34, 58, 110, 117,
        //        108, 108, 44, 34, 117, 112, 100, 97, 116, 101, 84, 105, 109, 101, 34, 58, 34, 48, 56, 48, 49, 50, 48,
        //        49, 57, 49, 56, 48, 56, 49, 55, 49, 49, 49, 49, 49, 49, 34, 44, 34, 105, 110, 79, 117, 116, 34, 58, 34,
        //        73, 110, 34, 44, 34, 101, 118, 101, 110, 116, 84, 121, 112, 101, 34, 58, 49, 125, 44, 123, 34, 100, 101,
        //        118, 105, 99, 101, 65, 100, 100, 114, 101, 115, 115, 34, 58, 34, 51, 54, 56, 55, 49, 68, 34, 44, 34, 97,
        //        99, 99, 101, 115, 115, 84, 105, 109, 101, 34, 58, 34, 48, 56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56,
        //        49, 56, 34, 44, 34, 99, 97, 114, 100, 73, 100, 34, 58, 34, 84, 48, 54, 66, 57, 81, 72, 87, 74, 48, 78,
        //        50, 56, 55, 55, 51, 34, 44, 34, 105, 115, 115, 117, 101, 67, 111, 117, 110, 116, 34, 58, 34, 50, 34, 44,
        //        34, 117, 115, 101, 114, 110, 97, 109, 101, 34, 58, 110, 117, 108, 108, 44, 34, 117, 112, 100, 97, 116,
        //        101, 84, 105, 109, 101, 34, 58, 34, 48, 56, 48, 49, 50, 48, 49, 57, 49, 56, 48, 56, 49, 55, 49, 49, 49,
        //        49, 49, 50, 34, 44, 34, 105, 110, 79, 117, 116, 34, 58, 34, 73, 110, 34, 44, 34, 101, 118, 101, 110,
        //        116, 84, 121, 112, 101, 34, 58, 49, 125, 93, 125, 125
        //    };
        //}

        //private List<EventLog> GetEventLogs()
        //{
        //    var eventLog = new EventLog
        //    {
        //        Id = 1,
        //        Antipass = "I",
        //        CardId = "A",
        //        CardType = 2,
        //        CompanyId = 1,
        //        DoorName = "196E69",
        //        EventTime = DateTime.Now,
        //        EventType = 2,
        //        IcuId = 1,
        //        Icu = _icuDevices.First(),
        //        UserId = 1,
        //        User = GetTestUser()
        //    };
        //    return new List<EventLog> { eventLog };
        //}

        //private User GetTestUser()
        //{
        //    return new User
        //    {
        //        Id = 1,
        //        CompanyId = 1,
        //        UserCode = 1,
        //        CardId = "A",
        //        KeyPadPw = Encryptor.Encrypt("123", _configuration[Constants.Settings.EncryptKey]),
        //        ExpiredDate = DateTime.Now.AddMonths(12),
        //        Status = (short)Status.Valid
        //    };
        //}

        //private List<User> GetListUsers()
        //{
        //    var user = new User
        //    {
        //        Id = 1,
        //        CompanyId = 1,
        //        UserCode = 1,
        //        CardId = "A",
        //        KeyPadPw = Encryptor.Encrypt("123", _configuration[Constants.Settings.EncryptKey]),
        //        ExpiredDate = DateTime.Now.AddMonths(12),
        //        Status = (short)Status.Valid,
        //        Department = new Department
        //        {
        //            DepartName = "Department name"
        //        }
        //    };
        //    return new List<User> { user };
        //}

        //private List<IcuDevice> GetTestDevices()
        //{
        //    var devices = new List<IcuDevice>
        //    {
        //        new IcuDevice
        //        {
        //            Id = 1,
        //            DeviceAddress = "36871D",
        //            Door = new Door
        //            {
        //                Name = "196E69"
        //            },
        //            CompanyId = 1
        //        },
        //        new IcuDevice
        //        {
        //            Id = 2,
        //            DeviceAddress = "000001",
        //            Door = new Door
        //            {
        //                Name = "000001"
        //            },
        //            CompanyId = 1
        //        }
        //    };
        //    return devices;
        //}
    }
}
