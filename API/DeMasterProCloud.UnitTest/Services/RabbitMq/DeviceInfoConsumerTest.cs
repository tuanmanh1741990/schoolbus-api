﻿using Castle.Core.Logging;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoreLinq.Extensions;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services.RabbitMq
{
    public class DeviceInfoConsumerTest
    {
        private readonly Mock<IUnitOfWork> _mockUnitOfWork;
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<DeviceInfoConsumer> _deviceInfoConsumer;
        private readonly List<IcuDevice> _icuDevices;
        public DeviceInfoConsumerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);

            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();
            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            _icuDevices = TestHelper.GetTestIcuDevice();

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock Unit Of Work
            _mockUnitOfWork = new Mock<IUnitOfWork>();
            var appDbContext = TestHelper.GetAppDbContext();
            appDbContext.IcuDevice = TestHelper.GetMockDbSet(_icuDevices).Object;
            _mockUnitOfWork.Setup(x => x.IcuDeviceRepository).Returns(new IcuDeviceRepository(appDbContext, null));
            _deviceInfoConsumer = new Mock<DeviceInfoConsumer>(_configuration);
        }

        //[Fact]
        //public void ParseData_LengthLessThan100_ReturnNull()
        //{
        //    var result = _deviceInfoConsumer.Object.ParseData(GetDeviceInfoBytes().Slice(0, 49).ToArray());
        //    Assert.Null(result);
        //}

        //[Fact]
        //public void ParseData_ReturnEventCount()
        //{
        //    var result = _deviceInfoConsumer.Object.ParseData(GetDeviceInfoBytes());
        //    Assert.True(result.DeviceAddress == "0D9038");
        //    Assert.True(result.Gateway == "192.168.32.1");
        //    Assert.True(result.Ip == "192.168.32.104");
        //    Assert.True(result.Mac == "32:00:21:00:19:51");
        //    Assert.True(result.Subnet == "255.255.255.0");
        //    Assert.True(result.Version == "0.0.0");
        //}

        //[Fact]
        //public void DoWork_ByteZero_ReturnFalse()
        //{
        //    var result = _deviceInfoConsumer.Object.DoWork(new byte[] { 0 });
        //    Assert.True(result == false);
        //}

        //[Fact]
        //public void DoWork_DeviceNotFound_ReturnNull()
        //{
        //    var testData = GetDeviceInfoBytes();
        //    testData[1] = 99;
        //    var result = _deviceInfoConsumer.Object.DoWork(testData);
        //    Assert.True(result == false);
        //}

        //private byte[] GetDeviceInfoBytes()
        //{
        //    return new byte[]
        //    {
        //        123, 34, 86, 101, 114, 115, 105, 111, 110, 34, 58, 34, 48, 46, 48, 46, 48, 34, 44, 32, 34, 68, 101, 118,
        //        105, 99, 101, 65, 100, 100, 114, 101, 115, 115, 34, 58, 34, 48, 68, 57, 48, 51, 56, 34, 44, 32, 34, 77,
        //        97, 99, 34, 58, 34, 51, 50, 58, 48, 48, 58, 50, 49, 58, 48, 48, 58, 49, 57, 58, 53, 49, 34, 44, 32, 34,
        //        73, 112, 34, 58, 34, 49, 57, 50, 46, 49, 54, 56, 46, 51, 50, 46, 49, 48, 52, 34, 44, 32, 34, 83, 117,
        //        98, 110, 101, 116, 34, 58, 34, 50, 53, 53, 46, 50, 53, 53, 46, 50, 53, 53, 46, 48, 34, 44, 32, 34, 71,
        //        97, 116, 101, 119, 97, 121, 34, 58, 34, 49, 57, 50, 46, 49, 54, 56, 46, 51, 50, 46, 49, 34, 125
        //    };
        //}
    }
}
