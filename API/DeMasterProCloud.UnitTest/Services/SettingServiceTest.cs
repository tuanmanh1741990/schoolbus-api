using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class SettingServiceTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly Mock<SettingService> _mockSettingService;
        private readonly List<Setting> _settings;
        private readonly AppDbContext _appDbContext;

        public SettingServiceTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;

            _settings = TestHelper.GetTestSettings();

            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment.SetupGet(m => m.ContentRootPath).Returns(Environment.CurrentDirectory);
            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();

            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock UnitOfWork
            _appDbContext = TestHelper.GetAppDbContext();

            if (!_appDbContext.Setting.Any())
            {
                foreach (var setting in _settings)
                {
                    setting.Id = 0;
                    _appDbContext.Setting.Add(setting);
                    _appDbContext.SaveChanges();
                    _appDbContext.Entry(setting).State = EntityState.Detached;
                }
            }

            //_mockUnitOfWork.Setup(x => x.AppDbContext).Returns(appDbContext);
            _mockUnitOfWork = new Mock<UnitOfWork>(_appDbContext, mockHttpContext.Object);

            _mockSettingService = new Mock<SettingService>(_mockUnitOfWork.Object, _configuration);
        }

        //[Fact]
        //public void GetByKey_Key_ReturnSetting()
        //{
        //    var keyExistTest = "Key";
        //    var setting = _mockSettingService.Object.GetByKey(keyExistTest);
        //    Assert.True(setting.Key == keyExistTest);
        //}

        [Fact]
        public void GetByKey_Key_ReturnNull()
        {
            var companyId = 1;
            var keyNotExistTest = "KeyNotExist";
            var setting = _mockSettingService.Object.GetByKey(keyNotExistTest, companyId);
            Assert.Null(setting);
        }

        [Fact]
        public void GetById_Id_ReturnSetting()
        {
            var updateSetting = _mockUnitOfWork.Object.AppDbContext.Setting.First();
            var setting = _mockSettingService.Object.GetById(updateSetting.Id);
            Assert.True(setting != null && setting.Id == updateSetting.Id);
        }

        [Fact]
        public void GetById_Id_ReturnNull()
        {
            var idNotExistTest = int.MinValue;
            var setting = _mockSettingService.Object.GetById(idNotExistTest);
            Assert.Null(setting);
        }

        [Fact]
        public void Update_SettingModel_ReturnSuccess()
        {
            var updateSetting = _mockUnitOfWork.Object.AppDbContext.Setting.First();
            var newSettingModel = new SettingModel
            {
                Id = updateSetting.Id,
                Key = updateSetting.Key,
                Value = new List<string> { "NewValue" }
            };
            _mockSettingService.Object.Update(newSettingModel);
            var setting = _mockSettingService.Object.GetById(newSettingModel.Id);
            Assert.True(setting?.Value == JsonConvert.SerializeObject(newSettingModel.Value));
        }

        //[Fact]
        //public void Update_SettingModel_ReturnFail()
        //{
        //    var newSettingModel = new SettingModel
        //    {
        //        Id = 0,
        //        Key = null,
        //        Value = null
        //    };
        //    Assert.Throws<ArgumentNullException>(() => _mockSettingService.Object.Update(newSettingModel));
        //}

        [Fact]
        public void Update_ListSettingModel_ReturnSuccess()
        {
            var updateSettings = _mockUnitOfWork.Object.AppDbContext.Setting.Take(2).ToList();
            var settingModels = updateSettings.Select((t, i) => new SettingModel
            {
                Id = t.Id,
                Key = t.Key,
                Value = new List<string> { "NewValue" + i }
            })
                .ToList();
            _mockSettingService.Object.Update(settingModels);
            foreach (var settingModel in settingModels)
            {
                var setting = _mockSettingService.Object.GetById(settingModel.Id);
                Assert.True(setting?.Value == JsonConvert.SerializeObject(settingModel.Value));
            }
        }

        [Fact]
        public void Update_ListSettingModel_ReturnFail()
        {
            var updateSettings = _mockUnitOfWork.Object.AppDbContext.Setting.Take(2).ToList();
            var settingModels = updateSettings.Select(t => new SettingModel
            {
                Id = 0,
                Key = t.Key,
                Value = null
            })
                .ToList();
            Assert.Throws<NullReferenceException>(() => _mockSettingService.Object.Update(settingModels));
        }

        [Fact]
        public void GetAll_ReturnListFileSetting()
        {
            var companyId = 1;
            var settings = _mockSettingService.Object.GetAll(companyId);
            Assert.True(settings.Count == 2);
        }

        [Fact]
        public void GetAll_ReturnListFileSettingEmpty()
        {
            var companyId = 1;
            var configuration = new Mock<IConfiguration>();
            configuration.Setup(x => x.GetSection(Constants.Settings.FileSettings)).Returns((IConfigurationSection)null);
            var mockSettingService = new Mock<SettingService>(_mockUnitOfWork.Object, configuration.Object);
            var settings = mockSettingService.Object.GetAll(companyId);
            Assert.True(settings == null || !settings.Any());
        }

        [Fact]
        public void Set_SaveDatabase()
        {
            var companyId = 1; 

            _mockSettingService.Object.Set(companyId);

            var settings = _mockSettingService.Object.GetAll(companyId);
            Assert.True(settings[settings.Count - 1].Key != "monitoring_event_type_default" &&
                        settings[settings.Count - 2].Key != "monitoring_max_record_display");
        }


        [Fact]
        public void Set_NonSaveDatabase()
        {
            var companyId = 1;
            var beforeCount = _mockSettingService.Object.GetAll(companyId).Count;
            var configuration = new Mock<IConfiguration>();
            configuration.Setup(x => x.GetSection(Constants.Settings.FileSettings)).Returns((IConfigurationSection)null);
            var mockSettingService = new Mock<SettingService>(_mockUnitOfWork.Object, configuration.Object);
            mockSettingService.Object.Set(companyId);

            var afterSetCount = _mockSettingService.Object.GetAll(companyId).Count;
            Assert.True(beforeCount == afterSetCount);
        }
    }
}
