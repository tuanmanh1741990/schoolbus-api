﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Timezone;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace DeMasterProCloud.UnitTest.Services
{
    public class TimezoneServiceTest
    {
        private readonly Mock<UnitOfWork> _mockUnitOfWork;
        private readonly Mock<IQueueService> _mockQueueService;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private Mock<ILogger<TimezoneService>> _mockLogTimezoneService;
        private Mock<TimezoneService> _timezoneService;
        private readonly List<Timezone> _timezones;

        public TimezoneServiceTest()
        {
            _timezones = TestHelper.GetTestTimezone();
            _mockQueueService = new Mock<IQueueService>();
            _mockLogTimezoneService = new Mock<ILogger<TimezoneService>>();
            //Mock HttpContext
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock UnitOfWork

            var appDbContext = TestHelper.GetAppDbContext();
            List<Timezone> list = new List<Timezone>();

            if (!appDbContext.Timezone.Any())
            {
                foreach (var timezone in _timezones)
                {
                    timezone.Id = 0;
                    appDbContext.Timezone.Add(timezone);
                    appDbContext.SaveChanges();
                    appDbContext.Entry(timezone).State = EntityState.Detached;
                }
            }
            _mockUnitOfWork = new Mock<UnitOfWork>(appDbContext, _mockHttpContext.Object);

            _timezoneService = new Mock<TimezoneService>(_mockUnitOfWork.Object, _mockQueueService.Object,
                _mockHttpContext.Object, _mockLogTimezoneService.Object);
        }

        [Fact]
        public void Check_InitData_Return_TimezoneModel()
        {
            var timezone = _timezones.First();
            var result = _timezoneService.Object.InitData(timezone);
            Assert.True(result.Id == timezone.Id);
            Assert.True(result.Name == timezone.Name);
            Assert.True(result.Remarks == timezone.Remarks);
            Assert.True(result.Position == timezone.Position);
            Assert.True(result.CompanyId == timezone.CompanyId);
            //Assert.True(result.IsDe == timezone.Status);
        }

        [Fact]
        public void Check_InitData_Return_Null()
        {
            var result = _timezoneService.Object.InitData(null);
            Assert.True(result == null);
        }

        [Fact]
        public void Add_Timezone_Success()
        {
            var timezone = _timezones.First();
            var timezoneModel = _timezoneService.Object.InitData(timezone);
            _timezoneService.Object.Add(timezoneModel);
            var newTimezone = _mockUnitOfWork.Object.AppDbContext.Timezone.ToList().Last();
            Assert.True(newTimezone.Name == timezone.Name);
            Assert.True(newTimezone.Remarks == timezone.Remarks);
            Assert.True(newTimezone.CompanyId == timezone.CompanyId);
        }

        [Fact]
        public void Add_Timezone_Return_NullException()
        {
            Assert.Throws<NullReferenceException>(() => _timezoneService.Object.Add(null));
        }

        [Fact]
        public void Update_Timezone_Success()
        {
            var timezone = _timezones.First();
            var timezoneModel = _timezoneService.Object.InitData(timezone);
            _timezoneService.Object.Update(timezoneModel, timezone);
            var timezoneUpdated = _mockUnitOfWork.Object.AppDbContext.Timezone.Find(timezone.Id);
            Assert.True(timezoneUpdated.Name == timezone.Name);
            Assert.True(timezoneUpdated.Remarks == timezone.Remarks);
            Assert.True(timezoneUpdated.CompanyId == timezone.CompanyId);
            //Assert.True(timezoneUpdated.Status == timezone.Status);
            //Todo: update unit test
        }

        [Fact]
        public void Update_Timezone_Return_NullException()
        {
            Assert.Throws<NullReferenceException>(() => _timezoneService.Object.Update(null, null));
        }

        [Fact]
        public void Delete_Timezone_Success()
        {
            var timezone = _timezones.First();
            _timezoneService.Object.Delete(timezone);
            var timezoneDeleted = _mockUnitOfWork.Object.AppDbContext.Timezone.Find(timezone.Id);
            Assert.True(timezoneDeleted.IsDeleted);
        }

        [Fact]
        public void Delete_Timezone_Return_NullException()
        {
            Assert.Throws<NullReferenceException>(() => _timezoneService.Object.Delete(null));
        }

        [Fact]
        public void DeleteRange_Timezone_Success()
        {
            var timezones = _timezones.Take(3).ToList();
            _timezoneService.Object.DeleteRange(timezones);
            var timezoneDeleteds = _mockUnitOfWork.Object.AppDbContext.Timezone.Take(3).ToList();
            foreach (var item in timezoneDeleteds)
            {
                Assert.True(item.IsDeleted);
            }
        }

        [Fact]
        public void DeleteRange_Timezone_Return_NullException()
        {
            Assert.Throws<NullReferenceException>(() => _timezoneService.Object.DeleteRange(null));
        }

        [Fact]
        public void Get_TimezoneCount_Return_NumberTimezone()
        {
            var companyId = 1;
            var result = _timezoneService.Object.GetTimezoneCount(companyId);
            Assert.True(result == 3);
        }

        [Fact]
        public void Get_TimezoneCount_Return_NonExist()
        {
            var companyId = 0;
            var result = _timezoneService.Object.GetTimezoneCount(companyId);
            Assert.True(result == 0);
        }

        [Fact]
        public void Get_Timezones_Return_ListTimezone()
        {
            var companyId = 1;
            var result = _timezoneService.Object.GetTimezones(companyId);
            var expected = new List<string> { "UTC", "DST", "AKDT" };
            var resultNotExpected = result.Select(x => x.Name).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Name)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void Get_Timezones_Return_NonExist()
        {
            var companyId = 0;
            var result = _timezoneService.Object.GetTimezones(companyId);
            Assert.True(!result.Any());
        }

        //[Fact]
        //public void Get_PositionsByCompany_Return_ListPosition()
        //{
        //    var companyId = 1;
        //    var result = _timezoneService.Object.GetPositionsByCompany(companyId);
        //    var expected = new List<int> { 1, 1, 2 };
        //    var resultNotExpected = result.Select(x => x).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_PositionsByCompany_Return_Non_Exist()
        {
            var companyId = 0;
            var result = _timezoneService.Object.GetPositionsByCompany(companyId);
            Assert.True(!result.Any());
        }

        [Fact]
        public void GetSelectListTimezone_Test()
        {
            var result = _timezoneService.Object.GetSelectListTimezone().ToList();
            var expected = new List<string> { "UTC", "DST", "AKDT" };
            var resultNotExpected = result.Select(x => x.Text).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Text)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }
        [Theory]
        [InlineData("T", 1, 10, 1, "desc")]
        public void Get_PaginatedDevice_Return_ListIcuDevice_Success(string filter, int pageNumber, int pageSize, int sortColumn,
    string sortDirection)
        {

            var result = _timezoneService.Object.GetPaginated(filter, pageNumber, pageSize, sortColumn, sortDirection, out _, out _).ToList();
            var expected = new List<string> { "UTC", "DST", "AKDT" };
            var resultNotExpected = result.Select(x => x.Remark).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Remark)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        //[Fact]
        //public void Get_ByIdAndCompany_Return_Timezone()
        //{
        //    var timezoneId = _timezones.First().Id;
        //    var companyId = 1;
        //    var result = _timezoneService.Object.GetByIdAndCompany(timezoneId, companyId);
        //    Assert.True(result.Id == timezoneId);
        //    Assert.True(result.Name == "UTC");
        //    Assert.True(result.Remarks == "UTC");
        //    Assert.True(result.CompanyId == companyId);
        //    Assert.True(result.IsDeleted == false);
        //    Assert.True(result.Position == 1);
        //}

        [Fact]
        public void Get_ByIdAndCompany_Return_NonExist()
        {
            var timezoneId = 0;
            var companyId = 1;
            var result = _timezoneService.Object.GetByIdAndCompany(timezoneId, companyId);
            Assert.True(result == null);
        }
        [Fact]
        public void GetByIdsAndCompany_Return_ListTimezone()
        {

            var ids = new List<int>() { _timezones.First().Id, _timezones.Skip(1).First().Id, _timezones.Last().Id };
            var companyId = 1;
            var result = _timezoneService.Object.GetByIdsAndCompany(ids, companyId);
            var expected = new List<string> { "UTC", "DST" };
            var resultNotExpected = result.Select(x => x.Name).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Name)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void GetByIdsAndCompany_Return_NonExist()
        {

            var ids = new List<int>() { 0, -1, 1 };
            var companyId = 0;
            var result = _timezoneService.Object.GetByIdsAndCompany(ids, companyId);
            Assert.True(!result.Any());
        }
        //[Fact]
        //public void Write_TimezoneLog_Success()
        //{
        //    var timezone = _timezones.First();
        //    var icuDevices = TestHelper.GetTestIcuDevice();
        //    _timezoneService.Object.WriteTimezoneLog(timezone, ActionType.Add, icuDevices);
        //    var newTimezoneLogs = _mockUnitOfWork.Object.AppDbContext.TimezoneLog.ToList();
        //    var expected = new List<int> { 1, 2, 3, 4 };
        //    var resultNotExpected = newTimezoneLogs.Select(x => x.IcuId).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(newTimezoneLogs.Select(x => x.IcuId)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        //[Fact]
        //public void Write_TimezoneLog_Fail()
        //{
        //    var timezone = _timezones.First();
        //    var icuDevices = TestHelper.GetTestIcuDevice().Take(0).ToList();
        //    _timezoneService.Object.WriteTimezoneLog(timezone, ActionType.Add, icuDevices);
        //    var newTimezoneLogs = _mockUnitOfWork.Object.AppDbContext.TimezoneLog.ToList();
        //    Assert.True(!newTimezoneLogs.Any());
        //}

        //[Fact]
        //public void Get_TimezoneByNameAndCompany_Timezone()
        //{
        //    var companyId = 1;
        //    var name = "UTC";
        //    var result = _timezoneService.Object.GetTimezoneByNameAndCompany(companyId, name);
        //    Assert.True(result.Name == name);
        //    Assert.True(result.Remarks == "UTC");
        //    Assert.True(result.CompanyId == companyId);
        //    Assert.True(result.IsDeleted == false);
        //    Assert.True(result.Position == 1);
        //}

        [Fact]
        public void Get_TimezoneByNameAndCompany_Non_Exist()
        {
            var companyId = 1;
            var name = "AMT";
            var result = _timezoneService.Object.GetTimezoneByNameAndCompany(companyId, name);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Check_IsDefaultTimezone_Return_True()
        //{
        //    var companyId = 1;
        //    var timezoneId = _timezones.First().Id;
        //    var result = _timezoneService.Object.IsDefaultTimezone(companyId, timezoneId);
        //    Assert.True(result);
        //}

        [Fact]
        public void Check_IsDefaultTimezone_Return_False()
        {
            var companyId = 1;
            var timezoneId = _timezones.Last().Id;
            var result = _timezoneService.Object.IsDefaultTimezone(companyId, timezoneId);
            Assert.False(result);
        }

        [Fact]
        public void Check_IsExistedName_Return_True()
        {
            var timezoneId = 1;
            var name = "UTC";
            var result = _timezoneService.Object.IsExistedName(timezoneId, name);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsExistedName_Return_False()
        {
            var timezoneId = 2;
            var name = "DTC";
            var result = _timezoneService.Object.IsExistedName(timezoneId, name);
            Assert.False(result);
        }

       // [Fact]
        //public void Check_ConvertDayDetailToString_Return()
        //{
        //    var dayDetails = new List<DayDetail>();
        //    dayDetails.Add(new DayDetail()
        //    {
        //        From = 1,
        //        To = 3,
        //    });
        //    dayDetails.Add(new DayDetail()
        //    {
        //        From = 14,
        //        To = 345,
        //    });

        //    var result = _timezoneService.Object.ConvertDayDetailToString(dayDetails);
        //    foreach (var item in dayDetails)
        //    {
        //        Assert.True(item == null);
        //    }
        //}

        [Fact]
        public void Check_ConvertDayDetailToString_ReturnNull()
        {
            var dayDetails = new List<DayDetail>();
            var result = _timezoneService.Object.ConvertDayDetailToString(dayDetails);
            foreach (var item in dayDetails)
            {
                Assert.True(item==null);
            }
            
        }

        //[Fact]
        //public void Check_MappingData_Return_True()
        //{
        //    var dayDetails = new List<DayDetail>();
        //    var model = new TimezoneModel()
        //    {
        //        Id = 1,
        //        CompanyId = 1,
        //        Name = "UTC",
        //        Position = 1,
        //        Remarks = "UTC",
        //        Monday=new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=2
        //            },
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=2
        //            },
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=2
        //            }
        //        },
        //        Tuesday = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=2
        //            },
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=2
        //            },
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=2
        //            }
        //        },
        //        Wednesday = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=34
        //            },
        //            new DayDetail()
        //            {
        //                From=354,
        //                To=2232
        //            },
        //            new DayDetail()
        //            {
        //                From=31,
        //                To=243
        //            }
        //        },
        //        Thursday = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=11,
        //                To=25
        //            },
        //            new DayDetail()
        //            {
        //                From=5,
        //                To=22
        //            },
        //            new DayDetail()
        //            {
        //                From=6,
        //                To=287
        //            }
        //        },
        //        Friday = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=2,
        //                To=254
        //            },
        //            new DayDetail()
        //            {
        //                From=1,
        //                To=256
        //            },
        //            new DayDetail()
        //            {
        //                From=11,
        //                To=24
        //            }
        //        },
        //        Saturday = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=2,
        //                To=232
        //            },
        //            new DayDetail()
        //            {
        //                From=23,
        //                To=225
        //            },
        //            new DayDetail()
        //            {
        //                From=89,
        //                To=2676
        //            }
        //        },
        //        Sunday = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=2,
        //                To=232
        //            },
        //            new DayDetail()
        //            {
        //                From=23,
        //                To=225
        //            },
        //            new DayDetail()
        //            {
        //                From=89,
        //                To=2676
        //            }
        //        },
        //        HolidayType1 = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=52,
        //                To=432
        //            },
        //            new DayDetail()
        //            {
        //                From=213,
        //                To=225
        //            },
        //            new DayDetail()
        //            {
        //                From=898,
        //                To=2676
        //            }
        //        },
        //        HolidayType2 = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=2,
        //                To=32
        //            },
        //            new DayDetail()
        //            {
        //                From=3,
        //                To=22
        //            },
        //            new DayDetail()
        //            {
        //                From=9,
        //                To=276
        //            }
        //        },
        //        HolidayType3 = new List<DayDetail>()
        //        {
        //            new DayDetail()
        //            {
        //                From=12,
        //                To=22
        //            },
        //            new DayDetail()
        //            {
        //                From=23,
        //                To=225
        //            },
        //            new DayDetail()
        //            {
        //                From=79,
        //                To=2676
        //            }
        //        },
        //    };
        //    var timezone = _timezones.Skip(1).First();
        //    var result = _timezoneService.Object.MappingData(model, timezone);

        //}

        //[Fact]
        //public void Check_MappingData_Return_False()
        //{
        //    var dayDetails = new List<DayDetail>();
        //    var model = new TimezoneModel()
        //    {
        //        Id = 1,
        //        CompanyId = 1,
        //        Name = "UTC",
        //        Position = 1,
        //        Remarks = "UTC",
        //    };
        //    var timezone = _timezones.Skip(1).First();
        //    var result = _timezoneService.Object.MappingData(model, timezone);

        //}
    }
}
