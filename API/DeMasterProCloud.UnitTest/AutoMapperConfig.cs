﻿using AutoMapper;
using AutoMapper.Configuration;
using DeMasterProCloud.Api.Infrastructure.Mapper;

namespace DeMasterProCloud.UnitTest
{
    public static class AutoMapperConfig
    {
        private static object _thisLock = new object();
        private static bool _initialized = false;
        // Centralize automapper initialize
        public static void Initialize()
        {
            // This will ensure one thread can access to this static initialize call
            // and ensure the mapper is reseted before initialized
            lock (_thisLock)
            {
                if (!_initialized)
                {
                    var mapperConfiguration = new MapperConfigurationExpression();
                    mapperConfiguration.AddProfile<UserMapping>();
                    mapperConfiguration.AddProfile<AccountMapping>();
                    mapperConfiguration.AddProfile<DeviceMapping>();
                    mapperConfiguration.AddProfile<TimezoneMapping>();
                    mapperConfiguration.AddProfile<UserLogMapping>();                    
                    mapperConfiguration.AddProfile<EvenLogMapping>();
                    mapperConfiguration.AddProfile<SettingMapping>();
                    mapperConfiguration.AddProfile<DepartmentMapping>();
                    mapperConfiguration.AddProfile<AccessGroupMapping>();
                    mapperConfiguration.AddProfile<AccessGroupDeviceMapping>();
                    mapperConfiguration.AddProfile<HolidayMapping>();
                    mapperConfiguration.AddProfile<BuildingMapping>();
                    Mapper.Initialize(mapperConfiguration);
                    _initialized = true;

                }
            }
        }
    }
}
