﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using RabbitMQ.Client;
using RabbitMQ.Fakes;

namespace DeMasterProCloud.UnitTest
{
    public class TestHelper
    {
        public static IConfigurationRoot Configuration = GetIConfigurationRoot(Environment.CurrentDirectory);
        internal static Mock<DbSet<T>> GetMockDbSet<T>(ICollection<T> entities) where T : class
        {
            var mockSet = new Mock<DbSet<T>>();
            mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(entities.AsQueryable().Provider);
            mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(entities.AsQueryable().Expression);
            mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(entities.AsQueryable().ElementType);
            mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(entities.AsQueryable().GetEnumerator());
            mockSet.Setup(m => m.Add(It.IsAny<T>())).Callback<T>(entities.Add);
            return mockSet;
        }

        public static IConfigurationRoot GetIConfigurationRoot(string outputPath)
        {
            return new ConfigurationBuilder()
                .SetBasePath(outputPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{EnvironmentName.Development}.json", optional: true)
                .Build();
        }
        /// <summary>
        /// Get app db context
        /// </summary>
        /// <returns></returns>
        public static AppDbContext GetAppDbContext()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString())
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .EnableSensitiveDataLogging()
                .Options;
            return new AppDbContext(options);
        }

        /// <summary>
        /// Get app db context
        /// </summary>
        /// <returns></returns>
        public static DbContextOptionsBuilder GetDbContextOptionsBuilderOptions()
        {
            var builder = new DbContextOptionsBuilder<AppDbContext>().UseInMemoryDatabase(Guid.NewGuid().ToString())
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .EnableSensitiveDataLogging();
            return builder;
        }


        /// <summary>
        /// Get queue connection
        /// </summary>
        /// <returns></returns>
        public static IConnection GetQueueConnection()
        {
            return new FakeConnection(new RabbitServer());
        }

        /// <summary>
        /// Get test httpcontext for authorize
        /// </summary>
        /// <returns></returns>
        public static DefaultHttpContext GetTestHttpContext()
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, "phong.ld@3si.vn"),
                new Claim(Constants.ClaimName.AccountId, "1"),
                new Claim(Constants.ClaimName.CompanyId, "1"),
                new Claim(Constants.ClaimName.CompanyCode, "d000001"),
                new Claim(Constants.ClaimName.CompanyName, "duali"),
                new Claim(Constants.ClaimName.AccountType, "1")
            };
            var appIdentity = new ClaimsIdentity(claims);

            return new DefaultHttpContext { User = new ClaimsPrincipal(appIdentity) }; // or mock a `HttpContext`
        }

        public static List<User> GetTestUsers()
        {
            var department = new Department
            {
                Id = 1,
                DepartName = "Duali"
            };
            var company = new Company
            {
                Id = 1,
                Name = "Duali",
                Contact = "duali@gmail.com"
            };
            var company2 = new Company
            {
                Id = 2,
                Name = "Duali",
                Contact = "duali@gmail.com"
            };
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    CompanyId = 1,
                    AccessGroupId = 1,
                    //UserCode = 1,
                    //CardId = "A",
                    KeyPadPw = Encryptor.Encrypt("123", Configuration[Constants.Settings.EncryptKey]),
                    ExpiredDate = DateTime.Now.AddMonths(12),
                    Department = department,
                    Company = company,
                    //CardStatus = (short)CardStatus.Normal
                },
                new User
                {
                    Id = 2,
                    CompanyId = 1,
                    AccessGroupId = 2,
                    //UserCode = 2,
                   // CardId = "B",
                    KeyPadPw = Encryptor.Encrypt("1234", Configuration[Constants.Settings.EncryptKey]),
                    ExpiredDate = null,
                    Department = department,
                    Company = company
                },
                new User
                {
                    Id = 3,
                    CompanyId = 1,
                    AccessGroupId = 3,
                    //UserCode = 3,
                    //CardId = "C",
                    KeyPadPw = Encryptor.Encrypt("12345", Configuration[Constants.Settings.EncryptKey]),
                    ExpiredDate = DateTime.Now.AddMonths(12),
                    Department = department,
                    Company = company
                },
                new User
                {
                    Id = 4,
                    CompanyId = 1,
                    AccessGroupId = 1,
                    //UserCode = 4,
                    //CardId = "D",
                    KeyPadPw = Encryptor.Encrypt("123456", Configuration[Constants.Settings.EncryptKey]),
                    ExpiredDate = DateTime.Now.AddDays(-1),
                    Department = department,
                    Company = company
                },
                new User
                {
                    Id = 5,
                    CompanyId = 2,
                    AccessGroupId = 1,
                    //UserCode = 1,
                    //CardId = "E",
                    KeyPadPw = Encryptor.Encrypt("1234567", Configuration[Constants.Settings.EncryptKey]),
                    ExpiredDate = DateTime.Now.AddMonths(12),
                    Department = department,
                    Company = company2
                },
            };
            return users;
        }
        public static List<IcuDevice> GetTestIcuDevice()
        {
            var company = new Company
            {
                Id = 1,
                Code = "p000001",
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                ExpiredFrom = DateTime.Now,
                ExpiredTo = DateTime.Now.AddDays(10),
                Name = "3si",
                IsDeleted = false,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                Timezone = GetTestTimezone(),

            };

            return new List<IcuDevice>()
            {
                new IcuDevice()
                {
                    Id=1,
                    BuzzerReader0=0,
                    BuzzerReader1=0,
                    CompanyId=1,
                    Status=(short)Status.Valid,
                    IpAddress="192.168.1.1",
                    DeviceAddress="0D9038",
                    MacAddress="192.168.1.1",
                    PassbackRule=1,
                    Company=company,
                    ConnectionStatus = (short)IcuStatus.Connected,
                    LastCommunicationTime = DateTime.Now,
                    RegisterIdNumber = 1,
                    FirmwareVersion = "0.0.0",
                    CloseReverseLockFlag = true,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    Name = "door1",
                    SensorType = 1,
                    LedReader0 = 1,
                    LedReader1 = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    MPRCount = 1,
                    OpenDuration = 1,
                    SensorDuration = 1,
                    PassageTzId = 1,
                    //AccessTz = GetTestTimezone().First(),
                },
                 new IcuDevice()
                {
                    Id=2,
                    BuzzerReader0=1,
                    BuzzerReader1=1,
                    CompanyId=1,
                    Status=(short)Status.Invalid,
                    IpAddress="192.168.1.1",
                    DeviceAddress="196E69",
                    MacAddress="192.168.1.1",
                    PassbackRule=2,
                    Company=company,
                    ConnectionStatus = (short)IcuStatus.Disconneted,
                    LastCommunicationTime = DateTime.Now,
                    RegisterIdNumber = 1,
                    FirmwareVersion = "0.0.0",
                    BuildingId = 2,
                    CloseReverseLockFlag = true,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    Name = "door2",
                    SensorType = 1,
                    LedReader0 = 1,
                    LedReader1 = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    MPRCount = 1,
                    OpenDuration = 1,
                    SensorDuration = 1,
                    PassageTzId = 1,
                },
                    new IcuDevice()
                {
                    Id=3,
                    BuzzerReader0=0,
                    BuzzerReader1=0,
                    CompanyId=1,
                    Status=(short)Status.Valid,
                    IpAddress="192.168.1.1",
                    DeviceAddress="196E69",
                    MacAddress="192.168.1.1",
                    PassbackRule=2,
                    Company=company,
                    ConnectionStatus = (short)IcuStatus.Disconneted,
                    LastCommunicationTime = DateTime.Now,
                    RegisterIdNumber = 1,
                    FirmwareVersion = "0.0.0",
                    CloseReverseLockFlag = true,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    Name = "door3",
                    SensorType = 1,
                    LedReader0 = 1,
                    LedReader1 = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    MPRCount = 1,
                    OpenDuration = 1,
                    SensorDuration = 1,
                    PassageTzId = 1
                },
                       new IcuDevice()
                {
                    Id=4,
                    BuzzerReader0=0,
                    BuzzerReader1=0,
                    CompanyId=2,
                    Status=(short)Status.Valid,
                    IpAddress="192.168.1.2",
                    DeviceAddress="196E69",
                    MacAddress="192.168.1.2",
                    PassbackRule=3,
                    Company=company,
                    ConnectionStatus = (short)IcuStatus.Disconneted,
                    LastCommunicationTime = DateTime.Now,
                    RegisterIdNumber = 1,
                    FirmwareVersion = "0.0.0",
                    CloseReverseLockFlag = true,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    Name = "door4",
                    SensorType = 1,
                    LedReader0 = 1,
                    LedReader1 = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    MPRCount = 1,
                    OpenDuration = 1,
                    SensorDuration = 1,
                    PassageTzId = 1
                },
                          new IcuDevice()
                {
                    Id=5,
                    BuzzerReader0=1,
                    BuzzerReader1=1,
                    CompanyId=1,
                    //Status=(short)Status.Deleted,
                    IpAddress="192.168.1.1",
                    DeviceAddress="196E69",
                    MacAddress="192.168.1.1",
                    PassbackRule=2,
                    Company=company,
                    ConnectionStatus = (short)IcuStatus.Disconneted,
                    LastCommunicationTime = DateTime.Now,
                    RegisterIdNumber = 1,
                    FirmwareVersion = "0.0.0",
                    CloseReverseLockFlag = true,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    Name = "door5",
                    SensorType = 1,
                    LedReader0 = 1,
                    LedReader1 = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    MPRCount = 1,
                    OpenDuration = 1,
                    SensorDuration = 1,
                    PassageTzId = 1,
                }
            };
        }
        public static List<Timezone> GetTestTimezone()
        {
            return new List<Timezone>()
            {
                new Timezone()
                {
                    Id=1,
                    Name="UTC",
                    Remarks="UTC",
                    CompanyId=1,
                    CreatedBy=1,
                    CreatedOn=DateTime.Now,
                    IsDeleted = false,
                    UpdatedBy=1,
                    UpdatedOn=DateTime.Now,
                    Position=3,
                    MonTime1="{'From':10,'To':2065}",
                    MonTime2="{'From':1,'To':306}",
                    MonTime3="{'From':120,'To':500}",
                    MonTime4="{'From':120,'To':1888}",
                    TueTime1="{'From':10,'To':30}",
                    TueTime2="{'From':10,'To':140}",
                    TueTime3="{'From':12,'To':140}",
                    TueTime4="{'From':120,'To':1888}",
                    WedTime1="{'From':10,'To':250}",
                    WedTime2="{'From':1,'To':45}",
                    WedTime3="{'From':2,'To':230}",
                    WedTime4="{'From':2,'To':1999}",
                    ThurTime1="{'From':3,'To':40}",
                    ThurTime2="{'From':4,'To':1540}",
                    ThurTime3="{'From':3,'To':140}",
                    ThurTime4="{'From':2,'To':1999}",
                    FriTime1="{'From':4,'To':25}",
                    FriTime2="{'From':6,'To':30}",
                    FriTime3="{'From':8,'To':230}",
                    FriTime4="{'From':2,'To':1999}",
                    SatTime1="{'From':10,'To':30}",
                    SatTime2="{'From':9,'To':140}",
                    SatTime3="{'From':12,'To':140}",
                    SatTime4="{'From':2,'To':1999}",
                    SunTime1="{'From':10,'To':20}",
                    SunTime2="{'From':1,'To':230}",
                    SunTime3="{'From':2,'To':2320}",
                    SunTime4="{'From':2,'To':2320}",
                    HolType1Time1="{'From':3,'To':230}",
                    HolType1Time2="{'From':4,'To':1430}",
                    HolType1Time3="{'From':3,'To':1401}",
                    HolType1Time4="{'From':3,'To':1401}",
                    HolType2Time1="{'From':4,'To':234}",
                    HolType2Time2="{'From':6,'To':340}",
                    HolType2Time3="{'From':8,'To':2301}",
                    HolType2Time4="{'From':8,'To':2301}",
                    HolType3Time1="{'From':10,'To':303}",
                    HolType3Time2="{'From':9,'To':1401}",
                    HolType3Time3="{'From':12,'To':124}",
                    HolType3Time4="{'From':12,'To':124}"

                },
                new Timezone()
                {
                    Id=2,
                    Name="DST",
                    Remarks="DST",
                    CompanyId=1,
                    CreatedBy=1,
                    CreatedOn=DateTime.Now,
                    IsDeleted = false,
                    UpdatedBy=1,
                    UpdatedOn=DateTime.Now,
                    Position=4,
                    MonTime1="{'From':10,'To':2065}",
                    MonTime2="{'From':1,'To':306}",
                    MonTime3="{'From':120,'To':500}",
                    MonTime4="{'From':120,'To':1888}",
                    TueTime1="{'From':10,'To':30}",
                    TueTime2="{'From':10,'To':140}",
                    TueTime3="{'From':12,'To':140}",
                    TueTime4="{'From':120,'To':1888}",
                    WedTime1="{'From':10,'To':250}",
                    WedTime2="{'From':1,'To':45}",
                    WedTime3="{'From':2,'To':230}",
                    WedTime4="{'From':2,'To':1999}",
                    ThurTime1="{'From':3,'To':40}",
                    ThurTime2="{'From':4,'To':1540}",
                    ThurTime3="{'From':3,'To':140}",
                    ThurTime4="{'From':2,'To':1999}",
                    FriTime1="{'From':4,'To':25}",
                    FriTime2="{'From':6,'To':30}",
                    FriTime3="{'From':8,'To':230}",
                    FriTime4="{'From':2,'To':1999}",
                    SatTime1="{'From':10,'To':30}",
                    SatTime2="{'From':9,'To':140}",
                    SatTime3="{'From':12,'To':140}",
                    SatTime4="{'From':2,'To':1999}",
                    SunTime1="{'From':10,'To':20}",
                    SunTime2="{'From':1,'To':230}",
                    SunTime3="{'From':2,'To':2320}",
                    SunTime4="{'From':2,'To':2320}",
                    HolType1Time1="{'From':3,'To':230}",
                    HolType1Time2="{'From':4,'To':1430}",
                    HolType1Time3="{'From':3,'To':1401}",
                    HolType1Time4="{'From':3,'To':1401}",
                    HolType2Time1="{'From':4,'To':234}",
                    HolType2Time2="{'From':6,'To':340}",
                    HolType2Time3="{'From':8,'To':2301}",
                    HolType2Time4="{'From':8,'To':2301}",
                    HolType3Time1="{'From':10,'To':303}",
                    HolType3Time2="{'From':9,'To':1401}",
                    HolType3Time3="{'From':12,'To':124}",
                    HolType3Time4="{'From':12,'To':124}",
                },
                new Timezone()
                {
                    Id=3,
                    Name="AFT",
                    Remarks="AFT",
                    CompanyId=2,
                    CreatedBy=1,
                    CreatedOn=DateTime.Now,
                    IsDeleted = false,
                    UpdatedBy=1,
                    UpdatedOn=DateTime.Now,
                    Position=5,
                },
                new Timezone()
                {
                    Id=4,
                    Name="AKDT",
                    Remarks="AKDT",
                    CompanyId=1,
                    CreatedBy=1,
                    CreatedOn=DateTime.Now,
                    IsDeleted = false,
                    UpdatedBy=1,
                    UpdatedOn=DateTime.Now,
                    Position=6,
                },
                new Timezone()
                {
                    Id=5,
                    Name="AKST",
                    Remarks="AKST",
                    CompanyId=2,
                    CreatedBy=1,
                    CreatedOn=DateTime.Now,
                    IsDeleted = false,
                    UpdatedBy=1,
                    UpdatedOn=DateTime.Now,
                    Position=7,
                }
            };
        }

        public static List<Account> GetTestAccounts()
        {
            var company = new Company()
            {
                Id = 1,
                Code = "p000001",
                CreatedBy = 1,
                CreatedOn = DateTime.Now.AddDays(1),
                ExpiredFrom = DateTime.Now.AddDays(2),
                ExpiredTo = DateTime.Now.AddDays(10),
                Name = "3si",
                IsDeleted = false,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                RootFlag = true
            };
            var accounts = new List<Account>
            {
                new Account
                {
                    Id = 1,
                    Username = "a@gmail.com",
                    Password="10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                    CompanyId = 1,
                    RootFlag = true,
                    IsDeleted = false,
                    Company=company,
                    Type = 2
                },
                new Account
                {
                    Id = 2,
                    Username = "b@gmail.com",
                    Password="10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                    CompanyId = 1,
                    RootFlag = false,
                    IsDeleted = false,
                    Company=company,
                    Type=2
                },
                new Account
                {
                    Id = 3,
                    Username = "c@gmail.com",
                    Password="10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                    CompanyId = 1,
                    RootFlag = false,
                    IsDeleted = true,
                    Company=company,
                    Type = 2
                },
                new Account
                {
                    Id = 4,
                    CompanyId = 2,
                    Username = "d@gmail.com",
                    Password="10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                    RootFlag = false,
                    IsDeleted = false,
                    Company=company,
                    Type=2
                },
                new Account
                {
                    Id = 5,
                    Username = "e@gmail.com",
                    Password="10000:1YnPljoOly3tCxoO7RR28e8L2WNHG9jDssMZP4wfL3dxurGM",
                    CompanyId =2,
                    RootFlag = false,
                    IsDeleted = true,
                    Company=company,
                    Type=2
                },
            };
            return accounts;
        }

        public static List<Setting> GetTestSettings()
        {
            return new List<Setting>()
            {
                new Setting
                {
                    Id=1,
                    Key= "notification_email",
                    Value = "[\"true\"]"
                },
                new Setting
                {
                    Id=2,
                    Key= "list_user_to_notification",
                    Value = "[\"\"]"
                },
                new Setting
                {
                    Id=3,
                    Key= "Key2",
                    Value = "[\"Value2\"]"
                },
                new Setting
                {
                    Id=4,
                    Key= "Key3",
                    Value = "[\"Value3\"]"
                },
                new Setting
                {
                    Id=5,
                    Key= "monitoring_event_type_default",
                    Value = "[\"Value4\"]"
                }
                ,
                new Setting
                {
                    Id=6,
                    Key= "monitoring_max_record_display",
                    Value = "[\"Value5\"]"
                }
            };
        }
        public static List<Department> GetTestDepartments()
        {
            return new List<Department>()
            {
                new Department
                {
                    Id = 1,
                    CompanyId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    DepartName= "Department name 1",
                    DepartNo= "2",
                    ParentId = 1,
                    IsDeleted = false,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now
                },
                new Department
                {
                    Id = 2,
                    CompanyId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    DepartName= "Department name 2",
                    DepartNo= "3",
                    ParentId = 1,
                    IsDeleted = false,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now
                },
                new Department
                {
                    Id = 3,
                    CompanyId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    DepartName= "Department name 3",
                    DepartNo= "4",
                    ParentId = 1,
                    IsDeleted = false,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now
                },
                new Department
                {
                    Id = 4,
                    CompanyId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    DepartName= "Department name 4",
                    DepartNo= "5",
                    ParentId = 1,
                    IsDeleted = false,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now
                },
                new Department
                {
                    Id = 5,
                    CompanyId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    DepartName= "Department name 6",
                    DepartNo= "6",
                    ParentId = 1,
                    IsDeleted = true,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now
                },
                new Department
                {
                    Id = 6,
                    CompanyId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    DepartName= "Department name 7",
                    DepartNo= "7",
                    ParentId = 1,
                    IsDeleted = false,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now
                }
            };
        }
        public static List<Holiday> GetTestHolidays()
        {
            var company = new Company()
            {
                Id = 1,
                Code = "p000001",
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                ExpiredFrom = DateTime.Now,
                ExpiredTo = DateTime.Now.AddDays(10),
                Name = "3si",
                IsDeleted = false,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                Timezone = GetTestTimezone()
            };

            return new List<Holiday>()
            {
                new Holiday()
                {
                    Id=1,
                    CompanyId=1,
                    CreatedBy=1,
                    CreatedOn = DateTime.Now,
                    EndDate = DateTime.Now,
                    Name = "Duali",
                    Recursive = true,
                    Remarks = "Duali",
                    StartDate = DateTime.Now,
                    IsDeleted = false,
                    Type = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Company=company,
                },
                new Holiday()
                {
                    Id=2,
                    CompanyId=1,
                    CreatedBy=1,
                    CreatedOn = DateTime.Now,
                    EndDate = DateTime.Now,
                    Name = "Duali",
                    Recursive = true,
                    Remarks = "Duali",
                    StartDate = DateTime.Now,
                    IsDeleted = false,
                    Type = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Company=company,
                },
                new Holiday()
                {
                    Id=3,
                    CompanyId=1,
                    CreatedBy=1,
                    CreatedOn = DateTime.Now,
                    EndDate = DateTime.Now,
                    Name = "Duali",
                    Recursive = true,
                    Remarks = "Duali",
                    StartDate = DateTime.Now,
                    IsDeleted = true,
                    Type = 1,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Company=company,
                }

            };
        }

        public static List<Building> GetTestBuildings()
        {

            return new List<Building>()
            {
                new Building()
                {
                    Id=1,
                    CompanyId = 1,
                    Name="Head Quarter",
                },
                new Building()
                {
                    Id=2,
                    CompanyId = 1,
                    Name="63 Building",
                },
                new Building()
                {
                    Id=3,
                    CompanyId = 1,
                    Name="73 Building",
                },
                new Building()
                {
                    Id=4,
                    CompanyId = 1,
                    Name="83 Building",
                },
                new Building()
                {
                    Id=5,
                    CompanyId = 1,
                    Name="93 Building",
                },
            };
        }

        public static List<AccessGroup> GetTestAccessGroups()
        {
            return new List<AccessGroup>()
            {
                new AccessGroup
                {
                    Id=1,
                    Name = "Full Access",
                    IsDeleted = false,
                    IsDefault = true,
                    CompanyId=1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Type = (short)AccessGroupType.FullAccess
                },
                new AccessGroup
                {
                    Id=2,
                    Name = "No Access",
                    IsDeleted = false,
                    IsDefault = false,
                    CompanyId=1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Type = (short)AccessGroupType.NoAccess

                },
                new AccessGroup
                {
                    Id=3,
                    Name = "Normal Access",
                    IsDeleted = false,
                    IsDefault = false,
                    CompanyId=1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Type = (short)AccessGroupType.NormalAccess

                },
                new AccessGroup
                {
                    Id=4,
                    Name = "Normal Access",
                    IsDeleted = true,
                    IsDefault = false,
                    CompanyId=1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                    Type = (short)AccessGroupType.NormalAccess
                }
            };
        }

        public static List<AccessGroupDevice> GetTestAccessGroupDevices()
        {
            return new List<AccessGroupDevice>()
            {
                new AccessGroupDevice
                {
                    AccessGroupId = 1,
                    IcuId = 1,
                    TzId = 1,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                },

                new AccessGroupDevice
                {
                    AccessGroupId = 1,
                    IcuId = 2,
                    TzId = 2,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                },
                new AccessGroupDevice
                {
                    AccessGroupId = 1,
                    IcuId = 3,
                    TzId = 3,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,

                }
            };
        }

        public static List<SystemLog> GetTestSystemLogs()
        {
            var company = new Company()
            {
                Id = 1,
                Code = "p000001",
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                ExpiredFrom = DateTime.Now,
                ExpiredTo = DateTime.Now.AddDays(10),
                Name = "3si",
                IsDeleted = false,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                Timezone = GetTestTimezone()
            };

            var account = new Account
            {
                Id = 1,
                Username = "a@gmail.com",
                Password = "10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                CompanyId = 1,
                RootFlag = true,
                IsDeleted = false,
                Company = company,
                Type = 2
            };

            return new List<SystemLog>()
            {
                new SystemLog()
                {
                    Id=1,
                    Action=1,
                    CompanyId=1,
                    Content="test1",
                    ContentDetails="ContentDetails1",
                    ContentIds="{\"Id\":1,\"assigned_ids\":null}",
                    CreatedBy=1,
                    OpeTime = DateTime.Now,
                    Type = 1,
                    Company=company,
                    CreatedByNavigation=account,
                },
                new SystemLog()
                {
                    Id=2,
                    Action=1,
                    CompanyId=1,
                    Content="test2",
                    ContentDetails="ContentDetails3",
                    ContentIds="{\"Id\":2,\"assigned_ids\":null}",
                    CreatedBy=1,
                    OpeTime = DateTime.Now,
                    Type = 1,
                    Company=company,
                    CreatedByNavigation=account,
                },
                new SystemLog()
                {
                    Id=3,
                    Action=1,
                    CompanyId=1,
                    Content="test3",
                    ContentDetails="ContentDetails3",
                    ContentIds="{\"Id\":3,\"assigned_ids\":null}",
                    CreatedBy=1,
                    OpeTime = DateTime.Now,
                    Type = 1,
                    Company=company,
                    CreatedByNavigation=account,
                }
            };
        }

        public static List<UnregistedDevice> GetTestUnregistedDevices()
        {
            return new List<UnregistedDevice>
            {
                new UnregistedDevice
                {
                    Id = 1,
                    DeviceAddress = "6D178E",
                    Status = 0,
                    IpAddress = "192.168.1.1",
                    CompanyId = Constants.DefaultCompanyId,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                },
                new UnregistedDevice
                {
                    Id = 1,
                    DeviceAddress = "002FBE",
                    Status = 0,
                    IpAddress = "192.168.1.1",
                    CompanyId = Constants.DefaultCompanyId,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 1,
                    UpdatedOn = DateTime.Now,
                }
            };
        }

        public static List<DeviceMessage> GetTestDeviceMessages()
        {
            return new List<DeviceMessage>
            {
                new DeviceMessage
                {
                    Id = 1,
                    MessageId = 1,
                    Content = "Welcome!",
                    Remark = "Default Message",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 2,
                    MessageId = 2,
                    Content = "Thank you",
                    Remark = "Default Message",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 3,
                    MessageId = 3,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 4,
                    MessageId = 4,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 5,
                    MessageId = 5,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 6,
                    MessageId = 6,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 7,
                    MessageId = 7,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 8,
                    MessageId = 8,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 9,
                    MessageId = 9,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
                new DeviceMessage
                {
                    Id = 10,
                    MessageId = 10,
                    Content = "",
                    Remark = "",
                    CreatedOn = DateTime.Now,
                    UpdatedOn = DateTime.Now,
                    CompanyId = Constants.DefaultCompanyId,
                },
            };
        }
    }
}

