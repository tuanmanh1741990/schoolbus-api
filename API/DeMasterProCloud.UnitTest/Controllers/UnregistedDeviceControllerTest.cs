﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class UnregistedDeviceControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly Mock<IUnregistedDeviceService> _mockUnregistedDeviceService;
        private readonly List<UnregistedDevice> _unregistedDevices;
        public UnregistedDeviceControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockUnregistedDeviceService = new Mock<IUnregistedDeviceService>();

            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            _unregistedDevices = TestHelper.GetTestUnregistedDevices();

            //Initial mapping
            AutoMapperConfig.Initialize();
        }

        [Fact]
        public void Gets_ListAccessGroup_Return_OkObjectResult()
        {
            var unRegistedDeviceController = new UnregistedDeviceController(_mockUnregistedDeviceService.Object);
            var result = unRegistedDeviceController.GetUnregisterDevices(null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Add_MissingDevice_Return_SuccessResult()
        {
            _mockUnregistedDeviceService.Setup(x => x.GetByCompanyId()).Returns(_unregistedDevices);
            var unRegistedDeviceController = new UnregistedDeviceController(_mockUnregistedDeviceService.Object);
            var result = unRegistedDeviceController.AddMissingDevice();
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Add_MissingDevice_Return_NullObject()
        {
            var unRegistedDeviceController = new UnregistedDeviceController(_mockUnregistedDeviceService.Object);
            var result = unRegistedDeviceController.AddMissingDevice();
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, objectResult.StatusCode);
        }
    }
}
