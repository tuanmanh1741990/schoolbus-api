using Bogus;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Login;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Linq;
using DeMasterProCloud.DataModel.Account;
using Xunit;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using AutoMapper;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Options;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class AccountControllerTest
    {
        private readonly Mock<ICompanyService> _mockCompanyService;
        private readonly Mock<IAccountService> _mockAccountService;
        private readonly Mock<IHttpContextAccessor> mockHttpContext;
        private readonly AccountController _accountController;
        private readonly LoginModel _loginModel;
        private readonly Account _account;
        private readonly Company _company;
        private readonly Mock<IOptions<JwtOptionsModel>> _option;
        public AccountControllerTest()
        {
            _mockCompanyService = new Mock<ICompanyService>();
            _mockAccountService = new Mock<IAccountService>();
            var mockJwtService = new Mock<IJwtHandler>();
            mockHttpContext = new Mock<IHttpContextAccessor>();
            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            _accountController = new AccountController(_mockCompanyService.Object, _mockAccountService.Object,
                mockJwtService.Object, mockHttpContext.Object, _option.Object);

            var testLogin = new Faker<LoginModel>()
                //.RuleFor(u => u.CompanyCode, f => $"a{f.Random.Replace("######")}")
                .RuleFor(u => u.Username, (f, u) => f.Internet.Email(u.Username))
                .RuleFor(u => u.Password, (f, u) => f.Internet.Password(6));
            _loginModel = testLogin.Generate();

            var accountIds = 1;
            var testAccount = new Faker<Account>()
                .RuleFor(u => u.Id, f => accountIds++)
                .RuleFor(u => u.Username, f => _loginModel.Username)
                .RuleFor(u => u.Password, f => _loginModel.Password)
                .RuleFor(u => u.Type, f => (short)f.Random.Number(1, 3));
            _account = testAccount.Generate();

            var companyIds = 1;
            var testCompany = new Faker<Company>()
                .RuleFor(u => u.Id, f => companyIds++)
                //.RuleFor(u => u.Code, f => _loginModel.CompanyCode)
                .RuleFor(u => u.Name, f => f.Lorem.Sentence())
                .RuleFor(u => u.ExpiredFrom, f => DateTime.Now)
                .RuleFor(u => u.ExpiredTo, f => DateTime.Now.AddMonths(12));
            _company = testCompany.Generate();
            _account.Company = _company;
            AutoMapperConfig.Initialize();

        }

        [Fact]
        public void Check_Login_Return_CompanyNotExist()
        {
            _mockCompanyService.Setup(x => x.IsExistCompanyCode(
                It.Is<string>(i => i == "b000001")
            )).Returns(true);

            // Act
            var result = _accountController.Login(_loginModel);
            var failedResult = Assert.IsType<ApiUnauthorizedResult>(result);

            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal((int)LoginUnauthorized.CompanyNonExist, errorResult?.StatusCode);
        }

        [Fact]
        public void Check_Login_Return_ValidationFailedResult()
        {
            var accountModel = new LoginModel();
            _accountController.ModelState.AddModelError("error", "Account is not valid");
            var result = _accountController.Login(accountModel);
            var objectResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        }

        [Fact]
        public void Check_Login_Return_AccountNotExist()
        {
            //_mockCompanyService.Setup(x => x.IsExistCompanyCode(
            //    It.Is<string>(i => i == _loginModel.CompanyCode)
            //)).Returns(true);

            var failLogin = new Faker<LoginModel>()
                //.RuleFor(u => u.CompanyCode, f => _company.Code)
                .RuleFor(u => u.Username, (f, u) => f.Internet.Email(u.Username))
                .RuleFor(u => u.Password, (f, u) => f.Internet.Password(6));
            var failLoginModel = failLogin.Generate();

            _mockAccountService.Setup(x => x.GetAuthenticatedAccount(
                It.Is<LoginModel>(i => i == failLoginModel)
            ));

            // Act
            var result = _accountController.Login(_loginModel);
            var failedResult = Assert.IsType<ApiUnauthorizedResult>(result);

            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal((int)LoginUnauthorized.InvalidCredentials, errorResult?.StatusCode);
        }

        [Fact]
        public void Check_Login_Return_ExpiredCompany()
        {
            //_mockCompanyService.Setup(x => x.IsExistCompanyCode(
            //    It.Is<string>(i => i == _loginModel.CompanyCode)
            //)).Returns(true);

            _mockAccountService.Setup(x => x.GetAuthenticatedAccount(
                It.Is<LoginModel>(i => i == _loginModel)
            )).Returns(_account);

            var failCompany = new Faker<Company>()
                .RuleFor(u => u.Id, f => _company.Id)
                .RuleFor(u => u.Code, f => _company.Code)
                .RuleFor(u => u.Name, f => _company.Name)
                .RuleFor(u => u.ExpiredFrom, f => DateTime.Now.AddMonths(-12))
                .RuleFor(u => u.ExpiredTo, f => DateTime.Now.AddDays(-1));
            var company = failCompany.Generate();

            _mockCompanyService.Setup(x => x.IsValidCompany(
                It.Is<Company>(i => i == company)
            )).Returns(true);

            // Act
            var result = _accountController.Login(_loginModel);
            var failedResult = Assert.IsType<ApiUnauthorizedResult>(result);

            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal((int)LoginUnauthorized.CompanyExpired, errorResult?.StatusCode);
        }

        [Fact]
        public void Check_Login_Return_Account_Invalid()
        {
            //_mockCompanyService.Setup(x => x.IsExistCompanyCode(
            //    It.Is<string>(i => i == _loginModel.CompanyCode)
            //)).Returns(true);

            _mockAccountService.Setup(x => x.GetAuthenticatedAccount(
                It.Is<LoginModel>(i => i == _loginModel)
            )).Returns(_account);

            _mockCompanyService.Setup(x => x.IsValidCompany(
                It.Is<Company>(i => i == _account.Company)
            )).Returns(true);

            _account.IsDeleted = true;
            // Act
            var result = _accountController.Login(_loginModel);
            var failedResult = Assert.IsType<ApiUnauthorizedResult>(result);

            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal((int)LoginUnauthorized.InvalidCredentials, errorResult?.StatusCode);
        }

        [Fact]
        public void Check_Login_Return_OkObjectResult()
        {
            //_mockCompanyService.Setup(x => x.IsExistCompanyCode(
            //    It.Is<string>(i => i == _loginModel.CompanyCode)
            //)).Returns(true);

            _mockAccountService.Setup(x => x.GetAuthenticatedAccount(
                It.Is<LoginModel>(i => i == _loginModel)
            )).Returns(_account);

            _mockCompanyService.Setup(x => x.IsValidCompany(
                It.Is<Company>(i => i == _account.Company)
            )).Returns(true);

            // Act
            var result = _accountController.Login(_loginModel);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void Get_ById_Return_ReturnAccountDataModel()
        {
            var accountTestId = 1;
            _mockAccountService.Setup(x => x.GetById(accountTestId)).Returns(_account);
            var result = _accountController.Get(accountTestId);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var successResult = objectResult.Value as AccountDataModel;
            Assert.True(successResult?.Id == 1 && successResult.Username != null);
        }

        [Fact]
        public void Get_ById_Return_ApiErrorResult()
        {
            var accountTestId = 1;
            var account = new Account(); ;
            var result = _accountController.Get(accountTestId);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }
        [Fact]
        public void Get_Return_ApiErrorResult()
        {
            var accountTestId = 1;
            var account = new Account(); ;
            var result = _accountController.Get(accountTestId);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ById_ReturnInitDataOnly()
        {
            var newAccountId = 0;
            var result = _accountController.Get(newAccountId);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            var successResult = objectResult.Value as AccountDataModel;
            Assert.True(successResult?.Id == 0 && successResult.Username == null);
        }

        [Fact]
        public void Get_ListAccount_Success()
        {
            var result = _accountController.Get(search: null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
        [Fact]
        public void Add_Account_Return_ApiSuccessResult()
        {
            var accountModel = new AccountModel()
            {
                Username = "phuongngoc",
                Password = "123456789",
                //Status = (short)Status.Valid,
                //CompanyCode = "p000001",
                Role = 1,
            };
            var result = _accountController.Add(accountModel);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact]
        public void Add_Account_Return_ValidationFailedResult()
        {
            var accountModel = new AccountModel()
            {
                //Status = (short)Status.Valid,
                //CompanyCode = "p000001",
                Role = 1,
            };
            _accountController.ModelState.AddModelError("error", "Account is not valid");
            var result = _accountController.Add(accountModel);
            var objectResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_Account_Return_ValidationFailedResult()
        {
            var account = TestHelper.GetTestAccounts().First();
            var accountModel = Mapper.Map<AccountModel>(account);
            accountModel.Password = account.Password;
            accountModel.Username = "phuongcn";
            //accountModel.Status = (short)Status.Invalid;
            var mockJwtService = new Mock<IJwtHandler>();
            Mock<AccountController> accountController = new Mock<AccountController>(_mockCompanyService.Object, _mockAccountService.Object,
            mockJwtService.Object, mockHttpContext.Object);
            accountController.Setup(x => x.TryValidateModel(account)).Returns(true);
            var result = accountController.Object.Edit(accountModel.Id, accountModel);

            var objectResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_Account_Return_ApiErrorResult()
        {
            var id = 1;
            var model = Mapper.Map<AccountModel>(TestHelper.GetTestAccounts().First());
            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                              It.IsAny<ValidationStateDictionary>(),
                                              It.IsAny<string>(),
                                              It.IsAny<Object>()));

            _accountController.ObjectValidator = objectValidator.Object;
            var result = _accountController.Edit(id, model);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_Account_Return_ApiSuccessResult()
        {
            var id = 1;
            var model = Mapper.Map<AccountModel>(TestHelper.GetTestAccounts().First());
            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                              It.IsAny<ValidationStateDictionary>(),
                                              It.IsAny<string>(),
                                              It.IsAny<Object>()));

            _accountController.ObjectValidator = objectValidator.Object;
            _mockAccountService.Setup(x => x.GetById(id)).Returns(_account);
            var result = _accountController.Edit(id, model);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
        [Fact]
        public void Delete_Account_Return_ApiErrorResult()
        {
            var id = 1;
            var result = _accountController.Delete(id);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void DeleteAccount_Return_Status403Forbidden()
        {
            var id = 1;
            _mockAccountService.Setup(x => x.GetAccountByCurrentCompany(1)).Returns(TestHelper.GetTestAccounts().First);
            var result = _accountController.Delete(id);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status403Forbidden, objectResult.StatusCode);
        }

        [Fact]
        public void DeleteAccount_Return_ApiSuccessResult()
        {
            var id = 1;
            _mockAccountService.Setup(x => x.GetAccountByCurrentCompany(1)).Returns(TestHelper.GetTestAccounts().First);
            _mockAccountService.Setup(x => x.IsAllowDelete(It.IsAny<Account>(), It.IsAny<Account>())).Returns(true);
            var result = _accountController.Delete(id);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_Multiple_Account_Return_ApiErrorResult()
        {
            var accounts = new List<Account>();
            var ids = new List<int>() { 1, 2, 3, };
            _mockAccountService.Setup(x => x.GetAccountsByIds(ids, 1)).Returns(accounts);
            var result = _accountController.DeleteMultiple(ids);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_Multiple_Account_Return_Status403Forbidden()
        {
            var ids = new List<int>() { 1, 2, 3, };
            _mockAccountService.Setup(x => x.GetAccountsByIds(ids, 1)).Returns(TestHelper.GetTestAccounts());
            var result = _accountController.DeleteMultiple(ids);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status403Forbidden, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_Multiple_Account_Return_ApiSuccessResult()
        {
            var ids = new List<int>() { 1, 2, 3, };
            _mockAccountService.Setup(x => x.GetAccountsByIds(ids, 1)).Returns(TestHelper.GetTestAccounts());
            _mockAccountService.Setup(x => x.IsAllowDelete(It.IsAny<Account>(), It.IsAny<Account>())).Returns(true);
            var result = _accountController.DeleteMultiple(ids);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
    }
}
