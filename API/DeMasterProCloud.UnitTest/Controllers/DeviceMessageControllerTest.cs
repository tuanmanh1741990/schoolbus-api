﻿using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class DeviceMessageControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IDeviceMessageService> _mockDeviceMessageService;
        private readonly DeviceMessageController _deviceMessageController;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;

        private readonly List<DeviceMessage> _deviceMessages;
        //private readonly MapperConfigurationExpression _mapperConfiguration;

        public DeviceMessageControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //_devices = TestHelper.GetTestIcuDevice();
            _deviceMessages = TestHelper.GetTestDeviceMessages();
            var mockSet = TestHelper.GetMockDbSet(_deviceMessages);
            _mockDeviceMessageService = new Mock<IDeviceMessageService>();

            _deviceMessageController = new DeviceMessageController(_configuration, _mockDeviceMessageService.Object, _mockHttpContext.Object);
            //Initial mapping
            AutoMapperConfig.Initialize();
        }

        [Fact]
        public void Get_Test()
        {
            _mockDeviceMessageService.Setup(x => x.GetByIdAndCompany(It.Is<int>(i => i == 1), It.Is<int>(i => i == 1))).Returns(_deviceMessages.First());
            var result = _deviceMessageController.Get();
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        //[Fact]
        //public void Get_ListIcuDevice_Return_ApiErrorResult()
        //{
        //    var result = _deviceController.Get(100);
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Get_ById_Return_ApiErrorResult()
        //{
        //    var result = _deviceController.Get(100);
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Get_ById_Return_OkObjectResult()
        //{
        //    _mockDeviceService.Setup(x => x.GetByIdAndCompany(It.Is<int>(i => i == 1), It.Is<int>(i => i == 1))).Returns(_devices.First());
        //    var result = _deviceController.Get(id: 1);
        //    var objectResult = Assert.IsType<OkObjectResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Add_IcuDevice_Return_ApiSuccessResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object, _mockHttpContext.Object);

        //    var deviceModel = Mapper.Map<DeviceModel>(TestHelper.GetTestIcuDevice().First());
        //    var result = deviceController.Object.Add(deviceModel);
        //    var objectResult = Assert.IsType<ApiSuccessResult>(result);
        //    Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Add_IcuDevice_Return_ValidationFailedResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object, _mockHttpContext.Object);

        //    var deviceModel = Mapper.Map<DeviceModel>(TestHelper.GetTestIcuDevice().First());
        //    deviceController.Object.ModelState.AddModelError("CompanyCode", "CompanyId is require");
        //    var result = deviceController.Object.Add(deviceModel);
        //    var objectResult = Assert.IsType<ValidationFailedResult>(result);
        //    Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);

        //}

        //[Fact]
        //public void Edit_IcuDevice_Return_ValidationFailedResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);
        //    var deviceModel = Mapper.Map<DeviceModel>(TestHelper.GetTestIcuDevice().First());

        //    var result = deviceController.Object.Edit(TestHelper.GetTestIcuDevice().First().Id, deviceModel);
        //    var objectResult = Assert.IsType<ValidationFailedResult>(result);
        //    Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Edit_IcuDevice_Return_ApiErrorResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);
        //    var deviceModel = Mapper.Map<DeviceModel>(TestHelper.GetTestIcuDevice().First());

        //    deviceController.Setup(x => x.TryValidateModel(It.IsAny<DeviceModel>())).Returns(true);
        //    var result = deviceController.Object.Edit(TestHelper.GetTestIcuDevice().First().Id, deviceModel);
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Edit_IcuDevice_Return_ApiSuccessResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //       _mockHttpContext.Object);
        //    var deviceModel = Mapper.Map<DeviceModel>(TestHelper.GetTestIcuDevice().First());
        //    deviceController.Setup(x => x.TryValidateModel(It.IsAny<DeviceModel>())).Returns(true);
        //    _mockDeviceService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(_devices.First());
        //    var result = deviceController.Object.Edit(TestHelper.GetTestIcuDevice().First().Id, deviceModel);
        //    var objectResult = Assert.IsType<ApiSuccessResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        //}

        //[Fact]
        //public void Delete_IcuDevice_Return_ApiErrorResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);

        //    var result = deviceController.Object.Delete(1);
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Delete_IcuDevice_Return_ApiSuccessResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);

        //    _mockDeviceService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(_devices.First());
        //    var result = deviceController.Object.Delete(_devices.First().Id);
        //    var objectResult = Assert.IsType<ApiSuccessResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        //}

        //[Fact]
        //public void DeleteMultipleIcuDevice_Return_ApiErrorResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);

        //    _mockDeviceService.Setup(x => x.GetByIdsAndCompany(new List<int>() { 10, 11 }, It.Is<int>(j => j == 1))).Returns(_devices.Take(0).ToList());
        //    var result = deviceController.Object.DeleteMultiple(new List<int>() { 10, 11 });
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        //}

        //[Fact]
        //public void DeleteMultipleIcuDevice_Return_ApiSuccessResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);

        //    _mockDeviceService.Setup(x => x.GetByIdsAndCompany(new List<int>() { 1, 2 }, It.Is<int>(j => j == 1))).Returns(_devices);
        //    var result = deviceController.Object.DeleteMultiple(new List<int>() { 1, 2 });
        //    var objectResult = Assert.IsType<ApiSuccessResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Check_ToggleStatus_Return_ApiErrorResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);

        //    var result = deviceController.Object.ToggleStatus(10);
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        //}

        //[Fact]
        //public void Check_ToggleStatus_Return_ApiSuccessResult()
        //{
        //    Mock<DeviceController> deviceController = new Mock<DeviceController>(_configuration, _mockDeviceService.Object,
        //      _mockHttpContext.Object);

        //    _mockDeviceService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(_devices.First());
        //    var result = deviceController.Object.ToggleStatus(1);
        //    var objectResult = Assert.IsType<ApiSuccessResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        //}

        ////[Theory]
        ////[InlineData(1)]
        ////[InlineData(-1)]
        ////[InlineData(2)]
        ////public void Get_Condition_Test_Return_OkObjectResult(int mprId)
        ////{
        ////   _mockMprService.Setup(x => x.GetCountByMprId(1, 1)).Returns(0);
        ////    var result = _deviceController.GetCondition(mprId);
        ////    var objectResult = Assert.IsType<OkObjectResult>(result);
        ////    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        ////}

        ////[Fact]
        ////public void Get_Condition_Test_Return_OkObjectResult2()
        ////{
        ////    _mockMprService.Setup(x => x.GetCountByMprId(1, 2)).Returns(11);
        ////    var result = _deviceController.GetCondition(2);
        ////    var objectResult = Assert.IsType<OkObjectResult>(result);
        ////    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        ////}
        //[Theory]
        //[InlineData(0)]
        //[InlineData(1)]
        //public void Check_CreateTestData_Return_OkObjectResult(int numberOfDevice)
        //{
        //    var result = _deviceController.CreateTestData(numberOfDevice);
        //    var objectResult = Assert.IsType<OkObjectResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        //}

        ////[Fact]
        ////public void GetDeviceInfoResponse()
        ////{
        ////    var deviceAddress = "196E69";
        ////    var result = _deviceController.GetDeviceInfoResponse(deviceAddress);
        ////    var objectResult = Assert.IsType<ApiSuccessResult>(result);
        ////    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        ////}
    }
}
