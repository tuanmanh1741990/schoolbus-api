﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class DepartmentControllerTest
    {
        private readonly Mock<IDepartmentService> _mockDepartmentService;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly List<Department> _departments;

        public DepartmentControllerTest()
        {
            _mockDepartmentService = new Mock<IDepartmentService>();
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _departments = TestHelper.GetTestDepartments();
            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock HttpContext
            var httpContext = TestHelper.GetTestHttpContext();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(httpContext);
        }

        [Fact]
        public void GetListDepartment_Return200Ok()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var actionResult = departmentController.Get(null);
            var okObjectResult = Assert.IsType<OkObjectResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, okObjectResult.StatusCode);
        }

        [Fact]
        public void GetById_ReturnDepartmentModel()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            _mockDepartmentService.Setup(x => x.GetByIdAndCompany(It.Is<int>(i => i == 1), It.Is<int>(i => i == 1))).Returns(_departments.First());
            var result = departmentController.Get(1);
            var okObjectResult = Assert.IsType<OkObjectResult>(result);
            Assert.True(okObjectResult.Value is DepartmentModel);
        }

        [Fact]
        public void GetById_Return404()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var result = departmentController.Get(10);
            var failedResult = Assert.IsType<ApiErrorResult>(result);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            // ReSharper disable once PossibleNullReferenceException
            Assert.Equal(StatusCodes.Status404NotFound, errorResult.StatusCode);
        }

        [Fact]
        public void Add_ReturnValidationFail()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var model = Mapper.Map<DepartmentModel>(_departments.First());
            departmentController.ModelState.AddModelError("DepartmentCode", "Existed department");
            var result = departmentController.Add(model);
            var validationFailedResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, validationFailedResult.StatusCode);
        }

        [Fact]
        public void Add_ReturnOkObjectResult()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var departmentModel = Mapper.Map<DepartmentModel>(_departments.First());
            var result = departmentController.Add(departmentModel);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status201Created, okObjectResult.StatusCode);
        }

        [Fact]
        public void Edit_ReturnValidationFail()
        {
            var mockDepartmentController = new Mock<DepartmentController>(_mockDepartmentService.Object, _mockHttpContext.Object);
            mockDepartmentController.Setup(x => x.TryValidateModel(It.IsAny<DepartmentModel>())).Returns(false);
            var model = Mapper.Map<DepartmentModel>(_departments.First());
            var result = mockDepartmentController.Object.Edit(2, model);
            var validationFailedResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, validationFailedResult.StatusCode);
        }

        [Fact]
        public void Edit_ReturnNotFoundDepartment()
        {
            var mockDepartmentController = new Mock<DepartmentController>(_mockDepartmentService.Object, _mockHttpContext.Object);
            var model = Mapper.Map<DepartmentModel>(TestHelper.GetTestDepartments().First());
            mockDepartmentController.Setup(x => x.TryValidateModel(It.IsAny<DepartmentModel>())).Returns(true);
            var result = mockDepartmentController.Object.Edit(1, model);
            var errorObjectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, errorObjectResult.StatusCode);
        }

        [Fact]
        public void Edit_ReturnOkObjectResult()
        {
            _mockDepartmentService.Setup(x => x.GetByIdAndCompany(It.IsAny<int>(), It.IsAny<int>())).Returns(_departments.First());
            var mockDepartmentController = new Mock<DepartmentController>(_mockDepartmentService.Object, _mockHttpContext.Object);
            mockDepartmentController.Setup(x => x.TryValidateModel(It.IsAny<DepartmentModel>())).Returns(true);
            var model = Mapper.Map<DepartmentModel>(_departments.First());
            var result = mockDepartmentController.Object.Edit(_departments.First().Id, model);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, okObjectResult.StatusCode);
        }

        [Fact]
        public void Delete_ReturnNotFoundDepartment()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var result = departmentController.Delete(_departments.First().Id);
            var errorObjectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, errorObjectResult.StatusCode);
        }

        [Fact]
        public void Delete_ReturnOkObjectResult()
        {
            var departmentController = new Mock<DepartmentController>(_mockDepartmentService.Object, _mockHttpContext.Object);
            _mockDepartmentService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(_departments.First());
            var result2 = departmentController.Object.Delete(_departments.First().Id);
            var objectResult2 = Assert.IsType<ApiSuccessResult>(result2);
            Assert.Equal(StatusCodes.Status200OK, objectResult2.StatusCode);
        }

        [Fact]
        public void DeleteMultiple_ReturnNotFoundUser()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var result = departmentController.DeleteMultiple(new List<int> { 9, 10 });
            var errorObjectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, errorObjectResult.StatusCode);
        }

        [Fact]
        public void DeleteMultiple_ReturnOkObjectResult()
        {
            var departmentIds = _departments.Select(x => x.Id).ToList();
            _mockDepartmentService.Setup(x => x.GetByIdsAndCompany(It.IsAny<List<int>>(), It.IsAny<int>())).Returns(_departments);
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var result = departmentController.DeleteMultiple(departmentIds);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, okObjectResult.StatusCode);
        }

        [Fact]
        public void GetListDepartment_Test()
        {
            var departmentController = new DepartmentController(_mockDepartmentService.Object, _mockHttpContext.Object);
            var result = departmentController.GetListDepartment();
            var okObjectResult = Assert.IsType<JsonResult>(result);
            Assert.True(okObjectResult.Value is IEnumerable<Node>);
        }
    }
}
