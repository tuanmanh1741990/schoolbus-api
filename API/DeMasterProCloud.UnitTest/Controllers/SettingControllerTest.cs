using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class SettingControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly Mock<ISettingService> _mockSettingService;
        private readonly List<Setting> _settings;

        public SettingControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockSettingService = new Mock<ISettingService>();
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _settings = TestHelper.GetTestSettings();

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock HttpContext
            var httpContext = TestHelper.GetTestHttpContext();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(httpContext);
        }

        [Fact]
        public void Get_ReturnOkObjectResult()
        {
            var fileSettings = new List<FileSetting>
            {
                new FileSetting
                {
                    Category = "SettingControllerTest",
                    Key = "SettingControllerTestKey",
                    Values = new[] {"1"},
                },
                new FileSetting
                {
                    Category = "SettingControllerTest",
                    Key = "SettingControllerTestKey1",
                    Values = new[] {"2"},
                },
            };
            var companyId = 1;

            _mockSettingService.Setup(x => x.GetAll(companyId)).Returns(fileSettings);
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.Get();
            var okObjectResult = Assert.IsType<OkObjectResult>(actionResult);
            var result = okObjectResult.Value as List<SettingByCategoryModel>;
            Assert.True(result?.Count == 1 && result?.First().Category == "SettingControllerTest");
        }

        [Fact]
        public void Get_Id_ReturnOkObjectResult()
        {
            var setting = _settings.First();
            _mockSettingService.Setup(x => x.GetById(It.IsAny<int>())).Returns(setting);
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.Get(setting.Id);
            var okObjectResult = Assert.IsType<OkObjectResult>(actionResult);
            var result = okObjectResult.Value as SettingEditModel;
            Assert.True(result?.Id == setting.Id);
        }

        [Fact]
        public void Get_Id_ReturnNotFound()
        {
            var notFoundId = int.MinValue;
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.Get(notFoundId);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorResult?.StatusCode);
        }
        [Fact]
        public void Edit_SettingModel_ReturnSuccess()
        {
            var setting = _settings.First();
            _mockSettingService.Setup(x => x.GetById(It.IsAny<int>())).Returns(setting);

            var settingModel = new SettingModel
            {
                Id = setting.Id,
                Key = setting.Key,
                Value = new List<string> { "NewValue" }
            };
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.Edit(setting.Id, settingModel);
            Assert.IsType<ApiSuccessResult>(actionResult);
        }

        [Fact]
        public void Edit_SettingModel_ReturnNotFound()
        {
            var settingModel = new SettingModel
            {
                Id = 1,
                Key = "Key",
                Value = new List<string> { "NewValue" }
            };
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.Edit(settingModel.Id, settingModel);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorResult?.StatusCode);
        }

        [Fact]
        public void EditMultiple_ListSettingModel_ReturnSuccess()
        {
            var settingModels = new List<SettingModel>
            {
                new SettingModel
                {
                    Id = 1,
                    Key = "Key",
                    Value = new List<string> { "NewValue" }
                },
                new SettingModel
                {
                    Id = 2,
                    Key = "Key1",
                    Value = new List<string> { "NewValue1" }
                }
            };
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.EditMultiple(settingModels);
            Assert.IsType<ApiSuccessResult>(actionResult);
        }

        [Fact]
        public void EditMultiple_ListSettingModel_ReturnNotFound()
        {
            var settingController = new SettingController(_mockSettingService.Object, _configuration, _mockHttpContext.Object);
            var actionResult = settingController.EditMultiple(null);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorResult?.StatusCode);
        }
    }
}
