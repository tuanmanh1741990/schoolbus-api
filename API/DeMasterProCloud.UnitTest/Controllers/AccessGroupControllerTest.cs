using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class AccessGroupControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly Mock<IAccessGroupService> _mockAccessGroupService;
        private readonly Mock<IUserService> _mockUserService;
        private readonly Mock<ITimezoneService> _mockTimezoneService;
        private readonly Mock<IVisitService> _mockVisitService;
        private readonly List<AccessGroup> _accessGroups;

        public AccessGroupControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _accessGroups = TestHelper.GetTestAccessGroups();
            _mockAccessGroupService = new Mock<IAccessGroupService>();
            _mockUserService = new Mock<IUserService>();
            _mockTimezoneService = new Mock<ITimezoneService>();
            _mockVisitService = new Mock<IVisitService>();
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            //Initial mapping
            AutoMapperConfig.Initialize();
        }

        [Fact]
        public void Gets_ListAccessGroup_Return_OkObjectResult()
        {
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.Gets(null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Get_AccessGroup_Return_404NotFound()
        {
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.Get(1);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Gets_ListAssignDoors_Return_OkObjectResult()
        {
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.GetDoors(1, null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Gets_ListUnAssignDoors_Return_OkObjectResult()
        {
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.GetUnAssignDoors(1, null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Gets_ListAssignUsers_Return_OkObjectResult()
        {
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.GetUsers(1, null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Gets_ListUnAssignUsers_Return_OkObjectResult()
        {
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.GetUnAssignUsers(1, null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Get_AccessGroup_Return_OkObjectResult()
        {
            _mockAccessGroupService.Setup(x => x.GetById(It.Is<int>(i => i == 1))).Returns(_accessGroups.First());
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.Get(1);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Add_AccessGroup_Return_SuccessResult()
        {
            var accessGroupModel = Mapper.Map<AccessGroupModel>(_accessGroups.First());
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var result = accessGroupController.Add(accessGroupModel);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact]
        public void Add_AccessGroup_Return_ValidateFailResult()
        {
            var accessGroupModel = Mapper.Map<AccessGroupModel>(_accessGroups.First());
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            accessGroupController.ModelState.AddModelError("AccessGroupName", "Access group name already exist");
            var result = accessGroupController.Add(accessGroupModel);
            var objectResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_AccessGroup_Return_ValidationFailedResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            var accessGroupModel = Mapper.Map<AccessGroupModel>(_accessGroups.First());

            var result = accessGroupController.Object.Edit(_accessGroups.First().Id, accessGroupModel);
            var objectResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_AccessGroup_Return_NotFoundResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            var accessGroupModel = Mapper.Map<AccessGroupModel>(_accessGroups.First());

            accessGroupController.Setup(x => x.TryValidateModel(It.IsAny<AccessGroupModel>())).Returns(true);
            var result = accessGroupController.Object.Edit(_accessGroups.First().Id, accessGroupModel);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_AccessGroup_Return_ApiSuccessResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            var accessGroupModel = Mapper.Map<AccessGroupModel>(_accessGroups.First());
            accessGroupController.Setup(x => x.TryValidateModel(It.IsAny<AccessGroupModel>())).Returns(true);
            _mockAccessGroupService.Setup(x => x.GetById(1)).Returns(_accessGroups.First());
            var result = accessGroupController.Object.Edit(_accessGroups.First().Id, accessGroupModel);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_AccessGroup_Return_ApiErrorResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            var result = accessGroupController.Object.Delete(1);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_AccessGroup_Return_400BadRequest()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            _mockAccessGroupService.Setup(x => x.GetById(1)).Returns(_accessGroups.First());
            _mockAccessGroupService.Setup(x => x.GetNameAccessGroupCouldNotUpdateOrDelete(new List<int>() { 1 })).Returns("Access Group name");
            var result = accessGroupController.Object.Delete(1);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_AccessGroup_Return_ApiSuccessResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            _mockAccessGroupService.Setup(x => x.GetById(1)).Returns(_accessGroups.First());
            var result = accessGroupController.Object.Delete(_accessGroups.First().Id);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void DeleteMultiple_AccessGroup_Return_ApiErrorResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            _mockAccessGroupService.Setup(x => x.GetByIds(new List<int>() { 10, 11 })).Returns(_accessGroups.Take(0).ToList());
            var result = accessGroupController.Object.DeleteMultiple(new List<int>() { 10, 11 });
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void DeleteMultiple_AccessGroup_Return_400BadRequest()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            _mockAccessGroupService.Setup(x => x.GetByIds(new List<int>() { 1, 2 })).Returns(_accessGroups);
            _mockAccessGroupService.Setup(x => x.GetNameAccessGroupCouldNotUpdateOrDelete(new List<int>() { 1, 2 })).Returns("Access Group name");
            var result = accessGroupController.Object.DeleteMultiple(new List<int> { 1, 2 });
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, objectResult.StatusCode);
        }

        [Fact]
        public void DeleteMultiple_AccessGroup_Return_ApiSuccessResult()
        {
            Mock<AccessGroupController> accessGroupController = new Mock<AccessGroupController>(_mockAccessGroupService.Object, _mockHttpContext.Object);
            _mockAccessGroupService.Setup(x => x.GetByIds(new List<int>() { 1, 2 })).Returns(_accessGroups);
            var result = accessGroupController.Object.DeleteMultiple(new List<int>() { 1, 2 });
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void AssignDoors_DoorList_ReturnBadRequest()
        {
            var testAccessGroupIdNonExist = 0;
            var testAccessGroupAssignDoor = new AccessGroupAssignDoor();
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.AssignDoors(testAccessGroupIdNonExist, testAccessGroupAssignDoor);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);
        }

        [Fact]
        public void AssignDoors_DoorList_ReturnMessageOverMaxUser()
        {
            var testAccessGroupId = 1;
            var testAccessGroupAssignDoor = new AccessGroupAssignDoor();
            testAccessGroupAssignDoor.Doors.Add(new AccessGroupAssignDoorDetail
            {
                TzId = 1,
                DoorId = 1
            });
            _mockAccessGroupService.Setup(c => c.AssignDoors(It.IsAny<int>(), It.IsAny<AccessGroupAssignDoor>(), true))
                .Returns(AccessGroupResource.msgUnableToAssignOverMaxUser);
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.AssignDoors(testAccessGroupId, testAccessGroupAssignDoor);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);
        }

        [Fact]
        public void AssignDoors_DoorList_ReturnSuccess()
        {
            var testAccessGroupId = 1;
            var testAccessGroupAssignDoor = new AccessGroupAssignDoor();
            testAccessGroupAssignDoor.Doors.Add(new AccessGroupAssignDoorDetail
            {
                TzId = 1,
                DoorId = 1
            });
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.AssignDoors(testAccessGroupId, testAccessGroupAssignDoor);
            var objectResult = Assert.IsType<ApiSuccessResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void AssignUsers_UserIdList_ReturnBadRequest()
        {
            var testAccessGroupIdNonExist = 0;
            var testUserIdList = new List<int> { 1, 2, 3 };
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.AssignUsers(testAccessGroupIdNonExist, testUserIdList);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);
        }

        [Fact]
        public void AssignUsers_UserIdList_ReturnMessageOverMaxUser()
        {
            var testAccessGroupId = 1;
            var testUserIdList = new List<int> { 1, 2, 3 };
            _mockAccessGroupService.Setup(c => c.AssignUsers(It.IsAny<int>(), It.IsAny<List<int>>()))
                .Returns(AccessGroupResource.msgUnableToAssignOverMaxUser);
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.AssignUsers(testAccessGroupId, testUserIdList);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);
        }

        [Fact]
        public void AssignUsers_DoorList_ReturnSuccess()
        {
            var testAccessGroupId = 1;
            var testUserIdList = new List<int> { 1, 2, 3 };
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.AssignUsers(testAccessGroupId, testUserIdList);
            var objectResult = Assert.IsType<ApiSuccessResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        }

        [Fact]
        public void UnAssignUsers_UserIdList_ReturnSuccess()
        {
            var testAccessGroupId = 1;
            var testUserIdList = new List<int> { 1, 2 };
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.UnAssignUsers(testAccessGroupId, testUserIdList);
            var objectResult = Assert.IsType<ApiSuccessResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        }

        [Fact]
        public void UnAssignUsers_UserIdList_ReturnBadRequest()
        {
            var testAccessGroupId = 1;
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.UnAssignUsers(testAccessGroupId, null);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);

        }
        [Fact]
        public void UnAssignDoors_DoorList_ReturnSuccess()
        {
            var testAccessGroupId = 1;
            var testDoorIdList = new List<int> { 1, 2 };
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.UnAssignDoors(testAccessGroupId, testDoorIdList);
            var objectResult = Assert.IsType<ApiSuccessResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void UnAssignDoors_DoorList_ReturnBadRequest()
        {
            var testAccessGroupId = 1;
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.UnAssignDoors(testAccessGroupId, null);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);
        }

        [Fact]
        public void ChangeTimezone_AccessGroupId_ReturnSuccess()
        {
            var testAccessGroupId = 1;
            var accessGroupAssignDoorDetail = new AccessGroupAssignDoorDetail
            {
                DoorId = 1,
                TzId = 2
            };
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.ChangeTimezone(testAccessGroupId, accessGroupAssignDoorDetail);
            var objectResult = Assert.IsType<ApiSuccessResult>(actionResult);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void ChangeTimezone_AccessGroupId_ReturnBadRequest()
        {
            var testAccessGroupId = 1;
            var accessGroupController = new AccessGroupController(_mockAccessGroupService.Object, _mockHttpContext.Object, _mockVisitService.Object);
            var actionResult = accessGroupController.ChangeTimezone(testAccessGroupId, null);
            var failedResult = Assert.IsType<ApiErrorResult>(actionResult);
            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(StatusCodes.Status400BadRequest, errorResult?.StatusCode);
        }
    }
}
