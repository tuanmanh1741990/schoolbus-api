﻿using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class HolidayControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IHolidayService> _mockHolidayService;
        private readonly HolidayController _holidayController;
        private readonly List<Holiday> _holidays;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        //private readonly MapperConfigurationExpression _mapperConfiguration;

        public HolidayControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            _holidays = TestHelper.GetTestHolidays();
            var mockSet = TestHelper.GetMockDbSet(_holidays);
            _mockHolidayService = new Mock<IHolidayService>();

            _holidayController = new HolidayController(_configuration, _mockHolidayService.Object, _mockHttpContext.Object);
            //Initial mapping
            AutoMapperConfig.Initialize();

        }

        [Fact]
        public void Get_Test()
        {
            var result = _holidayController.Get(null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void GetById_Test()
        {
            var result1 = _holidayController.Get(1);
            var objectResult1 = Assert.IsType<ApiErrorResult>(result1);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult1.StatusCode);

            _mockHolidayService.Setup(x => x.GetHolidayByIdAndCompany(It.Is<int>(i => i == 1), It.Is<int>(i => i == 14))).Returns(_holidays.First());
            var result2 = _holidayController.Get(id: 14);
            var objectResult2 = Assert.IsType<OkObjectResult>(result2);
            Assert.Equal(StatusCodes.Status200OK, objectResult2.StatusCode);
        }

        [Fact]
        public void Add_Test()
        {
            Mock<HolidayController> holidayController = new Mock<HolidayController>(_configuration, _mockHolidayService.Object,
               _mockHttpContext.Object);


            HolidayModel holidayData = new HolidayModel()
            {
                Id = 1,
                Name = "Duali",
                CompanyId = 1,
                Type = 1,
                StartDate = DateTime.Now.ToString(),
                EndDate = DateTime.Now.AddMonths(1).ToString(),
                Recursive = false,
                Remarks = "Duali",
            };

            //var holidayModel = Mapper.Map<HolidayModel>(TestHelper.GetTestHolidays().First());

            var holidayModel = Mapper.Map<HolidayModel>(holidayData);
            var result1 = holidayController.Object.Add(holidayModel);
            var objectResult1 = Assert.IsType<ApiSuccessResult>(result1);
            Assert.Equal(StatusCodes.Status201Created, objectResult1.StatusCode);

            //holidayController.Object.ModelState.AddModelError("CompanyCode", "CompanyId is require");
            //var result2 = holidayController.Object.Add(holidayModel);
            //var objectResult2 = Assert.IsType<ValidationFailedResult>(result2);
            //Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult2.StatusCode);

        }

        [Fact]
        public void Edit_Test()
        {
            Mock<HolidayController> holidayController = new Mock<HolidayController>(_configuration, _mockHolidayService.Object,
              _mockHttpContext.Object);

            HolidayModel holidayData = new HolidayModel()
            {
                Id = 1,
                Name = "Duali",
                CompanyId = 1,
                Type = 1,
                StartDate = DateTime.Now.ToString(),
                EndDate = DateTime.Now.AddYears(1).ToString(),
                Recursive = false,
                Remarks = "Duali",
                //HolidayTypes = new List<SelectListItem>(),
            };

            var holidayModel = Mapper.Map<HolidayModel>(holidayData);

            var result1 = holidayController.Object.Edit(holidayData.Id, holidayModel);
            var objectResult1 = Assert.IsType<ValidationFailedResult>(result1);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult1.StatusCode);

            holidayController.Setup(x => x.TryValidateModel(It.IsAny<HolidayModel>())).Returns(true);
            var result2 = holidayController.Object.Edit(holidayData.Id, holidayModel);
            var objectResult2 = Assert.IsType<ApiErrorResult>(result2);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult2.StatusCode);

            _mockHolidayService.Setup(x => x.GetHolidayByIdAndCompany(1, 1)).Returns(_holidays.First());
            var result3 = holidayController.Object.Edit(holidayData.Id, holidayModel);
            var objectResult3 = Assert.IsType<ApiSuccessResult>(result3);
            Assert.Equal(StatusCodes.Status200OK, objectResult3.StatusCode);
        }

        [Fact]
        public void Delete_Test()
        {
            Mock<HolidayController> holidayController = new Mock<HolidayController>(_configuration, _mockHolidayService.Object,
              _mockHttpContext.Object);

            var result1 = holidayController.Object.Delete(1);
            var objectResult1 = Assert.IsType<ApiErrorResult>(result1);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult1.StatusCode);

            _mockHolidayService.Setup(x => x.GetHolidayByIdAndCompany(1, 1)).Returns(_holidays.First());
            var result2 = holidayController.Object.Delete(_holidays.First().Id);
            var objectResult2 = Assert.IsType<ApiSuccessResult>(result2);
            Assert.Equal(StatusCodes.Status200OK, objectResult2.StatusCode);
        }

        [Fact]
        public void DeleteMultiple_Test()
        {
            Mock<HolidayController> holidayController = new Mock<HolidayController>(_configuration, _mockHolidayService.Object,
              _mockHttpContext.Object);

            _mockHolidayService.Setup(x => x.GetByIds(new List<int>() { 10, 11 })).Returns(_holidays.Take(0).ToList());
            var result1 = holidayController.Object.DeleteMultiple(new List<int>() { 10, 11 });
            var objectResult1 = Assert.IsType<ApiErrorResult>(result1);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult1.StatusCode);

            _mockHolidayService.Setup(x => x.GetByIds(new List<int>() { 1, 2 })).Returns(_holidays);
            var result2 = holidayController.Object.DeleteMultiple(new List<int>() { 1, 2 });
            var objectResult2 = Assert.IsType<ApiSuccessResult>(result2);
            Assert.Equal(StatusCodes.Status200OK, objectResult2.StatusCode);
        }

        //[Fact]
        //public void ToggleStatus_Test()
        //{
        //    Mock<HolidayController> holidayController = new Mock<HolidayController>(_configuration, _mockHolidayService.Object,
        //      _mockHttpContext.Object);

        //    var result1 = holidayController.Object.ToggleStatus(10);
        //    var objectResult1 = Assert.IsType<ApiErrorResult>(result1);
        //    Assert.Equal(StatusCodes.Status404NotFound, objectResult1.StatusCode);

        //    _mockDeviceService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(_devices.First());
        //    var result2 = deviceController.Object.ToggleStatus(1);
        //    var objectResult2 = Assert.IsType<ApiSuccessResult>(result2);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult2.StatusCode);
        //}

        //[Fact]
        //public void GetCondition_Test()
        //{
        //    _mockMprService.Setup(x => x.GetCountByMprId(1, 1)).Returns(TestHelper.GetTestMprUser().ToList().Count);
        //    var result = _holidayController.GetCondition(1);
        //    var objectResult = Assert.IsType<OkObjectResult>(result);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        //}

        //[Fact]
        //public void CreateTestData_Test()
        //{
        //    var result1 = _deviceController.CreateTestData(0);
        //    var objectResult1 = Assert.IsType<OkObjectResult>(result1);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult1.StatusCode);

        //    var result2 = _deviceController.CreateTestData(1);
        //    var objectResult2 = Assert.IsType<OkObjectResult>(result2);
        //    Assert.Equal(StatusCodes.Status200OK, objectResult2.StatusCode);

        //}
    }
}
