﻿using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataModel.Building;
using Xunit;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class BuildingControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IBuildingService> _mockBuildingService;
        //private readonly BuildingController _buildingController;
        private readonly List<Building> _buildings;
        private readonly List<IcuDevice> _IcuDevices;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        //private readonly MapperConfigurationExpression _mapperConfiguration;

        public BuildingControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            _buildings = TestHelper.GetTestBuildings();
            _IcuDevices = TestHelper.GetTestIcuDevice();
            var mockSet = TestHelper.GetMockDbSet(_buildings);
            _mockBuildingService = new Mock<IBuildingService>();

            //_buildingController = new BuildingController(_configuration, _mockBuildingService.Object, _mockHttpContext.Object);

            AutoMapperConfig.Initialize();
        }

        [Fact]
        public void GetDoors_Test()
        {
            var _buildingController = new BuildingController(_configuration,_mockBuildingService.Object, _mockHttpContext.Object);

            var result = _buildingController.GetDoors(1, "");
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }



        [Fact]
        public void Add_Test()
        {
            BuildingModel buildingData = new BuildingModel()
            {
                Id = 1,
                Name = "Head Quarter",
            };
            var buildingModel = Mapper.Map<BuildingModel>(buildingData);
            var _buildingController = new BuildingController(_configuration,
                _mockBuildingService.Object, _mockHttpContext.Object);
            var result1 = _buildingController.Add(buildingModel);
            var objectResult1 = Assert.IsType<ApiSuccessResult>(result1);
            Assert.Equal(StatusCodes.Status201Created, objectResult1.StatusCode);
        }

        [Fact]
        public void AddDoors_Test()
        {
            var _buildingController = new BuildingController(_configuration, _mockBuildingService.Object, _mockHttpContext.Object);

            var doorIds = new List<int>();
            doorIds.Add(1);
           
            var result1 = _buildingController.AddDoors(1, doorIds);
            var objectResult1 = Assert.IsType<ApiSuccessResult>(result1);
            Assert.Equal(StatusCodes.Status201Created, objectResult1.StatusCode);
        }

        [Fact]
        public void EditBuildingName_Test()
        {
            var _buildingController = new BuildingController(_configuration, _mockBuildingService.Object, _mockHttpContext.Object);
            BuildingModel buildingData = new BuildingModel()
            {
                
                Name = "Head Quarter",
            };

            var result1 = _buildingController.Edit(1, buildingData);
            var objectResult1 = Assert.IsType<ApiSuccessResult>(result1);
            Assert.Equal(StatusCodes.Status200OK, objectResult1.StatusCode);
        }

        [Fact]
        public void Delete_Test()
        {
            var _buildingController = new BuildingController(_configuration, _mockBuildingService.Object, _mockHttpContext.Object);

            var result1 = _buildingController.Delete(2);
            var objectResult1 = Assert.IsType<ApiSuccessResult>(result1);
            Assert.Equal(StatusCodes.Status200OK, objectResult1.StatusCode);
        }

        [Fact]
        public void DeleteDoors_Test()
        {
            var _buildingController = new BuildingController(_configuration, _mockBuildingService.Object, _mockHttpContext.Object);
            var doorIds = new List<int>();
            doorIds.Add(1);
            var result1 = _buildingController.DeleteDoors(1, doorIds);
            var objectResult1 = Assert.IsType<ApiSuccessResult>(result1);
            Assert.Equal(StatusCodes.Status200OK, objectResult1.StatusCode);
        }
    }
}
