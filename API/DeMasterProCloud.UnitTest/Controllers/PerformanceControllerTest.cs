using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Service.Csv;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class PerformanceControllerTest
    {
        private readonly string _sendUserToIcuFile =
            $"SendUserToIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _parseEventFromIcuFile =
            $"ParseEventFromIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _getEventLogFile =
            $"GetEventLog_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _getUserFile =
            $"GetUser_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _saveUserToIcuFile =
            $"SaveUserToIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";


        private readonly string[] _parseEventFromIcu1 =
        {
            "0B5521",
            "10",
            "2018/11/26 10:29:17",
            "2018/11/26 10:30:46",
            "2018/11/26 10:30:46",
            "2018/11/26 10:31:16",
            "89.0681154",
            "89.0681154",
            "89.0681154",
            "89.0681154",
            "89.0681154",
            "89.0681154",
            "89.0681154",
            "89.0681154"
        };

        private readonly string[] _parseEventFromIcu2 =
        {
            "2018/11/26 10:29:17",
            "2018/11/26 10:30:46",
            "2018/11/26 10:30:46",
            "2018/11/26 10:31:16",
            "2018/11/26 10:31:16",
            "89.0681154",
            "30.8868406",
            "30.7493292",
            "30.7493292",
            "10",
            "0D8DA7"
        };

        private readonly string[] _sendUserToIcuHeader =
        {
            "Icu Address",
            "Index",
            "Request",
            "Number of processed user",
            "Total user",
            "Time receiving request (t1)",
            "Time parsing and saving data into database (t2)",
            "Time publishing message to queue (t3)",
            "t2-t1",
            "t3-t2",
            "Time receiving response from ICU (t4)",
            "t4-t1"
        };

        private readonly string[] _getEventLogHeader = {
            "Request",
            "Number of event",
            "Time receiving request (t1)",
            "Time finishing request (t2)",
            "Processing time in seconds (t2-t1)"
        };

        private readonly string[] _getUserHeader = {
            "Request",
            "Number of user",
            "Time receiving request (t1)",
            "Time finishing request (t2)",
            "Processing time in seconds (t2-t1)"
        };

        private readonly string[] _saveUserToIcuHeader = {
            "Icu Address",
            "Index",
            "Time ICU receiving message (t1)",
            "Time ICU finish saving user data (t2)",
            "t2-t1"
        };

        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly Mock<PerformanceController> _performanceController;

        public PerformanceControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            //Mock HttpContext
            var httpContext = TestHelper.GetTestHttpContext();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(httpContext);
            //Delete all test data
            //DeleteTestData(Environment.CurrentDirectory);
            _performanceController = new Mock<PerformanceController>(_configuration);
        }

        //[Fact]
        //public void ParseEvent_ReturnEmptyResult()
        //{
        //    var actionResult = _performanceController.Object.ParseEvent();
        //    Assert.IsType<EmptyResult>(actionResult);
        //}

        //[Fact]
        //public void ParseEvent_ReturnFileContentResult()
        //{
        //    CreateParseEventFromIcuTestData();
        //    var actionResult = _performanceController.Object.ParseEvent();
        //    var okObjectResult = Assert.IsType<FileContentResult>(actionResult);
        //    Assert.True(okObjectResult.FileContents.Length > 0);
        //    DeleteTestData(Environment.CurrentDirectory, "ParseEventFromIcu_");
        //}

        //[Fact]
        //public void GetContents_ReturnByteContent()
        //{
        //    CreateParseEventFromIcuTestData();
        //    var result = _performanceController.Object.GetContents(Environment.CurrentDirectory);
        //    File.WriteAllBytes(_parseEventFromIcuFile, result);
        //    var lines = File.ReadAllLines(_parseEventFromIcuFile);
        //    Assert.True(lines.First().Split(",").Length == _parseEventFromIcu1.Length && lines.Length == 3);
        //    DeleteTestData(Environment.CurrentDirectory, "ParseEventFromIcu_");
        //}

        //[Fact]
        //public void GetContents_ReturnNoContent()
        //{
        //    var result = _performanceController.Object.GetContents(Environment.CurrentDirectory);
        //    Assert.Null(result);
        //}

        //[Fact]
        //public void SendUserToIcu_ReturnFileContentResult()
        //{
        //    CreateSendUserToIcuTestData();
        //    var actionResult = _performanceController.Object.SendUserToIcu();
        //    var okObjectResult = Assert.IsType<FileContentResult>(actionResult);

        //    File.WriteAllBytes(_parseEventFromIcuFile, okObjectResult.FileContents);
        //    var lines = File.ReadAllLines(_parseEventFromIcuFile);
        //    Assert.True(lines.First().Split(",").Length == _sendUserToIcuHeader.Length && lines.Length == 2);
        //    DeleteTestData(Environment.CurrentDirectory, "SendUserToIcu_");
        //}

        //[Fact]
        //public void SendUserToIcu_ReturnEmptyResult()
        //{
        //    DeleteTestData(Environment.CurrentDirectory, "SendUserToIcu_");
        //    var actionResult = _performanceController.Object.SendUserToIcu();
        //    Assert.IsType<EmptyResult>(actionResult);
        //}

        //[Fact]
        //public void GetEventLog_ReturnEmptyResult()
        //{
        //    DeleteTestData(Environment.CurrentDirectory, "GetEventLog_");
        //    var actionResult = _performanceController.Object.GetEventLog();
        //    Assert.IsType<EmptyResult>(actionResult);
        //}

        //[Fact]
        //public void GetEventLog_ReturnFileContentResult()
        //{
        //    CreateGetEventLogTestData();
        //    var actionResult = _performanceController.Object.GetEventLog();
        //    var okObjectResult = Assert.IsType<FileContentResult>(actionResult);

        //    File.WriteAllBytes(_getEventLogFile, okObjectResult.FileContents);
        //    var lines = File.ReadAllLines(_getEventLogFile);
        //    Assert.True(lines.First().Split(",").Length == _getEventLogHeader.Length && lines.Length == 2);
        //    DeleteTestData(Environment.CurrentDirectory, "GetEventLog_");
        //}

        //[Fact]
        //public void GetUser_ReturnEmptyResult()
        //{
        //    DeleteTestData(Environment.CurrentDirectory, "GetUser_");
        //    var actionResult = _performanceController.Object.GetUser();
        //    Assert.IsType<EmptyResult>(actionResult);
        //}

        //[Fact]
        //public void GetUser_ReturnFileContentResult()
        //{
        //    CreateGetUserTestData();
        //    var actionResult = _performanceController.Object.GetUser();
        //    var okObjectResult = Assert.IsType<FileContentResult>(actionResult);

        //    File.WriteAllBytes(_getUserFile, okObjectResult.FileContents);
        //    var lines = File.ReadAllLines(_getUserFile);
        //    Assert.True(lines.First().Split(",").Length == _getUserHeader.Length && lines.Length == 2);
        //    DeleteTestData(Environment.CurrentDirectory, "GetUser_");
        //}

        //[Fact]
        //public void SaveUserToIcu_ReturnEmptyResult()
        //{
        //    var actionResult = _performanceController.Object.SaveUserToIcu();
        //    Assert.IsType<EmptyResult>(actionResult);
        //}

        //[Fact]
        //public void SaveUserToIcu_ReturnFileContentResult()
        //{
        //    CreateSaveUserToIcuTestData();
        //    var actionResult = _performanceController.Object.SaveUserToIcu();
        //    var okObjectResult = Assert.IsType<FileContentResult>(actionResult);

        //    File.WriteAllBytes(_saveUserToIcuFile, okObjectResult.FileContents);
        //    var lines = File.ReadAllLines(_saveUserToIcuFile);
        //    Assert.True(lines.First().Split(",").Length == _saveUserToIcuHeader.Length && lines.Length == 2);
        //    DeleteTestData(Environment.CurrentDirectory, "SaveUserToIcu_");
        //}

        //private void DeleteTestData(string inputDirectoryPath, string prefix)
        //{
        //    var inputFilePaths = Directory.GetFiles(inputDirectoryPath, $"{prefix}*.csv")
        //        .ToList();
        //    foreach (var filePath in inputFilePaths)
        //    {
        //        File.Delete(filePath);
        //    }
        //}

        private void CreateParseEventFromIcuTestData()
        {
            var csvFile1 =
                $"ParseEventFromIcu_0B5521_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
            CsvHelper.Write(null, new List<string[]> { _parseEventFromIcu1 }, csvFile1);

            var csvFile2 =
                $"ParseEventFromIcu_0D8DA7_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
            CsvHelper.Write(null, new List<string[]> { _parseEventFromIcu2 }, csvFile2);
        }

        private void CreateSendUserToIcuTestData()
        {
            var data = new[]
            {
                "005E87",
                "2",
                "https://localhost:44327/access-levels/1899/1/assign-users",
                "1",
                "1",
                "2018/11/23 17:15:29",
                "2018/11/23 17:16:10",
                "2018/11/23 17:16:10",
                "40.6097887",
                "0.1001943",
                "2018/11/23 17:16:10",
                "0.1001943"
            };
            CsvHelper.Write(_sendUserToIcuHeader, new List<string[]> { data }, _sendUserToIcuFile);
        }

        private void CreateGetEventLogTestData()
        {
            var data = new[]
            {
                "https://localhost:44327/event-logs",
                "10",
                "2018/11/23 17:15:29",
                "2018/11/23 17:16:10",
                "0.1001943"
            };
            CsvHelper.Write(_getEventLogHeader, new List<string[]> { data }, _getEventLogFile);
        }

        private void CreateGetUserTestData()
        {
            var data = new[]
            {
                "https://localhost:44327/users",
                "10",
                "2018/11/23 17:15:29",
                "2018/11/23 17:16:10",
                "0.1001943"
            };
            CsvHelper.Write(_getUserHeader, new List<string[]> { data }, _getUserFile);
        }

        private void CreateSaveUserToIcuTestData()
        {
            var data = new[]
            {
                "005E87",
                "10",
                "1001943",
                "1003943",
                "2000"
            };
            CsvHelper.Write(_saveUserToIcuHeader, new List<string[]> { data }, _saveUserToIcuFile);
        }
    }
}
