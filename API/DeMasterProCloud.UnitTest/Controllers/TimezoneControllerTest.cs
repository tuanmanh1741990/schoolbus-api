﻿using AutoMapper;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Timezone;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class TimezoneControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<ITimezoneService> _mockTimezoneService;
        private readonly Mock<IDeviceService> _mockDeviceService;
        private readonly TimezoneController _timezoneController;
        private readonly List<Timezone> _timezone;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        //private readonly MapperConfigurationExpression _mapperConfiguration;

        public TimezoneControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            _timezone = TestHelper.GetTestTimezone();
            var mockSet = TestHelper.GetMockDbSet(_timezone);
            _mockTimezoneService = new Mock<ITimezoneService>();
            _mockDeviceService = new Mock<IDeviceService>();

            _timezoneController = new TimezoneController(_configuration,_mockTimezoneService.Object, _mockHttpContext.Object,_mockDeviceService.Object);
            AutoMapperConfig.Initialize();
        }

        [Fact]
        public void Get_ListTimezone_Return_OkObjectResult()
        {

            var result = _timezoneController.Get(null);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ById_When_IdNotEqualZero_Return_OkObjectResult()
        {
            var id = 1;
            _mockTimezoneService.Setup(x => x.GetByIdAndCompany(It.IsAny<int>(), It.IsAny<int>())).Returns(_timezone.First());
            var result = _timezoneController.Get(id);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ById_When_IdEqualZero_Return_OkObjectResult()
        {
            var id = 0;
            var result = _timezoneController.Get(id);
            var objectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ById_Return_ApiErrorResult()
        {
            var id = 1;
            var result = _timezoneController.Get(id);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ById_Return_ApiSuccessResult()
        {
            var model = new TimezoneModel()
            {
                Id = 1,
                Name = "UTC",
                Remarks = "UTC",
                CompanyId = 1,
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                Position = 1,
            };
            var result = _timezoneController.Add(model);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status201Created, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ById_Return_Return_ValidationFailedResult()
        {
            var model = new TimezoneModel();
            _timezoneController.ModelState.AddModelError("error", "timezone is not valid");
            var result = _timezoneController.Add(model);
            var objectResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(StatusCodes.Status422UnprocessableEntity, objectResult.StatusCode);
        }


        [Fact]
        public void Edit_Timezone_Return_ApiErrorResult()
        {
            var id = 1;
            var model = Mapper.Map<TimezoneModel>(TestHelper.GetTestTimezone().First());
            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                              It.IsAny<ValidationStateDictionary>(),
                                              It.IsAny<string>(),
                                              It.IsAny<Object>()));
            _timezoneController.ObjectValidator = objectValidator.Object;
            var result = _timezoneController.Edit(id, model);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Edit_Timezone_Return_ApiSuccessResult()
        {
            var id = 1;
            var model = Mapper.Map<TimezoneModel>(TestHelper.GetTestTimezone().First());
            var objectValidator = new Mock<IObjectModelValidator>();
            objectValidator.Setup(o => o.Validate(It.IsAny<ActionContext>(),
                                              It.IsAny<ValidationStateDictionary>(),
                                              It.IsAny<string>(),
                                              It.IsAny<Object>()));
            _mockTimezoneService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(TestHelper.GetTestTimezone().First());
            _timezoneController.ObjectValidator = objectValidator.Object;
            var result = _timezoneController.Edit(id, model);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_Timezone_Return_ApiErrorResult()
        {
            var result = _timezoneController.Delete(1);
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        [Fact]
        public void Delete_IcuDevice_Return_ApiSuccessResult()
        {
            _mockTimezoneService.Setup(x => x.GetByIdAndCompany(1, 1)).Returns(_timezone.First());
            var result = _timezoneController.Delete(_timezone.First().Id);
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);
        }
        [Fact]
        public void DeleteMultiple_IcuDevice_Return_ApiErrorResult()
        {
            _mockTimezoneService.Setup(x => x.GetByIdsAndCompany(new List<int>() { 10, 11 }, It.Is<int>(j => j == 1))).Returns(_timezone.Take(0).ToList());
            var result = _timezoneController.DeleteMultiple(new List<int>() { 10, 11 });
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status404NotFound, objectResult.StatusCode);
        }

        //[Fact]
        //public void DeleteMultipleIcuDevice_Return_ApiSuccessResult()
        //{
        //    _mockTimezoneService.Setup(x => x.GetByIdsAndCompany(new List<int>() { 1, 2 }, It.Is<int>(j => j == 1))).Returns(_timezone);
        //    var result = _timezoneController.DeleteMultiple(new List<int>() { 1, 2 });
        //    var objectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.Equal(StatusCodes.Status400BadRequest, objectResult.StatusCode);
        //}

        [Fact]
        public void DeleteMultipleIcuDevice_When_HasNotTimezone_Return_ApiSuccessResult()
        {
            _mockDeviceService.Setup(x => x.HasTimezone(It.IsAny<int>(), It.IsAny<int>())).Returns(true);
            _mockTimezoneService.Setup(x => x.GetByIdsAndCompany(new List<int>() { 1, 2 }, It.Is<int>(j => j == 1))).Returns(_timezone);
            var result = _timezoneController.DeleteMultiple(new List<int>() { 1, 2 });
            var objectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(StatusCodes.Status400BadRequest, objectResult.StatusCode);
        }

        [Fact]
        public void DeleteMultipleIcuDevice_When_PositionNotDefault_Return_ApiSuccessResult()
        {
            var timezone = _timezone.Skip(3).ToList();
            _mockTimezoneService.Setup(x => x.GetByIdsAndCompany(new List<int>() { 1, 2 }, It.Is<int>(j => j == 1))).Returns(timezone);
            var result = _timezoneController.DeleteMultiple(new List<int>() { 1, 2 });
            var objectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(StatusCodes.Status200OK, objectResult.StatusCode);

        }
    }
}
