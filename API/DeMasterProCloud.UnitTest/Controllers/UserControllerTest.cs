using AutoMapper;
using Bogus;
using DeMasterProCloud.Api.Controllers;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Controllers
{
    public class UserControllerTest
    {
        private readonly IConfigurationRoot _configuration;
        private readonly Mock<IHttpContextAccessor> _mockHttpContext;
        private readonly Mock<IUserService> _mockUserService;
        private readonly Mock<IEventLogService> _mockEventLogService;
        private readonly List<User> _users;

        public UserControllerTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            ApplicationVariables.Configuration = _configuration;
            _mockUserService = new Mock<IUserService>();
            _mockEventLogService = new Mock<IEventLogService>();
            _mockHttpContext = new Mock<IHttpContextAccessor>();
            _users = GetTestUsers();

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock HttpContext
            var httpContext = TestHelper.GetTestHttpContext();
            _mockHttpContext.Setup(x => x.HttpContext).Returns(httpContext);
        }

        [Fact]
        public void Get_ListUser_Return_OkObjectResult()
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var actionResult = userController.Gets(null);
            var okObjectResult = Assert.IsType<OkObjectResult>(actionResult);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, okObjectResult.StatusCode);
        }

        [Fact]
        public void Get_ListUser_Return404()
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.Get(10);
            var failedResult = Assert.IsType<ApiErrorResult>(result);

            var errorResult = failedResult.Value as ApiErrorResultModel;
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorResult?.StatusCode);
        }

        [Fact]
        public void Get_UserById_ReturnUserModel()
        {
            _mockUserService.Setup(x => x.GetById(It.Is<int>(i => i == 1))).Returns(_users.First());
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.Get(1);
            var okObjectResult = Assert.IsType<OkObjectResult>(result);
            Assert.True(okObjectResult.Value is UserModel);
        }

        [Fact]
        public void Add_User_ReturnValidationFail()
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var model = Mapper.Map<UserModel>(GetNewUser());
            userController.ModelState.AddModelError("UserCode", "Existed User code");
            var result = userController.Add(model);
            var validationFailedResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status422UnprocessableEntity, validationFailedResult.StatusCode);
        }

        [Fact]
        public void Add_Return_OkObjectResult()
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var model = Mapper.Map<UserModel>(GetNewUser());
            var result = userController.Add(model);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status201Created, okObjectResult.StatusCode);
        }

        [Fact]
        public void Edit_User_Return_ValidationFail()
        {
            var model = Mapper.Map<UserModel>(_users.First());

            var mockUserController = new Mock<UserController>(_mockUserService.Object, _mockHttpContext.Object, _configuration);
            mockUserController.Setup(x => x.TryValidateModel(It.IsAny<UserModel>())).Returns(false);
            var result = mockUserController.Object.Edit(1, model);
            var validationFailedResult = Assert.IsType<ValidationFailedResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status422UnprocessableEntity, validationFailedResult.StatusCode);
        }

        [Fact]
        public void Edit_User_Return_NotFoundUser()
        {
            var mockUserController = new Mock<UserController>(_mockUserService.Object, _mockHttpContext.Object, _configuration);
            var model = Mapper.Map<UserModel>(_users.First());
            mockUserController.Setup(x => x.TryValidateModel(It.IsAny<UserModel>())).Returns(true);
            var result = mockUserController.Object.Edit(1, model);
            var errorObjectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorObjectResult.StatusCode);
        }

        [Fact]
        public void Edit_User_Return_OkObjectResult()
        {
            _mockUserService.Setup(x => x.GetByIdAndCompany(It.IsAny<int>(), It.IsAny<int>())).Returns(_users.First());
            var mockUserController = new Mock<UserController>(_mockUserService.Object, _mockHttpContext.Object, _configuration);
            mockUserController.Setup(x => x.TryValidateModel(It.IsAny<UserModel>())).Returns(true);
            var model = Mapper.Map<UserModel>(_users.First());
            var result = mockUserController.Object.Edit(1, model);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, okObjectResult.StatusCode);
        }

        [Fact]
        public void Delete_User_Return_NotFoundUser()
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.Delete(1);
            var errorObjectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorObjectResult.StatusCode);
        }

        [Fact]
        public void Delete_User_Return_OkObjectResult()
        {
            var user = _users.First();
            _mockUserService.Setup(x => x.GetByIdAndCompany(It.IsAny<int>(), It.IsAny<int>())).Returns(user);
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.Delete(user.Id);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, okObjectResult.StatusCode);
        }

        [Fact]
        public void Delete_MultipleUser_Return_NotFoundUser()
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.DeleteMultiple(new List<int> { 1, 2 });
            var errorObjectResult = Assert.IsType<ApiErrorResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status404NotFound, errorObjectResult.StatusCode);
        }

        [Fact]
        public void Delete_MultipleUser_Return_OkObjectResult()
        {
            var userIds = _users.Select(x => x.Id).ToList();
            _mockUserService.Setup(x => x.GetByIdsAndCompany(It.IsAny<List<int>>(), It.IsAny<int>())).Returns(_users);
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.DeleteMultiple(userIds);
            var okObjectResult = Assert.IsType<ApiSuccessResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, okObjectResult.StatusCode);
        }
        //[Fact]
        //public void ImportUser_Test()
        //{
        //    var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration);

        //    //Arrange
        //    var fileMock = new Mock<IFormFile>();
        //    //Setup mock file using a memory stream
        //    var content = "Hello World from a Fake File";
        //    var fileName = "test.txt";
        //    var ms = new MemoryStream();
        //    var writer = new StreamWriter(ms);
        //    writer.Write(content);
        //    writer.Flush();
        //    ms.Position = 0;
        //    fileMock.Setup(c => c.OpenReadStream()).Returns(ms);
        //    fileMock.Setup(c => c.FileName).Returns(fileName);
        //    fileMock.Setup(c => c.Length).Returns(ms.Length);
        //    //fileMock.
            
        //    var result = userController.ImportUser(fileMock.Object, "txt");
        //    var okObjectResult = Assert.IsType<ApiErrorResult>(result);
        //    Assert.NotEqual(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, okObjectResult.StatusCode);
        //}

        //[Fact]
        //public void ExportUser_Test()
        //{
        //    var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration);

        //    //var result = userController.ExportUser("txt", "", 1, "desc");


        //    var apiSuccessResult = Assert.IsType<ApiSuccessResult>(result);
        //    Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, apiSuccessResult.StatusCode);

        //    //var okObjectResult = Assert.IsType<FileContentResult>(result);

        //    //Assert.Contains("export_", okObjectResult.FileDownloadName);
        //    ////Assert.Equal("export_", okObjectResult.FileDownloadName);

        //}

        [Fact]
        public void GetAccessibleDoors_Test()
        {

        }

        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        public void Check_CreateTestData_Return_OkObjectResult(int numberOfUser)
        {
            var userController = new UserController(_mockUserService.Object, _mockHttpContext.Object, _configuration, _mockEventLogService.Object);
            var result = userController.CreateTestData(numberOfUser);
            var okObjectResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(Microsoft.AspNetCore.Http.StatusCodes.Status200OK, okObjectResult.StatusCode);           
        }

        public User GetNewUser()
        {
            var fakeUser = new Faker<User>()
                //.RuleFor(u => u.UserCode, f => 1)
                .RuleFor(u => u.CompanyId, f => 1)
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.KeyPadPw, f => Encryptor.Encrypt(f.Random.Replace("########"), _configuration[Constants.Settings.EncryptKey]))
                //.RuleFor(u => u.CardId, f => f.Random.Replace("****************"))
                .RuleFor(u => u.DepartmentId, f => 1)
                .RuleFor(u => u.EffectiveDate, f => DateTime.Now)
                .RuleFor(u => u.IssuedDate, f => DateTime.Now)
                .RuleFor(u => u.ExpiredDate, f => DateTime.Now.AddMonths(12))
                .RuleFor(u => u.Address, (f, u) => f.Address.FullAddress())
                .RuleFor(u => u.Nationality, (f, u) => f.Address.StreetName())
                .RuleFor(u => u.City, (f, u) => f.Address.City())
                .RuleFor(u => u.Position, f => f.Random.Replace("**********"))
                .RuleFor(u => u.PostCode, (f, u) => f.Address.ZipCode())
                .RuleFor(u => u.HomePhone, f => f.Random.Replace("########"))
                .RuleFor(u => u.OfficePhone, f => f.Random.Replace("########"))
                .RuleFor(u => u.Job, (f, u) => f.Person.Company.Name)
                .RuleFor(u => u.Responsibility, (f, u) => f.Person.Company.Bs);
            return fakeUser.Generate();
        }

        public List<User> GetTestUsers()
        {
            var users = new List<User>();
            for (var i = 1; i <= 10; i++)
            {
                var i1 = i;
                var fakeUser = new Faker<User>()
                    .RuleFor(u => u.Id, f => i1)
                    //.RuleFor(u => u.UserCode, f => i1)
                    .RuleFor(u => u.CompanyId, f => 1)
                    .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                    .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                    .RuleFor(u => u.KeyPadPw, f => Encryptor.Encrypt(f.Random.Replace("########"), _configuration[Constants.Settings.EncryptKey]))
                    //.RuleFor(u => u.CardId, f => f.Random.Replace("****************"))
                    .RuleFor(u => u.DepartmentId, f => 1)
                    .RuleFor(u => u.EffectiveDate, f => DateTime.Now)
                    .RuleFor(u => u.IssuedDate, f => DateTime.Now)
                    .RuleFor(u => u.ExpiredDate, f => DateTime.Now.AddMonths(12))
                    .RuleFor(u => u.Address, (f, u) => f.Address.FullAddress())
                    .RuleFor(u => u.Nationality, (f, u) => f.Address.StreetName())
                    .RuleFor(u => u.City, (f, u) => f.Address.City())
                    .RuleFor(u => u.Position, f => f.Random.Replace("**********"))
                    .RuleFor(u => u.PostCode, (f, u) => f.Address.ZipCode())
                    .RuleFor(u => u.HomePhone, f => f.Random.Replace("########"))
                    .RuleFor(u => u.OfficePhone, f => f.Random.Replace("########"))
                    .RuleFor(u => u.Job, (f, u) => f.Person.Company.Name)
                    .RuleFor(u => u.Responsibility, (f, u) => f.Person.Company.Bs);
                var user = fakeUser.Generate();
                users.Add(user);
            }
            return users;
        }
    }
}
