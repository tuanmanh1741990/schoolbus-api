using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.Repository;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class SettingRepositoryTest
    {
        private readonly ISettingRepository _settingRepository;

        public SettingRepositoryTest()
        {
            var settings = TestHelper.GetTestSettings();
            var mockSet = TestHelper.GetMockDbSet(settings);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(c => c.Setting).Returns(mockSet.Object);
            mockContext.Setup(c => c.Set<Setting>()).Returns(mockSet.Object);
            _settingRepository = new SettingRepository(mockContext.Object, null);
        }

        //[Fact]
        //public void Get_ByKey_ReturnSetting()
        //{
        //    var keyTest = "Key";
        //    var result = _settingRepository.GetByKey(keyTest);
        //    Assert.True(result.Id == 1);
        //}

        [Fact]
        public void Get_ByKey_ReturnNull()
        {
            var keyNotExistTest = "KeyNotExist";
            var companyId = 1;
            var result = _settingRepository.GetByKey(keyNotExistTest, companyId);
            Assert.Null(result);
        }

        //[Fact]
        //public void IsKeyExist_SettingModel_ReturnTrue()
        //{
        //    var settingModelTest = new SettingModel
        //    {
        //        Id = 0,
        //        Key = "Key",
        //        Category = "Category",
        //        Value = new List<string> { "Value" }
        //    };
        //    var result = _settingRepository.IsKeyExist(settingModelTest);
        //    Assert.True(result);
        //}

        [Fact]
        public void IsKeyExist_SettingModel_ReturnFalse()
        {
            var settingModelNotExistTest = new SettingModel
            {
                Id = 0,
                Key = "KeyNotExist",
                Category = "Category",
                Value = new List<string> { "Value" }
            };
            var result = _settingRepository.IsKeyExist(settingModelNotExistTest);
            Assert.False(result);
        }
    }
}