﻿using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class BuildingRepositoryTest
    {
        private readonly IBuildingRepository _buildingRepository;

        public BuildingRepositoryTest()
        {
            var buildings = TestHelper.GetTestBuildings();
            var mockSet = TestHelper.GetMockDbSet(buildings);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(c => c.Building).Returns(mockSet.Object);
            mockContext.Setup(c => c.Set<Building>()).Returns(mockSet.Object);
            _buildingRepository = new BuildingRepository(mockContext.Object, null);
        }

        [Fact]
        public void GetByCompanyId_Test()
        {
            var result = _buildingRepository.GetByCompanyId(1);
            Assert.True(result != null);
        }

 
    }
}