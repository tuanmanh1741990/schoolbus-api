﻿using System;
using System.Collections.Generic;
using System.Text;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class UnregistedDeviceRepositoryTest
    {
        private readonly IUnregistedDeviceRepository _accessGroupRepository;
        private readonly List<UnregistedDevice> _unregistedDevices;

        public UnregistedDeviceRepositoryTest()
        {
            _unregistedDevices = TestHelper.GetTestUnregistedDevices();
            var mockSet = TestHelper.GetMockDbSet(_unregistedDevices);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(d => d.UnregistedDevice).Returns(mockSet.Object);
            mockContext.Setup(d => d.Set<UnregistedDevice>()).Returns(mockSet.Object);
            _accessGroupRepository = new UnregistedDeviceRepository(mockContext.Object, null);
        }
    }
}
