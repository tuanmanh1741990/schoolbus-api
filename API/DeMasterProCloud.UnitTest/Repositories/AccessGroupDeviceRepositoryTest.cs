﻿using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using Xunit;
using DeMasterProCloud.Common.Infrastructure;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class AccessGroupDeviceRepositoryTest
    {
        private readonly IAccessGroupDeviceRepository _accessGroupDeviceRepository;
        public AccessGroupDeviceRepositoryTest()
        {
            var accAccessGroupDevices = TestHelper.GetTestAccessGroupDevices();
            var mockSet = TestHelper.GetMockDbSet(accAccessGroupDevices);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(d => d.AccessGroupDevice).Returns(mockSet.Object);
            mockContext.Setup(d => d.Set<AccessGroupDevice>()).Returns(mockSet.Object);
            _accessGroupDeviceRepository = new AccessGroupDeviceRepository(mockContext.Object, null);
        }

        //[Fact]
        //public void GetUserCount_DeviceId_ReturnNumberOfUser()
        //{
        //    var testIcuId = 1;
        //    var result = _accessGroupDeviceRepository.GetUserCount(testIcuId);
        //    Assert.True(result == 2);
        //}

        [Fact]
        public void GetUserCount_DeviceId_ReturnZero()
        {
            var testIcuIdNonExist = 0;
            var result = _accessGroupDeviceRepository.GetUserCount(testIcuIdNonExist);
            Assert.True(result == 0);
        }

        //[Fact]
        //public void HasData_CompanyAndAccessGroup_ReturnTrue()
        //{
        //    var testAccessGroup = 1;
        //    var result = _accessGroupDeviceRepository.HasData(Constants.DefaultCompanyId, testAccessGroup);
        //    Assert.True(result);
        //}

        [Fact]
        public void HasData_CompanyAndAccessGroup_ReturnFalse()
        {
            var testAccessGroupNonExist = 0;
            var result = _accessGroupDeviceRepository.HasData(Constants.DefaultCompanyId, testAccessGroupNonExist);
            Assert.False(result);
        }
    }
}
