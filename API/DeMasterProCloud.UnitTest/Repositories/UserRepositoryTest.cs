using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Repository;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class UserRepositoryTest
    {
        private readonly IUserRepository _userRepository;
        private readonly IConfigurationRoot _configuration;

        public UserRepositoryTest()
        {
            _configuration = TestHelper.GetIConfigurationRoot(Environment.CurrentDirectory);
            var users = TestHelper.GetTestUsers();
            var mockSet = TestHelper.GetMockDbSet(users);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(c => c.User).Returns(mockSet.Object);
            mockContext.Setup(c => c.Set<User>()).Returns(mockSet.Object);
            _userRepository = new UserRepository(mockContext.Object, null);
        }

        //[Fact]
        //public void Get_ValidUser_Return_ListUser_Succcess()
        //{
        //    var result = _userRepository.GetValidUser(1).ToList();
        //    var expected = new List<int> { 1, 2 };
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_ValidUser_Return_ListUser_Fail()
        {
            var result = _userRepository.GetValidUser(10).ToList();
            Assert.True(result.Count == 0);
        }

        //[Fact]
        //public void Get_UsersByCardId_ListUser_Succcess()
        //{
        //    var result = _userRepository.GetUsersByCardId(1, new List<string> { "A", "B", "C", "D", "E" });
        //    var expected = new List<int> { 1, 2, 3, 4 };
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_UsersByCardId_ListUser_Fail()
        {
            var result = _userRepository.GetUsersByCardId(10, new List<string> { "A", "B", "C", "D", "E" });
            Assert.True(result.Count == 0);
        }

        //[Fact]
        //public void Get_ByIds_Return_ListId_Success()
        //{
        //    var result = _userRepository.GetByIds(1, new List<int> { 1, 2, 3, 4, 5 });
        //    var expected = new List<int> { 1, 2 };
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_ByIds_Return_ListId_Fail()
        {
            var result = _userRepository.GetByIds(10, new List<int> { 1, 2, 3, 4, 5 });
            Assert.True(result.Count == 0);
        }

        //[Fact]
        //public void Get_UserCodes_Return_ListUserCodes_Susscess()
        //{
        //    var result = _userRepository.GetUserCodes(1);
        //    var expected = new List<int> { 1, 2, 3, 4 };
        //    var resultNotExpected = result.Select(x => x).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        //[Fact]
        //public void Get_UserCodes_Return_ListUserCodes_Fail()
        //{
        //    var result = _userRepository.GetUserCodes(10);
        //    Assert.True(result.Count == 0);
        //}

        //[Fact]
        //public void Get_ByCardId_Return_User()
        //{
        //    var result = _userRepository.GetByCardId(1, "A");
        //    Assert.True(result != null && result.CardId == "A");
        //}

        //[Fact]
        //public void Get_ByCardId_Return_Null()
        //{
        //    var result = _userRepository.GetByCardId(1, "Y");
        //    Assert.True(result == null);
        //}

        //[Fact]
        //public void Get_ByKeyPadPw_Return_User()
        //{
        //    var result = _userRepository.GetByKeyPadPw(1,
        //        Encryptor.Encrypt("123", _configuration[Constants.Settings.EncryptKey]));
        //    Assert.True(result != null && result.KeyPadPw == Encryptor.Encrypt("123", _configuration[Constants.Settings.EncryptKey]));
        //}

        //[Fact]
        //public void Get_ByKeyPadPw_Return_Null()
        //{
        //    var result = _userRepository.GetByKeyPadPw(10,
        //        Encryptor.Encrypt("123", _configuration[Constants.Settings.EncryptKey]));
        //    Assert.True(result == null);
        //}
        //[Fact]
        //public void Check_IsCardIdExist_CardId_Return_True()
        //{
        //    var result = _userRepository.IsCardIdExist(1, "A");
        //    Assert.True(result);
        //}

        //[Fact]
        //public void Check_IsCardIdExist_CardId_Return_False()
        //{
        //    var result = _userRepository.IsCardIdExist(1, "X");
        //    Assert.False(result);
        //}

        //[Fact]
        //public void Check_IsCardIdExist_UserModel_Return_True()
        //{
        //    var userModel = new UserModel
        //    {
        //        Id = 9,
        //        CardId = "A"
        //    };
        //    var result = _userRepository.IsCardIdExist(1, userModel);
        //    Assert.True(result);
        //}

        //[Fact]
        //public void Check_IsCardIdExist_UserModel_Return_False()
        //{
        //    var userModel = new UserModel
        //    {
        //        Id = 9,
        //        CardId = "X"
        //    };
        //    var result = _userRepository.IsCardIdExist(1, userModel);
        //    Assert.False(result);
        //}

        //[Fact]
        //public void Check_IsUserCodeExist_Return_True()
        //{
        //    var userModel = new UserModel
        //    {
        //        Id = 9,
        //        UserCode = 1
        //    };
        //    var result = _userRepository.IsUserCodeExist(1, userModel);
        //    Assert.True(result);
        //}

        //[Fact]
        //public void Check_IsUserCodeExist_Return_False()
        //{
        //    var userModel = new UserModel
        //    {
        //        Id = 9,
        //        UserCode = 9
        //    };
        //    var result = _userRepository.IsUserCodeExist(1, userModel);
        //    Assert.False(result);
        //}

        //[Fact]
        //public void Check_IsKeyPadPasswordExist_Return_True()
        //{
        //    var userModel = new UserModel
        //    {
        //        Id = 9,
        //        KeyPadPassword = Encryptor.Encrypt("123", _configuration[Constants.Settings.EncryptKey])
        //    };

        //    var result = _userRepository.IsKeyPadPasswordExist(1, userModel.Id, userModel.KeyPadPassword);
        //    Assert.True(result);
        //}

        //[Fact]
        //public void Check_IsKeyPadPasswordExist_Return_False()
        //{
        //    var userModel = new UserModel
        //    {
        //        Id = 9,
        //        KeyPadPassword = Encryptor.Encrypt("999", _configuration[Constants.Settings.EncryptKey])
        //    };

        //    var result = _userRepository.IsKeyPadPasswordExist(1, userModel.Id, userModel.KeyPadPassword);
        //    Assert.False(result);
        //}
    }
}