﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class TimezoneRepositoryTest
    {
        private readonly ITimezoneRepository _timezoneRepository;
        public TimezoneRepositoryTest()
        {
            var timezones = TestHelper.GetTestTimezone();
            var mockSet = TestHelper.GetMockDbSet(timezones);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(d => d.Timezone).Returns(mockSet.Object);
            mockContext.Setup(d => d.Set<Timezone>()).Returns(mockSet.Object);
            _timezoneRepository = new TimezoneRepository(mockContext.Object, null);
        }

        [Fact]
        public void Get_ByIdAndCompany_Return_Timezone()
        {
            var timezoneId = 1;
            var companyId = 1;
            var result = _timezoneRepository.GetByIdAndCompany(timezoneId, companyId);
            Assert.True(result.Id == timezoneId);
            Assert.True(result.Name == "UTC");
            Assert.True(result.Remarks == "UTC");
            Assert.True(result.CompanyId == companyId);
            Assert.True(result.IsDeleted == false);
            Assert.True(result.Position == 3);
        }

        [Fact]
        public void Get_ByIdAndCompany_Return_NonExist()
        {
            var timezoneId = 0;
            var companyId = 1;
            var result = _timezoneRepository.GetByIdAndCompany(timezoneId, companyId);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Get_DefaultTimezone_Return_Timezone()
        //{
        //    var companyId = 1;
        //    var result = _timezoneRepository.GetDefaultTimezone(companyId);
        //    Assert.True(result.Id == 1);
        //    Assert.True(result.Name == "UTC");
        //    Assert.True(result.Remarks == "UTC");
        //    Assert.True(result.CompanyId == companyId);
        //    Assert.True(result.IsDeleted == false);
        //    Assert.True(result.Position == 3);
        //}

        [Fact]
        public void Get_DefaultTimezone_Return_NonExist()
        {
            var companyId = 0;
            var result = _timezoneRepository.GetDefaultTimezone(companyId);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_ByCompany_Return_ListTimezone()
        {
            var companyId = 1;
            var result = _timezoneRepository.GetByCompany(companyId);
            var expected = new List<int> { 1, 2, 4 };
            var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void Get_ByCompany_Return_NonExist()
        {
            var companyId = 0;
            var result = _timezoneRepository.GetByCompany(companyId);
            Assert.True(!result.Any());
        }

        //[Fact]
        //public void Get_PositionsByCompany_Return_ListPosition()
        //{
        //    var companyId = 1;
        //    var result = _timezoneRepository.GetPositionsByCompany(companyId);
        //    var expected = new List<int> { 1, 1, 2 };
        //    var resultNotExpected = result.Select(x => x).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_PositionsByCompany_Return_Non_Exist()
        {
            var companyId = 0;
            var result = _timezoneRepository.GetPositionsByCompany(companyId);
            Assert.True(!result.Any());
        }

        [Fact]
        public void Add_DefaultTimezone_Success()
        {
            var companyId = 1;
            _timezoneRepository.AddDefaultTimezone(companyId);
        }

        //[Fact]
        //public void Get_TimezoneByNameAndCompany_Timezone()
        //{
        //    var companyId = 1;
        //    var name = "UTC";
        //    var result = _timezoneRepository.GetTimezoneByNameAndCompany(companyId, name);
        //    Assert.True(result.Id == 1);
        //    Assert.True(result.Name == name);
        //    Assert.True(result.Remarks == "UTC");
        //    Assert.True(result.CompanyId == companyId);
        //    Assert.True(result.IsDeleted == false);
        //    Assert.True(result.Position == 1);
        //}

        [Fact]
        public void Get_TimezoneByNameAndCompany_Non_Exist()
        {
            var companyId = 1;
            var name = "AMT";
            var result = _timezoneRepository.GetTimezoneByNameAndCompany(companyId, name);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_TimezoneCount_Return_NumberTimezone()
        {
            var companyId = 1;
            var result = _timezoneRepository.GetTimezoneCount(companyId);
            Assert.True(result == 3);
        }

        [Fact]
        public void Get_TimezoneCount_Return_NonExist()
        {
            var companyId = 0;
            var result = _timezoneRepository.GetTimezoneCount(companyId);
            Assert.True(result == 0);
        }

        [Fact]
        public void GetByIdsAndCompany_Return_NonExist()
        {
            var ids = new List<int>() { 1, 2, 4, 5 };
            var companyId = 1;
            var result = _timezoneRepository.GetByIdsAndCompany(ids, companyId);
            var expected = new List<int> { 1, 2, 4 };
            var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }
    }
}
