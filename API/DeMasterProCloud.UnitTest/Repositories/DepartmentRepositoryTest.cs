﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class DepartmentRepositoryTest
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly List<Department> _departments;

        public DepartmentRepositoryTest()
        {
            _departments = TestHelper.GetTestDepartments();
            var mockSet = TestHelper.GetMockDbSet(_departments);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(i => i.Department).Returns(mockSet.Object);
            mockContext.Setup(i => i.Set<Department>()).Returns(mockSet.Object);

            //Initial mapping
            AutoMapperConfig.Initialize();

            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();
            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());

            _departmentRepository = new DepartmentRepository(mockContext.Object, mockHttpContext.Object);
        }

        [Fact]
        public void AddDefault_Test()
        {
            var company = new Company
            {
                Id = 2,
                Name = "3S Intersoft JSC",
            };
            _departmentRepository.AddDefault(company);
        }

        [Fact]
        public void InitDepartment_Test()
        {
            var departmentModel = Mapper.Map<DepartmentModel>(_departments.First());
            _departmentRepository.InitDepartment(departmentModel);
        }

        [Fact]
        public void IsDepartmentNameExist_Test()
        {
            var departmentModel1 = Mapper.Map<DepartmentModel>(_departments.First());
            departmentModel1.Id = 10;//Other id
            var result1 = _departmentRepository.IsDepartmentNameExist(departmentModel1);
            Assert.True(result1);
            var departmentModel2 = Mapper.Map<DepartmentModel>(_departments.First());
            var result = _departmentRepository.IsDepartmentNameExist(departmentModel2);
            Assert.True(result == false);
        }

        [Fact]
        public void IsDepartmentNumberExist_Test()
        {
            var departmentModel1 = Mapper.Map<DepartmentModel>(_departments.First());
            departmentModel1.Id = 10;//a Other id
            var result1 = _departmentRepository.IsDepartmentNumberExist(departmentModel1);
            Assert.True(result1);
            var departmentModel2 = Mapper.Map<DepartmentModel>(_departments.First());
            var result2 = _departmentRepository.IsDepartmentNumberExist(departmentModel2);
            Assert.True(result2 == false);
        }

        [Fact]
        public void GetByIdAndCompanyId_Test()
        {
            var department = _departments.First();
            var result = _departmentRepository.GetByIdAndCompanyId(department.Id, department.CompanyId);
            Assert.True(result.DepartName == department.DepartName);
            Assert.True(result.DepartNo == department.DepartNo);
        }

        [Fact]
        public void GetByIdsAndCompanyId_Test()
        {
            var department1 = _departments.First();
            var department2 = _departments.ElementAt(1);
            var result = _departmentRepository.GetByIdsAndCompanyId(new List<int> { department1.Id, department2.Id }, 1);
            Assert.True(result[0].DepartName == department1.DepartName);
            Assert.True(result[1].DepartName == department2.DepartName);
            Assert.True(result[0].DepartNo == department1.DepartNo);
            Assert.True(result[1].DepartNo == department2.DepartNo);
        }

        //[Fact]
        //public void GetDefautDepartmentByCompanyId_Test()
        //{
        //    var department = _departments.First();
        //    var result = _departmentRepository.GetDefautDepartmentByCompanyId(department.Id);
        //    Assert.True(result.DepartName == department.DepartName);
        //    Assert.True(result.DepartNo == department.DepartNo);
        //}

        //[Fact]
        //public void GetDepartmentHierarchy_Test()
        //{
        //    var result = _departmentRepository.GetDepartmentHierarchy();
        //    Assert.True(result.Count() == 1);
        //}

        [Fact]
        public void DeleteRange_Test()
        {
            var departments = _departments.ToList();
            _departmentRepository.DeleteRange(departments);
        }
    }
}
