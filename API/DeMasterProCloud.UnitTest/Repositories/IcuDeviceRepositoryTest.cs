﻿using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class IcuDeviceRepositoryTest
    {
        private readonly IIcuDeviceRepository _icuDeviceRepository;
        public IcuDeviceRepositoryTest()
        {
            var icuDevice = TestHelper.GetTestIcuDevice();
            var mockSet = TestHelper.GetMockDbSet(icuDevice);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(i => i.IcuDevice).Returns(mockSet.Object);
            mockContext.Setup(i => i.Set<IcuDevice>()).Returns(mockSet.Object);
            _icuDeviceRepository = new IcuDeviceRepository(mockContext.Object, null);
        }

        [Fact]
        public void Get_DeviceByCompanyAndAddress_Return_IcuDeviceModel()
        {
            var icuAddressTest = "0D9038";
            var result = _icuDeviceRepository.GetDeviceByCompanyAndAddress(1, icuAddressTest);
            Assert.True(result != null && result.Id == 1);
        }

        [Fact]
        public void Get_DeviceByCompanyAndAddress_Return_NonExist()
        {
            var icuAddressTest = "196E68";
            var result = _icuDeviceRepository.GetDeviceByCompanyAndAddress(1, icuAddressTest);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Get_ActiveDeviceByCompanyAndAddress_Return_IcuDevice()
        //{
        //    var icuAddressTest = "0D9038";
        //    var result = _icuDeviceRepository.GetActiveDeviceByCompanyAndAddress(1, icuAddressTest);
        //    Assert.True(result != null && result.Id == 1);
        //}

        [Fact]
        public void Get_ActiveDeviceByCompanyAndAddress_Return_NonExist()
        {
            var icuAddressTest = "196E68";
            var result = _icuDeviceRepository.GetActiveDeviceByCompanyAndAddress(1, icuAddressTest);
            Assert.True(result == null);
        }

        [Fact]
        public void Get_ByIdsAndCompany_Return_ListIcuDevice_Success()
        {
            List<int> icuIds = new List<int> { 1, 2, 3, 4 };
            var result = _icuDeviceRepository.GetByIdsAndCompany(icuIds, 1);
            var expected = new List<int> { 1, 2, 3 };
            var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
            var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
            Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        }

        [Fact]
        public void Get_ByIdsAndCompany_Return_ListIcuDevice_Fail()
        {
            List<int> icuIds = new List<int> { 1, 2, 3, 4 };
            var result = _icuDeviceRepository.GetByIdsAndCompany(icuIds, 10);
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void Get_ByIcuId_Return_IcuDevice()
        {
            var result = _icuDeviceRepository.GetByIcuId(1);
            Assert.True(result != null && result.Id == 1);
        }

        [Fact]
        public void Get_ByIcuId_Return_NonExist()
        {
            var result = _icuDeviceRepository.GetByIcuId(10);
            Assert.True(result == null);
        }

        //[Fact]
        //public void Get_DevicesByCompany_Return_List_IcuDevice_Success()
        //{
        //    var result = _icuDeviceRepository.GetDevicesByCompany(1);
        //    var expected = new List<int> { 1, 2, 3 };
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_DevicesByCompany_Return_List_IcuDevice_Fail()
        {
            var result = _icuDeviceRepository.GetDevicesByCompany(100);
            Assert.True(result.Count == 0);
        }

        //[Fact]
        //public void Get_ActiveDevicesByCompany_Return_List_IcuDevice_Success()
        //{
        //    var result = _icuDeviceRepository.GetActiveDevicesByCompany(1).ToList();
        //    var expected = new List<int> { 1, 3 };
        //    var resultNotExpected = result.Select(x => x.Id).Except(expected).ToList();
        //    var expectedNotResult = expected.Except(result.Select(x => x.Id)).ToList();
        //    Assert.True(!resultNotExpected.Any() && !expectedNotResult.Any());
        //}

        [Fact]
        public void Get_ActiveDevicesByCompany_Return_List_IcuDevice_Fail()
        {
            var result = _icuDeviceRepository.GetActiveDevicesByCompany(100).ToList();
            Assert.True(result.Count == 0);
        }
    }
}
