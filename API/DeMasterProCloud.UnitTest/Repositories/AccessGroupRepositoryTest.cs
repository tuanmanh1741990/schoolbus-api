﻿using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using Xunit;
using DeMasterProCloud.Common.Infrastructure;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class AccessGroupRepositoryTest
    {
        private readonly IAccessGroupRepository _accessGroupRepository;
        private readonly List<AccessGroup> _accessGroups;

        public AccessGroupRepositoryTest()
        {
            _accessGroups = TestHelper.GetTestAccessGroups();
            var mockSet = TestHelper.GetMockDbSet(_accessGroups);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(d => d.AccessGroup).Returns(mockSet.Object);
            mockContext.Setup(d => d.Set<AccessGroup>()).Returns(mockSet.Object);
            _accessGroupRepository = new AccessGroupRepository(mockContext.Object, null);
        }

        [Fact]
        public void GetIdAndCompany_ReturnAccessGroup()
        {
            var result =
                _accessGroupRepository.GetByIdAndCompanyId(Constants.DefaultCompanyId, _accessGroups.First().Id);
            Assert.True(result.Name == _accessGroups.First().Name);
            Assert.True(result.IsDefault == _accessGroups.First().IsDefault);
        }

        [Fact]
        public void GetIdAndCompany_ReturnNull()
        {
            var result =
                _accessGroupRepository.GetByIdAndCompanyId(2, _accessGroups.First().Id);
            Assert.True(result == null);
        }

        [Fact]
        public void GetIdsAndCompany_ReturnAccessGroup()
        {
            var result =
                _accessGroupRepository.GetByIdsAndCompanyId(Constants.DefaultCompanyId, _accessGroups.Select(x => x.Id).ToList());
            Assert.True(result.Count == 3);
        }

        [Fact]
        public void GetIdsAndCompany_ReturnNull()
        {
            var result =
                _accessGroupRepository.GetByIdsAndCompanyId(2, _accessGroups.Select(x => x.Id).ToList());
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void GetListAccessGroup_ReturnAccessGroup()
        {
            var result =
                _accessGroupRepository.GetListAccessGroups(Constants.DefaultCompanyId);
            Assert.True(result.Count == 3);
        }

        [Fact]
        public void GetListAccessGroup_ReturnNull()
        {
            var result =
                _accessGroupRepository.GetListAccessGroups(2);
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void GetAccessGroupUnSetDefault_ReturnAccessGroup()
        {
            var result =
                _accessGroupRepository.GetAccessGroupUnSetDefault(Constants.DefaultCompanyId, 1);
            Assert.True(result.Count == 2);
        }

        [Fact]
        public void GetAccessGroupUnSetDefault_ReturnNull()
        {
            var result =
                _accessGroupRepository.GetAccessGroupUnSetDefault(2, 1);
            Assert.True(result.Count == 0);
        }

        [Fact]
        public void GetNoAccessGroup_ReturnAccessGroup()
        {
            var result = _accessGroupRepository.GetNoAccessGroup(Constants.DefaultCompanyId);
            Assert.True(result != null);
            Assert.True(result.Type == (short)AccessGroupType.NoAccess);
        }

        [Fact]
        public void GetNoAccessGroup_ReturnNull()
        {
            var result = _accessGroupRepository.GetNoAccessGroup(2);
            Assert.True(result == null);
        }

        [Fact]
        public void GetFullAccessGroup_ReturnAccessGroup()
        {
            var result = _accessGroupRepository.GetFullAccessGroup(Constants.DefaultCompanyId);
            Assert.True(result != null);
            Assert.True(result.Type == (short)AccessGroupType.FullAccess);
        }

        [Fact]
        public void HasExistName_ReturnTrue()
        {
            var result = _accessGroupRepository.HasExistName(5, _accessGroups.First().Name);
            Assert.True(result);
        }

        [Fact]
        public void HasExistName_ReturnFalse()
        {
            var result = _accessGroupRepository.HasExistName(_accessGroups.First().Id, _accessGroups.First().Name);
            Assert.True(result == false);
        }

        [Fact]
        public void GetDefaultAccessGroup_CompanyId_ReturnAccessGroup()
        {
            var result = _accessGroupRepository.GetDefaultAccessGroup(Constants.DefaultCompanyId);
            Assert.True(result.IsDefault);
        }

        [Fact]
        public void GetDefaultAccessGroup_CompanyId_ReturnNull()
        {
            var result = _accessGroupRepository.GetDefaultAccessGroup(0);
            Assert.True(result == null);
        }
    }
}
