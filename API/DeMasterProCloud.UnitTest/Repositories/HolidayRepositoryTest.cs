﻿using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class HolidayRepositoryTest
    {
        private readonly IHolidayRepository _holidayRepository;
        public HolidayRepositoryTest()
        {
            var holidays = TestHelper.GetTestHolidays();
            var mockSet = TestHelper.GetMockDbSet(holidays);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(i => i.Holiday).Returns(mockSet.Object);
            mockContext.Setup(i => i.Set<Holiday>()).Returns(mockSet.Object);
            _holidayRepository = new HolidayRepository(mockContext.Object, null);

        }

        [Fact]
        public void GetHolidayByCompany_Test_ReturnHolidays()
        {
            var result = _holidayRepository.GetHolidayByCompany(1);
            Assert.True(result != null);
        }

        [Fact]
        public void GetHolidayByCompany_Test_ReturnNull()
        {
            var result = _holidayRepository.GetHolidayByCompany(0);
            Assert.True(result.Count == 0);
            
        }

        [Fact]
        public void GetHolidayByIdAndCompany_Test_ReturnHoliday()
        {
            var result = _holidayRepository.GetHolidayByIdAndCompany(1, 1);
            Assert.True(result != null);
        }

        [Fact]
        public void GetHolidayByIdAndCompany_Test_ReturnNull()
        {
            var result = _holidayRepository.GetHolidayByIdAndCompany(0, 0);
            Assert.True(result == null);
        }

        [Fact]
        public void GetHolidayByNameAndCompany_Test_ReturnHoliday()
        {
            var result = _holidayRepository.GetHolidayByNameAndCompany(1, "Duali");
            Assert.True(result != null);
        }

        [Fact]
        public void GetHolidayByNameAndCompany_Test_ReturnNull()
        {
            var result = _holidayRepository.GetHolidayByNameAndCompany(0, "Duali");
            Assert.True(result == null);
        }

        [Fact]
        public void GetHolidayCount_Test()
        {
            var result = _holidayRepository.GetHolidayCount(1);
            Assert.True(result == 2);
        }
    }
}
