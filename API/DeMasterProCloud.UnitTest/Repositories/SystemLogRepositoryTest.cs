﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class SystemLogRepositoryTest
    {
        private readonly ISystemLogRepository _systemLogRepository;
        public SystemLogRepositoryTest()
        {
            var systemLogs = TestHelper.GetTestSystemLogs();
            var mockSet = TestHelper.GetMockDbSet(systemLogs);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(i => i.SystemLog).Returns(mockSet.Object);
            mockContext.Setup(i => i.Set<SystemLog>()).Returns(mockSet.Object);


            //Mock HttpContext
            var mockHttpContext = new Mock<IHttpContextAccessor>();
            mockHttpContext.Setup(x => x.HttpContext).Returns(TestHelper.GetTestHttpContext());
            
            _systemLogRepository = new SystemLogRepository(mockContext.Object, mockHttpContext.Object);


        }

        [Fact]
        public void Add_Test()
        {

            var company = new Company()
            {
                Id = 1,
                Code = "p000001",
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                ExpiredFrom = DateTime.Now,
                ExpiredTo = DateTime.Now.AddDays(10),
                Name = "3si",
                IsDeleted = false,
                UpdatedBy = 1,
                UpdatedOn = DateTime.Now,
                //Timezone = GetTestTimezone(),
            };

            var account = new Account
            {
                Id = 1,
                Username = "a@gmail.com",
                Password = "10000:2t6nUATDI1XYPQHsgokJba5uOhltTqTJ/VqhXLbyWC9o3pmP",
                CompanyId = 1,
                RootFlag = true,
                IsDeleted = false,
                Company = company,
                Type = 2
            };

            var systemLog = new SystemLog()
            {
                Id = 4,
                Action = 1,
                CompanyId = 1,
                Content = "test4",
                ContentDetails = "ContentDetails4",
                ContentIds = "{\"Id\":4,\"assigned_ids\":null}",
                CreatedBy = 1,
                OpeTime = DateTime.Now,
                Type = 1,
                Company = company,
                CreatedByNavigation = account,
            };
            _systemLogRepository.Add(systemLog);
            Assert.True(true);
            
        }
    }
}
