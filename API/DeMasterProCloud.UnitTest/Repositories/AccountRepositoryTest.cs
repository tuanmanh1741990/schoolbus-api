using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Moq;
using Xunit;

namespace DeMasterProCloud.UnitTest.Repositories
{
    public class AccountRepositoryTest
    {
        private readonly IAccountRepository _accountRepository;

        public AccountRepositoryTest()
        {
            var accounts = TestHelper.GetTestAccounts();
            var mockSet = TestHelper.GetMockDbSet(accounts);
            var mockContext = new Mock<AppDbContext>();
            mockContext.Setup(c => c.Account).Returns(mockSet.Object);
            mockContext.Setup(c => c.Set<Account>()).Returns(mockSet.Object);
            _accountRepository = new AccountRepository(mockContext.Object, null);
        }

        [Fact]
        public void Get_RootAccountByCompany_Return_Account()
        {
            var result = _accountRepository.GetRootAccountByCompany(1);
            Assert.True(result != null /*&& result.RootFlag*/);
        }

        [Fact]
        public void Get_RootAccountByCompany_Return_NonExist()
        {
            var result = _accountRepository.GetRootAccountByCompany(3);
            Assert.False(result != null /*&& result.RootFlag*/);
        }

        [Fact]
        public void AddOrUpdate_DefaultAccount_Return_Account()
        {
            var company = new Company
            {
                Id = 1
            };
            var result = _accountRepository.AddOrUpdateDefaultAccount(company, "a@gmail.com", null);
            Assert.True(result != null);
        }

        [Fact]
        public void Check_IsExist_Return_True()
        {
            var result = _accountRepository.IsExist(2, "a@gmail.com", 1);
            Assert.True(result);
        }

        [Fact]
        public void Check_IsExist_Return_False_WhenNonExistedInCompany()
        {
            var result = _accountRepository.IsExist(1, "a@gmail.com", 1);
            Assert.False(result);
        }

        [Fact]
        public void Check_IsExist_Return_False_WhenNonExistedUsername()
        {
            var result = _accountRepository.IsExist(1, "x@gmail.com", 1);
            Assert.False(result);
        }

        [Fact]
        public void Get_ById_Return_Account()
        {
            var result = _accountRepository.GetById(1);
            Assert.True(result != null);
        }

        [Fact]
        public void Get_ById_Return_NonExist()
        {
            var result = _accountRepository.GetById(9);
            Assert.False(result != null);
        }
    }
}