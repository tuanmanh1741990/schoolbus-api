﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.Csv;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.EventLog;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// User controller
    /// </summary>
    [Produces("application/ms-excel", "application/json", "application/text")]
    public class UserController : Controller
    {
        private readonly IEventLogService _eventLogService;
        private readonly IUserService _userService;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly string[] _headers = {
            "Request",
            "Number of user",
            "Time receiving request (t1)",
            "Time finishing request (t2)",
            "Processing time in seconds (t2-t1)"
        };

        /// <summary>
        /// UserController constructor
        /// </summary>
        /// <param name="userService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="configuration"></param>
        /// <param name="eventLogService"></param>
        public UserController(IUserService userService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IEventLogService eventLogService)
        {
            _userService = userService;
            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
            _eventLogService = eventLogService;
        }

        /// <summary>
        /// Get user list
        /// </summary>
        /// <param name="search">검색어 필터</param>
        /// <param name="pageNumber">페이지 넘버</param>
        /// <param name="pageSize">페이지 사이즈</param>
        /// <param name="sortColumn">sorting할 컬럼 Idx</param>
        /// <param name="sortDirection">asc or desc</param>
        /// <param name="isValid">valid user or all user</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUsers)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Gets(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 1,
            string sortDirection = "desc", bool isValid = true, int departmentId = 0)
        {
            var users = _userService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered, isValid, departmentId).AsEnumerable().Select(Mapper.Map<UserListModel>).ToList();

            foreach (var user in users)
            {
                user.CardList = _userService.GetCardListByUserId(user.Id);
            }

            var pagingData = new PagingData<UserListModel>
            {
                Data = users,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get user by id. In the case id = 0 then just getting initial data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUsersId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new UserDataModel();
            if (id != 0)
            {
                var user = _userService.GetById(id);
                if (user == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                model = Mapper.Map<UserDataModel>(user);
            }
            _userService.InitData(model);
            return Ok(model);
        }


        /// <summary>
        /// Get Card Count by accessGroup ID
        /// </summary>
        /// <param name="id"> user identifier. </param>
        /// <returns> IActionResult about card count that specific user has </returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUsersCardCount)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetCardCount(int id)
        {
            //var model = new UserDataModel();
            //if (id != 0)
            //{
            //    var user = _userService.GetById(id);
            //    if (user == null)
            //    {
            //        return new ApiErrorResult(StatusCodes.Status404NotFound);
            //    }
            //    model = Mapper.Map<UserDataModel>(user);
            //}

            int cardCount = _userService.GetCardCount(id);
            return Ok(cardCount);
        }


        /// <summary>
        /// Function to reboot test
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiUsersTest)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult RebootTest(string cmd)
        {

            Process.Start(cmd);

            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, UserResource.lblUser));
        }

        /// <summary>
        /// Add new a user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiUsers)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]UserModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            // Checking if email is existed in system.
            var valid = _userService.IsDuplicatedAccountCreated(model.Email);
            if (!valid)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, AccountResource.lblEmail));
            }
            
            // Checking if card is existed in system.
            if (model.CardList != null && model.CardList.Any())
            {
                foreach (var card in model.CardList)
                {
                    var isCardIdExist = _userService.IsCardIdExist(card.CardId);

                    if (isCardIdExist)
                    {
                        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, UserResource.lblCardId));
                    }
                }
            }

            if (model.PermissionType != (short)PermissionType.NotUse && string.IsNullOrEmpty(model.Email))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.EmailDoestNotExist));
            }
            var user = _userService.Add(model);
            //automation add qr code if Plugin qrcode = true
            var listUserByAccountID = _userService.GetUserByUserId(user);
            _userService.AutomationCreateQrCode(listUserByAccountID);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, UserResource.lblUser), user.ToString());
        }

        /// <summary>
        /// Edit user by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiUsersId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]UserModel model)
        {

            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            //email 중복 체크
            if (model.PermissionType == (int)PermissionType.SystemAdmin)
            {
                var account = _userService.GetAccountByEmail(model.Email);
                if (account != null && _userService.IsAccountExist(account.Id, model.Email))
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, AccountResource.lblUsername));
                }
            }

            var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (user == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (model.Email != null)
            {
                var account = _userService.GetAccountByUserName(user.CompanyId, model.Email);
                if (account != null)
                {
                    var alreadyLinkAccount = _userService.GetAccountAlreadyLinkToUser(user.CompanyId, account.Id);
                    if (alreadyLinkAccount != null )
                    {
                        if (alreadyLinkAccount.Id != user.Id)
                        {
                            return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity,
                                string.Format(MessageResource.Exist, AccountResource.lblEmail));
                        }
                    }
                }
                
            }

            if (model.PermissionType != (short)PermissionType.NotUse && string.IsNullOrEmpty(model.Email))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.EmailDoestNotExist));
            }

            _userService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblUser, ""));
        }


        ///// <summary>
        ///// Add a new card to exist user
        ///// </summary>
        ///// <param name="id">exist user Id</param>
        ///// <param name="model">card model</param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route(Constants.Route.ApiUsersAddNewCard)]
        //[Authorize(Policy = Constants.Policy.PrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public IActionResult AddNewCard(int id, [FromBody]CardModel model)
        //{
        //    // check if the user is not null and not deleted
        //    var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
        //    // if user is null or deleted, return error No.404
        //    if (user == null)
        //    {
        //        return new ApiErrorResult(StatusCodes.Status404NotFound);
        //    }
        //    // if model is not null, check for duplicated the card id
        //    if (model == null)
        //    {
        //        // if model is null, return error No.404
        //        return new ApiErrorResult(StatusCodes.Status404NotFound);
        //    }
        //    else
        //    {
        //        var isCardExist = _userService.IsCardIdExist(model.CardId);
        //        // if card is in this company, return error No.422
        //        if (isCardExist)
        //        {
        //            return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, UserResource.lblCardId));
        //        }
        //        // if card is not in this company, update the exist user information
        //        _userService.AddCard(user, model);

        //        // and return success.
        //        return new ApiSuccessResult(StatusCodes.Status200OK, string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblUser, ""));
        //    }
        //}

        /// <summary>
        /// Delete a user by Id
        /// </summary>
        /// <param name="id">Id of UserLogin to delete</param>
        /// <returns>Json reslut object</returns>
        [HttpDelete]
        [Route(Constants.Route.ApiUsersId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (user == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _userService.Delete(user);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, UserResource.lblUser));
        }

        /// <summary>
        /// Delete multiple user
        /// </summary>
        /// <param name="ids">List user id to delete</param>
        /// <returns>Json result object</returns>
        [HttpDelete]
        [Route(Constants.Route.ApiUsers)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var users = _userService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId());
            if (users == null || !users.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _userService.DeleteRange(users);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, UserResource.lblUser));
        }


        /// <summary>
        /// Get Door List for 1 user
        /// </summary>
        /// <param name="id">List user id to Accessible Doors</param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="isValid"></param>
        /// <returns>Json result object</returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUsersAccessibleDoors)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AccessibleDoors(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc", bool isValid = true)
        {
            var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (user == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var doors = _userService.GetPaginatedAccessibleDoors(user, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered);

            var pagingData = new PagingData<AccessibleDoorModel>
            {
                Data = doors,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Export Door List for 1 user
        /// <param name="id">List user id to Accessible Doors</param>
        /// </summary>
        /// <param name="search"></param>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Json result object</returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUsersAccessibleDoorsExport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ExportAccessibleDoors(int id, string search, string type = "excel", int sortColumn = 0,
            string sortDirection = "desc")
        {
            var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (user == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var fileData = _userService.ExportAccessibleDoors(user, type, search, out var totalRecords, out var recordsFiltered, sortColumn, sortDirection);
            var filename = string.Format(Constants.ExportFileFormat, UserResource.lblExportAccessibleDoor + $"_{user.FirstName + "_" + user.LastName}", DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";
            if (totalRecords == 0 || recordsFiltered == 0)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageExportDataIsZero, UserResource.lblAccessibleDoors));
            }

            return File(fileData, type.Equals("excel") ? "application/ms-excel" : "application/text", fullName);
        }



        /// <summary>
        /// Generate test data
        /// </summary>
        /// <param name="numberOfUser"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/users/create-test-data")]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CreateTestData(int numberOfUser)
        {
            if (numberOfUser == 0)
            {
                return Ok(new { message = "No user data is created!" });
            }

            var stopWatch = Stopwatch.StartNew();
            _userService.GenerateTestData(numberOfUser);
            stopWatch.Stop();
            Trace.WriteLine($"Elapsed time {stopWatch.ElapsedMilliseconds} ms");
            return Ok(new
            {
                message =
                    $"{numberOfUser} user(s) data were created successfully in {TimeSpan.FromMilliseconds(stopWatch.ElapsedMilliseconds).TotalSeconds} seconds!"
            });
        }

        /// <summary>
        /// Import users data
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiUsersImport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ImportUser(IFormFile file, string type = "excel")
        {
            if (file.Length == 0)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound, MessageResource.msgFailLengthFile);
            }
            var fileType = FileHelpers.GetFileExtension(file);
            var excelExtension = new[] { ".xls", ".xlsx" };
            var txtExtension = new[] { ".txt" };
            if (type.Equals("excel", StringComparison.OrdinalIgnoreCase) && !excelExtension.Contains(fileType) ||
                type.Equals("txt", StringComparison.OrdinalIgnoreCase) && !txtExtension.Contains(fileType))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(MessageResource.msgErrorFileExtension, type));
            }

            var result = _userService.ImportFile(type, file);
            if (!result.Result)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, result.Message);
            }
            else
            {
                return new ApiSuccessResult(StatusCodes.Status200OK, result.Message);
            }
        }

        /// <summary>
        /// Export user data
        /// </summary>
        /// <param name="type"></param>
        /// <param name="search"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [Produces("application/text", "application/ms-excel")]
        [HttpGet]
        [Route(Constants.Route.ApiUsersExportProjectD)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ExportUser(string type = "excel", string search = "", int sortColumn = 1, string sortDirection = "desc")
        {
            var fileData = _userService.Export(type, search, sortColumn, sortDirection, out var totalRecords, out var recordsFiltered);
            var filename = string.Format(Constants.ExportFileFormat, UserResource.lblExport + "_User", DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";
            if (totalRecords == 0 || recordsFiltered == 0)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageExportDataIsZero, UserResource.lblUser));
            }

            return File(fileData, type.Equals("excel") ? "application/ms-excel" : "application/text", fullName);
        }

        /// <summary>
        /// Write performance csv data
        /// </summary>
        /// <param name="numberOfUser"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        [NonAction]
        private void WritePerformanceCsvData(int numberOfUser, DateTime startTime, DateTime endTime)
        {
            var enableLog = Convert.ToBoolean(_configuration[Constants.Settings.EnablePerformanceLog]);
            if (enableLog)
            {
                var csvFile = $"GetUser_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
                var contents = new[]
                {
                    _httpContext.Request.GetEncodedUrl(),
                    numberOfUser.ToString(),
                    startTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),
                    endTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),
                    endTime.Subtract(startTime).TotalSeconds.ToString(CultureInfo.InvariantCulture)
                };
                CsvHelper.Write(_headers, new List<string[]> { contents }, csvFile);
            }
        }

        /// <summary>
        /// Get List of card types.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiCardTypes)]
        public IActionResult GetListCardType()
        {
            var type = typeof(CardType);
            var data = Enum
                .GetNames(type)
                .Select(name => new
                {
                    Id = (int)Enum.Parse(type, name),
                    Name = name
                })
                .ToArray();
            return Ok(data);
        }

        /// <summary>
        /// Get card by user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiUserIdentification)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetCardsByUser(int id)
        {
            if (id != 0)
            {
                var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                if (user != null)
                {
                    var cards = _userService.GetCardListByUserId(user.Id);
                    return Ok(cards);
                }
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }

        ///// <summary>
        ///// Create card by user
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route(Constants.Route.ApiUserIdentification)]
        //[Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public IActionResult CreateCardsByUser(int id, [FromBody]CardModel model)
        //{
        //    if (id != 0)
        //    {
        //        var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
        //        if (user != null)
        //        {
        //            var isCardIdExist = _userService.IsExistGetCardByUser(id, model.Id);

        //            if (isCardIdExist)
        //            {
        //                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, UserResource.lblCardId));
        //            }
        //            var card = _userService.AddCardByUser(id, model);
        //            return new ApiSuccessResult(StatusCodes.Status200OK,
        //                string.Format(MessageResource.MessageAddSuccess, UserResource.lblCard), card.CardId);
        //        }
        //    }
        //    return new ApiErrorResult(StatusCodes.Status404NotFound);
        //}

        /// <summary>
        /// Add a new identification to exist user
        /// </summary>
        /// <param name="id">exist user Id</param>
        /// <param name="model">card model</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiUserIdentification)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AddNewIdentification(int id, [FromBody]CardModel model)
        {
            if (model != null)
            {
                ModelState.Clear();
                // Checking validation is failed.
                if (!TryValidateModel(model))
                {
                    return new ValidationFailedResult(ModelState);
                }
                // Checking validation is succeed.
                else
                {
                    if (id == 0)
                    {
                        return new ApiSuccessResult(StatusCodes.Status200OK);
                    }
                    else
                    {
                        var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                        // if user is null or deleted, return error No.404
                        if (user == null)
                        {
                            return new ApiErrorResult(StatusCodes.Status404NotFound);
                        }
                        else
                        {
                            var isCardExist = _userService.IsCardIdExist(model.CardId);
                            // if card is in this company, return error No.422
                            if (isCardExist)
                            {
                                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, UserResource.lblCardId));
                            }

                            if (model.CardType == (short)CardType.QrCode)
                            {
                                var exitedQr = _userService.GetQrCodeBelongToUser(user.Id);
                                if (!exitedQr)
                                {
                                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.ExistedQrCode));
                                }
                            }

                            if (model.CardType == (short)CardType.PassCode)
                            {
                                var exitedPassCode = _userService.GetPassCodeByUser(user.Id, _httpContext.User.GetCompanyId());
                                if (exitedPassCode != null)
                                {
                                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.ExistedPassCode));
                                }
                            }

                            // if card is not in this company, update the exist user information
                            var card = _userService.AddIdentification(user, model);

                            // If the function of the service returns null, it is a failure.
                            if (card.CardId == null)
                            {
                                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.MsgFail));
                            }
                            // If returned result is not null, it is a success. So API returns success.
                            else
                            {
                                return new ApiSuccessResult(StatusCodes.Status200OK, string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblUser, model.CardType == (short)CardType.QrCode ? UserResource.lblQR : UserResource.lblCard), card.CardId);
                            }
                        }
                    }
                }
            }
            else
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.InvalidInformation);
            }
            
        }

        /// <summary>
        /// Get card by user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiCardByUser)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetCardByUser(int id, int cardId)
        {
            if (id != 0)
            {
                var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                if (user != null)
                {
                    var card = _userService.GetCardByUser(id, cardId);
                    return Ok(card);
                }
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }

        /// <summary>
        /// Update by user
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cardId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiCardByUser)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult UpdateCardByUser(int id, int cardId, [FromBody]CardModel model)
        {
            if (id != 0)
            {
                var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                if (user != null)
                {
                    var isCardIdExist = _userService.IsExistGetCardByUser(id, cardId);

                    if (isCardIdExist)
                    {
                        _userService.UpdateCardByUser(id, cardId, model);
                        return new ApiSuccessResult(StatusCodes.Status200OK,
                            string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblCard));
                    }
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }

        /// <summary>
        /// Delete identification
        /// </summary>
        /// <param name="id">user index number</param>
        /// <param name="cardId">index number of card, not cardId</param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiCardByUser)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteCardByUser(int id, int cardId)
        {
            if (id != 0)
            {
                // check if the user is not null and not deleted
                var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                // if user is null or deleted, return error No.404
                if (user == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                else
                {
                    // Check for exist the card belong to user
                    var isCardExist = _userService.IsExistGetCardByUser(id, cardId);
                    // if card is not belong to user, return error No.404
                    if (!isCardExist)
                    {
                        return new ApiErrorResult(StatusCodes.Status404NotFound);
                    }
                    // if card is belong to user, delete the card from user
                    else
                    {
                        _userService.DeleteCardByUser(user, cardId);

                        return new ApiSuccessResult(StatusCodes.Status200OK, string.Format(MessageResource.MessageDeleteSuccess, UserResource.lblCard));
                    }
                }
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
        
        /// <summary>
        /// Get Dynamic QR by user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiGetDynamicQrByUser)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdminAndEmployee, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.QrCode)]
        public IActionResult GetDynamicQrByUser()
        {
            var user = _userService.GetUserByAccountId(_httpContext.User.GetAccountId());
            if (user != null)
            {
                var identification = _userService.GetQrByUserId(user.Id);
                if (identification != null)
                {
                    var code = _userService.GetDynamicQrCode(user.Id, identification.CardId);
                
                    var duration  = new Random();
                    return Ok(new DynamicQr
                    {
                        QrCode = code,
                        Duration = duration.Next(5, 16)
                    });
                }
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
        
        /// <summary>
        /// Assign User to Defalut working time
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.AssignUserToDeFaultWorkingTime)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.TimeAttendance)]
        public IActionResult AssignUserToDefaultWorkingTime()
        {
            _userService.AssignUserToDefaultWorkingTime();
            return Ok();
        }
        
        /// <summary>
        /// View Access history Attendance
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessHistoryUser)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.TimeAttendance)]
        public IActionResult AccessHistoryAttendance(int id, DateTime start, DateTime end, int pageNumber = 1, int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            var user = _userService.GetById(id);
            if (user != null)
            {
                var accessHistory = _eventLogService
                    .GetAccessHistoryAttendance(id, start, end, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                        out var recordsFiltered).AsEnumerable().Select(Mapper.Map<EventLogListModel>).ToList();
                
                var pagingData = new PagingData<EventLogListModel>
                {
                    Data = accessHistory,
                    Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
                };
                
                return Ok(pagingData);
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
        
        /// <summary>
        /// View Access history Attendance Each user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessHistoryEachUser)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdminAndEmployee, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.TimeAttendance)]
        public IActionResult AccessHistoryAttendanceEachUser(DateTime start, DateTime end, int pageNumber = 1, int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            var user = _userService.GetUserByAccountId(_httpContext.User.GetAccountId());
            if (user != null)
            {
                var accessHistory = _eventLogService
                    .GetAccessHistoryAttendance(user.Id, start, end, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                        out var recordsFiltered).AsEnumerable().Select(Mapper.Map<EventLogListModel>).ToList();
                
                var pagingData = new PagingData<EventLogListModel>
                {
                    Data = accessHistory,
                    Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
                };
                
                return Ok(pagingData);
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
        
        /// <summary>
        /// Validation Dynamic QR by user
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiValidationDynamicQrByUser)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdminAndEmployee, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.QrCode)]
        public IActionResult ValidationDynamicQrByUser(string dynamicQr, string rid, string actionType)
        {
            if (!string.IsNullOrEmpty(dynamicQr) && !string.IsNullOrEmpty(rid))
            {
                var user = _userService.GetUserByAccountId(_httpContext.User.GetAccountId());
                var validDevice = _userService.GetDeviceFromCompany(rid);
                if (!validDevice)
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, MessageResource.InvalidDevice);
                }
                
                if (dynamicQr.Substring(0, Constants.DynamicQr.NameProject.Length) != Constants.DynamicQr.NameProject)
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, MessageResource.InvalidQrCode);
                }

                if (dynamicQr.Length < Constants.DynamicQr.MaxLengthQr)
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, MessageResource.InvalidQrCode);
                }
                
                var validation = _userService.ValidationDynamicQr(dynamicQr.Substring(Constants.DynamicQr.NameProject.Length + 1));

                var validationQr = new ValidationQr
                {
                    Messages = validation
                };
                
                _eventLogService.CreateEventLogForDesktopApp(rid, user, validation, actionType);
                return Ok(validationQr);
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound); 
        }
        
    }
}