﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Device controller
    /// </summary>
    [Produces("application/json")]
    public class DeviceController : Controller
    {
        private readonly IDeviceService _deviceService;
        private readonly IUserService _userService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Device controller
        /// </summary>
        /// <param name="deviceService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="userService"></param>
        public DeviceController(IDeviceService deviceService,
            IHttpContextAccessor httpContextAccessor, IUserService userService)
        {
            _deviceService = deviceService;
            _userService = userService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// <summary>
        /// Return Json object for all device list
        /// </summary>
        /// <param name="search">What user is searching for</param>
        /// <param name="pageNumber">current page number</param>
        /// <param name="pageSize">Number of items to show on a page</param>
        /// <param name="sortColumn">the Item Based on Sorting</param>
        /// <param name="sortDirection">ascending or descending order</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevices)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(string search, int deviceType = 0, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc", short operationType = 0, List<int> companyId = null, List<int> connectionStatus = null, List<int> _deviceType = null)
        {
            var devices = _deviceService.GetPaginatedDevices(search, deviceType, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered, operationType,  companyId , connectionStatus , _deviceType ).AsEnumerable().Select(Mapper.Map<DeviceListModel>).ToList();

            var pagingData = new PagingData<DeviceListModel>
            {
                Data = devices,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Return Json object for all device list
        /// </summary>
        /// <param name="search">What user is searching for</param>
        /// <param name="pageNumber">current page number</param>
        /// <param name="pageSize">Number of items to show on a page</param>
        /// <param name="sortColumn">the Item Based on Sorting</param>
        /// <param name="sortDirection">ascending or descending order</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiValidDevices)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetValidDevice(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc", List<int> companyId = null, List<int> connectionStatus = null, List<int> deviceType = null)
        {
            var devices = _deviceService.GetPaginatedDeviceValid(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered, companyId, connectionStatus, deviceType).AsEnumerable().Select(Mapper.Map<DeviceListModel>).ToList();
            foreach (var item in devices)
            {
                var totalTime = Math.Abs(Math.Ceiling(DateTime.Now.Subtract(item.CreatedOn).TotalMinutes));
                item.TotalTime = Convert.ToInt32(totalTime);
                item.UpTime = Convert.ToInt32(Math.Ceiling((decimal)(item.UpTimeOnlineDevice * 100 / totalTime)));
            }
            var pagingData = new PagingData<DeviceListModel>
            {
                Data = devices,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDeviceGetFilterByCompanyAndIcu)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult FilterCompanyAndIcu()
        {
            var data = _deviceService.ListFilterDeviceMonitoring();

            return Ok(data);
        }

        /// <summary>
        /// Get device information by id
        /// </summary>
        /// <param name="id">device id to get</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesId)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new DeviceDataModel();
            var device = new IcuDevice();

            if (id != 0)
            {
                device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                if (device == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                model = Mapper.Map<DeviceDataModel>(device);
            }
            _deviceService.InitData(model, device.CompanyId ?? 0);
            return Ok(model);
        }

        /// <summary>
        /// Add device to system
        /// </summary>
        /// <param name="model">This model has information about the device to be added.</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiDevices)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]DeviceModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            int deviceId = _deviceService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, DeviceResource.lblDevice), deviceId.ToString());
        }

        /// <summary>
        /// Update device information
        /// </summary>
        /// <param name="id">device id to edit</param>
        /// <param name="model">This model has modified information about the device.</param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiDevicesId)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]DeviceModel model)
        {
            model.Id = id;
            ModelState.Clear();

            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());

            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _deviceService.Update(model);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, DeviceResource.lblDevice + " " + device.DeviceAddress, ""));
        }

        /// <summary>
        /// Delete device by id ( only 1 device )
        /// </summary>
        /// <param name="id">device id to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiDevicesId)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _deviceService.Delete(device);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, DeviceResource.lblDevice));
        }

        /// <summary>
        /// Delete devices by multi id ( 1~multi device(s) )
        /// </summary>
        /// <param name="ids">list of devices to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiDevices)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var devices = _deviceService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId())
                .ToList();
            if (!devices.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _deviceService.DeleteRange(devices);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, DeviceResource.lblDevice));
        }

        /// <summary>
        /// Change device status to Valid or Invalid
        /// </summary>
        /// <param name="id">Device ID to change status</param>
        /// <param name="model">This model has information about device status.</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiDeviceUpdateStatus)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ToggleStatus(int id, [FromBody]DeviceStatus model)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _deviceService.UpdateDeviceStatus(device, model.Status);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, DeviceResource.lblDevice, ""));
        }

        /// <summary>
        /// Generate test data
        /// </summary>
        /// <param name="numberOfDevice">number of device to test</param>
        /// <returns></returns>
        [HttpPost]
        [Route("/devices/create-test-data")]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CreateTestData(int numberOfDevice)
        {
            if (numberOfDevice == 0)
            {
                return Ok(new { message = "No device data is created!" });
            }

            var stopWatch = Stopwatch.StartNew();
            _deviceService.GenerateTestData(numberOfDevice);
            stopWatch.Stop();

            Trace.WriteLine($"Elapsed time {stopWatch.ElapsedMilliseconds} ms");
            return Ok(new
            {
                message =
                    $"{numberOfDevice} device(s) data were created sucessfully in {TimeSpan.FromMilliseconds(stopWatch.ElapsedMilliseconds).TotalSeconds} seconds!"
            });
        }

        /// <summary>
        /// get device infomation in response. ( ex. Send current time )
        /// </summary>
        /// <param name="id">device id to get information about</param>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.ApiDeviceInfo)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetDeviceInfoResponse(int id)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            _deviceService.SendDeviceInfo(device.DeviceAddress);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.PublishMessageToGetInfoDevice, DeviceResource.lblDevice));
        }


        /// <summary>
        /// send device instruction command.
        /// </summary>
        /// <param name="model">This model has information about instruction.</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiDeviceInstruction)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult SendDeviceInstruction([FromBody]DeviceInstruction model)
        {
            var devices = _deviceService.GetByIdsAndCompany(model.Ids, _httpContext.User.GetCompanyId());
            if (devices == null || !devices.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (string.IsNullOrEmpty(model.Command))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.CommandCanBeNotEmpty);
            }

            List<IcuDevice> onlineDevices = new List<IcuDevice>();
            var offlineDeviceAddr = "";

            onlineDevices = _deviceService.GetOnlineDevices(devices, ref offlineDeviceAddr);

            _deviceService.SendInstruction(onlineDevices, model);

            if (!onlineDevices.Any() || !string.IsNullOrEmpty(offlineDeviceAddr))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, string.Format(MessageResource.msgOfflineDevice, offlineDeviceAddr));
            }
            else
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.SendMessageDeviceInstruction, DeviceResource.lblDevice));
            }
        }

        /// <summary>
        /// Reinstall the devices
        /// </summary>
        /// <param name="deviceDetails">List of ReinstallDeviceDetail class.
        ///                             This class includes DeviceId and ProcessId.
        ///                             DeviceId is device id to reinstall.
        ///                             ProcessId is process id made by web app to show percentage through progress bar.</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiReinstallDevices)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ReinstallDevices([FromBody]List<ReinstallDeviceDetail> deviceDetails)
        {
            var ids = new List<int>();

            foreach (var deviceDetail in deviceDetails)
            {
                ids.Add(deviceDetail.DeviceId);
            }

            var devices = _deviceService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId()).ToList();
            if (!devices.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            List<IcuDevice> onlineDevices = new List<IcuDevice>();

            var offlineDeviceAddr = "";

            onlineDevices = _deviceService.GetOnlineDevices(devices, ref offlineDeviceAddr);

            _deviceService.Reinstall(onlineDevices, false, deviceDetails);

            if (!onlineDevices.Any() || !string.IsNullOrEmpty(offlineDeviceAddr))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, string.Format(MessageResource.msgOfflineDevice, offlineDeviceAddr));
            }
            else
            {
                return new ApiSuccessResult(StatusCodes.Status200OK, string.Format(MessageResource.IsReinstalling));
            }
        }

        /// <summary>
        /// Copy device
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiCopyDevices)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CopyDevices(int id, [FromBody]List<int> ids)
        {
            var deviceCopy = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            var devices = _deviceService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId())
                .ToList();

            if (deviceCopy == null || !devices.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            // Check device status ( device to be pasted )
            List<IcuDevice> onlineDevices = new List<IcuDevice>();
            var offlineDeviceAddr = "";

            onlineDevices = _deviceService.GetOnlineDevices(devices, ref offlineDeviceAddr);

            _deviceService.CopyDevices(deviceCopy, onlineDevices);

            if (!onlineDevices.Any() || !string.IsNullOrEmpty(offlineDeviceAddr))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, string.Format(MessageResource.msgOfflineDevice, offlineDeviceAddr));
            }
            else
            {
                return new ApiSuccessResult(StatusCodes.Status200OK, string.Format(MessageResource.CopySuccess, DeviceResource.lblDevice));
            }

        }

        /// <summary>
        /// Get device type list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDeviceTypes)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetListDeviceTypes(List<int> ids)
        {
            var devices = _deviceService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId());
            if (devices == null || !devices.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var model = _deviceService.GetListDeviceType(devices);
            return Ok(model);
        }

        /// <summary>
        /// file upload to update device
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiDeviceUploadFile)]
        [Consumes("multipart/form-data")]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult UploadFileProgress(IFormCollection models)
        {
            var files = models.Files;
            if (files == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            _deviceService.UploadFile(files);
            return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.UploadFile);
        }

        /// <summary>
        /// Copy device
        /// </summary>
        /// <param name="ids">device id list</param>
        /// <param name="processIds">process id list</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiStopProcess)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult StopProcess(List<int> ids, [FromBody]List<string> processIds)
        {
            if (processIds == null || !processIds.Any() || ids == null || !ids.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var devices = _deviceService.GetByIds(ids);

            var stopInstruction = new DeviceInstruction()
            {
                Ids = ids,
                Command = Constants.CommandType.StopUpdateFW,
                OpenPeriod = 0,
                OpenUntilTime = ""
            };

            _deviceService.StopProcess(devices, processIds);
            //_deviceService.SendInstruction(devices, stopInstruction);
            return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.msgStopSuccess);
        }

        /// <summary>
        /// Return Json object for accessible user list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesAccessibleUsers)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetAccessibleUser(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var users = _deviceService.GetPaginatedAccessibleUsers(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered).ToList();

            foreach (var user in users)
            {
                user.CardList = _deviceService.GetCardModelsByUserId(user.Id);

                if (user.CardList.Any())
                {
                    user.CardId = user.CardList.FirstOrDefault().CardId;
                    user.CardStatus = ((CardStatus)user.CardList.FirstOrDefault().CardStatus).GetDescription();
                }
            }

            var pagingData = new PagingData<AccessibleUserModel>
            {
                Data = users,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Return Json object for accessible user list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesAccessibleUsersExport)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ExportAccessibleUser(int id, string search, string type = "excel", int sortColumn = 0,
            string sortDirection = "desc")
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var fileData = _deviceService.ExportAccessibleUsers(device, type, search, out var totalRecords, out var recordsFiltered, sortColumn, sortDirection);
            var filename = string.Format(Constants.ExportFileFormat, DeviceResource.lblExportAccessibleUser + $"_{device.DeviceAddress}", DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";
            if (totalRecords == 0 || recordsFiltered == 0)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageExportDataIsZero, DeviceResource.lblAccessibleUsers));
            }

            return File(fileData, type.Equals("excel") ? "application/ms-excel" : "application/text", fullName);
        }


        /// <summary>
        /// Check Device information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msgId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCheckDeviceSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CheckDeviceInfo(int id, string msgId)
        {
            var icuDevice = _deviceService.GetById(id);
            if (icuDevice == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var model = _deviceService.LoadDeviceSetting(icuDevice, msgId);
            return Ok(model);
        }

        /// <summary>
        /// Check Timezone information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msgId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCheckTimezoneSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CheckTimezoneInfo(int id, string msgId)
        {
            var icuDevice = _deviceService.GetByIdAndCompanyIncludeTimezone(id);
            if (icuDevice == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var timezoneInfo = _deviceService.LoadTimezone(icuDevice, msgId);
            return Ok(timezoneInfo);
        }

        /// <summary>
        /// Check Holiday information
        /// </summary>
        /// <param name="id"></param>
        /// <param name="msgId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCheckHolidaySetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CheckHolidayInfo(int id, string msgId)
        {
            var icuDevice = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (icuDevice == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var holidayInfos = _deviceService.LoadHoliday(icuDevice, msgId);
            return Ok(holidayInfos);
        }

        /// <summary>
        /// Check User information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCheckUserSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CheckUserInfo(int id)
        {
            var icuDevice = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (icuDevice == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var messageIdModel = _deviceService.LoadUser(icuDevice);
            return Ok(messageIdModel);
        }

        /// <summary>
        /// get Transmit information
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiTransmitInfo)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetTransmitInfo()
        {
            var transmitData = _deviceService.GetTransmitAllData();
            return Ok(transmitData);
        }

        /// <summary>
        /// transmit data to device(s).
        /// </summary>
        ///
        /// <remarks>   Edward, 2020-02-03. </remarks>
        ///
        /// <param name="model">    This model has information about the device to receive data.  </param>
        ///
        /// <returns>   An IActionResult. </returns>
        [HttpPost]
        [Route(Constants.Route.ApiTransmitData)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult TransmitData([FromBody]TransmitDataModel model)
        {
            if (!model.TransmitIds.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var ids = new List<int>();

            foreach (var deviceDetail in model.Devices)
            {
                ids.Add(deviceDetail.DeviceId);
            }

            var devices = _deviceService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId()).ToList();
            if (!devices.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            List<IcuDevice> onlineDevices = new List<IcuDevice>();

            var offlineDeviceAddr = "";

            onlineDevices = _deviceService.GetOnlineDevices(devices, ref offlineDeviceAddr);

            _deviceService.TransmitData(model, onlineDevices);

            if (!onlineDevices.Any() || !string.IsNullOrEmpty(offlineDeviceAddr))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, string.Format(MessageResource.msgDeviceDisconnected, offlineDeviceAddr));
            }
            else
            {
                return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.msgTransmitSuccess);
            }
        }

        /// <summary>
        /// Export User data
        /// </summary>
        /// <param name="id"> device identifier </param>
        /// <returns></returns>
        [Produces("application/text", "application/ms-excel")]
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesUserInfoExport)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ExportUser(int id)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var fileData = _deviceService.ExportUserSetting(device);
            var filename = string.Format(Constants.ExportFileFormat, device.DeviceAddress, DateTime.Now);
            var fullName = $"{filename}.xlsx";
            return File(fileData, "application/ms-excel", fullName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesUsersInfo)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUserInfo(int id, string cardId)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var userInfo = _deviceService.GetUserInfo(device, cardId);
            return Ok(userInfo);
        }

        /// <summary>
        /// Get user infomation by user's card ID
        /// </summary>
        /// <param name="id">device ID</param>
        /// <param name="cardId">Card ID</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesUsersInfoByCardId)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUserInfoByCardId(int id, string cardId)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var userInfo = _deviceService.GetUserInfoByCardId(device, cardId);
            return Ok(userInfo);
        }

        /// <summary>
        /// Return Json object for all device list
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesMasterCard)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetMasterCard(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var devices = _deviceService.GetPaginatedDeviceMasterCard(search, pageNumber, pageSize, sortColumn,
                sortDirection, out var recordsTotal,
                out var recordsFiltered);

            var pagingData = new PagingData<MasterCardModel>
            {
                Data = devices,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Return Json object for all device list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDevicesUserMasterCard)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUserMasterCard(int id, int sortColumn = 0, string sortDirection = "desc")
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var userMasterCard = _deviceService.GetUserMasterCard(device, sortColumn, sortDirection);
            return Ok(userMasterCard);
        }

        /// <summary>
        /// Return Json object for all device list
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiExportUserMasterCard)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ExportUserMasterCard(int id)
        {
            var device = _deviceService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (device == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var fileData = _deviceService.ExportUserMasterCard(device);
            var filename = string.Format(Constants.ExportFileFormat, "MasterCard_" + device.DeviceAddress, DateTime.Now);
            var fullName = $"{filename}.xlsx";
            return File(fileData, "application/ms-excel", fullName);
        }

        /// <summary>
        /// Return Json object for all device list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDeviceInitialize)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetList()
        {
            var model = _deviceService.InitializeData();
            return Ok(model);
        }


        /// <summary>
        /// Backup Request
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiBackupRequest)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult BackupRequest()
        {
            //유효성 체크(검증)


            _deviceService.RequestBackup();
            return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.msgTransmitSuccess);
        }


        /// <summary>
        /// Set Max Icu Send User Count(default : 5) If input is 0, set default value.
        /// Set Delay between sending user protocol (unit : ms)
        /// </summary>
        /// <param name="count"></param>
        /// <param name="delay">ms</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiTestSetSendIcuUserCount)]
        //[Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult SetMaxIcuSendUserCount(int count = 5, int delay = 0)
        {
            if (count == 0)
                Constants.MaxSendIcuUserCount = 5;
            else
                Constants.MaxSendIcuUserCount = count;

            Constants.MaxSendUserDelay = delay;


            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, DeviceResource.lblDevice, ""));
        }

        /// <summary>
        ///             gets device history.
        /// </summary>
        ///
        /// <remarks>   Edward, 2020-03-06. </remarks>
        ///
        /// <param name="id">           device id to get. </param>
        /// <param name="pageNumber">   (Optional) current page number. </param>
        /// <param name="pageSize">     (Optional) Number of items to show on a page. </param>
        ///
        /// <returns>   The device history. </returns>

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDeviceHistory)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetDeviceHistory(int id, int pageNumber = 1, int pageSize = 10)
        {
            var device = _deviceService.GetById(id);

            if (device != null)
            {
                var deviceHistory = _deviceService.GetHistory(id, pageNumber, pageSize, out var totalRecords).ToList();

                var pagingData = new PagingData<DeviceHistoryModel>
                {
                    Data = deviceHistory,
                    Meta = { RecordsTotal = totalRecords }
                };

                return Ok(pagingData);
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }


        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiReUpdateUpTimeDevice)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ReUpdateUpTimOnlineDevice()
        {
            try
            {
                int count = _deviceService.ReUpdateUpTimOnlineDevice();
                var reponse = new ReponseStatus();
                reponse.message = "Success";
                reponse.statusCode = true;
                reponse.data = count;
                return Ok(reponse);
            }
            catch (Exception ex)
            {
                ex.ToString();
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

        }

        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiReUpdateUpTimeDeviceById)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ReUpdateUpTimOnlineDeviceById(int id)
        {
            try
            {
                _deviceService.ReUpdateUpTimOnlineDeviceById(id);
                return Ok();
            }
            catch (Exception ex)
            {
                ex.ToString();
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

        }


    }
}