﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealType;
using DeMasterProCloud.DataModel.MealEventLog;
using Microsoft.Extensions.Configuration;
using ClosedXML.Excel;
using System.IO;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// mealType controller
    /// </summary>
    [Produces("application/json")]
    [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
    public class MealEventLogController : Controller
    {
        private readonly IMealEventLogService _mealEventLogService;
        private readonly IConfiguration _config;
        public MealEventLogController(IMealEventLogService mealEventLogService, IConfiguration config)
        {
            _mealEventLogService = mealEventLogService;
            _config = config;
        }



        /// <summary>
        /// get (a) meal eventLog(s) by search filter
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiMealEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetListMealSetting(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        string sortDirection = "desc", string firstAccessTime = null, string lastAccessTime = null, string cardId = null, int departmentId = 0, List<int> mealCode = null, int userId = 0, List<int> icuId = null, List<int> buildingId = null, List<int> cornerId = null, int isManual = -1)
        {
            int rangeMonth = _config.GetValue<int>("CanteenManagement:rangeMonth");

            if (firstAccessTime == null || lastAccessTime == null)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgInputAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(firstAccessTime) > DateTimeHelper.ConvertIsoToDateTime(lastAccessTime))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgCompareAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(lastAccessTime).Subtract(DateTimeHelper.ConvertIsoToDateTime(firstAccessTime)).Days / (365.25 / 12) > rangeMonth)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgSearchLimitedAccessTime, ""));
            }
            var listMealEventLog = _mealEventLogService.GetListMealEventLogManual(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered, firstAccessTime, lastAccessTime, cardId, departmentId, mealCode, userId, icuId, buildingId, cornerId, isManual);

            var pagingData = new PagingData<MealEventLogModel>
            {
                Data = listMealEventLog,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// get a mealType by Id
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateMealEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Get(int id)
        {
            var mealEventLog = _mealEventLogService.GetMealEventLogManual(id);
            if (mealEventLog == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return Ok(mealEventLog);
        }


        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiReportMealEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Report(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        string sortDirection = "desc", string firstAccessTime = null, string lastAccessTime = null, string cardId = null, int departmentId = 0, List<int> mealCode = null, int userId = 0, string doorName = null, List<int> buildingId = null, int isReport = 0)
        {
            int rangeMonth = _config.GetValue<int>("CanteenManagement:rangeMonth");
            if (firstAccessTime == null || lastAccessTime == null)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgInputAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(firstAccessTime) > DateTimeHelper.ConvertIsoToDateTime(lastAccessTime))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgCompareAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(lastAccessTime).Subtract(DateTimeHelper.ConvertIsoToDateTime(firstAccessTime)).Days / (365.25 / 12) > rangeMonth)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgSearchLimitedAccessTime, ""));
            }
            var mealEventLog = _mealEventLogService.Report(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered, firstAccessTime, lastAccessTime, cardId, departmentId, mealCode, userId, doorName, buildingId, isReport);
            if (mealEventLog == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            

            var pagingData = new PagingData<MealEventLogReportModel>
            {
                Data = mealEventLog,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiReportMealEventLogByBuilding)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult ReportByBuilding(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        string sortDirection = "desc", string firstAccessTime = null, string lastAccessTime = null, List<int> buildingId = null)
        {
            int rangeMonth = _config.GetValue<int>("CanteenManagement:rangeMonth");
            if (firstAccessTime == null || lastAccessTime == null)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgInputAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(firstAccessTime) > DateTimeHelper.ConvertIsoToDateTime(lastAccessTime))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgCompareAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(lastAccessTime).Subtract(DateTimeHelper.ConvertIsoToDateTime(firstAccessTime)).Days / (365.25 / 12) > rangeMonth)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgSearchLimitedAccessTime, ""));
            }
            var mealEventLog = _mealEventLogService.ListReportByBuildingModel(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered, firstAccessTime, lastAccessTime, buildingId);
            if (mealEventLog == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var pagingData = new PagingData<ReportCanteenManagementByBuildingModel>
            {
                Data = mealEventLog,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiReportMealEventLogByCorner)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult ReportByCorner(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        string sortDirection = "desc", string firstAccessTime = null, string lastAccessTime = null, List<int> cornerId = null)
        {
            int rangeMonth = _config.GetValue<int>("CanteenManagement:rangeMonth");
            if (firstAccessTime == null || lastAccessTime == null)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgInputAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(firstAccessTime) > DateTimeHelper.ConvertIsoToDateTime(lastAccessTime))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgCompareAccessTime, ""));
            }
            else if (DateTimeHelper.ConvertIsoToDateTime(lastAccessTime).Subtract(DateTimeHelper.ConvertIsoToDateTime(firstAccessTime)).Days / (365.25 / 12) > rangeMonth)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.msgSearchLimitedAccessTime, ""));
            }
            var mealEventLog = _mealEventLogService.ListReportByCorner(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered, firstAccessTime, lastAccessTime, cornerId);
            if (mealEventLog == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var pagingData = new PagingData<ReportCanteenManagementByCornerModel>
            {
                Data = mealEventLog,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(pagingData);
        }


        /// <summary>
        /// add a mealType
        /// </summary>
        /// <param name="model"> MealType Model </param>
        [HttpPost]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiMealEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Add([FromBody]MealEventLogAddModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            var res = _mealEventLogService.AddMealEventLogManual(model);
            if (res.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, CanteenResource.lblMealEventLog), res.data.ToString());
        }

        [HttpPost]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiMealEventLogExportToExcel)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Excel(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        string sortDirection = "desc", string firstAccessTime = null, string lastAccessTime = null, string cardId = null, int departmentId = -1, List<int> mealCode = null, int userId = -1, string doorName = null, List<int> buildingId = null, int isReport = 1)
        {
            var mealEventLog = _mealEventLogService.Report(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
               out var recordsFiltered, firstAccessTime, lastAccessTime, cardId, departmentId, mealCode, userId, doorName, buildingId, isReport);
            var data = mealEventLog.GroupBy(x => new { x.userName, x.totalPrice }).Select(x1 => new MealEventLogModel
            {
                userName = x1.First().userName,
                Price = x1.Sum(x => x.totalPrice)
            }).FirstOrDefault();

            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Users");
                var currentRow = 1;
                worksheet.Cell(currentRow, 1).Value = "Id";
                worksheet.Cell(currentRow, 2).Value = "User Name";
                worksheet.Cell(currentRow, 3).Value = "Birthday";
                worksheet.Cell(currentRow, 4).Value = "Employee number";
                worksheet.Cell(currentRow, 5).Value = "Department";
                worksheet.Cell(currentRow, 6).Value = "User Code";
                worksheet.Cell(currentRow, 7).Value = "Card ID";
                worksheet.Cell(currentRow, 8).Value = "Total Count";
                worksheet.Cell(currentRow, 9).Value = "Total price";
                for (int i = 0; i < mealEventLog.Count; i++)
                {
                    var item = mealEventLog[i];
                    currentRow++;
                    worksheet.Cell(currentRow, 1).Value = i + 1;
                    worksheet.Cell(currentRow, 2).Value = item.userName;
                    worksheet.Cell(currentRow, 3).Value = item.birthDay.ToString();
                    worksheet.Cell(currentRow, 4).Value = item.employeeNumber;
                    worksheet.Cell(currentRow, 5).Value = item.departmentName;
                    worksheet.Cell(currentRow, 6).Value = item.userCode;
                    worksheet.Cell(currentRow, 7).Value = item.cardId;
                    worksheet.Cell(currentRow, 8).Value = i + 1;
                    worksheet.Cell(currentRow, 9).Value = item.totalPrice;
                }

                worksheet.Cell(currentRow, 1).Value = "";
                worksheet.Cell(currentRow, 2).Value = "Total price";
                worksheet.Cell(currentRow, 3).Value = "";
                worksheet.Cell(currentRow, 4).Value = "";
                worksheet.Cell(currentRow, 5).Value = "";
                worksheet.Cell(currentRow, 6).Value = "";
                worksheet.Cell(currentRow, 7).Value = "";
                worksheet.Cell(currentRow, 8).Value = "";
                worksheet.Cell(currentRow, 9).Value = data.Price;

                using (var stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();

                    return File(
                        content,
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "ReportSettlemenCanteenManagement.xlsx");
                }
            }
        }


        /// <summary>
        /// edit a mealType
        /// </summary>
        /// <param name="id"> identifier of mealType </param>
        /// <param name="model"> MealTypeModel </param>
        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateMealEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Edit(int id, [FromBody]MealEventLogUpdateModel model)
        {

            model.mealEventLogId = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }


            var res = _mealEventLogService.UpdateMealEventLogManual(model);

            if (res.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, CanteenResource.lblMealEventLog, ""));
        }


        [HttpDelete]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateMealEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Delete(int id)
        {
            var res = _mealEventLogService.DeleteMeaEventLog(id);

            if (res.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (res.statusCode == true && res.message == "")
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(CanteenResource.DeleteMealEventLog, ""));
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, CanteenResource.lblMealEventLog));
        }
    }
}