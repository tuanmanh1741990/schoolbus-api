﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Timezone;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using Microsoft.Extensions.Configuration;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Timezone Controller
    /// </summary>
    [Produces("application/json")]
    public class TimezoneController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ITimezoneService _timezoneService;
        private readonly IDeviceService _deviceService;
        private readonly HttpContext _httpContext;


        /// <summary>
        /// Timezone controller
        /// </summary>
        /// <param name="timezoneService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="deviceService"></param>
        /// <param name="configuration"></param>
        public TimezoneController(IConfiguration configuration, ITimezoneService timezoneService, IHttpContextAccessor httpContextAccessor, IDeviceService deviceService)
        {
            _configuration = configuration;
            _timezoneService = timezoneService;
            _httpContext = httpContextAccessor.HttpContext;
            _deviceService = deviceService;
        }

        /// <summary>
        /// Return Json object for all timezone list
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiTimezones)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var timezones = _timezoneService.GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered).AsEnumerable().ToList();           

            var pagingData = new PagingData<TimezoneListModel>
            {
                Data = timezones,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get timezone
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiTimezonesId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new TimezoneModel();
            if (id != 0)
            {
                var timezone = _timezoneService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
                if (timezone == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound, MessageResource.RecordNotFound);
                }
                model = _timezoneService.InitData(timezone);
            }
            return Ok(model);
        }

        /// <summary>
        /// Add timezone
        /// </summary>
        /// <param name="model">TimezoneModel</param>
        [HttpPost]
        [Route(Constants.Route.ApiTimezones)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]TimezoneModel model)
        {
            var maxTimezone = Constants.Settings.DefaultMaxTimezone;
            if (!string.IsNullOrEmpty(_configuration[Constants.Settings.MaxTimezone]))
            {
                maxTimezone = Convert.ToInt32(_configuration[Constants.Settings.MaxTimezone]);
            }
            var timezoneCount = _timezoneService.GetTimezoneCount(_httpContext.User.GetCompanyId());
            if (timezoneCount >= maxTimezone)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(TimezoneResource.msgLimitTimezone, maxTimezone));
            }

            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            _timezoneService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, TimezoneResource.lblTimezone));
        }

        /// <summary>
        /// Edit timezone
        /// </summary>
        /// <param name="id">timezone id to edit</param>
        /// <param name="model">TimezoneModel</param>
        [HttpPut]
        [Route(Constants.Route.ApiTimezonesId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]TimezoneModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            var timezone = _timezoneService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (timezone == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            //Check whether it's default timezone
            if (timezone.Position == Constants.Settings.DefaultPositionPassageTimezone ||
                timezone.Position == Constants.Settings.DefaultPositionActiveTimezone)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, TimezoneResource.msgCannotUpdateDefaultTz);
            }

            _timezoneService.Update(model, timezone);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, TimezoneResource.lblTimezone, ""));
        }

        /// <summary>
        /// Delete timezone
        /// </summary>
        /// <param name="id">timezone id to delete</param>
        [HttpDelete]
        [Route(Constants.Route.ApiTimezonesId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var timezone = _timezoneService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (timezone == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (_deviceService.HasTimezone(timezone.Id, _httpContext.User.GetCompanyId()))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, TimezoneResource.msgCannotDeleteTimezone);
            }

            //Check whether it's default timezone
            if (timezone.Position == Constants.Settings.DefaultPositionPassageTimezone ||
                timezone.Position == Constants.Settings.DefaultPositionActiveTimezone)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, TimezoneResource.msgCannotDeleteDefaultTz);
            }

            _timezoneService.Delete(timezone);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, TimezoneResource.lblTimezone));
        }

        /// <summary>
        /// Timezone multiple delete
        /// </summary>
        /// <param name="ids">timezone id list to delete</param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiTimezones)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var timezones = _timezoneService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId());
            if (!timezones.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var companyId = _httpContext.User.GetCompanyId();
            foreach (var timezone in timezones)
            {
                //Check whether it's default timezone
                if (timezone.Position == Constants.Settings.DefaultPositionPassageTimezone ||
                    timezone.Position == Constants.Settings.DefaultPositionActiveTimezone)
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, TimezoneResource.msgCannotDeleteDefaultTz);
                }

                if (_deviceService.HasTimezone(timezone.Id, companyId))
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, TimezoneResource.msgCannotDeleteTimezone);
                }
            }

            _timezoneService.DeleteRange(timezones);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, TimezoneResource.lblTimezone));
        }
    }
}
