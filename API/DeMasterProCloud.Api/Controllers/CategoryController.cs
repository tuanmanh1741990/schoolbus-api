﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using DeMasterProCloud.DataModel.Category;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Category controller
    /// </summary>
    [Produces("application/json")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Controller of category
        /// </summary>
        /// <param name="categoryService"></param>
        /// <param name="httpContextAccessor"></param>
        public CategoryController(ICategoryService categoryService, IHttpContextAccessor httpContextAccessor)
        {
            _categoryService = categoryService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// <summary>
        /// get (a) category(s) by search filter
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCategories)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Gets(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var categories = _categoryService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            var categoryOptions = _categoryService
                .GetOptionsPaginated(search, pageNumber, pageSize, sortColumn, sortDirection);

            var categoryPagingData = new CategoryPagingData<CategoryListModel, CategoryOptionListModel>
            {
                Categories = categories,
                Options = categoryOptions,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(categoryPagingData);
        }

        /// <summary>
        /// get a category by Id
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCategoriesId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var category = _categoryService.GetCategoryByIdAndCompanyId(id, _httpContext.User.GetCompanyId());
            if (category == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var categoryModel = Mapper.Map<CategoryModel>(category);

            var options = _categoryService.GetCategoryOptionsByCategoryId(category.Id).AsQueryable()
                        .Select(Mapper.Map<CategoryOptionModel>).ToList();

            categoryModel.CategoryOptions = options;

            return Ok(categoryModel);
        }

        /// <summary>
        /// Get hierarchy of category
        /// </summary>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCaregoriesHierarchy)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetCategoryList()
        {
            var categories = _categoryService.GetCategoryHierarchy();

            return Json(categories);
        }

        /// <summary>
        /// Get hierarchy of category option
        /// </summary>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCaregoryOptionsHierarchy)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetOptionList()
        {
            var options = _categoryService.GetOptionHierarchy();

            return Json(options);
        }

        /// <summary>
        /// add categories
        /// </summary>
        /// <param name="model"> list of CategoryModel </param>
        [HttpPost]
        [Route(Constants.Route.ApiCategories)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]List<CategoryModel> model)
        {
            List<string> categoryNames = new List<string>();

            foreach (var categoryModel in model)
            {
                if (categoryNames.Contains(categoryModel.Name))
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, String.Format(MessageResource.msgDuplicatedData, CategoryResource.lblCategoryName));
                }

                ModelState.Clear();
                if (!TryValidateModel(categoryModel))
                {
                    return new ValidationFailedResult(ModelState);
                }

                if (categoryModel.ParentCategoryId != null)
                {
                    var parentCategory = _categoryService
                        .GetCategoryByIdAndCompanyId(categoryModel.ParentCategoryId.Value, _httpContext.User.GetCompanyId());
                    // Check if parentCategory value in model is wrong. 
                    if (parentCategory == null)
                    {
                        return new ApiErrorResult(StatusCodes.Status404NotFound);
                    }
                }

                categoryNames.Add(categoryModel.Name);
            }

            _categoryService.Add(model);

            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, CategoryResource.Category));
        }

        /// <summary>
        /// edit Categories
        /// </summary>
        /// <param name="model"> list of CategoryModel </param>
        [HttpPut]
        [Route(Constants.Route.ApiCategories)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit([FromBody]List<CategoryModel> model)
        {
            List<string> categoryNames = new List<string>();

            var companyId = _httpContext.User.GetCompanyId();

            foreach(var categoryModel in model)
            {
                if (categoryNames.Contains(categoryModel.Name))
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, String.Format(MessageResource.msgDuplicatedData, CategoryResource.lblCategoryName));
                }

                ModelState.Clear();
                if (!TryValidateModel(model))
                {
                    return new ValidationFailedResult(ModelState);
                }

                var category = _categoryService.GetCategoryByIdAndCompanyId(categoryModel.Id, companyId);
                if (category == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                else
                {
                    var options = categoryModel.CategoryOptions;

                    foreach (var option in options)
                    {
                        if (option.CategoryId != categoryModel.Id)
                        {
                            return new ApiErrorResult(StatusCodes.Status404NotFound);
                        }
                    }
                }

                if (categoryModel.ParentCategoryId != null)
                {
                    var parentCategory = _categoryService.GetCategoryByIdAndCompanyId(categoryModel.ParentCategoryId.Value, companyId);
                    // Check if parentCategory value in model is wrong. 
                    if (parentCategory == null)
                    {
                        return new ApiErrorResult(StatusCodes.Status404NotFound);
                    }
                }

                categoryNames.Add(categoryModel.Name);
            }

            _categoryService.Update(model);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, CategoryResource.Category, ""));
        }

        /// <summary>
        /// delete a category by id
        /// </summary>
        /// <param name="id"> identifier of category </param>
        [HttpDelete]
        [Route(Constants.Route.ApiCategoriesId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var category = _categoryService.GetCategoryByIdAndCompanyId(id, companyId);
            if (category == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            // Check whether there are any child category.
            var childCategories = _categoryService.GetCategoriesByParentIdAndCompanyId(id, companyId);
            if(childCategories != null && childCategories.Any())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, string.Format(CategoryResource.msgDeleteFailChild));
            }

            _categoryService.Delete(category);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, CategoryResource.Category));
        }
    }
}