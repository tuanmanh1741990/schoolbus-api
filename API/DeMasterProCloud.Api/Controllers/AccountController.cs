﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Account;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Login;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ForgotPasswordModel = DeMasterProCloud.DataModel.Login.ForgotPasswordModel;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using Org.BouncyCastle.Asn1.Ocsp;
using Microsoft.Extensions.Options;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Define a login route for api app
    /// </summary>
    [Produces("application/json")]
    public class AccountController : ControllerBase
    {
        private readonly ICompanyService _companyService;
        private readonly IAccountService _accountService;
        private readonly IJwtHandler _jwtHandler;
        private readonly HttpContext _httpContext;
        private readonly JwtOptionsModel _options;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyService"></param>
        /// <param name="accountService"></param>
        /// <param name="jwtHandler"></param>
        /// <param name="httpContextAccessor"></param>
        public AccountController(ICompanyService companyService, IAccountService accountService,
            IJwtHandler jwtHandler, IHttpContextAccessor httpContextAccessor, IOptions<JwtOptionsModel> options)
        {
            _companyService = companyService;
            _accountService = accountService;
            _jwtHandler = jwtHandler;
            _httpContext = httpContextAccessor.HttpContext;
            _options = options.Value;
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route(Constants.Route.ApiLogin)]
        //[Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Login([FromBody]LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            //Check user is no one, then make default account
            if (!_accountService.HasDefaultAccount())
            {
                return new ApiUnauthorizedResult((int)LoginUnauthorized.AccountNonExist, AccountResource.msgNonAccount);
            }

            //Get the account in the system
            var account = _accountService.GetAuthenticatedAccount(model);
            if (account == null)
            {
                return new ApiUnauthorizedResult((int)LoginUnauthorized.InvalidCredentials, MessageResource.InvalidCredentials);
            }

            if (account.Type != 0)
            {
                //Check where if the account is in valid company
                if (!_companyService.IsValidCompany(account.Company))
                {
                    return new ApiUnauthorizedResult((int)LoginUnauthorized.CompanyExpired, AccountResource.msgCompanyExpired);
                }
            }

            //Check where if the account is valid status
            if (account.IsDeleted)
            {
                return new ApiUnauthorizedResult((int)LoginUnauthorized.InvalidCredentials, MessageResource.InvalidCredentials);
            }

            var claims = new[]
            {
                //new Claim(ClaimTypes.Name, account.Username),
                new Claim(Constants.ClaimName.Username, account.Username),
                new Claim(Constants.ClaimName.AccountId, account.Id.ToString()),
                new Claim(Constants.ClaimName.CompanyId, account.Type == 0 ? "0" :account.CompanyId.ToString()),
                new Claim(Constants.ClaimName.CompanyCode, account.Type == 0 ? "s000001" : account.Company.Code),
                new Claim(Constants.ClaimName.CompanyName, account.Type == 0 ? "SystemAdmin" : account.Company.Name),
                new Claim(Constants.ClaimName.AccountType, account.Type.ToString())
            };
            var token = _jwtHandler.BuilToken(claims);
            var refreshToken = "";

            _accountService.SaveSystemLogLogIn(account);


            refreshToken = _accountService.GetRefreshTokenByUserName(account.Username, Convert.ToInt32(account.CompanyId));
            var accByRefreshToken = _accountService.GetAccountByRefreshToken(refreshToken);

            if (refreshToken == null || refreshToken == "" || accByRefreshToken.CreateDateRefreshToken.AddMonths(_options.expiryRefreshToken) < DateTime.Now)
            {
                refreshToken = _jwtHandler.GenerateRefreshToken();
            }
            _accountService.AddTokenAndRefreshTokenToDB(refreshToken, account, _options.expiryRefreshToken);
            var companyCode = "";

            if (account.Type == 0)
            {
                companyCode = "s000001";
            }
            else
            {
                companyCode = _companyService.GetCodeById(account.CompanyId ?? 0);
            }

            if (account.Type != 0)
            {
                var plugIn = _companyService.GetPluginByCompanyAllowShowing(account.CompanyId ?? 0);
                var json = JsonConvert.DeserializeObject<Dictionary<string, bool>>(plugIn.PlugIns);
                return Ok(new TokenModel
                {
                    AuthToken = token,
                    RefreshToken = refreshToken,
                    AccountType = account.Type,
                    CompanyCode = companyCode,
                    QueueService = _accountService.GetUserRabbitCredential(account),
                    PlugIn = json,
                    AccountId = account.Id,
                    ExpireAccessToken = _options.ExpiryMinutes,
                    UserTimeZone = _accountService.GetById(account.Id).TimeZone
                });
            }
            return Ok(new TokenModel
            {
                AuthToken = token,
                RefreshToken = refreshToken,
                AccountType = account.Type,
                CompanyCode = companyCode,
                QueueService = _accountService.GetUserRabbitCredential(account),
                ExpireAccessToken = _options.ExpiryMinutes,
                UserTimeZone = _accountService.GetById(account.Id).TimeZone
            });
        }

        /// <summary>
        /// Refresh token
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.APiRefreshToken)]
        public IActionResult RefreshToken(string refreshToken)
        {
            var responseStatus = new ReponseStatus();
            var account = _accountService.GetAccountByRefreshToken(refreshToken);
            var companyCode = "";
            if(account == null)
            {
                BadRequest();
            }


            if (account.Type == 0)
            {
                companyCode = "s000001";
            }
            else
            {
                companyCode = _companyService.GetCodeById(account.CompanyId ?? 0);
            }
            var claims = new[]
            {
                new Claim(Constants.ClaimName.Username, account.Username),
                new Claim(Constants.ClaimName.AccountId, account.Id.ToString()),
                new Claim(Constants.ClaimName.CompanyId, account.Type == 0 ? "0" :account.CompanyId.ToString()),
                new Claim(Constants.ClaimName.CompanyCode, account.Type == 0 ? "s000001" : (account.Company == null ? "" : account.Company.Code)),
                new Claim(Constants.ClaimName.CompanyName, account.Type == 0 ? "SystemAdmin" : (account.Company == null ? "" : account.Company.Name)),
                new Claim(Constants.ClaimName.AccountType, account.Type.ToString())
            };
            var token = _jwtHandler.BuilToken(claims);
            responseStatus.message = "Refresh Token Success";
            responseStatus.statusCode = true;
            responseStatus.data = new TokenModel
            {
                AuthToken = token,
                RefreshToken = refreshToken,
                AccountType = account.Type,
                CompanyCode = companyCode,
                QueueService = _accountService.GetUserRabbitCredential(account),
                ExpireAccessToken = _options.ExpiryMinutes,
                UserTimeZone = account.TimeZone
            };
            return Ok(responseStatus);
        }


        /// <summary>
        /// Get Account(s)
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccounts)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accounts = _accountService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<AccountListModel>).ToList();
            foreach (var account in accounts)
            {
                account.Role = HttpUtility.HtmlDecode(account.Role);
            }
            var pagingData = new PagingData<AccountListModel>
            {
                Data = accounts,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        /// <summary>
        /// Get Account information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccountsId)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new AccountDataModel();
            if (id != 0)
            {
                if (_httpContext.User.GetAccountType() == (short)AccountType.SystemAdmin)
                {
                    var account = _accountService.GetById(id);
                    if (account == null)
                    {
                        return new ApiErrorResult(StatusCodes.Status404NotFound);
                    }
                    model = Mapper.Map<AccountDataModel>(account);
                    if (account.Id == _httpContext.User.GetAccountId())
                    {
                        model.IsCurrentAccount = true;
                    }

                    //return Ok(model);
                    _accountService.InitData(model);
                }
                else
                {
                    var account = _accountService.GetById(id);

                    if (account == null || account.CompanyId != _httpContext.User.GetCompanyId())
                    {
                        return new ApiErrorResult(StatusCodes.Status404NotFound);
                    }
                    model = Mapper.Map<AccountDataModel>(account);
                    if (account.Id == _httpContext.User.GetAccountId())
                    {
                        model.IsCurrentAccount = true;
                    }

                    //return Ok(model);
                    _accountService.InitData(model);
                }

            }
            else
            {
                _accountService.InitData(model);
            }
            return Ok(model);
        }

        /// <summary>
        /// Add a account.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiAccounts)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]AccountModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            //if (_accountService.IsExist(model.Username))
            //{
            //    return new ApiErrorResult(StatusCodes.Status400BadRequest, string.Format(MessageResource.Exist, AccountResource.lblEmail));
            //}

            var accountId = _accountService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, AccountResource.lblAccount), accountId.ToString());
        }


        /// <summary>
        /// Edit the account information.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiAccountsId)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]AccountModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            var account = _accountService.GetById(id);

            if (_httpContext.User.GetAccountType() == (short)AccountType.SystemAdmin)
            {
                if (account == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
            }
            else
            {
                if (account == null || account.CompanyId != _httpContext.User.GetCompanyId())
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
            }

            if (model.Username != account.Username)
            {
                var valid = _accountService.GetAccountByEmail(model.Username);
                if (valid != null)
                {
                    if (valid.Id != account.Id)
                    {
                        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, AccountResource.lblEmail));
                    }
                }
            }
            _accountService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, AccountResource.lblAccount, ""));
        }


        /// <summary>
        /// Delete a account.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiAccountsId)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var account = _accountService.GetAccountByCurrentCompany(id);
            if (account == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var accountLogin = _accountService.GetAccountLogin(User);
            if (!_accountService.IsAllowDelete(account, accountLogin))
            {
                return new ApiErrorResult(StatusCodes.Status403Forbidden);
            }

            _accountService.Delete(account);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, AccountResource.lblAccount));
        }


        /// <summary>
        /// Delete a number of accounts.
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiAccounts)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var accounts = _accountService.GetAccountsByIds(ids, _httpContext.User.GetCompanyId());
            if (!accounts.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var accountLogin = _accountService.GetAccountLogin(User);
            if (accounts.Any(m => !_accountService.IsAllowDelete(m, accountLogin)))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, AccountResource.msgNotDelete);
            }

            _accountService.DeleteRange(accounts);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, AccountResource.lblAccount));
        }

        /// <summary>
        /// When forgot password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route(Constants.Route.ApiForgotPassword)]
        public IActionResult ForgotPassword([FromBody] ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            var account = _accountService.GetAccountByEmail(model.Email);
            if (account == null)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.MessageEmailNotRegister);
            }

            _accountService.SendResetAccountMail(account, account.CompanyId ?? 0);

            return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.MessageSendEmailSuccess);
        }

        /// <summary>
        /// Reset password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route(Constants.Route.ApiResetPassword)]
        public IActionResult ResetPassword([FromBody] ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            var clAcc = _jwtHandler.GetPrincipalFromExpiredToken(model.Token);
            if (clAcc == null)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.InvalidInformation);
            }
            var username = clAcc.GetUsername();
            var account = _accountService.GetAccountByEmail(username);
            if (account == null)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.EmailDoestNotExist);
            }

            account.Password = SecurePasswordHasher.Hash(model.NewPassword);
            _accountService.ChangePassword(account);

            return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.MessageResetPassSuccess);
        }

        /// <summary>
        /// Get time zone
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route(Constants.Route.ApiAccountsListTimeZone)]
        public IActionResult GetListTimeZone()
        {
            var dictionary = _accountService.GetTimeZone();
            return Ok(dictionary);
        }

        /// <summary>
        /// Updated Time Zone for User preferences page
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.ApiUpdateTimeZoneByAccounts)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdminAndEmployee, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult UpdateTimeZone([FromBody]AccountTimeZoneModel model)
        {
            model.Id = _httpContext.User.GetAccountId();
            var account = _accountService.GetById(_httpContext.User.GetAccountId());
            if (account == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (String.IsNullOrEmpty(model.TimeZone))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }
            else
            {
                var timezone = Helpers.IsValidTimeZone(model.TimeZone);
                if (!timezone)
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest);
                }
            }
            _accountService.UpdateTimeZone(model, model.TimeZone);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, AccountResource.lblAccount, ""));
        }

        /// <summary>
        /// Get version of system
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route(Constants.Route.ApiInformationVersionSystem)]
        public IActionResult GetVersionSystem()
        {
            var dictionary = _accountService.GetInfomationSystem();
            return Ok(dictionary);
        }


        /// <summary>
        ///      gets primary accounts.
        /// </summary>
        /// <remarks>   Edward, 2020-03-25. </remarks>
        /// <param name="search">           Words to be searched. </param>
        /// <param name="pageNumber">       (Optional) The Page number. </param>
        /// <param name="pageSize">         (Optional) The contents count on current page. </param>
        /// <param name="sortColumn">       (Optional) criteria number to sort. </param>
        /// <param name="sortDirection">    (Optional) asc or desc. </param>
        /// <returns>   The primary accounts. </returns>
        [HttpGet]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route(Constants.Route.ApiGetPrimaryAccounts)]
        public IActionResult GetPrimaryAccounts(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accounts = _accountService
                .GetPaginatedPrimaryAccount(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<AccountListModel>).ToList();
            foreach (var account in accounts)
            {
                account.Role = HttpUtility.HtmlDecode(account.Role);
            }
            var pagingData = new PagingData<AccountListModel>
            {
                Data = accounts,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        /// <summary>
        /// Get account type
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiGetAccountsType)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetAccountType()
        {
            var accountType = _accountService.GetAccountType();
            return Ok(accountType);
        }
    }
}