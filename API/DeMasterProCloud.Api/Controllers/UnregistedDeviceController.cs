﻿using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.UnregistedDevice;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// UnregistedDevice controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UnregistedDeviceController : Controller
    {
        private readonly IUnregistedDeviceService _unregistedDeviceService;

        /// <summary>
        /// Unregisted controller
        /// </summary>
        /// <param name="unregistedDeviceService"></param>
        public UnregistedDeviceController(IUnregistedDeviceService unregistedDeviceService)
        {
            _unregistedDeviceService = unregistedDeviceService;
        }

        /// <summary>
        /// Get the missingDevice(s)
        /// </summary>
        /// /// <param name="search"></param>
        /// /// <param name="pageNumber"></param>
        /// /// <param name="pageSize"></param>
        /// /// <param name="sortColumn"></param>
        /// /// <param name="sortDirection"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUnregitedDevices)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUnregisterDevices(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var devices = _unregistedDeviceService.GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered).AsEnumerable().ToList();

            var pagingData = new PagingData<UnregistedDeviceModel>
            {
                Data = devices,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Add a missingDevice to icuDevice
        /// </summary>
        [HttpPost]
        [Route(Constants.Route.ApiAddMissingDevices)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AddMissingDevice()
        {
            //var missingDevices = _unregistedDeviceService.GetByCompanyId();
            var missingDevices = _unregistedDeviceService.GetAll();
            if (missingDevices == null)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }

            _unregistedDeviceService.AddMissingDevice(missingDevices);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateMissingDeviceSuccess));
        }
    }
}