﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataModel.Visit;
using Newtonsoft.Json;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using DeMasterProCloud.DataModel.VisitArmy;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// VisitArmy controller
    /// </summary>
    [Produces("application/json")]
    [CheckAddOnAttribute(Constants.PlugIn.ArmyManagement)]
    public class VisitArmyController : Controller
    {
        private readonly IVisitService _visitService;
        private readonly IVisitArmyService _visitArmyService;
        private readonly IAccountService _accountService;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly IAccessGroupService _accessGroupService;


        /// <summary>
        /// Visitor for army Controller constructor
        /// </summary>
        /// <param name="visitService"></param>
        /// <param name="visitArmyService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="configuration"></param>
        /// <param name="accountService"></param>
        /// <param name="accessGroupService"></param>
        public VisitArmyController(IVisitService visitService, IVisitArmyService visitArmyService,
                                    IAccountService accountService, IAccessGroupService accessGroupService,
                                    IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _visitService = visitService;
            _visitArmyService = visitArmyService;
            _accountService = accountService;
            _accessGroupService = accessGroupService;

            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
        }

        /// <summary>
        /// Get visit list
        /// </summary>
        /// <param name="model"></param>
        /// <param name="visitorName"></param>
        /// <param name="birthDay"></param>
        /// <param name="processStatus"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitArmy)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Gets(VisitOperationTime model, string visitorName,
            string birthDay, List<int> processStatus, int pageNumber = 1, int pageSize = 10, int sortColumn = 11, string sortDirection = "desc")
        {
            var visits = _visitArmyService
                .GetPaginated(model.OpeDateFrom, model.OpeDateTo, model.OpeTimeFrom, model.OpeTimeTo,
                    visitorName, birthDay, processStatus, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<VisitListModel>).ToList();

            var pagingData = new PagingData<VisitListModel>
            {
                Data = visits,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get visitArmy by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitArmyId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new VisitArmyDataModel();
            if (id != 0)
            {
                var visit = _visitService.GetById(id);
                if (visit == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                model = Mapper.Map<VisitArmyDataModel>(visit);
            }

            _visitArmyService.InitData(model);

            model.ListCardStatus = EnumHelper.ToEnumList<VisitingCardStatusNormalType>();
            model.CardStatus = (short)VisitingCardStatusType.Delivered;
            return Ok(model);
        }


        /// <summary>
        /// Add new a visitArmy
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiVisitArmy)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult Add([FromBody]VisitArmyModel model)
        {
            // check validation of visitArmy model
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }
            var visitSetting = _visitService.GetVisitSettingCompany();

            // condition check creat visitor required approved
            if (visitSetting.ApprovalStepNumber != (short)VisitSettingType.NoStep)
            {
                if (model.ApproverId1 == 0)
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.Approvedisrequired);
                }
            }

            if (model.ApproverId1 != 0)
            {
                var firstApprovalAccounts = JsonConvert.DeserializeObject<List<int>>(visitSetting.FirstApproverAccounts);

                if (!firstApprovalAccounts.Contains(model.ApproverId1))
                {
                    return new ApiErrorResult(StatusCodes.Status403Forbidden, MessageResource.PermissionforApproved);
                }
            }

            var visitModel = Mapper.Map<VisitModel>(model);
            int visitId = _visitService.Add(visitModel);

            _visitArmyService.Add(visitId, model);

            return new ApiSuccessResult(StatusCodes.Status201Created,
             string.Format(MessageResource.MessageAddSuccess, VisitResource.lblVisit));
        }

        /// <summary>
        /// Edit visit and visitArmy by id
        /// </summary>
        /// <param name="id"> identifier of visitor </param>
        /// <param name="model"> visitArmyModel </param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiVisitArmyId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult Edit(int id, [FromBody]VisitArmyModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            var visit = _visitService.GetById(id);
            if (visit == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (visit.CreatedBy != _httpContext.User.GetAccountId())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.EditVisitFailedByAuthor);
            }

            if (visit.Status != (short)VisitChangeStatusType.Waiting)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.EditVisitFailed);
            }


            // update visit information
            var visitModel = Mapper.Map<VisitModel>(model);
            _visitService.Update(visitModel);

            // update visitArmy information
            _visitArmyService.Update(model);


            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisit, ""));
        }

        /// <summary>
        /// Delete VisitArmy by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiVisitArmyId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult Deleted(int id)
        {
            var visit = _visitService.GetById(id);
            if (visit == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (visit.Status != (short)VisitChangeStatusType.Waiting)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.DeletedVisitFailed);
            }

            if (_httpContext.User.GetAccountId() != visit.CreatedBy)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.DeletedVisitFailedRegisterPermission);
            }

            // Delete Visit
            _visitService.Delete(id);
            // Delete VisitArmy
            _visitArmyService.Delete(id);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, VisitResource.lblVisit, ""));
        }


        /// <summary>
        /// Get review count only about visitArmy
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiVisitArmyReviewCount)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult GetArmyReviewCount()
        {
            var count = _visitArmyService.GetArmyReviewCount();

            return Ok(count);
        }


    }
}
