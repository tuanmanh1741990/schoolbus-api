﻿using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.DeviceMessage;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// DeviceMessage controller
    /// </summary>
    [Produces("application/json")]
    [CheckAddOnAttribute(Constants.PlugIn.ScreenMessage)]
    public class DeviceMessageController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IDeviceMessageService _deviceMessageService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Device Message controller
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="deviceMessageService"></param>
        /// <param name="httpContextAccessor"></param>
        public DeviceMessageController(IConfiguration configuration, IDeviceMessageService deviceMessageService,
            IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _deviceMessageService = deviceMessageService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// <summary>
        /// Return Json object for all device message list
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDeviceMessage)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var devices = _deviceMessageService.GetPaginated(pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered).ToList();

            var pagingData = new PagingData<DeviceMessageListModel>
            {
                Data = devices,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Edit name of a building
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiUpdateDeviceMessage)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id,[FromBody]DeviceMessageModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            _deviceMessageService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, DeviceMessageResource.lblDeviceMessage, ""));
        }

    }
}