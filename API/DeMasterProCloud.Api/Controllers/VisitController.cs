﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.DataModel.Visit;
using Newtonsoft.Json;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.EventLog;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Visit controller
    /// </summary>
    [Produces("application/ms-excel", "application/json", "application/text")]
    public class VisitController : Controller
    {
        private readonly IVisitService _visitService;
        private readonly IAccountService _accountService;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly IAccessGroupService _accessGroupService;


        /// <summary>
        /// VisitorController constructor
        /// </summary>
        /// <param name="visitService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="configuration"></param>
        /// <param name="accountService"></param>
        /// <param name="accessGroupService"></param>
        public VisitController(IVisitService visitService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IAccountService accountService, IAccessGroupService accessGroupService)
        {
            _visitService = visitService;
            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
            _accountService = accountService;
            _accessGroupService = accessGroupService;
        }

        /// <summary>
        /// Get visit by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new VisitDataModel();
            if (id != 0)
            {
                var visit = _visitService.GetById(id);
                if (visit == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                model = Mapper.Map<VisitDataModel>(visit);
            }
            _visitService.InitData(model);

            model.ListCardStatus = EnumHelper.ToEnumList<VisitingCardStatusNormalType>();
            model.CardStatus = (short)VisitingCardStatusType.Delivered;

            return Ok(model);
        }

        /// <summary>
        /// Get visit list
        /// </summary>
        /// <param name="model"></param>
        /// <param name="visitorName"></param>
        /// <param name="birthDay"></param>
        /// <param name="visitorDepartment"></param>
        /// <param name="position"></param>
        /// <param name="visiteeSite"></param>
        /// <param name="visitReason"></param>
        /// <param name="visiteeName"></param>
        /// <param name="phone"></param>
        /// <param name="processStatus"></param>
        /// <param name="approverName1"></param>
        /// <param name="approverName2"></param>
        /// <param name="rejectReason"></param>
        /// <param name="cardId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisits)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Gets(VisitOperationTime model, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason, string visiteeName, string phone, List<int> processStatus,
            string approverName1, string approverName2, string rejectReason, string cardId, int pageNumber = 1, int pageSize = 10, int sortColumn = 11,
            string sortDirection = "desc")
        {
            var visits = _visitService
                .GetPaginated(model.OpeDateFrom, model.OpeDateTo, model.OpeTimeFrom, model.OpeTimeTo,
                    visitorName, birthDay, visitorDepartment, position, visiteeSite, visitReason, visiteeName, phone, processStatus,
                    approverName1, approverName2, rejectReason, cardId, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<VisitListModel>).ToList();

            var pagingData = new PagingData<VisitListModel>
            {
                Data = visits,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Visit Report Init
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitsReportInit)]
        public IActionResult VisitReportInit()
        {
            var model = _visitService.InitReportData();
            return Ok(model);
        }

        /// <summary>
        /// Get visit list
        /// </summary>
        /// <param name="model"></param>
        /// <param name="doorIds"></param>
        /// <param name="visitorName"></param>
        /// <param name="cardId"></param>
        /// <param name="inOutType"></param>
        /// <param name="eventType"></param>
        /// <param name="visiteeSite"></param>
        /// <param name="cardStatus"></param>
        /// <param name="birthDay"></param>
        /// <param name="visitorDepartment"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitsReport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult VisitReport(VisitOperationTime model, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType, string visiteeSite,
            List<int> cardStatus, string birthDay, string visitorDepartment,
            int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var visitReports = _visitService
                .VisitReport(model.OpeDateFrom, model.OpeDateTo, model.OpeTimeFrom, model.OpeTimeTo, doorIds,
                    visitorName, cardId, inOutType, eventType, visiteeSite, cardStatus, birthDay, visitorDepartment, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<VisitReportModel>).ToList();

            var idx = (pageNumber - 1) * pageSize + 1;
            foreach (var visitReport in visitReports)
            {
                visitReport.Id = idx++;

                var buildingName = _visitService.GetBuildingNameByRid(visitReport.Device);
                visitReport.Building = buildingName;
                visitReport.CardStatus = ((CardStatus)_visitService.GetCardStatus(visitReport.CardId)).GetDescription();
            }
            var pagingData = new PagingData<VisitReportModel>
            {
                Data = visitReports,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        /// <summary>
        /// rabbit mq test action
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitsInit)]
        public IActionResult VisitManageInit()
        {
            var model = _visitService.InitListData();
            return Ok(model);
        }

        /// <summary>
        /// Add new a visit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiVisits)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult Add([FromBody]VisitModel model)
        {
            // check validation of visit model
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }
            var visitSetting = _visitService.GetVisitSettingCompany();
            // condition check creat visitor required approved
            if (visitSetting.ApprovalStepNumber != (short) VisitSettingType.NoStep)
            {
                if (model.ApproverId1 == 0)
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, 
                        MessageResource.Approvedisrequired);
                }
            }

            if (model.ApproverId1 != 0)
            {
                var firstApprovalAccounts = JsonConvert.DeserializeObject<List<int>>(visitSetting.FirstApproverAccounts);
                if (!firstApprovalAccounts.Contains(model.ApproverId1))
                {
                    return new ApiErrorResult(StatusCodes.Status403Forbidden,
                        MessageResource.PermissionforApproved);
                }
            }
            _visitService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
             string.Format(MessageResource.MessageAddSuccess, VisitResource.lblVisit));
        }



        /// <summary>
        /// Edit visitor by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiVisitsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult Edit(int id, [FromBody]VisitModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            var visit = _visitService.GetById(id);
            if (visit == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if(visit.CreatedBy != _httpContext.User.GetAccountId())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.EditVisitFailedByAuthor);
            }
            
            if (visit.Status != (short) VisitChangeStatusType.Waiting)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.EditVisitFailed);
            }

            _visitService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisit, ""));
        }

        /// <summary>
        /// Change status
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="statusId"></param>
        /// <param name="cardId"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiVisitsChangeStatus)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult ChangeStatus(List<int> ids, int statusId, string cardId, string reason)
        {
            
            var visits = _visitService.GetByIds(ids);
            if (visits == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            //회수 처리
            if (statusId == (short) VisitStatus.Reclamation)
            {
                //if (visits.Count > 1)
                //{
                //    //교부되지 않은 카드입니다.
                //    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgNotDeliveredCard));
                //}
                var visit = _visitService.GetByCardId(cardId);
                if (visit == null)
                {
                    //교부되지 않은 카드입니다.
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgNotDeliveredCard));
                }

                if (visit.Status == (short)VisitStatus.Reclamation)
                {
                    //이미 회수된 카드입니다.
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgAlreadyReturnedCard));
                }

                var card = _visitService.GetCardByCardId(cardId);
                if (card.CardType != (short)PassType.VisitCard)
                {
                    //방문증이 아닙니다.
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgNotVisitCard));
                }

                _visitService.ReturnCard(cardId);
            }

            //교부 처리
            else if (statusId == (short) VisitStatus.DeliveryOk)
            {

                var visitCard = _visitService.GetCardByCardId(cardId);
                var visit = _visitService.GetById(ids.FirstOrDefault());

                if (visit != null && visit.Status != (short) VisitStatus.Issueing)
                {
                    //승인되지 않았습니다.
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgNotApprovedVisit));
                }
                
                if (visitCard != null && visitCard.VisitId != null)
                {
                    //이미 교부된 카드입니다.
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity,
                        string.Format(VisitResource.msgAlreadyDeliveredCard));
                }
                _visitService.PassOnCard(ids.FirstOrDefault(), cardId);
            }

            //이 외에
            else
            {
                _visitService.ChangeStatus(statusId, ids, reason);
            }

            
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisit, ""));
        }

        /// <summary>
        ///  
        /// </summary>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="visitorName"></param>
        /// <param name="birthDay"></param>
        /// <param name="visitorDepartment"></param>
        /// <param name="position"></param>
        /// <param name="visiteeSite"></param>
        /// <param name="visitReason"></param>
        /// <param name="visiteeName"></param>
        /// <param name="phone"></param>
        /// <param name="cardStatus"></param>
        /// <param name="approverName1"></param>
        /// <param name="approverName2"></param>
        /// <param name="rejectReason"></param>
        /// <param name="cardId"></param>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [Produces("application/csv", "application/ms-excel")]
        [HttpGet]
        [Route(Constants.Route.ApiVisitsExport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult ExportVisitManage(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, string type = "excel", int sortColumn = 1, string sortDirection = "desc")
        {
            var fileData = _visitService.Export(type, accessDateFrom,
                accessDateTo, accessTimeFrom, accessTimeTo, visitorName,
                birthDay, visitorDepartment, position, visiteeSite, visitReason,
                visiteeName, phone, cardStatus, approverName1, approverName2,
                rejectReason, cardId, sortColumn, sortDirection, out var totalRecords, out var recordsFiltered);
            var filename = string.Format(Constants.ExportFileFormat, VisitResource.lblExport + "_Visitor", DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";
            if (totalRecords == 0 || recordsFiltered == 0)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageExportDataIsZero, VisitResource.lblVisit));
            }

            return File(fileData, type.Equals("excel") ? "application/ms-excel" : "application/csv", fullName);
        }

        /// <summary>
        /// Export 
        /// </summary>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="doorIds"></param>
        /// <param name="visitorName"></param>
        /// <param name="cardId"></param>
        /// <param name="inOutType"></param>
        /// <param name="eventType"></param>
        /// <param name="visiteeSite"></param>
        /// <param name="cardStatus"></param>
        /// <param name="birthDay"></param>
        /// <param name="visitorDepartment"></param>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [Produces("application/csv", "application/ms-excel")]
        [HttpGet]
        [Route(Constants.Route.ApiVisitsReportExport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult ExportVisitReportManage(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, string type = "excel", int sortColumn = 1, string sortDirection = "desc")
        {
            var fileData = _visitService.ExportVisitReport(type, accessDateFrom,
                accessDateTo, accessTimeFrom, accessTimeTo, doorIds,
                visitorName, cardId, inOutType, eventType, visiteeSite,
                cardStatus, birthDay, visitorDepartment, sortColumn, sortDirection, out var totalRecords, out var recordsFiltered);
            var filename = string.Format(Constants.ExportFileFormat, VisitResource.lblExport + "_VisitorReport", DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";
            if (totalRecords == 0 || recordsFiltered == 0)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageExportDataIsZero, VisitResource.lblVisit));
            }

            return File(fileData, type.Equals("excel") ? "application/ms-excel" : "application/csv", fullName);
        }



        /// <summary>
        /// rabbit mq test action
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitsPreRegisterInit)]
        public IActionResult VisitPreRegisterInit()
        {
            var model = new VisitDataModel();
            _visitService.InitData(model);
            model.VisitType = (short)VisitType.OutSider;
            model.ListCardStatus = EnumHelper.ToEnumList<VisitingCardStatusPreRegisterType>();
            model.CardStatus = (short) VisitingCardStatusPreRegisterType.Request;
            return Ok(model);
        }

        /// <summary>
        /// Add new a visit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiVisitsPreRegister)]
        //[Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult VisitPreRegister([FromBody]VisitModel model)
        {
            //if (!ModelState.IsValid)
            //{
            //    return new ValidationFailedResult(ModelState);
            //}

            


            _visitService.PreRegister(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, VisitResource.lblVisit));
        }

        ///// <summary>
        ///// Edit user by id
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //[HttpPut]
        //[Route(Constants.Route.ApiVisitsReleaseCard)]
        //[Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public IActionResult ReleaseCard(string cardId)
        //{

        //    var visit = _visitService.GetByCardId(cardId);
        //    if (visit == null)
        //    {
        //        //교부되지 않은 카드입니다.
        //        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgNotDeliveredCard));
        //    }

        //    if (visit.Status == (short) VisitStatus.Reclamation)
        //    {
        //        //이미 회수된 카드입니다.
        //        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgAlreadyReturnedCard));
        //    }

        //    var card = _visitService.GetCardByCardId(cardId);
        //    if (card.CardType != (short) PassType.VisitCard)
        //    {
        //        //방문증이 아닙니다.
        //        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(VisitResource.msgNotVisitCard));
        //    }

        //    _visitService.ReturnCard(cardId);
        //    return new ApiSuccessResult(StatusCodes.Status200OK,
        //        string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisit));
        //}
        
        
        /// <summary>
        /// Update visit setting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiVisitsSetting)]
        [Authorize(Policy = Constants.Policy.PrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult EditVisitSettingType([FromBody]VisitSettingModel model)
        {
            if (model.ApprovalStepNumber != 0)
            {
                if (model.FirstApproverAccounts == null)
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, VisitResource.RequiredApprover);
                }
                // Check list account added is belong to company
                try
                {
                    var listAccountFirstApprove =  JsonConvert.DeserializeObject<List<int>>(model.FirstApproverAccounts);
                    foreach (var id in listAccountFirstApprove)
                    {
                        var existed = _accountService.IsExistAccount(id, _httpContext.User.GetCompanyId());
                        if (!existed)
                        {
                            return new ApiErrorResult(StatusCodes.Status403Forbidden, 
                                string.Format(MessageResource.AccountIsInValid, id));
                        }
                    }
                }
                catch (Exception e)
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, e.Message);
                }
                
                if (model.ApprovalStepNumber == (short) VisitSettingType.SecondsStep)
                {
                    if (model.SecondsApproverAccounts == null)
                    {
                        return new ApiErrorResult(StatusCodes.Status400BadRequest, VisitResource.RequiredApprover);
                    }
                    // Check list account added is belong to company
                    try
                    {
                        var listAccountSecondsApprove =  JsonConvert.DeserializeObject<List<int>>(model.SecondsApproverAccounts);
                        foreach (var id in listAccountSecondsApprove)
                        {
                            var existed = _accountService.IsExistAccount(id, _httpContext.User.GetCompanyId());
                            if (!existed)
                            {
                                return new ApiErrorResult(StatusCodes.Status403Forbidden, 
                                    string.Format(MessageResource.AccountIsInValid, id));
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return new ApiErrorResult(StatusCodes.Status400BadRequest, e.Message);
                    }
                }
            }
            _visitService.UpdateVisitSettingCompany(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisitSetting, VisitResource.lblCompany));
        }
        
        /// <summary>
        /// Get visit setting
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiVisitsSetting)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult GetVisitSetting()
        {
            var visitSetting = _visitService.GetVisitSettingCompany();
            return Ok(visitSetting);
        }
        
        
        [HttpGet]
        [Route(Constants.Route.ApiVisitIdentification)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult GetIdentificationVisitor(int id)
        {
            var cards = _visitService.GetCardByVisitor(id);
            return Ok(cards);
        }
        
        /// <summary>
        /// Add identification to visitor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiVisitIdentification)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult AddIdentificationVisitor(int id, [FromBody] CardModel model)
        {
            if (model.CardId != null)
            {
                var visitSetting = _visitService.GetVisitSettingCompany();
                var visitor = _visitService.GetById(id);
                if (visitor == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }

                if (visitor.Status == (short) VisitChangeStatusType.Approved)
                { 
                    var isCardExist = _visitService.IsCardIdExist(model.CardId, visitor.CompanyId);

                    if (isCardExist)
                    {
                        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, UserResource.lblCardId));
                    }
                    else
                    {
                        var card = _visitService.GetCardByVisitor(id);

                        if (card != null)
                        {
                            if (card.CardId != model.CardId)
                            {
                                _visitService.DeletedAssignedCardVisitor(id);

                                var cardId = _visitService.AssignedCardVisitor(id, model);
                            }

                            return new ApiSuccessResult(StatusCodes.Status200OK,
                                string.Format(MessageResource.MessageSuccessIssueCard));
                        }
                        else
                        {
                            var cardId = _visitService.AssignedCardVisitor(id, model);

                            return new ApiSuccessResult(StatusCodes.Status200OK,
                                string.Format(MessageResource.MessageSuccessIssueCard));
                        }
                    }
                }
                else
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, 
                        MessageResource.CannotAssignedCardNotApproved);
                }

            }
            return new ApiErrorResult(StatusCodes.Status400BadRequest, 
                MessageResource.CardIdBlank);
        }

        [HttpPatch]
        [Route(Constants.Route.ApiApprovedVisitor)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult ApprovedVisitor(int id ,[FromBody]ApprovedModel model)
        {
            var visitSetting = _visitService.GetVisitSettingCompany();

            var visitor = _visitService.GetById(id);
            if (visitor == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            
            if (visitor.Status == (short) VisitChangeStatusType.Rejected || 
                visitor.Status == (short) VisitChangeStatusType.Finished ||
                visitor.Status == (short) VisitChangeStatusType.FinishedWithoutReturnCard ||
                visitor.Status == (short) VisitChangeStatusType.CardIssued)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, 
                    MessageResource.CannotContinueApproved);
            }

            if (visitSetting.ApprovalStepNumber != (short) VisitSettingType.NoStep)
            {
                if (visitSetting.ApprovalStepNumber == (short) VisitSettingType.FirstStep)
                {
                    if (visitor.Status == (short) VisitChangeStatusType.Waiting)
                    {
                        if (_httpContext.User.GetAccountId() != visitor.ApproverId1)
                        {
                            return new ApiErrorResult(StatusCodes.Status403Forbidden,
                                MessageResource.PermissionforApproved);
                        }

                        if (model.Approved)
                        {
                            _visitService.UpdateApprovalVisitor(id, (short) VisitChangeStatusType.Approved);
                        }
                        else
                        {
                            _visitService.UpdateApprovalVisitor(id, (short) VisitChangeStatusType.Rejected);
                        }
                        return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.ApprovedSuccessfully);
                    }
                    else
                    {
                        return new ApiErrorResult(StatusCodes.Status400BadRequest,
                            MessageResource.CannotContinueApproved);
                    }
                }
                else
                {
                    if (visitor.Status == (short) VisitChangeStatusType.Waiting)
                    {
                        if (_httpContext.User.GetAccountId()!=visitor.ApproverId1)
                        {
                            return new ApiErrorResult(StatusCodes.Status403Forbidden,
                                MessageResource.PermissionforApproved);
                        }
                        if (model.Approved)
                        {
                            _visitService.UpdateApprovalVisitor(id, (short) VisitChangeStatusType.Approved1);
                        }
                        else
                        {
                            _visitService.UpdateApprovalVisitor(id, (short) VisitChangeStatusType.Rejected);
                        }
                        return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.ApprovedSuccessfully);
                    } else if (visitor.Status == (short) VisitChangeStatusType.Approved1)
                    {
                        var secondApprovalAccounts = JsonConvert.DeserializeObject<List<int>>(visitSetting.SecondsApproverAccounts);
                        if (!secondApprovalAccounts.Contains(_httpContext.User.GetAccountId()))
                        {
                            return new ApiErrorResult(StatusCodes.Status403Forbidden,
                                MessageResource.PermissionforApproved);
                        }
                        if (model.Approved)
                        {
                            _visitService.UpdateApprovalVisitor(id, (short) VisitChangeStatusType.Approved);
                        }
                        else
                        {
                            _visitService.UpdateApprovalVisitor(id, (short) VisitChangeStatusType.Rejected);
                        }
                        return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.ApprovedSuccessfully);
                    }
                    else
                    {
                        return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.CannotContinueApproved);
                    }
                }
                
            }
            
            return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.CannotContinueApproved);
        }
        

        /// <summary>
        /// Assign doors to visitor ( AccessGroup )
        /// </summary>
        /// <param name="id"></param>
        /// <param name="door"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiAssignedDoorVisitor)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult AssingedDoorVisitor(int id, List<int> door)
        {
            var success = _visitService.AssignedDoorVisitor(id, door);
            if (success)
            {
                return new ApiSuccessResult(StatusCodes.Status201Created,
                    string.Format(MessageResource.MessageAddSuccess, AccessGroupResource.lblAccessGroup));
            }
            return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.CannotAssignedDoorVisitor);
        }
        
        [HttpPatch]
        [Route(Constants.Route.ApiRejectVisitor)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult RejectVisitor(int id, [FromBody]RejectedModel model )
        {
            var success = _visitService.RejectVisitor(id, model);
            if (success)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    MessageResource.RejectSuccess);
            }
            return new ApiErrorResult(StatusCodes.Status400BadRequest,
                MessageResource.RejectFailed);
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiHistoryVisitor)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult VisitHistoryLog(int id, int pageNumber = 1, int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            var visitReports = _visitService
                .GetPaginatedVisitHistoryLog(id, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().ToList();
            
            var pagingData = new PagingData<VisitListHistoryModel>
            {
                Data = visitReports,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }
        
        [HttpGet]
        [Route(Constants.Route.ApiGetLengthVisitsReview)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult GetLengthVisitNeedToReview()
        {
            var count = _visitService.LengthVisitNeedToReview();
            return Ok(count);

        }

        /// <summary>
        /// Return visitor's card.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiReturnCard)]
        [Authorize(Policy = Constants.Policy.PrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult ReturnCard([FromBody]GetBackVisit model)
        {
            if (model.VisitIds == null || !model.VisitIds.Any())
            {
                if (model.CardId == null || string.IsNullOrEmpty(model.CardId))
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, 
                        MessageResource.CardIdBlank);
                }
                _visitService.ReturnCardVisitor(model.CardId, model.Reason);
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisit, ""));
            }
            _visitService.ReturnVisitor(model.VisitIds, model.Reason);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, VisitResource.lblVisit, ""));
        }
        
        /// <summary>
        /// Delete Visit by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiVisitsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.VisitManagement)]
        public IActionResult Deleted(int id)
        {
            var visit = _visitService.GetById(id);
            if (visit == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (visit.Status != (short) VisitChangeStatusType.Waiting)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.DeletedVisitFailed);
            }
            
            if (_httpContext.User.GetAccountId() != visit.CreatedBy)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, MessageResource.DeletedVisitFailedRegisterPermission);
            }

            _visitService.Delete(id);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, VisitResource.lblVisit, ""));
        }

        /// <summary>
        /// Return History of Visitor
        /// </summary>
        /// <param name="id">VisitId</param>
        /// <param name="deviceId">DeviceId</param>
        /// <param name="pageNumber">current page number</param>
        /// <param name="pageSize">Number of items to show on a page</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiVisitsAccessHistory)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetHistoryVisitor(int id, int pageNumber = 1, int pageSize = 10)
        {
            var devices = _visitService.GetHistoryVisitor(id, pageNumber, pageSize, out var recordsTotal).AsEnumerable().Select(Mapper.Map<EventLogListModel>).ToList();
           
            var pagingData = new PagingData<EventLogListModel>
            {
                Data = devices == null ? new List<EventLogListModel>() : devices,

                Meta = { RecordsTotal = devices.Count == 0 ? 0 : recordsTotal }
            };
            return Ok(pagingData);
        }

    }
}
