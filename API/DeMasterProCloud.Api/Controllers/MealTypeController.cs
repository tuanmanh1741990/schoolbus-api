﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealType;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// mealType controller
    /// </summary>
    [Produces("application/json")]
    [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
    public class MealTypeController : Controller
    {
        private readonly IMealTypeService _mealTypeService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Controller of mealType
        /// </summary>
        /// <param name="mealTypeService"></param>
        /// <param name="httpContextAccessor"></param>
        public MealTypeController(IMealTypeService mealTypeService, IHttpContextAccessor httpContextAccessor)
        {
            _mealTypeService = mealTypeService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// <summary>
        /// get (a) meal type(s) by search filter
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCanteenMealTypes)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetMealTypes(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            var mealTypes = _mealTypeService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            var categoryPagingData = new PagingData<MealTypeModel>
            {
                Data = mealTypes,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(categoryPagingData);
        }

        /// <summary>
        /// get a mealType by Id
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCanteenMealTypeId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var mealType = _mealTypeService.GetMealTypeByIdAndCompanyId(id, _httpContext.User.GetCompanyId());
            if (mealType == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var mealTypeModel = Mapper.Map<MealTypeModel>(mealType);

            return Ok(mealTypeModel);
        }

        /// <summary>
        /// add a mealType
        /// </summary>
        /// <param name="model"> MealType Model </param>
        [HttpPost]
        [Route(Constants.Route.ApiCanteenMealTypes)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]MealTypeModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            int mealTypeId = _mealTypeService.Add(model);

            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, CanteenResource.lblMealType), mealTypeId.ToString());
        }

        /// <summary>
        /// edit a mealType
        /// </summary>
        /// <param name="id"> identifier of mealType </param>
        /// <param name="model"> MealTypeModel </param>
        [HttpPut]
        [Route(Constants.Route.ApiCanteenMealTypeId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]MealTypeModel model)
        {

            model.Id = id;
            //ModelState.Clear();
            //if (!TryValidateModel(model))
            //{
            //    return new ValidationFailedResult(ModelState);
            //}

            var companyId = _httpContext.User.GetCompanyId();

            var mealType = _mealTypeService.GetMealTypeByIdAndCompanyId(id, companyId);
            var checkValidate = _mealTypeService.GetMealTypeByCompanyIdAndCode(id, companyId, model.Code);

            if (mealType == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            if (checkValidate == null)
            {
                _mealTypeService.Update(model);
            }
            else
            {
                return new ValidationFailedResult(ModelState);
            }



            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, CanteenResource.lblMealType, ""));
        }

        /// <summary>
        /// delete a mealType by id
        /// </summary>
        /// <param name="id"> identifier of mealType </param>
        [HttpDelete]
        [Route(Constants.Route.ApiCanteenMealTypeId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var mealType = _mealTypeService.GetMealTypeByIdAndCompanyId(id, companyId);
            if (mealType == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _mealTypeService.Delete(mealType);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, CanteenResource.lblMealType));
        }

        /// <summary>
        /// Get a list of avilable MealType code.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCanteenMealTypeCodeList)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetCodeList()
        {
            var codeList = _mealTypeService.GetAvailableMealTypeCodeList();

            return Ok(codeList);
        }



    }
}