﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.Service;
using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.Web;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.IO;
using DeMasterProCloud.DataModel.SystemLog;
using DeMasterProCloud.Common.Resources;


namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// SystemLog controller
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SystemLogController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ISystemLogService _systemLogService;
        private readonly HttpContext _httpContext;
        private IConverter _converter;

        private readonly string[] _headers = {
            "Request",
            "Number of event",
            "Time receiving request (t1)",
            "Time finishing request (t2)",
            "Processing time in seconds (t2-t1)"
        };

        /// <summary>
        /// Report controller
        /// </summary>
        /// <param name="systemLogService"></param>
        /// <param name="configuration"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="converter"></param>
        public SystemLogController(ISystemLogService systemLogService,
            IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IConverter converter)
        {
            _systemLogService = systemLogService;
            _configuration = configuration;
            _httpContext = httpContextAccessor.HttpContext;
            _converter = converter;
        }

        /// <summary>
        /// get System Log paginated
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSystemLogs)]
        public IActionResult SystemLog(SystemLogOperationTime model, int? objectType, int? company, string search, int? actionType, int pageNumber = 1,
            int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            var systemLogs = _systemLogService.GetPaginated(
                    model.OpeDateFrom, model.OpeDateTo, model.OpeTimeFrom,
                    model.OpeTimeTo, objectType, actionType, company, search, out var totalRecords,
                    out var recordsFiltered, pageNumber, pageSize, sortColumn, sortDirection);

            var pagingData = new PagingData<SystemLogListModel>
            {
                Data = systemLogs,
                Meta = { RecordsTotal = totalRecords, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// GET /system-logs/ActionListItems
        /// <summary>
        /// Get Action List items of System Log
        /// </summary>
        /// <param name="systemLogType"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSystemLogsActionListItems)]
        public IActionResult GetActionListItems(int systemLogType)
        {
            var actionListItems = _systemLogService.GetActionListItems(systemLogType);
            return Ok(actionListItems);
        }

        /// GET /system-logs/SystemLogListItems
        /// <summary>
        /// Get SystemLogTypelistItems
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSystemLogsTypeListItems)]
        public IActionResult GetSystemLogTypes()
        {
            var result = _systemLogService.GetSystemLogTypeListItems();
            return Ok(result);
        }


        /// <summary>
        /// View the system log data through pdf file.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="objectType"></param>
        /// <param name="action"></param>
        /// <param name="company"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSystemLogsExportPdf)]
        public IActionResult PdfView(SystemLogOperationTime model, int sortColumn, string sortDirection,
            int? objectType, int? action, int? company)
        {
            var accountType = _httpContext.User.GetAccountType();
            var systemLogs = _systemLogService.FilterDataWithOrder(sortColumn, sortDirection, out _, out _
                    , model.OpeDateFrom, model.OpeDateTo, model.OpeTimeFrom, model.OpeTimeTo, objectType, action,
                    company).AsEnumerable()
                .Select(m =>
                {
                    var result = new SystemLogListModel
                    {
                        //UserAccount = (accountType == (short)AccountType.SuperAdmin)
                        //    ? m.Company.Name
                        //    : m.CreatedByNavigation.Username,
                        UserAccount = m.UserAccount,
                        OperationTime = m.OperationTime,
                        OperationType = m.OperationType,
                        Action = m.Action,
                        Message = HttpUtility.HtmlDecode(m.Message),
                    };
                    return result;
                }).ToList();

            ViewBag.Message = systemLogs;

            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
            };

            var view = this.View("PdfView");
            var html = ViewResultExtensions.ToHtml(View(), _httpContext);
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = html,
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/css", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" },
            };

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            var file = _converter.Convert(pdf);
            var filename = string.Format(Constants.ExportFileFormat, SystemLogResource.lblSystemLog, DateTime.Now) + ".pdf";

            _systemLogService.SaveSystemLogExport(filename);

            return File(file, "application/pdf", filename);
        }

        /// GET /system-logs/export
        /// <summary>
        /// Export file for system Logs
        /// </summary>
        /// <param name="model"></param>
        /// <param name="objectType"></param>
        /// <param name="company"></param>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSystemLogsExport)]
        public IActionResult Export(SystemLogOperationTime model, int? objectType, int? action, int? company,
            string type = "excel", string filter = "", int sortColumn = 1, string sortDirection = "desc")
        {
            //var stopWatch = Stopwatch.StartNew();
            var fileData = _systemLogService.Export(type, sortColumn, sortDirection, out _, out _
                , model.OpeDateFrom, model.OpeDateTo, model.OpeTimeFrom, model.OpeTimeTo, objectType, action, company);
            //stopWatch.Stop();
            var filename = string.Format(Constants.ExportFileFormat, SystemLogResource.lblSystemLog, DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";

            _systemLogService.SaveSystemLogExport(fullName);

            if (type != null && type.Equals("excel"))
                return File(fileData, "application/ms-excel", fullName);
            return File(fileData, "application/text", fullName);

        }
    }
}