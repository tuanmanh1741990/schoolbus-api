﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Holiday controller
    /// </summary>
    [Produces("application/json")]
    [CheckMultipleAddOnAttribute(new string [] { Constants.PlugIn.TimeAttendance, Constants.PlugIn.AccessControl })]
    public class HolidayController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IHolidayService _holidayService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Holiday controller
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="holidayService"></param>
        /// <param name="httpContextAccessor"></param>
        public HolidayController(IConfiguration configuration, IHolidayService holidayService,
            IHttpContextAccessor httpContextAccessor)
        {
            
            _configuration = configuration;
            _holidayService = holidayService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// GET /holidays : return all existing holidays with paging and sorting
        /// <summary>
        /// Return Json object for all holiday list
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiHolidays)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var holidays = _holidayService.GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<HolidayListModel>).ToList();

            var pagingData = new PagingData<HolidayListModel>
            {
                Data = holidays,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// POST /holidays : Create new holiday
        /// <summary>
        /// Add holiday
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiHolidays)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]HolidayModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }
            //32 is MAX Holiday
            //I'll change it with define constant to constants.cs
            var maxHoliday = Constants.Settings.DefaultMaxHoliday;
            if (!string.IsNullOrEmpty(_configuration[Constants.Settings.MaxHoliday]))
            {
                maxHoliday = Convert.ToInt32(_configuration[Constants.Settings.MaxHoliday]);
            }

            var holidayCount =
                Convert.ToDateTime(model.EndDate).Subtract(Convert.ToDateTime(model.StartDate)).TotalDays +
                _holidayService.GetHolidayCount(_httpContext.User.GetCompanyId());
            if (holidayCount >= maxHoliday)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(HolidayResource.msgLimitHoliday, maxHoliday));
            }

            _holidayService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, HolidayResource.lblHoliday));
        }

        /// GET /holidays/{ID} : Get details of one holiday
        /// <summary>
        /// Get holiday infor by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiHolidaysId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new HolidayModel();
            if (id != 0)
            {
                var holiday = _holidayService.GetHolidayByIdAndCompany(_httpContext.User.GetCompanyId(), id);

                if (holiday == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                model = Mapper.Map<HolidayModel>(holiday);
            }
            _holidayService.InitData(model);
            return Ok(model);
        }

        /// PUT /holidays/{ID} : Update a holiday
        /// <summary>
        /// Update holiday
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiHolidaysId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]HolidayModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            
            var holiday = _holidayService.GetHolidayByIdAndCompany(_httpContext.User.GetCompanyId(), id);
            if (holiday == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _holidayService.Update(model, holiday);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, HolidayResource.lblHoliday, holiday.Name));
        }

        /// DELETE /holidays : Delete multiple holidays by list of ids
        /// <summary>
        /// Delete holidays by multi id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiHolidays)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var holidays = _holidayService.GetByIds(ids)
                .ToList();
            if (!holidays.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _holidayService.DeleteRange(holidays);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, HolidayResource.lblHoliday));
        }

        /// DELETE /holidays/{ID} : Delete 1 holiday by ID
        /// <summary>
        /// Delete holiday by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiHolidaysId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var holiday = _holidayService.GetHolidayByIdAndCompany(_httpContext.User.GetCompanyId(), id);
            if (holiday == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _holidayService.Delete(holiday);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, HolidayResource.lblHoliday));
        }
    }
}