﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.ExceptionalMeal;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeMasterProCloud.Api.Controllers
{
    [Produces("application/json")]
    public class ExceptionalMealController : Controller
    {
        private readonly IExceptionalMealService _exceptionalMealService;

        /// <summary>
        /// Device controller
        /// </summary>
        /// <param name="userDiscountService"></param>
        public ExceptionalMealController(IExceptionalMealService exceptionalMealService)
        {
            _exceptionalMealService = exceptionalMealService;

        }



        /// <summary>
        /// Return Apiresult add new userDiscount
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiExceptionalMeal)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Add([FromBody]AddExceptionalMealModel model)
        {
            var exceptionalMeal = _exceptionalMealService.AddExceptionalMeal(model);
            if (exceptionalMeal == null || exceptionalMeal.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (exceptionalMeal.message == null && exceptionalMeal.statusCode == true)
            {
                //  return new ApiSuccessResult(StatusCodes.Status201Created,
                //string.Format(MessageResource.Exist, CanteenResource.lblCornerSetting));
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.lblExceptionalMeal));
            }

            return new ApiSuccessResult(StatusCodes.Status201Created,
               string.Format(MessageResource.MessageAddSuccess, CanteenResource.lblExceptionalMeal), exceptionalMeal.data.ToString());
        }


        /// <summary>
        /// Return Apiresult update userDiscount
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateExceptionalMeal)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Update(int id, [FromBody]AddExceptionalMealModel model)
        {
            if (id == null || id == 0)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }
            var exceptionalMeal = _exceptionalMealService.UpdateExceptionalMeal(id, model);
            if (exceptionalMeal == null || exceptionalMeal.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageUpdateSuccess, "", CanteenResource.lblExceptionalMeal));
        }

        [HttpDelete]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateExceptionalMeal)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Delete(int id)
        {
            var exceptionalMeal = _exceptionalMealService.DeleteExceptionalMeal(id);
            if (exceptionalMeal == null || exceptionalMeal.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageDeleteSuccess, CanteenResource.lblExceptionalMeal));
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiExceptionalMeal)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetListlistExceptionalMeal(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var listExceptionalMealModel = _exceptionalMealService.GetListExceptionalMeal(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered);
            var pagingData = new PagingData<ExceptionalMealModel>
            {
                Data = listExceptionalMealModel,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(pagingData);
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateExceptionalMeal)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetUserDiscount(int id)
        {
            var exceptionalMeal = _exceptionalMealService.GetExceptionalMeal(id);

            return Ok(exceptionalMeal);
        }
    }
}