﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Department controller
    /// </summary>
    [Produces("application/json")]
    public class DepartmentController : Controller
    {
        private readonly IDepartmentService _departmentService;
        private readonly HttpContext _httpContext;
        /// <summary>
        /// Ctor of department
        /// </summary>
        /// <param name="departmentService"></param>
        /// <param name="httpContextAccessor"></param>
        public DepartmentController(IDepartmentService departmentService,
            IHttpContextAccessor httpContextAccessor)
        {
            _departmentService = departmentService;
            _httpContext = httpContextAccessor.HttpContext;

        }

        /// <summary>
        /// get (a) department(s) by search filter
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route(Constants.Route.ApiDepartments)]
        public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var departments = _departmentService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            var pagingData = new PagingData<DepartmentListModel>
            {
                Data = departments,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(pagingData);
        }


        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route(Constants.Route.ApiDepartmentsChild)]
        public IActionResult GetDepartmentHiearchy(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
           string sortDirection = "desc")
        {
            var departments = _departmentService.GetListDepartment(search, out var recordsTotal, out var recordsFiltered, pageNumber, pageSize, sortColumn, sortDirection);
            var pagingData = new PagingData<DepartmentListItemModel>
            {
                Data = departments.OrderBy(x => x.Id).ToList(),
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(pagingData);
        }

        /// <summary>
        /// get a department by Id
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [Route(Constants.Route.ApiDepartmentsId)]
        public IActionResult Get(int id)
        {
            var department = _departmentService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (department == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            var model = Mapper.Map<DepartmentModel>(department);
            _departmentService.InitDepartment(model);
            return Ok(model);
        }

        /// <summary>
        /// add a departments 
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [Route(Constants.Route.ApiDepartments)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]DepartmentModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            if (model.DepartmentManagerId != null)
            {
                var account = _departmentService.GetAccount(_httpContext.User.GetCompanyId(), model.DepartmentManagerId.Value);
                if (account == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
            }


            var department = _departmentService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, DepartmentResource.lblDepartment), department.ToString());
        }

        /// <summary>
        /// edit a department
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        [HttpPut]
        [Route(Constants.Route.ApiDepartmentsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]DepartmentModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            var department = _departmentService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (department == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (model.DepartmentManagerId != 0)
            {
                var account = _departmentService.GetAccount(_httpContext.User.GetCompanyId(), model.DepartmentManagerId.Value);
                if (account != null)
                {
                    if (account.CompanyId != department.CompanyId)
                    {
                        return new ApiErrorResult(StatusCodes.Status404NotFound);
                    }
                }
                else
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
            }
            else
            {
                model.DepartmentManagerId = null;
            }
            // check parentid
            var checkUpdateDepartment = _departmentService.CheckEditDepartment(model);
            if (checkUpdateDepartment)
            {
                _departmentService.Update(model);
            }
            else
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.ParentIdInvalid, DepartmentResource.lblDepartment, ""));
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, DepartmentResource.lblDepartment, ""));
        }

        /// <summary>
        /// delete a department by id
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [Route(Constants.Route.ApiDepartmentsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var currentCompanyId = _httpContext.User.GetCompanyId();

            var department = _departmentService.GetByIdAndCompany(id, currentCompanyId);
            if (department == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (_departmentService.GetByCompanyId(currentCompanyId).Count() <= 1 || _departmentService.IsUserExist(id))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, $"{DepartmentResource.msgNotDeleted} ({MessageResource.ExistUser})");
            }

            _departmentService.Delete(department);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, DepartmentResource.lblDepartment));
        }

        /// <summary>
        /// get department(s) by id list
        /// </summary>
        /// <param name="ids">list to ids on the device to delete</param>
        [HttpDelete]
        [Route(Constants.Route.ApiDepartments)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var currentCompanyId = _httpContext.User.GetCompanyId();

            var departments = _departmentService.GetByIdsAndCompany(ids, currentCompanyId);
            if (departments == null || !departments.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            foreach (var id in ids)
            {
                if (_departmentService.GetByCompanyId(currentCompanyId).Count() <= 1 || _departmentService.IsUserExist(id))
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, $"{DepartmentResource.msgNotDeleted} ({MessageResource.ExistUser})");
                }
            }

            _departmentService.DeleteRange(departments);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, DepartmentResource.lblDepartment));
        }

        /// <summary>
        /// 
        /// </summary>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDepartmentsHierarchy)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetListDepartment()
        {
            var departments = _departmentService.GetDeparmentHierarchy();
            return Json(departments);
        }

        /// <summary>
        /// Import departments data
        /// </summary>
        /// <param name="file"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiDepartmentsImport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ImportDepartment(IFormFile file, string type = "excel")
        {
            if (file.Length == 0)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound, MessageResource.msgFailLengthFile);
            }
            var fileType = FileHelpers.GetFileExtension(file);
            var excelExtension = new[] { ".xls", ".xlsx" };
            var txtExtension = new[] { ".csv" };
            if (type.Equals("excel", StringComparison.OrdinalIgnoreCase) && !excelExtension.Contains(fileType) ||
                type.Equals("csv", StringComparison.OrdinalIgnoreCase) && !txtExtension.Contains(fileType))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(MessageResource.msgErrorFileExtension, type));
            }

            var result = _departmentService.ImportFile(type, file, out int total, out int fail);

            if (!result)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(MessageResource.msgFailedToImportDetails, fail, total));
            }
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.msgSuccessToImportDetails, total));


            // TODO
            // return true -> all datas are imported normally.
            // return false -> There is failed to import data.
        }


        /// <summary>
        /// Export departments data
        /// </summary>
        /// <param name="type"> file type </param>
        /// <param name="filter"> search filter </param>
        /// <param name="sortColumn"> column based on alignment </param>
        /// <param name="sortDirection"> ascending or descending </param>
        /// <returns></returns>
        [Produces("application/csv", "application/ms-excel")]
        [HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiDepartmentsExport)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin,
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ExportDepartment(string type = "excel", string filter = "", int sortColumn = 0,
            string sortDirection = "desc")
        {
            var fileData = _departmentService.Export(type, filter, sortColumn, sortDirection, out int totalRecords,
                out int recordsFiltered);

            if (totalRecords == 0 || recordsFiltered == 0)
            {
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageExportDataIsZero, UserResource.lblUser));
            }

            var filename = string.Format(Constants.ExportFileFormat, "export_department", DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.csv";
            return File(fileData, type.Equals("excel") ? "application/ms-excel" : "application/csv", fullName);
        }
    }
}