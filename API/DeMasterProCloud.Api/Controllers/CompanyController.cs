﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.DataModel.Company;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.PlugIn;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Company Controller
    /// </summary>
    //[ApiExplorerSettings(IgnoreApi = true)]
    [Produces("application/json")]
    [Authorize(Policy = Constants.Policy.SystemAndSuperAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CompanyController : Controller
    {
        private readonly ICompanyService _companyService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Company controller
        /// </summary>
        /// <param name="companyService"></param>
        /// <param name="httpContextAccessor"></param>
        public CompanyController(ICompanyService companyService,IHttpContextAccessor httpContextAccessor)
        {
            _companyService = companyService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        ///// <summary>
        ///// Get Logo image from Company
        ///// </summary>
        ///// <param name="logoBase64"></param>
        ///// <returns></returns>
        //[HttpPut]
        //[Route(Constants.Route.ApiCompaniesUpdateLogo)]
        //public IActionResult UpdateLogo(string logoBase64)
        //{
        //    if (!string.IsNullOrEmpty(logoBase64))
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest);
        //    }
        //    _companyService.UpdateLogo(logoBase64);
        //    return new ApiSuccessResult(StatusCodes.Status200OK,
        //        string.Format(MessageResource.MessageLogoUpdateSuccess, CompanyResource.lblCompany));
        //}

        ///// <summary>
        ///// Get Logo image from Company
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Route(Constants.Route.ApiCompaniesGetLogo)]
        //public IActionResult GetLogo()
        //{
        //    var currentCompany = _companyService.GetById(_httpContext.User.GetCompanyId());
        //    if (currentCompany == null)
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest);
        //    }
        //    var logo = _companyService.GetCurrentLogo(currentCompany);
        //    return Ok(logo);
        //}


        /// <summary>
        /// Get the Company information
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCompaniesId)]
        public IActionResult Get(int id)
        {
            var company = _companyService.GetById(id);
            if (company == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            var model = Mapper.Map<CompanyModel>(company);
            model.LogoEnable = true;
            model.MiniLogoEnable = true;
            return Ok(model);
        }

        /// <summary>
        /// Get Companies information
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCompanies)]
        public IActionResult Gets(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            var companies = _companyService.GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal, out var recordsFiltered).ToList();
            if (companies == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var pagingData = new PagingData<CompanyListModel>
            {
                Data = companies,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get Company information
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCompanyId)]
        public IActionResult GetInfomation(int id)
        {
            var companyInfo = _companyService.GetDefaultInfoById(id);
            if (companyInfo == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return Ok(companyInfo);
        }

        /// <summary>
        /// Add a company
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiCompanies)] 
        public IActionResult Add([FromBody]CompanyModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }
            var companyId= _companyService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, CompanyResource.lblCompany), companyId.ToString());
        }

        /// <summary>
        /// Assign device(s) to a Company
        /// </summary>
        /// <param name="deviceIds">selected device(s) Id(s)</param>
        /// <param name="id">companyId</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiAssignCompany)]
        //[Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AssignDoor([FromBody]List<int> deviceIds, int id)
        {
            if (!deviceIds.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _companyService.AssignToCompany(deviceIds, id);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageAssignSuccess, DeviceResource.lblDevice));
        }

        /// <summary>
        /// Edit company information
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiCompaniesId)]
        public IActionResult Edit(int id, [FromBody]CompanyModel model)
        {
            model.Id = id;
            ModelState.Clear();

            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            var company = _companyService.GetById(id);
            if (company == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _companyService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, CompanyResource.lblCompany, model.Name));
        }

        /// <summary>
        /// Delete a company
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiCompaniesId)]
        public IActionResult Delete(int id)
        {
            var company = _companyService.GetById(id);
            if (company == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _companyService.Delete(company);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, CompanyResource.lblCompany));
        }

        /// <summary>
        /// Delete companies
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiCompanies)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var companies = _companyService.GetByIds(ids);
            if (!companies.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var rootCompanies = companies.Where(m => m.RootFlag).ToList();
            var delCompanies = companies.Except(rootCompanies).ToList();

            if (delCompanies.Any())
            {
                _companyService.DeleteRange(delCompanies);
            }
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, CompanyResource.lblCompany));
        }
        
        /// <summary>
        /// Reset key companies
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.ApiRegenerateCompaniesKey)]
        //[Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult RegenerateAesKey()
        {
            _companyService.ResetAesKeyCompanies();
            return Ok();
        }
        
        
        /// <summary>
        /// Reset key company
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.ApiRegenerateCompanyKey)]
        //[Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult RegenerateAesKeyCompany(int id)
        {
            _companyService.ResetAesKeyCompany(id);
            return Ok();
        }

        ///// <summary>
        ///// Get Logo image from Company
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[AllowAnonymous]
        //[Route(Constants.Route.ApiCompaniesLogo)]
        //public IActionResult Logo(int id = 0)
        //{
        //    var logoDefault = "avatar.png";
        //    var companyId = id;
        //    if (id == 0)
        //    {
        //        companyId = _httpContext.User.GetCompanyId();
        //        logoDefault = "logo.png";
        //    }
        //    var company = _companyService.GetById(companyId);
        //    if (company?.Logo != null)
        //    {
        //        return File(company.Logo, "image/png");
        //    }

        //    var webRootPath = _hostingEnvironment.WebRootPath;
        //    var file = System.IO.Path.Combine(webRootPath, "images", logoDefault);
        //    var byteData = System.IO.File.ReadAllBytes(file);
        //    return File(byteData, "image/png");
        //}

        ///// <summary>
        ///// Get mini logo image from Company
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[AllowAnonymous]
        //[Route(Constants.Route.ApiCompaniesMiniLogo)]
        //public IActionResult MiniLogo(int id = 0)
        //{
        //    var miniLogoDefault = "avatar.png";
        //    var companyId = id;
        //    if (id == 0)
        //    {
        //        companyId = _httpContext.User.GetCompanyId();
        //        miniLogoDefault = "logo-xs.png";
        //    }
        //    var company = _companyService.GetById(companyId);
        //    if (company?.MiniLogo != null)
        //    {
        //        return File(company.MiniLogo, "image/png");
        //    }

        //    var webRootPath = _hostingEnvironment.WebRootPath;
        //    var file = System.IO.Path.Combine(webRootPath, "images", miniLogoDefault);
        //    var byteData = System.IO.File.ReadAllBytes(file);
        //    return File(byteData, "image/png");
        //}

        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Route(Constants.Route.ApiCompanies)]
        //public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        //    string sortDirection = "desc")
        //{
        //    var companies = _companyService
        //        .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
        //            out var recordsFiltered).AsEnumerable().Select(m =>
        //        {
        //            m.ExpiredTo = m.ExpiredDate.ToSettingDateString();
        //            m.CreatedOn = m.CreatedDate.ToSettingDateString();
        //            m.Id = m.RootFlag ? 0 : m.Id;
        //            return m;
        //        }).ToList();

        //    var pagingData = new PagingData<CompanyListModel>
        //    {
        //        Data = companies,
        //        Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
        //    };
        //    return Ok(pagingData);
        //}
        
        /// <summary>
        /// Get plug-ins
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(Constants.Route.ApiGetPlugByCompany)]
        //[Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetPluginByCompany(int id)
        {
            var companyPlugIn = _companyService.GetPluginByCompany(id);
            if (companyPlugIn != null)
            {
                return Ok(companyPlugIn);
            }

            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
        
        /// <summary>
        /// Update plug-ins
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.ApiGetPlugByCompany)]
        //[Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult UpdatePluginByCompany(int id, [FromBody] PlugIns model)
        {
            var plugIn = _companyService.GetPluginByCompany(id);
            if (plugIn != null)
            {
                var plugInId = _companyService.UpdatePluginByCompany(id, model);
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageEnabledSuccess, UserResource.lblPlugIn, plugInId));
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
    }
}
