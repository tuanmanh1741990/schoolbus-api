﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.MealSetting;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeMasterProCloud.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealSettingController : ControllerBase
    {
        private readonly IMealSettingService _mealSettingService;

        /// <summary>
        /// Device controller
        /// </summary>
        /// <param name="mealSettingService"></param>
        public MealSettingController(IMealSettingService mealSettingService)
        {
            _mealSettingService = mealSettingService;

        }



        /// <summary>
        /// Return Apiresult add new mealSetting
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiMealSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult AddMealSetting([FromBody]AddMealSettingModel model)
        {
            var mealSetting = _mealSettingService.AddMealSetting(model);
            if (mealSetting == null || mealSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            else if (mealSetting.message == null && mealSetting.statusCode == true)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.lblMealSetting));
            }

            else if (mealSetting.message == "" && mealSetting.statusCode == true)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.msgRangeTimeExists));
            }

            return new ApiSuccessResult(StatusCodes.Status201Created,
               string.Format(MessageResource.MessageAddSuccess, CanteenResource.lblMealSetting), mealSetting.data.ToString());
        }


        /// <summary>
        /// Return Apiresult update mealSetting
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateMealSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult UpdateMealSetting(int id, [FromBody]AddMealSettingModel model)
        {
            if (id == null || id == 0)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }
            var mealSetting = _mealSettingService.UpdateMealSetting(id, model);

            if (mealSetting == null && mealSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            else if (mealSetting.message == "" && mealSetting.statusCode == true)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.msgRangeTimeExists));
            }


            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageUpdateSuccess, "", CanteenResource.lblMealSetting));
        }

        [HttpDelete]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateMealSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult DeleteMealSetting(int id)
        {
            var mealSetting = _mealSettingService.DeleteMealSetting(id);
            if (mealSetting == null || mealSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageDeleteSuccess, CanteenResource.lblMealSetting));
        }

        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Route(Constants.Route.ApiMealSetting)]
        //[Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        //public IActionResult GetListMealSetting(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        //    string sortDirection = "desc")
        //{
        //    var listMealSetting = _mealSettingService.GetListMealSetting(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
        //        out var recordsFiltered);

        //    var pagingData = new PagingData<MealSettingModel>
        //    {
        //        Data = listMealSetting,

        //        Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
        //    };
        //    return Ok(pagingData);
        //}

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiMealSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetListMealSetting(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
       string sortDirection = "desc")
        {
            var listMealSetting = _mealSettingService.ListMealSetting(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered);

            var pagingData = new PagingData<ListMealSettingModel>
            {
                Data = listMealSetting,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateMealSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetMealSetting(int id)
        {
            var mealSetting = _mealSettingService.GetMealSetting(id);


            return Ok(mealSetting);
        }
    }
}