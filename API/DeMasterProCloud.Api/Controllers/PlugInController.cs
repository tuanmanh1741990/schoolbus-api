using System.Collections.Generic;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.PlugIn;
using Newtonsoft.Json;


namespace DeMasterProCloud.Api.Controllers
{
    [Produces("application/ms-excel", "application/json", "application/text")]
    [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PluginController : Controller
    {
        private readonly IPluginService _pluginService;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly ICompanyService _companyService;
        
        
        /// <summary>
        /// Working type controller
        /// </summary>
        /// <param name="workingService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="configuration"></param>
        public PluginController(IPluginService pluginService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, ICompanyService companyService)
        {
            _pluginService = pluginService;
            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
            _companyService = companyService;
        }
        
        /// <summary>
        /// Get Solution(s)
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiGetPlugInCompany)]
        public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var solutions = _pluginService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<PlugInModel>).ToList();

            var pagingData = new PagingData<PlugInModel>
            {
                Data = solutions,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }
        
        
        [HttpPut]
        [Route(Constants.Route.ApiUpdatePlugInCompany)]
        public IActionResult Update(int id, [FromBody] PlugIns model)
        {
            var plugIn = _pluginService.GetPluginCompany(_httpContext.User.GetCompanyId(), id);
            if (plugIn != null)
            {
                var plugInId = _pluginService.Update(id, model);
                return new ApiSuccessResult(StatusCodes.Status200OK,
                    string.Format(MessageResource.MessageEnabledSuccess, UserResource.lblPlugIn, plugInId));
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }

    }
}