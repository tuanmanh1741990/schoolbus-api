﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Performance controller
    /// </summary>
    [Produces("application/json")]
    public class PerformanceController : Controller
    {
        private readonly Random _random = new Random();
        private readonly string _sendUserToIcu =
            $"SendUserToIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _parseEventFromIcu =
            $"ParseEventFromIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _getEventLog =
            $"GetEventLog_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _getUser =
            $"GetUser_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string _saveUserToIcu =
            $"SaveUserToIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";

        private readonly string[] _headers = {
            "Icu Device",
            "Number of event",
            "Time pushing event from ICU",
            "Time receiving event from MQ",
            "Time finish saving data into DB",
            "Time publish event log JSON to MQ",
            "Time message travel in queue",
            "Time parsing data",
            "Time checking if ICU is exist",
            "Time prepare event log object",
            "Time Save Event log to DB",
            "Time close DB connection",
            "Total time until finish saving data to DB",
            "Total time until publish event JSON to MQ",
        };

        private readonly IConfiguration _configuration;
        private readonly IDeviceService _deviceService;
        private readonly IUserService _userService;
        private readonly IQueueService _queueService;

        /// <summary>
        /// Schedule controller
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="deviceService"></param>
        /// <param name="userService"></param>
        /// <param name="queueService"></param>
        public PerformanceController(IConfiguration configuration, IDeviceService deviceService,
            IUserService userService, IQueueService queueService)
        {
            _configuration = configuration;
            _deviceService = deviceService;
            _userService = userService;
            _queueService = queueService;
        }

        /// <summary>
        /// Get performance infor of the case [send user to icu]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("performance/send-user-to-icu")]
        public IActionResult SendUserToIcu()
        {
            var content = System.IO.File.ReadAllBytes(_sendUserToIcu);
            var result =
                new FileContentResult(content, "application/octet-stream")
                {
                    FileDownloadName = _sendUserToIcu
                };
            return result;
        }

        /// <summary>
        /// Get performance infor of the case [parse event from icu]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("performance/parse-event-from-icu")]
        public IActionResult ParseEvent()
        {
            //var content = GetContents(Environment.CurrentDirectory);
            var content = System.IO.File.ReadAllBytes(_parseEventFromIcu);
            var result =
                new FileContentResult(content, "application/octet-stream")
                {
                    FileDownloadName = _parseEventFromIcu
                };
            return result;
        }

        /// <summary>
        /// Get performance infor of the case [get event]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("performance/get-event_log")]
        public IActionResult GetEventLog()
        {
            
            var content = System.IO.File.ReadAllBytes(_getEventLog);
            var result =
                new FileContentResult(content, "application/octet-stream")
                {
                    FileDownloadName = _getEventLog
                };
            return result;
            
        }

        /// <summary>
        /// Get performance infor of the case [get user]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("performance/get-user")]
        public IActionResult GetUser()
        {
            var content = System.IO.File.ReadAllBytes(_getUser);
            var result =
                new FileContentResult(content, "application/octet-stream")
                {
                    FileDownloadName = _getUser
                };
            return result;
        }

        /// <summary>
        /// Get performance infor of the case [get user]
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("performance/save-user-to-icu")]
        public IActionResult SaveUserToIcu()
        {
           
            var content = System.IO.File.ReadAllBytes(_saveUserToIcu);
            var result =
                new FileContentResult(content, "application/octet-stream")
                {
                    FileDownloadName = _saveUserToIcu
                };
            return result;
        }

        /// <summary>
        /// Get contents
        /// </summary>
        /// <param name="inputDirectoryPath"></param>
        public byte[] GetContents(string inputDirectoryPath)
        {
            var reg = new Regex(@"ParseEventFromIcu_([0-9A-F]{6})_(\d{8})\.csv$");
            var inputFilePaths = Directory.GetFiles(inputDirectoryPath, "*.csv")
                .Where(path => reg.IsMatch(path))
                .ToList();
            if (!inputFilePaths.Any())
            {
                return null;
            }
            var ms = new MemoryStream();
            var stream = new StreamWriter(ms);
            var csvBuilder = new StringBuilder();
            csvBuilder.AppendLine(string.Join(",", _headers));
            foreach (var inputFilePath in inputFilePaths)
            {
                var lines = System.IO.File.ReadAllLines(inputFilePath);
                foreach (var line in lines)
                {
                    csvBuilder.AppendLine(line);
                }
            }
            stream.Write(csvBuilder.ToString());
            stream.Flush();
            return ms.ToArray();
        }

        /// <summary>
        /// Test mqtt
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route("performance/test-mqtt")]
        public IActionResult TestMqttCentos(int numberOfConcurrentIcu, int numberOfEventSentByEachIcu, int timeDelayBetweenEachMessage)
        {
            
            var currentDomain = "http://0.0.0.0:5000";
            var rabbitMqHost = _configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsHost);
            var args =
                $"{numberOfConcurrentIcu} {numberOfEventSentByEachIcu} {timeDelayBetweenEachMessage} {currentDomain} {rabbitMqHost}";
            var result = Run("icu_mqtt_simulator.py", args);

            return Ok($"Finish test mqtt: {result}");
            
        }

        /// <summary>
        /// Run the performance controller
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public string Run(string cmd, string args)
        {
            var start = new ProcessStartInfo
            {
                FileName = "python",
                Arguments = $"{cmd} {args}",
                UseShellExecute = false,
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };
            // Do not use OS shell
            // We don"t need new window
            // Any output, generated by application will be redirected back
            // Any error in standard output will be redirected back (for example exceptions)
            using (var process = Process.Start(start))
            {
                process?.WaitForExit();
                using (var reader = process?.StandardOutput)
                {
                    var result = reader?.ReadToEnd(); // Here is the result of StdOut(for example: print "test")
                    return result;
                }
            }
        }

        ///// <summary>
        ///// Test mqtt
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Route("performance/test-mqtt-win")]
        //public async Task<IActionResult> TestMqttWin(int numberOfConcurrentIcu, int numberOfEventSentByEachIcu,
        //    int timeDelayBetweenEachMessage)
        //{
        //    try
        //    {
        //        if (numberOfConcurrentIcu > 1000)
        //        {
        //            return Ok("Unable to create number of concurrent exceed 1000!");
        //        }

        //        var excludeIcuDevices = new List<string> { "002FBE", "087A23", "01E208", "01E256" };
        //        var devices = _deviceService.GetPaginated(string.Empty, 1, 1000, 0, "desc", out _,
        //            out _).Where(c => !excludeIcuDevices.Contains(c.DeviceAddress)).ToList();

        //        if (numberOfConcurrentIcu > devices.Count)
        //        {
        //            return Ok($"Unable to create number of concurrent exceed number of icu is {devices.Count}!");
        //        }

        //        var users = _userService.GetPaginated(string.Empty, 1, 1000, 0, "desc", out _,
        //            out _).Where(c => !string.IsNullOrEmpty(c.CardId)).ToList();
        //        var userCardIds = users.Select(c => c.CardId).ToList();
        //        var tasks = new List<Task>();
        //        for (var i = 0; i < numberOfConcurrentIcu; i++)
        //        {
        //            var icuAddress = devices[i].DeviceAddress;
        //            tasks.Add(Task.Run(() =>
        //                SendEventLog(icuAddress, userCardIds, timeDelayBetweenEachMessage)));
        //        }
        //        await Task.WhenAll(tasks);
        //        return Ok("Finish test mqtt");
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        return Ok($"Test mqtt failed: {e.Message}");
        //    }
        //}

        /// <summary>
        /// Send EventLog
        /// </summary>
        /// <param name="icuAddress"></param>
        /// <param name="userCardIds"></param>
        /// <param name="timeDelayBetweenEachMessage"></param>
        public void SendEventLog(string icuAddress, List<string> userCardIds, int timeDelayBetweenEachMessage)
        {
            var index = _random.Next(userCardIds.Count - 1);
            var receiveEventLogHeaderData = new ReceiveEventLogHeaderData
            {
                Total = 1,
                Events = new List<ReceiveEventLogDetailData>
                {
                    new ReceiveEventLogDetailData
                    {
                        DeviceAddress = icuAddress,
                        AccessTime = DateTime.Now.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmssFfffff),
                        CardId = userCardIds[index],
                        IssueCount = 1,
                        UserName = "guest",
                        UpdateTime = DateTime.Now.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmssFfffff),
                        InOut = "In",
                        EventType = 1
                    }
                }
            };

            var receiveEventLogProtocolData = new ReceiveEventLogProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.EventLogWebApp,
                Data = receiveEventLogHeaderData
            };

            var routingKey = $"{Constants.RabbitMq.EventLogTopic}.{icuAddress}";
            var message = JsonConvert.SerializeObject(receiveEventLogProtocolData,
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            _queueService.Publish(routingKey, message);
            Thread.Sleep(timeDelayBetweenEachMessage);
        }
    }
}
