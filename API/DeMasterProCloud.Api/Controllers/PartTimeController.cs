﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Building;
using DeMasterProCloud.DataModel.PartTime;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// PartTime controller
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PartTimeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IPartTimeService _partTimeService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Building controller
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="partTimeService"></param>
        /// <param name="httpContextAccessor"></param>
        public PartTimeController(IConfiguration configuration, IPartTimeService partTimeService,
            IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _partTimeService = partTimeService;
            _httpContext = httpContextAccessor.HttpContext;
        }


        /// POST /buildings/creation : Create new building
        /// <summary>
        /// Add building
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiPartTimes)]
        public IActionResult Add([FromBody]PartTimeModel model)
        {
            //if (!ModelState.IsValid)
            //{
            //    return new ValidationFailedResult(ModelState);
            //}

            _partTimeService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
               string.Format(MessageResource.MessageAddSuccess, "PartTime"));
        }

        /////// GET /buildings : return all existing buildings with paging and sorting
        /////// <summary>
        /////// Return Json object for all Building list
        /////// </summary>
        /////// <param name="search"></param>
        /////// <param name="pageNumber"></param>
        /////// <param name="pageSize"></param>
        /////// <param name="sortColumn"></param>
        /////// <param name="sortDirection"></param>
        /////// <returns></returns>
        ////[HttpGet]
        ////[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        ////[Route(Constants.Route.ApiBuildings)]
        ////public IActionResult Gets(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        ////    string sortDirection = "desc")
        ////{
        ////    var buildings = _buildingService.GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
        ////            out var recordsFiltered).ToList();

        ////    var pagingData = new PagingData<BuildingListModel>
        ////    {
        ////        Data = buildings,
        ////        Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
        ////    };
        ////    return Ok(pagingData);
        ////}

        /////// GET /buildings : return all existing buildings with paging and sorting
        /////// <summary>
        /////// Return Json object for all Building list
        /////// </summary>
        /////// <param name="id"></param>
        /////// <returns></returns>
        ////[HttpGet]
        ////[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        ////[Route(Constants.Route.ApiBuildingsId)]
        ////public IActionResult Get(int id)
        ////{
        ////    var building = _buildingService.GetById(id);
        ////    if (building == null)
        ////    {
        ////        return new ApiErrorResult(StatusCodes.Status404NotFound);
        ////    }
        ////    var model = Mapper.Map<BuildingListModel>(building);
        ////    return Ok(model);
        ////}


        ///// GET /buildings/{id} : return all existing doors of a building with paging and sorting
        ///// <summary>
        ///// Return Json object for all door list in a building
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="search"></param>
        ///// <param name="pageNumber"></param>
        ///// <param name="pageSize"></param>
        ///// <param name="sortColumn"></param>
        ///// <param name="sortDirection"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Route(Constants.Route.ApiBuildingsDoorList)]
        //public IActionResult GetDoors(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        //    string sortDirection = "desc")
        //{
        //    var doors = _buildingService.GetPaginatedDoors(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
        //            out var recordsFiltered).ToList();

        //    var pagingData = new PagingData<BuildingDoorModel>
        //    {
        //        Data = doors,
        //        Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
        //    };
        //    return Ok(pagingData);
        //}

        ///// GET /buildings/{id} : return all existing doors of a building with paging and sorting
        ///// <summary>
        ///// Return Json object for all door list in a building
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="search"></param>
        ///// <param name="pageNumber"></param>
        ///// <param name="pageSize"></param>
        ///// <param name="sortColumn"></param>
        ///// <param name="sortDirection"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //[Route(Constants.Route.ApiBuildingsUnAssignDoors)]
        //public IActionResult GetUnAssignDoors(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
        //    string sortDirection = "desc")
        //{
        //    var doors = _buildingService.GetPaginatedUnAssignDoors(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
        //        out var recordsFiltered).ToList();

        //    var pagingData = new PagingData<BuildingUnAssignDoorModel>
        //    {
        //        Data = doors,
        //        Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
        //    };
        //    return Ok(pagingData);
        //}

        

        ///// POST /buildings/door-creation/{id} : Create new door
        ///// <summary>
        ///// Add a door to a building
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="doorIds"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[Route(Constants.Route.ApiBuildingsAssignDoors)]
        //public IActionResult AddDoors(int id, [FromBody]List<int> doorIds)
        //{
        //    if (id == 0 || doorIds == null || !doorIds.Any())
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest);
        //    }
        //    _buildingService.AddDoors(id, doorIds);
        //    return new ApiSuccessResult(StatusCodes.Status201Created, BuildingResource.msgAssignDoors);

        //}


        ///// PUT /buildings/{id}/edit-building : Edit a building's name
        ///// <summary>
        ///// Edit name of a building
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="model"></param>
        ///// <returns></returns>
        //[HttpPut]
        //[Route(Constants.Route.ApiBuildingsId)]
        //public IActionResult Edit(int id, [FromBody]BuildingModel model)
        //{
        //    if (id == 0 || model == null)
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest);
        //    }

        //    model.Id = id;
        //    ModelState.Clear();
        //    if (!ModelState.IsValid)
        //    {
        //        return new ValidationFailedResult(ModelState);
        //    }
        //    _buildingService.Update(id, model);
        //    return new ApiSuccessResult(StatusCodes.Status200OK,
        //            string.Format(MessageResource.MessageUpdateSuccess, BuildingResource.lblBuilding));
        //}


        ///// DELETE /buildings/{id}/delete : delete a building
        ///// <summary>
        ///// Delete a building
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //[Route(Constants.Route.ApiBuildingsId)]
        //public IActionResult Delete(int id)
        //{
        //    if (id == 0)
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest);
        //    }
        //    if (id == Constants.DefaultBuildingId)//default building - Head Quarter
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest, BuildingResource.msgCannotDeleteDefaultBuilding);
        //    }
        //    _buildingService.Delete(id);
        //    return new ApiSuccessResult(StatusCodes.Status200OK,
        //            string.Format(MessageResource.MessageDeleteSuccess, BuildingResource.lblBuilding));


        //}

        ///// <summary>
        ///// Delete list access group
        ///// </summary>
        ///// <param name="ids"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //[Route(Constants.Route.ApiBuildings)]
        //public IActionResult DeleteMultiple(List<int> ids)
        //{
        //    var buildings = _buildingService.GetByIds(ids);
        //    if (!buildings.Any())
        //    {
        //        return new ApiErrorResult(StatusCodes.Status404NotFound);
        //    }

        //    if (ids.Contains(Constants.DefaultBuildingId))
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest, BuildingResource.msgCannotDeleteDefaultBuilding);
        //    }

        //    _buildingService.DeleteRange(buildings);
        //    return new ApiSuccessResult(StatusCodes.Status200OK,
        //        string.Format(MessageResource.MessageDeleteMultipleSuccess, BuildingResource.lblBuilding));
        //}


        ///// DELETE /buildings/{id}/delete : delete a building
        ///// <summary>
        ///// Delete a building
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="doorIds"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //[Route(Constants.Route.ApiBuildingsUnAssignDoors)]
        //public IActionResult DeleteDoors(int id, [FromBody]List<int> doorIds)
        //{
        //    if (id == 0 || doorIds == null || !doorIds.Any())
        //    {
        //        return new ApiErrorResult(StatusCodes.Status400BadRequest);
        //    }
        //    _buildingService.DeleteDoors(id, doorIds);
        //    //return new ApiSuccessResult(StatusCodes.Status200OK, BuildingResource.msgUnAssignDoors);
        //    return new ApiSuccessResult(StatusCodes.Status200OK,
        //        string.Format(MessageResource.MessageDeleteSuccess, DeviceResource.lblDevice));

        //}
    }
}