﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.CornerSetting;
using DeMasterProCloud.DataModel.UserDiscount;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// CornerSetting controller
    /// </summary>
    [Produces("application/json")]
    public class UserDiscountController : Controller
    {
        private readonly IUserDiscountService _userDiscountService;

        /// <summary>
        /// Device controller
        /// </summary>
        /// <param name="userDiscountService"></param>
        public UserDiscountController(IUserDiscountService userDiscountService)
        {
            _userDiscountService = userDiscountService;

        }



        /// <summary>
        /// Return Apiresult add new userDiscount
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiUserDiscount)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Add([FromBody]AddUserDiscountModel model)
        {
            var userDiscount = _userDiscountService.AddUserDiscount(model);
            if (userDiscount == null || userDiscount.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            if (userDiscount.message == null && userDiscount.statusCode == true)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.lblUserDiscount));
            }

            return new ApiSuccessResult(StatusCodes.Status201Created,
               string.Format(MessageResource.MessageAddSuccess, CanteenResource.lblUserDiscount), userDiscount.data.ToString());
        }


        /// <summary>
        /// Return Apiresult update userDiscount
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateUserDiscount)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Update(int id, [FromBody]AddUserDiscountModel model)
        {
            if (id == null || id == 0)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }
            var cornerSetting = _userDiscountService.UpdateUserDiscount(id ,model);
            if (cornerSetting == null || cornerSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageUpdateSuccess, "", CanteenResource.lblUserDiscount));
        }

        [HttpDelete]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateUserDiscount)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Delete(int id)
        {
            var cornerSetting = _userDiscountService.DeleteUserDiscountg(id);
            if (cornerSetting == null || cornerSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageDeleteSuccess, CanteenResource.lblUserDiscount));
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUserDiscount)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetListUserDiscount(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var listUserDiscount = _userDiscountService.GetListUserDiscount(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered);
            var pagingData = new PagingData<UserDiscountModel>
            {
                Data = listUserDiscount,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            return Ok(pagingData);
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateUserDiscount)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetUserDiscount(int id)
        {
            var userDiscount = _userDiscountService.GetUserDiscount(id);

            return Ok(userDiscount);
        }
    }
}