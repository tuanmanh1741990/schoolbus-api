﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.CornerSetting;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// CornerSetting controller
    /// </summary>
    [Produces("application/json")]
    public class CornerSettingController : Controller
    {
        private readonly ICornerSettingService _cornerSettingService;

        /// <summary>
        /// Device controller
        /// </summary>
        /// <param name="cornerSettingService"></param>
        public CornerSettingController(ICornerSettingService cornerSettingService)
        {
            _cornerSettingService = cornerSettingService;

        }



        /// <summary>
        /// Return Apiresult add new cornerSetting
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiCornerSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult Add([FromBody]AddCornerSettingModel model)
        {
            var cornerSetting = _cornerSettingService.AddCornerSetting(model);
            if (cornerSetting == null || cornerSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (cornerSetting.message == null && cornerSetting.statusCode == true)
            {
                //  return new ApiSuccessResult(StatusCodes.Status201Created,
                //string.Format(MessageResource.Exist, CanteenResource.lblCornerSetting));
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.lblCornerSetting));
            }

            return new ApiSuccessResult(StatusCodes.Status201Created,
               string.Format(MessageResource.MessageAddSuccess, CanteenResource.lblCornerSetting),cornerSetting.data.ToString());
        }


        /// <summary>
        /// Return Apiresult add new cornerSetting
        /// </summary>
        /// <param name="model">What user is searching for</param>
        /// <returns></returns>
        [HttpPut]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateCornerSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult UpdateCornerSetting(int id, [FromBody]AddCornerSettingModel model)
        {
            if (id == null || id == 0)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }
            var cornerSetting = _cornerSettingService.UpdateCornerSetting(id, model);
            if (cornerSetting == null || cornerSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (cornerSetting.message == null && cornerSetting.statusCode == true)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, CanteenResource.lblCornerSetting));
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageUpdateSuccess, "", CanteenResource.lblCornerSetting));
        }

        [HttpDelete]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateCornerSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult DeleteCornerSetting(int id)
        {
            var cornerSetting = _cornerSettingService.DeleteCornerSetting(id);
            if (cornerSetting == null || cornerSetting.statusCode == false)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            return new ApiSuccessResult(StatusCodes.Status200OK,
               string.Format(MessageResource.MessageDeleteSuccess, CanteenResource.lblCornerSetting));
        }

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiCornerSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetListCornerSetting(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var listCornerSetting = _cornerSettingService.GetListCornerSetting(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered);

            var pagingData = new PagingData<CornerSettingModel>
            {
                Data = listCornerSetting,

                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUpdateCornerSetting)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [CheckAddOnAttribute(Constants.PlugIn.CanteenManagement)]
        public IActionResult GetCornerSetting(int id)
        {
            var cornerSetting = _cornerSettingService.GetCornerSetting(id);
            return Ok(cornerSetting);
        }
    }
}