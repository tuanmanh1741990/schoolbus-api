using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.WorkingModel;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;


namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Working type controller
    /// </summary>
    [Produces("application/ms-excel", "application/json", "application/text")]
    [CheckAddOnAttribute(Constants.PlugIn.TimeAttendance)]
    public class WorkingTypeController : Controller 
    {
        private readonly IWorkingService _workingService;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly IAttendanceService _attendanceService;
        private readonly IUserService _userService;
        
        
        /// <summary>
        /// Working type controller
        /// </summary>
        /// <param name="workingService"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="configuration"></param>
        public WorkingTypeController(IWorkingService workingService, IHttpContextAccessor httpContextAccessor, IConfiguration configuration, IAttendanceService attendanceService, IUserService userService)
        {
            _workingService = workingService;
            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
            _attendanceService = attendanceService;
            _userService = userService;
        }
        
        /// <summary>
        /// Get WorkingType(s)
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiGetWorkingTypeCompany)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var workingTypes = _workingService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<WorkingListModel>).ToList();

            var pagingData = new PagingData<WorkingListModel>
            {
                Data = workingTypes,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }
        
        /// <summary>
        /// Get Working Type
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiGetWorkingTypeCompanyDetail)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetDetail(int id)
        {
            var workingType = _workingService.GetWorkingType(id, _httpContext.User.GetCompanyId());
            return Ok(workingType);
        }
        
        /// <summary>
        /// Add new a working type
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiAddWorkingTypeCompany)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody] WorkingModel model)
        {
            var listWorking = JsonConvert.DeserializeObject<List<WorkingTime>>(model.WorkingDay);
            var listDay = new List<string>();
            foreach (var s in listWorking)
            {
                // Try validate Working Type
                bool validType = Helpers.TryValidateWorkingType(s.Type);
                if (validType != true)
                {
                    return new ValidationFailedResult(ModelState);
                }
                
                // Try validate Working Time
                bool validTime = Helpers.TryValidateWorkingTime(s.Start, s.End);
                if (validTime != true)
                {
                    return new ValidationFailedResult(ModelState);
                }
                
                // Try validate Working Day
                bool validDay = Helpers.TryValidateWorkingDay(s.Name);
                if (validDay != true)
                {
                    return new ValidationFailedResult(ModelState);
                }

                listDay.Add(s.Name);
            }
            
            // Try validate Working Day Distinct
            bool validDistinctDay = Helpers.TryValidateWorkingDayDistinct(listDay);
            if (validDistinctDay != true)
            {
                return new ValidationFailedResult(ModelState);
            }
            
            // Try validate Working Day Existed
            var existed = _workingService.CheckWorkingTypeExisted(model.Name);
            if(existed != null)
            {
                return new ValidationFailedResult(ModelState);
            }
            
            var working = _workingService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageAddSuccess, UserResource.lblWorkingType, ""), working.ToString());
        }

        /// <summary>
        ///     updates working type. 
        /// </summary>
        /// <remarks>   Edward, 2020-03-11. </remarks>
        /// <param name="id">       The identifier of working type. </param>
        /// <param name="model">    workingTime model. </param>
        /// <returns>   An IActionResult. </returns>
        [HttpPut]
        [Route(Constants.Route.ApiUpdateWorkingTypeCompany)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Update(int id, [FromBody] WorkingModel model)
        {
            var listWorking = JsonConvert.DeserializeObject<List<WorkingTime>>(model.WorkingDay);
            var listDay = new List<string>();
            var workingType = _workingService.GetWorkingType(id, _httpContext.User.GetCompanyId());
            
            // Check working Type already existed
            if (workingType == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            foreach (var s in listWorking)
            {
                // Try validate Working Type
                bool validType = Helpers.TryValidateWorkingType(s.Type);
                if (validType != true)
                {
                    return new ValidationFailedResult(ModelState);
                }

                // Try validate Working Time
                bool validTime = Helpers.TryValidateWorkingTime(s.Start, s.End);
                if (validTime != true)
                {
                    return new ValidationFailedResult(ModelState);
                }

                // Try validate Working Day
                bool validDay = Helpers.TryValidateWorkingDay(s.Name);
                if (validDay != true)
                {
                    return new ValidationFailedResult(ModelState);
                }

                listDay.Add(s.Name);
            }

            // Try validate Working Day Distinct
            bool validDistinctDay = Helpers.TryValidateWorkingDayDistinct(listDay);
            if (validDistinctDay != true)
            {
                return new ValidationFailedResult(ModelState);
            }

            // Try validate Working Day Existed
            //var existed = _workingService.CheckWorkingTypeExisted(model.Name);
            //if(existed.Id != id)
            //{
            //    return new ValidationFailedResult(ModelState);
            //}
            var existed = _workingService.CheckNameWorkingTime(model.Name, id);
            if (!existed)
            {
                return new ValidationFailedResult(ModelState);
            }

            _workingService.Update(id, model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblWorkingType, ""));

        }
        
        /// <summary>
        /// Deleted Working Type
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiGetWorkingTypeCompanyDetail)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeletedWorkingType(int id)
        {
            var workingType = _workingService.GetWorkingType(id, _httpContext.User.GetCompanyId());
            
            if (workingType != null)
            {
                if (workingType.IsDefault)
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity , UserResource.DeleteDefaultWorkingType);
                }
                var users = _workingService.GetUserUsingWorkingType(id, _httpContext.User.GetCompanyId());
                if (users.Any())
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity , UserResource.DeleteWorkingTypeUserWorkingOn);
                }
                _workingService.Delete(workingType);
                
            }
            else
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, UserResource.lblWorkingType));
            
        }
        
        /// <summary>
        /// Assign Multiple User to Defalut working time
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [Route(Constants.Route.AssignMultipleUsersToWorkingTime)]
        [Authorize(Policy = Constants.Policy.PrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AssignMultipleUserToDefaultWorkingTime(int id, string listUser)
        {
            if (!string.IsNullOrEmpty(listUser))
            {
                String[] strListId = listUser.Split(",");
                if (strListId.Any())
                {
                    foreach (var userId in strListId)
                    {
                        var user = _userService.GetByIdAndCompany(Int32.Parse(userId), _httpContext.User.GetCompanyId());
                        if (user == null)
                        {
                            return new ApiErrorResult(StatusCodes.Status404NotFound,
                                string.Format(MessageResource.UserIsNotValidation, userId));
                        }
                    }
                }
                else
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity);
                }
                
                var workingType = _workingService.GetWorkingType(id, _httpContext.User.GetCompanyId());
                if (workingType != null)
                {
                    _workingService.AssignMultipleUserToWorkingTime(id, listUser);
                    return new ApiSuccessResult(StatusCodes.Status200OK,
                        string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblUser, listUser));
                }
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity);
        }
    }
}