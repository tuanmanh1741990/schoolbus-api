﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Setting controller
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SettingController : Controller
    {
        private readonly ISettingService _settingService;
        private readonly IConfiguration _configuration;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Ctor of setting
        /// </summary>
        /// <param name="settingService"></param>
        /// <param name="configuration"></param>
        /// <param name="httpContextAccessor"></param>
        public SettingController(ISettingService settingService, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _settingService = settingService;
            _configuration = configuration;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// <summary>
        /// Get the setting
        /// </summary>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSettings)]
        public IActionResult Get()
        {
            var settings = _settingService.GetAll(_httpContext.User.GetCompanyId()).GroupBy(x => x.Category)
                .Select(x => new SettingByCategoryModel
                {
                    Category = x.Key,
                    Settings = x.Select(m => m).ToList()
                })
                .ToList();
            return Ok(settings);
        }

        /// <summary>   Get the setting. </summary>
        ///
        /// <remarks>   Edward, 2020-01-29. </remarks>
        ///
        /// <param name="id">   The identifier. </param>
        ///
        /// <returns>   An IActionResult. </returns>

        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiSettingsId)]
        public IActionResult Get(int id)
        {
            var setting = _settingService.GetById(id);
            if (setting == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            var model = Mapper.Map<SettingEditModel>(setting);
            return Ok(model);
        }

        /// <summary>   (An Action that handles HTTP PUT requests) edits. </summary>
        ///
        /// <remarks>   Edward, 2020-01-29. </remarks>
        ///
        /// <param name="id">       The identifier. </param>
        /// <param name="model">    The model. </param>
        ///
        /// <returns>   An IActionResult. </returns>

        [HttpPut]
        [Route(Constants.Route.ApiSettingsId)]
        public IActionResult Edit(int id, [FromBody]SettingModel model)
        {
            var setting = _settingService.GetById(id);
            if (setting == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _settingService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, SettingResource.lblSetting, ""));
        }

        /// <summary>   (An Action that handles HTTP PATCH requests) edit multiple. </summary>
        ///
        /// <remarks>   Edward, 2020-01-29. </remarks>
        ///
        /// <param name="models">   The models. </param>
        ///
        /// <returns>   An IActionResult. </returns>

        [HttpPatch]
        [Route(Constants.Route.ApiSettings)]
        public IActionResult EditMultiple([FromBody]List<SettingModel> models)
        {
            if (models == null || !models.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            _settingService.Update(models);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, SettingResource.lblSetting, ""));
        }
    }
}