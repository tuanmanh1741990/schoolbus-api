﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.AccessGroupDevice;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Access group controller
    /// </summary>
    [Produces("application/json")]
    [CheckAddOnAttribute(Constants.PlugIn.AccessControl)]

    public class AccessGroupController : Controller
    {
        private readonly IAccessGroupService _accessGroupService;
        private readonly HttpContext _httpContext;
        private readonly IVisitService _visitService;

        /// <summary>
        /// Constructor for inject services
        /// </summary>
        /// <param name="accessGroupService"></param>
        /// <param name="contextAccessor"></param>
        public AccessGroupController(IAccessGroupService accessGroupService, IHttpContextAccessor contextAccessor, IVisitService visitService)
        {
            _accessGroupService = accessGroupService;
            _httpContext = contextAccessor.HttpContext;
            _visitService = visitService;
        }

        /// <summary>
        /// List all access group with pagination
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroups)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Gets(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accessGroups = _accessGroupService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<AccessGroupListModel>).ToList();

            var pagingData = new PagingData<AccessGroupListModel>
            {
                Data = accessGroups,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroupsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var accessGroup = _accessGroupService.GetById(id);
            if (accessGroup == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            var model = Mapper.Map<AccessGroupListModel>(accessGroup);
            return Ok(model);
        }

        /// <summary>
        /// Get a access group by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroupsDoors)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetDoors(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accessGroups = _accessGroupService
                .GetPaginatedForDoors(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            var pagingData = new PagingData<AccessGroupDeviceDoor>
            {
                Data = accessGroups,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get a access group by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroupsUnAssignedDoors)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUnAssignDoors(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accessGroups = _accessGroupService
                .GetPaginatedForUnAssignDoors(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            var pagingData = new PagingData<AccessGroupDeviceUnAssignDoor>
            {
                Data = accessGroups,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get list of all door by accessGroupId for visit
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroupsAllDoorsForVisit)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUnAssignDoorsForVisit(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var unassignedDoorsTree = _accessGroupService
                .GetPaginatedAllDoorsForVisit(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            var pagingData = new PagingData<Node>
            {
                Data = unassignedDoorsTree,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get a access group by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroupsUsersList)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUsers(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var users = _accessGroupService
                .GetPaginatedForUsers(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);

            foreach (var user in users)
            {
                user.CardList = _accessGroupService.GetCardListByUserId(user.Id);
                if(user.CardList != null && user.CardList.Any())
                    user.CardId = user.CardList?.FirstOrDefault().CardId;
            }
            


            var pagingData = new PagingData<UserForAccessGroup>
            {
                Data = users,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get a access group by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAccessGroupsUnAssignedUsers)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetUnAssignUsers(int id, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var users = _accessGroupService
                .GetPaginatedForUnAssignUsers(id, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered);


            foreach (var user in users)
            {
                user.CardList = _accessGroupService.GetCardListByUserId(user.Id);
                if (user.CardList != null && user.CardList.Any())
                    user.CardId = user.CardList?.FirstOrDefault().CardId;
            }

            var pagingData = new PagingData<UnAssignUserForAccessGroup>
            {
                Data = users,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Add new access group
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiAccessGroups)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]AccessGroupModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            _accessGroupService.Add(model);
            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, AccessGroupResource.lblAccessGroup));
        }

        /// <summary>
        /// Edit a access group
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiAccessGroupsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]AccessGroupModel model)
        {
            model.Id = id;
            ModelState.Clear();
/*            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }*/

            var accessGroup = _accessGroupService.GetById(id);
            if (accessGroup == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            //if (!model.IsDefault)
            //{
            //    var name = _accessGroupService.GetNameAccessGroupCouldNotUpdateOrDelete(new List<int> { id });
            //    if (!string.IsNullOrEmpty(name))
            //    {
            //        return new ApiErrorResult(StatusCodes.Status400BadRequest,
            //            string.Format(AccessGroupResource.msgCanNotUpdateAccessGroup, name));
            //    }
            //}

            if(accessGroup.Type != (short)AccessGroupType.NormalAccess && !(!accessGroup.IsDefault && model.IsDefault))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                        string.Format(AccessGroupResource.msgCanNotUpdateAccessGroup, accessGroup.Name));
            }

            _accessGroupService.Update(model);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, AccessGroupResource.lblAccessGroup, ""));
        }

        /// <summary>
        /// Delete a access group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiAccessGroupsId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var accessGroupModel = _accessGroupService.GetById(id);
            if (accessGroupModel == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var name = _accessGroupService.GetNameAccessGroupCouldNotUpdateOrDelete(new List<int> { id });
            if (!string.IsNullOrEmpty(name))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(AccessGroupResource.msgCanNotDeleteAccessGroup, name));
            }

            var accessGroup = Mapper.Map<AccessGroup>(accessGroupModel);
            _accessGroupService.Delete(accessGroup);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, AccessGroupResource.lblAccessGroup));
        }

        /// <summary>
        /// Delete list access group
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiAccessGroups)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var accessGroups = _accessGroupService.GetByIds(ids);
            if (!accessGroups.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            var name = _accessGroupService.GetNameAccessGroupCouldNotUpdateOrDelete(ids);
            if (!string.IsNullOrEmpty(name))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest,
                    string.Format(AccessGroupResource.msgCanNotDeleteAccessGroup, name));
            }

            _accessGroupService.DeleteRange(accessGroups);
            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, AccessGroupResource.lblAccessGroup));
        }

        /// <summary>
        ///     Assign users to an access group
        /// </summary>
        /// <remarks>   Edward, 2020-03-12. </remarks>
        /// <param name="id">       Access group identifier. </param>
        /// <param name="userIds">  List of identifiers for the users. </param>
        /// <returns>   An IActionResult. </returns>
        [HttpPost]
        [Route(Constants.Route.ApiAccessGroupsAssignUsers)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AssignUsers(int id, [FromBody]List<int> userIds)
        {
            if (id == 0 || userIds == null || !userIds.Any())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.msgEmptyUser);
            }

            var result = _accessGroupService.AssignUsers(id, userIds);
            if (!string.IsNullOrEmpty(result))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, result);
            }
            return new ApiSuccessResult(StatusCodes.Status200OK, AccessGroupResource.msgAssignUsers);
        }

        /// <summary>
        /// Assign doors to an access group
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiAccessGroupsAssignDoors)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult AssignDoors(int id, [FromBody]AccessGroupAssignDoor model)
        {
            if (id == 0 || model == null || !model.Doors.Any())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }

            var accessGroup = _accessGroupService.GetById(id);
            if (accessGroup.Name.Contains(Constants.Settings.NameAccessGroupVisitor))
            {
                string[] str = accessGroup.Name.Split("-");
                var visit = _visitService.GetById(Int32.Parse(str[1]));
                var lastApproval = _visitService.GetLastApproval(visit.Id);
                if (lastApproval != 0)
                {
                    if (lastApproval != _httpContext.User.GetAccountId())
                    {
                        return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.assingedVisitAuthor);
                    }
                }
                else
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.assingedVisitNotApproval);
                }
            }

            var newModel = new AccessGroupAssignDoor();

            foreach (var door in model.Doors)
            {
                var detailModel = new AccessGroupAssignDoorDetail()
                {
                    DoorId = door.DoorId,
                    TzId = door.TzId,
                    CompanyId = _httpContext.User.GetCompanyId()
                };

                newModel.Doors.Add(detailModel);
            }
            
            var result = _accessGroupService.AssignDoors(id, newModel);
            if (!string.IsNullOrEmpty(result))
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, result);
            }
            return new ApiSuccessResult(StatusCodes.Status200OK, AccessGroupResource.msgAssignDoors);
        }

        /// <summary>
        /// Remove doors from an access group
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiAccessGroupsUnAssignDoors)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult UnAssignDoors(int id, [FromBody]List<int> doorIds)
        {
            if (id == 0 || doorIds == null || !doorIds.Any())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }

            var accessGroup = _accessGroupService.GetById(id);
            if (accessGroup.Name.Contains(Constants.Settings.NameAccessGroupVisitor))
            {
                string[] str = accessGroup.Name.Split("-");
                var visit = _visitService.GetById(Int32.Parse(str[1]));
                var lastApproval = _visitService.GetLastApproval(visit.Id);
                if (lastApproval != 0)
                {
                    if (lastApproval != _httpContext.User.GetAccountId())
                    {
                        return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.assingedVisitAuthor);
                    }
                }
                else
                {
                    return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.assingedVisitNotApproval);
                }
            }
            if (accessGroup != null && accessGroup.Type == (short)AccessGroupType.FullAccess)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.msgCanNotDeleteDevice);
            }

            _accessGroupService.UnAssignDoors(id, doorIds);
            return new ApiSuccessResult(StatusCodes.Status200OK, AccessGroupResource.msgUnAssignDoors);
        }

        /// <summary>
        /// Remove users from an access group
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [Route(Constants.Route.ApiAccessGroupsUnAssignUsers)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult UnAssignUsers(int id, [FromBody]List<int> userIds)
        {
            if (id == 0 || userIds == null || !userIds.Any())
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }
            _accessGroupService.UnAssignUsers(id, userIds);
            return new ApiSuccessResult(StatusCodes.Status200OK, AccessGroupResource.msgUnAssignUsers);
        }

        /// <summary>
        /// Change timezone for the door in the access group
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiAccessGroupsChangeTimezone)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult ChangeTimezone(int id, [FromBody]AccessGroupAssignDoorDetail model)
        {
            if (id == 0 || model == null || model.DoorId == 0 || model.TzId == 0)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest);
            }

            var accessGroup = _accessGroupService.GetById(id);
            if (accessGroup.Type == (short)AccessGroupType.FullAccess)
            {
                return new ApiErrorResult(StatusCodes.Status400BadRequest, AccessGroupResource.msgCanNotChangeTimezone);
            }

            var accessGroupAssignDoor = new AccessGroupAssignDoor
            {
                Doors = new List<AccessGroupAssignDoorDetail> { model }
            };
            _accessGroupService.ChangeTimezone(id, accessGroupAssignDoor);
            return new ApiSuccessResult(StatusCodes.Status200OK, AccessGroupResource.msgChangeTimezone);
        }
    }
}