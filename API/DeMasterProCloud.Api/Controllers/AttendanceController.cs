using System;
using System.Collections.Generic;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.WorkingModel;
using DeMasterProCloud.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Text;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Attendance;
using DeMasterProCloud.DataModel.EventLog;


namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Attendance controller
    /// </summary>
    [Produces("application/ms-excel", "application/json", "application/text")]
    [CheckAddOnAttribute(Constants.PlugIn.TimeAttendance)]
    public class AttendanceController : ControllerBase
    {
        private readonly IAttendanceService _attendanceService;
        private readonly IJwtHandler _jwtHandler;
        private readonly HttpContext _httpContext;
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        private readonly IDepartmentService _departmentService;
        private readonly IEventLogService _eventLogService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attendanceService"></param>
        /// <param name="jwtHandler"></param>
        /// <param name="httpContextAccessor"></param>
        public AttendanceController(IAttendanceService attendanceService,
            IJwtHandler jwtHandler, IHttpContextAccessor httpContextAccessor,
            IUserService userService, IDepartmentService departmentService,
            IAccountService accountService, IEventLogService eventLogService)
        {
            _attendanceService = attendanceService;
            _jwtHandler = jwtHandler;
            _httpContext = httpContextAccessor.HttpContext;
            _userService = userService;
            _departmentService = departmentService;
            _accountService = accountService;
            _eventLogService = eventLogService;
        }

        /// <summary>
        /// Get WorkingType(s)
        /// </summary>
        /// <param name="search"></param>
        /// <param name="departmentId"></param>
        /// <param name="attendanceType"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiGetAttendanceByCompany)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(string search, string departmentId, string attendanceType, string timezone, DateTime start, DateTime end, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var attendance = _attendanceService
                .GetPaginatedAttendanceReport(search, departmentId, attendanceType, timezone, start, end, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<AttendanceListModel>).ToList();

            var pagingData = new PagingData<AttendanceListModel>
            {
                Data = attendance,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        /// <summary>
        /// Load old event log to attendance list
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiReCheckAttendanceFromEventLog)]
        [Authorize(Policy = Constants.Policy.SystemAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Recheck(int companyId = 0, string fromAccessTime = null, string toAccessTime = null)
        {
            _attendanceService.Recheck(Constants.Attendance.RangTimeAllDay, companyId, fromAccessTime, toAccessTime);
            return Ok();
        }

        /// <summary>
        /// Edit Attendance 
        /// </summary>
        /// <returns></returns>
        [HttpPatch]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEditAttendance)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]AttendanceModel model)
        {
            if (id != 0)
            {
                var attendance = _attendanceService.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(), id);
                if (attendance != null)
                {
                    var user = _userService.GetById(attendance.UserId);
                    var department = _departmentService.GetById(user.DepartmentId);
                    if (department != null)
                    {

                        if (department.DepartmentManagerId != _httpContext.User.GetAccountId())
                        {
                            return new ApiErrorResult(StatusCodes.Status403Forbidden,
                                string.Format(MessageResource.NotIsDepartmentManager));
                        }

                        if (Enum.IsDefined(typeof(AttendanceType), model.Type))
                        {
                            if (model.ClockInD <= model.ClockOutD)
                            {
                                _attendanceService.Update(id, model);
                                return new ApiSuccessResult(StatusCodes.Status200OK,
                                    string.Format(MessageResource.UpdateSuccessAttendance));
                            }
                            else
                            {
                                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity,
                                    string.Format(MessageResource.TimeNotIncorrect));
                            }
                        }
                        else
                        {
                            return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity,
                                string.Format(MessageResource.TypeisNotDefine));
                        }
                    }
                    else
                    {
                        return new ApiErrorResult(StatusCodes.Status403Forbidden,
                            string.Format(MessageResource.NotIsDepartmentManager));
                    }
                }
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }

        /// <summary>
        /// export Attendance to excel 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiExportAttendance)]
        [Authorize(Policy = Constants.Policy.PrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Export(string search, string departmentId, string attendanceType, DateTime start, DateTime end,
            string sortDirection = "desc")
        {

            var attendances = _attendanceService
                .ExportAttendanceReport(search, departmentId, attendanceType, start, end, out var recordsTotal,
                    out var recordsFiltered).AsEnumerable().Select(Mapper.Map<AttendanceListModel>).ToList();

            var account = _accountService.GetById(_httpContext.User.GetAccountId());

            var str = _attendanceService.ExportAttendanceFile(attendances, account.TimeZone);

            HttpContext.Response.Headers.Add("content-disposition", "attachment; filename=Attendance" + start + "-To-" + end + ".csv");

            HttpContext.Response.ContentType = "multipart/form-data";

            byte[] temp = Encoding.UTF8.GetBytes(str.ToString());

            return File(temp, "multipart/form-data");
        }



        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiGetAttendanceRecordEachUser)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdminAndEmployee, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetAtteandanceRecordEachUser(DateTime start, DateTime end, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var user = _userService.GetUserByAccountId(_httpContext.User.GetAccountId());
            if (user != null)
            {
                var attendance = _attendanceService
                    .GetPaginatedAttendanceRecordEachUser(user.Id, start, end, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                        out var recordsFiltered).AsEnumerable().Select(Mapper.Map<AttendanceListModel>).ToList();

                var pagingData = new PagingData<AttendanceListModel>
                {
                    Data = attendance,
                    Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
                };
                return Ok(pagingData);
            }

            return new ApiErrorResult(StatusCodes.Status404NotFound);
        }
    }
}