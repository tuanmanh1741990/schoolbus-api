﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service;
using AutoMapper;
using DeMasterProCloud.DataModel.AccessGroup;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Monitoring controller
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MonitoringController : Controller
    {
        private readonly IDeviceService _deviceService;

        /// <summary>
        /// Report controller
        /// </summary>
        /// <param name="deviceService"></param>
        public MonitoringController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }

        /// <summary>
        /// Initial door list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiMonitorsDoorList)]
        public IActionResult GetDoorList()
        {
            var door = _deviceService.GetDoorList();
            var model = Mapper.Map<IEnumerable<DoorListModel>>(door);
            return Ok(model);
        }
    }
}
