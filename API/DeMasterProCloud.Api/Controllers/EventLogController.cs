﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.EventLog;
using DeMasterProCloud.Service;
using System.Linq;
using System.Threading;
using DeMasterProCloud.Service.Csv;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using AutoMapper;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Device;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.Diagnostics;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// EventLog controller
    /// </summary>
    [Produces("application/json")]
    [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class EventLogController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IEventLogService _eventLogService;
        private readonly IUserService _userService;
        private readonly IConverter _converter;
        private readonly HttpContext _httpContext;

        private readonly string[] _headers = {
            "Request",
            "Number of event",
            "Time receiving request (t1)",
            "Time finishing request (t2)",
            "Processing time in seconds (t2-t1)"
        };

        /// <summary>
        /// Report controller
        /// </summary>
        /// <param name="eventLogService"></param>
        /// <param name="userService"></param>
        /// <param name="configuration"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="converter"></param>
        public EventLogController(IEventLogService eventLogService, IUserService userService,
            IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IConverter converter)
        {
            _eventLogService = eventLogService;
            _userService = userService;
            _configuration = configuration;
            _httpContext = httpContextAccessor.HttpContext;
            _converter = converter;
        }

        /// <summary>
        /// rabbit mq test action
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsInit)]
        public IActionResult EventLogInit()
        {
            var model = _eventLogService.InitData();
            return Ok(model);
        }

        /// <summary>
        /// rabbit mq test action
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsReportInit)]
        public IActionResult EventLogReportInit()
        {
            var model = _eventLogService.InitReportData();
            return Ok(model);
        }

        /// <summary>
        /// 출입 이벤트를 조회할 수 있는 API입니다.
        /// 진행번호, 출입시간, 이름, 생년월일, 사용자코드, 부서, 카드번호, RID, 문 이름, 장소(건물명), 출/입, 이벤트 종류 순으로 데이터가 출력됩니다.
        /// This function is used in Monitoring page.
        /// </summary>
        /// <param name="model">이벤트 조회 시 시간 범위를 설정하는 파라미터입니다.</param>
        /// <param name="eventType">[Integer (1~44)]특정 이벤트 조회 시 필터할 수 있는 파라미터입니다. ex) 일반 출입 : 1 (이벤트 타입 정의는 매뉴얼을 참고해 주세요.)</param>
        /// <param name="userName">[String]사용자 이름을 필터할 수 있는 파라미터입니다.</param>
        /// <param name="inOutType">[Integer (0, 1)출입문의 In/Out 타입을 필터할 수 있는 파라미터입니다. ex) In : 0, Out : 1</param>
        /// <param name="cardId">[String]카드 ID를 필터할 수 있는 파라미터입니다.</param>
        /// <param name="doorIds">[Integer]출입문의 ID를 필터할 수 있는 파라미터입니다.</param>
        /// <param name="building">[String]건물명을 필터할 수 있는 파라미터입니다.</param>
        /// <param name="department">[String]부서명을 필터할 수 있는 파라미터입니다.</param>
        /// <param name="verifyMode">[Integer (0~3)]인증 모드를 필터할 수 있는 파라미터입니다.</param>
        /// <param name="company">[Integer]회사 ID를 필터할 수 있는 파라미터입니다. 미입력 시 기본 회사 ID가 입력됩니다.</param>
        /// <param name="pageNumber">[Integer] 페이징을 위한 파라미터입니다. 몇번째 페이지의 데이터를 출력할지 선택합니다. 미입력 시 1</param>
        /// <param name="pageSize">[Integer] 페이징을 위한 파라미터입니다. 한 페이지에 출력할 수 있는 최대 데이터 수를 정합니다. 미입력 시 10</param>
        /// <param name="sortColumn">[Integer (0~11)]어떠한 컬럼을 기준으로 데이터 정렬을 할 것인지에 대한 파라미터입니다.</param>
        /// <param name="sortDirection">[String ("asc" or "desc")]데이터의 정렬 방향을 조정하기 위한 파라미터입니다.(오름차순("asc") / (내림차순("desc")</param>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogs)]
        public IActionResult EventLog(EventLogAccessTimeModel model, List<int> eventType, string userName,
            List<string> inOutType, string cardId, List<int> doorIds, string building, string department, string verifyMode, int? company, int pageNumber = 1,
            int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            var startTime = DateTime.Now;
            var eventLogs = _eventLogService.GetPaginated(model.AccessDateFrom, model.AccessDateTo,
                    model.AccessTimeFrom, model.AccessTimeTo, eventType, userName, inOutType, cardId,
                    doorIds, building, department, verifyMode, company, pageNumber, pageSize, sortColumn, sortDirection,
                    out var recordsTotal, out var recordsFiltered).ToList()
                .Select(m =>
                {
                    var result = new EventLogListModel
                    {
                        //Id = Guid.NewGuid(),
                        AccessTime =
                            m.EventTime.ToSettingDateTimeString(),
                        UnixTime = m.EventTime.ToSettingDateTimeUnique(),
                        //UserName = m.User?.FirstName + m.User?.LastName,
                        UserName = m.UserName,
                        Department = m.User?.Department.DepartName,
                        CardId = m.CardId,
                        IsRegisteredCard = ((m.EventType == (short)EventType.UnRegisteredCard ||
                                   m.EventType == (short)EventType.UnregisteredCardAndPwd) &&
                                  !string.IsNullOrEmpty(m.CardId) && !_userService.IsCardIdExist(m.CardId)) &&
                                 User.GetAccountType() != (short)AccountType.SuperAdmin,

                        Device = m.Icu?.DeviceAddress,
                        DoorName = m.DoorName,
                        //Building = m.Icu?.Building.Name,
                        IcuId = m.Icu?.Id,
                        UserId = m.User?.Id,
                        VisitId = m.VisitId,
                        VerifyMode = m.Icu != null ? ((VerifyMode)m.Icu.VerifyMode).GetDescription() : string.Empty,
                        Type = m.CardType,
                        InOut = Constants.AntiPass.Contains(m.Antipass)
                            ? ((Antipass)Enum.Parse(typeof(Antipass), m.Antipass)).GetDescription()
                            : string.Empty,
                        EventDetail = ((EventType)m.EventType).GetDescription(),
                        IssueCount = m.IssueCount,
                        CardStatus = string.Empty,
                        //CardStatus = m.User != null? ((CardStatus)m.User.CardStatus).GetDescription() : string.Empty,
                        ExpireDate = m.User?.ExpiredDate.ToSettingDateString(),

                        UserType = m.VisitId != null ? (short) UserType.Visit : m.UserId != null ? (short) UserType.Normal : m.UserId
                    };

                    return result;
                }).ToList();

            var endTime = DateTime.Now;
            WritePerformanceCsvData(pageSize, startTime, endTime);

            var idx = (pageNumber - 1) * pageSize + 1;
            foreach (var eventLog in eventLogs)
            {
                eventLog.Id = idx++;

                var cardStatus = _eventLogService.GetCardStatus(eventLog.CardId);
                eventLog.CardStatus = string.IsNullOrEmpty(eventLog.CardId) ? string.Empty : ((CardStatus)cardStatus).GetDescription();
            }

            var pagingData = new PagingData<EventLogListModel>
            {
                Data = eventLogs,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Write performance csv data
        /// </summary>
        /// <param name="numberOfEvent"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        [NonAction]
        private void WritePerformanceCsvData(int numberOfEvent, DateTime startTime, DateTime endTime)
        {
            var enableLog = Convert.ToBoolean(_configuration[Constants.Settings.EnablePerformanceLog]);
            if (enableLog)
            {
                var csvFile = $"GetEventLog_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
                var contents = new[]
                {
                    _httpContext.Request.GetEncodedUrl(),
                    numberOfEvent.ToString(),
                    startTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),
                    endTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),
                    endTime.Subtract(startTime).TotalSeconds.ToString(CultureInfo.InvariantCulture)
                };
                CsvHelper.Write(_headers, new List<string[]> { contents }, csvFile);

            }
        }

        /// <summary>
        /// Export system log
        /// </summary>
        /// <param name="model"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="doorIds"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsExport)]
        public IActionResult Export(EventLogAccessTimeModel model, List<int> eventType, string userName,
            List<string> inOutType, string cardId, List<int> doorIds, string building, string department,
            string verifyMode, int? company, string type = "excel", int sortColumn = 0, string sortDirection = "desc")
        {
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            var fileData = _eventLogService.Export(type, sortColumn, sortDirection, out _, out _
                , model.AccessDateFrom, model.AccessDateTo, model.AccessTimeFrom, model.AccessTimeTo,
                eventType, userName, inOutType, cardId, doorIds, building, department, verifyMode,
                company);
            var filename = string.Format(Constants.ExportFileFormat, EventLogResource.lblEventLog, DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.txt";


            _eventLogService.SaveSystemLogExport(fullName);

            if (type != null && type.Equals("excel"))
                return File(fileData, "application/ms-excel", fullName);
            return File(fileData, "application/text", fullName);

        }

        /// <summary>
        /// Export 2 -> Where does this function used?
        /// </summary>
        /// <param name="model"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="doorIds"></param>
        /// <param name="buildingList"></param>
        /// <param name="department"></param>
        /// <param name="cardType"></param>
        /// <param name="company"></param>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsReportExport)]
        public IActionResult ExportReport(EventLogAccessTimeModel model, List<int> eventType, string userName,
           List<int> inOutType, string cardId, List<int> doorIds, List<int> buildingList, string department,
            List<int> cardType, int? company, string type = "excel", int sortColumn = 0, string sortDirection = "desc")
        {
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }
            var fileData = _eventLogService.ExportReport(type, sortColumn, sortDirection, out _, out _
                , model.AccessDateFrom, model.AccessDateTo, model.AccessTimeFrom, model.AccessTimeTo,
                eventType, userName, inOutType, cardId, doorIds, buildingList, department, cardType,
                company);
            var filename = string.Format(Constants.ExportFileFormat, EventLogResource.lblEventLog, DateTime.Now);
            var fullName = type == "excel" ? $"{filename}.xlsx" : $"{filename}.txt";


            _eventLogService.SaveSystemLogExport(fullName);

            if (type != null && type.Equals("excel"))
                return File(fileData, "application/ms-excel", fullName);
            return File(fileData, "application/text", fullName);

        }


        /// <summary>
        /// View the event log data through pdf file.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="doorIds"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsExportPdf)]
        public IActionResult EventLogPdfView(EventLogAccessTimeModel model, List<int> eventType, string userName,
            List<string> inOutType, string cardId, List<int> doorIds, string building, string department,
            string verifyMode, int? company, int sortColumn = 0, string sortDirection = "desc")
        {
            var eventLogs = _eventLogService.FilterDataWithOrder(model.AccessDateFrom, model.AccessDateTo,
                model.AccessTimeFrom, model.AccessTimeTo,
                eventType, userName, inOutType, cardId, doorIds, building, department, verifyMode,
                company, sortColumn, sortDirection, out _, out _).AsEnumerable()
                .Select(m =>
                {
                    var result = new EventLogListModel
                    {
                        //Access Time,User Name,Card Id,Device,Door Name,Card Type,IN/OUT,Event Detail
                        AccessTime = m.CreatedOn.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),//Access time
                        UserName = m.EventType != (int)EventType.UnRegisteredCard ? m.User?.FirstName : string.Empty,
                        CardId = m.CardId,
                        Device = m.Icu.DeviceAddress,
                        DoorName = m.DoorName,
                        CardType = ((CardType)m.CardType).GetDescription(),
                        InOut = ((InOut)m.Icu.PassbackRule).GetDescription(),
                        EventDetail = ((EventType)m.EventType).GetDescription(),
                    };
                    return result;
                }).ToList();

            ViewBag.Message = eventLogs;
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report",
            };

            var html = View().ToHtml(_httpContext);
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = html,
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/css", "styles.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" },
            };

            var pdf = new HtmlToPdfDocument
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            var file = _converter.Convert(pdf);
            var filename = string.Format(Constants.ExportFileFormat, EventLogResource.lblEventLog, DateTime.Now) + ".pdf";

            _eventLogService.SaveSystemLogExport(filename);

            return File(file, "application/pdf", filename);
        }

        /// <summary>
        /// Get Device List for Event Recovery
        /// </summary>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Json result object</returns>
        [HttpGet]
        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsRecoveryDevices)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult GetDeviceList(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var doors = _eventLogService.GetPaginatedRecoveryDevices(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered).AsEnumerable().Select(Mapper.Map<RecoveryDeviceModel>).AsQueryable().ToList();

            var pagingData = new PagingData<RecoveryDeviceModel>
            {
                Data = doors,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get Device List for Event Recovery
        /// </summary>
        /// <param name="deviceIds"></param>
        /// <param name="model"></param>
        /// <param name="search"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns>Json result object</returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsEventInquiry)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult EventCountInquiry(List<int> deviceIds, EventLogAccessTimeModel model, string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
            string sortDirection = "desc")
        {
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            var doors = _eventLogService.GetPaginatedEventRecoveryInquireDevices(deviceIds, model.AccessDateFrom, model.AccessDateTo, model.AccessTimeFrom, model.AccessTimeTo, search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                out var recordsFiltered);

            var pagingData = new PagingData<RecoveryDeviceModel>
            {
                Data = doors,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Event Recovery
        /// </summary>
        /// <param name="eventRecoveryProgressModels"></param>
        /// <param name="model"></param>
        /// <returns>Json result object</returns>
        [HttpPost]
        [Route(Constants.Route.ApiEventLogsRecovery)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult EventRecovery([FromBody]List<EventRecoveryProgressModel> eventRecoveryProgressModels, EventLogAccessTimeModel model)
        {
            if (eventRecoveryProgressModels == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            _eventLogService.EventRecovery(eventRecoveryProgressModels, model.AccessDateFrom, model.AccessDateTo,
                model.AccessTimeFrom, model.AccessTimeTo);
            return new ApiSuccessResult(StatusCodes.Status200OK, MessageResource.msgEventRecoverySuccess);
        }

        /// <summary>
        /// This function is used in Report page.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="doorIds"></param>
        /// <param name="userName"></param>
        /// <param name="cardId"></param>
        /// <param name="department"></param>
        /// <param name="inOutType"></param>
        /// <param name="eventType"></param>
        /// <param name="buildingList"></param>
        /// <param name="cardType"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsReport)]
        public IActionResult EventReport(EventLogAccessTimeModel model, List<int> doorIds, string userName,
            string cardId, string department, List<int> inOutType, List<int> eventType, List<int> buildingList, List<int> cardType, string culture, int pageNumber = 1,
            int pageSize = 10, int sortColumn = 0, string sortDirection = "desc")
        {
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }


            var stopWatch = Stopwatch.StartNew();

            var eventLogs = _eventLogService.GetPaginatedEventLogReport(model.AccessDateFrom, model.AccessDateTo,
                    model.AccessTimeFrom, model.AccessTimeTo, doorIds, userName,
            cardId, department, inOutType, eventType, buildingList, cardType, culture, pageNumber, pageSize, sortColumn, sortDirection,
                    out var recordsTotal, out var recordsFiltered);

            var eventLogList = eventLogs.ToList();

            var idx = (pageNumber - 1) * pageSize + 1;
            foreach (var eventLog in eventLogList)
            {
                eventLog.Id = idx++;
            }

            var pagingData = new PagingData<EventLogReportListModel>
            {
                Data = eventLogList,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };

            stopWatch.Stop();
            Trace.WriteLine($"Elapsed time {stopWatch.ElapsedMilliseconds} ms");
            Trace.WriteLine($"Search {recordsTotal} event(s) successfully in {TimeSpan.FromMilliseconds(stopWatch.ElapsedMilliseconds).TotalSeconds} seconds!");
            Trace.WriteLine($"PageNumber : {pageNumber}, PageSize : {pageSize}");

            return Ok(pagingData);
        }



        /// <summary>
        /// 오픈 API - 사용자 이벤트 조회
        /// </summary>
        /// <param name="id">사용자 ID</param>
        /// <returns>return Event log for 1 user</returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiEventLogsOpenReport)]
        public IActionResult OpenAPIEventReport(int id)
        {
            var eventLogs = _eventLogService.GetPaginatedOpenEventLogReport(id,
                    out var recordsTotal, out var recordsFiltered).ToList()
                .Select(m =>
                {
                    var result = new EventLogReportListModel
                    {
                        //Id = Guid.NewGuid(),
                        AccessTime =
                            m.EventTime.ToSettingDateTimeString(),
                        UserName = m.User != null ? m.User.FirstName + " " + m.User.LastName : m.UserName ?? String.Empty,
                        BirthDay = m.IsVisit ? m.Visit?.BirthDay.ToSettingDateString() : m.User?.BirthDay.ToSettingDateString(),
                        EmployeeNumber = m.IsVisit ? m.Visit?.VisitorEmpNumber : m.User?.EmpNumber,
                        Department = m.IsVisit ? m.Visit?.VisitorDepartment : m.User?.Department?.DepartName,
                        CardId = m.CardId,


                        DeviceAddress = m.Icu?.DeviceAddress,
                        DoorName = m.DoorName,
                        Building = m.Icu?.Building.Name,
                        InOut = Constants.AntiPass.Contains(m.Antipass)
                            ? ((Antipass)Enum.Parse(typeof(Antipass), m.Antipass)).GetDescription()
                            : string.Empty,

                        IcuId = m.Icu?.Id,

                        UserId = m.User?.Id,
                        VisitId = m.Visit?.Id,


                        EventDetail = ((EventType)m.EventType).GetDescription(),
                        IssueCount = m.IssueCount,
                        CardStatus = ((CardStatus)_eventLogService.GetCardStatus(m.CardId)).GetDescription(),
                        CardType = ((PassType)m.CardType).GetDescription(),
                    };

                    return result;
                }).ToList();

            

            var idx = 1;
            foreach (var eventLog in eventLogs)
            {
                eventLog.Id = idx++;
            }

            var pagingData = new PagingData<EventLogReportListModel>
            {
                Data = eventLogs,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }


        /// <summary>
        /// Generate test data
        /// </summary>
        /// <param name="numberOfevent"> number of event. </param>
        /// <returns></returns>
        [HttpPost]
        [Route("/event-logs/create-test-data")]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult CreateTestData(int numberOfevent)
        {
            if (numberOfevent == 0)
            {
                return Ok(new { message = "No user data is created!" });
            }

            var stopWatch = Stopwatch.StartNew();
            _eventLogService.GenerateTestData(numberOfevent);
            stopWatch.Stop();
            Trace.WriteLine($"Elapsed time {stopWatch.ElapsedMilliseconds} ms");
            return Ok(new
            {
                message =
                    $"{numberOfevent} user(s) data were created successfully in {TimeSpan.FromMilliseconds(stopWatch.ElapsedMilliseconds).TotalSeconds} seconds!"
            });
        }


        //=========================================================================================================================================//
        // For Duali Korea

        /// <summary>
        /// Get event log for attendence
        /// </summary>
        /// <param name="AccessDateFrom"> start date </param>
        /// <param name="AccessDateTo"> end date </param>
        /// <param name="userIds"> list of identifier of user </param>
        /// <param name="departmentIds"> list of identifier of department </param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiAttendance)]
        public IActionResult GetEventLogForAttendance(string AccessDateFrom, string AccessDateTo, List<int> userIds, List<int> departmentIds)
        {
            var eventLogs = _eventLogService.GetAttendenceForDuali(AccessDateFrom, AccessDateTo,
                   userIds, departmentIds).ToList()
                .Select(m =>
                {
                    var result = new EventLogListModel
                    {
                        AccessTime =
                            m.EventTime.ToSettingDateTimeString(),
                        UserName = m.UserName,
                        Department = m.User?.Department.DepartName,
                        UserId = m.User?.Id,
                    };

                    return result;
                }).ToList();

            var pagingData = new PagingData<EventLogListModel>
            {
                Data = eventLogs,
            };

            return Ok(pagingData);
        }

    }
}