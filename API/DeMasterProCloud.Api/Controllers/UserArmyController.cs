﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Category;

namespace DeMasterProCloud.Api.Controllers
{
    /// <summary>
    /// Army user controller
    /// </summary>
    [Produces("application/json")]
    [CheckAddOnAttribute(Constants.PlugIn.ArmyManagement)]
    public class UserArmyController : Controller
    {
        private readonly IUserService _userService;
        private readonly IUserArmyService _userArmyService;
        //private readonly IEventLogService _eventLogService;
        private readonly ICategoryService _categoryService;
        private readonly HttpContext _httpContext;

        /// <summary>
        /// Controller of Army user
        /// </summary>
        /// <param name="userService"> user service </param>
        /// <param name="userArmyService"> userArmy service </param>
        /// <param name="categoryService"> category service </param>
        /// <param name="httpContextAccessor"></param>
        public UserArmyController(IUserService userService, IUserArmyService userArmyService, ICategoryService categoryService, IHttpContextAccessor httpContextAccessor)
        {
            _userService = userService;
            _userArmyService = userArmyService;
            _categoryService = categoryService;
            _httpContext = httpContextAccessor.HttpContext;
        }

        /// <summary>
        /// Get army user list
        /// </summary>
        /// <param name="search"> filter to search </param>
        /// <param name="pageNumber"> number of page </param>
        /// <param name="pageSize"> size of page </param>
        /// <param name="sortColumn"> index number of sort column </param>
        /// <param name="sortDirection"> asc or desc </param>
        /// <param name="isValid"> valid user or all user </param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUserArmys)]
        [Authorize(Policy = Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Gets(string search, int pageNumber = 1, int pageSize = 10, int sortColumn = 1,
            string sortDirection = "desc", bool isValid = true)
        {
            var userArmys = _userArmyService
                .GetPaginated(search, pageNumber, pageSize, sortColumn, sortDirection, out var recordsTotal,
                    out var recordsFiltered, out List<HeaderData> userHeader, isValid);

            foreach (var userArmy in userArmys)
            {
                userArmy.CardList = _userService.GetCardListByUserId(userArmy.Id);
            }

            var companyId = _httpContext.User.GetCompanyId();

            var pagingData = new PagingData<UserArmyListModel, HeaderData>
            {
                Data1 = userArmys,
                Data2 = userHeader,
                Meta = { RecordsTotal = recordsTotal, RecordsFiltered = recordsFiltered }
            };
            return Ok(pagingData);
        }

        /// <summary>
        /// Get user by id. In the case id = 0 then just getting initial data.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        [Route(Constants.Route.ApiUserArmysId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Get(int id)
        {
            var model = new UserArmyDataModel();
            if (id != 0)
            {
                var user = _userService.GetById(id);
                if (user == null)
                {
                    return new ApiErrorResult(StatusCodes.Status404NotFound);
                }
                model = Mapper.Map<UserArmyDataModel>(user);
            }
            _userArmyService.InitData(model);

            return Ok(model);
        }

        /// <summary>
        /// Add new a army user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(Constants.Route.ApiUserArmys)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Add([FromBody]UserArmyModel model)
        {
            if (!ModelState.IsValid)
            {
                return new ValidationFailedResult(ModelState);
            }

            // Checking if email is existed in system.
            var valid = _userService.IsDuplicatedAccountCreated(model.Email);
            if (!valid)
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, AccountResource.lblEmail));
            }

            // Checking if card is existed in system.
            if (model.CardList != null && model.CardList.Any())
            {
                foreach (var card in model.CardList)
                {
                    var isCardIdExist = _userService.IsCardIdExist(card.CardId);

                    if (isCardIdExist)
                    {
                        return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, UserResource.lblCardId));
                    }
                }
            }

            if (model.PermissionType != (short)PermissionType.NotUse && string.IsNullOrEmpty(model.Email))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.EmailDoestNotExist));
            }

            // Add user information
            var userModel = Mapper.Map<UserModel>(model);
            int userId = _userService.Add(userModel);

            // Add userArmy information
            _userArmyService.Add(userId, model);

            return new ApiSuccessResult(StatusCodes.Status201Created,
                string.Format(MessageResource.MessageAddSuccess, UserResource.lblUser));
        }

        /// <summary>
        /// Edit army user by id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(Constants.Route.ApiUserArmysId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Edit(int id, [FromBody]UserArmyModel model)
        {
            model.Id = id;
            ModelState.Clear();
            if (!TryValidateModel(model))
            {
                return new ValidationFailedResult(ModelState);
            }

            // Check duplication of E-mail
            if (model.PermissionType == (int)PermissionType.SystemAdmin)
            {
                var account = _userService.GetAccountByEmail(model.Email);
                if (account != null && _userService.IsAccountExist(account.Id, model.Email))
                {
                    return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.Exist, AccountResource.lblUsername));
                }
            }

            var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (user == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            if (model.Email != null)
            {
                var account = _userService.GetAccountByUserName(user.CompanyId, model.Email);
                if (account != null)
                {
                    var alreadyLinkAccount = _userService.GetAccountAlreadyLinkToUser(user.CompanyId, account.Id);
                    if (alreadyLinkAccount != null)
                    {
                        if (alreadyLinkAccount.Id != user.Id)
                        {
                            return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity,
                                string.Format(MessageResource.Exist, AccountResource.lblEmail));
                        }
                    }
                }

            }

            if (model.PermissionType != (short)PermissionType.NotUse && string.IsNullOrEmpty(model.Email))
            {
                return new ApiErrorResult(StatusCodes.Status422UnprocessableEntity, string.Format(MessageResource.EmailDoestNotExist));
            }

            // update user information
            var userModel = Mapper.Map<UserModel>(model);
            _userService.Update(userModel);

            // update userArmy information
            _userArmyService.Update(model);


            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageUpdateSuccess, UserResource.lblUser, ""));
        }

        /// <summary>
        /// Delete a army user by Id
        /// </summary>
        /// <param name="id"> Identifier of user to be deleted </param>
        /// <returns>Json reslut object</returns>
        [HttpDelete]
        [Route(Constants.Route.ApiUserArmysId)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult Delete(int id)
        {
            var user = _userService.GetByIdAndCompany(id, _httpContext.User.GetCompanyId());
            if (user == null)
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            // Delete User ( isDeleted = false -> true )
            _userService.Delete(user);
            // Delete UserArmy ( Delete )
            _userArmyService.Delete(id);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteSuccess, UserResource.lblUser));
        }

        /// <summary>
        /// Delete multiple army user by list of user id
        /// </summary>
        /// <param name="ids"> List of user Id </param>
        /// <returns>Json result object</returns>
        [HttpDelete]
        [Route(Constants.Route.ApiUserArmys)]
        [Authorize(Policy = Constants.Policy.SuperAndPrimaryAdmin, AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IActionResult DeleteMultiple(List<int> ids)
        {
            var users = _userService.GetByIdsAndCompany(ids, _httpContext.User.GetCompanyId());

            if (users == null || !users.Any())
            {
                return new ApiErrorResult(StatusCodes.Status404NotFound);
            }

            // Delete Users ( isDeleted = false -> true )
            _userService.DeleteRange(users);
            // Delete UserArmy ( Delete )
            _userArmyService.DeleteRange(ids);

            return new ApiSuccessResult(StatusCodes.Status200OK,
                string.Format(MessageResource.MessageDeleteMultipleSuccess, UserResource.lblUser));
        }
    }
}