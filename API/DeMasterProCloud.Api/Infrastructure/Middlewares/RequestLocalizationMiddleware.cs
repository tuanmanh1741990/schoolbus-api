﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.CodeAnalysis.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace DeMasterProCloud.Api.Infrastructure.Middlewares
{
    /// <summary>
    /// Request Localization middleware
    /// </summary>
    public class RequestLocalizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _configuration;
        private readonly IOptions<List<string>> _options;

        /// <summary>
        /// Request localization in middle ware
        /// </summary>
        /// <param name="next"></param>
        /// <param name="configuration"></param>
        /// <param name="options"></param>
        public RequestLocalizationMiddleware(RequestDelegate next, IConfiguration configuration, IOptions<List<string>> options)
        {
            _next = next;
            _configuration = configuration;
            _options = options;
        }

        public async Task Invoke(HttpContext context)
        {
            SetCurrentCulture(context);
            await _next(context);
        }

        /// <summary>
        /// Set culture for each api request
        /// </summary>
        /// <param name="context"></param>
        private void SetCurrentCulture(HttpContext context)
        {
            var reqCulture = context.Request.Query["culture"].ToString();
            if (string.IsNullOrEmpty(reqCulture))
            {
                //Set default culture en-US
                reqCulture = _options.Value[0];
            }
            var culture = new CultureInfo(reqCulture);
            //context.Response.Cookies.Append(
            //    CookieRequestCultureProvider.DefaultCookieName,
            //    CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
            //    new CookieOptions { Expires = DateTimeOffset.UtcNow.AddDays(30) }
            //);

            CultureInfo.CurrentCulture = new CultureInfo(culture.Name);
            CultureInfo.CurrentUICulture = culture;
        }
    }
}
