﻿using FluentValidation;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.Service;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Device Validation
    /// </summary>
    public class DeviceValidation : AbstractValidator<DeviceModel>
    {
        /// <summary>
        /// Device Validation
        /// </summary>
        /// <param name="deviceService"></param>
        public DeviceValidation(IDeviceService deviceService)
        {
            RuleFor(reg => reg.DeviceAddress).NotEmpty().MaximumLength(6)
                .Must((reg, d) => !deviceService.IsDeviceAddressExist(reg))
                .WithMessage(DeviceResource.msgDeviceAddressInUse);

            RuleFor(reg => reg.DoorName).NotEmpty().MaximumLength(100);

            //RuleFor(reg => reg.ActiveTimezoneId).NotEmpty();

            RuleFor(reg => reg.VerifyMode).NotNull();

            RuleFor(reg => reg.MPRCount).NotEmpty().InclusiveBetween(1, 10)
                .WithMessage(string.Format(MessageResource.RangeFromTo, DeviceResource.lblCondition));

            RuleFor(reg => reg.MPRInterval).InclusiveBetween(1, 180)
                .WithMessage(string.Format(MessageResource.RangeFromToInterval, DeviceResource.lblMprInterval));

            //RuleFor(reg => reg.IpAddress).NotEmpty()/*.When(reg => reg.NetworkEnable)*/
            //    .Must((reg, strings) => Helpers.IsIpAddress(reg.IpAddress))
            //    .WithMessage(reg => string.Format(MessageResource.InvalidIpAddress, reg.IpAddress))
            //    /*.When(reg => reg.NetworkEnable)*/;

            RuleFor(reg => reg.LockOpenDuration).NotNull().InclusiveBetween(1, 254);

            RuleFor(reg => reg.SensorDuration).InclusiveBetween(1, 254)
                //.Must((reg, c) => (reg.StatusDelay >= reg.LockOpenDuration))
                //.WithMessage(string.Format(MessageResource.GreaterThanOrEqual, DeviceResource.lblStatusDelay, DeviceResource.lblLockOpenDuration))
                .When(reg => reg.Alarm == true);
            //.When(reg => reg.SensorType != (short)SensorType.None);

            //RuleFor(reg => reg.CloseReverseLock).Must((reg, c) => reg.CloseReverseLock)
            //    .WithMessage(string.Format(MessageResource.Required, DeviceResource.lblCloseReverseLock))
            //    .When(reg => reg.SensorType != (short)SensorType.None);

            // [Edward] 2020.03.04
            // Add validation to prevent the different card readers have same role.
            RuleFor(reg => reg.RoleReader1)
                .Must((reg, c) => reg.RoleReader0 != reg.RoleReader1)
                .WithMessage(string.Format(DeviceResource.msgDuplicateRole))
                .When(reg => reg.UseCardReader == 0 || reg.DeviceType == (short)DeviceType.Icu300N);
        }
    }
}
