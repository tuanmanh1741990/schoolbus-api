﻿using FluentValidation;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.DataModel.DeviceMessage;
using DeMasterProCloud.Service;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// DeviceMessage validation
    /// </summary>
    public class DeviceMessageValidation : AbstractValidator<DeviceMessageModel>
    {
        /// <summary>
        /// DeviceMessage validation
        /// </summary>
        /// <param name="deviceMessageService"></param>
        public DeviceMessageValidation(IDeviceMessageService deviceMessageService)
        {
            RuleFor(reg => reg.MessageId).InclusiveBetween(1, 10)
                .WithMessage(string.Format(MessageResource.RangeFromTo, DeviceMessageResource.lblMessageId));
        }
    }
}
