﻿using System;
using Bogus.Extensions;
using FluentValidation;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Account;
using DeMasterProCloud.DataModel.Login;
using DeMasterProCloud.Service;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Validation for account model
    /// </summary>
    public class AccountValidation : AbstractValidator<AccountModel>
    {
        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="accountService"></param>
        public AccountValidation(IAccountService accountService)
        {
            RuleFor(reg => reg.Username).NotEmpty()
                .EmailAddress()
                .MaximumLength(50)
                .Must((reg, c) => !accountService.IsExist(reg.Username))
                .When(reg => reg.Id == 0)
                .WithMessage(string.Format(MessageResource.Exist, AccountResource.lblEmail));


            RuleFor(reg => reg.Password).NotEmpty()
                .When(reg => reg.Id == 0)
                .MinimumLength(6)
                .MaximumLength(50)
                .Must((reg, c) => !Helpers.IsUnicode(reg.Password))
                .WithMessage(
                    string.Format(MessageResource.CannotIncludeUnicodeCharacter, AccountResource.lblPassword));

            RuleFor(reg => reg.ConfirmPassword).NotEmpty()
                .When(reg => reg.Id == 0)
                .MinimumLength(6)
                .MaximumLength(50)
                .Equal(reg => reg.Password).WithMessage(string.Format(MessageResource.Compare,
                    AccountResource.lblConfirmPassword, AccountResource.lblPassword))
                .Must((reg, c) => !Helpers.IsUnicode(reg.ConfirmPassword))
                .WithMessage(
                string.Format(MessageResource.CannotIncludeUnicodeCharacter, AccountResource.lblConfirmPassword));

            RuleFor(reg => reg.Role).NotNull()
                .Must((reg, c) => accountService.IsValidAccountType(reg.Role))
                .WithMessage(AccountResource.msgInvalidAccountType);

            RuleFor(reg => reg.TimeZone)
                .MaximumLength(100)
                .Must((reg, c) => Helpers.IsValidTimeZone(reg.TimeZone))
                .When(reg => !String.IsNullOrEmpty(reg.TimeZone));

        }
    }

    /// <summary>
    /// Forget password validation
    /// </summary>
    public class ForgotPasswordValidation : AbstractValidator<ForgotPasswordModel>
    {
        /// <summary>
        /// Forgot password validation
        /// </summary>
        public ForgotPasswordValidation()
        {
            RuleFor(reg => reg.Email).NotEmpty()
                .EmailAddress()
                .MaximumLength(50);
        }
    }

    public class ResetPasswordValidation : AbstractValidator<ResetPasswordModel>
    {
        public ResetPasswordValidation()
        {
            RuleFor(reg => reg.NewPassword).NotEmpty()
                .MinimumLength(6)
                .MaximumLength(50);

            RuleFor(reg => reg.ConfirmNewPassword).NotEmpty()
                .MinimumLength(6)
                .MaximumLength(50)
                .Equal(reg => reg.NewPassword).WithMessage(string.Format(MessageResource.Compare,
                    AccountResource.lblConfirmNewPassword, AccountResource.lblNewPassword));

            RuleFor(reg => reg.Token).NotEmpty();
        }
    }
}