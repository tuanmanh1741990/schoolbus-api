﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.DataModel.Visit;
using DeMasterProCloud.Service;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Validation for visitor
    /// </summary>
    public class VisitValidation : AbstractValidator<VisitModel>
    {
        /// <summary>
        /// Validation for visitor
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="visitService"></param>
        public VisitValidation(IConfiguration configuration, IVisitService visitService)
        {
            RuleFor(reg => reg.VisitorName).NotEmpty().WithMessage(MessageResource.VisitorNameInValid);
            RuleFor(reg => reg.VisitorName).MaximumLength(100);

            RuleFor(reg => reg.BirthDay).Must((reg, c) => DateTimeHelper.IsDateTime(reg.BirthDay))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, VisitResource.lblBirthDay,
                    Helpers.GetDateServerFormat()));



            RuleFor(reg => reg.VisitorEmpNumber).MaximumLength(100);
            /*RuleFor(reg => reg.Position).NotEmpty();*/
            RuleFor(reg => reg.Phone).MaximumLength(50);
            /*RuleFor(reg => reg.AccessGroupId).NotEmpty();*/


            RuleFor(reg => reg.StartDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.StartDate))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, VisitResource.lblStartDate,
                    Helpers.GetDateServerFormat()));

            RuleFor(reg => reg.EndDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.EndDate))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, VisitResource.lblEndDate,
                    Helpers.GetDateServerFormat()));


            //RuleFor(reg => reg.Address).MaximumLength(100);

            //RuleFor(reg => reg.DepartmentId).NotEmpty();
            //RuleFor(reg => reg.AccessGroupId).NotEmpty();



            //RuleFor(reg => reg.EffectiveDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.EffectiveDate))
            //    .WithMessage(string.Format(MessageResource.InvalidDateFormat, UserResource.lblEffectiveDate,
            //        Helpers.GetDateServerFormat()));

            //RuleFor(reg => reg.ExpiredDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.ExpiredDate))
            //    .WithMessage(string.Format(MessageResource.InvalidDateFormat, UserResource.lblExpiredDate,
            //        Helpers.GetDateServerFormat()));

            //RuleFor(reg => reg.ExpiredDate).Must((reg, c) => DateTimeHelper.CheckDateTime(reg.ExpiredDate, reg.EffectiveDate))
            //    .WithMessage(string.Format(MessageResource.InvalidDate, UserResource.lblExpiredDate,
            //        UserResource.lblEffectiveDate));

            //RuleFor(reg => reg.IssuedDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.IssuedDate))
            //    .WithMessage(string.Format(MessageResource.InvalidDateFormat, UserResource.lblIssuedDate,
            //        Helpers.GetDateServerFormat()));

            //RuleFor(reg => reg.FirstName).NotEmpty()
            //    .MaximumLength(100);

            //RuleFor(reg => reg.LastName).MaximumLength(100);

            //RuleFor(reg => reg.Nationality).MaximumLength(100);

            //RuleFor(reg => reg.City).MaximumLength(100);

            //RuleFor(reg => reg.Position).MaximumLength(100);

            //RuleFor(reg => reg.PostCode).MaximumLength(20);

            //RuleFor(reg => reg.HomePhone).MaximumLength(20);

            //RuleFor(reg => reg.OfficePhone).MaximumLength(20);

            //RuleFor(reg => reg.Job).MaximumLength(100);

            //RuleFor(reg => reg.Responsibility).MaximumLength(100);

            //RuleFor(reg => reg.IssueCount).NotEmpty()
            //    .Must((reg, c) => Regex.IsMatch(reg.IssueCount, RegexConstants.Integer))
            //    .WithMessage(string.Format(MessageResource.Number, UserResource.lblIssueCount));

            //RuleFor(reg => reg.KeyPadPassword).MaximumLength(4)
            //    .Must((reg, c) => Regex.IsMatch(reg.KeyPadPassword, RegexConstants.Integer))
            //    .WithMessage(string.Format(MessageResource.Number, UserResource.lblKeyPadPassword))
            //    .When(reg => !string.IsNullOrEmpty(reg.KeyPadPassword))
            //    .Must((reg, c) => !userService.IsKeyPadPasswordExist(reg.Id,
            //        Encryptor.Encrypt(reg.KeyPadPassword, configuration[Constants.Settings.EncryptKey])))
            //    .WithMessage(string.Format(MessageResource.Exist, UserResource.lblKeyPadPassword))
            //    .When(reg => !string.IsNullOrEmpty(reg.KeyPadPassword));
        }
    }
}
