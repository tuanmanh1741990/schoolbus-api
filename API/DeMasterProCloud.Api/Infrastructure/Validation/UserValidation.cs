﻿using FluentValidation;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Service;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>   A user validation. </summary>
    ///
    /// <remarks>   Edward, 2020-01-30. </remarks>

    public class UserValidation : AbstractValidator<UserModel>
    {
        /// <summary>   Constructor. </summary>
        ///
        /// <remarks>   Edward, 2020-01-30. </remarks>
        ///
        /// <param name="configuration">    The configuration. </param>
        /// <param name="userService">      The user service. </param>

        public UserValidation(IConfiguration configuration, IUserService userService)
        {
            //RuleFor(reg => reg.Avatar).Must(m =>
            //        Helpers.IsValidImage(Helpers.GetFileExtension(m),
            //            configuration[Constants.AllowImageType].Split(",")))
            //    .When(m => !string.IsNullOrEmpty(m.Avatar))
            //    .WithMessage(string.Format(MessageResource.InvalidImageExtension, UserResource.lblAvatar,
            //        configuration[Constants.AllowImageType]));

            RuleFor(reg => reg.Address).MaximumLength(100);

            RuleFor(reg => reg.DepartmentId).NotEmpty();
            RuleFor(reg => reg.AccessGroupId).NotEmpty();


            RuleFor(reg => reg.UserCode)
                .MaximumLength(10)
                .WithMessage(string.Format(UserResource.msgUserCodeLength))
                .Must((reg, c) => !userService.IsDuplicatedUserCode(reg.Id, reg.UserCode))
                .WithMessage(string.Format(UserResource.msgUserCodeIsBeingUsedByAnother));

            //RuleFor(reg => reg.CardList).NotEmpty()
            //    .maximumlength(50)
            //    .must((reg, c) => !userservice.iscardidexist(reg))
            //    .withmessage(string.Format(MessageResource.Exist, UserResource.lblCardId));

            RuleFor(reg => reg.Email)
                .Must((reg, c) => userService.IsEmailValid(reg.Email))
                .WithMessage(string.Format(MessageResource.EmailInvalid))
                /*.Must((reg, c) => !userService.IsDuplicatedEmail(reg.Id, reg.Email))
                .WithMessage(string.Format(MessageResource.Exist, AccountResource.lblEmail))*/
                .When(reg => !string.IsNullOrEmpty(reg.Email))
                .NotEmpty()
                .MaximumLength(50)
                .WithMessage(string.Format(MessageResource.EmailMaxLength));

            RuleFor(reg => reg.EffectiveDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.EffectiveDate))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, UserResource.lblEffectiveDate,
                    Helpers.GetDateServerFormat()));

            RuleFor(reg => reg.ExpiredDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.ExpiredDate))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, UserResource.lblExpiredDate,
                    Helpers.GetDateServerFormat()));

            RuleFor(reg => reg.ExpiredDate).Must((reg, c) => DateTimeHelper.CheckDateTime(reg.ExpiredDate, reg.EffectiveDate))
                .WithMessage(string.Format(MessageResource.InvalidDate, UserResource.lblExpiredDate,
                    UserResource.lblEffectiveDate));

            //RuleFor(reg => reg.IssuedDate).Must((reg, c) => DateTimeHelper.IsDateTime(reg.IssuedDate))
            //    .WithMessage(string.Format(MessageResource.InvalidDateFormat, UserResource.lblIssuedDate,
            //        Helpers.GetDateServerFormat()));

            RuleFor(reg => reg.FirstName).NotEmpty()
                .MaximumLength(100);

            //RuleFor(reg => reg.LastName).MaximumLength(100);

            RuleFor(reg => reg.Nationality).MaximumLength(100);

            RuleFor(reg => reg.City).MaximumLength(100);

            RuleFor(reg => reg.Position).MaximumLength(100);

            RuleFor(reg => reg.PostCode).MaximumLength(20);

            RuleFor(reg => reg.HomePhone).MaximumLength(20);

            RuleFor(reg => reg.OfficePhone).MaximumLength(20);

            //RuleFor(reg => reg.Job).MaximumLength(100);

            //RuleFor(reg => reg.Responsibility).MaximumLength(100);

            //RuleFor(reg => reg.IssueCount).NotEmpty()
            //    .Must((reg, c) => Regex.IsMatch(reg.IssueCount, RegexConstants.Integer))
            //    .WithMessage(string.Format(MessageResource.Number, UserResource.lblIssueCount));

            //RuleFor(reg => reg.KeyPadPassword).MaximumLength(4)
            //    .Must((reg, c) => Regex.IsMatch(reg.KeyPadPassword, RegexConstants.Integer))
            //    .WithMessage(string.Format(MessageResource.Number, UserResource.lblKeyPadPassword))
            //    .When(reg => !string.IsNullOrEmpty(reg.KeyPadPassword))
            //    .Must((reg, c) => !userService.IsKeyPadPasswordExist(reg.Id,
            //        Encryptor.Encrypt(reg.KeyPadPassword, configuration[Constants.Settings.EncryptKey])))
            //    .WithMessage(string.Format(MessageResource.Exist, UserResource.lblKeyPadPassword))
            //    .When(reg => !string.IsNullOrEmpty(reg.KeyPadPassword));
        }
    }
}
