﻿using FluentValidation;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.Service;
using DeMasterProCloud.Common.Infrastructure;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Holiday validation
    /// </summary>
    public class HolidayValidation : AbstractValidator<HolidayModel>
    {
        /// <summary>
        /// Holiday validation
        /// </summary>
        /// <param name="holidayService"></param>
        public HolidayValidation(IHolidayService holidayService)
        {
            RuleFor(reg => reg.Name).NotEmpty()
                .MaximumLength(50)
                .Must((reg, strings) => !holidayService.IsExistedName(reg.Id, reg.Name))
                .WithMessage(reg => string.Format(MessageResource.Exist, reg.Name));
            RuleFor(reg => reg.StartDate).NotEmpty()
                .Must((reg, c) => DateTimeHelper.IsDateTime(reg.StartDate))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, HolidayResource.lblStartDate,
                    Helpers.GetDateServerFormat()))
                .Must((reg, strings) => !holidayService.IsOverLapDurationTime(reg.Id, reg.StartDate, reg.EndDate))
                .WithMessage(HolidayResource.msgOverlapDurationTime);
            RuleFor(reg => reg.EndDate).NotEmpty()
                .Must((reg, c) => DateTimeHelper.IsDateTime(reg.StartDate))
                .WithMessage(string.Format(MessageResource.InvalidDateFormat, HolidayResource.lblEndDate,
                    Helpers.GetDateServerFormat()))
                .Must((reg, strings) => Helpers.CompareDate(reg.StartDate, reg.EndDate))
                .WithMessage(reg => string.Format(MessageResource.GreaterThan, HolidayResource.lblEndDate, reg.StartDate));
            RuleFor(reg => reg.Type).Must((reg, c) => reg.Type > 0)
                .WithMessage(reg => string.Format(MessageResource.NotSelected, HolidayResource.lblHolidayType));
        }

    }
}
