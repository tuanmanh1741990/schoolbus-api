﻿using FluentValidation;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel.Category;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Category validation
    /// </summary>
    public class CategoryValidation : AbstractValidator<CategoryModel>
    {
        /// <summary>
        /// Category validation
        /// </summary>
        /// <param name="categoryService"> service of category </param>
        public CategoryValidation(ICategoryService categoryService)
        {
            RuleFor(reg => reg.Name).NotEmpty()
                .MaximumLength(100)
                .Must((reg, n) => !categoryService.IsCategoryNameExist(reg.Name))
                .When(reg => reg.Id == 0)
                .WithMessage(string.Format(MessageResource.Exist, CategoryResource.lblCategoryName));

            RuleFor(reg => reg.CategoryOptions)
                .Must((reg, n) => categoryService.CheckCategoryOption(reg.ParentCategoryId, reg.CategoryOptions))
                .When(reg => reg.Id == 0 && reg.CategoryOptions != null)
                .WithMessage(string.Format(MessageResource.InvalidInformation));
        }
    }
}
