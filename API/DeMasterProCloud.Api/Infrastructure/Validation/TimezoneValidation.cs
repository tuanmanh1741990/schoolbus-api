﻿using FluentValidation;
using DeMasterProCloud.DataModel.Timezone;
using DeMasterProCloud.Service;
using DeMasterProCloud.Common.Resources;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Timezone validation
    /// </summary>
    public class TimezoneValidation : AbstractValidator<TimezoneModel>
    {
        /// <summary>
        /// Timezone validation
        /// </summary>
        /// <param name="timezoneService"></param>
        public TimezoneValidation(ITimezoneService timezoneService)
        {
            RuleFor(reg => reg.Name).NotEmpty()
                .MaximumLength(50)
                .Must((reg, strings) => !timezoneService.IsExistedName(reg.Id, reg.Name))
                .WithMessage(reg => string.Format(MessageResource.Exist, reg.Name));
        }
    }
}
