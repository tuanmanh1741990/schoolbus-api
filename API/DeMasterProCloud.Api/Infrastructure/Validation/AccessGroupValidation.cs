﻿using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.Service;
using FluentValidation;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// AccessGroup validation
    /// </summary>
    public class AccessGroupValidation : AbstractValidator<AccessGroupModel>
    {
        /// <summary>
        /// AccessGroup validation
        /// </summary>
        /// <param name="accessGroupService"></param>
        public AccessGroupValidation(IAccessGroupService accessGroupService)
        {
            RuleFor(reg => reg.Name).NotEmpty()
                .Must((reg, c) => !accessGroupService.HasExistName(reg.Id, reg.Name))
                .WithMessage(string.Format(MessageResource.Exist, AccessGroupResource.lblAccessGroupName));
        }
    }
}
