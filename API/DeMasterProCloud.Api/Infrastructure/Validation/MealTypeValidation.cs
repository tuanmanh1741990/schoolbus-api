﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using FluentValidation;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Service;
using DeMasterProCloud.DataModel.MealType;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Validation for MealTypeModel
    /// </summary>
    public class MealTypeValidation : AbstractValidator<MealTypeModel>
    {
        /// <summary>
        /// Validation for MealTypeModel
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="mealTypeService"></param>
        public MealTypeValidation(IConfiguration configuration, IMealTypeService mealTypeService)
        {
            RuleFor(reg => reg.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage(CanteenResource.msgNameIsNull);

            RuleFor(reg => reg.Code)
                .NotEmpty()
                .WithMessage(CanteenResource.msgMealTypeCodeIsNull)
                .Must((reg, c) => mealTypeService.CheckCodeIsValid(reg.Code))
                .WithMessage(CanteenResource.msgDuplicatedCode);
        }
    }
}
