﻿using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Building;
using DeMasterProCloud.Service;
using FluentValidation;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Building validation
    /// </summary>
    public class BuildingValidation : AbstractValidator<BuildingModel>
    {
        /// <summary>
        /// Building validation.
        /// </summary>
        /// <param name="buildingService"></param>
        public BuildingValidation(IBuildingService buildingService)
        {
            RuleFor(reg => reg.Name)
                .NotEmpty()
                .WithMessage(reg => string.Format(MessageResource.MessageCanNotBlank, BuildingResource.lblBuildingName))
                .MaximumLength(50)
                .Must((reg, strings) => !buildingService.IsExistedBuildingName(reg.Id,reg.Name))
                .WithMessage(reg => string.Format(MessageResource.Exist, reg.Name));
        }

    }
}
