﻿using FluentValidation;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.Service;

namespace DeMasterProCloud.Api.Infrastructure.Validation
{
    /// <summary>
    /// Department validation
    /// </summary>
    public class DepartmentValidation : AbstractValidator<DepartmentModel>
    {
        /// <summary>
        /// Department validation
        /// </summary>
        /// <param name="departmentService"></param>
        public DepartmentValidation(IDepartmentService departmentService)
        {
            RuleFor(reg => reg.Name).NotEmpty()
                .MaximumLength(100)
                .Must((reg, n) => !departmentService.IsDepartmentNameExist(reg))
                .WithMessage(string.Format(MessageResource.Exist, DepartmentResource.lblDepartmentName));

            RuleFor(reg => reg.Number).NotEmpty()
                .MaximumLength(50)
                .Must((reg, n) => !departmentService.IsDepartmentNumberExist(reg))
                .WithMessage(string.Format(MessageResource.Exist, DepartmentResource.lblDepartmentNumber));
            RuleFor(reg => reg.Number).NotEmpty()
                .MaximumLength(20);
            RuleFor(reg => reg.Name).NotEmpty()
                .MaximumLength(30);
            
                
                
        }
    }
}
