using AutoMapper;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.WorkingModel;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Working type mapping
    /// </summary>
    public class WorkingTypeMapping : Profile
    {
        /// <summary>
        /// Working type mapping
        /// </summary>
        public WorkingTypeMapping()
        {

            CreateMap<WorkingModel, WorkingType>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.CompanyId, opt => opt.MapFrom(src => src.CompanyId))
                .ForMember(dest => dest.WorkingDay, opt => opt.MapFrom(src => src.WorkingDay))
                .ForMember(dest => dest.IsDefault, opt => opt.MapFrom(src => src.IsDefault));
        }
    }
}