using AutoMapper;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Attendance;
using DeMasterProCloud.DataModel.WorkingModel;


namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    public class AttendanceMapping : Profile
    {
        public AttendanceMapping()
        {
            CreateMap<AttendanceModel, Attendance>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type));
        }
    }
}