﻿using AutoMapper;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.AccessGroupDevice;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// AccessGroupDevice Mapping
    /// </summary>
    public class AccessGroupDeviceMapping : Profile
    {
        /// <summary>
        /// AccessGroupDevice mapping
        /// </summary>
        public AccessGroupDeviceMapping()
        {
            CreateMap<AccessGroupDevice, AccessGroupDeviceDoor>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.IcuId))
                .ForMember(dest => dest.DeviceAddress, opt => opt.MapFrom(src => src.Icu.DeviceAddress))
                .ForMember(dest => dest.DoorName, opt => opt.MapFrom(src => src.Icu.Name))
                .ForMember(dest => dest.TzId, opt => opt.MapFrom(src => src.Tz.Id))
                .ForMember(dest => dest.Timezone, opt => opt.MapFrom(src => src.Tz.Name))
                .ForMember(dest => dest.Building, opt => opt.MapFrom(src => src.Icu.Building.Name));
        }
    }
}
