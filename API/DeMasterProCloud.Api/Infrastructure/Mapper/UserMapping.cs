﻿using System;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Service.Protocol;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Config mapping for user
    /// </summary>
    public class UserMapping : Profile
    {
        /// <summary>
        /// ctor user mapping
        /// </summary>
        public UserMapping()
        {
            CreateMap<User, UserListModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Department,
                    opt => opt.MapFrom(src => src.Department.DepartName))
                .ForMember(dest => dest.EmployeeNo, opt => opt.MapFrom(src => src.EmpNumber))
                .ForMember(dest => dest.ExpiredDate,
                    opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()))
            //.ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
            //.ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
            ////.ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription()))
            .ForMember(dest => dest.AccessGroup, opt => opt.MapFrom(src => src.AccessGroup.Name));

            CreateMap<Visit, User>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar));

            CreateMap<UserModel, User>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PermissionType, opt => opt.MapFrom(src => src.PermissionType))
                //.ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Sex, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.KeyPadPw, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.Password)
                    ? Encryptor.Encrypt(src.Password, ApplicationVariables.Configuration[Constants.Settings.EncryptKey])
                    : src.Password))
                .ForMember(dest => dest.PassType, opt => opt.MapFrom(src => src.CardType))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.OfficePhone))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.DepartmentId))
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.CardId) ? src.CardId.Trim() : src.CardId))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                //.ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.UserCode) ? string.Format("%06d",src.Id) : src.UserCode))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone))
                .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Note))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.WorkingTypeId, opt => opt.MapFrom(src => src.WorkingTypeId))
                .ForMember(dest => dest.WorkType, opt => opt.MapFrom(src => src.WorkType))
                //.ForMember(dest => dest.Avatar, opt =>
                //{
                //    opt.Condition(m => !string.IsNullOrEmpty(m.Avatar));
                //    opt.MapFrom(src => System.Text.Encoding.UTF8.GetBytes(src.Avatar));
                //})
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
                .Include<UserDataModel, User>();
            CreateMap<UserDataModel, User>();

            CreateMap<User, UserModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => src.PassType))
                //.ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Sex))
                .ForMember(dest => dest.Password, opt =>
                {
                    opt.Condition(src => !string.IsNullOrEmpty(src.KeyPadPw));
                    opt.MapFrom(src => Encryptor.Decrypt(src.KeyPadPw,
                            ApplicationVariables.Configuration[Constants.Settings.EncryptKey]));
                })
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.OfficePhone))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.DepartmentId))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.ToSettingDateString()))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone))
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Remarks))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay.ToSettingDateString()))
                .ForMember(dest => dest.WorkingTypeId, opt => opt.MapFrom(src => src.WorkingTypeId))
                .ForMember(dest => dest.WorkType, opt => opt.MapFrom(src => src.WorkType))
                //.ForMember(dest => dest.Avatar, opt =>
                //{
                //    opt.Condition(src => src.Avatar != null);
                //    opt.MapFrom(src => System.Text.Encoding.UTF8.GetString(src.Avatar));
                //})
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
                .Include<User, UserDataModel>();
            CreateMap<User, UserDataModel>();


            CreateMap<UserImportExportModel, User>()
                //.ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode.Value))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName.Value))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName.Value))
                .ForMember(dest => dest.Sex, opt => opt.MapFrom(src => src.Sex.Value))
                .ForMember(dest => dest.KeyPadPw, opt =>
                {
                    //opt.Condition(m => !string.IsNullOrEmpty(m.KeyPadPassword.Value));
                    //opt.MapFrom(src => Encryptor.Encrypt(src.KeyPadPassword.Value,
                    //    ApplicationVariables.Configuration[Constants.Settings.EncryptKey]));
                    opt.MapFrom(src => string.Empty);
                })
                //.ForMember(dest => dest.IssuedDate, opt => opt.MapFrom(src => src.IssuedDate.Value))
                .ForMember(dest => dest.IssuedDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position.Value))
                .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.CompanyPhone.Value))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode.Value))
                .ForMember(dest => dest.Job, opt => opt.MapFrom(src => src.Job.Value))
                .ForMember(dest => dest.Avatar, opt => opt.Ignore())
                .ForMember(dest => dest.DepartmentId, opt => opt.Ignore())
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId.Value))
                .ForMember(dest => dest.EmpNumber, opt => opt.MapFrom(src => src.EmployeeNumber))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.Value))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.Value))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Value))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City.Value))
                .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone.Value))
                .ForMember(dest => dest.Responsibility,
                    opt => opt.MapFrom(src => src.Responsibility.Value))
                .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Remarks.Value))
                .ForMember(dest => dest.AccessGroupId, opt =>
                {
                    //opt.MapFrom(src => src.AccessGroupName.Value);

                    opt.MapFrom(src => src.AccessGroupId);
                })
                //.ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount.Value))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard.Value));
            //.ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.CardStatus.Value));
            CreateMap<User, UserForAccessGroup>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
                .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => src.Department.DepartName));

            CreateMap<User, UnAssignUserForAccessGroup>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId ?? ""))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
                .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => src.Department.DepartName))
                .ForMember(dest => dest.AccessGroupName, opt => opt.MapFrom(src => src.AccessGroup.Name));

            CreateMap<User, AccessibleUserModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId ?? ""))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.DepartName))
                .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => src.EmpNumber))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()));
            //.ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => ((CardStatus) src.CardStatus).GetDescription()));

            CreateMap<UserDetail, SendUserDetail>()
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpireDate))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate))
                .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => Helpers.MaskKeyPadPw(src.Password)))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.AdminFlag))
                .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.EmployeeNumber) ? src.EmployeeNumber : string.Empty))
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.DepartmentName) ? src.DepartmentName : string.Empty))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.CardStatus,
                    opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription()));

            CreateMap<User, SendUserDetail>()
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.ExpireDate,
                    opt => opt.MapFrom(src => src.ExpiredDate.Value.ToString(Constants.DateTimeFormat.DdMdYyyyFormat)))
                .ForMember(dest => dest.EffectiveDate,
                    opt => opt.MapFrom(src =>
                        src.EffectiveDate.Value.ToString(Constants.DateTimeFormat.DdMdYyyyFormat)))
                //.ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
                .ForMember(dest => dest.Password,
                    opt => opt.MapFrom(src => Helpers.MaskAndDecrytorKeyPadPw(src.KeyPadPw)))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
                .ForMember(dest => dest.EmployeeNumber,
                    opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.EmpNumber) ? src.EmpNumber : string.Empty))
                .ForMember(dest => dest.Department,
                    opt => opt.MapFrom(src =>
                        !string.IsNullOrEmpty(src.Department.DepartNo) ? src.Department.DepartName : string.Empty))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName));
                //.ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription()));

            CreateMap<User, UserMasterCardModelDetail>()
                //.ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.Card.))

                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName));

            CreateMap<SendUserDetail, UserMasterCardModelDetail>()
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName));



            CreateMap<User, UserArmyListModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => src.Department.DepartName))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()))
                .ForMember(dest => dest.AccessGroupName, opt => opt.MapFrom(src => src.AccessGroup.Name));


            CreateMap<UserArmyModel, User>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PermissionType, opt => opt.MapFrom(src => src.PermissionType))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Sex, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.KeyPadPw, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.Password)
                    ? Encryptor.Encrypt(src.Password, ApplicationVariables.Configuration[Constants.Settings.EncryptKey])
                    : src.Password))
                .ForMember(dest => dest.PassType, opt => opt.MapFrom(src => src.CardType))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.OfficePhone))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.DepartmentId))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone))
                .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Note))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.WorkingTypeId, opt => opt.MapFrom(src => src.WorkingTypeId))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
                .Include<UserArmyDataModel, User>();
            CreateMap<UserArmyDataModel, User>();



            CreateMap<UserArmyModel, UserArmy>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.MilitaryNumber, opt => opt.MapFrom(src => src.MilitaryNumber))
                .ForMember(dest => dest.SecretIssuanceBasis, opt => opt.MapFrom(src => src.SecretIssuanceBasis))
                .ForMember(dest => dest.BackGroundCheckNumber, opt => opt.MapFrom(src => src.BackGroundCheckNumber))
                .ForMember(dest => dest.EnlistmentDate, opt => opt.MapFrom(src => src.EnlistmentDate))
                .ForMember(dest => dest.WorkType, opt => opt.MapFrom(src => src.WorkType))
                .Include<UserArmyDataModel, UserArmy>();
            CreateMap<UserArmyDataModel, UserArmy>();


            CreateMap<UserArmyModel, UserModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PermissionType, opt => opt.MapFrom(src => src.PermissionType))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password))
                .ForMember(dest => dest.PasswordCheck, opt => opt.MapFrom(src => src.PasswordCheck))
                .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => src.CardType))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.OfficePhone))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.DepartmentId))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone))
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Note))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.WorkingTypeId, opt => opt.MapFrom(src => src.WorkingTypeId))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
                .ForMember(dest => dest.CategoryOptionIds, opt => opt.MapFrom(src => src.CategoryOptionIds))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.ApplyReason, opt => opt.MapFrom(src => src.ApplyReason))
                //.ForMember(dest => dest.WorkType, opt => opt.MapFrom(src => src.WorkType))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.CardList, opt => opt.MapFrom(src => src.CardList))
                .Include<UserArmyDataModel, UserDataModel>();
            CreateMap<UserArmyDataModel, UserDataModel>();




            CreateMap<User, UserArmyModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => src.PassType))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Sex))
                .ForMember(dest => dest.Password, opt =>
                {
                    opt.Condition(src => !string.IsNullOrEmpty(src.KeyPadPw));
                    opt.MapFrom(src => Encryptor.Decrypt(src.KeyPadPw,
                            ApplicationVariables.Configuration[Constants.Settings.EncryptKey]));
                })
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.OfficePhone))
                .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
                .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.DepartmentId))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
                .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()))
                .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.ToSettingDateString()))
                .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone))
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Remarks))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay.ToSettingDateString()))
                .ForMember(dest => dest.WorkingTypeId, opt => opt.MapFrom(src => src.WorkingTypeId))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
                .Include<User, UserArmyDataModel>();
            CreateMap<User, UserArmyDataModel>();
        }
    }
}
