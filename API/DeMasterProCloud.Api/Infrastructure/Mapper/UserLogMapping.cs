﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.UserLog;
using DeMasterProCloud.Service.Protocol;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Define usser log mapping
    /// </summary>
    public class UserLogMapping : Profile
    {
        /// <summary>
        /// Define usser log mapping ( UserLog to UserProtocolDetailData )
        /// </summary>
        public UserLogMapping()
        {
            CreateMap<UserLog, UserProtocolDetailData>()
                //.ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => src.User.UserCode))
                //.ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.User.IssueCount))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FirstName + " " + src.User.LastName))
                .ForMember(dest => dest.AdminFlag, opt => opt.MapFrom(src => src.User.IsMasterCard ? 1 : 0))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.DepartmentName,
                    opt => opt.MapFrom(src => src.User.Department.DepartName))
                .ForMember(dest => dest.EffectiveDate,
                    opt => opt.MapFrom(src =>
                        src.EffectiveDate.HasValue
                            ? src.EffectiveDate.Value.ToString(Constants.DateTimeFormat.DdMMyyyy)
                            : null))
                .ForMember(dest => dest.ExpireDate,
                    opt => opt.MapFrom(src =>
                        src.ExpiredDate.HasValue
                            ? src.ExpiredDate.Value.ToString(Constants.DateTimeFormat.DdMMyyyy)
                            : null))
                .ForMember(dest => dest.Password,
                    opt => opt.MapFrom(src =>
                        !string.IsNullOrEmpty(src.User.KeyPadPw)
                            ? Encryptor.Decrypt(src.User.KeyPadPw,
                                ApplicationVariables.Configuration[Constants.Settings.EncryptKey])
                            : ""))
                //.ForMember(dest => dest.AntiPassBack, opt => opt.MapFrom(src => src.User.CardStatus))
                .ForMember(dest => dest.Timezone, opt => opt.MapFrom(src => src.TzPosition));
        }
    }
}
