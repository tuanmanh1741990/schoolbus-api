﻿using System.Web;
using AutoMapper;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Building;
using DeMasterProCloud.DataModel.Category;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Create mapping for category
    /// </summary>
    public class CategoryMapping : Profile
    {
        /// <summary>
        /// category mapping.
        /// </summary>
        public CategoryMapping()
        {
            CreateMap<Category, CategoryModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ParentCategoryId, opt => opt.MapFrom(src => src.ParentCategoryId));

            CreateMap<CategoryModel, Category>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ParentCategoryId, opt => opt.MapFrom(src => src.ParentCategoryId));

            CreateMap<CategoryOption, CategoryOptionModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.CategoryId))
                .ForMember(dest => dest.ParentOptionId, opt => opt.MapFrom(src => src.ParentOptionId));

            CreateMap<CategoryOptionModel, CategoryOption>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.CategoryId))
                .ForMember(dest => dest.ParentOptionId, opt => opt.MapFrom(src => src.ParentOptionId));
        }
    }
}
