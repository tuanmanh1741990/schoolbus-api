﻿using System;
using System.Linq;
using System.Threading;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.EventLog;
using DeMasterProCloud.DataModel.Visit;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.Protocol;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Mapping define for EventLog
    /// </summary>
    public class EvenLogMapping : Profile
    {

        /// <summary>
        /// Ctor for EvenLogMapping
        /// </summary>
        public EvenLogMapping()
        {
            CreateMap<EventLog, EventLogListModel>()
                .ForMember(dest => dest.IcuId, opt => opt.MapFrom(src => src.IcuId))
                .ForMember(dest => dest.AccessTime,
                    opt => opt.MapFrom(src => src.EventTime.ToSettingDateTimeStringLocal()))
                //.ForMember(dest => dest.UserCode,
                //    opt => opt.MapFrom(src => src.User != null ? (int?) src.User.UserCode : null))
                .ForMember(dest => dest.UserName,
                    opt => opt.MapFrom(src => src.User != null ? src.User.FirstName : null))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(/*src => src.User != null ? src.User.CardId : null*/src => src.CardId))
                .ForMember(dest => dest.Device,
                    opt => opt.MapFrom(src => src.Icu != null ? src.Icu.DeviceAddress : null))
                .ForMember(dest => dest.DoorName, opt => opt.MapFrom(src => src.DoorName))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.CardType))
                .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => ((CardType) src.CardType).GetDescription()))
                .ForMember(dest => dest.InOut, opt => opt.MapFrom(src => Constants.AntiPass.Contains(src.Antipass)
                    ? ((Antipass) Enum.Parse(typeof(Antipass), src.Antipass)).GetDescription()
                    : EventLogResource.lblUnknown))
                .ForMember(dest => dest.EventDetail,
                    opt => opt.MapFrom(src => ((EventType) src.EventType).GetDescription()));


            CreateMap<EventLog, EventLogReportListModel>()
                .ForMember(dest => dest.AccessTime,
                    opt => opt.MapFrom(src => src.EventTime.ToString(ApplicationVariables.Configuration[
                            Constants.DateTimeServerFormat + ":" + Thread.CurrentThread.CurrentCulture.Name])))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.IsVisit
                    ? src.Visit != null && src.Visit.BirthDay != null ? src.Visit.BirthDay.ToSettingDateString() : ""
                    : src.User != null && src.User.BirthDay != null ? src.User.BirthDay.ToSettingDateString() : ""))
                .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => src.IsVisit
                    ? src.Visit != null ? src.Visit.VisiteeEmpNumber : ""
                    : src.User != null ? src.User.EmpNumber : ""))
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.IsVisit
                    ? src.Visit != null ? src.Visit.VisitorDepartment : ""
                    : src.User != null && src.User.Department != null ? src.User.Department.DepartName : ""))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.DeviceAddress, opt => opt.MapFrom(src => src.Icu != null ? src.Icu.DeviceAddress : ""))
                .ForMember(dest => dest.DoorName, opt => opt.MapFrom(src => src.DoorName))
                .ForMember(dest => dest.Building, opt => opt.MapFrom(src => src.Icu != null && src.Icu.Building != null ? src.Icu.Building.Name : ""))
                .ForMember(dest => dest.InOut, opt => opt.MapFrom(src => Constants.AntiPass.Contains(src.Antipass)
                    ? ((Antipass)Enum.Parse(typeof(Antipass), src.Antipass)).GetDescription()
                    : "" ))
                .ForMember(dest => dest.Action, opt => opt.MapFrom(src => src.Antipass))
                .ForMember(dest => dest.IcuId, opt => opt.MapFrom(src => src.IcuId))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId != null ? src.UserId : null))
                .ForMember(dest => dest.VisitId, opt => opt.MapFrom(src => src.VisitId))
                .ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.User != null ? src.User.UserCode : ""))
                .ForMember(dest => dest.EventDetail, opt => opt.MapFrom(src => ((EventType)src.EventType).GetDescription()))
                .ForMember(dest => dest.CardType, opt => opt.MapFrom(src => ((CardType)src.CardType).GetDescription()))
                .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
                .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription() ))
                /*.ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => ((PassType)src.CardType).GetDescription() ))*/;


            CreateMap<EventLog, VisitReportModel>()
                .ForMember(dest => dest.AccessTime,
                    opt => opt.MapFrom(src => src.EventTime.ToSettingDateTimeStringLocal()))
                .ForMember(dest => dest.UserName,
                    opt => opt.MapFrom(src => src.Visit != null ? src.Visit.VisitorName : null))
                .ForMember(dest => dest.BirthDay,
                    opt => opt.MapFrom(src => src.Visit != null ? src.Visit.BirthDay.ToSettingDateString() : null))
                .ForMember(dest => dest.Department,
                    opt => opt.MapFrom(src => src.Visit != null ? src.Visit.VisitorDepartment : null))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.Device,
                    opt => opt.MapFrom(src => src.Icu != null ? src.Icu.DeviceAddress : null))
                .ForMember(dest => dest.DoorName, opt => opt.MapFrom(src => src.DoorName))
                //Building Name...
                .ForMember(dest => dest.InOut, opt => opt.MapFrom(src => Constants.AntiPass.Contains(src.Antipass)
                    ? ((Antipass) Enum.Parse(typeof(Antipass), src.Antipass)).GetDescription()
                    : EventLogResource.lblUnknown))
                .ForMember(dest => dest.EventDetail,
                    opt => opt.MapFrom(src => ((EventType) src.EventType).GetDescription()))
                .ForMember(dest => dest.VisitId,
                    opt => opt.MapFrom(src => src.VisitId))
                .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount));
                //Card Status...
                




            CreateMap<EventLog, SendEventLogDetailData>()
                .ForMember(dest => dest.IcuId, opt => opt.MapFrom(src => src.IcuId))
                .ForMember(dest => dest.AccessTime,
                    opt => opt.MapFrom(src => src.EventTime.ToSettingDateTimeString()))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.Device,
                    opt => opt.MapFrom(src => src.Icu != null ? src.Icu.DeviceAddress : null))
                .ForMember(dest => dest.DoorName,
                    opt => opt.MapFrom(src => src.Icu != null ? src.Icu.Name : null))
                .ForMember(dest => dest.EventDetailCode,
                    opt => opt.MapFrom(src => src.EventType))
                .ForMember(dest => dest.EventDetail,
                    opt => opt.MapFrom(src => ((EventType) src.EventType).GetDescription()));
        }
    }
}