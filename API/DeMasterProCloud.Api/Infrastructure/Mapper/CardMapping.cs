﻿using System;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.DataModel.Visit;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Config mapping for Card
    /// </summary>
    public class CardMapping : Profile
    {
        /// <summary>
        /// ctor card mapping
        /// </summary>
        public CardMapping()
        {

            CreateMap<CardModel, Card>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
                .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.CardStatus))
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Description));

            CreateMap<Card, CardModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
                .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.CardStatus))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Note));



            //CreateMap<Visit, VisitDataModel>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.VisitType, opt => opt.MapFrom(src => src.VisitType))
            //    .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
            //    .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
            //    .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
            //    .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
            //    .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
            //    .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
            //    .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
            //    .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
            //    .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
            //    .ForMember(dest => dest.VisiteeDepartment, opt => opt.MapFrom(src => src.VisiteeDepartment))
            //    .ForMember(dest => dest.VisiteeEmpNumber, opt => opt.MapFrom(src => src.VisiteeEmpNumber))
            //    .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
            //    .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
            //    .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
            //    .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.VisitingCardState))
            //    .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId1))
            //    .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId2))
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId));

            //CreateMap<Visit, VisitListModel>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.ApplyDate,
            //        opt => opt.MapFrom(src => src.ApplyDate.Value.ToString(Constants.DateTimeFormat.YyyyMMdd)))
            //    .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
            //    .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
            //    .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
            //    .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
            //    .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
            //    .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
            //    .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
            //    .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
            //    .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
            //    .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
            //    .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
            //    .ForMember(dest => dest.Approver1, opt => opt.MapFrom(src => src.ApproverId1.ToString()))
            //    .ForMember(dest => dest.Approver2, opt => opt.MapFrom(src => src.ApproverId2.ToString()))
            //    .ForMember(dest => dest.RejectReason, opt => opt.MapFrom(src => src.RejectReason))
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId));




            //CreateMap<UserDataModel, User>();

            //CreateMap<User, UserModel>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    //.ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode))
            //    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
            //    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
            //    .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Sex))
            //    .ForMember(dest => dest.KeyPadPassword, opt =>
            //    {
            //        opt.Condition(src => !string.IsNullOrEmpty(src.KeyPadPw));
            //        opt.MapFrom(src => Encryptor.Decrypt(src.KeyPadPw,
            //                ApplicationVariables.Configuration[Constants.Settings.EncryptKey]));
            //    })
            //    .ForMember(dest => dest.IssuedDate, opt => opt.MapFrom(src => src.IssuedDate.ToSettingDateString()))
            //    .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
            //    .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.OfficePhone))
            //    .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode))
            //    .ForMember(dest => dest.Job, opt => opt.MapFrom(src => src.Job))
            //    .ForMember(dest => dest.Avatar, opt =>
            //    {
            //        opt.Condition(src => src.Avatar != null);
            //        opt.MapFrom(src => System.Text.Encoding.UTF8.GetString(src.Avatar));
            //    })
            //    .ForMember(dest => dest.DepartmentId, opt => opt.MapFrom(src => src.DepartmentId))
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => src.EmpNumber))
            //    .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()))
            //    .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.ToSettingDateString()))
            //    .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality))
            //    .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
            //    .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone))
            //    .ForMember(dest => dest.Responsibility,
            //        opt => opt.MapFrom(src => src.Responsibility))
            //    .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Remarks))
            //    .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
            //    .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => Convert.ToInt32(src.IssueCount)))
            //    .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
            //    .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.CardStatus))
            //    .Include<User, UserDataModel>();
            //CreateMap<User, UserDataModel>();


            //CreateMap<UserImportExportModel, User>()
            //    //.ForMember(dest => dest.UserCode, opt => opt.MapFrom(src => src.UserCode.Value))
            //    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName.Value))
            //    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName.Value))
            //    .ForMember(dest => dest.Sex, opt => opt.MapFrom(src => src.Sex.Value))
            //    .ForMember(dest => dest.KeyPadPw, opt =>
            //    {
            //        //opt.Condition(m => !string.IsNullOrEmpty(m.KeyPadPassword.Value));
            //        //opt.MapFrom(src => Encryptor.Encrypt(src.KeyPadPassword.Value,
            //        //    ApplicationVariables.Configuration[Constants.Settings.EncryptKey]));
            //        opt.MapFrom(src => string.Empty);
            //    })
            //    //.ForMember(dest => dest.IssuedDate, opt => opt.MapFrom(src => src.IssuedDate.Value))
            //    .ForMember(dest => dest.IssuedDate, opt => opt.MapFrom(src => DateTime.Now))
            //    .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position.Value))
            //    .ForMember(dest => dest.OfficePhone, opt => opt.MapFrom(src => src.CompanyPhone.Value))
            //    .ForMember(dest => dest.PostCode, opt => opt.MapFrom(src => src.PostCode.Value))
            //    .ForMember(dest => dest.Job, opt => opt.MapFrom(src => src.Job.Value))
            //    .ForMember(dest => dest.Avatar, opt => opt.Ignore())
            //    .ForMember(dest => dest.DepartmentId, opt => opt.Ignore())
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId.Value))
            //    .ForMember(dest => dest.EmpNumber, opt => opt.MapFrom(src => src.EmployeeNumber))
            //    .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.Value))
            //    .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.Value))
            //    .ForMember(dest => dest.Nationality, opt => opt.MapFrom(src => src.Nationality.Value))
            //    .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City.Value))
            //    .ForMember(dest => dest.HomePhone, opt => opt.MapFrom(src => src.HomePhone.Value))
            //    .ForMember(dest => dest.Responsibility,
            //        opt => opt.MapFrom(src => src.Responsibility.Value))
            //    .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Remarks.Value))
            //    .ForMember(dest => dest.AccessGroupId, opt =>
            //    {
            //        //opt.MapFrom(src => src.AccessGroupName.Value);

            //        opt.MapFrom(src => src.AccessGroupId);
            //    })
            //    .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount.Value))
            //    .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard.Value))
            //    .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.CardStatus.Value));
            //CreateMap<User, UserForAccessGroup>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
            //    .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => src.Department.DepartName));

            //CreateMap<User, UnAssignUserForAccessGroup>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId ?? ""))
            //    .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
            //    .ForMember(dest => dest.DepartmentName, opt => opt.MapFrom(src => src.Department.DepartName))
            //    .ForMember(dest => dest.AccessGroupName, opt => opt.MapFrom(src => src.AccessGroup.Name));

            //CreateMap<User, AccessibleUserModel>()
            //    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId ?? ""))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
            //    .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.DepartName))
            //    .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => src.EmpNumber))
            //    .ForMember(dest => dest.ExpiredDate, opt => opt.MapFrom(src => src.ExpiredDate.ToSettingDateString()))
            //    .ForMember(dest => dest.CardStatus,
            //        opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription()));

            //CreateMap<UserDetail, SendUserDetail>()
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpireDate))
            //    .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate))
            //    .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
            //    .ForMember(dest => dest.Password, opt => opt.MapFrom(src => Helpers.MaskKeyPadPw(src.Password)))
            //    .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.AdminFlag))
            //    .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.EmployeeNumber) ? src.EmployeeNumber : string.Empty))
            //    .ForMember(dest => dest.Department, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.DepartmentCode) ? src.DepartmentCode : string.Empty))
            //    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
            //    .ForMember(dest => dest.CardStatus,
            //        opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription()));

            //CreateMap<User, SendUserDetail>()
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.ExpireDate, opt => opt.MapFrom(src => src.ExpiredDate.Value.ToString(Constants.DateTimeFormat.DdMdYyyyFormat)))
            //    .ForMember(dest => dest.EffectiveDate, opt => opt.MapFrom(src => src.EffectiveDate.Value.ToString(Constants.DateTimeFormat.DdMdYyyyFormat)))
            //    .ForMember(dest => dest.IssueCount, opt => opt.MapFrom(src => src.IssueCount))
            //    .ForMember(dest => dest.Password, opt => opt.MapFrom(src => Helpers.MaskAndDecrytorKeyPadPw(src.KeyPadPw)))
            //    .ForMember(dest => dest.IsMasterCard, opt => opt.MapFrom(src => src.IsMasterCard))
            //    .ForMember(dest => dest.EmployeeNumber, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.EmpNumber) ? src.EmpNumber : string.Empty))
            //    .ForMember(dest => dest.Department, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.Department.DepartNo) ? src.Department.DepartNo : string.Empty))
            //    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName))
            //    .ForMember(dest => dest.CardStatus,
            //        opt => opt.MapFrom(src => ((CardStatus)src.CardStatus).GetDescription()));

            //CreateMap<User, UserMasterCardModelDetail>()
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FirstName + " " + src.LastName));

            //CreateMap<SendUserDetail, UserMasterCardModelDetail>()
            //    .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
            //    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName));
        }
    }
}
