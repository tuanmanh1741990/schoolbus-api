﻿using System;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Visit;
using DeMasterProCloud.DataModel.VisitArmy;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// Config mapping for user
    /// </summary>
    public class VisitMapping : Profile
    {
        /// <summary>
        /// ctor user mapping
        /// </summary>
        public VisitMapping()
        {

            CreateMap<VisitModel, Visit>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.VisitType, opt => opt.MapFrom(src => src.VisitType))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
                .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
                .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
                .ForMember(dest => dest.VisiteeDepartment, opt => opt.MapFrom(src => src.VisiteeDepartment))
                .ForMember(dest => dest.VisiteeEmpNumber, opt => opt.MapFrom(src => src.VisiteeEmpNumber))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
                .ForMember(dest => dest.VisitingCardState, opt => opt.MapFrom(src => src.CardStatus))
                .ForMember(dest => dest.ApproverId1, opt => opt.MapFrom(src => src.ApproverId))
                .ForMember(dest => dest.ApproverId2, opt => opt.MapFrom(src => src.ApproverId))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.VisitorEmpNumber, opt => opt.MapFrom(src => src.VisitorEmpNumber))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId));


            CreateMap<Visit, VisitDataModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.VisitType, opt => opt.MapFrom(src => src.VisitType))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay.ToSettingDateString()))
                .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate.ToSettingDateString()))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate.ToSettingDateString()))
                .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
                .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
                .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
                .ForMember(dest => dest.VisiteeDepartment, opt => opt.MapFrom(src => src.VisiteeDepartment))
                .ForMember(dest => dest.VisiteeEmpNumber, opt => opt.MapFrom(src => src.VisiteeEmpNumber))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
                .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.VisitingCardState))
                .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId1))
                .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId2))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.ProcessStatus, opt => opt.MapFrom(src => ((VisitChangeStatusType)src.Status).GetDescription() ))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId));
                

            CreateMap<Visit, VisitListModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ApplyDate,
                    opt => opt.MapFrom(src => src.ApplyDate.ToSettingDateTimeString()))
                .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay.ToSettingDateString()))
                .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate.ToSettingDateString()))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate.ToSettingDateString()))
                .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
                .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
                .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.ProcessStatus, opt => opt.MapFrom(src => ((VisitChangeStatusType)src.Status).GetDescription() ))
                .ForMember(dest => dest.Approver1, opt => opt.MapFrom(src => src.ApproverId1.ToString()))
                .ForMember(dest => dest.Approver2, opt => opt.MapFrom(src => src.ApproverId2.ToString()))
                .ForMember(dest => dest.RejectReason, opt => opt.MapFrom(src => src.RejectReason))
                .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId));

            CreateMap<SystemLog, VisitListHistoryModel>()
               .ForMember(dest => dest.EventDetails, opt => opt.MapFrom(src => src.Content + "\n" + src.ContentDetails));

            CreateMap<EventLog, VisitListHistoryModel>()
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.DoorName, opt => opt.MapFrom(src => src.DoorName.ToString()))
                .ForMember(dest => dest.EventTime, opt => opt.MapFrom(src => src.EventTime.ToString()))
                .ForMember(dest => dest.Antipass, opt => opt.MapFrom(src => Constants.AntiPass.Contains(src.Antipass)
                    ? ((Antipass)Enum.Parse(typeof(Antipass), src.Antipass)).GetDescription()
                    : EventLogResource.lblUnknown));


            // For army
            CreateMap<VisitArmyModel, Visit>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.VisitType, opt => opt.MapFrom(src => src.VisitType))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
                .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
                .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
                .ForMember(dest => dest.VisiteeDepartment, opt => opt.MapFrom(src => src.VisiteeDepartment))
                .ForMember(dest => dest.VisiteeEmpNumber, opt => opt.MapFrom(src => src.VisiteeEmpNumber))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
                .ForMember(dest => dest.VisitingCardState, opt => opt.MapFrom(src => src.CardStatus))
                .ForMember(dest => dest.ApproverId1, opt => opt.MapFrom(src => src.ApproverId))
                .ForMember(dest => dest.ApproverId2, opt => opt.MapFrom(src => src.ApproverId))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.VisitorEmpNumber, opt => opt.MapFrom(src => src.VisitorEmpNumber))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId));

            CreateMap<VisitArmyModel, VisitArmy>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.VisitId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.MilitaryNumber, opt => opt.MapFrom(src => src.MilitaryNumber))
                .ForMember(dest => dest.Relationship, opt => opt.MapFrom(src => src.Relationship))
                .ForMember(dest => dest.BackGroundCheckNumber, opt => opt.MapFrom(src => src.BackGroundCheckNumber))
                .ForMember(dest => dest.Gender, opt => opt.MapFrom(src => src.Gender))
                .ForMember(dest => dest.VisitorRank, opt => opt.MapFrom(src => src.VisitorRank))
                .ForMember(dest => dest.VisiteeRank, opt => opt.MapFrom(src => src.VisiteeRank));

            CreateMap<Visit, VisitArmyDataModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.VisitType, opt => opt.MapFrom(src => src.VisitType))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay.ToSettingDateString()))
                .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate.ToSettingDateString()))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate.ToSettingDateString()))
                .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
                .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
                .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
                .ForMember(dest => dest.VisiteeDepartment, opt => opt.MapFrom(src => src.VisiteeDepartment))
                .ForMember(dest => dest.VisiteeEmpNumber, opt => opt.MapFrom(src => src.VisiteeEmpNumber))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
                .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.VisitingCardState))
                .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId1))
                .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId2))
                .ForMember(dest => dest.CardId, opt => opt.MapFrom(src => src.CardId))
                .ForMember(dest => dest.ProcessStatus, opt => opt.MapFrom(src => ((VisitChangeStatusType)src.Status).GetDescription()))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId));

            CreateMap<VisitArmyModel, VisitModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.VisitorName, opt => opt.MapFrom(src => src.VisitorName))
                .ForMember(dest => dest.VisitorDepartment, opt => opt.MapFrom(src => src.VisitorDepartment))
                .ForMember(dest => dest.VisitorEmpNumber, opt => opt.MapFrom(src => src.VisitorEmpNumber))
                .ForMember(dest => dest.VisitType, opt => opt.MapFrom(src => src.VisitType))
                .ForMember(dest => dest.VisitReason, opt => opt.MapFrom(src => src.VisitReason))
                .ForMember(dest => dest.VisiteeName, opt => opt.MapFrom(src => src.VisiteeName))
                .ForMember(dest => dest.VisiteeSite, opt => opt.MapFrom(src => src.VisiteeSite))
                .ForMember(dest => dest.VisiteeDepartment, opt => opt.MapFrom(src => src.VisiteeDepartment))
                .ForMember(dest => dest.VisiteeEmpNumber, opt => opt.MapFrom(src => src.VisiteeEmpNumber))
                .ForMember(dest => dest.BirthDay, opt => opt.MapFrom(src => src.BirthDay))
                .ForMember(dest => dest.AccessGroupId, opt => opt.MapFrom(src => src.AccessGroupId))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.ApproverId, opt => opt.MapFrom(src => src.ApproverId))
                .ForMember(dest => dest.ApproverId1, opt => opt.MapFrom(src => src.ApproverId1))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
                .ForMember(dest => dest.CardStatus, opt => opt.MapFrom(src => src.CardStatus))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.StartDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => src.EndDate))
                .ForMember(dest => dest.IsDecision, opt => opt.MapFrom(src => src.IsDecision))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position))
                .ForMember(dest => dest.ProcessStatus, opt => opt.MapFrom(src => src.ProcessStatus));


        }
    }
}
