﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.MessageLog;

namespace DeMasterProCloud.Api.Infrastructure.Mapper
{
    /// <summary>
    /// MessageLog mapping
    /// </summary>
    public class MessageLogMapping : Profile
    {
        /// <summary>
        /// MessageLog mapping
        /// </summary>
        public MessageLogMapping()
        {
            CreateMap<MessageLog, MessageLogModel>()
                .ForMember(dest => dest.MsgId, opt => opt.MapFrom(src => src.MsgId))
                .ForMember(dest => dest.Topic, opt => opt.MapFrom(src => src.Topic))
                .ForMember(dest => dest.PayLoad, opt => opt.MapFrom(src => src.PayLoad))
                .ForMember(dest => dest.Status,
                    opt => opt.MapFrom(src => ((MessageStatus) src.Status).GetDescription()))
                .ForMember(dest => dest.PublishTime,
                    opt => opt.MapFrom(src => src.PublishedTime.ToSettingDateTimeString()))
                .ForMember(dest => dest.ResponseTime,
                    opt => opt.MapFrom(src => src.ResponseTime.ToSettingDateTimeString()))
                .ForMember(dest => dest.ProtocolType, opt => opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.GroupMsgId, opt => opt.MapFrom(src => src.GroupMsgId));
        }
    }
}
