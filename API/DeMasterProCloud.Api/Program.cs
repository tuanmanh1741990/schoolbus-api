﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.RabbitMq;
using EasyNetQ.Management.Client;
using EasyNetQ.Management.Client.Model;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace DeMasterProCloud.Api
{
    /// <summary>
    /// Demaster-pro cloud API
    /// </summary>
    public class Program
    {
   
        /// <summary>
        /// This is main
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                var loggerFactory = ApplicationVariables.LoggerFactory = services.GetRequiredService<ILoggerFactory>();
                var configuration = services.GetRequiredService<IConfiguration>();
                var unitOfWork = services.GetRequiredService<IUnitOfWork>();
                var settingService = services.GetRequiredService<ISettingService>();
                DbInitializer.Initialize(unitOfWork, configuration);

                //Init application setting
                //settingService.Set();

                //RabbitMQ consumer registration
                var consumerService = services.GetRequiredService<IConsumerService>();
                consumerService.Register();
            }
            host.Run(); 
        }

        /// <summary>
        /// Create web host builder.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureKestrel((context, options) =>
                {
                    // Set properties and call methods on options
                })
                .UseUrls("http://*:5000")
                .UseIISIntegration()
                .UseSentry()
                .UseStartup<Startup>();
    }
}
