﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using DeMasterProCloud.Api.Infrastructure.Filters;
using DeMasterProCloud.Api.Infrastructure.Middlewares;
using DeMasterProCloud.Api.Infrastructure.Utilities;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Api;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.DataModel.PlugIn;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using DinkToPdf;
using DinkToPdf.Contracts;
using EasyNetQ.Management.Client;
using EasyNetQ.Management.Client.Model;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using RabbitMQ.Client;
using Swashbuckle.AspNetCore.Swagger;
using RequestLocalizationMiddleware = DeMasterProCloud.Api.Infrastructure.Middlewares.RequestLocalizationMiddleware;
using StatusCodes = Microsoft.AspNetCore.Http.StatusCodes;
using User = DeMasterProCloud.DataAccess.Models.User;
using Prometheus;

namespace DeMasterProCloud.Api
{
    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        /// <param name="locOptions"></param>
        public Startup(IHostingEnvironment env, IOptions<RequestLocalizationOptions> locOptions)
        {
            Console.WriteLine(@"Environment: " + env.EnvironmentName);
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            LocOptions = locOptions;

            AppLog.Init();
            //var eventLog = new EventLog();
            //eventLog.Antipass = "Antipass";
            //AppLog.EventSave(eventLog);
            ////var appLog = new AppLog();
            ////var eventLog = new EventLog();
            ////eventLog.Antipass = "Antipass";
            ////eventLog.CardId = "CardId";
            ////appLog.EventSave(eventLog);
            ////appLog.EventSave(eventLog);
        }
        /// <summary>
        /// Loc options
        /// </summary>
        public IOptions<RequestLocalizationOptions> LocOptions;

        /// <summary>
        /// Configuration
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            #region Bind Strongly Type Config

            var jwtSection = Configuration.GetSection(Constants.Settings.JwtSection);
            var jwtOptions = new JwtOptionsModel();
            jwtSection.Bind(jwtOptions);
            services.Configure<JwtOptionsModel>(jwtSection);

            var cultureSection = Configuration.GetSection(Constants.Settings.Cultures);
            var cultureOptions = new List<string>();
            cultureSection.Bind(cultureOptions);
            services.Configure<List<string>>(cultureSection);

            var supportedCultures = new List<CultureInfo>();
            foreach (var culture in cultureOptions)
            {
                supportedCultures.Add(new CultureInfo(culture));
            }
            #endregion

            #region framework services

            // configration for localiztion
            services.AddLocalization(options => options.ResourcesPath = Constants.Settings.ResourcesDir);
            services.Configure<RequestLocalizationOptions>(
                opts =>
                {
                    opts.DefaultRequestCulture = new RequestCulture(cultureOptions[0]);
                    // Formatting numbers, dates, etc.
                    opts.SupportedCultures = supportedCultures;
                    // UI strings that we have localized.
                    opts.SupportedUICultures = supportedCultures;
                    opts.RequestCultureProviders = new List<IRequestCultureProvider>
                    {
                        new QueryStringRequestCultureProvider(),
                        new CookieRequestCultureProvider()
                    };
                });

            // Service for init mapping
            services.AddAutoMapper(typeof(Startup).Assembly);

            // Config authentication
            services.AddAuthentication(options =>
            {
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
            .AddCookie(option =>
            {
                option.ExpireTimeSpan = TimeSpan.FromMinutes(int.Parse(
                    Configuration[Constants.Settings.LoginExpiredTime] ?? Constants.Settings.DefaultExpiredTime));
                option.AccessDeniedPath = Constants.Route.AccessDeniedPage;
            });

            // Config Authorize
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Constants.Policy.SystemAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SystemAdmin).ToString()));

                options.AddPolicy(Constants.Policy.SuperAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SuperAdmin).ToString()));

                options.AddPolicy(Constants.Policy.PrimaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.PrimaryManager).ToString()));

                options.AddPolicy(Constants.Policy.SecondaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SecondaryManager).ToString()));

                options.AddPolicy(Constants.Policy.SystemAndSuperAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SystemAdmin).ToString(),
                        ((short)AccountType.SuperAdmin).ToString()));

                options.AddPolicy(Constants.Policy.SystemAndSuperAndPrimaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SystemAdmin).ToString(),
                        ((short)AccountType.SuperAdmin).ToString(),
                        ((short)AccountType.PrimaryManager).ToString()));

                options.AddPolicy(Constants.Policy.SuperAndPrimaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SuperAdmin).ToString(),
                        ((short)AccountType.PrimaryManager).ToString()));

                options.AddPolicy(Constants.Policy.PrimaryAndSecondaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.PrimaryManager).ToString(),
                        ((short)AccountType.SecondaryManager).ToString()));

                options.AddPolicy(Constants.Policy.SuperAndPrimaryAndSecondaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SuperAdmin).ToString(),
                        ((short)AccountType.PrimaryManager).ToString(),
                        ((short)AccountType.SecondaryManager).ToString()));

                options.AddPolicy(Constants.Policy.SuperAndPrimaryAndSecondaryAdminAndEmployee, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SuperAdmin).ToString(),
                        ((short)AccountType.PrimaryManager).ToString(),
                        ((short)AccountType.SecondaryManager).ToString(),
                        ((short)AccountType.Employee).ToString()));

                options.AddPolicy(Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdmin, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SystemAdmin).ToString(),
                        ((short)AccountType.SuperAdmin).ToString(),
                        ((short)AccountType.PrimaryManager).ToString(),
                        ((short)AccountType.SecondaryManager).ToString()));
                
                options.AddPolicy(Constants.Policy.Employee, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.Employee).ToString()));
                options.AddPolicy(Constants.Policy.PrimaryAndSecondaryAdminAndEmployee, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.PrimaryManager).ToString(),
                        ((short)AccountType.SecondaryManager).ToString(),
                        ((short)AccountType.Employee).ToString()));
                options.AddPolicy(Constants.Policy.SystemAndSuperAndPrimaryAndSecondaryAdminAndEmployee, policy =>
                    policy.RequireClaim(Constants.ClaimName.AccountType,
                        ((short)AccountType.SystemAdmin).ToString(),
                        ((short)AccountType.PrimaryManager).ToString(),
                        ((short)AccountType.SecondaryManager).ToString(),
                        ((short)AccountType.Employee).ToString()));
                //options.AddPolicy("ValidAccount", policy =>
                //            policy.Requirements.Add(new ValidAccountRequirement(new[] { Status.Valid})));

            });

            services.AddHsts(options =>
            {
                options.Preload = true;
                options.IncludeSubDomains = true;
                options.MaxAge = TimeSpan.FromDays(60);
            });

            // Add bearer authentication
            services.AddAuthentication()
                .AddJwtBearer(cfg =>
                {
                    //cfg.RequireHttpsMetadata = false;
                    cfg.SaveToken = true;
                    cfg.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = true,
                        ValidateIssuer = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.SecretKey)),
                        ValidIssuer = jwtOptions.Issuer,
                        ValidAudience = jwtOptions.Issuer,
                        // Validate the token expiry  
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                });
            // Config the db connection string
            services.AddEntityFrameworkNpgsql().AddDbContext<AppDbContext>(options =>
            {
                var connection = Environment.GetEnvironmentVariable(Constants.Settings.DefaultEnvironmentConnection);
                if (string.IsNullOrEmpty(connection))
                {
                    connection = Configuration.GetConnectionString(Constants.Settings.DefaultConnection);
                }
                options.UseNpgsql(connection,
                    sqlOptions =>
                    {
                        sqlOptions.MigrationsAssembly(Constants.Settings.DeMasterProCloudDataAccess);
                        // Configuring Connection Resiliency: 
                        // https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                        sqlOptions.EnableRetryOnFailure(5, TimeSpan.FromSeconds(30), new List<string>());
                        sqlOptions.CommandTimeout(3600);
                    });
                // Changing default behavior when client evaluation occurs to throw. 
                // Default in EF Core would be to log a warning when client evaluation is performed.
                options.ConfigureWarnings(
                    warnings => warnings.Ignore(RelationalEventId.QueryClientEvaluationWarning));
                // Check Client vs. Server evaluation: 
                // https://docs.microsoft.com/en-us/ef/core/querying/client-eval
            });

            // Swagger Tools
            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc(Constants.Swagger.V1, new Info
                {
                    Title = CommonResource.lblSwaggerTitle,
                    Version = Constants.Swagger.V1,
                    Description =
                        CommonResource.lblSwaggerDescription,
                    TermsOfService = CommonResource.lblSwaggerTerm
                });


                // Configure the XML comments file path for the Swagger JSON and UI.

                var xmlFile = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";

                options.IncludeXmlComments(System.IO.Path.Combine(AppContext.BaseDirectory, xmlFile));


                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
                    { "Bearer", Enumerable.Empty<string>() },
                });
            });
            //services.ConfigureSwaggerGen(options =>
            //{
            //    options.OperationFilter<AddParameterOperation>(); //Register File Upload Operation Filter
            //});
            // Cors Config
            services.AddCors(options =>
            {
                options.AddPolicy(Constants.Swagger.CorsPolicy,
                    builder => builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader());
            });
            services.AddLogging(builder => builder.AddConsole());

            // Adds a default in-memory implementation of IDistributedCache
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.Cookie.Name = Constants.AppSession;
                options.IdleTimeout = TimeSpan.FromMinutes(Convert.ToDouble(
                    Configuration[Constants.Settings.ExpiredSessionTime] ?? Constants.Settings.DefaultExpiredTime));
            });
            // Custom service
            services.AddRouting(options => options.LowercaseUrls = true);
            #endregion

            #region Application services
            // Common services
            services.AddScoped<IJwtHandler, JwtHandler>();
            services.AddSingleton<IPasswordHasher<User>, PasswordHasher<User>>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ValidateModelFilter>();
            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();
            services.AddSingleton<IMailService, MailService>();
            services.AddSingleton<IQueueService, QueueService>();

            // Services business
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IDeviceService, DeviceService>();
            services.AddScoped<ICompanyService, CompanyService>();
            services.AddScoped<ITimezoneService, TimezoneService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IHolidayService, HolidayService>();
            services.AddScoped<ISystemLogService, SystemLogService>();
            services.AddScoped<ISettingService, SettingService>();
            services.AddScoped<IAccessGroupService, AccessGroupService>();
            services.AddScoped<IAccessGroupDeviceService, AccessGroupDeviceService>();
            services.AddScoped<IEventLogService, EventLogService>();
            services.AddScoped<IConsumerService, ConsumerService>();
            services.AddScoped<IUnregistedDeviceService, UnregistedDeviceService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<IBuildingService, BuildingService>();
            services.AddScoped<IVisitService, VisitService>();
            services.AddScoped<IDeviceMessageService, DeviceMessageService>();
            services.AddScoped<IPartTimeService, PartTimeService>();
            services.AddScoped<IWorkingService, WorkingService>();
            services.AddScoped<IAttendanceService, AttendanceService>();
            services.AddScoped<IPluginService, PlugInService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<IUserArmyService, UserArmyService>();
            services.AddScoped<ICornerSettingService, CornerSettingService>();
            services.AddScoped<IUserDiscountService, UserDiscountService>();
            services.AddScoped<IMealTypeService, MealTypeService > ();
            services.AddScoped<IMealSettingService, MealSettingService>();
            services.AddScoped<IVisitArmyService, VisitArmyService>();
            services.AddScoped<IExceptionalMealService, ExceptionalMealService>();
            services.AddScoped<IMealEventLogService, MealEventLogService>();

            services.Configure<FormOptions>(x =>
            {
                x.BufferBody = true;
                x.ValueCountLimit = 4096;
                x.MultipartBodyLengthLimit = 52428800;
            });

            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ApiVersionReader = new HeaderApiVersionReader("x-api-version");
            });

            services.AddMvc(options =>
            {
                options.ModelBinderProviders.Insert(0, new ModelBinderProvider());
            })
                .AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.ReferenceLoopHandling =
                        ReferenceLoopHandling.Ignore;
                })
                .AddControllersAsServices()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddSessionStateTempDataProvider()
                .AddFluentValidation(fvc =>
                    fvc.RegisterValidatorsFromAssemblyContaining<Startup>()).AddViewLocalization(
                    LanguageViewLocationExpanderFormat.Suffix,
                    opts => { opts.ResourcesPath = Constants.Settings.ResourcesDir; })
                .AddDataAnnotationsLocalization();

            //var context = new CustomAssemblyLoadContext();
            //if (Environment.OSVersion.Platform == PlatformID.WinCE)
            //{
            //    context.LoadUnmanagedLibrary(Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.dll"));
            //}
            //else if (Environment.OSVersion.Platform == PlatformID.Unix)
            //{
            //    context.LoadUnmanagedLibrary(Path.Combine(Directory.GetCurrentDirectory(), "libwkhtmltox.so"));
            //}

            services.AddSingleton(typeof(IConverter), new SynchronizedConverter(new PdfTools()));
            #endregion
        }

        private bool CheckAccountValidInMessageQueue(string companyCode)
        {
            Console.WriteLine("Start to get connection" + DateTime.Now);
            var initial = new ManagementClient(
                Configuration.GetSection("QueueConnectionSettingsApi:Host").Value,
                Configuration.GetSection("QueueConnectionSettingsApi:UserName").Value,
                Configuration.GetSection("QueueConnectionSettingsApi:Password").Value,
                Int32.Parse(Configuration.GetSection("QueueConnectionSettingsApi:Port").Value)
            );

            var userAsync = initial.GetUserAsync(companyCode);
            try
            {
                if (userAsync.Result != null)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        private Task AddAccountCompanyMessageQueue(string companyCode, ManagementClient initial, Vhost vHost)
        {
            Console.WriteLine("Start to create uer " + companyCode);
            var userInfor = new UserInfo(companyCode, Convert.ToBase64String(Encoding.UTF8.GetBytes(companyCode)));

            // Add tag permission for user
            userInfor.AddTag(Constants.RabbitMq.Permission);

            // Create user info
            var user = initial.CreateUserAsync(userInfor);

            var topicPermissionInfo = new TopicPermissionInfo(user.Result, vHost);
            topicPermissionInfo.SetExchangeType(Constants.RabbitMq.ExChangeType)
                .SetWrite(".*")
                .SetRead(string.Format(Constants.RabbitMq.TopicPermissionNormalUserList, companyCode));
            initial.CreateTopicPermissionAsync(topicPermissionInfo).ConfigureAwait(true);


            var permissionInfo = new PermissionInfo(user.Result, vHost);
            permissionInfo.SetConfigure(".*")
                .SetWrite(".*")
                .SetRead(".*");
            initial.CreatePermissionAsync(permissionInfo).ConfigureAwait(true);
            return Task.CompletedTask;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="configuration"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IConfiguration configuration)
        {
            ApplicationVariables.env = env;
            ApplicationVariables.Configuration = configuration;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler(Constants.Route.ErrorPage);
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseSession();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMetricServer();
            app.UseHttpMetrics();

            app.Use(async (context, next) =>
            {
                await next();
                // Custom 404 error
                if (context.Response.StatusCode == StatusCodes.Status404NotFound
                    && !context.Response.HasStarted)
                {
                    context.Response.ContentType = "application/json";
                    var response = new ApiErrorResult(context.Response.StatusCode);
                    var payload = JsonConvert.SerializeObject(response);
                    await context.Response.WriteAsync(payload);
                }
            });
            var logDir = Path.GetDirectoryName(Configuration[Constants.Logger.LogFile]);
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var listCompany = db.Company.Where(m => !m.IsDeleted).Select(
                        c => c.Code).ToList();
                    foreach (var companyCode in listCompany)
                    {
                        if (!CheckAccountValidInMessageQueue(companyCode))
                        {
                            var initial = new ManagementClient(
                                Configuration.GetSection("QueueConnectionSettingsApi:Host").Value,
                                Configuration.GetSection("QueueConnectionSettingsApi:UserName").Value,
                                Configuration.GetSection("QueueConnectionSettingsApi:Password").Value,
                                Int32.Parse(Configuration.GetSection("QueueConnectionSettingsApi:Port").Value)
                            );
                            var vHost = initial.GetVhostAsync(
                                Configuration.GetSection("QueueConnectionSettingsApi:VirtualHost").Value);
                            AddAccountCompanyMessageQueue(companyCode, initial, vHost.Result);
                        }
                    }
                }
            }

            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var listCompanyId = db.Company.Where(m => !m.IsDeleted).Select(
                        c => c.Id).ToList();
                    if (listCompanyId.Any())
                    {
                        foreach (var companyId in listCompanyId)
                        {
                            Company company = db.Company.Where(c => c.Id == companyId).Single();
                            
                                if (String.IsNullOrEmpty(company.SecretCode))
                                {
                                    System.Security.Cryptography.AesCryptoServiceProvider crypto =
                                        new System.Security.Cryptography.AesCryptoServiceProvider();
                                    crypto.KeySize = Constants.AutoRenew.KeySize;
                                    crypto.BlockSize = Constants.AutoRenew.BlockSize;
                                    crypto.GenerateKey();
                                    byte[] keyGenerated = crypto.Key;
                                    string secretCode = Convert.ToBase64String(keyGenerated);
                                    company.SecretCode = secretCode;
                                    db.SaveChanges();
                                }
                        }
                    }
                }
            }

            // Check company have already have setting default if not create new
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var settings = Configuration.GetSection(Constants.Settings.FileSettings)?.Get<List<FileSetting>>();
                    var listCompanyId = db.Company.Where(a =>!a.IsDeleted).Select(
                        c => c.Id).ToList();
                    if (settings != null && settings.Any())
                    {
                        if (listCompanyId.Count != 0)
                        {
                            foreach (var companyId in listCompanyId)
                            {
                                foreach (var i in settings)
                                {
                                    bool settingCompany = db.Setting.Any(
                                        c => c.CompanyId == companyId && c.Key == i.Key);
                                    if (settingCompany != true)
                                    {
                                        var newSetting = new Setting
                                        {
                                            Key = i.Key,
                                            Value = JsonConvert.SerializeObject(i.Values),
                                            CompanyId = companyId
                                        };
                                        db.Setting.Add(newSetting);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Check timezone on building add default timezone if null
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var buildingList = db.Building.Where(b => !b.IsDeleted).Select(
                        c => c.Id).ToList();
                    foreach (var buildingId in buildingList)
                    {
                        Building building = db.Building.Where(c => c.Id == buildingId).Single();
                        if (building.TimeZone == null)
                        {
                            building.TimeZone = Helpers.GetLocalTimeZone();
                            db.SaveChanges();
                        }
                    }

                }
            }
            
            // Check working type of company is default and create if is null
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var companies = db.Company.Where(m => !m.IsDeleted).Select(
                        c => c.Id).ToList();
                    foreach (var companyId in companies)
                    {
                        WorkingType workingType = db.WorkingType.Where(c => c.CompanyId == companyId && c.IsDefault).FirstOrDefault();
                        if (workingType == null)
                        {
                            var workingTime = new WorkingType
                            {
                                Name = Constants.Attendance.DefaultName,
                                IsDefault = true,
                                CompanyId = companyId,
                                WorkingDay = Constants.Attendance.DefaultWorkingTime
                            };
                            db.WorkingType.Add(workingTime);
                            db.SaveChanges();
                        }
                    }

                }
            }
            
            // Check working type of visit setting is default and create if is null
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var companies = db.Company.Where(m => !m.IsDeleted).Select(
                        c => c.Id).ToList();
                    foreach (var companyId in companies)
                    {
                        VisitSetting visitType = db.VisitSetting.FirstOrDefault(c => c.CompanyId == companyId);
                        if (visitType == null)
                        {
                            var setting = new VisitSetting()
                            {
                                CompanyId = companyId,
                                ApprovalStepNumber = (short) VisitSettingType.NoStep
                            };
                            db.VisitSetting.Add(setting);
                            db.SaveChanges();
                        }
                    }

                }
            }
            
            // Check working type of company is default and create if is null
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var services = serviceScope.ServiceProvider;
                using (var db = services.GetRequiredService<AppDbContext>())
                {
                    var companies = db.Company.Where(m => !m.IsDeleted).Select(
                        c => c.Id).ToList();
                    foreach (var companyId in companies)
                    {
                        PlugIn solution = db.PlugIn.Where(c => c.CompanyId == companyId).FirstOrDefault();
                        if (solution == null)
                        {
                            var plugIns = new PlugIns
                            {
                                AccessControl = true,
                                TimeAttendance = true,
                                VisitManagement = true,
                                CanteenManagement = true,
                                CardIssuing = true,
                                Common = true,
                                QrCode = true,
                                PassCode = true,
                                ScreenMessage = true,

                                ArmyManagement = true,
                            };
                            
                            var plugInsDescription = new PlugInsDescription()
                            {
                                AccessControl = Constants.PlugInValue.AccessControlDescription,
                                TimeAttendance = Constants.PlugInValue.TimeAttendanceDescription,
                                VisitManagement = Constants.PlugInValue.VisitManagementDescription,
                                CanteenManagement = Constants.PlugInValue.CanteenManagementDescription,
                                CardIssuing = Constants.PlugInValue.CardIssuingDescription,
                                Common = Constants.PlugInValue.CommonDescription,
                                QrCode = Constants.PlugInValue.QrCodeDescription,
                                PassCode = Constants.PlugInValue.PassCodeDescription,
                                ScreenMessage = Constants.PlugInValue.ScreenMessageDescription,

                                ArmyManagement = Constants.PlugInValue.ArmyManagementDescription,
                            };
                            var json = JsonConvert.SerializeObject(plugIns);
                            var jsonDescription = JsonConvert.SerializeObject(plugInsDescription);
                            
                            var newSolution = new PlugIn
                            {
                                CompanyId = companyId,
                                PlugIns = json,
                                PlugInsDescription = jsonDescription
                            };
                            db.PlugIn.Add(newSolution);
                            db.SaveChanges();
                        }
                    }

                }
            }

            // Open connection to rabbitmq
            var factory = Common.Infrastructure.QueueHelper.GetConnectionFactory(configuration);
            try
            {

                // Check last status of device when system restart
                using (var serviceScope = app.ApplicationServices.CreateScope())
                {
                    var services = serviceScope.ServiceProvider;
                    //var listDeviceAddress = services.GetService<AppDbContext>().IcuDevice.Select(c => c.DeviceAddress).ToList();
                    var listDeviceAddress = services.GetService<AppDbContext>().IcuDevice.Where(c => !c.IsDeleted).Select(c => c.DeviceAddress).ToList();

                    var queueService = services.GetService<IQueueService>();
                    if (listDeviceAddress.Any())
                    {
                        foreach (var value in listDeviceAddress)
                        {
                            var deviceInfo = new DeviceInfoProtocolData
                            {
                                MsgId = Guid.NewGuid().ToString(),
                                Type = Constants.Protocol.LoadDeviceInfo,
                                Data = new DeviceInfoProtocolDataHeader()
                            };
                            var message = deviceInfo.ToString();
                            var topic = Constants.RabbitMq.DeviceInfoTopic + "." + value;
                            var body = Encoding.UTF8.GetBytes(message);

                            queueService.Publish(topic, message, 1);
                            
                        }
                    }
                }
            }
            catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException e)
            {
                Thread.Sleep(200);
            }

            //call ConfigureLogger in a centralized place in the code
            loggerFactory.AddFile(Configuration[Constants.Logger.LogFile], LogLevel.Error);
            loggerFactory.AddFile(Configuration[Constants.Logger.LogFile], LogLevel.Warning);
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            ValidatorOptions.LanguageManager = new LanguageManager(options);
            app.UseRequestLocalization(options.Value);
            app.UseMiddleware<RequestLocalizationMiddleware>();
            app.UseCors(Constants.Swagger.CorsPolicy);
            app.UseMvcWithDefaultRoute();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseCookiePolicy();
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint(Constants.Swagger.SwaggerJsonPath, Constants.Swagger.MyApiV1);
                });
        }
    }
}
