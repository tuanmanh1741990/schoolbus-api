﻿using System.Collections.Generic;

namespace DeMasterProCloud.DataModel.Api
{
    public class TokenModel
    {
        public string AuthToken { get; set; }

        public string RefreshToken { get; set; }
        public short AccountType { get; set; }

        public string CompanyCode { get; set; }
        public Dictionary<string, string> QueueService { get; set; }
        
        public string UserTimeZone { get; set; }
        
        public int? AccountId { get; set; }
        public int ExpireAccessToken { get; set; }
        public Dictionary<string, bool> PlugIn { get; set; }
    }
}