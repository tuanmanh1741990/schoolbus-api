using System;

namespace DeMasterProCloud.DataModel.Visit
{
    public class VisitListHistoryModel
    {
        public int Id { get; set; }
        public int VisitorId { get; set; }
        public int? OldStatus { get; set; }
        public string OldStatusString { get; set; }
        public int NewStatus { get; set; }
        public string NewStatusString { get; set; }
        public int UpdatedBy { get; set; }
        public int CompanyId { get; set; }
        public string Reason { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string CardId { get; set; }
        public DateTime EventTime { get; set; }

        /// <summary>   Gets or sets the type of the event. </summary>
        /// <value> The type of the event. </value>
        public string EventType { get; set; }

        /// <summary>   Gets or sets the event details. </summary>
        /// <value> The event details. </value>
        public string EventDetails { get; set; }
        public string DoorName { get; set; }
        public string Antipass { get; set; }
    }
}