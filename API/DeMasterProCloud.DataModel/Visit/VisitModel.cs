﻿using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.AccessGroup;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.Visit
{
    public class VisitModel
    {
        [JsonIgnore] public int Id { get; set; }
        public string VisitorName { get; set; }
        public int VisitType { get; set; }
        public string BirthDay { get; set; }
        public string VisitorDepartment { get; set; }
        public string VisitorEmpNumber { get; set; }
        public string Position { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string VisiteeSite { get; set; }
        public string VisitReason { get; set; }
        public string VisiteeName { get; set; }
        public string VisiteeDepartment { get; set; }
        public string VisiteeEmpNumber { get; set; }

        public string Phone { get; set; }
        public string Address { get; set; }

        public bool IsDecision { get; set; }

        public short CardStatus { get; set; }
        
        public int ApproverId1 { get; set; }
        public int ApproverId { get; set; }

        public string CardId { get; set; }
        public int AccessGroupId { get; set; }
        public string ProcessStatus { get; set; }
        
        public string Avatar { get; set; }
    }

    public class VisitDataModel : VisitModel
    {
        public IEnumerable<EnumModel> VisitTypes { get; set; }
        public IEnumerable<EnumModel> ListCardStatus { get; set; }
        public List<AccessGroupModelForUser> AccessGroups { get; set; }
        //public IEnumerable<EnumModel> ListVisitPreregisterCardStatus { get; set; }
    }

    public class VisitListModel
    {
        public int Id { get; set; }
        public string ApplyDate { get; set; }
        public string VisitorName { get; set; }
        public string BirthDay { get; set; }
        public string VisitorDepartment { get; set; }
        public string Position { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string VisiteeSite { get; set; }
        public string VisitReason { get; set; }
        public string VisiteeName { get; set; }
        public string Phone { get; set; }
        public string ProcessStatus { get; set; }
        public string Approver1 { get; set; }
        public string Approver2 { get; set; }
        public string RejectReason { get; set; }
        public string CardId { get; set; }
        public int AccessGroupId { get; set; }
        public bool IsDecision { get; set; }
    }

    public class VisitReportModel
    {
        public int Id { get; set; }
        public int VisitId { get; set; }
        public string AccessTime { get; set; }
        public string UserName { get; set; }
        public string BirthDay { get; set; }
        public string Department { get; set; }
        public string CardId { get; set; }
        public string Device { get; set; }
        public string DoorName { get; set; }
        public string Building { get; set; }
        public string InOut { get; set; }
        public string EventDetail { get; set; }
        public int IssueCount { get; set; }
        public string CardStatus { get; set; }


    }

    public class VisitOperationTime
    {
        public string OpeDateFrom { get; set; }
        public string OpeDateTo { get; set; }
        public string OpeTimeFrom { get; set; }
        public string OpeTimeTo { get; set; }
    }

    public class VisitViewModel
    {
        public string OpeDateFrom { get; set; }
        public string OpeDateTo { get; set; }
        public string OpeTimeFrom { get; set; }
        public string OpeTimeTo { get; set; }
        public string VisitorName { get; set; }
        public string BirthDay { get; set; }
        public string VisitorDepartment { get; set; }
        public string Position { get; set; }
        public string VisiteeSite { get; set; }
        public string VisitReason { get; set; }
        public string VisiteeName { get; set; }
        public string Phone{ get; set; }
        public string ApproverName1 { get; set; }
        public string ApproverName2 { get; set; }
        public string RejectReason { get; set; }
        public string CardId { get; set; }
        public IEnumerable<EnumModel> ProcessStatus { get; set; }
    }


    public class VisitReportViewModel
    {
        public string AccessDateFrom { get; set; }
        public string AccessDateTo { get; set; }
        public string AccessTimeFrom { get; set; }
        public string AccessTimeTo { get; set; }

        public string UserName { get; set; }
        public string BirthDay { get; set; }
        public string Department { get; set; }
        public string CardId { get; set; }
        public string Device { get; set; }
        public string DoorName { get; set; }
        public string Building { get; set; }
        public string InOutType { get; set; }
        public string EventDetails { get; set; }
        public int IssueCount { get; set; }
        private string CardStatus { get; set; }


        //public IEnumerable<SelectListItem> CardTypeList { get; set; }
        
        public IEnumerable<EnumModel> InOutList { get; set; }
        public IEnumerable<EnumModel> EventTypeList { get; set; }
        public IEnumerable<SelectListItemModel> DoorList { get; set; }
        public IEnumerable<EnumModel> ListCardStatus { get; set; }
        public IEnumerable<SelectListItemModel> BuildingList { get; set; }
    }

    public class VisitPreRegisterModel
    {
        [JsonIgnore] public int Id { get; set; }
        public string VisitorName { get; set; }
        public string VisitorEmpNumber { get; set; }
        public string BirthDay { get; set; }
        public string VisitorDepartment { get; set; }
        
        public string Position { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string VisiteeSite { get; set; }
        public string VisitReason { get; set; }
        public string VisiteeName { get; set; }
        public string VisiteeDepartment { get; set; }
        

        public string Phone { get; set; }
        public string Address { get; set; }

        

        public short CardStatus { get; set; }

        public IEnumerable<EnumModel> ListCardStatus { get; set; }
    }

    public class GetBackVisit
    {
        public List<int> VisitIds { get; set; }
        public string CardId { get; set; }
        public string Reason { get; set; }
    }
}