namespace DeMasterProCloud.DataModel.Visit
{
    public class VisitSettingModel
    {
        public string FirstApproverAccounts { get; set; }
        public string SecondsApproverAccounts { get; set; }
        public int ApprovalStepNumber{ get; set; }
        public bool OutSide{ get; set; }
    }

    public class ApprovedModel
    {
        public bool Approved { get; set; }
    }
    
    public class RejectedModel
    {
        public string Reason { get; set; }
    }
    
    public class TakeBackCardModel
    {
        public string CardId { get; set; }
        public string Reason { get; set; }
    }
}