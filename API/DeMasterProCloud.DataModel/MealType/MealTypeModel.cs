﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.MealType
{
    public class MealTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
    }

    public class MealTypeCodeModel
    {
        public int EventNumber { get; set; }
        public string EventName { get; set; }
    }
}
