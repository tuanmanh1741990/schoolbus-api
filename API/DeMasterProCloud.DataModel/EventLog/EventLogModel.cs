﻿using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;

namespace DeMasterProCloud.DataModel.EventLog
{
    public class EventLogViewModel
    {
        public string AccessDateFrom { get; set; }
        public string AccessDateTo { get; set; }
        public string AccessTimeFrom { get; set; }
        public string AccessTimeTo { get; set; }
        public short? EventType { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string InOutType { get; set; }
        public string CardId { get; set; }
        public string DoorName { get; set; }
        public short? CardType { get; set; }
        //public IEnumerable<SelectListItem> CardTypeList { get; set; }
        public int? Company { get; set; }
        public IEnumerable<EnumModel> InOutList { get; set; }
        public IEnumerable<EnumModel> EventTypeList { get; set; }
        public IEnumerable<EnumModel> VerifyModeList { get; set; }
        public IEnumerable<SelectListItemModel> DoorList { get; set; }
        public IEnumerable<SelectListItemModel> BuildingList { get; set; }
        public IEnumerable<SelectListItemModel> DepartmentList { get; set; }
        public IEnumerable<SelectListItemModel> CompanyItems { get; set; }
    }

    public class EventLogReportViewModel
    {
        public string AccessDateFrom { get; set; }
        public string AccessDateTo { get; set; }
        public string AccessTimeFrom { get; set; }
        public string AccessTimeTo { get; set; }
        public short? EventType { get; set; }
        public string UserCode { get; set; }
        public string UserName { get; set; }
        public string InOutType { get; set; }
        public string CardId { get; set; }
        public string DoorName { get; set; }
        public short? CardType { get; set; }
        //public IEnumerable<SelectListItem> CardTypeList { get; set; }
        public int? Company { get; set; }
        public IEnumerable<EnumModel> InOutList { get; set; }
        public IEnumerable<EnumModel> EventTypeList { get; set; }
        public IEnumerable<EnumModel> VerifyModeList { get; set; }
        public IEnumerable<SelectListItemModel> DoorList { get; set; }
        public IEnumerable<SelectListItemModel> BuildingList { get; set; }
        public IEnumerable<SelectListItemModel> DepartmentList { get; set; }
        public IEnumerable<SelectListItemModel> CompanyItems { get; set; }
        public IEnumerable<EnumModel> CardTypeList { get; set; }
    }


    public class EventReportPdfModel
    {
        public string EventTime { get; set; }
        public int? UserCode { get; set; }
        public string FirstName { get; set; }
        public string CardId { get; set; }
        public string DeviceAddress { get; set; }
        public string DoorName { get; set; }
        public string CardType { get; set; }
        public string InOutStatus { get; set; }
        public string EventType { get; set; }
        public string Company { get; set; }
    }

    public class EventLogAccessTimeModel
    {
        public string AccessDateFrom { get; set; }
        public string AccessDateTo { get; set; }
        public string AccessTimeFrom { get; set; }
        public string AccessTimeTo { get; set; }
    }

    public class EventTypeListModel
    {
        public List<EnumModel> EventTypeList { get; set; }
    }

    public class EventCountByDeviceModel
    {
        public int DeviceId { get; set; }
        public int Count { get; set; }
        //public string ProcessId { get; set; }
    }


    public class EventRecoveryinputModel
    {
        public List<EventRecoveryProgressModel> ProgressModels { get; set; }
        public EventLogAccessTimeModel DateTime { get; set; }
    }
    public class EventRecoveryProgressModel
    {
        public int DeviceId { get; set; }
        public string ProcessId { get; set; }
    }

    public class EventCountIdProcessIdModel
    {
        public int DeviceId { get; set; }
        public string ProcessId { get; set; }
    }

}
