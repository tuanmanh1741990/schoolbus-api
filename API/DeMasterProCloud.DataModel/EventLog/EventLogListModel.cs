﻿using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.EventLog
{
    /// <summary>
    /// EventLog List class
    /// </summary>
    public class EventLogListModel
    {
        public int Id { get; set; }
        public int? IcuId { get; set; }
        public int? UserId { get; set; }
        public int? VisitId { get; set; }
        public string AccessTime { get; set; }
        public string UserName { get; set; }
        public string Department { get; set; }
        public string CardId { get; set; }
        public bool IsRegisteredCard { get; set; }
        public string Device { get; set; }
        public string DoorName { get; set; }
        public string Building { get; set; }
        public string VerifyMode { get; set; }
        public string CardType { get; set; }
        public string ExpireDate { get; set; }
        [JsonIgnore]
        public short Type { get; set; }
        public string InOut { get; set; }
        public string EventDetail { get; set; }
        public int IssueCount { get; set; }
        public string CardStatus { get; set; }
        
        public double UnixTime { get; set; }

        /// <summary>
        /// Variables that distinguish user types of events
        /// 0 : Nomal
        /// 1 : Visit
        /// </summary>
        public int? UserType { get; set; }
    }
}
