﻿using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.EventLog
{

    /// <summary>
    /// EventLogReport List class
    /// </summary>
    public class EventLogReportListModel
    {
        public int Id { get; set; }
        public int? IcuId { get; set; }
        public int? UserId { get; set; }
        public int? VisitId { get; set; }
        public string AccessTime { get; set; }
        public string UserName { get; set; }
        public string BirthDay { get; set; }
        public string EmployeeNumber { get; set; }
        public string Department { get; set; }
        public string CardId { get; set; }
        public string DeviceAddress { get; set; }
        public string DoorName { get; set; }
        public string Building { get; set; }
        public string UserCode { get; set; }

        public string InOut { get; set; }
        public string EventDetail { get; set; }
        public int IssueCount { get; set; }
        public string CardStatus { get; set; }
        public string CardType { get; set; }
        
        public string Action { get; set; }

    }
}
