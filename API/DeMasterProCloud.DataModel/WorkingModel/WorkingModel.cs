using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.WorkingModel
{
    public class WorkingModel
    {
        public string Name { get; set; }
        public int CompanyId { get; set; }
        
        [Column(TypeName = "jsonb")]
        public string WorkingDay { get; set; }
        
        public bool IsDefault { get; set; }
        
    }
    
        
    public class WorkingListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        [Column(TypeName = "jsonb")]
        public string WorkingDay { get; set; }
        
        public bool IsDefault { get; set; }
    }
    public class WorkingTime
    {
        [JsonProperty("Name")]
        public string Name { get; set; }
        
        [JsonProperty("Start")]
        public string Start { get; set; }
        
        [JsonProperty("End")]
        public string End { get; set; }
        
        [JsonProperty("Type")]
        public string Type { get; set; }
    }
}