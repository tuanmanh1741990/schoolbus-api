﻿namespace DeMasterProCloud.DataModel.AccessGroup
{
    public class AccessGroupModelForUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public short Type { get; set; }
    }
}
