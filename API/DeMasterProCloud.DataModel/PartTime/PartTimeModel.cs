﻿using System.ComponentModel.DataAnnotations;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.User;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.PartTime
{
    public class PartTimeModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Contact { get; set; }
        public int HourlyWage { get; set; }
        public string AccountNumber { get; set; }
        public string Bank { get; set; }
        public string AccountHolder { get; set; }
    }

    public class PartTimeDateModel
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }

    public class PartTimerListModel
    {
        public int Count { get; set; }
        public List<UserModel> UserList { get; set; }

    }

    //public class BuildingListModel
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    //public class BuildingDoorModel
    //{
    //    public int Id { get; set; }
    //    public string DoorName { get; set; }
    //    public string DeviceAddress { get; set; }
    //    public string DeviceType { get; set; }
    //    public string ActiveTz { get; set; }
    //    public string PassageTz { get; set; }
    //}

    //public class BuildingUnAssignDoorModel
    //{
    //    public int Id { get; set; }
    //    public string Building { get; set; }
    //    public string DoorName { get; set; }
    //    public string DeviceAddress { get; set; }
    //}
}
