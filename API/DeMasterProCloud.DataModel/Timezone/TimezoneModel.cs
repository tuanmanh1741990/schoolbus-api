﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using DeMasterProCloud.Common.Resources;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.Timezone
{
    public class TimezoneModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        public int Position { get; set; }
        public string Name { get; set; }
        [JsonIgnore]
        public int CompanyId { get; set; }
        public List<DayDetail> Monday { get; set; }
        public List<DayDetail> Tuesday { get; set; }
        public List<DayDetail> Wednesday { get; set; }
        public List<DayDetail> Thursday { get; set; }
        public List<DayDetail> Friday { get; set; }
        public List<DayDetail> Saturday { get; set; }
        public List<DayDetail> Sunday { get; set; }
        public List<DayDetail> HolidayType1 { get; set; }
        public List<DayDetail> HolidayType2 { get; set; }
        public List<DayDetail> HolidayType3 { get; set; }
        public string Remarks { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
    }

    public class TimezoneListModel
    {
        public int Id { get; set; }
        public string TimezoneName { get; set; }
        public string Remark { get; set; }
        public int Position { get; set; }
    }

    public class DayDetail
    {
        public int From { get; set; }
        public int To { get; set; }
    }

    public class TimezoneInfoModel
    {
        public string MsgId { get; set; }
        public List<TimezoneDetailModel> Timezones { get; set; }
        //public TimezoneDetailModel PassageTz { get; set; }
    }

    public class TimezoneDetailModel
    {
        public string Name { get; set; }
        public int Position { get; set; }
        public string Type { get; set; }
        public TimezoneIntervalTime Monday { get; set; }
        public TimezoneIntervalTime Tuesday { get; set; }
        public TimezoneIntervalTime Wednesday { get; set; }
        public TimezoneIntervalTime Thursday { get; set; }
        public TimezoneIntervalTime Friday { get; set; }
        public TimezoneIntervalTime Saturday { get; set; }
        public TimezoneIntervalTime Sunday { get; set; }
        public TimezoneIntervalTime Holiday1 { get; set; }
        public TimezoneIntervalTime Holiday2 { get; set; }
        public TimezoneIntervalTime Holiday3 { get; set; }
    }

    public class TimezoneIntervalTime
    {
        public string Interval1 { get; set; }
        public string Interval2 { get; set; }
        public string Interval3 { get; set; }
        public string Interval4 { get; set; }
    }
}
