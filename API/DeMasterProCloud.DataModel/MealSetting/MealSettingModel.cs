﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.MealSetting
{
    public class MealSettingModel
    {
        public int id { get; set; }
        public int icuDevice { get; set; }
        public int cornerId { get; set; }
        public int mealTypeId { get; set; }
        public decimal start { get; set; }
        public decimal end { get; set; }
        public decimal price { get; set; }
        public string mealTypeName { get; set; }
        public string icuDeviceName { get; set; }
        public string cornerName { get; set; }
        public string rId { get; set; }
        public object data { get; set; }
        public int Code { get; set; }
        public int mealSettingId { get; set; }
    }


    public class ListMealSettingModel
    {

        public int icuDevice { get; set; }
        public string icuDeviceName { get; set; }
        public string cornerName { get; set; }
        public int cornerId { get; set; }
        public string rId { get; set; }
        public object data { get; set; }
        public int mealSettingId { get; set; }
        public decimal price { get; set; }
    }

    public class ObjectMealSettingModel
    {

        public string mealTypeName { get; set; }
        public int mealTypeId { get; set; }
        public int mealSettingId { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public decimal price { get; set; }
    }

    public class AddMealSettingModel
    {
        public int id { get; set; }
        public string rId { get; set; }
        public int cornerId { get; set; }
        public int mealTypeId { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public decimal price { get; set; }
        public int icuId { get; set; }
    }
}
