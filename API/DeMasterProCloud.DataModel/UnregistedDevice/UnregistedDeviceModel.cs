﻿namespace DeMasterProCloud.DataModel.UnregistedDevice
{
    public class UnregistedDeviceModel
    {
        public int Id { get; set; }
        public string DeviceAddress { get; set; }
        public short Status { get; set; }
        public string IpAddress { get; set; }
    }
}
