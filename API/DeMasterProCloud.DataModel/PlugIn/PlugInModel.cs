using System.ComponentModel.DataAnnotations.Schema;

namespace DeMasterProCloud.DataModel.PlugIn
{
    public class PlugInModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        
        [Column(TypeName = "jsonb")]
        public string Solutions { get; set; }
        
        [Column(TypeName = "jsonb")]
        public string PlugInsDescription { get; set; }
        
    }

    public class PlugIns
    {
        public PlugIns()
        {
            Common=true;
            AccessControl = false;
            ArmyManagement = false;
            VisitManagement = false;
            TimeAttendance = false;
            CanteenManagement = false;
            CardIssuing = false;
            ScreenMessage = false;
            QrCode = false;
            PassCode = false;
        }
        public bool Common { get; set; }
        public bool AccessControl { get; set; }
        public bool VisitManagement { get; set; }
        public bool TimeAttendance { get; set; }
        public bool CanteenManagement { get; set; }
        public bool CardIssuing { get; set; }
        public bool ScreenMessage { get; set; }
        public bool QrCode { get; set; }
        public bool PassCode { get; set; }

        public bool ArmyManagement { get; set; }
    }

    public class PlugInsDescription
    {
        public string Common { get; set; }
        public string AccessControl { get; set; }
        public string VisitManagement { get; set; }
        public string TimeAttendance { get; set; }
        public string CanteenManagement { get; set; }
        public string CardIssuing { get; set; }
        public string ScreenMessage { get; set; }
        public string QrCode { get; set; }
        public string PassCode { get; set; }

        public string ArmyManagement { get; set; }
    }
}