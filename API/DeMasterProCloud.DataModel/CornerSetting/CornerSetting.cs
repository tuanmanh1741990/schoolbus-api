﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.CornerSetting
{
    public class CornerSettingModel
    {
        public int id { get; set; }
        public int? companyId { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string companyName { get; set; }
    }

    public class AddCornerSettingModel
    {
        public int id { get; set; }
        public int? companyId { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string description { get; set; }
    }
}
