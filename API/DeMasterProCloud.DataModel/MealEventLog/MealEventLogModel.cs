﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.MealEventLog
{
    public class MealEventLogAddModel
    {
        public DateTime eventTime { get; set; }
        public int icuId { get; set; }
        public string userName { get; set; }
        public int userId { get; set; }
        public int mealCode { get; set; }
        public string mealType { get; set; }
        public string doorName { get; set; }
    }

    public class MealEventLogUpdateModel
    {
        public DateTime eventTime { get; set; }
        public int icuId { get; set; }
        public string userName { get; set; }
        public int userId { get; set; }
        public int mealCode { get; set; }
        public string mealType { get; set; }
        public decimal Price { get; set; }
        public string doorName { get; set; }
        public int mealEventLogId { get; set; }
        public int eventLogId { get; set; }
    }

    public class MealEventLogModel
    {
        public DateTime eventTime { get; set; }
        public int icuId { get; set; }
        public string userName { get; set; }
        public int userId { get; set; }
        public int mealCode { get; set; }
        public string mealType { get; set; }
        public decimal Price { get; set; }
        public string doorName { get; set; }
        public int mealEventLogId { get; set; }
        public int eventLogId { get; set; }
        public string deviceName { get; set; }
        public int companyId { get; set; }
        public bool isManual { get; set; }
        public int deptId { get; set; }
        public string departmentName { get; set; }
        public string userCode { get; set; }
        public string cardId { get; set; }
        public decimal totalPrice { get; set; }
        public string employeeNumber { get; set; }
        public DateTime? birthDay { get; set; }
        public int buildingId { get; set; }
        public string deviceAddress { get; set; }
        public string buildingName { get; set; }
        public string cornerName { get; set; }
        public int cornerId { get; set; }
    }

    public class MealEventLogReportModel
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public string cardId { get; set; }
        public string departmentName { get; set; }
        public string userCode { get; set; }
        public decimal totalPrice { get; set; }
        public string employeeNumber { get; set; }
        public DateTime? birthDay { get; set; }

    }

    public class ReportCanteenManagementByBuildingModel
    {
        public string Day { get; set; }
        public object LstBuilding { get; set; }
    }

    public class ReportCanteenManagementByCornerModel
    {
        public string Day { get; set; }
        public object LstCorner { get; set; }
    }

    public class ReportCanteenGroupBy
    {
        public int BuildingId { get; set; }
        public int EventLogId { get; set; }
        public int IcuId { get; set; }
        public int TotalCount { get; set; }
        public string AccessTime { get; set; }
        public int EvenType { get; set; }
        public string MealTypeName { get; set; }
    }

    public class ReportCornerGroupBy
    {
        public int MealTypeId { get; set; }
        public int CornerId { get; set; }
        public int TotalCount { get; set; }

        public string MealTypeName { get; set; }
        public string CornerName { get; set; }
    }

    public class MealTypeByBuildingModel
    {
        public string mealTypeName { get; set; }
        public int totalCount { get; set; }
        public int mealTypeId { get; set; }
    }


    public class ReportMealEventLogByCornerModel
    {
        public int EventLogId { get; set; }
        public string AccessTime { get; set; }
        public int CornerId { get; set; }
        public int MealTypeId { get; set; }
        public int TotalCount { get; set; }
    }
}
