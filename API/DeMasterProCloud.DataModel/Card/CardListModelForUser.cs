﻿namespace DeMasterProCloud.DataModel.AccessGroup
{
    public class CardListModelForUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }
}
