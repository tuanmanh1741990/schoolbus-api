﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.ExceptionalMeal
{
    public class ExceptionalMealModel
    {
        public int id { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public decimal price { get; set; }
        public int mealSettingId { get; set; }
        public string icuDeviceName { get; set; }
        public string cornerName { get; set; }
        public string mealTypeName { get; set; }
        public decimal fromHout { get; set; }
        public decimal toHour { get; set; }
        public decimal priceDiscount { get; set; }
        public int icuDeviceId { get; set; }
    }

    public class AddExceptionalMealModel
    {
        public int id { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public decimal price { get; set; }
        public int mealSettingId { get; set; }
       
    }
}
