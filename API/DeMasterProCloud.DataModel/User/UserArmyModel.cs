﻿using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.DataModel.Category;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.User
{
    public class UserArmyModel
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string Email { get; set; }
        public string BirthDay { get; set; }
        public int? DepartmentId { get; set; }
        public string UserCode { get; set; }
        public string Position { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpiredDate { get; set; }
        public bool IsMasterCard { get; set; }
        public short Status { get; set; }
        public short CardType { get; set; }
        public bool Gender { get; set; }
        public string Address { get; set; }
        public string Nationality { get; set; }
        public string City { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string PostCode { get; set; }
        public string Note { get; set; }
        public int PermissionType { get; set; }
        public string Password { get; set; }
        public string PasswordCheck { get; set; }
        public string ApplyReason { get; set; }

        public List<CardModel> CardList { get; set; }

        public List<int> CategoryOptionIds { get; set; }

        public int AccessGroupId { get; set; }
        
        public int?  WorkingTypeId { get; set; }

        public string Avatar { get; set; }

        // informations for army
        public string MilitaryNumber { get; set; }
        public string BackGroundCheckNumber { get; set; }
        public string EnlistmentDate { get; set; }
        public string SecretIssuanceBasis { get; set; }
        /// <summary>
        /// This information is used in UserModel.
        /// In UserModel, API bring this value from User table in DB.
        /// But in UserArmyModel, API should bring the value from UserArmy table in DB.
        /// </summary>
        public short WorkType { get; set; }
    }

    public class UserArmyDataModel : UserArmyModel
    {
        public IEnumerable<EnumModel> Genders { get; set; }
        public IEnumerable<EnumModel> ListCardStatus { get; set; }
        public IEnumerable<EnumModel> ListPassType { get; set; }
        public IEnumerable<EnumModel> ListWorkType { get; set; }
        public IEnumerable<EnumModel> ListUserStatus { get; set; }
        public IEnumerable<EnumModel> ListPermissionType { get; set; }
        public IEnumerable<EnumModel> ListAccessGroupType { get; set; }
        public List<DepartmentModelForUser> Departments { get; set; }
        public List<AccessGroupModelForUser> AccessGroups { get; set; }
        public IEnumerable<Category.Node> Categories { get; set; }
        public IEnumerable<Category.Node> CategoryOptions { get; set; }
    }  
    
    public class UserArmyListModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserCode { get; set; }
        public string DepartmentName { get; set; }
        public string AccessGroupName { get; set; }
        public string MilitaryNo { get; set; }
        public string ExpiredDate { get; set; }
        public List<CardModel> CardList { get; set; }
        
        public List<UserCategoryDataModel> CategoryOptions { get; set; }
    }

}
