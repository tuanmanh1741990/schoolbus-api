﻿using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.DataModel.Category;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.User
{
    public enum Sex
    {
        Male = 1,
        Female = 2
    }

    public enum FileType
    {
        Excel = 0,
        Txt = 1
    }

    public class UserModel
    {
        [JsonIgnore]
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string Email { get; set; }
        public string BirthDay { get; set; }
        public int? DepartmentId { get; set; }
        public string UserCode { get; set; }
        public string Position { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpiredDate { get; set; }
        public bool IsMasterCard { get; set; }
        public short Status { get; set; }
        public short CardType { get; set; }
        public bool Gender { get; set; }
        public short WorkType { get; set; }


        public string Address { get; set; }
        public string Nationality { get; set; }
        public string City { get; set; }
        public string HomePhone { get; set; }
        public string OfficePhone { get; set; }
        public string PostCode { get; set; }
        public string Note { get; set; }
        public int PermissionType { get; set; }
        public string Password { get; set; }
        public string PasswordCheck { get; set; }
        public string ApplyReason { get; set; }

        //public string CardList { get; set; }

        public List<CardModel> CardList { get; set; }

        public List<int> CategoryOptionIds { get; set; }


        public int AccessGroupId { get; set; }
        
        public int?  WorkingTypeId { get; set; }

        public string Avatar { get; set; }

        //public string LastName { get; set; }
        //public string KeyPadPassword { get; set; }

        //public string IssuedDate { get; set; }
        //public string Job { get; set; }
        //public string CardId { get; set; }
        //public string EmployeeNumber { get; set; }

        //public string Responsibility { get; set; }


        //public string IssueCount { get; set; }

        //public short CardStatus { get; set; }
    }

    public class UserDataModel : UserModel
    {
        public IEnumerable<EnumModel> Genders { get; set; }
        public IEnumerable<EnumModel> ListCardStatus { get; set; }
        public IEnumerable<EnumModel> ListPassType { get; set; }
        public IEnumerable<EnumModel> ListWorkType { get; set; }
        public IEnumerable<EnumModel> ListUserStatus { get; set; }
        public IEnumerable<EnumModel> ListPermissionType { get; set; }
        public IEnumerable<EnumModel> ListAccessGroupType { get; set; }
        public List<DepartmentModelForUser> Departments { get; set; }
        public List<AccessGroupModelForUser> AccessGroups { get; set; }
        public IEnumerable<Category.Node> Categories { get; set; }
        public IEnumerable<Category.Node> CategoryOptions { get; set; }
    }

    public class CardModel
    {
        public int Id { get; set; }
        public string CardId { get; set; }
        public int IssueCount { get; set; }
        public short CardStatus { get; set; }
        public string Description { get; set; }
        
        public int CardType { get; set; }
    }

    public class UserCardModel
    {
       public UserModel UserModel { get; set; }
       public CardModel CardModel { get; set; }
    }



    //public class UserModel
    //{
    //    [JsonIgnore]
    //    public int Id { get; set; }
    //    //public int UserCode { get; set; }
    //    public string FirstName { get; set; }
    //    public string LastName { get; set; }
    //    public bool Gender { get; set; }
    //    public string KeyPadPassword { get; set; }
    //    public string IssuedDate { get; set; }
    //    public string Position { get; set; }
    //    public string OfficePhone { get; set; }
    //    public string PostCode { get; set; }
    //    public string Job { get; set; }
    //    public string Address { get; set; }
    //    public string Avatar{ get; set; }
    //    public int? DepartmentId { get; set; }
    //    public string CardId { get; set; }
    //    public string EmployeeNumber { get; set; }
    //    public string ExpiredDate { get; set; }
    //    public string EffectiveDate { get; set; }
    //    public string Nationality { get; set; }
    //    public string City { get; set; }
    //    public string HomePhone { get; set; }
    //    public string Responsibility { get; set; }
    //    public string Note { get; set; }
    //    public int AccessGroupId { get; set; }
    //    public string IssueCount { get; set; }
    //    public bool IsMasterCard { get; set; }
    //    public short CardStatus { get; set; }
    //}

    //public class UserDataModel : UserModel
    //{
    //    public IEnumerable<EnumModel> Genders { get; set; }
    //    public IEnumerable<EnumModel> ListCardStatus { get; set; }
    //    public List<DepartmentModelForUser> Departments { get; set; }
    //    public List<AccessGroupModelForUser> AccessGroups { get; set; }
    //}

    public class UserListModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserCode { get; set; }
        public string Department { get; set; }
        public string EmployeeNo { get; set; }
        public string ExpiredDate { get; set; }
        public List<CardModel> CardList { get; set; }
        
        //public string IssueCount { get; set; }
        //public string IsMasterCard { get; set; }
        //public string CardStatus { get; set; }
        public string AccessGroup { get; set; }
    }

    public class AccessibleUserModel
    {
        public int Id { get; set; }
        public string CardId { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string EmployeeNumber { get; set; }
        public string ExpiredDate { get; set; }
        public string CardStatus { get; set; }
        public List<CardModel> CardList { get; set; }
    }
}
