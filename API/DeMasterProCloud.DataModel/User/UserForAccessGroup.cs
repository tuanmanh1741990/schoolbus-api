﻿using System.Collections.Generic;

namespace DeMasterProCloud.DataModel.User
{
    public class UserForAccessGroup
    {
        public int Id { get; set; }
        public string CardId { get; set; }
        public string FullName { get; set; }
        public string DepartmentName { get; set; }
        public List<CardModel> CardList { get; set; }
    }

    public class UnAssignUserForAccessGroup
    {
        public int Id { get; set; }
        public string CardId { get; set; }
        public string FullName { get; set; }
        public string DepartmentName { get; set; }
        public string AccessGroupName { get; set; }
        public List<CardModel> CardList { get; set; }
    }
}
