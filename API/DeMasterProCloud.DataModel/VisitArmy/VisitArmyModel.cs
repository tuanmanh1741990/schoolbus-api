﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.AccessGroup;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.VisitArmy
{
    public class VisitArmyModel
    {
        [JsonIgnore] public int Id { get; set; }
        public string VisitorName { get; set; }
        public int VisitType { get; set; }
        public string BirthDay { get; set; }
        public string VisitorDepartment { get; set; }
        public string VisitorEmpNumber { get; set; }
        public string Position { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string VisiteeSite { get; set; }
        public string VisitReason { get; set; }
        public string VisiteeName { get; set; }
        public string VisiteeDepartment { get; set; }
        public string VisiteeEmpNumber { get; set; }

        public string Phone { get; set; }
        public string Address { get; set; }

        public bool IsDecision { get; set; }

        public short CardStatus { get; set; }

        public int ApproverId1 { get; set; }
        public int ApproverId { get; set; }

        public string CardId { get; set; }
        public int AccessGroupId { get; set; }
        public string ProcessStatus { get; set; }

        public string Avatar { get; set; }

        // For army
        public string MilitaryNumber { get; set; }
        public string Relationship { get; set; }
        public string BackGroundCheckNumber { get; set; }
        public int Gender { get; set; }
        public string VisitorRank { get; set; }
        public string VisiteeRank { get; set; }
    }

    public class VisitArmyDataModel : VisitArmyModel
    {
        public IEnumerable<EnumModel> VisitTypes { get; set; }
        public IEnumerable<EnumModel> ListCardStatus { get; set; }
        public List<AccessGroupModelForUser> AccessGroups { get; set; }

        public IEnumerable<EnumModel> Genders { get; set; }
    }
}
