﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.UserDiscount
{
    public class UserDiscountModel
    {
        public int id { get; set; }
        public int userId { get; set; }
        public decimal amount { get; set; }
        public string userName { get; set; }
    }

    public class AddUserDiscountModel
    {
        public int id { get; set; }
        public int userId { get; set; }
        public decimal amount { get; set; }
    }
}
