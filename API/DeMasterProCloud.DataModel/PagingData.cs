﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel
{
    public class PagingData<T>
    {
        public PagingData()
        {
            Meta = new Meta();
        }
        public List<T> Data { get; set; }
        public Meta Meta { get; set; }
    }

    public class Meta
    {
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
    }

    public class PagingData<T1, T2>
    {
        public PagingData()
        {
            Meta = new Meta();
        }
        public List<T1> Data1 { get; set; }
        public List<T2> Data2 { get; set; }
        public Meta Meta { get; set; }
    }

    public class HeaderData
    {
        public int HeaderId { get; set; }
        public string HeaderName { get; set; }
        public string HeaderVariable { get; set; }
        public bool IsCategory { get; set; }
    }
}
