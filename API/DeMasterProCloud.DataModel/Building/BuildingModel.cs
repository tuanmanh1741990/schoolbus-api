﻿using System.ComponentModel.DataAnnotations;
using DeMasterProCloud.Common.Resources;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.Building
{
    public class BuildingModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        [Display(Name = nameof(BuildingResource.lblBuildingName), ResourceType = typeof(BuildingResource))]
        public string Name { get; set; }
        
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Location { get; set; }
        public string TimeZone { get; set; }
        
    }

    public class BuildingListModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class BuildingDoorModel
    {
        public int Id { get; set; }
        public string DoorName { get; set; }
        public string DeviceAddress { get; set; }
        public string DeviceType { get; set; }
        public string ActiveTz { get; set; }
        public string PassageTz { get; set; }
        public string ConnectionStatus { get; set; }
        /// <summary>
        /// This is door's status.
        /// </summary>
        public string DoorStatus { get; set; }
    }

    public class BuildingUnAssignDoorModel
    {
        public int Id { get; set; }
        public string Building { get; set; }
        public string DoorName { get; set; }
        public string DeviceAddress { get; set; }
    }
}
