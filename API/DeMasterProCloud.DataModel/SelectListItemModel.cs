﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel
{
    public class SelectListItemModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
