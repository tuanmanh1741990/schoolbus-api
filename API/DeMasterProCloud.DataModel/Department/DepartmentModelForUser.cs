﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataModel.Department
{
    public class DepartmentModelForUser
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
