﻿using DeMasterProCloud.DataModel.User;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataModel.Department
{
    public class DepartmentModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public int? ParentId { get; set; }
        public int? DepartmentManagerId { get; set; }
        //public IEnumerable<SelectListItem> ParentDepartments { get; set; }
    }

    public class DepartmentImportExportModel
    {
        public Field<string> Name { get; set; }
        public Field<string> Number { get; set; }
        public Field<int?> ParentId { get; set; }
        public bool IsValid { get; set; }
    }
}
