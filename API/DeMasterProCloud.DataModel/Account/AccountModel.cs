﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using DeMasterProCloud.Common.Resources;
using Newtonsoft.Json;
using DeMasterProCloud.DataModel.Company;
using DeMasterProCloud.Common.Infrastructure;

namespace DeMasterProCloud.DataModel.Account
{
    public class AccountTimeZoneModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string TimeZone { get; set; }
    }
    public class AccountModel
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int? CompanyId { get; set; }
        public bool RootFlag { get; set; }
        public short Role { get; set; }
        //public string Remarks { get; set; }
        public short Status { get; set; } = 1;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TimeZone { get; set; }
    }
    public class AccountDataModel
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int CompanyId { get; set; }
        public bool RootFlag { get; set; }
        public short Role { get; set; }
        public short Status { get; set; }
        public string TimeZone { get; set; }
        public bool IsCurrentAccount { get; set; }
        public IEnumerable<CompanyListModel> CompanyIdList { get; set; }
        public IEnumerable<EnumModel> RoleList { get; set; }
        public IEnumerable<SelectListItem> StatusList { get; set; }
    }


    public class AccountListModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
        public string CompanyName { get; set; }
        
        public string TimeZone { get; set; }
        //public string Remarks { get; set; }
    }

    //public class ForgotPasswordModel
    //{
    //    public string CompanyCode { get; set; }
    //    public string Username { get; set; }
    //}

    public class ContactModel
    {
        public int CompanyId { get; set; }
        public string Contact { get; set; }
    }
}
