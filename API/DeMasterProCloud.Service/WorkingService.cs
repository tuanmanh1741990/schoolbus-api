using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Company;
using DeMasterProCloud.DataModel.WorkingModel;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;


namespace DeMasterProCloud.Service
{
    public interface IWorkingService : IPaginationService<WorkingListModel>
    {
        new IQueryable<WorkingListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        WorkingType GetWorkingType(int workingId, int companyId);
        int Add(WorkingModel model);
        void Update(int id, WorkingModel model);
        WorkingType CheckWorkingTypeExisted(string workingName);
        List<User> GetUserUsingWorkingType(int workingId, int companyId);
        void Delete(WorkingType workingType);
        
        void AssignMultipleUserToWorkingTime(int workingTypeId, string listUserId);
        bool CheckNameWorkingTime(string workingName, int id);
    }
    
    public class WorkingService : IWorkingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ICompanyService _companyService;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Ctor for account service
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="companyService"></param>
        /// <param name="mailService"></param>
        /// <param name="logger"></param>
        public WorkingService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor,
            ICompanyService companyService, IConfiguration configuration, ILogger<AccountService> logger)
        {
            _unitOfWork = unitOfWork;
            _companyService = companyService;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
            _configuration = configuration;
            
        }
        
        public IQueryable<WorkingListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.WorkingType
                .Where(c => c.CompanyId == _httpContext.User.GetCompanyId())
                .Select(m => new WorkingListModel()
                {
                    Id = m.Id,
                    Name = m.Name,
                    WorkingDay = m.WorkingDay,
                    IsDefault= m.IsDefault
                });

            totalRecords = data.Count();
            

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Company.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Company[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber -1)  * pageSize).Take(pageSize);
            return data;
        }
        
        
        public int Add(WorkingModel model)
        {
            var workingId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var workingType = Mapper.Map<WorkingType>(model);
                        workingType.CompanyId = _httpContext.User.GetCompanyId();
                        var defaultWorkingType = CheckWorkingTypeDefault(_httpContext.User.GetCompanyId());
                        if (!defaultWorkingType)
                        {
                            workingType.IsDefault = true;
                        }
                        else
                        {
                            workingType.IsDefault = false;
                        }
                        _unitOfWork.WorkingRepository.Add(workingType);
                        _unitOfWork.Save();

                        /*//Save system log
                        var content = BuildingResource.msgAdd;
                        List<string> details = new List<string>
                        {
                            $"{BuildingResource.lblBuildingName} : {building.Name}",
                            $"{BuildingResource.lblAddress} : {building.Address}",
                            $"{BuildingResource.lblCity} : {building.City}",
                            $"{BuildingResource.lblCountry} : {building.Country}",
                            $"{BuildingResource.lblPostalCode} : {building.PostalCode}"
                        };
                        var contentsDetails = string.Join("\n", details);

                        _unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.Add,
                            content, contentsDetails, null, _httpContext.User.GetCompanyId());*/

                        _unitOfWork.Save();
                        transaction.Commit();
                        workingId = workingType.Id;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
            return workingId;
        }

        public WorkingType CheckWorkingTypeExisted(string workingName)
        {
            var existed = _unitOfWork.WorkingRepository.CheckWorkingTypeExisted(workingName, _httpContext.User.GetCompanyId());
            return existed;
        }

        public bool CheckNameWorkingTime(string workingName, int id)
        {
            var checkWorkingTime = _unitOfWork.WorkingRepository.CheckNameWorkingTime(workingName, _httpContext.User.GetCompanyId(),id);
            return checkWorkingTime;
        }
        public WorkingType GetWorkingType(int id, int companyId)
        {
            var workingType = _unitOfWork.AppDbContext.WorkingType.FirstOrDefault(m => m.Id == id && m.CompanyId == companyId);

            if (workingType != null)
            {
                if (workingType.CompanyId != 0)
                {
                    var company = _companyService.GetById(workingType.CompanyId);
                    workingType.Company = company;
                }

            }
            return workingType;
        }

        private bool CheckWorkingTypeDefault(int companyId)
        {
            var workingType = _unitOfWork.AppDbContext.WorkingType.FirstOrDefault(m =>  m.CompanyId == companyId && m.IsDefault);

            if (workingType != null)
            {
                return true;
            }
            return false;
        }
        
        private WorkingType GetWorkingTypeDefault(int companyId)
        {
            var workingType = _unitOfWork.AppDbContext.WorkingType.FirstOrDefault(m =>  m.CompanyId == companyId && m.IsDefault);

            return workingType;
        }

        public void Update(int id, WorkingModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var workingType = _unitOfWork.WorkingRepository.GetById(id);
                        model.CompanyId = _httpContext.User.GetCompanyId();
                        
                        var workingTypeDefault = GetWorkingTypeDefault(_httpContext.User.GetCompanyId());
                        if (model.IsDefault == true)
                        {
                            if (workingType.Id != workingTypeDefault.Id)
                            {
                                workingTypeDefault.IsDefault = false;
                                
                            }
                        }
                        else
                        {
                            if (workingType.Id == workingTypeDefault.Id)
                            {
                                model.IsDefault = true;
                            }
                        }

                        Mapper.Map(model, workingType);
                        _unitOfWork.WorkingRepository.Update(workingTypeDefault);
                        _unitOfWork.WorkingRepository.Update(workingType);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
        }
        public List<User> GetUserUsingWorkingType(int workingId, int companyId)
        {
            var users = _unitOfWork.AppDbContext.User.Where(m =>  m.CompanyId == companyId && !m.IsDeleted && m.WorkingTypeId == workingId).ToList();
            return users;
        }
        
        public void Delete(WorkingType workingType)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _unitOfWork.WorkingRepository.Delete(workingType);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

        }
        
        public void AssignMultipleUserToWorkingTime(int workingTypeId, string listUserId)
        {
            String[] strListId = listUserId.Split(",");
            foreach (var userId in strListId)
            {
                var user = _unitOfWork.UserRepository.GetById(Int32.Parse(userId));
                user.WorkingTypeId = workingTypeId;
                _unitOfWork.UserRepository.Update(user);
            }
            _unitOfWork.Save();
        }
    }
}