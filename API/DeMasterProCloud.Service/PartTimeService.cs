﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DeMasterProCloud.DataModel.Building;
using DeMasterProCloud.DataModel.PartTime;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Building service interface
    /// </summary>
    public interface IPartTimeService
    {

        void Add(PartTimeModel model);
        //void AddDoors(int id, List<int> doorIds);
        //IQueryable<BuildingDoorModel> GetPaginatedDoors(int id, string filter, int pageNumber, int pageSize, int sortColumn,
        //    string sortDirection, out int totalRecords, out int recordsFiltered);
        //IQueryable<BuildingUnAssignDoorModel> GetPaginatedUnAssignDoors(int id, string filter, int pageNumber,
        //    int pageSize, int sortColumn,
        //    string sortDirection, out int totalRecords, out int recordsFiltered);
        //void Delete(int id);
        //void DeleteRange(List<Building> buildings);
        //void DeleteDoors(int id, List<int> doorIds);
        //void Update(int id, BuildingModel model);
        //bool IsExistedBuildingName(int id, string name);
        //Building GetById(int id);
        //List<Building> GetByIds(List<int> ids);

    }

    public class PartTimeService : IPartTimeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;

        public PartTimeService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor, ILogger<PartTimeService> logger)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
        }


        /// <summary>
        /// Add PartTime
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public void Add(PartTimeModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var partTime = new PartTime()
                        {
                            CompanyId = _httpContext.User.GetCompanyId(),
                            UserId = model.UserId,
                            Contact = model.Contact,
                            HourlyWage = model.HourlyWage,
                            AccountHolder = model.AccountHolder,
                            Bank = model.Bank,
                            AccountNumber = model.AccountNumber,
                            IsDeleted = false,
                        };
                        
                        _unitOfWork.PartTimeRepository.Add(partTime);
                        _unitOfWork.Save();

                        ////Save system log
                        //var content = $"{ActionLogTypeResource.Add}: {building.Name} ({BuildingResource.lblBuilding})";
                        //_unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.Add,
                        //    content);

                        //_unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
        }

        

    }
}