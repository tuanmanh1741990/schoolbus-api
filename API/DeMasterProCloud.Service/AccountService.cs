﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Account;
using DeMasterProCloud.DataModel.Login;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;
using MoreLinq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using System.Threading;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// UserLogin service interface
    /// </summary>
    public interface IAccountService : IPaginationService<Account>
    {
        int Add(AccountModel model);

        void Update(AccountModel model);

        void UpdateTimeZone(AccountTimeZoneModel model, string userTimeZone = "");

        void Delete(Account account);

        void DeleteRange(List<Account> accounts);

        void ChangePassword(Account account);


        Account GetById(int id);

        List<Account> GetAccountsByIds(List<int> idArr, int companyId);

        bool IsExist(int id, string username, int? companyId = null);

        bool IsExist(string username);

        Account GetAccountByCompanyCodeAndUsername(string companyCode, string username);

        Account GetAuthenticatedAccount(LoginModel model);

        IEnumerable<SelectListItem> GetAccountRoles(short? selected);

        Account GetValidAccount(string username, int companyId);

        bool IsAllowDelete(Account account, Account accountLogin);

        Account GetAccountLogin(ClaimsPrincipal user);

        Account GetRootAccountByCompany(int companyId);

        Account GetAccountByCurrentCompany(int id);

        AccountDataModel InitData(AccountDataModel model);

        bool IsValidAccountType(short accountType);

        Account GetAccountByEmail(string email);

        void SaveSystemLogLogIn(Account account);

        bool HasDefaultAccount(/*int companyId*/);

        Dictionary<string, List<string>> GetTimeZone();

        Dictionary<string, string> GetInfomationSystem();

        Dictionary<string, string> GetUserRabbitCredential(Account account);

        bool IsSystemAdmin(int accountId);

        IQueryable<Account> GetPaginatedPrimaryAccount(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        IEnumerable<EnumModel> GetAccountType();

        bool IsExistAccount(int id, int companyId);

        string GetTokenByAccount(Account account);

        void SendResetAccountMail(Account account, int companyId);
        void SendWelcomeMail(string email, string userName, string token, int companyId);

        void SendWelcomeMailForManager(string email, string token, int companyId);

        void SendWelcomeMailForAdmin(string email, string token);

        void SendUpdateMail(string email, string userName, string token, int companyId);
        void SendRequestApprovalMail(Visit visitor);
        void SendQRMail(Visit visitor, Image QrImage);

        void AddTokenAndRefreshTokenToDB(string refreshToken, Account model, int expiryRefreshToken);
        Account GetAccountByRefreshToken(string refreshToken);
        string GetRefreshTokenByUserName(string userName, int CompanyId);
        void AddPrimaryManager(AccountModel model, Account account);
        void DeletePrimaryManager(AccountModel model, int oldCompanyId);
    }

    /// <summary>
    /// Implement Account service inteface
    /// </summary>
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ICompanyService _companyService;
        private readonly IMailService _mailService;
        //private readonly IVisitService _visitService;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IJwtHandler _jwtHandler;
        private readonly ISettingService _settingService;

        /// <summary>
        /// Ctor for account service
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="companyService"></param>
        /// <param name="mailService"></param>
        /// <param name="logger"></param>
        public AccountService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor,
            ICompanyService companyService, IMailService mailService,/* IVisitService visitService,*/
            IConfiguration configuration, ILogger<AccountService> logger, IJwtHandler jwtHandler, ISettingService settingService)
        {
            _unitOfWork = unitOfWork;
            _companyService = companyService;
            _httpContext = httpContextAccessor.HttpContext;
            _mailService = mailService;
            //_visitService = visitService;
            _logger = logger;
            _configuration = configuration;

            //ArrayList<String> arrayList = new ArrayList<string>();

            _jwtHandler = jwtHandler;
            _settingService = settingService;
        }

        /// <summary>
        /// Add account
        /// </summary>
        /// <param name="model"></param>
        public int Add(AccountModel model)
        {
            var accountId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var account = Mapper.Map<Account>(model);
                        TimeZoneInfo localZone = TimeZoneInfo.Local;
                        if (_httpContext.User.GetAccountType() != 0)
                        {
                            var companyId = _httpContext.User.GetCompanyId();
                            account.CompanyId = companyId;
                        }
                        else
                        {
                            account.CompanyId = account.CompanyId == 0 ? null : account.CompanyId;
                        }
                        account.TimeZone = string.IsNullOrEmpty(account.TimeZone) ? localZone.Id : account.TimeZone;
                        account.Username = account.Username.ToLower();

                        _unitOfWork.AccountRepository.Add(account);

                        // Save for account ID ( new added account )
                        // The new account ID should be used for storing system log.
                        _unitOfWork.Save();

                        if (account.CompanyId.HasValue)
                        {
                            account.Company = _unitOfWork.CompanyRepository.GetCompanyById(account.CompanyId);

                            // make content for storing system log
                            var content = string.Format(AccountResource.lblAddNew);
                            // make contentsDetails for storing system log
                            List<string> details = new List<string>
                            {
                                $"{AccountResource.lblEmail} : {account.Username}",
                                $"{AccountResource.lblRole} : {((AccountType)account.Type).GetDescription()}"
                            };
                            var contentsDetails = string.Join("\n", details); ;
                            //Save system log
                            _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Add,
                                    content, contentsDetails, null, account.CompanyId);

                            _unitOfWork.Save();

                            if (account.Type != (short)AccountType.Employee)
                            {
                                //Send email to manager ( primary manager, Secondary manager )
                                var token = GetTokenByAccount(account);

                                SendWelcomeMailForManager(account.Username, token, account.CompanyId.Value);
                            }
                            if (account.Type == (short)AccountType.PrimaryManager)
                            {
                                AddPrimaryManager(model, account);
                            }
                        }
                        else if (account.Type == (short)AccountType.SystemAdmin)
                        {
                            //Send email to Administrator
                            var token = GetTokenByAccount(account);
                            SendWelcomeMailForAdmin(account.Username, token);
                        }

                        //Save and commit
                        _unitOfWork.Save();
                        transaction.Commit();
                        accountId = account.Id;
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("Failed to add account.");
                        _logger.LogError($"{e.Message}:{Environment.NewLine} {e.StackTrace}");

                        transaction.Rollback();
                        throw;
                    }
                }
            });
            return accountId;
        }

        /// <summary>
        /// Update account
        /// </summary>
        /// <param name="model"></param>
        public void Update(AccountModel model)
        {
            //_unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            //{
            //    using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
            //    {
            try
            {
                var account = GetById(model.Id);
                List<string> changes = new List<string>();
                Int32 oldRole = account.Type;
                var hasChange = HasChange(account, model, ref changes);

                var oldCompanyId = account.CompanyId;

                Mapper.Map(model, account);

                //account.CompanyId = account.CompanyId == 0 ? null : account.CompanyId;
                _unitOfWork.AccountRepository.Update(account);

                var newCompanyId = account.CompanyId;

                //Save system log

                if (oldCompanyId == newCompanyId && oldCompanyId != 0)
                {
                    // In this case, company info has not changed.
                    // check change about extra data.
                    if (hasChange)
                    {
                        // Save the system log type as update.
                        var content = $"{AccountResource.lblUpdate}\n{AccountResource.lblEmail} : {account.Username}";
                        var contentsDetails = string.Join("\n", changes);
                        _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Update,
                            content, contentsDetails, null, oldCompanyId);

                        if (oldRole != model.Role)
                        {
                            // [Edward] 2020.04.08
                            // Send Email to manager ( employee -> manager )
                            if (oldRole == (short)AccountType.Employee)
                            {
                                var token = GetTokenByAccount(account);
                                SendWelcomeMailForManager(account.Username, token, oldCompanyId.Value);
                            }
                            if (model.Role == (short)AccountType.PrimaryManager)
                            {
                                AddPrimaryManager(model, account);

                            }
                        }
                    }
                }
                else
                {
                    // In this case, company info has changed.
                    // It is not important if extra data is changed or not.
                    // Just save the system log as "remove" or "add" to old or new company.
                    if (oldCompanyId != null)
                    {
                        // save the "Remove" system log to old company
                        var content = string.Format(AccountResource.lblDelete);
                        var contentsDetails = $"{AccountResource.lblEmail} : {account.Username}";

                        _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Delete,
                            content, contentsDetails, null, oldCompanyId);
                        DeletePrimaryManager(model, Convert.ToInt32(oldCompanyId));
                    }

                    if (newCompanyId != null && newCompanyId != 0)
                    {
                        // save the "Add" system log to new company
                        var content = string.Format(AccountResource.lblAddNew);

                        List<string> details = new List<string>
                                {
                                    $"{AccountResource.lblEmail} : {account.Username}",
                                    $"{AccountResource.lblRole} : {((AccountType)account.Type).GetDescription()}"
                                };

                        var contentsDetails = string.Join("\n", details);

                        _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Add,
                            content, contentsDetails, null, newCompanyId);
                        if (model.Role == (short)AccountType.PrimaryManager)
                        {
                            AddPrimaryManager(model, account);
                        }
                    }
                }

                //Save and commit
                _unitOfWork.Save();
                //transaction.Commit();
            }
            catch (Exception)
            {
                //transaction.Rollback();
                throw;
            }
            //    }
            //});
        }

        /// <summary>
        /// Update account
        /// </summary>
        /// <param name="model"></param>
        public void UpdateTimeZone(AccountTimeZoneModel model, string userTimeZone = "")
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var account = GetById(model.Id);
                        if (account != null)
                        {
                            if (!string.IsNullOrEmpty(userTimeZone))
                            {
                                account.TimeZone = userTimeZone;
                            }
                        }

                        //Save and commit
                        _unitOfWork.AccountRepository.Update(account);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete account
        /// </summary>
        /// <param name="account"></param>
        public void Delete(Account account)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        account.IsDeleted = true;
                        _unitOfWork.AccountRepository.Update(account);

                        //if (account.CompanyId != 0)
                        //{
                        //Save system log
                        var content = string.Format(AccountResource.lblDelete);
                        var contentsDetails = $"{AccountResource.lblEmail} : {account.Username}";
                        _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Delete,
                                content, contentsDetails, null, account.CompanyId);
                        //}

                        //Save and commit
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a list of account
        /// </summary>
        /// <param name="accounts"></param>
        public void DeleteRange(List<Account> accounts)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var account in accounts)
                        {
                            account.IsDeleted = true;

                            //Save system log
                            var content = string.Format(AccountResource.lblDelete);
                            var contentsDetails = $"{AccountResource.lblEmail} : {account.Username}";
                            _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Delete,
                                content, contentsDetails, null, account.CompanyId);

                            _unitOfWork.AccountRepository.Update(account);
                        }

                        //Save and commit
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Get account by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Account GetById(int id)
        {
            var account = _unitOfWork.AppDbContext.Account.FirstOrDefault(m => m.Id == id && !m.IsDeleted);

            if (account != null)
            {
                if (account.CompanyId != 0)
                {
                    var company = _companyService.GetById(account.CompanyId ?? 0);
                    account.Company = company;
                }
                else
                {
                    //account.Company = new Company();
                }
            }

            //return _unitOfWork.AppDbContext.Account/*.Include(m => m.Company)*/.FirstOrDefault(m =>
            //    m.Id == id && !m.IsDeleted /*&& !m.Company.IsDeleted*/);
            return account;
        }

        /// <summary>
        /// Get list of account by ids
        /// </summary>
        /// <param name="idArr"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Account> GetAccountsByIds(List<int> idArr, int companyId)
        {
            var accounts = _unitOfWork.AppDbContext.Account/*.Include(m => m.Company)*/.Where(m =>
                     idArr.Any(x => x == m.Id) /*&& m.CompanyId == companyId*/ /*&& !m.Company.IsDeleted*/);

            if (companyId == 0)
            {
                return accounts.ToList();
            }
            else
            {
                return accounts.Where(m => m.CompanyId == companyId).ToList();
            }

            //return _unitOfWork.AppDbContext.Account/*.Include(m => m.Company)*/.Where(m =>
            //         idArr.Any(x => x == m.Id) && m.CompanyId == companyId /*&& !m.Company.IsDeleted*/)
            //    .ToList();
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<Account> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var accountType = _httpContext.User.GetAccountType();

            IQueryable<Account> data = _unitOfWork.AppDbContext.Account/*.Include(m => m.Company)*/
                .Where(m => !m.IsDeleted /*&& m.CompanyId == companyId*/ && m.Type >= accountType);

            if (companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId && m.Company.IsDeleted == false);
            }

            foreach (var eachData in data)
            {
                var company = _companyService.GetById(eachData.CompanyId ?? 0);

                if (company != null)
                {
                    eachData.Company = company;
                }
                else
                {
                    eachData.Company = new Company();
                }
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Username.ToUpper().Contains(filter.ToUpper()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Account.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Account[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        /// <summary>
        /// Check if account is exist (id, name, companyId)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool IsExist(int id, string username, int? companyId = null)
        {
            if (companyId == null)
            {
                companyId = _httpContext.User.GetCompanyId();
            }

            var userLogin = _unitOfWork.AccountRepository.Get(m =>
                m.Username == username && m.CompanyId == companyId && !m.IsDeleted && m.Id == id);

            return userLogin != null;
        }

        /// <summary>
        /// Check if account is exist ( only name )
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool IsExist(string username)
        {
            var userLogin = _unitOfWork.AccountRepository.Get(m =>
                m.Username.ToLower() == username.ToLower() && !m.IsDeleted);

            return userLogin != null;
        }

        /// <summary>
        /// Get account by company code and username
        /// </summary>
        /// <param name="companyCode"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public Account GetAccountByCompanyCodeAndUsername(string companyCode, string username)
        {
            return _unitOfWork.AppDbContext.Account.Include(m => m.Company).FirstOrDefault(m =>
                m.Company.Code == companyCode && m.Username == username && !m.IsDeleted &&
                !m.Company.IsDeleted);
        }

        /// <summary>
        /// Get authentication account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Account GetAuthenticatedAccount(LoginModel model)
        {
            var account = _unitOfWork.AppDbContext.Account/*.Include(m => m.Company)*/
                .FirstOrDefault(m =>
                    m.Username.ToLower() == model.Username.ToLower() /*&& m.Company.Code == model.CompanyCode*/ &&
                    !m.IsDeleted /*&& !m.Company.IsDeleted /*&& m.Status == (short)Status.Valid*/);

            if (account != null)
            {
                if (account.CompanyId != 0)
                    account.Company = _companyService.GetById(account.CompanyId ?? 0);

                if (SecurePasswordHasher.Verify(model.Password, account.Password))
                {
                    return account;
                }
            }
            return null;
        }

        /// <summary>
        /// Get account roles
        /// </summary>
        /// <param name="selected"></param>
        /// <returns></returns>
        public IEnumerable<EnumModel> GetAccountRoles()
        {
            var accountType = _httpContext.User.GetAccountType();
            return EnumHelper.ToEnumList<AccountType>();
        }

        /// <summary>
        /// Get account role by selected
        /// </summary>
        /// <param name="selected"></param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetAccountRoles(short? selected)
        {
            var accountType = _httpContext.User.GetAccountType();
            return EnumHelper.ToSelectList<AccountType>(selected).Where(m =>
                Convert.ToInt16(m.Value) >= accountType);
        }

        /// <summary>
        /// Get root account by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Account GetRootAccountByCompany(int companyId)
        {
            return _unitOfWork.AccountRepository.GetRootAccountByCompany(companyId);
        }

        /// <summary>
        /// Get valid account
        /// </summary>
        /// <param name="username"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Account GetValidAccount(string username, int companyId)
        {
            if (companyId == 0)
            {
                return _unitOfWork.AppDbContext.Account
                .FirstOrDefault(m => m.Username == username && !m.IsDeleted);
            }

            var account = _unitOfWork.AppDbContext.Account.Include(m => m.Company)
                .FirstOrDefault(m =>
                    m.Username == username && m.CompanyId == companyId &&
                    !m.IsDeleted && !m.Company.IsDeleted);

            if (account != null && _companyService.IsValidCompany(account.Company))
            {
                return account;
            }

            return null;
        }

        /// <summary>
        /// Check if account is allowed delete
        /// </summary>
        /// <param name="account"></param>
        /// <param name="accountLogin"></param>
        /// <returns></returns>
        public bool IsAllowDelete(Account account, Account accountLogin)
        {
            if (account.Id == accountLogin.Id)
            {
                return false;
            }

            if (account.RootFlag)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get login account
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Account GetAccountLogin(ClaimsPrincipal user)
        {
            return GetValidAccount(user.GetUsername(), user.GetCompanyId());
        }

        /// <summary>
        /// Change the password
        /// </summary>
        /// <param name="account"></param>
        public void ChangePassword(Account account)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _unitOfWork.AccountRepository.Update(account);

                        //Save system log
                        var content = $"{AccountResource.lblUpdate}\n{AccountResource.lblEmail} : {account.Username}";
                        var contentsDetails = string.Format(AccountResource.msgChangePassword, account.Username);
                        _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Update,
                            content, contentsDetails, null, account.CompanyId, account.Id);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }



        ///// <summary>
        ///// Send new account email
        ///// </summary>
        ///// <param name="email"></param>
        ///// <param name="originalPass"></param>
        //public void SendAccountMail(string email, string originalPass, string language)
        //{
        //    _logger.LogError("########## existing mail function 1 ");

        //    var thread = new Thread(delegate ()
        //    {
        //        _logger.LogError("########## existing mail function 2 ");

        //        var companyUrl = _configuration.GetSection("WebApp:Host").Value;
        //        var culture = new CultureInfo(language);
        //        var formatBody = MailContentResource.ResourceManager.GetString("BodyCompanyAccount", culture);
        //        var subject = MailContentResource.ResourceManager.GetString("SubjectCompanyAccount", culture);
        //        var body = string.Format(formatBody
        //            , email
        //            , companyUrl
        //            , companyUrl
        //            , email);

        //        _logger.LogError("########## existing mail function 3 ");

        //        _mailService.SendMail(email, null
        //            , subject
        //            , body);

        //        _logger.LogError("########## existing mail function 4 ");
        //    });
        //    thread.Start();
        //}


        /// <summary>
        /// Send email when registered a new user in system.
        /// </summary>
        /// <param name="email"> email address </param>
        /// <param name="name">  user's first name </param>
        public void SendWelcomeMail(string email, string userName, string token, int companyId)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var companyLanguage = _settingService.GetLanguage(companyId);
                var culture = new CultureInfo(companyLanguage);

                var supportMail = _mailService.GetSupportMailAddress();

                var subject = MailContentResource.ResourceManager.GetString("SubjectCompanyAccount", culture);

                var downloadLink_Andoid = Constants.Link.Android_Download;
                var downloadLink_IOS = Constants.Link.IOS_Download;

                var frontendURL = _mailService.GetFrontEndURL();

                var resetLink = frontendURL + "/reset-password/" + token;

                var thread = new Thread(delegate ()
                {
                    var pathToTemplateFile = _mailService.GetPathToTemplateFile("Welcome_User_Email.html");

                    var pathToLogoImage = _mailService.GetPathToImageFile("logo.png");
                    var b64String = _mailService.ConvertImageToBase64(pathToLogoImage);
                    var logoImageUrl = "data:image/png;base64, " + b64String;

                    var pathToAndroidImage = _mailService.GetPathToImageFile("android_icon.png");
                    b64String = _mailService.ConvertImageToBase64(pathToAndroidImage);
                    var androidImageUrl = "data:image/png;base64, " + b64String;

                    var pathToIOSImage = _mailService.GetPathToImageFile("ios_icon.png");
                    b64String = _mailService.ConvertImageToBase64(pathToIOSImage);
                    var iosImageUrl = "data:image/png;base64, " + b64String;

                    BodyBuilder builder = new BodyBuilder();

                    try
                    {
                        using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                        {
                            builder.HtmlBody = SourceReader.ReadToEnd();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }

                    string password = String.Format(MailContentResource.ResourceManager.GetString("BodyWelcomeUserPassword", culture), resetLink);
                    string welcome = MailContentResource.ResourceManager.GetString("BodyWelcome", culture);
                    string welcomeGreeting = String.Format(MailContentResource.ResourceManager.GetString("BodyWelcomeUserGreeting", culture), userName);
                    string downloadExplain = String.Format(MailContentResource.ResourceManager.GetString("BodyWelcomeDownloadExplain", culture));
                    string downloadButton_Android = MailContentResource.ResourceManager.GetString("BodyWelcomeAndroidDownload", culture);
                    string downloadButton_IOS = MailContentResource.ResourceManager.GetString("BodyWelcomeIOSDownload", culture);
                    string customerSupport = String.Format(MailContentResource.ResourceManager.GetString("BodyCustomerSupport", culture),
                                                        MailContentResource.ResourceManager.GetString("BodyWorkingTimeInfo", culture), supportMail);
                    string replyMessage = String.Format(MailContentResource.ResourceManager.GetString("BodyReplyMessage", culture), supportMail);

                    string mailBody = string.Format(builder.HtmlBody,
                                                    downloadLink_Andoid,
                                                    downloadLink_IOS,
                                                    "Download Link",
                                                    email,
                                                    password,
                                                    welcome,
                                                    welcomeGreeting,
                                                    downloadExplain,
                                                    downloadButton_Android,
                                                    downloadButton_IOS,
                                                    androidImageUrl,
                                                    iosImageUrl,
                                                    customerSupport,
                                                    replyMessage,
                                                    logoImageUrl);

                    _mailService.SendMail(email, null, subject, mailBody);
                });
                thread.Start();
            }
        }

        /// <summary>
        /// Send email when registered a new Manager Account in system.
        /// </summary>
        /// <param name="email"> email address </param>
        public void SendWelcomeMailForManager(string email, string token, int companyId)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var companyLanguage = _settingService.GetLanguage(companyId);
                var culture = new CultureInfo(companyLanguage);

                var supportMail = _mailService.GetSupportMailAddress();

                var subject = MailContentResource.ResourceManager.GetString("SubjectCompanyAccount", culture);

                var downloadLink_Andoid = Constants.Link.Android_Download;
                var downloadLink_IOS = Constants.Link.IOS_Download;

                var frontendURL = _mailService.GetFrontEndURL();

                var resetLink = frontendURL + "/reset-password/" + token;

                var thread = new Thread(delegate ()
                {
                    var pathToTemplateFile = _mailService.GetPathToTemplateFile("Welcome_Email.html");

                    var pathToImage = _mailService.GetPathToImageFile("logo.png");
                    var b64String = _mailService.ConvertImageToBase64(pathToImage);
                    var imageUrl = "data:image/png;base64," + b64String;

                    var pathToAndroidImage = _mailService.GetPathToImageFile("android_icon.png");
                    b64String = _mailService.ConvertImageToBase64(pathToAndroidImage);
                    var androidImageUrl = "data:image/png;base64, " + b64String;

                    var pathToIOSImage = _mailService.GetPathToImageFile("ios_icon.png");
                    b64String = _mailService.ConvertImageToBase64(pathToIOSImage);
                    var iosImageUrl = "data:image/png;base64, " + b64String;

                    BodyBuilder builder = new BodyBuilder();

                    try
                    {
                        using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                        {
                            builder.HtmlBody = SourceReader.ReadToEnd();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }

                    string userName = email;
                    string password = String.Format(MailContentResource.ResourceManager.GetString("BodyWelcomeManagerPassword", culture), resetLink);
                    string welcome = MailContentResource.ResourceManager.GetString("BodyWelcome", culture);
                    string welcomeGreeting = String.Format(MailContentResource.ResourceManager.GetString("BodyWelcomeManagerGreeting", culture));
                    string loginButton = MailContentResource.ResourceManager.GetString("BodyWelcomeLogin", culture);
                    string downloadButton_Android = MailContentResource.ResourceManager.GetString("BodyWelcomeAndroidDownload", culture);
                    string downloadButton_IOS = MailContentResource.ResourceManager.GetString("BodyWelcomeIOSDownload", culture);
                    string customerSupport = String.Format(MailContentResource.ResourceManager.GetString("BodyCustomerSupport", culture),
                                                        MailContentResource.ResourceManager.GetString("BodyWorkingTimeInfo", culture), supportMail);
                    string replyMessage = String.Format(MailContentResource.ResourceManager.GetString("BodyReplyMessage", culture), supportMail);

                    string downloadExplaination = MailContentResource.ResourceManager.GetString("BodyDownloadExplain", culture);

                    string mailBody = string.Format(builder.HtmlBody,
                                                    frontendURL,
                                                    "Portal URL",
                                                    userName,
                                                    password,
                                                    welcome,
                                                    welcomeGreeting,
                                                    loginButton,
                                                    downloadExplaination,
                                                    downloadLink_Andoid,
                                                    downloadLink_IOS,
                                                    androidImageUrl,
                                                    iosImageUrl,
                                                    "Download Link",
                                                    downloadButton_Android,
                                                    downloadButton_IOS,
                                                    customerSupport,
                                                    replyMessage,
                                                    imageUrl);

                    _mailService.SendMail(email, null, subject, mailBody);

                });
                thread.Start();
            }
        }

        /// <summary>
        /// Send email to (system) administrator when they are registered to system.
        /// </summary>
        /// <param name="email"> email address </param>
        public void SendWelcomeMailForAdmin(string email, string token)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var supportMail = _mailService.GetSupportMailAddress();

                var subject = MailContentResource.SubjectCompanyAccount;

                var downloadLink_Andoid = Constants.Link.Android_Download;
                var downloadLink_IOS = Constants.Link.IOS_Download;

                var frontendURL = _mailService.GetFrontEndURL();

                var resetLink = frontendURL + "/reset-password/" + token;

                var thread = new Thread(delegate ()
                {
                    var pathToTemplateFile = _mailService.GetPathToTemplateFile("Welcome_Email.html");

                    var pathToImage = _mailService.GetPathToImageFile("logo.png");
                    var b64String = _mailService.ConvertImageToBase64(pathToImage);
                    var imageUrl = "data:image/png;base64," + b64String;

                    var pathToAndroidImage = _mailService.GetPathToImageFile("android_icon.png");
                    b64String = _mailService.ConvertImageToBase64(pathToAndroidImage);
                    var androidImageUrl = "data:image/png;base64, " + b64String;

                    var pathToIOSImage = _mailService.GetPathToImageFile("ios_icon.png");
                    b64String = _mailService.ConvertImageToBase64(pathToIOSImage);
                    var iosImageUrl = "data:image/png;base64, " + b64String;

                    BodyBuilder builder = new BodyBuilder();

                    try
                    {
                        using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                        {
                            builder.HtmlBody = SourceReader.ReadToEnd();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }

                    string userName = email;
                    string password = String.Format(MailContentResource.BodyWelcomeManagerPassword, resetLink);
                    string welcome = MailContentResource.BodyWelcome;
                    string welcomeGreeting = String.Format(MailContentResource.BodyWelcomeAdminGreeting);
                    string loginButton = MailContentResource.BodyWelcomeLogin;
                    string downloadButton_Android = MailContentResource.ResourceManager.GetString("BodyWelcomeAndroidDownload");
                    string downloadButton_IOS = MailContentResource.ResourceManager.GetString("BodyWelcomeIOSDownload");
                    string customerSupport = String.Format(MailContentResource.BodyCustomerSupport,
                                                        MailContentResource.BodyWorkingTimeInfo, supportMail);
                    string replyMessage = String.Format(MailContentResource.BodyReplyMessage, supportMail);

                    string downloadExplaination = MailContentResource.ResourceManager.GetString("BodyDownloadExplain");

                    string mailBody = string.Format(builder.HtmlBody,
                                                    frontendURL,
                                                    "Portal URL",
                                                    userName,
                                                    password,
                                                    welcome,
                                                    welcomeGreeting,
                                                    loginButton,
                                                    downloadExplaination,
                                                    downloadLink_Andoid,
                                                    downloadLink_IOS,
                                                    androidImageUrl,
                                                    iosImageUrl,
                                                    "Download Link",
                                                    downloadButton_Android,
                                                    downloadButton_IOS,
                                                    customerSupport,
                                                    replyMessage,
                                                    imageUrl);

                    _mailService.SendMail(email, null, subject, mailBody);

                });
                thread.Start();
            }
        }

        /// <summary>
        /// Send update mail to user when user infomation(Email) is changed.
        /// (Employee -> manager)
        /// </summary>
        /// <param name="email"> email address </param>
        /// <param name="name"> user name </param>
        public void SendUpdateMail(string email, string userName, string token, int companyId)
        {
            if (!string.IsNullOrEmpty(email))
            {
                var companyLanguage = _settingService.GetLanguage(companyId);
                var culture = new CultureInfo(companyLanguage);

                var supportMail = _mailService.GetSupportMailAddress();

                var subject = MailContentResource.ResourceManager.GetString("SubjectCompanyUpdate", culture);

                var frontendURL = _mailService.GetFrontEndURL();

                var downloadLink = "download link(TODO)";

                var resetLink = frontendURL + "/reset-password/" + token;

                var thread = new Thread(delegate ()
                {
                    var pathToTemplateFile = _mailService.GetPathToTemplateFile("Welcome_Email.html");

                    var pathToImage = _mailService.GetPathToImageFile("logo.png");
                    var b64String = _mailService.ConvertImageToBase64(pathToImage);
                    var imageUrl = "data:image/png;base64," + b64String;

                    BodyBuilder builder = new BodyBuilder();

                    try
                    {
                        using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                        {
                            builder.HtmlBody = SourceReader.ReadToEnd();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }

                    string password = String.Format(MailContentResource.ResourceManager.GetString("BodyWelcomeManagerPassword", culture), resetLink);
                    string updated = MailContentResource.ResourceManager.GetString("BodyUpdated", culture);
                    string updatedGreeting = String.Format(MailContentResource.ResourceManager.GetString("BodyUpdatedGreeting", culture), userName);
                    string downloadButton = MailContentResource.ResourceManager.GetString("BodyWelcomeDownload", culture);
                    string customerSupport = String.Format(MailContentResource.ResourceManager.GetString("BodyCustomerSupport", culture),
                                                        MailContentResource.ResourceManager.GetString("BodyWorkingTimeInfo", culture), supportMail);
                    string replyMessage = String.Format(MailContentResource.ResourceManager.GetString("BodyReplyMessage", culture), supportMail);

                    string mailBody = string.Format(builder.HtmlBody,
                                                    downloadLink,
                                                    "Download Link",
                                                    email,
                                                    password,
                                                    updated,
                                                    updatedGreeting,
                                                    downloadButton,
                                                    customerSupport,
                                                    replyMessage,
                                                    imageUrl);

                    _mailService.SendMail(email, null, subject, mailBody);

                });
                thread.Start();
            }
        }


        /// <summary>
        /// Send reset account email
        /// </summary>
        /// <param name="email"></param>
        /// <param name="token"></param>
        public void SendResetAccountMail(Account account, int companyId)
        {
            var email = account.Username;

            if (!string.IsNullOrEmpty(email))
            {
                string companyLanguage = "";

                if (companyId == 0)
                    companyLanguage = "en-US";
                else
                    companyLanguage = _settingService.GetLanguage(companyId);

                var culture = new CultureInfo(companyLanguage);

                var supportMail = _mailService.GetSupportMailAddress();

                var subject = MailContentResource.ResourceManager.GetString("SubjectResetAccount", culture);

                var frontendURL = _mailService.GetFrontEndURL();


                var userName = GetUserNameByEmail(account.CompanyId ?? 0, email);
                var token = GetTokenByAccount(account);

                var resetLink = frontendURL + "/reset-password/" + token;

                var thread = new Thread(delegate ()
                {
                    var pathToTemplateFile = _mailService.GetPathToTemplateFile("Plain_Email.html");

                    var pathToImage = _mailService.GetPathToImageFile("logo.png");
                    var b64String = _mailService.ConvertImageToBase64(pathToImage);
                    var imageUrl = "data:image/png;base64," + b64String;

                    BodyBuilder builder = new BodyBuilder();

                    try
                    {
                        using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                        {
                            builder.HtmlBody = SourceReader.ReadToEnd();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }

                    string password = String.Format(MailContentResource.ResourceManager.GetString("BodyResetPassword", culture), resetLink);
                    var contents = string.Format(MailContentResource.ResourceManager.GetString("BodyResetAccount", culture), userName, password);
                    string customerSupport = String.Format(MailContentResource.ResourceManager.GetString("BodyCustomerSupport", culture),
                                                        MailContentResource.ResourceManager.GetString("BodyWorkingTimeInfo", culture), supportMail);
                    string replyMessage = String.Format(MailContentResource.ResourceManager.GetString("BodyReplyMessage", culture), supportMail);

                    string mailBody = string.Format(builder.HtmlBody,
                                                    contents,
                                                    customerSupport,
                                                    replyMessage,
                                                    imageUrl);

                    _mailService.SendMail(email, null, subject, mailBody);

                });
                thread.Start();
            }

        }

        /// <summary>
        /// Send email when registered a new visitor in system.
        /// </summary>
        /// <param name="visitor"> new visitor </param>
        public void SendRequestApprovalMail(Visit visitor)
        {
            if (visitor.ApproverId1 != 0)
            {
                Account account = GetAccountByCurrentCompany(visitor.ApproverId1);
                var email = account.Username;

                if (account != null && !string.IsNullOrEmpty(email))
                {
                    var approverName = GetUserNameByEmail(visitor.CompanyId, email);

                    var companyLanguage = _settingService.GetLanguage(visitor.CompanyId);
                    var culture = new CultureInfo(companyLanguage);

                    var supportMail = _mailService.GetSupportMailAddress();

                    var subject = MailContentResource.ResourceManager.GetString("SubjectRequestApproval", culture);

                    var token = GetTokenByAccount(account);

                    var frontendURL = _mailService.GetFrontEndURL();
                    var visitLink = frontendURL + "/visit";

                    var waitCount = GetRequestFirstApprovalCount(account) - 1;

                    var thread = new Thread(delegate ()
                    {
                        var pathToTemplateFile = _mailService.GetPathToTemplateFile("Approval_Email.html");

                        var pathToLogoImage = _mailService.GetPathToImageFile("logo.png");
                        var b64String = _mailService.ConvertImageToBase64(pathToLogoImage);
                        var logoImageUrl = "data:image/png;base64, " + b64String;

                        BodyBuilder builder = new BodyBuilder();

                        try
                        {
                            using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                            {
                                builder.HtmlBody = SourceReader.ReadToEnd();
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("The process failed: {0}", e.ToString());
                        }

                        string approvalGreeting = String.Format(MailContentResource.ResourceManager.GetString("BodyApprovalVisit", culture), approverName);

                        string lblVisitorName = String.Format(MailContentResource.ResourceManager.GetString("BodyVisitorName", culture));
                        string visitorName = visitor.VisitorName;

                        string lblVisitorContact = String.Format(MailContentResource.ResourceManager.GetString("BodyVisitorContact", culture));
                        string visitorContact = visitor.Phone;

                        string lblVisitTargetName = String.Format(MailContentResource.ResourceManager.GetString("BodyVisitTargetName", culture));
                        string visitTargetName = visitor.VisiteeName;

                        string lblVisitPurpose = String.Format(MailContentResource.ResourceManager.GetString("BodyVisitPurpose", culture));
                        string visitPurpose = visitor.VisitReason;

                        string approvalCount = String.Format(MailContentResource.ResourceManager.GetString("BodyApprovalCount", culture), waitCount);

                        string accessToWeb = String.Format(MailContentResource.ResourceManager.GetString("BodyLinkToWeb", culture));
                        string visitorButton = String.Format(MailContentResource.ResourceManager.GetString("BodyButtonVisitor", culture));

                        string customerSupport = String.Format(MailContentResource.ResourceManager.GetString("BodyCustomerSupport", culture),
                                                            MailContentResource.ResourceManager.GetString("BodyWorkingTimeInfo", culture), supportMail);
                        string replyMessage = String.Format(MailContentResource.ResourceManager.GetString("BodyReplyMessage", culture), supportMail);

                        string mailBody = string.Format(builder.HtmlBody,
                                                        logoImageUrl,
                                                        approvalGreeting,
                                                        lblVisitorName,
                                                        visitorName,
                                                        lblVisitorContact,
                                                        visitorContact,
                                                        lblVisitTargetName,
                                                        visitTargetName,
                                                        lblVisitPurpose,
                                                        visitPurpose,
                                                        waitCount <= 0 ? "" : approvalCount,
                                                        accessToWeb,
                                                        visitLink,
                                                        visitorButton,
                                                        customerSupport,
                                                        replyMessage
                                                        );

                        _mailService.SendMail(email, null, subject, mailBody);
                    });
                    thread.Start();
                }
            }
        }


        /// <summary>
        /// Send QR code email
        /// </summary>
        /// <param name="visitor"> new visitor </param>
        public void SendQRMail(Visit visitor, Image QrImage)
        {
            if (!string.IsNullOrEmpty(visitor.Email) && IsEmailValid(visitor.Email))
            {
                var email = visitor.Email;

                var visitorName = visitor.VisitorName;

                var companyName = _unitOfWork.CompanyRepository.GetById(visitor.CompanyId).Name;

                var companyLanguage = _settingService.GetLanguage(visitor.CompanyId);
                var culture = new CultureInfo(companyLanguage);

                var supportMail = _mailService.GetSupportMailAddress();

                var subject = MailContentResource.ResourceManager.GetString("SubjectQRmail", culture);

                var frontendURL = _mailService.GetFrontEndURL();

                var thread = new Thread(delegate ()
                {
                    var pathToTemplateFile = _mailService.GetPathToTemplateFile("QR_Email.html");

                    var pathToLogoImage = _mailService.GetPathToImageFile("logo.png");
                    var b64String = _mailService.ConvertImageToBase64(pathToLogoImage);
                    var logoImageUrl = "data:image/png;base64, " + b64String;

                    var stream = new MemoryStream();
                    QrImage.Save(stream, ImageFormat.Png);
                    stream.Position = 0;

                    byte[] imageBytes = stream.ToArray();
                    var base64QRcode = Convert.ToBase64String(imageBytes);
                    var qrImageUrl = "data:image/png;base64, " + base64QRcode;

                    BodyBuilder builder = new BodyBuilder();

                    try
                    {
                        using (StreamReader SourceReader = File.OpenText(pathToTemplateFile))
                        {
                            builder.HtmlBody = SourceReader.ReadToEnd();
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }

                    string approvalGreeting = String.Format(MailContentResource.ResourceManager.GetString("BodyApprovedVisit", culture), visitorName);

                    var usingQR = string.Format(MailContentResource.ResourceManager.GetString("BodySendQRtoVisit", culture), companyName);
                    var lostQR = string.Format(MailContentResource.ResourceManager.GetString("BodyLostQR", culture));

                    string customerSupport = String.Format(MailContentResource.ResourceManager.GetString("BodyCustomerSupport", culture),
                                                        MailContentResource.ResourceManager.GetString("BodyWorkingTimeInfo", culture), supportMail);
                    string replyMessage = String.Format(MailContentResource.ResourceManager.GetString("BodyReplyMessage", culture), supportMail);

                    string mailBody = string.Format(builder.HtmlBody,
                                                    logoImageUrl,
                                                    approvalGreeting,
                                                    usingQR,
                                                    qrImageUrl,
                                                    lostQR,
                                                    customerSupport,
                                                    replyMessage
                                                    );

                    _mailService.SendMail(email, null, subject, mailBody, stream);
                });
                thread.Start();

            }
        }

        /// <summary>
        /// get token through account infomation
        /// </summary>
        /// <param name="account"> Account </param>
        /// <returns> token </returns>
        public string GetTokenByAccount(Account account)
        {
            var claims = new[]
            {
                new Claim(Constants.ClaimName.Username, account.Username),
                new Claim(Constants.ClaimName.AccountId, account.Id.ToString()),
                new Claim(Constants.ClaimName.CompanyId, account.Type == (short)AccountType.SystemAdmin ? "0" :account.CompanyId.ToString()),
                new Claim(Constants.ClaimName.CompanyCode, account.Type == (short)AccountType.SystemAdmin ? "s000001" : account.Company.Code),
                new Claim(Constants.ClaimName.CompanyName, account.Type == (short)AccountType.SystemAdmin ? "SystemAdmin" : account.Company.Name),
                new Claim(Constants.ClaimName.AccountType, account.Type.ToString())
            };

            var token = _jwtHandler.BuilToken(claims);

            return token;
        }

        /// <summary>
        /// Get user name by email address
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public string GetUserNameByEmail(int companyId, string emailAddress)
        {
            if (emailAddress == null)
            {
                return null;
            }

            var user = _unitOfWork.UserRepository.GetUserByEmail(companyId, emailAddress);

            if (user != null)
            {
                return user.FirstName + user.LastName;
            }
            else
            {
                return emailAddress;
            }

        }

        /// <summary>
        /// Get the number of visitors waiting for first approval
        /// </summary>
        /// <returns></returns>
        public int GetRequestFirstApprovalCount(Account account)
        {
            var IsArmy = IsEnabledPlugInByAccount(account, Constants.PlugIn.ArmyManagement);

            var visitor = _unitOfWork.AppDbContext.Visit.Include(v => v.VisitArmy)
                                                        .Where(v => v.CompanyId == _httpContext.User.GetCompanyId()
                                                                    && v.Status == (short)VisitChangeStatusType.Waiting && v.ApproverId1 == account.Id
                                                                    && !v.IsDeleted);

            if (IsArmy)
            {
                visitor = visitor.Where(m => m.VisitArmy.Select(c => c.VisitId).Contains(m.Id));
                // Since this visitor has not yet been stored to VisitArmy, add "1" to count this visitor.
                return visitor.Count() + 1;
            }

            return visitor.Count();
        }

        /// <summary>
        /// Check if the email is valid or not
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        internal bool IsEmailValid(string emailAddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailAddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Get account by account Id.
        /// This function searches account through accountId only within the current company.
        /// </summary>
        /// <param name="accountId"> identifier of account </param>
        /// <returns></returns>
        public Account GetAccountByCurrentCompany(int accountId)
        {
            int companyId = _httpContext.User.GetCompanyId();

            return _unitOfWork.AppDbContext.Account.Include(m => m.Company)
                .FirstOrDefault(m => m.Id == accountId && !m.IsDeleted && m.CompanyId == companyId);
        }

        /// <summary>
        /// Initial data
        /// </summary>
        /// <param name="model"></param>
        public AccountDataModel InitData(AccountDataModel model)
        {
            model.CompanyIdList = null;
            model.RoleList = null;

            model.CompanyId = model.CompanyId.ToString() == null ? 0 : model.CompanyId;

            return model;
        }

        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="account">Account that contains existing information</param>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool HasChange(Account account, AccountModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                if (account.Username != model.Username)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, AccountResource.lblEmail, account.Username, model.Username));
                }

                if (account.Type != model.Role)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo,
                                                AccountResource.lblRole,
                                                ((AccountType)account.Type).GetDescription(),
                                                ((AccountType)model.Role).GetDescription()));
                }

                if (!(string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.ConfirmPassword)))
                {
                    changes.Add(string.Format(AccountResource.msgChangePassword, model.Username));
                }
            }

            return changes.Count() > 0;
        }

        /// <summary>
        /// Check whether account type is valid
        /// </summary>
        /// <param name="accountType"></param>
        /// <returns></returns>
        public bool IsValidAccountType(short accountType)
        {
            var validAccountsType = EnumHelper.ToSelectList<AccountType>().Select(m => m.Value).ToList();
            return validAccountsType.Contains(accountType.ToString());
        }

        /// <summary>
        /// Get by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public Account GetAccountByEmail(string email)
        {
            return _unitOfWork.AppDbContext.Account.Include(m => m.Company)
                .FirstOrDefault(m => m.Username == email && !m.IsDeleted);
        }

        /// <summary>
        /// Save system log about login
        /// </summary>
        /// <param name="account">logged in account</param>
        public void SaveSystemLogLogIn(Account account)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Save system log
                        if (account.Company != null)
                        {
                            var content = $"{AccountResource.lblLogin} : {account.Username}";
                            // login doesn't need a contentsDetails.
                            _unitOfWork.SystemLogRepository.Add(account.Id, SystemLogType.AccountManagement, ActionLogType.Login,
                                content, null, null, account.Company.Id, account.Id);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public bool HasDefaultAccount(/*int companyId*/)
        {
            var result = true;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var accountCount = _unitOfWork.AppDbContext.Account.Include(m => m.Company).Where(m =>
                             /*m.CompanyId == companyId && !m.Company.IsDeleted && */!m.IsDeleted && m.RootFlag)
                       .Count();

                        if (accountCount == 0)
                        {
                            _unitOfWork.AccountRepository.AddDefaultAccount(
                                _configuration[Constants.Settings.DefaultAccountUsername],
                                _configuration[Constants.Settings.DefaultAccountUsername]);
                            _unitOfWork.Save();
                            result = false;
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            return result;
        }

        public Dictionary<string, List<string>> GetTimeZone()
        {
            var dictionary = new Dictionary<string, List<string>>();
            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
            {
                List<string> existing;
                var key = "UTC" + " " + z.BaseUtcOffset.ToString().Replace(":00", "");
                if (!dictionary.TryGetValue(key.ToUpper(), out existing))
                {
                    existing = new List<string>();
                    dictionary[key] = existing;
                    existing.Add(z.Id);
                }
                else
                {
                    existing.Add(z.Id);
                }
            }
            return dictionary;
        }

        public Dictionary<string, string> GetUserRabbitCredential(Account account)
        {
            var host = _configuration.GetSection("QueueConnectionSettingsWebApp:Host");
            var virtualHost = _configuration.GetSection("QueueConnectionSettingsWebApp:VirtualHost");
            var port = _configuration.GetSection("QueueConnectionSettingsWebApp:Port");
            var username = _configuration.GetSection("QueueConnectionSettingsWebApp:UserName");
            var password = _configuration.GetSection("QueueConnectionSettingsWebApp:Password");

            Dictionary<string, string> queueService = new Dictionary<string, string>();
            queueService.Add(host.Key, host.Value);
            queueService.Add(port.Key, port.Value);
            queueService.Add(virtualHost.Key, virtualHost.Value);
            if (account.Type != (short)AccountType.SystemAdmin)
            {
                queueService.Add(username.Key, account.Company.Code);
                queueService.Add(password.Key, Convert.ToBase64String(Encoding.UTF8.GetBytes(account.Company.Code)));
            }
            else
            {
                queueService.Add(username.Key, username.Value);
                queueService.Add(password.Key, password.Value);
            }

            return queueService;
        }

        public bool IsSystemAdmin(int accountId)
        {
            if (accountId == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Dictionary<string, string> GetInfomationSystem()
        {
            var api = _configuration.GetSection("Version:Api");
            var webApp = _configuration.GetSection("Version:WebApp");
            var rabbitMQ = _configuration.GetSection("Version:RabbitMQ");
            var postgresql = _configuration.GetSection("Version:Postgresql");
            var supportdevice1 = _configuration.GetSection("Version:ICU300N");
            var supportdevice2 = _configuration.GetSection("Version:DE950");
            var supportdevice3 = _configuration.GetSection("Version:DE960");
            var supportdevice4 = _configuration.GetSection("Version:DQMini");
            var supportdevice5 = _configuration.GetSection("Version:ITouchPop2A");

            Dictionary<string, string> version = new Dictionary<string, string>();
            version.Add(api.Key, api.Value);
            version.Add(webApp.Key, webApp.Value);
            version.Add(rabbitMQ.Key, rabbitMQ.Value);
            version.Add(postgresql.Key, postgresql.Value);
            version.Add(supportdevice1.Key, supportdevice1.Value);
            version.Add(supportdevice2.Key, supportdevice2.Value);
            version.Add(supportdevice3.Key, supportdevice3.Value);
            version.Add(supportdevice4.Key, supportdevice4.Value);
            version.Add(supportdevice5.Key, supportdevice5.Value);

            return version;
        }

        public IQueryable<Account> GetPaginatedPrimaryAccount(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            IQueryable<Account> data = _unitOfWork.AppDbContext.Account
                .Where(m => !m.IsDeleted && m.Type == (short)AccountType.PrimaryManager);

            if (companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId && !m.Company.IsDeleted);
            }

            foreach (var eachData in data)
            {
                var company = _companyService.GetById(eachData.CompanyId ?? 0);

                if (company != null)
                {
                    eachData.Company = company;
                }
                else
                {
                    eachData.Company = new Company();
                }

                var user = _unitOfWork.UserRepository.GetUserByEmail(companyId, eachData.Username);

                if (user != null)
                {
                    eachData.Username = $"{user.FirstName} {user.LastName}({eachData.Username})";
                }
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Username.ToUpper().Contains(filter.ToUpper()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Account.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Account[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        public IEnumerable<EnumModel> GetAccountType()
        {
            var accountTypes = GetAccountRoles();
            if (_httpContext.User.GetAccountType() == (short)AccountType.SystemAdmin)
            {
                var accountType = accountTypes.ToList();

                accountType.RemoveAt((short)AccountType.SuperAdmin);
                accountTypes = accountType;
            }
            else
            {
                var accountType = accountTypes.ToList();

                accountType.RemoveAt((short)AccountType.SuperAdmin);
                accountType.RemoveAt((short)AccountType.SystemAdmin);
                accountTypes = accountType;
            }

            return accountTypes;
        }

        public bool IsExistAccount(int id, int companyId)
        {
            return _unitOfWork.AppDbContext.Account.FirstOrDefault(m => m.Id == id && m.CompanyId == companyId && !m.IsDeleted) != null;
        }


        public void AddTokenAndRefreshTokenToDB(string refreshToken, Account model, int expiryRefreshToken)
        {

            try
            {
                _unitOfWork.AccountRepository.AddTokenAndRefreshToken(refreshToken, model, expiryRefreshToken);

            }
            catch (Exception ex)
            {
                ex.ToString();
            }


        }

        public Account GetAccountByRefreshToken(string refreshToken)
        {
            Account lstAccount = new Account();
            lstAccount = _unitOfWork.AccountRepository.GetAccountByRefreshToken(refreshToken);
            if (lstAccount == null)
            {
                return null;
            }
            if (lstAccount.CompanyId != 0)
                lstAccount.Company = _companyService.GetById(lstAccount.CompanyId ?? 0);
            return lstAccount;
        }
        public string GetRefreshTokenByUserName(string userName, int CompanyId)
        {
            return _unitOfWork.AccountRepository.GetRefreshTokenByUserName(userName, CompanyId);
        }

        public void AddPrimaryManager(AccountModel model, Account account)
        {

            //var lstNoti = _unitOfWork.AppDbContext.Setting.Where(x => x.Key == "list_user_to_notification" && x.CompanyId == account.CompanyId).FirstOrDefault();
            var lstNoti = (from a in _unitOfWork.AppDbContext.Setting where a.Key == "list_user_to_notification" && a.CompanyId == account.CompanyId select a).FirstOrDefault();
            string[] Value = JsonConvert.DeserializeObject<string[]>(lstNoti.Value);
            if (Value.Contains(model.Username))
            {

            }
            else
            {
                int newLength = Value.Length + 1;
                string[] result = new string[newLength];
                for (int i = 0; i < Value.Length; i++)
                {
                    result[i] = Value[i];
                    result[newLength - 1] = model.Username;
                }
                StringBuilder builder = new StringBuilder();
                builder.Append('[');
                foreach (string value in result)
                {
                    builder.Append('"');
                    builder.Append(value);
                    builder.Append('"');
                    builder.Append(',');
                }
                builder.Append(']');

                lstNoti.Value = builder.ToString().TrimEnd(',');
                _unitOfWork.AppDbContext.Setting.Update(lstNoti);
                _unitOfWork.AppDbContext.SaveChanges();
            }
        }

        public void DeletePrimaryManager(AccountModel model, int oldCompanyId)
        {
            var lstNoti = (from a in _unitOfWork.AppDbContext.Setting where a.Key == "list_user_to_notification" && a.CompanyId == oldCompanyId select a).FirstOrDefault();
            string[] Value = JsonConvert.DeserializeObject<string[]>(lstNoti.Value);
            int newLength = Value.Length + 1;
            string[] result = new string[newLength];

            for (int i = 0; i < Value.Length; i++)
            {
                if (Value[i] != model.Username)
                {
                    result[i] = Value[i];
                }

            }
            StringBuilder builder = new StringBuilder();
            builder.Append('[');
            foreach (string value in result)
            {
                builder.Append('"');
                builder.Append(value);
                builder.Append('"');
                builder.Append(',');
            }
            builder.Append(']');
            lstNoti.Value = builder.ToString().TrimEnd(',');
            _unitOfWork.AppDbContext.Setting.Update(lstNoti);
            _unitOfWork.AppDbContext.SaveChanges();
        }


        public bool IsEnabledPlugInByAccount(Account account, string plugInName)
        {
            var companyId = account.CompanyId;

            if (companyId != null)
            {
                var plugIns = _unitOfWork.PlugInRepository.GetPlugInByCompany(companyId.Value);

                var pluginPolicy = JsonConvert.DeserializeObject<Dictionary<string, bool>>(plugIns.PlugIns);

                foreach (KeyValuePair<string, bool> keyPlug in pluginPolicy.ToList())
                {
                    if (plugInName.Equals(keyPlug.Key))
                    {
                        return keyPlug.Value;
                    }
                }

                return false;
            }
            else
            {
                return false;
            }
        }

    }
}