﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.Repository;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Setting service interface
    /// </summary>
    public interface ISettingService
    {
        Setting GetById(int id);
        Setting GetByKey(string key, int companyId);
        string GetLanguage(int companyId);
        void Update(SettingModel model);
        void Update(List<SettingModel> models);
        List<FileSetting> GetAll(int companyId);
        string GetValueFromKey(string key, int companyId);
        void Set(int companyId);
        LogoModel GetCurrentLogo(int companyId);
    }

    public class SettingService : ISettingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;

        public SettingService(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Setting GetById(int id)
        {
            return _unitOfWork.SettingRepository.GetById(id);
        }

        /// <summary>
        /// Get by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Setting GetByKey(string key, int companyId)
        {
            return _unitOfWork.SettingRepository.GetByKey(key, companyId);
        }

        public string GetLanguage(int companyId)
        {
            return Helpers.GetStringFromValueSetting(_unitOfWork.SettingRepository.GetLanguage(companyId).Value);
        }

        /// <summary>
        /// Add a setting
        /// </summary>
        /// <param name="model"></param>
        public void Update(SettingModel model)
        {
            var setting = GetById(model.Id);
            var oldValue = setting.Value;
            var newValue = model.Value;
            Mapper.Map(model, setting);
            _unitOfWork.SettingRepository.Update(setting);

            //Save system log
            var content = string.Format(SettingResource.msgChangeSetting, setting.Key,
                oldValue, newValue);
            _unitOfWork.SystemLogRepository.Add(1, SystemLogType.SystemSetting,
                            ActionLogType.Update, content, null, new List<int> { setting.Id });

            _unitOfWork.Save();
        }

        /// <summary>
        /// Update a list of setting
        /// </summary>
        /// <param name="models"></param>
        public void Update(List<SettingModel> models)
        {
            var strSettings = new List<string>();
            var ids = new List<int>();
            foreach (var model in models)
            {
                var setting = _unitOfWork.SettingRepository.GetById(model.Id);
                setting.Value = JsonConvert.SerializeObject(model.Value);
                _unitOfWork.SettingRepository.Update(setting);
                strSettings.Add(setting.Key + ":" + setting.Value);
                ids.Add(setting.Id);
            }

            //Save system log
            var content = SettingResource.msgChangeSettingMultiple;
            var contentDetails = $"{SettingResource.lblSetting}: {string.Join("\n", strSettings)}";

            _unitOfWork.SystemLogRepository.Add(1, SystemLogType.SystemSetting,
                ActionLogType.Update, content, contentDetails, ids);
            _unitOfWork.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<FileSetting> GetAll(int companyId)
        {
            var settings = _configuration.GetSection(Constants.Settings.FileSettings)?.Get<List<FileSetting>>();
            if (settings != null && settings.Any())
            {
                var settingKeys = settings.Select(m => m.Key).ToList();
                var dbSettings = _unitOfWork.SettingRepository.GetByKeys(settingKeys, companyId);

                var resourceManager = SettingResource.ResourceManager;
                if (dbSettings != null && dbSettings.Any())
                {
                    foreach (var setting in settings)
                    {
                        var dbSetting = dbSettings.First(m => m.Key == setting.Key);
                        setting.Id = dbSetting.Id;
                        setting.Category = resourceManager.GetString(setting.Category);
                        setting.Title = resourceManager.GetString(setting.Title);
                        setting.Description = resourceManager.GetString(setting.Description);
                        setting.Values = JsonConvert.DeserializeObject<string[]>(dbSetting.Value);
                        if (setting.Options != null && setting.Options.Any())
                        {
                            foreach (var option in setting.Options)
                            {
                                option.Key = option.Key;
                                option.Value = resourceManager.GetString(option.Value);
                            }
                        }
                    }
                }
            }
            return settings;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Set(int companyId)
        {
            var settings = _configuration.GetSection(Constants.Settings.FileSettings)?.Get<List<FileSetting>>();
            if (settings != null && settings.Any())
            {
                var dbSettings = _unitOfWork.SettingRepository.GetAll().Select(m => m.Key).ToList();
                if (dbSettings.Any())
                {
                    settings = settings.Where(m => dbSettings.All(x => x != m.Key)).ToList();
                }
                foreach (var unApplySetting in settings)
                {
                    var setting = new Setting
                    {
                        Key = unApplySetting.Key,
                        Value = JsonConvert.SerializeObject(unApplySetting.Values),
                        CompanyId = companyId
                    };
                    _unitOfWork.SettingRepository.Add(setting);
                }
                _unitOfWork.Save();
            }
        }

        /// <summary>
        /// Get value from key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValueFromKey(string key, int companyId)
        {
            var value = "";
            var setting = GetByKey(key, companyId);
            if (setting != null)
            {
                value = Helpers.GetStringFromValueSetting(setting.Value);
            }
            return value;
        }

        /// <summary>
        /// Get current logo
        /// </summary>
        /// <returns></returns>
        public LogoModel GetCurrentLogo(int companyId)
        {
            if(companyId == 0)
            {
                var settings = _configuration.GetSection(Constants.Settings.FileSettings)?.Get<List<FileSetting>>();
                foreach (var unApplySetting in settings)
                {
                    if (unApplySetting.Key == Constants.Settings.Logo)
                    {
                        var logoUnknow = new LogoModel
                        {
                            Logo = unApplySetting.Values.FirstOrDefault()
                        };
                        return logoUnknow;
                    }
                }
            }
            var logo = _unitOfWork.SettingRepository.GetLogo(companyId);
            var logoModel = new LogoModel
            {
                Logo = Helpers.GetStringFromValueSetting(logo?.Value)
            };
            return logoModel;
        }
    }
}
