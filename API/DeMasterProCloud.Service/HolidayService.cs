﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.Repository;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DeMasterProCloud.Service.RabbitMq;
using DeMasterProCloud.Service.Protocol;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Holiday service interface
    /// </summary>
    public interface IHolidayService : IPaginationService<HolidayListModel>
    {
        void InitData(HolidayModel model);
        bool Add(HolidayModel model);
        bool Update(HolidayModel model, Holiday holiday);
        bool IsOverLapDurationTime(int holidayId, DateTime startDate, DateTime endDate);
        Holiday GetHolidayByIdAndCompany(int companyId, int holidayId);
        bool Delete(Holiday holiday);
        List<Holiday> GetByIds(List<int> ids);
        bool DeleteRange(List<Holiday> holidays);
        Holiday GetHolidayByNameAndCompany(int companyId, string name);
        List<Holiday> GetHolidayByCompany(int companyId);
        int GetHolidayCount(int companyId);
        bool IsExistedName(int holidayId, string name);
        bool IsOverLapDurationTime(int holidayId, string startDate, string endDate);
        //void SendHolidayToIcu(string deviceAddress, string groupMsgId = "", bool isPublish = true, bool isSave = true);
        void SendHolidayToIcu(string deviceAddress = null, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType="");
        HolidayProtocolData MakeUpdateHolidayProtocolData(List<Holiday> holidays, string protocolType);
    }

    public class HolidayService : IHolidayService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;

        public HolidayService(IUnitOfWork unitOfWork, IQueueService queueService,
            IHttpContextAccessor httpContextAccessor, ILogger<HolidayService> logger)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
        }

        /// <summary>
        /// Initial data
        /// </summary>
        /// <param name="model"></param>
        public void InitData(HolidayModel model)
        {
            model.HolidayTypeItems = EnumHelper.ToEnumList<HolidayType>();
            if (model.Type == 0)
            {
                model.Type = (int)HolidayType.HolidayType1;
            }
        }

        /// <summary>
        /// Add holiday
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Add(HolidayModel model)
        {
            var isSuccess = true;
            var companyId = _httpContext.User.GetCompanyId();

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var holiday = Mapper.Map<Holiday>(model);
                        holiday.CompanyId = companyId;
                        _unitOfWork.HolidayRepository.Add(holiday);
                        _unitOfWork.Save();

                        //Save system log
                        var content = $"{HolidayResource.lblAddNew}";
                        List<string> details = new List<string>()
                        {
                            $"{HolidayResource.lblHolidayName} : {holiday.Name}",
                            $"{HolidayResource.lblStartDate} : {holiday.StartDate}",
                            $"{HolidayResource.lblEndDate} : {holiday.EndDate}",
                            $"{HolidayResource.lblRecurring} : {(holiday.Recursive ? CommonResource.Use : CommonResource.NotUse)}",
                            $"{HolidayResource.lblHolidayType} : {holiday.Type}"
                        };
                        var contentsDetail = string.Join("\n", details);

                        _unitOfWork.SystemLogRepository.Add(holiday.Id, SystemLogType.Holiday, ActionLogType.Add,
                            content, contentsDetail, null, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                        isSuccess = false;
                    }
                }
            });
            //Send to icu
            var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(companyId);

            foreach (var device in devices)
            {
                SendHolidayToIcu(device.DeviceAddress);
            }

            return isSuccess;
        }

        /// <summary>
        /// Update holiday
        /// </summary>
        /// <param name="model"></param>
        /// <param name="holiday"></param>
        /// <returns></returns>
        public bool Update(HolidayModel model, Holiday holiday)
        {
            var isSuccess = true;
            var companyId = _httpContext.User.GetCompanyId();

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        List<string> changes = new List<string>();
                        var existingName = holiday.Name;
                        var isChanged = CheckChange(holiday, model, ref changes);
                        
                        //Update holiday
                        Mapper.Map(model, holiday);
                        _unitOfWork.HolidayRepository.Update(holiday);

                        if (isChanged)
                        {
                            //Save system log
                            var content = string.Format(HolidayResource.lblUpdateHoliday, existingName);
                            var contentsDetails = string.Join("\n", changes);
                            _unitOfWork.SystemLogRepository.Add(holiday.Id, SystemLogType.Holiday, ActionLogType.Update,
                                content, contentsDetails, null, companyId);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                        isSuccess = false;
                    }
                }
            });
            //Send to icu and save message log
            var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(companyId);

            foreach (var device in devices)
            {
                SendHolidayToIcu(device.DeviceAddress);
            }

            return isSuccess;
        }

        /// <summary>
        /// Delete holiday
        /// </summary>
        /// <param name="holiday"></param>
        /// <returns></returns>
        public bool Delete(Holiday holiday)
        {
            var isSuccess = true;
            var companyId = _httpContext.User.GetCompanyId();

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        holiday.IsDeleted = true;
                        _unitOfWork.HolidayRepository.Update(holiday);

                        //Save system log
                        var content =
                            $"{ActionLogTypeResource.Delete}: {holiday.Name} ({HolidayResource.lblHolidayName})";
                        _unitOfWork.SystemLogRepository.Add(holiday.Id, SystemLogType.Holiday, ActionLogType.Delete,
                            content, null, null, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                        isSuccess = false;
                    }
                }
            });

            //Send to icu and save message log
            var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(companyId);

            foreach (var device in devices)
            {
                SendHolidayToIcu(device.DeviceAddress);
            }

            return isSuccess;
        }

        /// <summary>
        /// Delete a list of holiday
        /// </summary>
        /// <param name="holidays"></param>
        /// <returns></returns>
        public bool DeleteRange(List<Holiday> holidays)
        {
            var isSuccess = true;
            var companyId = _httpContext.User.GetCompanyId();

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var holiday in holidays)
                        {
                            holiday.IsDeleted = true;
                            _unitOfWork.HolidayRepository.Update(holiday);
                        }

                        //Save system log
                        var holidayIds = holidays.Select(c => c.Id).ToList();
                        var holidayNames = holidays.Select(c => c.Name).ToList();

                        var content = string.Format(ActionLogTypeResource.DeleteMultipleType,
                            HolidayResource.lblHoliday);
                        var contentDetails = $"{HolidayResource.lblHolidayName}: {string.Join(", ", holidayNames)}";

                        _unitOfWork.SystemLogRepository.Add(holidayIds.First(), SystemLogType.Holiday, ActionLogType.DeleteMultiple, 
                            content, contentDetails, holidayIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                        isSuccess = false;
                    }
                }
            });
            //Send to icu and save message log
            var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(companyId);

            foreach (var device in devices)
            {
                SendHolidayToIcu(device.DeviceAddress);
            }

            return isSuccess;
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<HolidayListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.HolidayRepository.GetHolidayByCompany(companyId)
                .Select(Mapper.Map<HolidayListModel>).AsQueryable();

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Name.ToLower().Contains(filter.ToLower())
                                       || x.HolidayType.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Holiday.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Holiday[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        /// <summary>
        /// Check if there are some have changes
        /// </summary>
        /// <param name="holiday"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool HasChange(Holiday holiday, HolidayModel model)
        {
            return holiday.StartDate.Subtract(Convert.ToDateTime(model.StartDate)).Days != 0
                   || holiday.EndDate.Subtract(Convert.ToDateTime(model.EndDate)).Days != 0
                   || holiday.Type != model.Type || holiday.Recursive != model.Recursive;
        }

        /// <summary>
        /// Get list of holiday by ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<Holiday> GetByIds(List<int> ids)
        {
            return _unitOfWork.HolidayRepository.GetMany(m => ids.Contains(m.Id))
                .ToList();
        }

        /// <summary>
        /// Check two holiday is overlaped or not
        /// </summary>
        /// <param name="holidayId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public bool IsOverLapDurationTime(int holidayId, DateTime startDate, DateTime endDate)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var holidays = _unitOfWork.HolidayRepository.GetHolidayByCompany(companyId);
            foreach (var holiday in holidays)
            {
                if (holidayId != holiday.Id && holiday.StartDate.Subtract(endDate).Days <= 0
                                            && startDate.Subtract(holiday.EndDate).Days <= 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get holiday by id and company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="holidayId"></param>
        /// <returns></returns>
        public Holiday GetHolidayByIdAndCompany(int companyId, int holidayId)
        {
            var holiday = _unitOfWork.HolidayRepository.GetHolidayByIdAndCompany(companyId, holidayId);
            holiday.StartDate = Helpers.ConvertToUserTimeZoneReturnDate(Convert.ToDateTime(holiday.StartDate), _unitOfWork.AccountRepository.Get(m =>
                m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
            holiday.EndDate = Helpers.ConvertToUserTimeZoneReturnDate(Convert.ToDateTime(holiday.EndDate), _unitOfWork.AccountRepository.Get(m =>
                m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
            return holiday;
        }

        /// <summary>
        /// Get holiday by name and company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Holiday GetHolidayByNameAndCompany(int companyId, string name)
        {
            return _unitOfWork.HolidayRepository.GetHolidayByNameAndCompany(companyId, name);
        }

        /// <summary>
        /// Get holiday by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Holiday> GetHolidayByCompany(int companyId)
        {
            return _unitOfWork.HolidayRepository.GetHolidayByCompany(companyId);
        }

        /// <summary>
        /// Get holiday count
        /// </summary>
        /// <returns></returns>
        public int GetHolidayCount(int companyId)
        {
            var countHoliday = 0;
            var holidays = _unitOfWork.HolidayRepository.GetHolidayByCompany(companyId);
            if (holidays.Any())
            {
                countHoliday = GetAllCountHoliday(holidays);
            }
            return countHoliday;
        }

        /// <summary>
        /// Check if holiday name is exist
        /// </summary>
        /// <param name="holidayId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsExistedName(int holidayId, string name)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var holiday = GetHolidayByNameAndCompany(companyId, name);
            return holiday != null && holidayId != holiday.Id;
        }

        /// <summary>
        /// Check two holiday is overlaped or not
        /// </summary>
        /// <param name="holidayId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public bool IsOverLapDurationTime(int holidayId, string startDate, string endDate)
        {
            //            Need to validate datetime before doing the logic
            //Now i can input any value for the datetime and push to the server.
            //Incase datetime is invalid so it get 500 internal error.
            if (!Helpers.DateFormatCheck(startDate) || !Helpers.DateFormatCheck(endDate))
            {
                return false;
            }
            return IsOverLapDurationTime(holidayId, Convert.ToDateTime(startDate)
                , Convert.ToDateTime(endDate));
        }



        /// <summary>
        /// Send update holiday to rabbit mq server
        /// <param name="deviceAddress"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="isPublish"></param>
        /// <param name="isSave"></param>
        /// </summary>
        public void SendHolidayToIcu(string deviceAddress = null, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType="" )
        {
            string routingKey;
            var device = _unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

            if (!string.IsNullOrEmpty(deviceAddress))
            {
                routingKey = Constants.RabbitMq.HolidayTopic + "." + deviceAddress;
            }
            else
            {
                routingKey = Constants.RabbitMq.HolidayTopic;
            }

            var holidays = _unitOfWork.HolidayRepository.GetHolidayByCompany((int)device.CompanyId);
            var protocolData = MakeUpdateHolidayProtocolData(holidays, Constants.Protocol.UpdateHoliday);
            protocolData.Sender = _httpContext.User.GetUsername();
            var message = protocolData.ToString();
            if (groupLength == 1)
            {
                _queueService.Publish(routingKey, message);
            }
            else
            {
                _queueService.SendGroupMessage(deviceAddress, protocolData.MsgId, message, topic: routingKey, groupMsgId, groupIndex, groupLength, processId, actionType: actionType);
            }
        }

        /// <summary>
        /// Make holiday protocol data
        /// </summary>
        /// <param name="holidays"></param>
        /// <param name="protocolType"></param>
        /// <returns></returns>
        public HolidayProtocolData MakeUpdateHolidayProtocolData(List<Holiday> holidays, string protocolType)
        {
            var holidayProtocolData = new HolidayProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };

            var holidayProtocolHeaderData = new HolidayProtocolDataHeader
            {
                Total = GetAllCountHoliday(holidays),
                Holidays = Mapper.Map<List<HolidayProtocolDataDetail>>(holidays)
            };

            holidayProtocolData.Data = holidayProtocolHeaderData;
            return holidayProtocolData;
        }

        /// <summary>
        /// Get count holidays
        /// </summary>
        /// <param name="holidays"></param>
        /// <returns></returns>
        public int GetAllCountHoliday(List<Holiday> holidays)
        {
            var count = 0;
            foreach (var holiday in holidays)
            {
                var listDate = DateTimeHelper.GetListRangeDate(holiday.StartDate, holiday.EndDate);
                count += listDate.Count;
            }

            return count;
        }

        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="holiday">Holiday that contains existing information</param>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool CheckChange(Holiday holiday, HolidayModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                if (holiday.Name != model.Name)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, HolidayResource.lblHolidayName, holiday.Name, model.Name));
                }

                if (holiday.Type != model.Type)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, HolidayResource.lblHolidayType, holiday.Type, model.Type));
                }

                if (holiday.StartDate.ToSettingDateString() != model.StartDate)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, HolidayResource.lblStartDate,
                        holiday.StartDate.ToSettingDateString(), model.StartDate));
                }

                if (holiday.EndDate.ToSettingDateString() != model.EndDate)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, HolidayResource.lblEndDate,
                        holiday.EndDate.ToSettingDateString(), model.EndDate));
                }

                if (holiday.Recursive != model.Recursive)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, HolidayResource.lblRecurring,
                        holiday.Recursive ? CommonResource.Use : CommonResource.NotUse,
                        model.Recursive ? CommonResource.Use : CommonResource.NotUse));
                }

            }

            return changes.Count() > 0;
        }

    }
}