﻿using System;
using System.Collections.Generic;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using FluentScheduler;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Repository;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.DataModel.WorkingModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DeMasterProCloud.Service.Infrastructure
{
    public class SetTimeCronJob : IJob
    {
        private readonly IQueueService _queueService;
        private List<IcuDevice> _devices;
        private readonly IConfiguration _configuration;

        public SetTimeCronJob(IConfiguration configuration, IQueueService queueService)
        {
            _queueService = queueService;
            _configuration = configuration;
        }

        public void Execute()
        {
            var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
            //_devices = _unitOfWork.IcuDeviceRepository.GetOnlineDevices();
            _devices = unitOfWork.IcuDeviceRepository.GetNotDeletedDevices();
            var deviceService = new DeviceService(unitOfWork);
            foreach (var device in _devices)
            {
                // We only send schedule message if there is no active groups message
                if (ApplicationVariables.PendingGroupMessages.ContainsKey(device.DeviceAddress))
                {
                    if (ApplicationVariables.PendingGroupMessages[device.DeviceAddress].Count == 0)
                    {
                        var groupMsgId = Guid.NewGuid().ToString();
                        SendSetTime(device, groupMsgId, unitOfWork);
                        SendRequestDeviceInfo(device, groupMsgId);
                    }
                }
                else
                {
                    var groupMsgId = Guid.NewGuid().ToString();
                    SendSetTime(device, groupMsgId, unitOfWork);
                    SendRequestDeviceInfo(device, groupMsgId);
                }
                deviceService.UpdateUpTimeToDevice(device.Id);
            }
            unitOfWork.Dispose();
        }

        private void SendSetTime(IcuDevice device, string groupMsgId, IUnitOfWork unitOfWork)
        {
            if (device.BuildingId != null)
            {
                var building = unitOfWork.BuildingRepository.GetByIdAndCompanyId(device.CompanyId.Value, device.BuildingId.Value);

                if(building != null)
                {
                    var timezone = building.TimeZone;

                    var utcHour = Helpers.GetUtcOffSetHour(timezone);
                    var utcMinute = Helpers.GetUtcOffSetMinute(timezone);
                    var dataDetailSetTime = new DeviceInstructionSetTimeDetail
                    {
                        Command = Constants.CommandType.SetTime,
                        utcHour = utcHour,
                        utcMinute = utcMinute,
                        Options =
                    {
                        Time = Helpers.ConvertDateTimeByTimeZone(timezone),
                        IsSchedule = true
                    }
                    };

                    var msgId = Guid.NewGuid().ToString();
                    string type = Constants.Protocol.DeviceInstruction;

                    var deviceSetTimeInstruction = new DeviceInstructionSetTimeProtocolData
                    {
                        MsgId = msgId,
                        Type = type,
                        Data = dataDetailSetTime
                    };
                    var message = deviceSetTimeInstruction.ToString();

                    var topic = $"{Constants.RabbitMq.DeviceInstructionTopic}.{device.DeviceAddress}";

                    _queueService.SendGroupMessage(device.DeviceAddress, msgId: msgId, message: message, topic: topic,
                        groupMsgId: groupMsgId, groupIndex: 0, groupLength: 2, actionType: Constants.ActionType.SetTimeAndGetInfo);
                }
                
            }

        }

        private void SendRequestDeviceInfo(IcuDevice device, string groupMsgId)
        {
            var msgId = Guid.NewGuid().ToString();
            string type = Constants.Protocol.LoadDeviceInfo;
            var deviceInfo = new DeviceInfoProtocolData
            {
                MsgId = msgId,
                Type = type,
                Data = new DeviceInfoProtocolDataHeader()
            };
            var message = deviceInfo.ToString();

            var topic = $"{Constants.RabbitMq.DeviceInfoTopic}.{device.DeviceAddress}";
            _queueService.SendGroupMessage(device.DeviceAddress, msgId: msgId, message: message, topic: topic,
                groupMsgId: groupMsgId, groupIndex: 1, groupLength: 2, actionType: Constants.ActionType.SetTimeAndGetInfo);

        }
    }



    public class MyRegistry : Registry
    {

        public MyRegistry(IConfiguration configuration, IQueueService queueService, IUnitOfWork unitOfWork)
        {
            Schedule(() => new SetTimeCronJob(configuration, queueService)).ToRunNow()
                .AndEvery(Constants.RabbitMq.Scheduler).Seconds();
            Schedule(() => new RecheckAttendance(unitOfWork, configuration, queueService))
                .ToRunEvery(Constants.Attendance.SchedulerDay)
                .Days().At(Constants.Attendance.SchedulerHour, Constants.Attendance.SchedulerMinute);
            Schedule(() => new CreateAttendanceNewDay(unitOfWork, configuration, queueService)).ToRunNow()
                .AndEvery(Constants.Attendance.SchedulerAttendance).Hours();
            Schedule(() => new CheckVisitorExpired(unitOfWork, configuration, queueService)).ToRunNow()
                .AndEvery(Constants.Settings.TimeCheckVisitExpired).Hours();
        }
        
        public class RecheckAttendance : IJob
        {
            private readonly IQueueService _queueService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IConfiguration _configuration;
            
            public RecheckAttendance(IUnitOfWork unitOfWork, IConfiguration configuration,
                IQueueService queueService)
            {
                _unitOfWork = unitOfWork;
                _configuration = configuration;
                _queueService = queueService;
            }

            public void Execute()
            {
                new AttendanceService(new HttpContextAccessor() , _configuration).Recheck(Constants.Attendance.RangTimeToDay,0,null,null);
            }
        }
        
        public class CheckVisitorExpired : IJob
        {
            private readonly IQueueService _queueService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IConfiguration _configuration;
            
            public CheckVisitorExpired(IUnitOfWork unitOfWork, IConfiguration configuration,
                IQueueService queueService)
            {
                _unitOfWork = unitOfWork;
                _configuration = configuration;
                _queueService = queueService;
            }

            public void Execute()
            {
                var visitors = _unitOfWork.AppDbContext.Visit.Where(v => 
                    v.Status ==(short) VisitChangeStatusType.Waiting ||
                    v.Status ==(short) VisitChangeStatusType.Approved1 ||
                    v.Status ==(short) VisitChangeStatusType.Approved ||
                    v.Status ==(short) VisitChangeStatusType.CardIssued && !v.IsDeleted);
                if (visitors.Any())
                {
                    foreach (var visitor in visitors)
                    {
                        if (visitor.EndDate < DateTime.Now)
                        {
                            if (visitor.Status == (short) VisitChangeStatusType.CardIssued)
                            {
                                visitor.Status = (short) VisitChangeStatusType.FinishedWithoutReturnCard;
                                var card = _unitOfWork.AppDbContext.Card.FirstOrDefault(c => c.VisitId == visitor.Id);
                                _unitOfWork.CardRepository.Delete(card);
                                _unitOfWork.VisitRepository.Update(visitor);
                                _unitOfWork.Save();
                                SendIdentificationToDevice(visitor, card, false);
                            }
                            else
                            {
                                visitor.Status = (short) VisitChangeStatusType.FinishedWithoutReturnCard;
                                _unitOfWork.VisitRepository.Update(visitor);
                                _unitOfWork.Save();
                            }
                        
                        }
                    }
                }
                
            }
            
            private void SendIdentificationToDevice(Visit visitor, Card newCard, bool isAdd)
            {
                if (visitor == null)
                {
                    return;
                }

                var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(visitor.CompanyId, visitor.AccessGroupId);

                if (agDevices != null && agDevices.Any())
                {
                    foreach (var agDevice in agDevices)
                    {
                        SendIdentificationToDeviceVisitor(agDevice, visitor, newCard, isAdd);
                    }
                }
            }
            
            private void SendIdentificationToDeviceVisitor(AccessGroupDevice agDevice, Visit visitor, Card card, bool isAdd)
            {
                var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{agDevice.Icu.DeviceAddress}";

                var protocolData = new UserProtocolData()
                {
                    MsgId = Guid.NewGuid().ToString(),
                    Type = isAdd ? Constants.Protocol.AddUser : Constants.Protocol.DeleteUser,
                    Sender = ""
                };
                var visitProtocolHeaderData = new UserProtocolHeaderData
                {
                    Total = 1,
                    UpdateFlag = 1,
                    Users = new List<UserProtocolDetailData>
                    {
                        new UserProtocolDetailData()
                        {
                            EmployeeNumber = visitor.VisitorName,
                            UserName = visitor.VisitorName,
                            DepartmentName = visitor.VisitorDepartment,
                            CardId = card.CardId,
                            IssueCount = 0,
                            AdminFlag = (short) (card.IsMasterCard ? 1 : 0),
                            EffectiveDate = visitor.StartDate.ToString(Constants.DateTimeFormat.DdMMyyyy),
                            ExpireDate = visitor.EndDate.ToString(Constants.DateTimeFormat.DdMMyyyy),
                            AntiPassBack = card.CardStatus,
                            Timezone =  agDevice.Tz?.Position ?? 1,
                            Password = ""
                        }
                    }
                };
                protocolData.Data = visitProtocolHeaderData;

                _queueService.Publish(routingKey, protocolData.ToString());
            }
        }
        

        public class CreateAttendanceNewDay : IJob
        {
            private readonly IQueueService _queueService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IConfiguration _configuration;

            
            public CreateAttendanceNewDay(IUnitOfWork unitOfWork, IConfiguration configuration,
                IQueueService queueService)
            {
                _unitOfWork = DbHelper.CreateUnitOfWork(configuration);
                _configuration = configuration;
                _queueService = queueService;
            }

            public void Execute()
            {
                DateTime now = DateTime.Now;
                var attenType = (short) AttendanceType.AbsentNoReason;
                if (now.DayOfWeek == DayOfWeek.Saturday || now.DayOfWeek == DayOfWeek.Sunday)
                {
                    attenType = (short) AttendanceType.Holiday;
                }
                
                var users = _unitOfWork.UserRepository.GetAllUserInSystem();
                Console.WriteLine(users.Count);
                foreach (var user in users)
                {
                    var workingTime = _unitOfWork.WorkingRepository.GetById(user.WorkingTypeId.Value);
                    if (workingTime != null)
                    {
                        var listWorking = JsonConvert.DeserializeObject<List<WorkingTime>>(workingTime.WorkingDay);
                        foreach (var timeWork in listWorking)
                        {
                            if (timeWork.Name == Helpers.GetNextWorkingDay(now.DayOfWeek.ToString()))
                            {
                                var attendanceExisted =
                                    _unitOfWork.AttendanceRepository.CheckAttendanceAlreadyCreated(user.Id, user.CompanyId,
                                        now.Date.AddDays(1));
                                if (!attendanceExisted)
                                {
                                    // Parse Time to epoch
                                    String[] strStart = timeWork.Start.Split(':', ' ');
                                    String[] strEnd = timeWork.End.Split(':', ' ');
                                    var hourStart = Convert.ToInt32(strStart.FirstOrDefault());
                                    var hourEnd = Convert.ToInt32(strEnd.FirstOrDefault());
                                    var minutesStart = Convert.ToInt32(strStart[strStart.Length - 1]);
                                    var minutesEnd = Convert.ToInt32(strEnd[strEnd.Length - 1]);

                                    var start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                    var end = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(timeWork);
                                    var attendance = new Attendance
                                    {
                                        UserId = user.Id,
                                        Date = now.Date.AddDays(1),
                                        CompanyId = user.CompanyId,
                                        StartD = start,
                                        EndD = end,
                                        Type = attenType,
                                        WorkingTime = jsonString,
                                        ClockInD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                                        ClockOutD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                                    };
                                    _unitOfWork.AttendanceRepository.Add(attendance);
                                    _unitOfWork.Save();
                                }
                            }
                        }
                    }
                }
            }
        }
        
        public class CheckAutoRenewQrCode : IJob
        {
            private readonly IQueueService _queueService;
            private readonly IUnitOfWork _unitOfWork;
            private readonly IConfiguration _configuration;

            public CheckAutoRenewQrCode(IUnitOfWork unitOfWork, IConfiguration configuration,
                IQueueService queueService)
            {
                _unitOfWork = unitOfWork;
                _configuration = configuration;
                _queueService = queueService;
            }

            public void Execute()
            {
                var listSetting = _unitOfWork.SettingRepository.GetCompaniesWithAutoRenew();
                foreach (var setting in listSetting)
                {
                    if (Helpers.GetStringFromValueSetting(setting.Value) == "true")
                    {
                        var cards = _unitOfWork.CardRepository.GetCardQrByCompanyId(setting.CompanyId);

                        var company =
                            _unitOfWork.CompanyRepository.GetById(setting.CompanyId);

                        var timePeriodString = Helpers.GetStringFromValueSetting(
                            _unitOfWork.SettingRepository.GetCompaniesPeriodAutoRenew(setting.CompanyId).Value);

                        int timePeriodInt = Int32.Parse(timePeriodString);

                        int compare = DateTime.Compare(company.CreatedOn.AddDays(timePeriodInt), DateTime.Now);

                        if (compare < 0)
                        {
                            company.SecretCode = GenAesKey();
                            _unitOfWork.CompanyRepository.Update(company);

                            var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(company.Id);
                            if (devices.Count != 0)
                            {
                                foreach (var device in devices)
                                {
                                    PublishMessageToDevice(device);
                                }

                            }
                            
                        }
                    }
                }
            }

            public IcuDeviceProtocolData MakeDeviceProtocolData(IcuDevice icuDevice, string protocolType)
            {
                var data = Mapper.Map<IcuDeviceProtocolDetailData>(icuDevice);
                if (icuDevice.PassageTzId != null)
                {
                    var passageTz =
                        _unitOfWork.TimezoneRepository.GetByIdAndCompany(icuDevice.PassageTzId.Value,
                            icuDevice.CompanyId ?? 0);
                    data.PassageTimezone = passageTz.Position;
                }

                var activeTz =
                    _unitOfWork.TimezoneRepository.GetByIdAndCompany(icuDevice.ActiveTzId, icuDevice.CompanyId ?? 0);
                if (activeTz != null)
                {
                    data.ActiveTimezone = activeTz.Position;
                }

                if (icuDevice.DeviceType == (short) DeviceType.Icu300N)
                {
                    data.ReaderCount = 2;
                    data.ReaderConfig = new List<int>();

                    var configReader0 = icuDevice.RoleReader0 << 0;
                    configReader0 += icuDevice.LedReader0 << 1;
                    configReader0 += icuDevice.BuzzerReader0 << 2;

                    data.ReaderConfig.Add(configReader0 ?? 0);

                    var configReader1 = icuDevice.RoleReader1 << 0;
                    configReader1 += icuDevice.LedReader1 << 1;
                    configReader1 += icuDevice.BuzzerReader1 << 2;

                    data.ReaderConfig.Add(configReader1 ?? 0);

                }
                else if (icuDevice.DeviceType == (short) DeviceType.ItouchPop)
                {
                    data.ReaderCount = icuDevice.UseCardReader == 0 ? 2 : 1;
                    data.ReaderConfig = new List<int>();

                    var configReader0 = icuDevice.RoleReader0 << 0;
                    configReader0 += icuDevice.UseCardReader << 1;
                    configReader0 += icuDevice.BuzzerReader0 << 2;

                    data.ReaderConfig.Add(configReader0 ?? 0);

                    if (data.ReaderCount == 2)
                    {
                        var configReader1 = icuDevice.RoleReader1 << 0;
                        configReader1 += icuDevice.LedReader1 << 1;
                        configReader1 += icuDevice.BuzzerReader1 << 2;

                        data.ReaderConfig.Add(configReader1 ?? 0);
                    }
                }

                var deviceProtocolData = new IcuDeviceProtocolData
                {
                    MsgId = Guid.NewGuid().ToString(),
                    Type = protocolType,
                    Data = data
                };
                return deviceProtocolData;
            }

            public void PublishMessageToDevice(IcuDevice device)
            {
                var protocolData = MakeDeviceProtocolData(device, Constants.Protocol.UpdateDeviceConfig);
                var message = protocolData.ToString();
                var topic = $"{Constants.RabbitMq.ConfigurationTopic}.{device.DeviceAddress}";
                _queueService.Publish(topic, message);
            }

            public string GenAesKey()
            {
                const string chars = Constants.DynamicQr.AllowChars;

                var secretCode = new string(Enumerable.Repeat(chars, Constants.DynamicQr.LenghtOfSecretCode)
                    .Select(s => s[random.Next(s.Length)]).ToArray());
                return secretCode;
            }
            
            private static Random random = new Random();
        }
    }
}
