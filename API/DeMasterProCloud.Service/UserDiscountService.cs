﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.UserDiscount;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service
{
    public interface IUserDiscountService
    {
        ReponseStatus AddUserDiscount(AddUserDiscountModel model);
        ReponseStatus UpdateUserDiscount(int id, AddUserDiscountModel model);
        ReponseStatus DeleteUserDiscountg(int id);
        List<UserDiscountModel> GetListUserDiscount(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        UserDiscountModel GetUserDiscount(int id);
    }
    public class UserDiscountService : IUserDiscountService
    {
        private readonly IUnitOfWork _unitOfWork;
        DeMasterProCloud.DataAccess.Models.UserDiscount obj = new DataAccess.Models.UserDiscount();
        ReponseStatus res = new ReponseStatus();
        public UserDiscountService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ReponseStatus AddUserDiscount(AddUserDiscountModel model)
        {

            try
            {
                obj.UserId = model.userId;
                obj.Amount = model.amount;
                var add = _unitOfWork.UserDiscountRepository.AddUserDiscount(obj);
                if (add.statusCode == false)
                {
                    res.message = Constants.UserDiscount.AddFailed;
                    res.statusCode = false;
                    return res;
                }
                if (add.statusCode == true && add.message == null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.UserDiscount.AddSuccess;
                res.statusCode = true;
                res.data = add.data;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.UserDiscount.AddFailed;
                res.statusCode = false;
            }
            return res;

        }

        public ReponseStatus UpdateUserDiscount(int id, AddUserDiscountModel model)
        {

            try
            {
                obj.Amount = model.amount;
                obj.Id = id;
                var update = _unitOfWork.UserDiscountRepository.UpdateUserDiscount(obj);
                if (update.statusCode == false)
                {
                    res.message = Constants.UserDiscount.UpdateFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.UserDiscount.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.UserDiscount.UpdateFailed;
                res.statusCode = false;
            }
            return res;

        }
        public ReponseStatus DeleteUserDiscountg(int id)
        {

            try
            {
                var delete = _unitOfWork.UserDiscountRepository.DeleteUserDiscount(id);
                if (delete.statusCode == false)
                {
                    res.message = Constants.UserDiscount.DeleteFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.UserDiscount.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.UserDiscount.DeleteFailed;
                res.statusCode = false;
            }
            return res;

        }

        public List<UserDiscountModel> GetListUserDiscount(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var listUserDiscount = _unitOfWork.UserDiscountRepository.GetListUserDiscount(filter, pageNumber, pageSize, sortColumn,
             sortDirection, out totalRecords, out recordsFiltered).ToList();

            return listUserDiscount;
        }

        public UserDiscountModel GetUserDiscount(int id)
        {
            var userDiscount = _unitOfWork.UserDiscountRepository.GetUserDiscount(id);
            if(userDiscount == null)
            {
                return null;
            }    
            return userDiscount;
        }

    }
}
