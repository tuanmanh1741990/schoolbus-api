﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealEventLog;
using DeMasterProCloud.DataModel.MealSetting;
using DeMasterProCloud.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DeMasterProCloud.Service
{
    public interface IMealEventLogService
    {
        ReponseStatus AddMealEventLog(int mealTypeId, decimal price, string cardId, int cornerId);
        ReponseStatus AddMealEventLogManual(MealEventLogAddModel model);
        ReponseStatus UpdateMealEventLogManual(MealEventLogUpdateModel model);
        List<MealEventLogModel> GetListMealEventLogManual(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, List<int> icuId, List<int> buildingId, List<int> cornerId, int isManual);
        MealEventLogModel GetMealEventLogManual(int id);

        ReponseStatus DeleteMeaEventLog(int id);
        List<MealEventLogReportModel> Report(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, string doorName, List<int> buildingId, int isReport);
        List<ReportCanteenManagementByBuildingModel> ListReportByBuildingModel(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> buildingId);
        List<ReportCanteenManagementByCornerModel> ListReportByCorner(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> cornerId);
    }
    public class MealEventLogService : IMealEventLogService
    {
        private readonly IUnitOfWork _unitOfWork;
        DeMasterProCloud.DataAccess.Models.MealEventLog obj = new DataAccess.Models.MealEventLog();
        ReponseStatus res = new ReponseStatus();
        public MealEventLogService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ReponseStatus AddMealEventLog(int mealTypeId, decimal price, string cardId, int cornerId)
        {
            try
            {
                var add = _unitOfWork.MealEventLogRepository.AddEventLog(mealTypeId, price, cardId, cornerId);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;

        }

        public ReponseStatus AddMealEventLogManual(MealEventLogAddModel model)
        {
            try
            {
                var add = _unitOfWork.MealEventLogRepository.AddMealEventLogManual(model);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = add.data;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;
        }


        public ReponseStatus UpdateMealEventLogManual(MealEventLogUpdateModel model)
        {
            try
            {
                var add = _unitOfWork.MealEventLogRepository.UpdateMealEventLogManual(model);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;


        }


        public List<MealEventLogModel> GetListMealEventLogManual(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, List<int> icuId, List<int> buildingId, List<int> cornerId, int isManual)
        {
            var lstmealEventLogModel = _unitOfWork.MealEventLogRepository.GetListMealEventLogManual(filter, pageNumber, pageSize, sortColumn,
              sortDirection, out totalRecords, out recordsFiltered, firstAccessTime, lastAccessTime, cardId, departmentId, mealCode, userId, icuId, buildingId, cornerId, isManual);
            return lstmealEventLogModel;
        }

        public MealEventLogModel GetMealEventLogManual(int id)
        {
            var mealEventLogModel = _unitOfWork.MealEventLogRepository.GetMealEventLogManual(id);
            return mealEventLogModel;
        }

        public ReponseStatus DeleteMeaEventLog(int id)
        {
            try
            {
                var add = _unitOfWork.MealEventLogRepository.DeleteMeaEventLog(id);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }
                if (add.statusCode == false && add.message == "")
                {
                    res.message = "";
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;

        }

        public List<MealEventLogReportModel> Report(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, string doorName, List<int> buildingId, int isReport)
        {
            var lst = _unitOfWork.MealEventLogRepository.Report(filter, pageNumber, pageSize, sortColumn,
               sortDirection, out totalRecords, out recordsFiltered, firstAccessTime, lastAccessTime, cardId, departmentId, mealCode, userId, doorName, buildingId, isReport);
            return lst;
        }

        public List<ReportCanteenManagementByBuildingModel> ListReportByBuildingModel(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> buildingId)
        {
            var lst = _unitOfWork.MealEventLogRepository.ListReportByBuilding(filter, pageNumber, pageSize, sortColumn,
               sortDirection, out totalRecords, out recordsFiltered, firstAccessTime, lastAccessTime, buildingId);
            return lst;
        }
        public List<ReportCanteenManagementByCornerModel> ListReportByCorner(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> cornerId)
        {
            var lst = _unitOfWork.MealEventLogRepository.ListReportByCorner(filter, pageNumber, pageSize, sortColumn,
               sortDirection, out totalRecords, out recordsFiltered, firstAccessTime, lastAccessTime, cornerId);
            return lst;
        }
    }
}
