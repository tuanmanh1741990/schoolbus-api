﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.DeviceMessage;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Linq.Dynamic.Core;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service.Protocol;
using Microsoft.EntityFrameworkCore;

namespace DeMasterProCloud.Service
{
    public interface IDeviceMessageService
    {
        IQueryable<DeviceMessageListModel> GetPaginated(int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        void Update(DeviceMessageModel model);
        DeviceMessage GetByIdAndCompany(int id, int companyId);

        void SendDeviceMessage(List<IcuDevice> iTouchPopDevices, List<DeviceMessage> messages = null,
            string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType = "");
    }
    public class DeviceMessageService : IDeviceMessageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;

        public DeviceMessageService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor, IConfiguration configuration,
            ILogger<DeviceMessageService> logger, IQueueService queueService)
        {
            _unitOfWork = unitOfWork;
            _httpContext = contextAccessor.HttpContext;
            _configuration = configuration;
            _logger = logger;
            _queueService = queueService;
            //SendDeviceMessage();
        }

        /// <summary>
        /// Get paginted
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<DeviceMessageListModel> GetPaginated(int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.DeviceMessageRepository.GetByCompanyId(companyId);

            totalRecords = data.Count();
            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.DeviceMessageList.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.DeviceMessageList[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.AsEnumerable<DeviceMessage>().Select(Mapper.Map<DeviceMessageListModel>).AsQueryable();
        }

        /// <summary>
        /// Update device message
        /// </summary>
        /// <param name="model"></param>
        public void Update(DeviceMessageModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var messageId = model.MessageId;
                        var deviceMessage = _unitOfWork.DeviceMessageRepository.GetById(model.Id);

                        Mapper.Map(model, deviceMessage);
                        _unitOfWork.DeviceMessageRepository.Update(deviceMessage);
                        _unitOfWork.Save();
                        //Save system log
                        var content = string.Format(DeviceMessageResource.msgDeviceMessageChange, string.Format("{0:0000}", messageId), model.Content);
                        _unitOfWork.SystemLogRepository.Add(model.Id, SystemLogType.MessageSetting, ActionLogType.Update,
                            content, null, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                        //var messages = new List<DeviceMessage> { deviceMessage };
                        //SendDeviceMessage(null, messages);
                        var iTouchPopDevices = _unitOfWork.IcuDeviceRepository.GetDevicesByCompany(_httpContext.User.GetCompanyId())
                            .Where(d => d.DeviceType == (short)DeviceType.ItouchPop).ToList();

                        SendDeviceMessage(iTouchPopDevices);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
        }

        /// <summary>
        /// Make Device Message protocol data
        /// </summary>
        /// <param name="messages"></param>
        /// <param name="protocolType"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        public DeviceMessageProtocolData MakeUpdateDeviceMessageProtocolData(IcuDevice device, List<DeviceMessage> messages, string protocolType)
        {
            //var messageList = new List<DeviceMessage>(messages.ToArray());
            var messageList = new List<DeviceMessage>();

            foreach (var message in messages)
            {
                messageList.Add(new DeviceMessage()
                {
                    Company = message.Company,
                    CompanyId = message.CompanyId,
                    Content = message.Content,
                    CreatedOn = message.CreatedOn,
                    CreatedBy = message.CreatedBy,
                    Id = message.Id,
                    MessageId = message.MessageId,
                    Remark = message.Remark,
                    UpdatedBy = message.UpdatedBy,
                    UpdatedOn = message.UpdatedOn
                });
            }

            var buildingName = _unitOfWork.BuildingRepository.GetById(device.BuildingId ?? Constants.DefaultBuildingId).Name;
            foreach (var message in messageList)
            {
                if (message.Content.Contains(Constants.FormatBuilding))
                {
                    message.Content = message.Content.Replace(Constants.FormatBuilding, buildingName);
                }

                if (message.Content.Contains(Constants.FormatDoor))
                {
                    message.Content = message.Content.Replace(Constants.FormatDoor, device.Name);
                }
            }


            var deviceMessageProtocolData = new DeviceMessageProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };

            var deviceMessageProtocolHeaderData = new DeviceMessageProtocolDataHeader()
            {
                Messages = Mapper.Map<List<DeviceMessageProtocolDataDetail>>(messageList)
            };

            deviceMessageProtocolData.Data = deviceMessageProtocolHeaderData;
            return deviceMessageProtocolData;
        }

        /// <summary>
        /// Send update device message to rabbit mq server
        /// <param name="messages"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="isPublish"></param>
        /// <param name="isSave"></param>
        /// </summary>
        public void SendDeviceMessage(List<IcuDevice> iTouchPopDevices, List<DeviceMessage> messages = null,
            string groupMsgId = "", int groupIndex=0, int groupLength=1, string processId="", string actionType="")
        {
            
            foreach (var iTouchPopDevice in iTouchPopDevices)
            {
                if (messages == null)
                {
                    messages = _unitOfWork.DeviceMessageRepository.GetByCompanyId(iTouchPopDevice.CompanyId).ToList();
                }

                string routingKey = Constants.RabbitMq.DeviceMessageTopic + "." + iTouchPopDevice.DeviceAddress;

                var protocolData = MakeUpdateDeviceMessageProtocolData(iTouchPopDevice, messages,
                    Constants.Protocol.UpdateDeviceMessage);
                var message = protocolData.ToString();

                // We public message directly to the topic if it is single message
                // Else we publish message to multiple messages queue
                if (groupLength == 1)
                {
                    _queueService.Publish(routingKey, message);
                }
                else
                {
                    _queueService.SendGroupMessage(iTouchPopDevice.DeviceAddress, protocolData.MsgId,
                        message, topic: routingKey, groupMsgId, groupIndex+iTouchPopDevices.IndexOf(iTouchPopDevice), groupLength, processId, actionType: actionType);

                }
            }
        }


        public DeviceMessage GetByIdAndCompany(int id, int companyId)
        {
            return _unitOfWork.AppDbContext.DeviceMessage.Include(m => m.Company)
                .FirstOrDefault(m =>
                    m.Id == id && m.CompanyId == companyId && !m.Company.IsDeleted);
        }

    }
}