﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DeviceInstructionConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        private ISendMessageService _sendMessageService;
        private INotificationService _notificationService;
        public DeviceInstructionConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceInstructionConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _sendMessageService = new SendMessageService(_queueService);
                _notificationService = new NotificationService(_queueService);

                var eventLogs = new List<EventLog>();
                var response = ParseData(data);
                var deviceAddress = response.Data.DeviceAddress;
                var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

                // check that whether msgId is null
                if (response.MsgId == null)
                {
                    _logger.LogWarning($"MsgId of the message {response.ToString()} is null! We ignore the message!");
                    return;
                }

                if (!ApplicationVariables.SendingMessages.ContainsKey(response.MsgId))
                {
                    _logger.LogWarning($"Can not find original message of message {response.ToString()}! We ignore the message!");
                }
                else
                {
                    var originalMessagePayload = ApplicationVariables.SendingMessages.GetValueOrDefault(response.MsgId);

                    var originalMessage = JsonConvert.DeserializeObject<DeviceInstructionProtocolData>(originalMessagePayload);
                    
                    string userName = originalMessage.Sender;

                    if (response.Data.Status.ToLower().Equals(Constants.MessageType.Success) 
                        && (originalMessage.Data.Command == Constants.CommandType.ForceOpen
                      || originalMessage.Data.Command == Constants.CommandType.ForceClose 
                      || originalMessage.Data.Command == Constants.CommandType.Release))
                    {
                        new MailService(_configuration).SendEmailForceOpen(response.Data.DeviceAddress, originalMessage.Data.Command, userName);
                    }

                    if (icuDevice == null)
                    {
                        _logger.LogWarning($"Can not find the icu address {deviceAddress}!");
                        return;
                    }

                    // Receiving a response is that the device is connected to the server.
                    icuDevice.ConnectionStatus = (int)IcuStatus.Connected;

                    var companyCode = icuDevice.CompanyId != null ? icuDevice.Company.Code : "";
                    // If sender is Admin user we send to admin notification topic
                    var account = unitOfWork.AppDbContext.Account.FirstOrDefault(m =>
                        m.Username == userName &&
                        !m.IsDeleted);
                    if (account != null && account.Type == 0)
                    {
                        companyCode = "";
                    }

                    // if message return error then push notification
                    if (response.Data.Status.Equals(Constants.MessageType.Failure,
                        StringComparison.OrdinalIgnoreCase))
                    {
                        _notificationService.SendMessage(Constants.MessageType.Error,
                            Constants.NotificationType.SendDeviceInstructionError, userName,
                            MessageResource.msgSendInstructionFailure, companyCode);
                        //return;
                    }

                    //update device
                    //icuDevice.ConnectionStatus = (short)IcuStatus.Connected;
                    //icuDevice.LastCommunicationTime = DateTime.Now;
                    if (!string.IsNullOrEmpty(response.Data.DoorStatus))
                    {
                        icuDevice.DoorStatus = response.Data.DoorStatus;
                    }
                    bool isNotify = true;

                    unitOfWork.IcuDeviceRepository.Update(icuDevice);

                    if (originalMessage.Data.Command == Constants.CommandType.SetTime)
                    {
                        var setTimeMessage = JsonConvert.DeserializeObject<DeviceInstructionSetTimeProtocolData>(originalMessagePayload);
                        if (setTimeMessage.Data.Options.IsSchedule) isNotify = false;
                    }
                    if (isNotify)
                    {
                        _notificationService.SendMessage(Constants.MessageType.Success,
                            Constants.NotificationType.SendDeviceInstructionSuccess, userName,
                            string.Format(MessageResource.DeviceInstructionSuccess, icuDevice.DeviceAddress), companyCode);
                    }

                    //Add event log
                    AddSingleEventLog(unitOfWork, eventLogs, icuDevice, response);

                    unitOfWork.Save();
                    //Send event log
                    if (eventLogs.Any())
                    {
                        try
                        {
                            SendDataToFe(unitOfWork, icuDevice, eventLogs);
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                        }
                    }

                    _sendMessageService.SendConnectionStatus(icuDevice, response.Data.DoorStatus);
                    unitOfWork.Dispose();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                _logger.LogWarning(ParseData(data).ToString());
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private DeviceInstructionResponse ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var result = JsonConvert.DeserializeObject<DeviceInstructionResponse>(jsonString);
            return result;
        }

        /// <summary>
        /// Get command from payload
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        private string GetCommand(string payload)
        {
            var result = JsonConvert.DeserializeObject<DeviceInstructionProtocolData>(payload);
            return result.Data.Command;
        }

        /// <summary>
        /// Get state from payload
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        private int GetState(string payload)
        {
            var result = JsonConvert.DeserializeObject<DeviceInstructionUpdateDeviceStateProtocolData>(payload);
            return result.Data.Options.State;
        }

        /// <summary>
        /// Get username from payload
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        private string GetUsername(string payload)
        {
            var result = JsonConvert.DeserializeObject<DeviceInstructionProtocolData>(payload);
            return result.Data.UserName ?? "";
        }

        /// <summary>
        /// Get IsSchedule option from payload
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        private bool GetIsScheduleOption(string payload)
        {
            var result = JsonConvert.DeserializeObject<DeviceInstructionSetTimeProtocolData>(payload);
            return result.Data.Options.IsSchedule;
        }

        /// <summary>
        /// Send user log data to front end
        /// </summary>
        /// <param name="icuDevice"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="eventLogs"></param>
        internal void SendDataToFe(IUnitOfWork unitOfWork, IcuDevice icuDevice, List<EventLog> eventLogs)
        {
            var lastEventLog = eventLogs.Last();
            var eventLogDetail = new SendEventLogDetailData
            {
                Id = Guid.NewGuid(),
                IcuId = icuDevice.Id,
                AccessTime = lastEventLog.EventTime.ToSettingDateTimeString(),
                UnixTime = (lastEventLog.EventTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                Device = icuDevice.DeviceAddress,
                DoorName = icuDevice.Name,
                EventDetail = ((EventType)lastEventLog.EventType).GetDescription(),
                EventDetailCode = lastEventLog.EventType,
                CardStatus = null,
                UserName = lastEventLog.UserName,
                BuildingId = icuDevice.BuildingId.Value
            };

            var events = new List<SendEventLogDetailData> { eventLogDetail };

            var result = new SendEventLogListModelData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.EventLogWebApp,
                Data = new SendEventLogHeaderData
                {
                    Total = 1,
                    Events = events
                }
            };

            var jsonString = result.ToString();

            var companyCode = icuDevice.CompanyId != null ? icuDevice.Company.Code : "";

            var routingKey = $"{Constants.RabbitMq.EventLogJsonTopic}.{companyCode}";

            //Send data to the frontend
            var message = Encoding.UTF8.GetBytes(jsonString);
            //_queueService.Publish(Constants.RabbitMq.EventLogJsonTopic, message, 1);
            _queueService.Publish(routingKey, message, 1);
        }

        /// <summary>
        /// Add single event log
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="icuDevice"></param>
        /// <param name="eventlogs"></param>
        /// <param name="response"></param>
        private void AddSingleEventLog(IUnitOfWork unitOfWork, List<EventLog> eventlogs, IcuDevice icuDevice, DeviceInstructionResponse response)
        {
            try
            {
                var originalMessagePayload = ApplicationVariables.SendingMessages.GetValueOrDefault(response.MsgId);
                var originalMessage = JsonConvert.DeserializeObject<DeviceInstructionProtocolData>(originalMessagePayload);
                var state = 0;
                var command = originalMessage.Data.Command;
                if (command == Constants.CommandType.UpdateDeviceState)
                {
                    state = GetState(originalMessagePayload);
                }
                else if (command == Constants.CommandType.SetTime)
                {
                    if (GetIsScheduleOption(originalMessagePayload))
                    {
                        // If schedule message, we dont need to create event.
                        return;
                    }
                }
                string userName = originalMessage.Sender;

                var status = response.Data.Status;
                var eventLog = new EventLog
                {
                    IcuId = icuDevice.Id,
                    DoorName = icuDevice.Name,
                    CompanyId = icuDevice.CompanyId,
                    EventType = (int)Helpers.GetEventType(command, state, status),
                    EventTime = DateTime.UtcNow,
                    UserName = userName
                    //UserName = GetUsername(payload)
                };
                unitOfWork.EventLogRepository.Add(eventLog);
                eventlogs.Add(eventLog);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
            }
        }

        /// <summary>
        /// Send load DeviceInfo
        /// </summary>
        /// <param name="deviceAddress"></param>
        private void SendDeviceInfo(string deviceAddress)
        {
            _sendMessageService.SendDeviceInfo(deviceAddress);
        }

        /// <summary>
        /// Parse main firmware payload
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        private MainFirmwareProtocolData ParseMainFirmwarePayload(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                _logger.LogWarning("Payload is empty");
                return null;
            }
            var payLoad = JsonConvert.DeserializeObject<MainFirmwareProtocolData>(jsonString);
            return payLoad;
        }
    }
}