﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Csv;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Prometheus;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class EventLogPerformanceConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly string[] _headers = {
            "Icu Device",
            "Number of event",
            "Time pushing event from ICU",
            "Time receiving event from MQ",
            "Time finish saving data into DB",
            "Time publish event log JSON to MQ",
            "Time message travel in queue",
            "Time parsing data",
            "Time checking if ICU is exist",
            "Time prepare event log object",
            "Time Save Event log to DB",
            "Time close DB connection",
            "Total time until finish saving data to DB",
            "Total time until publish event JSON to MQ"
        };

        private static readonly Histogram EventLogTotalTimeHistogram = Metrics
                    .CreateHistogram("dmp_event_log_processing_time", "Histogram of total event log processing time is milliseconds.",
                        new HistogramConfiguration
                        {
                            // We divide measurements in 10 buckets of $100 each, up to $1000.
                            Buckets = Histogram.ExponentialBuckets(start: 10, factor: 2, count: 10),
                            //Buckets = Histogram.
                            LabelNames = new[] { "topic", "deviceAddress" }
                        });

        public EventLogPerformanceConsumer(IConfiguration configuration)
        {
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<EventLogPerformanceConsumer>();
        }

        /// <summary>
        /// Receive event log data from icu and save into database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                //var jsonString = Encoding.UTF8.GetString(data);
                //var content = jsonString.Split(",");
                //var csvFile =
                //    $"ParseEventFromIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
                //var contents = new List<string[]> { content };
                //CsvHelper.Write(_headers, contents, csvFile);
                var jsonSerializer = new Common.Infrastructure.JsonSerializer();
                var msg = jsonSerializer.DeSerialize<PrometheusMetricData>(data);
                EventLogTotalTimeHistogram.WithLabels(msg.Topic, msg.DeviceAddress).Observe(msg.TotalMilliseconds);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
            }
        }
    }
}
