﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DeviceConfigConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private ISendMessageService _sendMessageService;
        private readonly IQueueService _queueService;
        private INotificationService _notificationService;
        public DeviceConfigConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceConfigConsumer>();
        }

        /// <summary>
        /// Receive message from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _sendMessageService = new SendMessageService(_queueService);
                _notificationService = new NotificationService(_queueService);
                var response = ParseData(data);
                var messageId = response.MsgId;

                var deviceAddress = Helpers.GetDeviceAddress(routingKey);

                if (!ApplicationVariables.SendingMessages.ContainsKey(response.MsgId))
                {
                    _logger.LogWarning($"({deviceAddress})Can not find original message of message {response.ToString()}! We ignore the message!");

                    return;
                }

                var originalMessagePayload = ApplicationVariables.SendingMessages.GetValueOrDefault(response.MsgId);

                var originalMessage = JsonConvert.DeserializeObject<BasicProtocolData>(originalMessagePayload);

                var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

                var companyCode = icuDevice.Company != null ? icuDevice.Company.Code : "";

                var userName = originalMessage.Sender;
                // If sender is Admin user we send to admin notification topic
                var account = unitOfWork.AppDbContext.Account.FirstOrDefault(m =>
                    m.Username == userName &&
                    !m.IsDeleted);
                if (account!=null && account.Type == 0)
                {
                    companyCode = "";
                }
                _notificationService.SendMessage(Constants.MessageType.Success,
                    Constants.NotificationType.SendDeviceInstructionSuccess, userName,
                    string.Format(MessageResource.DeviceConfigSuccess, deviceAddress, "update"), companyCode);
                
                if (response.Data.Status.Equals(Constants.MessageType.Failure,
                    StringComparison.OrdinalIgnoreCase))
                {
                    _notificationService.SendMessage(Constants.MessageType.Error,
                        Constants.NotificationType.SendDeviceConfigError, userName,
                        MessageResource.msgSendDeviceConfigError, companyCode);
                    return;
                }

                if (icuDevice == null)
                {
                    _logger.LogWarning($"Can not find the icu address {deviceAddress}!");
                    return;
                }

                icuDevice.ConnectionStatus = (short)IcuStatus.Connected;

                icuDevice.LastCommunicationTime = DateTime.Now;
                unitOfWork.IcuDeviceRepository.Update(icuDevice);
                _sendMessageService.SendConnectionStatus(icuDevice);
                unitOfWork.Save();
                
                unitOfWork.Dispose();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Parse data from icu response
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public IcuDeviceResponseProtocolData ParseData(byte[] data)
        {
            if (data.Length < 50)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var deviceResponse = JsonConvert.DeserializeObject<IcuDeviceResponseProtocolData>(jsonString);
            return deviceResponse;
        }
    }
}
