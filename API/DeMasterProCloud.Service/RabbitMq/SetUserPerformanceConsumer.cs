﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Csv;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;


namespace DeMasterProCloud.Service.RabbitMq
{
    public class SetUserPerformanceConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly bool _enableLog;
        private readonly string _sendUserToIcu =
            $"SendUserToIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
        private readonly string[] _headers = {
            "Request",
            "MsgId",
            "Message Type",
            "Device Address",
            "Total added user",
            "Total removed user",
            "User count",
            "Time receiving request (t1)",
            "Time parsing and adding data into memory (t2)",
            "Time saving data into database (t3)",
            "Time publishing message to queue (t4)",
            "Time receive reponse from Icu(5)",
            "t2-t1",
            "t3-t2",
            "t4-t3",
            "t5-t4"
        };

        public SetUserPerformanceConsumer(IConfiguration configuration)
        {
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<SetUserPerformanceConsumer>();
            _enableLog = Convert.ToBoolean(configuration[Constants.Settings.EnablePerformanceLog]);
        }

        /// <summary>
        /// Receive event log data from icu and save into database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                var jsonString = Encoding.UTF8.GetString(data);
                var content = jsonString.Split(",");
                if (content[0] == "Response_User_Log")
                {
                    //Todo:Open file csv and search for the row that have msgId
                    if (File.Exists(_sendUserToIcu))
                    {
                        var contents = new List<string[]>();
                        var lines = File.ReadAllLines(_sendUserToIcu);
                        for (var i = 1; i < lines.Length; i++)
                        {
                            var line = lines[i].Split(",");
                            if (line[1] == content[1])
                            {
                                line[11] = content[2];
                                var pusingTime = Convert.ToDateTime(line[10]);
                                var receiveTime = Convert.ToDateTime(content[2]);
                                line[15] = receiveTime.Subtract(pusingTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture);
                            }

                            contents.Add(line);
                        }

                        File.Delete(_sendUserToIcu);
                        CsvHelper.Write(_headers, contents, _sendUserToIcu);
                    }
                    //Add t5 and t5-t4
                }
                else if (content[0] == "Send_User_Log")
                {
                    var csvFile =
                        $"SendUserToIcu_{DateTime.Now.ToString("yyyyMMdd", CultureInfo.InvariantCulture)}.csv";
                    content = content.Where(x => x != content[0]).ToArray();
                    var contents = new List<string[]> { content };
                    CsvHelper.Write(_headers, contents, csvFile);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
            }
        }
    }
}
