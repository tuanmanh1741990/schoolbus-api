﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class MultipleMessagesConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly bool _enableLog;
        private readonly IQueueService _queueService;
        private IList<string> _pendingMessages;
        private IList<string> _discardMessageGroup;

        public MultipleMessagesConsumer(IConfiguration configuration, IQueueService queueService,
            IList<string> pendingMessages, IList<string> discardMessageGroup)
        {
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<MultipleMessagesConsumer>();
            _enableLog = Convert.ToBoolean(configuration[Constants.Settings.EnablePerformanceLog]);
            _queueService = queueService;
            _pendingMessages = pendingMessages;
            _discardMessageGroup = discardMessageGroup;
        }

        /// <summary>
        /// Receive event log data from icu and save into database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var _notificationService = new NotificationService(_queueService);
                var jsonSerializer = new Common.Infrastructure.JsonSerializer();

                var msg = jsonSerializer.DeSerialize<MultipleServiceProtocolData>(data);
                if (!_discardMessageGroup.Contains(msg.GroupMsgId))
                {
                    var deviceAddress = Helpers.GetDeviceAddress(msg.Topic);
                    var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);
                    var companyCode = icuDevice.CompanyId != null ? icuDevice.Company.Code : "";
                    //_pendingMessages.Add(msg.MsgId);

                    // Adding to pending list
                    if (ApplicationVariables.PendingGroupMessages.ContainsKey(deviceAddress))
                    {
                        ApplicationVariables.PendingGroupMessages[deviceAddress].Add(msg.MsgId);
                    }
                    else
                    {
                        ApplicationVariables.PendingGroupMessages[deviceAddress] = new List<string>();
                        ApplicationVariables.PendingGroupMessages[deviceAddress].Add(msg.MsgId);
                    }

                    var timeOut = Constants.RabbitMq.MessageResponseTimeout;

                    var waitToSendUpdating = 3000;

                    bool updateProgress = false;

                    // If the message is set device time, we update the content of the message to current time
                    if (msg.Topic.Contains(Constants.RabbitMq.DeviceInstructionTopic))
                    {
                        var deviceSetTimeInstruction = jsonSerializer.DeSerialize<DeviceInstructionSetTimeProtocolData>(Encoding.UTF8.GetBytes(msg.PayLoad));
                        if (deviceSetTimeInstruction.Data.Command == Constants.CommandType.SetTime)
                        {
                            msg.PayLoad = deviceSetTimeInstruction.ToString();
                        }
                        else if (deviceSetTimeInstruction.Data.Command == Constants.CommandType.UpdateFirmware)
                        {
                            //Here response only come back after device is restarted so we extend it longer
                            timeOut = timeOut * 3;
                            updateProgress = true;
                        }
                    }

                    _queueService.Publish(msg.Topic, msg.PayLoad);

                    var start = DateTime.Now;
                    var isSuccess = true;
                    int i = 0;

                    float fullProgress = 90f;
                    float preProgress = (msg.MessageIndex + 1) * (fullProgress / msg.TotalMessages);

                    while (ApplicationVariables.PendingGroupMessages[deviceAddress].Contains(msg.MsgId))
                    {
                        System.Threading.Thread.Sleep(Constants.RabbitMq.MessageResponseInterval);

                        i++;

                        // This condition is for stopping process immediately when API receives Fail response.
                        if (ApplicationVariables.PendingGroupMessages[deviceAddress].Contains(FileStatus.Failure.GetDescription()))
                        {
                            ApplicationVariables.PendingGroupMessages[deviceAddress].Remove(FileStatus.Failure.GetDescription());

                            isSuccess = false;
                            break;
                        }

                        var now = DateTime.Now;
                        TimeSpan timespan = now - start;
                        if (timespan.TotalMilliseconds > timeOut)
                        {
                            _logger.LogWarning(String.Format("Timeout: Message {0} take longer than {1} milliseconds to response", msg.PayLoad.ToString(), timeOut));
                            isSuccess = false;
                            // Remove message from the pending group message
                            if (ApplicationVariables.PendingGroupMessages.ContainsKey(deviceAddress))
                            {
                                ApplicationVariables.PendingGroupMessages[deviceAddress].Remove(msg.MsgId);
                            }
                            break;
                        }
                        // Send updating progress after 3 sec from all file transfer messages are sent
                        // 3 sec ->
                        else if (updateProgress && timespan.TotalMilliseconds > waitToSendUpdating && i > 10)
                        {
                            float increase = (100 - fullProgress - 1) / (timeOut / Constants.RabbitMq.MessageResponseInterval);
                            preProgress = preProgress + (increase * i);

                            _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, (decimal)preProgress, Constants.LongProgressName.Updating));
                            i = 0;
                        }
                    }

                    if (isSuccess)
                    {
                        // We receiced response successfully, update long progress status.
                        _logger.LogInformation(String.Format("At {0} Send message {1}/{2} to topic {3} successfully", msg.PublishedTime, msg.MessageIndex + 1, msg.TotalMessages, msg.Topic));

                        // Publish message to the long_process_progress queue to update the progress
                        if (!string.IsNullOrEmpty(msg.ProcessId))
                        {
                            // send progress bar data
                            if (msg.ActionType == Constants.ActionType.Reinstall)
                            {
                                //var progress = (Constants.LongProgressPercentage.ReinstallStep0 + Constants.LongProgressPercentage.ReinstallStep1) + ((Decimal)(msg.MessageIndex + 1) / msg.TotalMessages) * (100 - (Constants.LongProgressPercentage.ReinstallStep0 + Constants.LongProgressPercentage.ReinstallStep1));
                                decimal progress1 = (decimal)((msg.MessageIndex + 1) * 100) / msg.TotalMessages;
                                //_logger.LogInformation(String.Format("progress : {0} %, {1} %", progress, progress1));
                                //_queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, progress, Constants.LongProgressName.Reinstall));
                                _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, progress1, Constants.LongProgressName.Reinstall));
                            }
                            else if (msg.ActionType == Constants.ActionType.TransmitData)
                            {
                                //var progress = (Constants.LongProgressPercentage.ReinstallStep0 + Constants.LongProgressPercentage.ReinstallStep1) + ((Decimal)(msg.MessageIndex + 1) / msg.TotalMessages) * (100 - (Constants.LongProgressPercentage.ReinstallStep0 + Constants.LongProgressPercentage.ReinstallStep1));
                                decimal progress1 = (decimal)((msg.MessageIndex + 1) * 100) / msg.TotalMessages;
                                //_logger.LogInformation(String.Format("progress : {0} %, {1} %", progress, progress1));
                                //_queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, progress, Constants.LongProgressName.TransmitData));
                                _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, progress1, Constants.LongProgressName.TransmitData));
                            }
                            else if (msg.ActionType == Constants.ActionType.UpdateDevice)
                            {
                                var processUnit = fullProgress / msg.TotalMessages;
                                var progress = (msg.MessageIndex + 1) >= msg.TotalMessages ? 100 : ((msg.MessageIndex + 1) * processUnit);
                                _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, (decimal)progress, Constants.LongProgressName.Downloading));
                            }

                            // If received last message of the group success, then send notificationt to user.
                            if (msg.MessageIndex + 1 == msg.TotalMessages)
                            {
                                if (msg.ActionType == Constants.ActionType.Reinstall)
                                {
                                    _notificationService.SendMessage(Constants.MessageType.Success,
                                    Constants.NotificationType.ReinstallSuccess, msg.Username,
                                    string.Format(MessageResource.ReinstallSuccess, deviceAddress), companyCode);
                                }
                                else if (msg.ActionType == Constants.ActionType.Install)
                                {
                                    _notificationService.SendMessage(Constants.MessageType.Success,
                                    Constants.NotificationType.InstallSuccess, msg.Username,
                                    string.Format(MessageResource.InstallSuccess, deviceAddress), companyCode);
                                }
                                else if (msg.ActionType == Constants.ActionType.TransmitData)
                                {
                                    _notificationService.SendMessage(Constants.MessageType.Success,
                                    Constants.NotificationType.TransmitDataSuccess, msg.Username,
                                    string.Format(MessageResource.TransmitSuccess, deviceAddress), companyCode);
                                }
                                else if (msg.ActionType == Constants.ActionType.UpdateDevice)
                                {
                                    _notificationService.SendMessage(Constants.MessageType.Success,
                                   Constants.NotificationType.UploadSuccess, msg.Username,
                                   string.Format(MessageResource.MessageUpdateSuccess, "device", deviceAddress), companyCode);
                                }
                            }
                        }
                    }
                    else
                    {
                        _logger.LogInformation(String.Format("Send message {0}/{1} to topic {2} Failed", msg.MessageIndex + 1, msg.TotalMessages, msg.Topic));
                        // Send update progress bar
                        if (!string.IsNullOrEmpty(msg.ProcessId))
                        {
                            if (msg.ActionType == Constants.ActionType.Reinstall)
                            {
                                _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, -1, Constants.LongProgressName.ReinstallFailed));
                            }
                            else if (msg.ActionType == Constants.ActionType.TransmitData)
                            {
                                _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, -1, Constants.LongProgressName.TransmitDataFailed));
                            }
                            else if (msg.ActionType == Constants.ActionType.UpdateDevice)
                            {
                                _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(msg.ProcessId, -1, Constants.LongProgressName.UpdatingFailed));
                            }
                        }
                        // Send notification to webapp that reinstall fail

                        if (msg.ActionType == Constants.ActionType.Reinstall)
                        {
                            _notificationService.SendMessage(Constants.MessageType.Error,
                        Constants.NotificationType.ReinstallError, msg.Username,
                        string.Format(MessageResource.ReinstallError, deviceAddress), companyCode);
                        }
                        else if (msg.ActionType == Constants.ActionType.TransmitData)
                        {
                            _notificationService.SendMessage(Constants.MessageType.Error,
                        Constants.NotificationType.TransmitDataError, msg.Username,
                        string.Format(MessageResource.TransmitError, deviceAddress), companyCode);
                        }
                        else if (msg.ActionType == Constants.ActionType.UpdateDevice)
                        {
                            _notificationService.SendMessage(Constants.MessageType.Error,
                        Constants.NotificationType.UploadError, msg.Username,
                        string.Format(MessageResource.msgUploadFileError, deviceAddress), companyCode);
                        }
                        else if (msg.ActionType == Constants.ActionType.SetTimeAndGetInfo)
                        {
                            //TODO send Message about device status is 0
                            //var deviceData = new DeviceConnectionStatusDetail();
                            //deviceData.DeviceAddress = deviceAddress;
                            //deviceData.Status = 0;
                            //var protocolData = new DeviceConnectionStatusProtocolData
                            //{
                            //    MsgId = Guid.NewGuid().ToString(),
                            //    Type = Constants.Protocol.ConnectionStatus,
                            //    Data = deviceData
                            //};
                            //var protocolMessage = protocolData.ToString();
                            //var routingKey = Constants.RabbitMq.DeviceOnlineTopic;
                            //_queueService.Publish(routingKey, protocolMessage);
                        }

                        _discardMessageGroup.Add(msg.GroupMsgId);
                        // Clean up pendingMessages list
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
            }
        }
    }
}