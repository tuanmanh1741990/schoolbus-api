﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class AccessControlConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;

        public AccessControlConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<AccessControlConsumer>();
        }

        /// <summary>
        /// Do work for access control
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var receiveTime = DateTime.UtcNow;
                var response = ParseData(data);
                var messageId = response.MsgId;

                var deviceAddress = Helpers.GetDeviceAddress(routingKey);

                if (!ApplicationVariables.SendingMessages.ContainsKey(response.MsgId))
                {
                    _logger.LogWarning($"({deviceAddress})Can not find original message of message {response.ToString()}! We ignore the message!");

                    return;
                }
                var originalMessagePayload = ApplicationVariables.SendingMessages.GetValueOrDefault(response.MsgId);

                var originalMessage = JsonConvert.DeserializeObject<BasicProtocolData>(originalMessagePayload);

                //Send to get device info
                SendDeviceInfo(deviceAddress);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from icu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>

        private UserResponseProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var resultData = JsonConvert.DeserializeObject<UserResponseProtocolData>(jsonString);
            return resultData;
        }

        /// <summary>
        /// Publish a message to get device info
        /// <param name="deviceAddress"></param>
        /// <param name="groupId"></param>
        /// </summary>
        public void SendDeviceInfo(string deviceAddress, string groupId = "")
        {
            var deviceInfo = new DeviceInfoProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.LoadDeviceInfo,
                Data = new DeviceInfoProtocolDataHeader()
            };
            var message = deviceInfo.ToString();
            var topic = Constants.RabbitMq.DeviceInfoTopic + "." + deviceAddress;
            _queueService.Publish(topic, message);
        }
    }
}