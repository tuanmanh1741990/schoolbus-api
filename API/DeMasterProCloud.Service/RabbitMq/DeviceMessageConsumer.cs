﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DeviceMessageConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public DeviceMessageConsumer(IConfiguration configuration)
        {
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceMessageConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var response = ParseData(data);
                var messageId = response.MsgId;
                unitOfWork.Save();
                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private DeviceMessageProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var deviceMessageResponse = JsonConvert.DeserializeObject<DeviceMessageProtocolData>(jsonString);
            return deviceMessageResponse;
        }
    }
}