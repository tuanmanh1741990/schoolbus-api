﻿using System;
using System.Collections.Generic;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System.Net.Security;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class QueueHelper
    {
        /// <summary>
        /// Get connection factory information from environment variables first. If it has been not found then get from appsettings.json file.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static ConnectionFactory GetConnectionFactory(IConfiguration configuration)
        {
            ConnectionFactory factory;
            Boolean enableSsl;
            var queueHost = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsHost);
            if (!string.IsNullOrEmpty(queueHost))
            {
                factory = new ConnectionFactory
                {
                    HostName = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsHost),
                    VirtualHost = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsVirtualHost),
                    Port = int.Parse(Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsPort)),
                    UserName = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsUserName),
                    Password = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsPassword)
                };
                enableSsl = bool.Parse(Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsEnableSsl));
            }
            else
            {
                factory = new ConnectionFactory
                {
                    HostName = configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsHost),
                    VirtualHost = configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsVirtualHost),
                    Port = configuration.GetValue<int>(Constants.Settings.QueueConnectionSettingsPort),
                    UserName = configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsUserName),
                    Password = configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsPassword)
                };
                enableSsl = configuration.GetValue<bool>(Constants.Settings.QueueConnectionSettingsEnableSsl);
            }
            if (enableSsl)
            {
                factory.Ssl = GetSslOption(configuration, queueHost);
            }
            return factory;
        }

        private static SslOption GetSslOption(IConfiguration configuration, String queueHost = null)
        {
            SslOption sslOption;

            if (!string.IsNullOrEmpty(queueHost))
            {
                sslOption = new SslOption
                {
                    Enabled = true,
                    AcceptablePolicyErrors = SslPolicyErrors.RemoteCertificateNameMismatch |
                                            SslPolicyErrors.RemoteCertificateChainErrors,
                    CertPath = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsCertPath),
                    CertPassphrase = Environment.GetEnvironmentVariable(Constants.Settings.QueueEnvironmentConnectionSettingsCertPassphrase)
                };
            }
            else
            {
                sslOption = new SslOption
                {
                    Enabled = true,
                    AcceptablePolicyErrors = SslPolicyErrors.RemoteCertificateNameMismatch |
                                            SslPolicyErrors.RemoteCertificateChainErrors,
                    CertPath = configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsCertPath),
                    CertPassphrase = configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsCertPassphrase)
                };
            }

            return sslOption;
        }
    }
}
