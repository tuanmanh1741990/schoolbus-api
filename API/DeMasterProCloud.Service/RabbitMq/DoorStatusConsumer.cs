﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DoorStatusConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        private ISendMessageService _sendMessageService;
        private INotificationService _notificationService;
        public DoorStatusConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DoorStatusConsumer>();

        }
        

        /// <summary>
        /// Receive status message from device
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _sendMessageService = new SendMessageService(_queueService);
                _notificationService = new NotificationService(_queueService);

                var response = ParseData(data);
                var deviceAddress = response.Data.DeviceAddress;
                var doorStatus = response.Data.DoorState;

                var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

                if (icuDevice == null)
                {
                    _logger.LogWarning($"Can not find the icu address {deviceAddress}!");
                    return;
                }

                //update device
                icuDevice.ConnectionStatus = (short)IcuStatus.Connected;
                icuDevice.LastCommunicationTime = DateTime.Now;
                if (!string.IsNullOrEmpty(response.Data.DoorState.ToString()))
                {
                    switch (doorStatus)
                    {
                        case (int)DoorStatus.ClosedAndLock:
                            icuDevice.DoorStatus = DoorStatus.ClosedAndLock.GetDescription();
                            break;

                        case (int)DoorStatus.ClosedAndUnlocked:
                            icuDevice.DoorStatus = DoorStatus.ClosedAndUnlocked.GetDescription();
                            break;

                        case (int)DoorStatus.Opened:
                            icuDevice.DoorStatus = DoorStatus.Opened.GetDescription();
                            break;

                        case (int)DoorStatus.ForceOpened:
                            icuDevice.DoorStatus = DoorStatus.ForceOpened.GetDescription();
                            break;

                        case (int)DoorStatus.PassageOpened:
                            icuDevice.DoorStatus = DoorStatus.PassageOpened.GetDescription();
                            break;

                        case (int)DoorStatus.EmergencyOpened:
                            icuDevice.DoorStatus = DoorStatus.EmergencyOpened.GetDescription();
                            break;

                        case (int)DoorStatus.EmergencyClosed:
                            icuDevice.DoorStatus = DoorStatus.EmergencyClosed.GetDescription();
                            break;

                        case (int)DoorStatus.Unknown:
                            icuDevice.DoorStatus = DoorStatus.Unknown.GetDescription();
                            break;

                        case (int)DoorStatus.Lock:
                            icuDevice.DoorStatus = DoorStatus.Lock.GetDescription();
                            break;

                        case (int)DoorStatus.Unlock:
                            icuDevice.DoorStatus = DoorStatus.Unlock.GetDescription();
                            break;
                    }
                }

                unitOfWork.IcuDeviceRepository.Update(icuDevice);

                unitOfWork.Save();
                _sendMessageService.SendConnectionStatus(icuDevice, icuDevice.DoorStatus);
                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private DoorStatusProtocol ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var result = JsonConvert.DeserializeObject<DoorStatusProtocol>(jsonString);
            return result;
        }
    }
}