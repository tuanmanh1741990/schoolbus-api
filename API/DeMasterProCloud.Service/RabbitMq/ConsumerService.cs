﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using FluentScheduler;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public interface IConsumerService
    {
        //void HandleAccessControlResponseForDevice(AccessControlConsumer accessControlConsumer, IModel channel, string routingKey, string deviceAddress);
        void Register();
    }

    /// <summary>
    /// Consumer service
    /// </summary>
    public class ConsumerService : IConsumerService
    {
        private readonly IConfiguration _configuration;

        //private readonly IConnection _connection;
        private readonly List<IcuDevice> _devices;

        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        private List<string> _pendingMessages;
        private List<string> _discardMessageGroup;
        private readonly ILogger _logger;
        private IHostingEnvironment _env;

        /// <summary>
        /// Consumer service constructor
        /// </summary>
        /// <param name="configuration"></param>
        public ConsumerService(IConfiguration configuration, IHostingEnvironment env)
        {
            _configuration = configuration;

            //Get active device list
            _unitOfWork = DbHelper.CreateUnitOfWork(configuration);
            _devices = _unitOfWork.IcuDeviceRepository.GetUndeletedDevice().ToList();
            _queueService = new QueueService(_configuration);
            JobManager.Initialize(new MyRegistry(_configuration, _queueService, _unitOfWork));
            _pendingMessages = new List<string>();
            _discardMessageGroup = new List<string>();
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<ConsumerService>();

            _env = env;
        }

        /// <summary>
        /// Register consumer by routing key
        /// </summary>
        public void Register()
        {
            //Register general consumer
            HandleConnectionStatus(Constants.RabbitMq.DeviceOnlineTopic);
            HandleEventLogPerformance(Constants.RabbitMq.EventLogTaskQueue);
            HandleSetUserPerformance(Constants.RabbitMq.SetUserTaskQueue);
            HandleDoorStatus(Constants.RabbitMq.DoorStatusTopic);

            //Register device related consumer based on device
            if (_devices != null && _devices.Any())
            {
                var multipleMessagesConsumer = new MultipleMessagesConsumer(_configuration, _queueService, _pendingMessages, _discardMessageGroup);
                var _connection = ApplicationVariables.getAvailableConnection(_configuration);
                foreach (var device in _devices)
                {
                    // Handle multiple message task queue
                    var channel1 = _connection.CreateModel();
                    HandleMultipleMessagesTaskPerDevice(multipleMessagesConsumer, $"{Constants.RabbitMq.MultipleMessagesTaskQueue}_{device.DeviceAddress}", device, channel1);
                    // Handle all the message came from device
                    var channel2 = _connection.CreateModel();
                    HandleResponse(device, channel2, _configuration, _queueService, _pendingMessages);
                }
            }
        }

        /// <summary>
        /// Handle for check holiday response
        /// </summary>
        public static void HandleResponse(IcuDevice device, IModel channel, IConfiguration configuration, IQueueService queueService, List<string> pendingMessages)
        {
            var accessLevelLogConsumer = new AccessControlConsumer(configuration, queueService);
            var deviceInfoConsumer = new DeviceInfoConsumer(configuration, queueService);
            var deviceConfigConsumer = new DeviceConfigConsumer(configuration, queueService);
            var deviceInstructionConsumer = new DeviceInstructionConsumer(configuration, queueService);
            var timezoneConsumer = new TimezoneConsumer(configuration, queueService);
            var holidayConsumer = new HolidayConsumer(configuration, queueService);
            var fileTransferConsumer = new FileTransferConsumer(configuration, queueService);
            var deviceMessageConsumer = new DeviceMessageConsumer(configuration);
            var deviceSettingConsumer = new DeviceSettingConsumer(configuration, queueService);
            var userInfoConsumer = new UserInfoConsumer(configuration, queueService);
            var eventCountConsumer = new EventCountConsumer(configuration, queueService);
            var eventRecoveryConsumer = new EventRecoveryConsumer(configuration, queueService);
            var checkHolidayConsumer = new CheckHolidayConsumer(configuration, queueService);
            var checkTimezoneConsumer = new CheckTimezoneConsumer(configuration, queueService);
            var eventLogConsumer = new EventLogConsumer(configuration, queueService);

            List<string> responseTopics = new List<string>(new string[] {
            });

            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: $".topic.*.{device.DeviceAddress}");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                Boolean resultOfResponse = true;

                //Distribute to corresponsding consumer
                //if (ea.RoutingKey.Contains("_response"))
                //    Helpers.RemoveMsgIdFromPendingList(ea.Body, ref pendingMessages);
                
                if (ea.RoutingKey == $"{Constants.RabbitMq.LoadTimezoneResponseTopic}.{device.DeviceAddress}")
                {
                    checkTimezoneConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.AccessControlResponseTopic}.{device.DeviceAddress}")
                {
                    accessLevelLogConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.DeviceInfoResponseTopic}.{device.DeviceAddress}")
                {
                    deviceInfoConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.ConfigurationResponseTopic}.{device.DeviceAddress}")
                {
                    deviceConfigConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.DeviceInstructionResponseTopic}.{device.DeviceAddress}")
                {
                    deviceInstructionConsumer.DoWork(ea.Body);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.TimezoneResponseTopic}.{device.DeviceAddress}")
                {
                    timezoneConsumer.DoWork(ea.Body);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.HolidayResponseTopic}.{device.DeviceAddress}")
                {
                    holidayConsumer.DoWork(ea.Body);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.FileTranferResponseTopic}.{device.DeviceAddress}")
                {
                    resultOfResponse = fileTransferConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.DeviceMessageResponseTopic}.{device.DeviceAddress}")
                {
                    deviceMessageConsumer.DoWork(ea.Body);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.DeviceSettingResponseTopic}.{device.DeviceAddress}")
                {
                    deviceSettingConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.LoadUserResponseTopic}.{device.DeviceAddress}")
                {
                    userInfoConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.EventLogEventCountResponseTopic}.{device.DeviceAddress}")
                {
                    eventCountConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.EventRecoveryResponseTopic}.{device.DeviceAddress}")
                {
                    eventRecoveryConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.LoadHolidayResponseTopic}.{device.DeviceAddress}")
                {
                    checkHolidayConsumer.DoWork(ea.Body, ea.RoutingKey);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else if (ea.RoutingKey == $"{Constants.RabbitMq.EventLogTopic}.{device.DeviceAddress}")
                {
                    await eventLogConsumer.DoWork(ea.Body);
                    channel.BasicAck(ea.DeliveryTag, false);
                }
                else
                {
                    // Discard message
                    var logger = ApplicationVariables.LoggerFactory.CreateLogger<ConsumerService>();
                    //logger.LogInformation($"Discard message from topic: {ea.RoutingKey}");
                    channel.BasicAck(ea.DeliveryTag, false);
                }

                // Remove messages from sending message list when received response
                if (ea.RoutingKey.Contains("_response"))
                {
                    var jsonString = Encoding.UTF8.GetString(ea.Body);
                    try
                    {
                        var msg = JsonConvert.DeserializeObject<BasicProtocolData>(jsonString);

                        if (ApplicationVariables.SendingMessages.ContainsKey(msg.MsgId))
                        {
                            ApplicationVariables.SendingMessages.TryRemove(msg.MsgId, out _);
                        }

                        device.ConnectionStatus = (short)IcuStatus.Connected;

                        device.LastCommunicationTime = DateTime.Now;

                        if (resultOfResponse)
                        {
                            // Remove message from the pending group message
                            if (ApplicationVariables.PendingGroupMessages.ContainsKey(device.DeviceAddress))
                            {
                                ApplicationVariables.PendingGroupMessages[device.DeviceAddress].Remove(msg.MsgId);
                            }
                        }
                        // The result of response is "Failure". 
                        else
                        {
                            //ApplicationVariables.PendingGroupMessages[device.DeviceAddress];
                        }
                    }
                    catch (Exception ex)
                    {

                        var deviceAddress = device.DeviceAddress;
                        var logger = ApplicationVariables.LoggerFactory.CreateLogger<ConsumerService>();
                        logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                        logger.LogWarning($"There is error when processing data from ({deviceAddress}): {jsonString}");
                    }
                }
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);

            // Adding channels to existing list
            if (ApplicationVariables.DeviceChannelList.ContainsKey(device.DeviceAddress))
            {
                ApplicationVariables.DeviceChannelList[device.DeviceAddress].Add(channel);
            }
            else
            {
                ApplicationVariables.DeviceChannelList[device.DeviceAddress] = new List<IModel>();
                ApplicationVariables.DeviceChannelList[device.DeviceAddress].Add(channel);
            }
        }

        /// <summary>
        /// Handler connection status for device
        /// </summary>
        /// <param name="routingKey"></param>
        private void HandleConnectionStatus(string routingKey)
        {
            var _connection = ApplicationVariables.getAvailableConnection(_configuration);
            var channel = _connection.CreateModel();
            var deviceConsumer = new DeviceConnectionStatusConsumer(_configuration, _queueService);
            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: routingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                deviceConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
        }

        /// <summary>
        /// Handler status for door
        /// </summary>
        /// <param name="routingKey"></param>
        private void HandleDoorStatus(string routingKey)
        {
            var _connection = ApplicationVariables.getAvailableConnection(_configuration);
            var channel = _connection.CreateModel();
            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: routingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceConsumer = new DoorStatusConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                deviceConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
        }

        /// <summary>
        /// Register consumer to get event log from an icu device
        /// </summary>
        /// <param name="queueService"></param>
        /// <param name="routingKey"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="settingService"></param>
        public void HandleEventLogForDevice(IQueueService queueService, ISettingService settingService,
            string routingKey, string deviceAddress, IModel channel = null)
        {
            if (channel == null)
            {
                var _connection = ApplicationVariables.getAvailableConnection(_configuration);
                channel = _connection.CreateModel();
            }
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{routingKey}.{deviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var eventLogConsumer = new EventLogConsumer(_configuration, queueService);
                await eventLogConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Register consumer to get user data log from icu for an icu device
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="routingKey"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="queueService"></param>
        public void HandleAccessControlResponseForDevice(AccessControlConsumer accessLevelLogConsumer, IModel channel, string routingKey, string deviceAddress)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{routingKey}.{deviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                accessLevelLogConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handler for event log performance response
        /// <param name="routingKey"></param>
        /// </summary>
        private void HandleEventLogPerformance(string routingKey)
        {
            var _connection = ApplicationVariables.getAvailableConnection(_configuration);
            var channel = _connection.CreateModel();
            channel.QueueDeclare(queue: routingKey,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceConsumer = new EventLogPerformanceConsumer(_configuration);
                deviceConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: routingKey,
                autoAck: false,
                consumer: consumer);
        }

        /// <summary>
        /// Handler for set user performance response
        /// <param name="routingKey"></param>
        /// </summary>
        private void HandleSetUserPerformance(string routingKey)
        {
            var _connection = ApplicationVariables.getAvailableConnection(_configuration);
            var channel = _connection.CreateModel();
            channel.QueueDeclare(queue: routingKey,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceConsumer = new SetUserPerformanceConsumer(_configuration);
                deviceConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: routingKey,
                autoAck: false,
                consumer: consumer);
        }

        /// <summary>
        /// Handler for publishing set of multiple message to device
        /// <param name="routingKey"></param>
        /// </summary>
        private void HandleMultipleMessagesTaskPerDevice(MultipleMessagesConsumer multipleMessagesConsumer, string routingKey, IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare(queue: routingKey,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null).QueueName;
            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                multipleMessagesConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: routingKey,
                autoAck: false,
                consumer: consumer);

            // Add channel to device channel list
            if (ApplicationVariables.DeviceChannelList.ContainsKey(device.DeviceAddress))
            {
                ApplicationVariables.DeviceChannelList[device.DeviceAddress].Add(channel);
            }
            else
            {
                ApplicationVariables.DeviceChannelList[device.DeviceAddress] = new List<IModel>();
                ApplicationVariables.DeviceChannelList[device.DeviceAddress].Add(channel);
            }
        }
    }
}