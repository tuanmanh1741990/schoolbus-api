﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DeviceSettingConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        private readonly ILogger _logger;

        public DeviceSettingConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceSettingConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var response = ParseData(data);
                var messageId = response.MsgId;
                unitOfWork.Save();
                var deviceAddress = Helpers.GetDeviceAddress(routingKey);
                SendToFe(unitOfWork, response, deviceAddress);

                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private DeviceSettingResponse ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var deviceSettingResponse = JsonConvert.DeserializeObject<DeviceSettingResponse>(jsonString);

            return deviceSettingResponse;
        }

        /// <summary>
        /// Send to fe
        /// </summary>
        /// <param name="response"></param>
        /// <param name="unitOfWork"></param>
        public void SendToFe(IUnitOfWork unitOfWork, DeviceSettingResponse response, string deviceAddress)
        {
            IcuDevice device = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);
            var deviceInfoReceive = response.Data;
            // For send to web app.
            var deviceInfo = Mapper.Map<DeviceSettingDetail>(deviceInfoReceive);

            var activeTimezone = unitOfWork.TimezoneRepository.GetByPositionAndCompany(deviceInfoReceive.ActiveTimezone ?? 0, device.CompanyId);
            deviceInfo.ActiveTimezone = deviceInfoReceive.ActiveTimezone == null ? null : activeTimezone?.Name;

            var passageTimezone = unitOfWork.TimezoneRepository.GetByPositionAndCompany(deviceInfoReceive.PassageTimezone ?? 0, device.CompanyId);
            deviceInfo.PassageTimezone = deviceInfoReceive.PassageTimezone == null ? null : passageTimezone?.Name;

            var readerConfig = response.Data.ReaderConfig;

            if (readerConfig != null)
            {
                ParseConfigData(ref deviceInfo, response, 0);

                if (response.Data.ReaderCount > 1)
                {
                    ParseConfigData(ref deviceInfo, response, 1);
                }
            }

            var protocol = new SendDeviceSettingProtocol
            {
                MsgId = response.MsgId,
                Type = Constants.Protocol.LoadDeviceSettingWebApp,
                Data = deviceInfo
            };
            var routingKey = Constants.RabbitMq.DeviceSettingWebAppTopic;
            _queueService.Publish(routingKey, protocol.ToString());
        }

        public void ParseConfigData(ref DeviceSettingDetail deviceInfo, DeviceSettingResponse response, int readerNumber)
        {
            var readerConfig = response.Data.ReaderConfig;
            var config = Convert.ToByte(readerConfig[readerNumber]);

            BitArray bits = new BitArray(new byte[] { config });

            if (readerNumber == 0)
            {
                deviceInfo.BuzzerReader0 = bits[2] ? "OFF" : "ON";

                //if(deviceInfo.DeviceType == (int) DeviceType.Icu300N)
                //{
                deviceInfo.LedReader0 = bits[1] ? "Red" : "Blue";
                //}
                //else if(deviceInfo.DeviceType == (int)DeviceType.ItouchPop)
                //{
                deviceInfo.UseCardReader = bits[1] ? "Not use" : "Use";
                //}

                deviceInfo.RoleReader0 = bits[0] ? "Out" : "In";
            }
            else if (readerNumber == 1)
            {
                deviceInfo.BuzzerReader1 = bits[2] ? "OFF" : "ON";

                deviceInfo.LedReader1 = bits[1] ? "Red" : "Blue";

                deviceInfo.RoleReader1 = bits[0] ? "Out" : "In";
            }
        }
    }
}