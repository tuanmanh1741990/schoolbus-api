﻿using System;
using System.Linq;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.Repository;
using Microsoft.EntityFrameworkCore;
using System.Threading;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class EventCountConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        private INotificationService _notificationService;

        public EventCountConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _queueService = queueService;
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<EventCountConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _notificationService = new NotificationService(_queueService);
                var response = ParseData(data);
                var messageId = response.MsgId;
                   unitOfWork.Save();

                var deviceAddress = Helpers.GetDeviceAddress(routingKey);
                var device = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

                // Insert date condition to Query.
                // Date must be calculated by timezone.
                var buildingId = unitOfWork.IcuDeviceRepository.GetByIcuId(device.Id).BuildingId;
                var buildingTimeZone = unitOfWork.BuildingRepository.GetById(buildingId.Value).TimeZone;

                var processId = response.Data.ProcessId;
                var fromDateTime = response.Data.FromTime.Equals("00000000000000") ? new DateTime() : DateTimeHelper.ConvertDateTimeByTimeZoneToSystemTimeZone(response.Data.FromTime, buildingTimeZone);
                var toDateTime = response.Data.ToTime.Equals("00000000000000") ? DateTime.Now : DateTimeHelper.ConvertDateTimeByTimeZoneToSystemTimeZone(response.Data.ToTime, buildingTimeZone);

                if (device == null)
                {
                    _logger.LogWarning($"Can not find the device address {deviceAddress}!");
                    return;
                }

                var deviceId = device.Id;
                var deviceCount = response.Data.Count;
                var queryable = unitOfWork.AppDbContext.EventLog
                    .Include(m => m.User)
                    .Include(m => m.User.Department)
                    .Include(m => m.Icu)
                    .Include(m => m.Icu.Building)
                    .Include(m => m.Company)
                    // The event No.26, 27 are made by API.
                    // Device doesn't have these events.
                    // So, These events are not searched.
                    .Where(m => m.EventType != 26 && m.EventType != 27)
                    .AsQueryable();

                if (!response.Data.FromTime.Equals("00000000000000"))
                {
                    queryable = queryable.Where(m =>
                        m.EventTime >= fromDateTime);
                }

                if (!response.Data.ToTime.Equals("00000000000000"))
                {
                    queryable = queryable.Where(m =>
                        m.EventTime <= toDateTime);
                }

                var dbCount = queryable.Count(d => d.IcuId == deviceId);

                var eventCountDetail = new SendEventCountDetailData
                {
                    ProcessId = processId,
                    DeviceId = deviceId,
                    DeviceEventCount = deviceCount,
                    DbEventCount = dbCount,
                    FromTime = response.Data.FromTime
                };

                SendDataToFe(unitOfWork, eventCountDetail);
                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private EventCountResponse ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var eventCountResponse = JsonConvert.DeserializeObject<EventCountResponse>(jsonString);
            return eventCountResponse;
        }


        /// <summary>
        /// Send event count data to front end
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="eventCountDetail"></param>
        internal void SendDataToFe(IUnitOfWork unitOfWork, SendEventCountDetailData eventCountDetail)
        {

            var result = new SendEventCountModelData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.EventCountWebApp,
                Data = eventCountDetail,
            };

            var jsonString = result.ToString();

            var companyId = unitOfWork.IcuDeviceRepository.GetById(eventCountDetail.DeviceId).CompanyId;
            var companyCode = unitOfWork.CompanyRepository.GetById(companyId ?? 0).Code;
            var routingKey = $"{Constants.RabbitMq.EventCountJsonTopic}.{companyCode}";

            //Send data to the frontend
            var message = Encoding.UTF8.GetBytes(jsonString);
            Thread.Sleep(1500);
            _queueService.Publish(routingKey, message, 1);
        }
    }
}
