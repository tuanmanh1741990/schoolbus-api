using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DeviceConnectionStatusConsumer
    {
        private readonly IConfiguration _configuration;
        private ISettingService _settingService;
        private IMailService _mailService;
        private readonly ILogger _logger;
        private ISendMessageService _sendMessageService;
        private readonly IQueueService _queueService;

        public DeviceConnectionStatusConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceConnectionStatusConsumer>();
        }

        /// <summary>
        /// Receive connection status from icu and save into database
        /// </summary>
        /// <param name="data"></param>
        public bool DoWork(byte[] data)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _settingService = new SettingService(unitOfWork, _configuration);
                _mailService = new MailService(_configuration);
                _sendMessageService = new SendMessageService(_queueService);

                var result = ParseData(data);
                if (result == null)
                {
                    return false;
                }

                //Check icu address is register in the cloud or not
                var deviceAddress = result.Data.DeviceAddress;
                var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);
                // Check mac address
                var macAddress = result.Data.MacAddress;
                var icuDeviceByMac = unitOfWork.IcuDeviceRepository.GetDeviceByMacAddress(macAddress);

                var status = result.Data.Status;

                if ((icuDeviceByMac == null /* &&I think it would be good to have a condition to check the form of the MAC address.*/) && (icuDevice == null && deviceAddress.Length == Constants.MaxDeviceAddressDigit))
                {
                    var unregistedDevice = unitOfWork.UnregistedDevicesRepository.GetByDeviceAddress(deviceAddress);
                    var unregistedDeviceByMac = unitOfWork.UnregistedDevicesRepository.GetByMacAddress(macAddress);

                    if (unregistedDeviceByMac == null && status == (short)IcuStatus.Connected)
                    {
                        var device = Mapper.Map<UnregistedDevice>(result);
                        unitOfWork.UnregistedDevicesRepository.Add(device);
                    }
                    else if (unregistedDeviceByMac != null && status == (short)IcuStatus.Connected)
                    {
                        unregistedDeviceByMac.DeviceAddress = result.Data.DeviceAddress;
                        unregistedDeviceByMac.IpAddress = result.Data.IpAddress;
                        unregistedDeviceByMac.UpdatedOn = DateTime.UtcNow;

                        unitOfWork.UnregistedDevicesRepository.Update(unregistedDeviceByMac);
                    }

                    unitOfWork.Save();
                    return false;
                }
                else if (icuDevice != null && icuDeviceByMac == null && deviceAddress.Length == Constants.MaxDeviceAddressDigit)
                {
                    if (icuDevice.MacAddress == null)
                    {
                        _logger.LogInformation("MacAddress is null in DB.");
                        icuDevice.MacAddress = macAddress;
                        unitOfWork.IcuDeviceRepository.Update(icuDevice);

                        unitOfWork.Save();
                    }
                    else
                    {
                        // TODO propose reinstalling.
                        return false;
                    }
                }
                else if ((icuDevice != null && icuDeviceByMac != null && deviceAddress.Length == Constants.MaxDeviceAddressDigit) || status == 0)
                {
                    if (((icuDevice.MacAddress == macAddress) && (icuDeviceByMac.DeviceAddress == deviceAddress)) || status == 0)
                    {
                        // Check if device status is changed. We only create event when status changed.
                        // If status same as status stored in DB, we did not create event.
                        if (icuDevice.ConnectionStatus != status)
                        {
                            icuDevice.ConnectionStatus = (short)status;
                            icuDevice.LastCommunicationTime = DateTime.Now;
                            unitOfWork.IcuDeviceRepository.Update(icuDevice);
                            
                            // Add event log for Communication Status
                            var eventType = EventType.CommunicationSucceed;
                            if (icuDevice.ConnectionStatus == 0)
                            {
                                eventType = EventType.CommunicationFailed;
                            }
                            var eventLog = new EventLog
                            {
                                IcuId = icuDevice.Id,
                                DoorName = icuDevice.Name,
                                CompanyId = icuDevice.CompanyId,
                                EventType = (int)eventType,
                                EventTime = DateTime.UtcNow
                            };
                            unitOfWork.EventLogRepository.Add(eventLog);

                            // Send event log to webapp
                            var eventLogDetail = Mapper.Map<SendEventLogDetailData>(eventLog);
                            eventLogDetail.Id = Guid.NewGuid();
                            eventLogDetail.Device = icuDevice.DeviceAddress;
                            eventLogDetail.DoorName = icuDevice.Name;
                            eventLogDetail.EventDetailCode = eventLog.EventType;
                            eventLogDetail.UnixTime =
                                (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                            eventLogDetail.BuildingId = icuDevice.BuildingId.Value;

                            var events = new List<SendEventLogDetailData> { eventLogDetail };

                            var feEvent = new SendEventLogListModelData
                            {
                                MsgId = Guid.NewGuid().ToString(),
                                Type = Constants.Protocol.EventLogWebApp,
                                Data = new SendEventLogHeaderData
                                {
                                    Total = 1,
                                    Events = events
                                }
                            };

                            var companyCode = icuDevice.Company != null ? icuDevice.Company.Code : "";
                            var routingKey = $"{Constants.RabbitMq.EventLogJsonTopic}.{companyCode}";

                            var jsonString = feEvent.ToString();
                            var message = Encoding.UTF8.GetBytes(jsonString);
                            _queueService.Publish(routingKey, message, 1);

                            unitOfWork.Save();

                            _sendMessageService.SendConnectionStatus(icuDevice);

                            // Send notification emails
                            if (icuDevice.BuildingId != null && icuDevice.CompanyId != null)
                            {
                                var companyId = icuDevice.Company.Id;
                                Int32 buildingId = icuDevice.BuildingId.Value;
                                var buildingName = unitOfWork.BuildingRepository.GetById(buildingId).Name ?? "YourBuilding";

                                // Check whether "notification_email" setting value is checked or not.
                                var isNotificationEmail = _settingService
                                    .GetByKey(Constants.Settings.HasNotificationEmail, companyId).Value;
                                var isNotification = Helpers.GetStringFromValueSetting(isNotificationEmail);

                                // In this case, "notification_email" is TRUE in DB.
                                if (Convert.ToBoolean(isNotification))
                                {
                                    var settingValue_EmailList = _settingService
                                        .GetByKey(Constants.Settings.ListUserToNotificationEmail, companyId).Value;

                                    var emailList = getEmailListToNotification(settingValue_EmailList);

                                    if (emailList == null || !emailList.Any())
                                    {
                                        _logger.LogWarning("There is no user in the user notification list");
                                    }
                                    else
                                    {
                                        foreach (var user in emailList)
                                        {
                                            if (!string.IsNullOrEmpty(user))
                                            {
                                                var companyLanguage = _settingService.GetLanguage(companyId);
                                                var culture = new CultureInfo(companyLanguage);

                                                if (result.Data.Status == (short)IcuStatus.Disconneted)
                                                {
                                                    var subject = string.Format(MailContentResource.ResourceManager.GetString("SubjectDeviceDisconnected", culture),
                                                                                icuDevice.Name,
                                                                                result.Data.DeviceAddress,
                                                                                buildingName);

                                                    var contents = string.Format(MailContentResource.ResourceManager.GetString("BodyDeviceDisconnected", culture),
                                                                                icuDevice.Name,
                                                                                result.Data.DeviceAddress,
                                                                                buildingName);

                                                    _mailService.SendDeviceConnectionStatusMail(user, subject, contents, culture);
                                                }
                                                else if (result.Data.Status == (short)IcuStatus.Connected)
                                                {
                                                    var subject = string.Format(MailContentResource.ResourceManager.GetString("SubjectDeviceConnected", culture),
                                                                                icuDevice.Name,
                                                                                result.Data.DeviceAddress,
                                                                                buildingName);

                                                    var contents = string.Format(MailContentResource.ResourceManager.GetString("BodyDeviceConnected", culture),
                                                                                icuDevice.Name,
                                                                                result.Data.DeviceAddress,
                                                                                buildingName);

                                                    _mailService.SendDeviceConnectionStatusMail(user, subject, contents, culture);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    unitOfWork.Dispose();
                    return true;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
            }
            return false;
        }

        /// <summary>
        /// Parsing data from ICU
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public DeviceConnectionStatusProtocolData ParseData(byte[] data)
        {
            if (data.Length < 13)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var strData = Encoding.UTF8.GetString(data);
            var resultData = JsonConvert.DeserializeObject<DeviceConnectionStatusProtocolData>(strData);
            return resultData;
        }

        public string[] getEmailListToNotification(string settingValue_EmailList)
        {
            if (string.IsNullOrEmpty(settingValue_EmailList))
            {
                _logger.LogWarning($"Can not find value in the key {Constants.Settings.ListUserToNotificationEmail}");

                return null;
            }

            var emailListValue = Helpers.GetStringFromValueSetting(settingValue_EmailList);

            if (string.IsNullOrEmpty(emailListValue))
            {
                _logger.LogWarning("There is no user in the user notification list");

                return null;
            }

            var emailList = emailListValue.Split(",");

            return emailList;
        }
    }
}