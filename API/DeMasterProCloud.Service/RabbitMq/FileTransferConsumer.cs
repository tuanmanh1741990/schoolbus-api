﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class FileTransferConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;

        public FileTransferConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<FileTransferConsumer>();
        }

        /// <summary>
        /// Receive response from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public Boolean DoWork(byte[] data, string routingKey)
        {
            try
            {
                var response = ParseData(data);

                if (response.Data.Status.Equals(FileStatus.Success.GetDescription()))
                {
                    return true;
                }
                else if (response.Data.Status.Equals(FileStatus.Failure.GetDescription()))
                {
                    var deviceAddress = Helpers.GetDeviceAddress(routingKey);

                    ApplicationVariables.PendingGroupMessages[deviceAddress].Add(FileStatus.Failure.GetDescription());

                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private DeviceUploadFileResponse ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"[File transfer]Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var filetransferResponse = JsonConvert.DeserializeObject<DeviceUploadFileResponse>(jsonString);
            return filetransferResponse;
        }

        /// <summary>
        /// Parse main firmware payload
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        private MainFirmwareProtocolData ParseMainFirmwarePayload(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                _logger.LogWarning("Payload is empty");
                return null;
            }
            var payLoad = JsonConvert.DeserializeObject<MainFirmwareProtocolData>(jsonString);
            return payLoad;
        }

        /// <summary>
        /// Parse main firmware payload
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        private DeviceUploadFileProtocolData ParseFileUploadPayload(string jsonString)
        {
            if (string.IsNullOrEmpty(jsonString))
            {
                _logger.LogWarning("Payload is empty");
                return null;
            }
            var payLoad = JsonConvert.DeserializeObject<DeviceUploadFileProtocolData>(jsonString);
            return payLoad;
        }
    }
}