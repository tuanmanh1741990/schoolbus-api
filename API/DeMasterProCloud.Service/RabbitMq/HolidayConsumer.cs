﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class HolidayConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        private INotificationService _notificationService;

        public HolidayConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<HolidayConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _notificationService = new NotificationService(_queueService);
                var response = ParseData(data);
                var messageId = response.MsgId;

                unitOfWork.Save();

                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private HolidayProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var holidayResponse = JsonConvert.DeserializeObject<HolidayProtocolData>(jsonString);
            return holidayResponse;
        }
    }
}