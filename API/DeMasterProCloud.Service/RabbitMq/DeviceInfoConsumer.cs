﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class DeviceInfoConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;
        private ISendMessageService _sendMessageService;
        public DeviceInfoConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceInfoConsumer>();
        }

        /// <summary>
        /// Receive event log data from icu and save into database
        /// </summary>
        /// <param name="data"></param>
        public bool DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                _sendMessageService = new SendMessageService(_queueService);

                var result = ParseData(data);
                //Check icu address is register in the cloud or not
                var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(result.Data.DeviceAddress);

                if (icuDevice == null)
                {
                    _logger.LogWarning($"Can not find the icu address {result.Data.DeviceAddress}!");
                    return false;
                }
                // TODO update device attribute

                //icuDevice.MacAddress = result.Data.Mac;

                icuDevice.FirmwareVersion = result.Data.Version;
                icuDevice.VersionReader0 = result.Data.Reader0Version;
                icuDevice.VersionReader1 = result.Data.Reader1Version;
                icuDevice.NfcModuleVersion = result.Data.NfcModuleVersion;
                icuDevice.ExtraVersion = result.Data.ExtraVersion;

                icuDevice.IpAddress = result.Data.IpAddress;
                icuDevice.RegisterIdNumber = result.Data.UserCount;
                icuDevice.LastCommunicationTime = DateTime.Now;
                icuDevice.ServerIp = result.Data.ServerIp;
                icuDevice.ServerPort = string.IsNullOrEmpty(result.Data.ServerPort) ? 0 : Convert.ToInt32(result.Data.ServerPort);
                icuDevice.NumberOfNotTransmittingEvent = result.Data.EventNotTransCount;
                icuDevice.EventCount = result.Data.EventCount;
                icuDevice.ConnectionStatus = (short)IcuStatus.Connected;

                // [Edward]
                icuDevice.MacAddress = icuDevice.MacAddress ?? result.Data.MacAddress;

                unitOfWork.IcuDeviceRepository.Update(icuDevice);
                unitOfWork.Save();
                _sendMessageService.SendConnectionStatus(icuDevice);
                unitOfWork.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                
                var deviceAddress = Helpers.GetDeviceAddress(routingKey);

                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                _logger.LogWarning($"There is error when processing data from ({deviceAddress}):{Encoding.UTF8.GetString(data)}");
                    return false;
            }
        }

        /// <summary>
        /// Parsing data from ICU
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public DeviceInfoResponse ParseData(byte[] data)
        {
            if (data.Length < 50)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }
            var jsonString = Encoding.UTF8.GetString(data);
            var deviceInfo = JsonConvert.DeserializeObject<DeviceInfoResponse>(jsonString);
            return deviceInfo;
        }
    }
}