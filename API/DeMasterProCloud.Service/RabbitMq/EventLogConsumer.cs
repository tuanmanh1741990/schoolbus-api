﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Information;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class EventLogConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        private readonly ILogger _logger;

        public EventLogConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<EventLogConsumer>();
        }

        /// <summary>
        /// Parsing data from ICU
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ReceiveEventLogProtocolData ParseData(byte[] data)
        {
            if (data.Length < 100)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var eventLogInfo = JsonConvert.DeserializeObject<ReceiveEventLogProtocolData>(jsonString);
            return eventLogInfo;
        }

        /// <summary>
        /// Receive event log data from icu and save into database
        /// </summary>
        /// <param name="data"></param>
        public async Task<List<EventLog>> DoWork(byte[] data)
        {
            var startTime = DateTime.Now;
            var unitOfWork = DbHelper.CreateUnitOfWork(_configuration, new HttpContextAccessor());
            //_settingService = new SettingService(unitOfWork, _configuration);
            var eventLogs = new List<EventLog>();
            try
            {
                var startParsingDataTime = DateTime.Now;
                var result = ParseData(data);
                var endParsingDataTime = DateTime.Now;
                if (result == null)
                {
                    return null;
                }

                //Check icu address is register in the cloud or not
                var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(result.Data.Events[0].DeviceAddress);

                if (icuDevice == null)
                {
                    _logger.LogWarning($"Can not find the icu address {result.Data.Events[0].DeviceAddress}!");
                    return null;
                }


                // update device information
                icuDevice.ConnectionStatus = (short)IcuStatus.Connected;
                icuDevice.LastCommunicationTime = DateTime.Now;
                unitOfWork.IcuDeviceRepository.Update(icuDevice);


                // add event data to DB
                var startAddDataTime = DateTime.Now;
                if (result.Data.Events.Count > 1)
                {
                    AddMultipleEventLog(unitOfWork, eventLogs, result, icuDevice);
                }
                else
                {
                    AddSingleEventLog(unitOfWork, eventLogs, result, icuDevice);
                }

                var endAddDataTime = DateTime.Now;

                if (eventLogs.Any())
                {
                    SendDataToFe(unitOfWork, eventLogs, result, icuDevice);
                }

                var endPushTime = DateTime.Now;

                var beforeSaveDb = DateTime.Now;
                await unitOfWork.SaveAsync();
                var afterSaveDb = DateTime.Now;

                SendToIcu(unitOfWork, result, icuDevice.DeviceAddress);

                unitOfWork.Dispose();

                var endSaveDataTime = DateTime.Now;
                var enableLog = Convert.ToBoolean(_configuration.GetSection("EnablePerformanceLog").Value);
                //TODO: Start writting performance log
                if (enableLog)
                {
                    try
                    {
                        var updateTime = DateTimeHelper.ConvertUpdateTime(result.Data.Events[0].UpdateTime);
                        var content = new[]
                        {
                            icuDevice.DeviceAddress,//Device address
                            result.Data.Total.ToString(),
                            updateTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),//Access time
                            startTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),//Start time Dowork
                            endSaveDataTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),//End time save data
                            endPushTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),//End push time
                            startTime.Subtract(updateTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//From Dowork to start parsing data
                            endParsingDataTime.Subtract(startParsingDataTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//Parsing data time
                            startAddDataTime.Subtract(endParsingDataTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//From end parsing data to start add data
                            endAddDataTime.Subtract(startAddDataTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//From start data time to after save db
                            afterSaveDb.Subtract(beforeSaveDb).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//From before save db to after save db
                            endSaveDataTime.Subtract(afterSaveDb).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//From after save db to end save db
                            endSaveDataTime.Subtract(startTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),//From Dowork to end save data
                            endPushTime.Subtract(startTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture) //From Dowork to end push time
                        };

                        //_queueService.PublishToSpecificQueue(Constants.RabbitMq.EventLogTaskQueue, string.Join(@",", content));
                        PrometheusMetricData metricData = new PrometheusMetricData
                        {
                            Topic = "/topic/event",
                            DeviceAddress = icuDevice.DeviceAddress,
                            TotalMilliseconds = endPushTime.Subtract(startTime).TotalMilliseconds
                        };
                        var json = Newtonsoft.Json.JsonConvert.SerializeObject(metricData);
                        _queueService.PublishToSpecificQueue(Constants.RabbitMq.EventLogTaskQueue, json);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                    }
                }
                var sendMessageService = new SendMessageService(_queueService);
                sendMessageService.SendConnectionStatus(icuDevice);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                AppLog.EventFailedSave(data);
            }

            return eventLogs;
        }

        /// <summary>
        /// Add single event log
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="eventLogs"></param>
        /// <param name="result"></param>
        /// <param name="icuDevice"></param>
        internal void AddSingleEventLog(IUnitOfWork unitOfWork, List<EventLog> eventLogs, ReceiveEventLogProtocolData result, IcuDevice icuDevice)
        {
            try
            {

                var eventLogDetail = result.Data.Events.First();
                //Get user from cloud database by card id
                User user = null;
                Visit visit = null;
                Card card = null;
                if (!string.IsNullOrEmpty(eventLogDetail.CardId))
                {
                    card = unitOfWork.CardRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                    if (card != null)
                    {
                        user = unitOfWork.UserRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                        visit = unitOfWork.VisitRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                    }
                }
                var buildingId = icuDevice.BuildingId;
                var buildingTimeZone = unitOfWork.BuildingRepository.GetById(buildingId.Value).TimeZone;
                var operationType = icuDevice.OperationType;
                var eventTime = DateTimeHelper.ConvertDateTimeByTimeZoneToSystemTimeZone(eventLogDetail.AccessTime, buildingTimeZone);
                var eventLog = new EventLog();
                if (visit != null)
                {
                    eventLog = new EventLog
                    {
                        IcuId = icuDevice.Id,
                        DoorName = icuDevice.Name,
                        CompanyId = icuDevice.CompanyId,
                        CardId = eventLogDetail.CardId,
                        IssueCount = eventLogDetail.IssueCount ?? 0,
                        //UserId = visit?.Id,
                        UserName = visit?.VisitorName,
                        DeptId = 0,
                        Antipass = eventLogDetail.InOut,
                        EventType = eventLogDetail.EventType,
                        EventTime = eventTime,
                        IsVisit = true,
                        VisitId = visit?.Id,
                        CardType = (Int16)card?.CardType,
                        CardStatus = card?.CardStatus ?? 0
                    };
                }
                else
                {
                    eventLog = new EventLog
                    {
                        IcuId = icuDevice.Id,
                        DoorName = icuDevice.Name,
                        CompanyId = icuDevice.CompanyId,
                        CardId = eventLogDetail.CardId,
                        IssueCount = eventLogDetail.IssueCount ?? 0,
                        UserId = user?.Id,
                        UserName = user?.FirstName + user?.LastName,
                        DeptId = user?.DepartmentId,
                        Antipass = eventLogDetail.InOut,
                        EventType = eventLogDetail.EventType,
                        EventTime = eventTime,
                        IsVisit = false,
                        CardType = user == null || (user?.PassType == (short)(PassType.PassCard)) ? (short)(PassType.PassCard) : (short)(PassType.VisitCard),
                        CardStatus = card?.CardStatus ?? 0
                    };
                }
                //Cuong: We should think again about checking dupplicate
                //if (unitOfWork.EventLogRepository.IsDuplicated(eventLog))
                //{
                //    //eventLogs.Add(eventLog);
                //    return;
                //}

                if (operationType == (short)OperationType.Restaurant)
                {
                    var mealSettingService = new MealSettingService(unitOfWork, new HttpContextAccessor());
                    var mealEventLog = new MealEventLogService(unitOfWork);
                    var exceptionalMeal = new ExceptionalMealService(unitOfWork);

                    if (eventLogDetail.EventType != Convert.ToInt32(EventType.UnRegisteredCard))
                    {
                        var mealSetting = mealSettingService.CheckAcessTime(eventLogDetail.DeviceAddress, eventLogDetail.AccessTime);
                        if (mealSetting.Code != (short)EventType.OnlyAccessibleAtMealtime && mealSetting.Code != (short)EventType.NoDoorActiveTime)
                        {

                            decimal price = 0;
                            decimal priceUserDiscount = 0;
                            var lstExceptionalMeal = exceptionalMeal.ListExceptionalMealByDateTime(Convert.ToInt32(icuDevice.CompanyId),icuDevice.Id, eventLogDetail.AccessTime);
                            var userDiscount = new UserDiscountService(unitOfWork).GetUserDiscount(user.Id);
                            if (userDiscount != null)
                            {
                                priceUserDiscount = userDiscount.amount;
                            }

                            if (lstExceptionalMeal == null)
                            {
                                price = mealSetting.price - priceUserDiscount;
                            }
                            else
                            {
                                price = lstExceptionalMeal.price - priceUserDiscount;
                            }
                            eventLog.EventType = mealSetting.Code;
                            unitOfWork.EventLogRepository.AddEventLog(eventLog);
                            //add data to meal eventLog
                            mealEventLog.AddMealEventLog(mealSetting.mealTypeId, price, eventLogDetail.CardId, mealSetting.cornerId);
                        }
                        if (mealSetting.Code == (short)EventType.OnlyAccessibleAtMealtime)
                        {
                            eventLog.EventType = (short)EventType.OnlyAccessibleAtMealtime;
                            unitOfWork.EventLogRepository.AddEventLog(eventLog);
                        }
                        else if (mealSetting.Code == (short)EventType.NoDoorActiveTime)
                        {
                            eventLog.EventType = (short)EventType.NoDoorActiveTime;
                            unitOfWork.EventLogRepository.AddEventLog(eventLog);
                        }
                    }
                }
                else
                {
                    unitOfWork.EventLogRepository.Add(eventLog);
                }

                var attendanceService = new AttendanceService(new HttpContextAccessor(), _configuration);
                _ = attendanceService.AddClockInOutAsync(user, eventLogDetail.InOut, eventTime, buildingId.Value);
                if (eventLog != null)
                    eventLogs.Add(eventLog);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                _logger.LogWarning(result.ToString());
            }
        }

        /// <summary>
        /// Add multiple event log
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="eventLogs"></param>
        /// <param name="result"></param>
        /// <param name="icuDevice"></param>
        internal void AddMultipleEventLog(IUnitOfWork unitOfWork, List<EventLog> eventLogs, ReceiveEventLogProtocolData result, IcuDevice icuDevice)
        {
            //Get list user by card ids
            //var cardIds = result.Data.Events.Where(x => !string.IsNullOrEmpty(x.CardId)).Select(x => x.CardId).ToList();
            //var usersByteCards = unitOfWork.UserRepository.GetUsersByCardId(icuDevice.CompanyId, cardIds);

            foreach (var eventLogDetail in result.Data.Events)
            {
                try
                {
                    //Get user from cloud database by card id
                    User user = null;
                    Visit visit = null;
                    Card card = null;

                    if (!string.IsNullOrEmpty(eventLogDetail.CardId))
                    {
                        if (card != null)
                        {
                            user = unitOfWork.UserRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                            visit = unitOfWork.VisitRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                        }
                    }

                    var buildingId = icuDevice.BuildingId;
                    var buildingTimeZone = unitOfWork.BuildingRepository.GetById(buildingId.Value).TimeZone;

                    var eventTime = DateTimeHelper.ConvertDateTimeByTimeZoneToSystemTimeZone(eventLogDetail.AccessTime, buildingTimeZone);
                    var eventLog = new EventLog();

                    if (visit != null)
                    {
                        eventLog.IcuId = icuDevice.Id;
                        eventLog.DoorName = icuDevice.Name;
                        eventLog.CompanyId = icuDevice.CompanyId;
                        eventLog.CardId = eventLogDetail.CardId;
                        eventLog.IssueCount = eventLogDetail.IssueCount ?? 0;
                        //UserId = visit?.Id,
                        eventLog.UserName = visit?.VisitorName;
                        eventLog.DeptId = 0;
                        eventLog.Antipass = eventLogDetail.InOut;
                        eventLog.EventType = eventLogDetail.EventType;
                        eventLog.EventTime = eventTime;
                        eventLog.IsVisit = true;
                        eventLog.VisitId = visit?.Id;
                        eventLog.CardType = (Int16)card?.CardType;
                        eventLog.CardStatus = card?.CardStatus ?? 0;
                    }
                    else
                    {
                        eventLog.IcuId = icuDevice.Id;
                        eventLog.DoorName = icuDevice.Name;
                        eventLog.CompanyId = icuDevice.CompanyId;
                        eventLog.CardId = eventLogDetail.CardId;
                        eventLog.IssueCount = eventLogDetail.IssueCount ?? 0;
                        eventLog.UserId = user?.Id;
                        eventLog.UserName = user?.FirstName + user?.LastName;
                        eventLog.DeptId = user?.DepartmentId;
                        eventLog.Antipass = eventLogDetail.InOut;
                        eventLog.EventType = eventLogDetail.EventType;
                        eventLog.EventTime = eventTime;
                        eventLog.IsVisit = user == null || (user?.PassType == (short)(PassType.PassCard)) ? false : true;
                        //eventLog.VisitId = user?.PassType == (short)(PassType.PassCard) ? null : card?.VisitId;
                        eventLog.CardType = user == null || (user?.PassType == (short)(PassType.PassCard)) ? (short)(PassType.PassCard) : (short)(PassType.VisitCard);
                        eventLog.CardStatus = card?.CardStatus ?? 0;
                    }
                    //Cuong: We should think again about checking dupplicate
                    //if (unitOfWork.EventLogRepository.IsDuplicated(eventLog))
                    //    continue;

                    //check icu is restaurant ???
                    var mealSettingService = new MealSettingService(unitOfWork, new HttpContextAccessor());
                    var mealEventLog = new MealEventLogService(unitOfWork);
                    var exceptionalMeal = new ExceptionalMealService(unitOfWork);
                    if (icuDevice.OperationType == (short)OperationType.Restaurant)
                    {
                        if (eventLogDetail.EventType != Convert.ToInt32(EventType.UnRegisteredCard))
                        {
                            var mealSetting = mealSettingService.CheckAcessTime(eventLogDetail.DeviceAddress, eventLogDetail.AccessTime);
                            if (mealSetting != null)
                            {
                                var lstExceptionalMeal = exceptionalMeal.ListExceptionalMealByDateTime(Convert.ToInt32(icuDevice.CompanyId), icuDevice.Id, eventLogDetail.AccessTime);
                                decimal price = 0;
                                decimal priceUserDiscount = 0;
                                var userDiscount = new UserDiscountService(unitOfWork).GetUserDiscount(user.Id);
                                if (userDiscount != null)
                                {
                                    priceUserDiscount = userDiscount.amount;
                                }
                                if (lstExceptionalMeal == null)
                                {
                                    price = mealSetting.price - priceUserDiscount;
                                }
                                else
                                {
                                    price = lstExceptionalMeal.price - priceUserDiscount;
                                }
                                eventLog.EventType = mealSetting.Code;
                                unitOfWork.EventLogRepository.Add(eventLog);
                                //add data to meal eventLog
                                mealEventLog.AddMealEventLog(mealSetting.mealTypeId, price, eventLogDetail.CardId, mealSetting.cornerId);
                            }
                            else
                            {
                                eventLog.EventType = (short)EventType.OnlyAccessibleAtMealtime;
                                unitOfWork.EventLogRepository.AddEventLog(eventLog);
                            }
                        }

                    }
                    else
                    {
                        unitOfWork.EventLogRepository.Add(eventLog);
                    }

                    // Create/Update timeattendance records
                    var attendanceService = new AttendanceService(new HttpContextAccessor(), _configuration);
                    _ = attendanceService.AddClockInOutAsync(user, eventLogDetail.InOut, eventTime, buildingId.Value);
                    if (eventLog != null)
                        eventLogs.Add(eventLog);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                    _logger.LogWarning(result.ToString());
                }
            }
        }

        /// <summary>
        /// Send user log data to front end
        /// </summary>
        /// <param name="eventLogs"></param>
        /// <param name="icuDevice"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="resultData"></param>
        internal void SendDataToFe(IUnitOfWork unitOfWork, List<EventLog> eventLogs, ReceiveEventLogProtocolData resultData, IcuDevice icuDevice)
        {
            //var device = unitOfWork.IcuDeviceRepository.GetById(icuDevice.Id);
            if (eventLogs != null)
            {
                eventLogs = eventLogs == null ? new List<EventLog>() : eventLogs;
                for (int i = 0; i < eventLogs.Count(); i++)
                {
                    // Because of ICU-300N
                    int minus = eventLogs.Count() == 2 ? 2 : 1;
                    var eventLog = eventLogs.ElementAt(eventLogs.Count() - minus + i);

                    var eventLogDetail = Mapper.Map<SendEventLogDetailData>(eventLog);
                    eventLogDetail.Id = Guid.NewGuid();
                    eventLogDetail.Device = icuDevice.DeviceAddress;
                    eventLogDetail.UnixTime = (DateTimeOffset.Parse(eventLogDetail.AccessTime).UtcDateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                    eventLogDetail.DoorName = icuDevice.Name;
                    eventLogDetail.EventDetailCode = resultData.Data.Events[i].EventType;
                    eventLogDetail.IssueCount = resultData.Data.Events[i].IssueCount != null ? resultData.Data.Events[i].IssueCount.ToString() : string.Empty;

                    eventLogDetail.InOut = resultData.Data.Events[i].InOut == "In" || resultData.Data.Events[i].InOut == "Out" ? resultData.Data.Events[i].InOut : "";

                    User user = null;
                    Visit visit = null;
                    Card card = null;

                    if (!string.IsNullOrEmpty(eventLogDetail.CardId))
                    {
                        card = unitOfWork.CardRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);

                        if (card != null)
                        {
                            if (card.UserId != null)
                            {
                                user = unitOfWork.UserRepository.GetByCardIdIncludeDepartment(icuDevice.CompanyId, eventLogDetail.CardId);
                                //user = unitOfWork.UserRepository.GetByUserId(icuDevice.CompanyId ?? 0, card.UserId.Value);
                            }
                            else if (card.VisitId != null)
                            {
                                //visit = unitOfWork.AppDbContext.Visit.FirstOrDefault(v => v.Id == card.VisitId && !v.IsDeleted);
                                visit = unitOfWork.VisitRepository.GetByVisitId(icuDevice.CompanyId ?? 0, card.VisitId.Value);
                            }

                        }
                    }

                    if (visit != null)
                    {
                        eventLogDetail.Department = visit?.VisitorDepartment;
                        eventLogDetail.UserName = visit?.VisitorName;
                        eventLogDetail.ExpireDate = visit?.StartDate.ToSettingDateString();
                        eventLogDetail.VisitId = visit?.Id;
                        eventLogDetail.CardStatus = card?.CardStatus;
                        eventLogDetail.BuildingId = icuDevice.BuildingId.Value;
                        eventLogDetail.UserType = (short)UserType.Visit;
                    }
                    else if (user != null)
                    {
                        eventLogDetail.Department = user?.Department.DepartName;
                        eventLogDetail.UserName = user?.FirstName + " " + user?.LastName;
                        eventLogDetail.ExpireDate = user?.ExpiredDate.ToSettingDateString();
                        eventLogDetail.UserId = user?.Id;
                        eventLogDetail.CardStatus = card?.CardStatus;
                        eventLogDetail.BuildingId = icuDevice.BuildingId.Value;
                        eventLogDetail.UserType = (short)UserType.Normal;
                    }
                    var mealSettingService = new MealSettingService(unitOfWork, new HttpContextAccessor());
                    var mealSetting = mealSettingService.CheckAcessTime(eventLogDetail.Device, resultData.Data.Events[i].AccessTime);

                    if (icuDevice.OperationType == (short)OperationType.Restaurant)
                    {
                        if (mealSetting == null)
                        {
                            eventLogDetail.EventDetailCode = (short)EventType.OnlyAccessibleAtMealtime;
                        }
                        else
                        {
                            if (eventLogDetail.EventDetailCode != Convert.ToInt32(EventType.UnRegisteredCard))
                            {
                                eventLogDetail.EventDetailCode = mealSetting.Code;
                            }
                        }

                    }
                    var events = new List<SendEventLogDetailData> { eventLogDetail };


                    var result = new SendEventLogListModelData
                    {
                        MsgId = Guid.NewGuid().ToString(),
                        Type = Constants.Protocol.EventLogWebApp,
                        Data = new SendEventLogHeaderData
                        {
                            Total = 1,
                            Events = events
                        }
                    };

                    var jsonString = result.ToString();

                    var companyCode = icuDevice.CompanyId != null ? icuDevice.Company.Code : "";

                    //Send data to the frontend
                    var message = Encoding.UTF8.GetBytes(jsonString);

                    var routingKey = $"{Constants.RabbitMq.EventLogJsonTopic}.{companyCode}";


                    //check icu is restaurant ???

                    if (icuDevice.OperationType == (short)OperationType.Restaurant)
                    {
                        if (resultData.Data.Events[i].EventType != Convert.ToInt32(EventType.UnRegisteredCard))
                        {

                            _queueService.Publish(routingKey, message, 1);

                        }
                        else
                        {
                            _queueService.Publish(routingKey, message, 1);
                        }



                    }
                    else
                    {
                        _queueService.Publish(routingKey, message, 1);
                    }


                    // Because of ICU-300N
                    if (eventLogs.Count() != 2) break;
                }
            }


        }

        /// <summary>
        /// Send to Icu
        /// </summary>
        /// <param name="result"></param>
        internal void SendToIcu(IUnitOfWork unitOfWork, ReceiveEventLogProtocolData result, string deviceAddress)
        {
            var responseIcu = new ResponseEventLogProtocolData
            {
                MsgId = result.MsgId,
                Type = Constants.Protocol.EventLogResponse,
                Data = new ResponseEventLogHeader
                {
                    Total = result.Data.Total
                }
            };
            _queueService.Publish(Constants.RabbitMq.EventLogResponseTopic + "." + deviceAddress, responseIcu.ToString());

        }

    }
}
