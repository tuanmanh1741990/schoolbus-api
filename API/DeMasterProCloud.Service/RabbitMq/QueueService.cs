﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DeMasterProCloud.Service.RabbitMq
{
    public interface IQueueService
    {
        void Publish(string routingKey, string message, byte deliveryMode = 2);

        void Publish(string routingKey, byte[] message, byte deliveryMode = 2);

        void PublishToSpecificQueue(string queue, string message);

        void SendGroupMessage(string deviceAddress, string msgId, string message, string topic, string groupMsgId, int groupIndex, int groupLength, string processId = "", string actionType = "");

        void ClearSpecificQueue(string queue);

        void Dispose();
    }

    /// <summary>
    /// Queue service
    /// </summary>
    public class QueueService : IQueueService, IDisposable
    {
        private readonly IConnection _connection;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        /// <summary>
        /// Queue service constructor with configuration file.
        /// </summary>
        /// <param name="configuration"></param>
        public QueueService(IConfiguration configuration)
        {
            var factory = QueueHelper.GetConnectionFactory(configuration);
            _connection = factory.CreateConnection("queue_service_connetion");
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<QueueService>();
        }

        /// <summary>
        /// Queue service constructor with connection.
        /// </summary>
        /// <param name="connection"></param>
        public QueueService(IConnection connection)
        {
            _connection = connection;
        }

        /// <summary>
        /// Publish message to list icu
        /// </summary>
        /// <param name="routingKey"></param>
        /// <param name="message"></param>
        /// <param name="deliveryMode"></param>
        public void Publish(string routingKey, string message, byte deliveryMode)
        {
            var body = Encoding.UTF8.GetBytes(message);
            Publish(routingKey, body, deliveryMode);
        }

        /// <summary>
        /// Publish message to list device and delay for response
        /// </summary>
        /// <param name="routingKey"></param>
        /// <param name="message"></param>
        /// <param name="deliveryMode"></param>
        public void Publish(string routingKey, string message, byte deliveryMode, ISendMessageService _sendMessageService, string msgId)
        {
            var body = Encoding.UTF8.GetBytes(message);
            Publish(routingKey, body, deliveryMode);
            //Task.Delay(Constants.Settings.ResponseTimeout)
            //              .ContinueWith(t => _sendMessageService.SendResponseNotification(msgId));
        }

        /// <summary>
        /// Publish message to list icu
        /// </summary>
        /// <param name="routingKey"></param>
        /// <param name="message"></param>
        /// <param name="deliveryMode"></param>
        public void Publish(string routingKey, byte[] message, byte deliveryMode)
        {
            // For all the message send to device except file transfer, we cache the message to process with the reponse later
            // We add all messsage send to device to cache, ignore message send to webapp
            // TODO: We may need to check for the case message is timeout
            List<string> ignoreTypes = new List<string>(new string[] { Constants.Protocol.Notification,
                                                                       Constants.Protocol.EventCountWebApp,
                                                                       Constants.Protocol.EventLogWebApp,
                                                                       Constants.Protocol.LongProcessProgress,
                                                                       Constants.Protocol.ConnectionStatus,
                                                                       Constants.Protocol.LoadAllUserWebApp,
                                                                       Constants.Protocol.LoadHolidayWebApp,
                                                                       Constants.Protocol.LoadTimezoneWebApp,
                                                                       Constants.Protocol.LoadDeviceSettingWebApp
                                                                       });
            var jsonString = Encoding.UTF8.GetString(message);
            if (!routingKey.Contains(Constants.RabbitMq.FileTranferTopic))
            {
                var msg = JsonConvert.DeserializeObject<BasicProtocolData>(jsonString);
                if (!ignoreTypes.Contains(msg.Type))
                {
                    ApplicationVariables.SendingMessages.TryAdd(msg.MsgId, jsonString);
                    //TODO: need to find appropriate timeout
                    Task.Delay(Constants.RabbitMq.MessageResponseTimeout * 3)
                               .ContinueWith(t => TimeoutHandler(msg.MsgId, routingKey));
                }
            }
            else
            {
                var msg = JsonConvert.DeserializeObject<DeviceUploadFileProtocolData>(jsonString);
                if (!ignoreTypes.Contains(msg.Type))
                {
                    ApplicationVariables.SendingMessages.TryAdd(msg.MsgId, jsonString);
                    Task.Delay(Constants.RabbitMq.MessageResponseTimeout * 3)
                               .ContinueWith(t => TimeoutHandler(msg.MsgId, routingKey));
                }
            }

            using (var channel = _connection.CreateModel())
            {
                //channel.BasicAcks += Channel_BasicAcks;
                //channel.ConfirmSelect();

                var properties = channel.CreateBasicProperties();
                properties.DeliveryMode = deliveryMode;

                channel.BasicPublish(exchange: Constants.RabbitMq.ExchangeName,
                    routingKey: routingKey,
                    basicProperties: properties,
                    body: message);

                //channel.WaitForConfirmsOrDie();
            }
        }

        /// <summary>
        /// Publish message to a specific queue
        /// </summary>
        /// <param name="queue"></param>
        /// <param name="message"></param>
        public void PublishToSpecificQueue(string queue, string message)
        {
            using (var channel = _connection.CreateModel())
            {
                var byteMessage = Encoding.UTF8.GetBytes(message);
                channel.QueueDeclare(queue: queue,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                    routingKey: queue,
                    basicProperties: properties,
                    body: byteMessage);
            }
        }

        public void SendGroupMessage(string deviceAddress, string msgId, string message, string topic, string groupMsgId, int groupIndex, int groupLength, string processId = "", string actionType = "")
        {
            //Send all message to icu
            var jsonSerializer = new Common.Infrastructure.JsonSerializer();

            // Publish all the message to multiple message task queue
            // Create a long message to inform webapp for progress
            var new_msg = new MultipleServiceProtocolData
            {
                MsgId = msgId,
                PayLoad = message,
                Topic = topic,
                Retry = 0,
                GroupMsgId = groupMsgId,
                //new_msg.TotalMessages = messages.Count;
                TotalMessages = groupLength,
                MessageIndex = groupIndex,
                ProcessId = processId,
                PublishedTime = (System.DateTime)DateTime.Now,
                ActionType = actionType
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(new_msg);
            this.PublishToSpecificQueue($"{Constants.RabbitMq.MultipleMessagesTaskQueue}_{deviceAddress}", json);
        }

        /// <summary>
        /// Clear a specific queue
        /// </summary>
        /// <param name="queue"></param>
        public void ClearSpecificQueue(string queue)
        {
            using (var channel = _connection.CreateModel())
            {
                channel.QueuePurge(queue);
                channel.Close();
            }
        }

        /// <summary>
        /// Timeout handler. This fucntion handle situation when API doesn't
        /// receive response on time. It will:
        /// - Mark device offline
        /// - Send notification if needed
        /// - clear message cache in queue
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="routingKey"></param>
        public void TimeoutHandler(string msgId, string routingKey)
        {
            if (ApplicationVariables.SendingMessages.ContainsKey(msgId))
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var sendMessageService = new SendMessageService(this);
                var deviceAddress = Helpers.GetDeviceAddress(routingKey);
                var device = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

                if (device != null)
                {
                    if (device.ConnectionStatus == (short)IcuStatus.Connected)
                    {
                        device.ConnectionStatus = (short)IcuStatus.Disconneted;
                        unitOfWork.Save();
                        sendMessageService.SendConnectionStatus(device);
                        _logger.LogWarning("Response of message {0} from device {1} did not arrive on time." +
                            " We set device connection status to disconected", msgId, deviceAddress);
                    }
                    else
                    {
                        _logger.LogWarning("Response of message {0} from device {1} did not arrive on time.", msgId, deviceAddress);
                    }
                }
                ApplicationVariables.SendingMessages.TryRemove(msgId, out _);
                //TODO: sending notification if needed

                unitOfWork.Dispose();
            }
        }

        /// <summary>
        /// Dispose connection in the case un-use
        /// </summary>
        public void Dispose()
        {
            if (_connection != null)
            {
                _connection.Close();
                _connection.Dispose();
            }
        }

        ~QueueService()
        {
            Dispose();
        }
    }
}