﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class CheckHolidayConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;

        public CheckHolidayConsumer(IConfiguration configuration,IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<CheckHolidayConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var response = ParseData(data);

                var deviceAddress = Helpers.GetDeviceAddress(routingKey);
                SendToFe(unitOfWork,response, deviceAddress);
                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private LoadHolidayProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var holidayResponse = JsonConvert.DeserializeObject<LoadHolidayProtocolData>(jsonString);
            return holidayResponse;
        }

        /// <summary>
        /// Send to fe
        /// </summary>
        /// <param name="response"></param>
        /// <param name="unitOfWork"></param>
        public void SendToFe(IUnitOfWork unitOfWork, LoadHolidayProtocolData response, string deviceAddress)
        {
            var holidayReceiveDatas = response.Data.Holidays;
            var holidaySends = new List<SendHolidayDetail>();
            if (holidayReceiveDatas.Any())
            {
                IcuDevice device = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);
                if(device.CompanyId != null)
                {
                    var allHoliday = unitOfWork.HolidayRepository.GetHolidayByCompany((int)device.CompanyId);
                    foreach (var holidayResponse in holidayReceiveDatas)
                    {
                        var holidaySend = Mapper.Map<SendHolidayDetail>(holidayResponse);
                        var holiday = allHoliday.FirstOrDefault(x => Convert.ToInt32(x.Recursive) == holidayResponse.Recurring
                                                            && x.Type == holidayResponse.HolidayType
                                                            && x.StartDate <= DateTimeHelper.ConvertToDateTime(holidayResponse.Date)
                                                            && x.EndDate >= DateTimeHelper.ConvertToDateTime(holidayResponse.Date));
                        holidaySend.Id = holiday?.Id;
                        holidaySend.Name = holiday?.Name;
                        holidaySend.StartDate = holiday?.StartDate.ToString(Constants.DateTimeFormat.MmDdYyyy);
                        holidaySend.EndDate = holiday?.EndDate.ToString(Constants.DateTimeFormat.MmDdYyyy);
                        if (holidaySends.All(x => x.Id != holidaySend.Id))
                        {
                            holidaySends.Add(holidaySend);
                        }
                    }
                }
                
            }

            var protocol = new SendHolidayProtocol
            {
                MsgId = response.MsgId,
                Type = Constants.Protocol.LoadHolidayWebApp,
                Data = new SendHolidayHeader
                {
                    Total = holidaySends.Count,
                    Holidays = holidaySends
                }
            };
            var routingKey = Constants.RabbitMq.HolidayWebAppTopic;
            _queueService.Publish(routingKey, protocol.ToString());
        }
    }
}
