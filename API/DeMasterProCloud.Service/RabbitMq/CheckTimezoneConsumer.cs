﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class CheckTimezoneConsumer
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;

        public CheckTimezoneConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<CheckTimezoneConsumer>();
        }

        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var response = ParseData(data);

                var deviceAddress = Helpers.GetDeviceAddress(routingKey);

                if (!ApplicationVariables.SendingMessages.ContainsKey(response.MsgId))
                {
                    _logger.LogWarning($"({deviceAddress})Can not find original message of message {response.ToString()}! We ignore the message!");

                    return;
                }
                var originalMessagePayload = ApplicationVariables.SendingMessages.GetValueOrDefault(response.MsgId);

                var originalMessage = JsonConvert.DeserializeObject<BasicProtocolData>(originalMessagePayload);
                
                var icuDevice =
                    unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);
                if (icuDevice == null)
                {
                    _logger.LogWarning($"Can not find the icu address {deviceAddress}!");
                    return;
                }
                var payload = Encoding.UTF8.GetString(data);
                SendToFe(unitOfWork, response, icuDevice, originalMessagePayload, payload);
                unitOfWork.Save();
                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private UpdateTimezoneProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            return ParseObject(jsonString);
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        private UpdateTimezoneProtocolData ParseObject(string payload)
        {
            var timezoneResponse = JsonConvert.DeserializeObject<UpdateTimezoneProtocolData>(payload);
            return timezoneResponse;
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        private LoadTimezoneProtocolData ParseObjectLoadTimezone(string payload)
        {
            var timezoneResponse = JsonConvert.DeserializeObject<LoadTimezoneProtocolData>(payload);
            return timezoneResponse;
        }

        /// <summary>
        /// Send to fe
        /// </summary>
        /// <param name="response"></param>
        /// <param name="unitOfWork"></param>
        /// <param name="device"></param>
        /// <param name="originalMessagePayload"></param>
        /// <param name="payload"></param>
        public void SendToFe(IUnitOfWork unitOfWork, UpdateTimezoneProtocolData response, IcuDevice device,
            string originalMessagePayload, string payload)
        {
            if (response.Data.FrameIndex == 1)
            {
                var newtimezoneProtocol = MakeProtocolData(unitOfWork, null, response, device.CompanyId);
                if (response.Data.FrameIndex == response.Data.TotalIndex)
                {
                    var routingKey = Constants.RabbitMq.LoadTimezoneWebAppTopic;
                    _queueService.Publish(routingKey, newtimezoneProtocol.ToString());
                }
            }
            else if (response.Data.FrameIndex > 1 && response.Data.FrameIndex <= response.Data.TotalIndex)
            {
                var responseOld = ParseObjectLoadTimezone(originalMessagePayload);
                var newtimezoneProtocol = MakeProtocolData(unitOfWork, responseOld, response, device.CompanyId);

                if (response.Data.FrameIndex == response.Data.TotalIndex)
                {
                    var routingKey = Constants.RabbitMq.LoadTimezoneWebAppTopic;
                    _queueService.Publish(routingKey, newtimezoneProtocol.ToString());
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="oldResponse"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private LoadTimezoneProtocolData MakeProtocolData(IUnitOfWork unitOfWork,
            LoadTimezoneProtocolData oldResponse, UpdateTimezoneProtocolData response, int? companyId)
        {
            var sendTimezones = new List<LoadTimezoneProtocolDetailData>();
            if (oldResponse != null)
            {
                var oldTimezones = oldResponse.Data.Timezones;
                if (oldTimezones.Any())
                {
                    sendTimezones.AddRange(oldTimezones);
                }
            }

            var timezones = response.Data.Timezone;
            if (timezones.Any())
            {
                foreach (var timezone in timezones)
                {
                    var sendTimezoneDetail = Mapper.Map<LoadTimezoneProtocolDetailData>(timezone);
                    var tz = unitOfWork.TimezoneRepository.GetByPositionAndCompany(timezone.TimezonePosition, companyId);
                    sendTimezoneDetail.Name = tz?.Name;
                    sendTimezoneDetail.Position = timezone.TimezonePosition;
                    //sendTimezoneDetail.Type = GetActiveOrPassageTz(device, timezone.TimezonePosition);
                    sendTimezones.Add(sendTimezoneDetail);
                }
            }

            var timezoneProtocol = new LoadTimezoneProtocolData
            {
                MsgId = response.MsgId,
                Type = Constants.Protocol.LoadTimezoneWebApp,
                Data = new LoadTimezoneProtocolHeaderData
                {
                    Total = sendTimezones.Count,
                    Timezones = sendTimezones
                }
            };

            return timezoneProtocol;
        }
    }
}