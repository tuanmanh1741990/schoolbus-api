﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class EventRecoveryConsumer
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly IQueueService _queueService;
        public EventRecoveryConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _queueService = queueService;
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<EventRecoveryConsumer>();
        }

        /// <summary>
        /// Receive response from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public async void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var eventLogs = new List<EventLog>();
                try
                {
                    var result = ParseData(data);

                    if (result == null)
                    {
                        return;
                    }

                    var messageId = result.MsgId;
                              unitOfWork.Save();

                    var deviceAddress = Helpers.GetDeviceAddress(routingKey);

                    //Check icu address is register in the cloud or not
                    var icuDevice = unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

                    if (icuDevice == null)
                    {
                        _logger.LogWarning($"Can not find the device address {deviceAddress}!");
                        return;
                    }
                    icuDevice.ConnectionStatus = (short)IcuStatus.Connected;
                    icuDevice.LastCommunicationTime = DateTime.Now;
                    unitOfWork.IcuDeviceRepository.Update(icuDevice);
                    var frameIndex = result.Data.FrameIndex;
                    var totalIndex = result.Data.TotalIndex;
                    var progressId = result.Data.ProcessId;

                    if (result.Data.Total > 1)
                    {
                        AddMultipleEventLog(unitOfWork, eventLogs, result.Data.Events, icuDevice);
                    }
                    else
                    {
                        AddSingleEventLog(unitOfWork, eventLogs, result.Data.Events, icuDevice);
                    }


                    SendProgressToFe(progressId, frameIndex, totalIndex);

                    await unitOfWork.SaveAsync();
                    unitOfWork.Dispose();

                    var sendMessageService = new SendMessageService(_queueService);
                    sendMessageService.SendConnectionStatus(icuDevice);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                    AppLog.EventFailedSave(data);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private ReceiveEventRecoveryProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var receiveEventRecoveryProtocolData = JsonConvert.DeserializeObject<ReceiveEventRecoveryProtocolData>(jsonString);
            return receiveEventRecoveryProtocolData;
        }


        /// <summary>
        /// Send message to frontend
        /// </summary>
        /// <param name="progressId"></param>
        /// <param name="frameIndex"></param>
        /// <param name="totalIndex"></param>
        private void SendProgressToFe(string progressId, int frameIndex, int totalIndex)
        {
            if (totalIndex == 0)
            {
                _logger.LogWarning($"Non sense data. Data length: {totalIndex}");
                return;
            }

            var processUnit = 100f / totalIndex;
            var progress = frameIndex + 1 == totalIndex
                    ? 100
                    : ((frameIndex + 1) * processUnit);
            var message = ProcessProgressProtocolData.MakeLongProcessProgressMessage(progressId, (decimal)progress, Constants.LongProgressName.Recorvering);
            //push msg
            var routingKey = Constants.RabbitMq.LongProcessProgressTopic;

            _queueService.Publish(routingKey, message);
        }

        /// <summary>
        /// Add single event log
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="eventLogs"></param>
        /// <param name="events"></param>
        /// <param name="icuDevice"></param>
        internal void AddSingleEventLog(IUnitOfWork unitOfWork, List<EventLog> eventLogs, List<ReceiveEventRecoveryEventDetailData> events, IcuDevice icuDevice)
        {
            try
            {
                var eventLogDetail = events.First();
                //Get user from cloud database by card id
                User user = null;
                Card card = null;
                if (!string.IsNullOrEmpty(eventLogDetail.CardId))
                {
                    user = unitOfWork.UserRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                    card = unitOfWork.CardRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                }

                var eventLog = new EventLog
                {
                    IcuId = icuDevice.Id,
                    DoorName = icuDevice.Name,
                    CompanyId = icuDevice.CompanyId,
                    CardId = eventLogDetail.CardId,
                    IssueCount = eventLogDetail.IssueCount ?? 0,
                    UserId = user?.Id,
                    DeptId = user?.DepartmentId,
                    Antipass = eventLogDetail.InOut,
                    EventType = eventLogDetail.EventType,
                    EventTime = DateTimeHelper.ConvertAccessTime(eventLogDetail.AccessTime),
                    IsVisit = user == null || (user?.PassType == (short)(PassType.PassCard)) ? false : true,
                    VisitId = user?.PassType == (short)(PassType.PassCard) ? null : card?.VisitId,
                    CardType = user == null || (user?.PassType == (short)(PassType.PassCard)) ? (short)(PassType.PassCard) : (short)(PassType.VisitCard),
                };
                if (unitOfWork.EventLogRepository.IsDuplicated(eventLog))
                {
                    eventLogs.Add(eventLog);
                    return;
                }

                unitOfWork.EventLogRepository.Add(eventLog);
                eventLogs.Add(eventLog);
                var eventLogJsonString = JsonConvert.SerializeObject(eventLog);
                AppLog.EventSave(eventLogJsonString);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
            }
        }

        /// <summary>
        /// Add multiple event log
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="eventLogs"></param>
        /// <param name="events"></param>
        /// <param name="icuDevice"></param>
        internal void AddMultipleEventLog(IUnitOfWork unitOfWork, List<EventLog> eventLogs, List<ReceiveEventRecoveryEventDetailData> events, IcuDevice icuDevice)
        {
            //Get list user by card ids
            var cardIds = events.Where(x => !string.IsNullOrEmpty(x.CardId)).Select(x => x.CardId).ToList();
            var usersByteCards = unitOfWork.UserRepository.GetUsersByCardId(icuDevice.CompanyId, cardIds);
            foreach (var eventLogDetail in events)
            {
                try
                {
                    //Get user from cloud database by card id
                    User user = null;
                    Card card = null;
                    if (!string.IsNullOrEmpty(eventLogDetail.CardId))
                    {
                        user = unitOfWork.UserRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                        card = unitOfWork.CardRepository.GetByCardId(icuDevice.CompanyId, eventLogDetail.CardId);
                    }

                    var buildingId = unitOfWork.IcuDeviceRepository.GetByIcuId(icuDevice.Id).BuildingId;
                    var buildingTimeZone = unitOfWork.BuildingRepository.GetById(buildingId.Value).TimeZone;
                    var eventTime = DateTimeHelper.ConvertDateTimeByTimeZoneToSystemTimeZone(eventLogDetail.AccessTime, buildingTimeZone);

                    var eventLog = new EventLog
                    {
                        IcuId = icuDevice.Id,
                        DoorName = icuDevice.Name,
                        CompanyId = icuDevice.CompanyId,
                        CardId = eventLogDetail.CardId,
                        IssueCount = eventLogDetail.IssueCount ?? 0,
                        UserId = user?.Id,
                        DeptId = user?.DepartmentId,
                        Antipass = eventLogDetail.InOut,
                        EventType = eventLogDetail.EventType,
                        //EventTime = DateTimeHelper.ConvertAccessTime(eventLogDetail.AccessTime),
                        EventTime = eventTime,
                        IsVisit = user == null || (user?.PassType == (short)(PassType.PassCard)) ? false : true,
                        VisitId = user?.PassType == (short)(PassType.PassCard) ? null : card?.VisitId,
                        CardType = user == null || (user?.PassType == (short)(PassType.PassCard)) ? (short)(PassType.PassCard) : (short)(PassType.VisitCard),
                    };
                    if (unitOfWork.EventLogRepository.IsDuplicated(eventLog))
                    {
                        eventLogs.Add(eventLog);
                        return;
                    }
                    unitOfWork.EventLogRepository.Add(eventLog);
                    eventLogs.Add(eventLog);
                    var eventLogJsonString = JsonConvert.SerializeObject(eventLog);
                    AppLog.EventSave(eventLogJsonString);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                    //AppLog.EventFailedSave(data);
                }
            }
        }
    }
}
