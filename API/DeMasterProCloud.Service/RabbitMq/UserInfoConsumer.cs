﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Csv;
using DeMasterProCloud.Service.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DeMasterProCloud.Service.RabbitMq
{
    public class UserInfoConsumer
    {
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;
        private readonly IConfiguration _configuration;

        //private string _loadUserFromIcu =
        //    "{0}" + $"_{DateTime.Now.ToString(Constants.DateTimeFormat.YyyyMMddhhmm, CultureInfo.InvariantCulture)}.csv";
        private string _loadUserFromIcu;
        private readonly string[] _headers = {
            "CardId",
            "Name",
            "DepartmentCode",
            "EmployeeNumber",
            "ExpireDate",
            "IssueCount",
            "IsMasterCard",
            "EffectiveDate",
            "CardStatus",
            "Password",
            "Timezone"
        };

        public UserInfoConsumer(IConfiguration configuration, IQueueService queueService)
        {
            _configuration = configuration;
            _queueService = queueService;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<UserInfoConsumer>();
        }
        /// <summary>
        /// Receive reponse from icu and update to database
        /// </summary>
        /// <param name="data"></param>
        public void DoWork(byte[] data, string routingKey)
        {
            try
            {
                var unitOfWork = DbHelper.CreateUnitOfWork(_configuration);
                var response = ParseData(data);
                var messageId = response.MsgId;
                if (!ApplicationVariables.SendingMessages.ContainsKey(response.MsgId))
                {
                    _logger.LogWarning($"Can not find original message of message {response.ToString()}! We ignore the message!");

                    return;
                }
                var originalMessagePayload = ApplicationVariables.SendingMessages.GetValueOrDefault(response.MsgId);

                var originalMessage = JsonConvert.DeserializeObject<BasicProtocolData>(originalMessagePayload);

                unitOfWork.Save();
                var deviceAdress = Helpers.GetDeviceAddress(routingKey);
                _loadUserFromIcu = $"{Constants.ExportType.LoadAllUser}_{deviceAdress}_{messageId}.csv";
                var icuDevice =
                    unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAdress);
                if (icuDevice == null)
                {
                    _logger.LogWarning($"Can not find the icu address {deviceAdress}!");
                    return;
                }

                SendToFe(unitOfWork, response, icuDevice);
                if (originalMessage.Type == Constants.Protocol.LoadAllUser)
                {
                    var user = response.Data.Users;
                    CreateOrUpdateFile(unitOfWork, user, icuDevice);
                }
                unitOfWork.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Create or update file
        /// </summary>
        /// <param name="users"></param>
        /// <param name="unitOfWork"></param>
        private void CreateOrUpdateFile(IUnitOfWork unitOfWork, List<UserDetail> users, IcuDevice icuDevice)
        {
            var contents = new List<string[]>();
            foreach (var user in users)
            {
                var content = new[]
                {
                    user.CardId,
                    user.UserName,
                    user.DepartmentName,
                    user.EmployeeNumber,
                    DateTimeHelper.ChangeFormatDatetime(user.ExpireDate),
                    user.IssueCount.ToString(),
                    user.AdminFlag.ToString(),
                    DateTimeHelper.ChangeFormatDatetime(user.EffectiveDate),
                    ((CardStatus)user.CardStatus).GetDescription(),
                    Helpers.MaskKeyPadPw(user.Password),
                    GetTimezoneName(unitOfWork,user, icuDevice.CompanyId)
                };
                contents.Add(content);
            }

            if (File.Exists(_loadUserFromIcu))
            {
                var lines = File.ReadAllLines(_loadUserFromIcu);
                for (var i = 1; i < lines.Length; i++)
                {
                    var line = lines[i].Split(",");
                    contents.Add(line);
                }

                File.Delete(_loadUserFromIcu);
                CsvHelper.Write(_headers, contents, _loadUserFromIcu);
            }
            else
            {
                CsvHelper.Write(_headers, contents, _loadUserFromIcu);
            }
        }


        /// <summary>
        /// 
        /// Parse data from response data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private LoadUserResponseProtocolData ParseData(byte[] data)
        {
            if (data.Length < 20)
            {
                _logger.LogWarning($"Non sense data. Data length: {data.Length}");
                return null;
            }

            var jsonString = Encoding.UTF8.GetString(data);
            var userResponse = JsonConvert.DeserializeObject<LoadUserResponseProtocolData>(jsonString);
            return userResponse;
        }

        /// <summary>
        /// Send message to get user
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="data"></param>
        /// <param name="device"></param>
        private void SendToFe(IUnitOfWork unitOfWork, LoadUserResponseProtocolData data, IcuDevice device)
        {
            var users = data.Data.Users;
            var sendUserDetails = new List<SendUserDetail>();
            if (users != null && users.Any())
            {
                foreach (var userDetail in users)
                {
                    var random = new Random();
                    var sendUser = Mapper.Map<SendUserDetail>(userDetail);
                    sendUser.Id = random.Next(10000, 99999);
                    sendUser.Timezone = GetTimezoneName(unitOfWork, userDetail, device.CompanyId);
                    sendUserDetails.Add(sendUser);
                }
            }
            var sendUserProtocol = new SendUserProtocolData
            {
                MsgId = data.MsgId,
                Type = Constants.Protocol.LoadAllUserWebApp,
                Data = new SendUserResponseHeader
                {
                    FrameIndex = data.Data.FrameIndex,
                    TotalIndex = data.Data.TotalIndex,
                    IcuId = device.Id,
                    DeviceType = ((DeviceType)device.DeviceType).GetDescription(),
                    Total = data.Data.Total,
                    Users = sendUserDetails
                }
            };
            var message = sendUserProtocol.ToString();
            var routingKey = Constants.RabbitMq.LoadUserWebAppTopic;
            _queueService.Publish(routingKey, message);
        }

        /// <summary>
        /// Get timezone name
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        private string GetTimezoneName(IUnitOfWork unitOfWork, UserDetail user, int? companyId)
        {
            var timezoneName = string.Empty;
            var timezone = unitOfWork.TimezoneRepository.GetByPositionAndCompany(user.Timezone, companyId);
            if (timezone != null)
            {
                timezoneName = timezone.Name;
            }

            return timezoneName;
        }
    }
}
