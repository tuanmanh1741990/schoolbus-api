using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Attendance;
using DeMasterProCloud.DataModel.WorkingModel;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Threading.Tasks;
using DeMasterProCloud.Service.Infrastructure;

namespace DeMasterProCloud.Service
{
    public interface IAttendanceService : IPaginationService<AttendanceListModel>
    {
        new IQueryable<AttendanceListModel> GetPaginated(string name, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        IQueryable<AttendanceListModel> GetPaginatedAttendanceReport(string name, string departmentIds, string attendanceType, string timezone,
            DateTime start, DateTime end, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        bool AddClockInOut(User user, string inOut, DateTime epoch, int buildingId = 0);
        Task<bool> AddClockInOutAsync(User user, string inOut, DateTime epoch, int buildingId = 0);

        void Recheck(string rangeTime, int companyId, string fromAccessTime, string toAccessTime);
        void Update(int attendanceId, AttendanceModel model);

        Attendance GetByIdAndCompanyId(int companyId, int attendanceId);

        IQueryable<AttendanceListModel> ExportAttendanceReport(string name, string departmentIds,
            string attendanceType,
            DateTime start, DateTime end, out int totalRecords, out int recordsFiltered);

        StringBuilder ExportAttendanceFile(List<AttendanceListModel> attendanceListModels, string timeZone);

        IQueryable<AttendanceListModel> GetPaginatedAttendanceRecordEachUser(int userId,
            DateTime start, DateTime end, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

    }


    public class AttendanceService : IAttendanceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Ctor for account service
        /// </summary>
        /// <param name="unitOfWork"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="companyService"></param>
        /// <param name="mailService"></param>
        /// <param name="logger"></param>
        public AttendanceService(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {

            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
            _unitOfWork = DbHelper.CreateUnitOfWork(_configuration);

        }

        public IQueryable<AttendanceListModel> GetPaginated(string name, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.Attendance
                .Where(c => c.CompanyId == _httpContext.User.GetCompanyId())
                .Select(m => new AttendanceListModel()
                {
                    Id = m.Id,
                    Date = m.Date,
                    Start = m.Start,
                    End = m.End,
                    WorkingTime = m.WorkingTime,
                    Type = m.Type,
                    UserId = m.UserId,
                    ClockIn = m.ClockIn,
                    ClockOut = m.ClockOut,
                    UserName = m.User.FirstName
                });

            totalRecords = data.Count();


            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Company.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Company[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        public IQueryable<AttendanceListModel> GetPaginatedAttendanceReport(string name, string departmentIds, string attendanceType, string timezone, DateTime start, DateTime end, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.Attendance
                .Where(c => c.CompanyId == _httpContext.User.GetCompanyId())
                .Select(m => new AttendanceListModel()
                {
                    Id = m.Id,
                    Date = m.Date,
                    Start = (m.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.Equals(0.0) &&
                            (m.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.Equals(0.0) ? 0 : (m.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    End = (m.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.Equals(0.0) &&
                          (m.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.Equals(0.0) ? 0 : (m.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    WorkingTime = m.WorkingTime,
                    Type = m.Type,
                    UserId = m.UserId,
                    ClockIn = (m.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    ClockOut = (m.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    UserName = m.User.FirstName,
                    User = m.User
                });

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(x => x.UserName.ToLower().Contains(name.ToLower()));
            }

            if (!string.IsNullOrEmpty(attendanceType))
            {
                String[] strlist = attendanceType.Split(",");
                if (strlist.Any())
                {
                    data = data.Where(x => strlist.Contains(x.Type.ToString()));
                }
            }

            if (!string.IsNullOrEmpty(departmentIds))
            {
                String[] strlist = departmentIds.Split(",");
                if (strlist.Any())
                {
                    data = data.Where(x => strlist.Contains(x.User.DepartmentId.ToString()));
                }
            }

            data = start <= end
                ? data.Where(o => o.Date >= start && o.Date <= end)
                : data.Where(o => o.Date <= start && o.Date.AddDays(1) <= end);

            recordsFiltered = data.Count();
            data = data.OrderBy(m => m.Date);
            data = data.OrderBy(m => m.UserId);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        


        public void Recheck(string rangeTime, int companyId, string fromAccessTime, string toAccessTime)
        {
            DateTime firsTime;
            DateTime lastTime;
            //Get List of company that enable Time Attendance
            if (fromAccessTime == null || toAccessTime == null)
            {
                firsTime = DateTime.Now.AddMonths(-1).Date;
                lastTime = DateTime.Now.Date;
            }
            else
            {
                firsTime = DateTimeHelper.ConverStringToDateTime(fromAccessTime);
                lastTime = DateTimeHelper.ConverStringToDateTime(toAccessTime);
            }

            if (companyId == 0)
            {
                var companies = _unitOfWork.CompanyRepository.GetCompaniesByPlugin(Constants.PlugIn.TimeAttendance);
                foreach (var company in companies)
                {
                    CallBackReCheck(company, rangeTime, firsTime, lastTime);
                }
            }
            else
            {
                var companies = _unitOfWork.CompanyRepository.GetCompanyByPlugin(Constants.PlugIn.TimeAttendance, companyId);
                if (companies.Count() > 0)
                {
                    foreach (var company in companies)
                    {
                        CallBackReCheck(company, rangeTime, firsTime, lastTime);
                    }
                }

            }

        }


        public void CallBackReCheck(Company company, string rangeTime, DateTime firsTime, DateTime lastTime)
        {
            //Get normal access event of given company
            var eventLogs = rangeTime == Constants.Attendance.RangTimeAllDay
                ? _unitOfWork.EventLogRepository.GetFirstInOutLogNormalAccess(company.Id, firsTime, lastTime)
                : _unitOfWork.EventLogRepository.GetFirstInOutNormalAccessToday(company.Id);
            //var eventLogs = _unitOfWork.EventLogRepository.GetFirstInOutNormalAccessToday(company.Id);
            foreach (var eventLog in eventLogs)
            {
                var user = _unitOfWork.UserRepository.GetById(eventLog.UserId.Value);
                var icuDevice = _unitOfWork.IcuDeviceRepository.GetByIdAndCompanyIdWithBuilding(eventLog.CompanyId.Value, eventLog.IcuId);
                if (icuDevice != null)
                {
                    Building building = _unitOfWork.BuildingRepository.GetById(icuDevice.BuildingId.Value);
                    //var timeZone = !string.IsNullOrEmpty(building.TimeZone) ? building.TimeZone : TimeZoneInfo.Local.Id;
                    var timeZone = !string.IsNullOrEmpty(building.TimeZone) ? TimeZoneInfo.Local.Id : building.TimeZone;
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                    var offset = cstZone.BaseUtcOffset;

                    _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
                    {
                        using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                        {
                            try
                            {
                                var attendance = _unitOfWork.AttendanceRepository.GetAttendanceAlreadyCreated(
                                        eventLog.UserId.Value,
                                        eventLog.CompanyId.Value, eventLog.EventTime.Date);
                                if (attendance == null)
                                {
                                    if (user.WorkingTypeId != null)
                                    {
                                        WorkingType workingType = _unitOfWork.WorkingRepository.GetById(user.WorkingTypeId.Value);
                                        var listWorking = JsonConvert.DeserializeObject<List<WorkingTime>>(workingType.WorkingDay);
                                        foreach (var timeWork in listWorking)
                                        {
                                            if (timeWork.Name == eventLog.EventTime.DayOfWeek.ToString())
                                            {
                                                String[] strStart = timeWork.Start.Split(':', ' ');
                                                String[] strEnd = timeWork.End.Split(':', ' ');
                                                var hourStart = Convert.ToInt32(strStart.FirstOrDefault());
                                                var hourEnd = Convert.ToInt32(strEnd.FirstOrDefault());
                                                var minutesStart = Convert.ToInt32(strStart[strStart.Length - 1]);
                                                var minutesEnd = Convert.ToInt32(strEnd[strEnd.Length - 1]);

                                                var start = eventLog.EventTime.Date.AddHours(hourStart).AddMinutes(minutesStart) - offset;
                                                var end = eventLog.EventTime.Date.AddHours(hourEnd).AddMinutes(minutesEnd) - offset;

                                                var epochStart = (start - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                                var epochEnd = (end - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                                var zero = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc) - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;

                                                var jsonString = JsonConvert.SerializeObject(timeWork);
                                                var newAttendance = new Attendance
                                                {
                                                    UserId = user.Id,
                                                    Date = eventLog.EventTime.Date,
                                                    CompanyId = user.CompanyId,
                                                    StartD = start,
                                                    ClockInD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                                                    ClockOutD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                                                    EndD = end,
                                                    WorkingTime = jsonString
                                                };
                                                if (eventLog.Antipass == Constants.Attendance.In)
                                                {
                                                    var epochClockIn = (eventLog.EventTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                                    if (newAttendance.ClockInD == new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                                        newAttendance.ClockInD = eventLog.EventTime;
                                                    newAttendance.Type = Helpers.CheckStatusAttendance(epochStart, epochEnd, epochClockIn, zero);
                                                }
                                                else
                                                {
                                                    var epochClockOut = (eventLog.EventTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                                    newAttendance.ClockOutD = eventLog.EventTime;
                                                    newAttendance.Type = Helpers.CheckStatusAttendance(epochStart, epochEnd, zero, epochClockOut);
                                                }

                                                _unitOfWork.AttendanceRepository.Add(newAttendance);
                                                _unitOfWork.Save();
                                                transaction.Commit();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    var startD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                    var endD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                    if (user.WorkingTypeId != null)
                                    {
                                        WorkingType workingType =
                                            _unitOfWork.WorkingRepository.GetById(user.WorkingTypeId.Value);
                                        var listWorking =
                                            JsonConvert.DeserializeObject<List<WorkingTime>>(workingType.WorkingDay);
                                        foreach (var timeWork in listWorking)
                                        {
                                            if (timeWork.Name == eventLog.EventTime.DayOfWeek.ToString())
                                            {
                                                String[] strStart = timeWork.Start.Split(':', ' ');
                                                String[] strEnd = timeWork.End.Split(':', ' ');
                                                var hourStart = Convert.ToInt32(strStart.FirstOrDefault());
                                                var hourEnd = Convert.ToInt32(strEnd.FirstOrDefault());
                                                var minutesStart = Convert.ToInt32(strStart[strStart.Length - 1]);
                                                var minutesEnd = Convert.ToInt32(strEnd[strEnd.Length - 1]);

                                                startD = eventLog.EventTime.Date.AddHours(hourStart)
                                                             .AddMinutes(minutesStart) - offset;
                                                endD = eventLog.EventTime.Date.AddHours(hourEnd)
                                                           .AddMinutes(minutesEnd) - offset;
                                            }
                                        }
                                    }

                                    attendance.StartD = startD;
                                    attendance.EndD = endD;
                                    if (attendance.EditedBy == null)
                                    {
                                        if (eventLog.Antipass == Constants.Attendance.In)
                                        {
                                            if (attendance.ClockInD == new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                            {
                                                attendance.ClockInD = eventLog.EventTime;
                                            }
                                            else
                                            {
                                                if (attendance.ClockInD > eventLog.EventTime)
                                                {
                                                    attendance.ClockInD = eventLog.EventTime;
                                                }
                                            }
                                        }

                                        if (eventLog.Antipass == Constants.Attendance.Out)
                                        {
                                            if (attendance.ClockOutD == new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                            {
                                                attendance.ClockOutD = eventLog.EventTime;
                                            }
                                            else
                                            {
                                                if (attendance.ClockOutD < eventLog.EventTime)
                                                {
                                                    attendance.ClockOutD = eventLog.EventTime;
                                                }
                                            }
                                        }

                                        if (attendance.ClockInD != new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                                            && attendance.ClockOutD != new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                        {
                                            attendance.Type = Helpers.CheckStatusAttendance(
                                                (attendance.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                                .TotalSeconds,
                                                (attendance.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                                .TotalSeconds,
                                                (attendance.ClockInD -
                                                 new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                                (attendance.ClockOutD -
                                                 new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
                                        }
                                        else
                                        {
                                            attendance.Type = (short)AttendanceType.AbNormal;
                                        }
                                    }
                                    _unitOfWork.AttendanceRepository.Update(attendance);
                                    _unitOfWork.Save();
                                    transaction.Commit();
                                }
                            }
                            catch (Exception)
                            {
                                transaction.Rollback();
                                throw;
                            }
                        }
                    });
                }
            }
        }

        public async Task<bool> AddClockInOutAsync(User user, string inOut, DateTime epoch, int buildingId = 0)
        {
            await Task.Delay(1);
            var result = AddClockInOut(user, inOut, epoch, buildingId);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="inOut"></param>
        /// <param name="epoch"></param>
        /// <param name="buildingId"></param>
        public bool AddClockInOut(User user, string inOut, DateTime epoch, int buildingId = 0)
        {

            if (user != null && buildingId != 0)
            {
                // Check timezone building
                Building building = _unitOfWork.BuildingRepository.GetById(buildingId);
                var timeZone = !string.IsNullOrEmpty(building.TimeZone) ? building.TimeZone : TimeZoneInfo.Local.Id;
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                var offSet = cstZone.BaseUtcOffset;


                var attendance = _unitOfWork.AttendanceRepository.GetAttendanceAlreadyCreated(user.Id, user.CompanyId, epoch.Date);
                if (attendance != null)
                {
                    var startD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    var EndD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    if (user.WorkingTypeId != null)
                    {
                        WorkingType workingTypes = _unitOfWork.WorkingRepository.GetById(user.WorkingTypeId.Value);
                        var listWorkings = JsonConvert.DeserializeObject<List<WorkingTime>>(workingTypes.WorkingDay);
                        foreach (var timeWork in listWorkings)
                        {
                            if (timeWork.Name == epoch.DayOfWeek.ToString())
                            {
                                String[] strStart = timeWork.Start.Split(':', ' ');
                                String[] strEnd = timeWork.End.Split(':', ' ');
                                var hourStart = Convert.ToInt32(strStart.FirstOrDefault());
                                var hourEnd = Convert.ToInt32(strEnd.FirstOrDefault());
                                var minutesStart = Convert.ToInt32(strStart[strStart.Length - 1]);
                                var minutesEnd = Convert.ToInt32(strEnd[strEnd.Length - 1]);

                                startD = epoch.Date.AddHours(hourStart).AddMinutes(minutesStart) - offSet;
                                EndD = epoch.Date.AddHours(hourEnd).AddMinutes(minutesEnd) - offSet;
                            }
                        }
                    }
                    attendance.StartD = startD;
                    attendance.EndD = EndD;
                    if (inOut == Constants.Attendance.In)
                    {
                        attendance.ClockOutD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

                        if (attendance.ClockInD == new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                        {
                            attendance.ClockInD = epoch;
                            attendance.Type = Helpers.CheckStatusAttendance(
                                (attendance.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                (attendance.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                (attendance.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                (attendance.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);

                        }
                        else
                        {
                            attendance.Type = Helpers.CheckStatusAttendance(
                                (attendance.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                (attendance.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                (attendance.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                                (attendance.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
                        }
                    }
                    else
                    {
                        attendance.ClockOutD = epoch;
                        attendance.Type = Helpers.CheckStatusAttendance(
                            (attendance.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                            (attendance.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                            (attendance.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                            (epoch - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
                    }

                    _unitOfWork.AttendanceRepository.Update(attendance);
                    _unitOfWork.Save();
                }
                else
                {
                    if (user.WorkingTypeId != null)
                    {
                        WorkingType workingType = _unitOfWork.WorkingRepository.GetById(user.WorkingTypeId.Value);
                        var listWorking = JsonConvert.DeserializeObject<List<WorkingTime>>(workingType.WorkingDay);
                        foreach (var timeWork in listWorking)
                        {
                            if (timeWork.Name == epoch.DayOfWeek.ToString())
                            {
                                String[] strStart = timeWork.Start.Split(':', ' ');
                                String[] strEnd = timeWork.End.Split(':', ' ');
                                var hourStart = Convert.ToInt32(strStart.FirstOrDefault());
                                var hourEnd = Convert.ToInt32(strEnd.FirstOrDefault());
                                var minutesStart = Convert.ToInt32(strStart[strStart.Length - 1]);
                                var minutesEnd = Convert.ToInt32(strEnd[strEnd.Length - 1]);

                                var start = epoch.Date.AddHours(hourStart).AddMinutes(minutesStart) - offSet;
                                var end = epoch.Date.AddHours(hourEnd).AddMinutes(minutesEnd) - offSet;

                                var epochStart = (start - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                var epochEnd = (end - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                var zero = (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc) - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;

                                var jsonString = JsonConvert.SerializeObject(timeWork);
                                var newAttendance = new Attendance
                                {
                                    UserId = user.Id,
                                    Date = epoch.Date,
                                    CompanyId = user.CompanyId,
                                    StartD = start,
                                    ClockInD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                                    ClockOutD = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc),
                                    EndD = end,
                                    WorkingTime = jsonString
                                };
                                if (inOut == Constants.Attendance.In)
                                {
                                    var epochClockIn = (epoch - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                    if (newAttendance.ClockInD == new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                                        newAttendance.ClockInD = epoch;
                                    newAttendance.Type = Helpers.CheckStatusAttendance(epochStart, epochEnd, epochClockIn, zero);
                                }
                                else
                                {
                                    var epochClockOut = (epoch - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                                    newAttendance.ClockOutD = epoch;
                                    newAttendance.Type = Helpers.CheckStatusAttendance(epochStart, epochEnd, zero, epochClockOut);
                                }

                                _unitOfWork.AttendanceRepository.Add(newAttendance);
                                _unitOfWork.Save();
                            }

                        }
                    }
                }
            }
            return true;
        }

        public Attendance GetByIdAndCompanyId(int companyId, int attendanceId)
        {
            return _unitOfWork.AttendanceRepository.GetAttendanceByIdAndCompanyId(companyId, attendanceId);
        }

        public void Update(int attendanceId, AttendanceModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var attendance = GetByIdAndCompanyId(_httpContext.User.GetCompanyId(), attendanceId);
                        attendance.Type = model.Type;
                        attendance.ClockInD = model.ClockInD;
                        attendance.ClockOutD = model.ClockOutD;
                        attendance.EditedBy = _httpContext.User.GetAccountId();
                        Mapper.Map(model, attendance);
                        _unitOfWork.AttendanceRepository.Update(attendance);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public IQueryable<AttendanceListModel> ExportAttendanceReport(string name, string departmentIds, string attendanceType, DateTime start, DateTime end, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.Attendance
                .Where(c => c.CompanyId == _httpContext.User.GetCompanyId())
                .Select(m => new AttendanceListModel()
                {
                    Id = m.Id,
                    Date = m.Date,
                    Start = (m.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    End = (m.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    WorkingTime = m.WorkingTime,
                    Type = m.Type,
                    UserId = m.UserId,
                    ClockIn = (m.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    ClockOut = (m.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    UserName = m.User.FirstName,
                    User = m.User
                });

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(name))
            {
                data = data.Where(x => x.UserName.ToLower().Contains(name.ToLower()));
            }

            if (!string.IsNullOrEmpty(attendanceType))
            {
                String[] strlist = attendanceType.Split(",");
                if (strlist.Any())
                {
                    data = data.Where(x => strlist.Contains(x.Type.ToString()));
                }
            }

            if (!string.IsNullOrEmpty(departmentIds))
            {
                String[] strlist = departmentIds.Split(",");
                if (strlist.Any())
                {
                    data = data.Where(x => strlist.Contains(x.User.DepartmentId.ToString()));
                }
            }

            data = start <= end
                ? data.Where(o => o.Date >= start && o.Date <= end)
                : data.Where(o => o.Date <= start && o.Date.AddDays(1) <= end);

            data = data.OrderBy(m => m.Date);
            data = data.OrderBy(m => m.UserId);

            recordsFiltered = data.Count();
            return data;
        }

        public StringBuilder ExportAttendanceFile(List<AttendanceListModel> attendanceListModels, string timeZone)
        {
            StringBuilder str = new StringBuilder();
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);

            str.AppendFormat("User ID,User Name,Date,Days,Start Time,End Time,ClockIn,ClockOut,Type,Total Time,{0}", Environment.NewLine);

            foreach (var attendance in attendanceListModels)

            {

                TimeSpan t = TimeSpan.FromSeconds(attendance.ClockOut - attendance.ClockIn);
                string totalTime = t.ToString(@"hh\:mm\:ss");
                var formatClockIn = "";
                var formatClockout = "";
                if (!attendance.ClockIn.Equals(0.0))
                {
                    var clockIn = Helpers.UnixTimeToDateTime(attendance.ClockIn);
                    formatClockIn = TimeZoneInfo.ConvertTimeFromUtc(clockIn, cstZone).ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs);
                }

                if (!attendance.ClockOut.Equals(0.0))
                {
                    var clockIn = Helpers.UnixTimeToDateTime(attendance.ClockOut);
                    formatClockout = TimeZoneInfo.ConvertTimeFromUtc(clockIn, cstZone).ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs);
                }

                if (attendance.ClockOut > attendance.ClockIn)
                {
                    if (attendance.ClockIn.Equals(0.0))
                    {
                        totalTime = "Cannot Calculated";
                    }

                }
                else
                {
                    if (attendance.ClockOut == attendance.ClockIn)
                    {
                        totalTime = (attendance.ClockOut - attendance.ClockIn).ToString();
                    }
                    else
                    {
                        totalTime = "Cannot Calculated";
                    }
                }

                str.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                    attendance.UserId,
                    attendance.UserName,
                    attendance.Date.ToString("yyyy/MM/dd"),
                    attendance.Date.DayOfWeek,
                    Helpers.UnixTimeToDateTime(attendance.Start).ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),
                    Helpers.UnixTimeToDateTime(attendance.End).ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSs),
                    formatClockIn,
                    formatClockout,
                    Enum.GetName(typeof(AttendanceType), attendance.Type),
                    totalTime + Environment.NewLine);

            }

            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(str.ToString());
            writer.Flush();
            stream.Position = 0;
            return str;
        }

        public IQueryable<AttendanceListModel> GetPaginatedAttendanceRecordEachUser(int userId, DateTime start, DateTime end, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.Attendance
                .Where(c => c.CompanyId == _httpContext.User.GetCompanyId() && c.UserId == userId)
                .Select(m => new AttendanceListModel()
                {
                    Id = m.Id,
                    Date = m.Date,
                    Start = (m.StartD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    End = (m.EndD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    WorkingTime = m.WorkingTime,
                    Type = m.Type,
                    UserId = m.UserId,
                    ClockIn = (m.ClockInD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    ClockOut = (m.ClockOutD - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds,
                    UserName = m.User.FirstName,
                    User = m.User
                });

            totalRecords = data.Count();

            data = start <= end
                ? data.Where(o => o.Date >= start && o.Date <= end)
                : data.Where(o => o.Date <= start && o.Date.AddDays(1) <= end);

            recordsFiltered = data.Count();
            data = data.OrderBy(m => m.Date);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }
    }
}