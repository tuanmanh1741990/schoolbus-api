﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MoreLinq.Extensions;
using System;
using System.Collections.Generic;
using DeMasterProCloud.DataModel.MealType;
using System.Linq;
using System.Reflection;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// MealType service interface
    /// </summary>
    public interface IMealTypeService
    {
        List<MealTypeModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        MealType GetMealTypeByIdAndCompanyId(int mealTypeId, int companyId);

        int Add(MealTypeModel model);
        void Update(MealTypeModel model);
        void Delete(MealType mealType);

        List<EnumModel> GetAvailableMealTypeCodeList();
        bool CheckCodeIsValid(int mealTypeCode);
        MealType GetMealTypeByCompanyIdAndCode(int mealTypeId, int companyId, int Code);
    }

    public class MealTypeService : IMealTypeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;

        public MealTypeService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor,
            ILogger<DepartmentService> logger)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
        }

        /// <summary>
        /// Add mealType
        /// </summary>
        /// <param name="model"> MealType Model </param>
        public int Add(MealTypeModel model)
        {
            int mealTypeId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        var mealType = Mapper.Map<MealType>(model);

                        mealType.CompanyId = companyId;

                        _unitOfWork.AppDbContext.MealType.Add(mealType);

                        _unitOfWork.Save();
                        transaction.Commit();
                        mealTypeId = mealType.Id;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        mealTypeId = 0;
                        throw;
                    }
                }
            });
            return mealTypeId;
        }

        /// <summary>
        /// Update mealTypes
        /// </summary>
        /// <param name="model"> list of CategoryModel </param>
        public void Update(MealTypeModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        MealType mealType = GetMealTypeByIdAndCompanyId(model.Id, companyId);
                        Mapper.Map(model, mealType);

                        mealType.CompanyId = companyId;

                        _unitOfWork.AppDbContext.MealType.Update(mealType);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a mealType
        /// </summary>
        /// <param name="category"> category to be deleted </param>
        public void Delete(MealType mealType)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _unitOfWork.AppDbContext.MealType.Remove(mealType);

                        //Save to database
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }


        public MealType GetMealTypeByIdAndCompanyId(int mealTypeId, int companyId)
        {
            var mealType = _unitOfWork.AppDbContext.MealType.Where(m => m.Id == mealTypeId && m.CompanyId == companyId).FirstOrDefault();

            return mealType;
        }
        public MealType GetMealTypeByCompanyIdAndCode(int mealTypeId, int companyId, int Code)
        {
            var mealType = _unitOfWork.AppDbContext.MealType.Where(m => m.Id != mealTypeId && m.CompanyId == companyId && m.Code == Code).FirstOrDefault();

            return mealType;
        }

        /// <summary>
        /// Get mealType data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<MealTypeModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            var result = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return result.ToList();
        }

        /// <summary>
        /// Filter and order mealType data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<MealTypeModel> FilterDataWithOrder(string filter, int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var mealTypeData = _unitOfWork.AppDbContext.MealType
                .Where(m => m.CompanyId == companyId).OrderBy(m => m.Id).AsQueryable();

            totalRecords = mealTypeData.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                mealTypeData = mealTypeData.Where(x =>
                    x.Name.ToLower().Contains(filter));
            }

            recordsFiltered = mealTypeData.Count();

            var result = mealTypeData.AsEnumerable()
                .Select(m =>
                {
                    MealTypeModel mealTypeModel = new MealTypeModel
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Code = m.Code,
                        Description = m.Description
                    };
                    return mealTypeModel;
                }).ToList();

            return result;
        }


        /// <summary>
        /// Get list of available code to set MealType's code
        /// </summary>
        /// <returns></returns>
        public List<EnumModel> GetAvailableMealTypeCodeList()
        {
            //List<MealTypeCodeModel> codeList = new List<MealTypeCodeModel>();

            //foreach (var test in Enum.GetValues(typeof(AvailableMealTypeCode)))
            //{
            //    MealTypeCodeModel mealTypeCode = new MealTypeCodeModel
            //    {
            //        EventNumber = (int)test,
            //        EventName = test.GetDescription()
            //    };

            //    codeList.Add(mealTypeCode);
            //}

            var codeList = EnumHelper.ToEnumList<AvailableMealTypeCode>();

            return codeList;
        }

        /// <summary>
        /// Check if MealType code is valid or not.
        /// </summary>
        /// <param name="mealTypeCode"></param>
        /// <returns></returns>
        public bool CheckCodeIsValid(int mealTypeCode)
        {
            var mealTypeList = GetAvailableMealTypeCodeList();

            //var test = mealTypeList.Cast<int>().ToList();

            List<int> codeList = new List<int>();

            foreach (var code in Enum.GetValues(typeof(AvailableMealTypeCode)))
            {
                codeList.Add((int)code);
            }

            if (codeList.Contains(mealTypeCode))
            {
                var companyId = _httpContext.User.GetCompanyId();
                // check if the code is duplicated or not
                var mealType = _unitOfWork.MealTypeRepository.GetByCodeAndCompanyId(mealTypeCode, companyId);

                if (mealType != null && mealType.Count() > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }



    }
}