﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.UnregistedDevice;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace DeMasterProCloud.Service
{
    public interface IUnregistedDeviceService
    {
        IQueryable<UnregistedDeviceModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<UnregistedDevice> GetByCompanyId();
        List<UnregistedDevice> GetAll();
        void AddMissingDevice(List<UnregistedDevice> missingDevices);
    }

    public class UnregistedDeviceService : IUnregistedDeviceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly IDeviceService _deviceService;

        public UnregistedDeviceService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor,IDeviceService deviceService)
        {
            _unitOfWork = unitOfWork;
            _httpContext = contextAccessor.HttpContext;
            _deviceService = deviceService;
        }

        /// <summary>
        /// Get paginated unregisted device
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<UnregistedDeviceModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            //var data = _unitOfWork.UnregistedDevicesRepository.GetByCompanyId(_httpContext.User.GetCompanyId())
            //    .AsEnumerable<UnregistedDevice>().Select(Mapper.Map<UnregistedDeviceModel>).AsQueryable();

            var data = _unitOfWork.UnregistedDevicesRepository.GetAll()
                .AsEnumerable<UnregistedDevice>().Select(Mapper.Map<UnregistedDeviceModel>).AsQueryable();

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                data = data.Where(x => x.DeviceAddress.ToLower().Contains(filter) ||
                                       x.IpAddress.ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.UnregistedDevices.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.UnregistedDevices[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        /// <summary>
        /// Get all by company id
        /// </summary>
        /// <returns></returns>
        public List<UnregistedDevice> GetByCompanyId()
        {
            return _unitOfWork.UnregistedDevicesRepository.GetByCompanyId(_httpContext.User.GetCompanyId()).ToList();
        }
        
        
        /// <summary>
        /// Get all by company id
        /// </summary>
        /// <returns></returns>
        public List<UnregistedDevice> GetAll()
        {
            return _unitOfWork.UnregistedDevicesRepository.GetAll().ToList();
        }



        /// <summary>
        /// Add all missing device
        /// </summary>
        /// <param name="missingDevices"></param>
        public void AddMissingDevice(List<UnregistedDevice> missingDevices)
        {
            foreach (var md in missingDevices)
            {
                //var device = Mapper.Map<DeviceModel>(md);
                var device = new DeviceModel
                {
                    DeviceAddress = md.DeviceAddress,
                    IpAddress = md.IpAddress,
                    DoorName = "Door " + md.DeviceAddress,

                    MPRCount = Constants.Settings.DefaultMprAuthCount,
                    VerifyMode = Constants.Settings.DefaultVerifyMode,
                    LockOpenDuration = Constants.Settings.DefaultLockOpenDurationSeconds,
                    SensorType = Constants.Settings.DefaultSensorType,

                    DeviceType = md.DeviceType.Contains(DeviceType.Icu300N.GetDescription())
                    ? (((int)DeviceType.Icu300N))
                    : (((int)DeviceType.ItouchPop)),

                    MacAddress = md.MacAddress
                };

                device.UseCardReader = device.DeviceType == (((int)DeviceType.Icu300N)) ? (int?) null : 1;

                device.RoleReader1 = device.DeviceType == (((int)DeviceType.ItouchPop)) ? (short?) null : (short) RoleRules.Out;
                device.LedReader1 = device.DeviceType == (((int)DeviceType.ItouchPop)) ? (short?) null : (short) CardReaderLed.Blue;

                device.BuzzerReader0 = device.DeviceType == (((int)DeviceType.ItouchPop)) ? (short?) null : (short) BuzzerReader.ON;
                device.BuzzerReader1 = device.DeviceType == (((int)DeviceType.ItouchPop)) ? (short?) null : (short) BuzzerReader.ON;

                device.DeviceBuzzer = (short)BuzzerReader.ON;

                var oldDevice = _unitOfWork.IcuDeviceRepository.GetDeviceByAddress(device.DeviceAddress);

                if (oldDevice == null)
                {
                    _deviceService.Add(device);
                }
                else
                {
                    device.Id = oldDevice.Id;
                    _deviceService.Update(device);
                }

                //_deviceService.Add(device);


            }
            //Clear missing device
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _unitOfWork.UnregistedDevicesRepository.DeleteRange(missingDevices);
                        //_unitOfWork.SaveAsync();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }
    }
}