﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Infrastructure.Exceptions;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MoreLinq.Extensions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
//using System.Linq.Dynamic.Core;
using System.Text;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Department service interface
    /// </summary>
    public interface IDepartmentService
    {
        void InitDepartment(DepartmentModel depModel);

        bool IsDepartmentNumberExist(DepartmentModel depModel);

        int Add(DepartmentModel depModel);

        void Update(DepartmentModel model);

        void Delete(Department model);

        void DeleteRange(List<Department> departments);

        Department GetByIdAndCompany(int? id, int companyId);

        List<Department> GetByIdsAndCompany(List<int> ids, int companyId);

        IEnumerable<Node> GetDeparmentHierarchy(int? id = null);

        bool IsDepartmentNameExist(DepartmentModel model);

        bool IsUserExist(int departmentId);

        List<Department> GetDescendantsOrself(int id, int companyId);

        Department GetDefaultDepartment(int companyId);

        List<Department> GetByCompanyId(int companyId);

        bool ImportFile(string type, IFormFile file, out int total, out int fail);

        byte[] Export(string type, string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered);

        List<DepartmentListModel> FilterDataWithOrder(string filter, int sortColumn, string sortDirection,
            out int totalRecords,
            out int recordsFiltered);

        Account GetAccount(int companyId, int accountId);

        int GetUserCount(int companyId, int departmentId);
        List<DepartmentListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        Department GetById(int id);
        List<DepartmentListItemModel> ListDeparmentParent(string filter, int sortColumn, string sortDirection,
         out int totalRecords, out int recordsFiltered);
        List<DepartmentListItemModel> GetPaginatedListMenu(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        List<DepartmentListItemModel> GetListDepartment(string search, out int recordsTotal, out int recordsFiltered, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
           string sortDirection = "desc");
        bool CheckEditDepartment(DepartmentModel model);

    }

    public class DepartmentService : IDepartmentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;
        private static List<DepartmentListItemModel> lst = new List<DepartmentListItemModel>();
        /// <summary>
        /// String array display in header sheet when export file
        /// </summary>
        private readonly string[] _header =
        {
            DepartmentResource.lblDepartmentNumber,
            DepartmentResource.lblDepartmentName,
            DepartmentResource.lblDepartmentUserCount,
            DepartmentResource.lblDepartmentManager
        };

        public DepartmentService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor,
            ILogger<DepartmentService> logger)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
        }

        /// <summary>
        /// Initital data
        /// </summary>
        /// <param name="depModel"></param>
        public void InitDepartment(DepartmentModel depModel)
        {
            _unitOfWork.DepartmentRepository.InitDepartment(depModel);
        }

        /// <summary>
        /// Add department
        /// </summary>
        /// <param name="depModel"></param>
        public int Add(DepartmentModel depModel)
        {
            var departmentId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var department = Mapper.Map<Department>(depModel);
                        department.CompanyId = _httpContext.User.GetCompanyId();
                        department.ParentId = (department.ParentId == null || department.ParentId == 0) ? null : department.ParentId;
                        _unitOfWork.DepartmentRepository.Add(department);
                        _unitOfWork.Save();

                        //Save system log
                        var content = $"{DepartmentResource.lblAddNew}";
                        var contentsDetails = $"{DepartmentResource.lblDepartmentNumber} : {department.DepartNo}" +
                                            $"\n{DepartmentResource.lblDepartmentName} : {department.DepartName}";

                        _unitOfWork.SystemLogRepository.Add(department.Id, SystemLogType.Department, ActionLogType.Add,
                            content, contentsDetails, null, department.CompanyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                        departmentId = department.Id;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            return departmentId;
        }

        /// <summary>
        /// Update department
        /// </summary>
        /// <param name="model"></param>
        public void Update(DepartmentModel model)
        {
            //_unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            //{
            //using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
            //{
            try
            {
                var department =
                    _unitOfWork.DepartmentRepository.GetByIdAndCompanyId(model.Id,
                        _httpContext.User.GetCompanyId());
                var currentDepartmentName = department.DepartName;
                Mapper.Map(model, department);
                _unitOfWork.DepartmentRepository.Update(department);

                if (currentDepartmentName != model.Name)
                {
                    //Save system log
                    var content = string.Format(DepartmentResource.msgChangeDepartmentName);
                    var contentsDetails = string.Format(MessageResource.msgChangeInfo, DepartmentResource.lblDepartmentName, currentDepartmentName, model.Name);

                    _unitOfWork.SystemLogRepository.Add(department.Id, SystemLogType.Department, ActionLogType.Update,
                        content, contentsDetails, null, _httpContext.User.GetCompanyId());
                }
                _unitOfWork.Save();
                //transaction.Commit();
            }
            catch (Exception)
            {
                //transaction.Rollback();
                throw;
            }
            //}
            //});
        }

        /// <summary>
        /// Delete a department
        /// </summary>
        /// <param name="department"></param>
        public void Delete(Department department)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        department.IsDeleted = true;
                        _unitOfWork.DepartmentRepository.Update(department);
                        //Save system log
                        var content =
                            $"{ActionLogTypeResource.Delete} : {department.DepartName} ({DepartmentResource.lblDepartmentName})";

                        _unitOfWork.SystemLogRepository.Add(department.Id, SystemLogType.Department,
                            ActionLogType.Delete, content, null, null, department.CompanyId);

                        //Save to database
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a list of department
        /// </summary>
        /// <param name="departments"></param>
        public void DeleteRange(List<Department> departments)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var department in departments)
                        {
                            department.IsDeleted = true;
                            _unitOfWork.DepartmentRepository.Update(department);
                        }

                        //Save system log
                        if (departments.Count == 1)
                        {
                            var department = departments.First();
                            var content =
                                $"{ActionLogTypeResource.Delete}: {department.DepartName} ({DepartmentResource.lblDepartmentName})";

                            _unitOfWork.SystemLogRepository.Add(department.Id, SystemLogType.Department, ActionLogType.Delete,
                                content, null, null, department.CompanyId);
                        }
                        else
                        {
                            var department = departments.First();
                            var content = string.Format(ActionLogTypeResource.DeleteMultipleType,
                                DepartmentResource.lblDepartment);
                            var departmentIds = departments.Select(c => c.Id).ToList();
                            var departmentNames = departments.Select(c => c.DepartName).ToList();
                            var contentDetails = $"{DepartmentResource.lblDepartmentName} : {string.Join(", ", departmentNames)}";

                            _unitOfWork.SystemLogRepository.Add(department.Id, SystemLogType.Department, ActionLogType.DeleteMultiple,
                                content, contentDetails, departmentIds, department.CompanyId);
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<DepartmentListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            var result = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return result.ToList();
        }

        public List<DepartmentListItemModel> GetPaginatedListMenu(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = ListDeparmentParent(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered);
            if (data == null)
            {
                totalRecords = 0;
                recordsFiltered = 0;
                return new List<DepartmentListItemModel>();
            }

            var result = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return result.ToList();
        }

        /// <summary>
        /// Check if department number is exist
        /// </summary>
        /// <param name="depModel"></param>
        /// <returns></returns>
        public bool IsDepartmentNumberExist(DepartmentModel depModel)
        {
            return _unitOfWork.DepartmentRepository.IsDepartmentNumberExist(depModel);
        }

        /// <summary>
        /// Get department by id and company
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Department GetByIdAndCompany(int? id, int companyId)
        {
            return _unitOfWork.DepartmentRepository.GetByIdAndCompanyId(id, companyId);
        }

        /// <summary>
        /// Get a list department by list id and company
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Department> GetByIdsAndCompany(List<int> ids, int companyId)
        {
            return _unitOfWork.DepartmentRepository.GetByIdsAndCompanyId(ids, companyId);
        }

        /// <summary>
        /// Get list descendant or self of deparment
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Department> GetDescendantsOrself(int id, int companyId)
        {
            var departments = _unitOfWork.DepartmentRepository.GetMany(m =>
                !m.IsDeleted && m.CompanyId == companyId).ToList();
            if (departments.Any())
            {
                return GetSelfOrChild(id, departments).ToList();
            }
            return null;
        }

        /// <summary>
        /// Get default department for user
        /// </summary>
        /// <returns></returns>
        public Department GetDefaultDepartment(int companyId)
        {
            var department = _unitOfWork.DepartmentRepository.GetByCompanyId(companyId).OrderBy(c => c.Id).FirstOrDefault();

            return department;
        }

        /// <summary>
        /// Get default department for user
        /// </summary>
        /// <returns></returns>
        public List<Department> GetByCompanyId(int companyId)
        {
            var departments = _unitOfWork.DepartmentRepository.GetMany(m =>
                !m.IsDeleted && m.CompanyId == companyId).ToList();
            return departments;
        }

        /// <summary>
        /// Check user presence
        /// </summary>
        /// <returns></returns>
        public bool IsUserExist(int departmentId)
        {
            var existUsers = _unitOfWork.UserRepository.GetByDepartmentId(departmentId);

            if (existUsers.Any())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check if department name is exist
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsDepartmentNameExist(DepartmentModel model)
        {
            return _unitOfWork.DepartmentRepository.IsDepartmentNameExist(model);
        }

        /// <summary>
        /// Get hierachy departments
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Node> GetDeparmentHierarchy(int? id = null)
        {
            return _unitOfWork.DepartmentRepository.GetDepartmentHierarchy(id);
        }

        #region Helpers

        /// <summary>
        /// Get self or child of department
        /// </summary>public const string ApiDepartmentsImport = "/departments/import";
        /// <param name="id"></param>
        /// <param name="departments"></param>
        /// <returns></returns>
        private IEnumerable<Department> GetSelfOrChild(int id, List<Department> departments)
        {
            yield return departments.FirstOrDefault(m => m.Id == id);
            foreach (var department in departments.Where(m => m.ParentId == id))
            {
                foreach (var child in GetSelfOrChild(department.Id, departments))
                {
                    yield return child;
                }
            }
        }

        public void Import()
        {
        }

        /// <summary>
        /// Export department data to excel or txt file
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns> byte[] fileByte </returns>
        public byte[] Export(string type, string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var fileByte = type == Constants.Excel
                ? ExportExcel(filter, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered)
                : ExportTxt(filter, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered);

            //Save system log
            var companyId = _httpContext.User.GetCompanyId();

            var content = DepartmentResource.msgExportDepartmentList;
            var contentsDetails = $"{AccountResource.lblUsername} : {_httpContext.User.GetUsername()}";

            _unitOfWork.SystemLogRepository.Add(1, SystemLogType.Department, ActionLogType.Export, content, contentsDetails, null, _httpContext.User.GetCompanyId());
            _unitOfWork.Save();

            return fileByte;
        }

        /// <summary>
        /// Export department data to excel file
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public byte[] ExportExcel(string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            byte[] result;
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(DepartmentResource.lblDepartment); //Worksheet name

                var departments = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords,
                        out recordsFiltered)
                    .ToList();

                //First add the headers for user sheet
                for (var i = 0; i < _header.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }
                var recordIndex = 2;

                foreach (var department in departments)
                {
                    //For the Department sheet
                    var colIndex = 1;
                    worksheet.Cells[recordIndex, colIndex++].Value = department.DepartmentNumber;
                    worksheet.Cells[recordIndex, colIndex++].Value = department.DepartmentName;
                    worksheet.Cells[recordIndex, colIndex++].Value = department.NumberUser;
                    worksheet.Cells[recordIndex, colIndex].Value = department.DepartmentManager;
                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        /// <summary>
        /// Export department data to txt file
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public byte[] ExportTxt(string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var departments = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered)
                .Select(x => new object[]
                {
                    x.DepartmentNumber,
                    x.DepartmentName,
                    x.NumberUser,
                    x.DepartmentManager
                }).ToList();

            // Build the file content
            var departmentTxt = new StringBuilder();
            departments.ForEach(line =>
            {
                departmentTxt.AppendLine(string.Join(",", line));
            });

            byte[] buffer = Encoding.UTF8.GetBytes($"{string.Join(",", _header)}\r\n{departmentTxt}");
            return buffer;
        }

        /// <summary>
        /// Filter and order data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<DepartmentListModel> FilterDataWithOrder(string filter, int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var data = _unitOfWork.AppDbContext.Department.Include(m => m.Parent).Include(m => m.DepartmentManager)
                .Where(m => !m.IsDeleted && m.CompanyId == companyId);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                data = data.Where(x =>
                    x.DepartNo.ToLower().Contains(filter) ||
                    x.DepartName.ToLower().Contains(filter) ||
                    (x.Parent != null && x.Parent.DepartName.ToLower().Contains(filter)));
            }

            recordsFiltered = data.Count();

            var result = data.AsEnumerable()
                .Select(m =>
                {
                    DepartmentListModel listModel = new DepartmentListModel
                    {
                        Id = m.Id,
                        DepartmentNumber = m.DepartNo,
                        DepartmentName = m.DepartName,
                        NumberUser = GetUserCount(companyId, m.Id),
                        DepartmentManagerId = m.DepartmentManagerId?.ToString(),
                        DepartmentManager = GetUserNameByEmail(companyId, m.DepartmentManager?.Username)
                    };
                    return listModel;
                });

            if (sortDirection.Equals("desc"))
            {
                switch (sortColumn)
                {
                    case 0:
                        result = result.OrderByDescending(c => c.DepartmentNumber);
                        break;
                    case 1:
                        result = result.OrderByDescending(c => c.DepartmentName);
                        break;
                    case 2:
                        result = result.OrderByDescending(c => c.NumberUser);
                        break;
                    case 3:
                        result = result.OrderByDescending(c => c.DepartmentManager);
                        break;

                    default:
                        break;
                };
            }
            else if (sortDirection.Equals("asc"))
            {
                switch (sortColumn)
                {
                    case 0:
                        result = result.OrderBy(c => c.DepartmentNumber);
                        break;
                    case 1:
                        result = result.OrderBy(c => c.DepartmentName);
                        break;
                    case 2:
                        result = result.OrderBy(c => c.NumberUser);
                        break;
                    case 3:
                        result = result.OrderBy(c => c.DepartmentManager);
                        break;

                    default:
                        break;
                };
            }

            return result.ToList();

        }



        public List<DepartmentListItemModel> ListDeparmentParent(string filter, int sortColumn, string sortDirection,
         out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var data = _unitOfWork.AppDbContext.Department.Include(m => m.Parent).Include(m => m.DepartmentManager)
                .Where(m => !m.IsDeleted && m.CompanyId == companyId);
            if (data == null)
            {
                totalRecords = 0;
                recordsFiltered = 0;
                return new List<DepartmentListItemModel>();
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                data = data.Where(x =>
                    x.DepartNo.ToLower().Contains(filter) ||
                    x.DepartName.ToLower().Contains(filter) ||
                    (x.Parent != null && x.Parent.DepartName.ToLower().Contains(filter)));
            }

            recordsFiltered = data.Count();

            var result = data.AsEnumerable()
                .Select(m =>
                {
                    DepartmentListItemModel listModel = new DepartmentListItemModel
                    //(m.Id, m.DepartNo, m.DepartName, false, null, null, m.DepartmentManagerId?.ToString()
                    //    , GetUserNameByEmail(companyId, m.DepartmentManager?.Username), GetUserCount(companyId, m.Id), m.ParentId)
                    {
                        Id = m.Id,
                        DepartmentNumber = m.DepartNo?.ToString(),
                        IsRoot = false,
                        EditUrl = null,
                        DeleteUrl = null,
                        DepartmentManagerId = m.DepartmentManagerId?.ToString(),
                        DepartmentManager = GetUserNameByEmail(companyId, m.DepartmentManager?.Username)?.ToString(),
                        DepartmentName = m.DepartName?.ToString(),
                        NumberUser = GetUserCount(companyId, m.Id),
                        ParentId = Convert.ToInt32(m.ParentId),
                        ParentDepartment = m.Parent == null ? "string" : m.Parent.DepartName?.ToString(),
                    };
                    return listModel;
                });

            if (sortDirection.Equals("desc"))
            {
                switch (sortColumn)
                {
                    case 0:
                        result = result.OrderByDescending(c => c.DepartmentNumber);
                        break;
                    case 1:
                        result = result.OrderByDescending(c => c.DepartmentName);
                        break;
                    case 2:
                        result = result.OrderByDescending(c => c.NumberUser);
                        break;
                    case 3:
                        result = result.OrderByDescending(c => c.DepartmentManager);
                        break;

                    default:
                        break;
                };
            }
            else if (sortDirection.Equals("asc"))
            {
                switch (sortColumn)
                {
                    case 0:
                        result = result.OrderBy(c => c.DepartmentNumber);
                        break;
                    case 1:
                        result = result.OrderBy(c => c.DepartmentName);
                        break;
                    case 2:
                        result = result.OrderBy(c => c.NumberUser);
                        break;
                    case 3:
                        result = result.OrderBy(c => c.DepartmentManager);
                        break;

                    default:
                        break;
                };
            }

            return result.OrderBy(x => x.ParentId).ToList();

        }

        public static List<DepartmentListItemModel> GenerateTree(List<DepartmentListItemModel> collection, DepartmentListItemModel rootItem)
        {

            List<DepartmentListItemModel> lst = new List<DepartmentListItemModel>();
            var listCollection = collection.Where(c => c.ParentId == rootItem.Id).ToList();
            foreach (DepartmentListItemModel c in listCollection)
            {
                lst.Add(new DepartmentListItemModel
                {
                    Id = c.Id,
                    ParentId = rootItem.Id,
                    Children = GenerateTree(collection, c),
                    ParentDepartment = rootItem.DepartmentName?.ToString(),
                    DepartmentManagerId = c.DepartmentManagerId?.ToString(),
                    DepartmentNumber = c.DepartmentNumber?.ToString(),
                    DepartmentName = c.DepartmentName?.ToString(),
                    NumberUser = c.NumberUser,
                    IsRoot = c.IsRoot,
                    EditUrl = c.EditUrl,
                    DeleteUrl = c.DeleteUrl,
                    DepartmentManager = c.DepartmentManager?.ToString()

                });
            }
            return lst;
        }

        public bool CheckEditDepartment(DepartmentModel model)
        {
            var collection = _unitOfWork.AppDbContext.Department.Select(x => new DepartmentListItemModel
            {
                Id = x.Id,
                ParentId = Convert.ToInt32(x.ParentId)
            }).ToList();

            DepartmentListItemModel dep = new DepartmentListItemModel();
            dep.Id = model.Id;
            dep.DepartmentName = model.Name;
            dep.ParentId = Convert.ToInt32(model.ParentId);
            dep.DepartmentManagerId = model.DepartmentManagerId.ToString();

            var lst = GenerateDepartment(collection, dep);

            if (model.Id == model.ParentId)
                return false;
            foreach (var item in lst)
            {
                if (model.Id == item.ParentId)
                    return false;
            }
            return true;
        }

        public static List<DepartmentListItemModel> GenerateDepartment(List<DepartmentListItemModel> collection, DepartmentListItemModel rootItem)
        {

            var lst = new List<DepartmentListItemModel>();

            var listCollection = collection.Where(c => c.Id == rootItem.ParentId).FirstOrDefault();


            var listParent = ListParentId(rootItem.ParentId);

            if (listCollection == null)
                return listParent;

            GenerateDepartment(collection, listCollection);


            return listParent;
        }

        public static List<DepartmentListItemModel> ListParentId(int ParentId)
        {

            DepartmentListItemModel ab = new DepartmentListItemModel();
            ab.ParentId = ParentId;
            lst.Add(ab);
            return lst;
        }

        public List<DepartmentListItemModel> GetListDepartment(string search, out int recordsTotal, out int recordsFiltered, int pageNumber = 1, int pageSize = 10, int sortColumn = 0,
           string sortDirection = "desc")
        {
            var departments = GetPaginatedListMenu(search, pageNumber, pageSize, sortColumn, sortDirection, out recordsTotal, out recordsFiltered);
            if (departments == null)
            {
                recordsTotal = 0;
                recordsFiltered = 0;
                return new List<DepartmentListItemModel>();
            }
            List<DepartmentListItemModel> lst = new List<DepartmentListItemModel>();
            foreach (var c in departments)
            {

                var lstDeparment = GenerateTree(departments, c);

                lst.Add(new DepartmentListItemModel
                {
                    Id = c.Id,
                    ParentId = c.ParentId,
                    Children = lstDeparment,
                    DepartmentManagerId = c.DepartmentManagerId?.ToString(),
                    DepartmentNumber = c.DepartmentNumber?.ToString(),
                    DepartmentName = c.DepartmentName?.ToString(),
                    NumberUser = c.NumberUser,
                    IsRoot = c.IsRoot,
                    EditUrl = c.EditUrl,
                    DeleteUrl = c.DeleteUrl,
                    DepartmentManager = c.DepartmentManager?.ToString()
                });
            }
            return lst.Where(x => x.ParentId == 0).ToList();
        }




        private void ExportStringHandler(string field, ExcelWorksheet worksheet,
            int recordIndex, int colIndex, string loginName)
        {
            worksheet.Cells[recordIndex, colIndex].Value = field.ToString();

            worksheet.Cells[recordIndex, colIndex].Style.Fill.PatternType =
                ExcelFillStyle.Solid;
            worksheet.Cells[recordIndex, colIndex].Style.Fill.BackgroundColor
                .SetColor(Color.Yellow);
            worksheet.Cells[recordIndex, colIndex].AddComment("Incorrect Data", loginName);
        }

        /// <summary>
        /// Export invalid data to file
        /// </summary>
        /// <param name="departments"></param>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public byte[] ExportErrorData(List<DepartmentModel> departments, string loginName)
        {
            byte[] result;
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(Constants.WorkSheetName); //Worksheet name

                //First add the headers
                for (var i = 0; i < _header.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }

                //Add values
                var recordIndex = 2;
                departments = departments.DistinctBy(c => c.Name).ToList();
                foreach (var department in departments)
                {
                    var colIndex = 1;
                    ExportStringHandler(department.Number, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(department.Name, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(department.ParentId + "", worksheet, recordIndex, colIndex, loginName);

                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        /// <summary>
        /// import department file
        /// </summary>
        /// <param name="type"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool ImportFile(string type, IFormFile file, out int total, out int fail)
        {
            var data = new List<DepartmentModel>();
            var totalList = new List<DepartmentModel>();
            var failList = new List<DepartmentModel>();

            var isSuccess = type == Constants.Excel ?
                LoadDepartmentsFromExcelFile(file, data, out total, ref failList)
                : LoadDepartmentsFromTextFile(file, data, out total, ref failList);

            fail = failList.Count();
            //if (isSuccess)
            //{
            //    var companyId = _httpContext.User.GetCompanyId();

            //    //Save system log
            //    var content = $"{DepartmentResource.msgImportDepartmentList}";
            //    var contentsDetails = $"{AccountResource.lblUsername} : {_httpContext.User.GetUsername()}" +
            //                          $"\n{CommonResource.lblFileName} : {file.FileName}";

            //    _unitOfWork.SystemLogRepository.Add(1, SystemLogType.Department, ActionLogType.Import,
            //        content, contentsDetails, null, companyId);

            //    _unitOfWork.Save();
            //}

            //isSuccess = true;
            return isSuccess;
        }

        /// <summary>
        /// Import file to DB
        /// </summary>
        /// <param name="listImportDepartments"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private bool Import(string fileName, List<DepartmentModel> listImportDepartments, List<DepartmentModel> failedToImport)
        {
            bool result = false;
            var companyId = _httpContext.User.GetCompanyId();

            var invalidDepartments = failedToImport;
            //var validDepartments = new List<DepartmentModel>();
            var validDepartments = listImportDepartments;
            //var updatingDepartments = new List<DepartmentModel>();
            var nameList = new List<string>();

            // [Edward] 2020.03.03
            // Delete logic about updating department through imported file.
            //foreach (var department in listImportDepartments)
            //{
            //    if (IsDepartmentNameExist(department))
            //    {
            //        invalidDepartments.Add((department));
            //    }
            //    else if (IsDepartmentNumberExist(department))
            //    {
            //        var dept = _unitOfWork.DepartmentRepository.GetByDepartmentCode(department.Number);
            //        department.Id = dept.Id;
            //        department.ParentId = dept.ParentId;
            //        updatingDepartments.Add((department));
            //    }
            //    else
            //    {
            //        validDepartments.Add(department);
            //    }
            //}

            validDepartments = validDepartments.AsQueryable().OrderBy(m => m.ParentId).ToList();
            if (validDepartments.Any())
            {
                foreach (var validDepartment in validDepartments)
                {
                    nameList.Add(validDepartment.Name);
                    Add(validDepartment);
                }
                result = true;
            }
            else
            {
                result = false;
            }

            // [Edward] 2020.03.03
            // Delete logic about updating department through imported file.
            //if (updatingDepartments.Any())
            //{
            //    foreach (var updatingDepartment in updatingDepartments)
            //    {
            //        Update(updatingDepartment);
            //    }
            //}

            //Save system log
            var content = $"{DepartmentResource.msgImportDepartmentList}";
            var contentsDetails = $"{AccountResource.lblUsername} : {_httpContext.User.GetUsername()}" +
                                  $"\n{CommonResource.lblFileName} : {fileName}";

            if (invalidDepartments.Any())
            {
                result = false;

                contentsDetails += "\n" + string.Format(DepartmentResource.msgFailedToImport, invalidDepartments.Count());

                foreach (var fail in invalidDepartments)
                {
                    contentsDetails += $"\n{DepartmentResource.lblDepartmentNumber} : {fail.Number}" +
                        $" | {DepartmentResource.lblDepartmentName} : {fail.Name}";
                }
            }

            var departments = _unitOfWork.DepartmentRepository.GetByNamesAndCompanyId(nameList, companyId);

            var departmentIds = departments.Select(c => c.Id).ToList();
            var departmentId = departmentIds.FirstOrDefault();

            _unitOfWork.SystemLogRepository.Add(departmentId, SystemLogType.Department, ActionLogType.Import,
                content, contentsDetails, departmentIds, companyId);

            _unitOfWork.Save();

            return result;
        }

        ///// <summary>
        ///// Load user from text file
        ///// </summary>
        ///// <param name="filePath"></param>
        ///// <param name="data"></param>
        private bool LoadDepartmentsFromTextFile(IFormFile file, List<DepartmentModel> data, out int total, ref List<DepartmentModel> failList)
        {
            try
            {
                var csvfilerecord = FileHelpers.ConvertToStringArray(file);

                total = 0;

                foreach (var row in csvfilerecord.Skip(1))
                {
                    if (string.IsNullOrEmpty(row) || row.Equals("\r")) continue;
                    var item = ReadLineFromCsv(row);

                    total++;

                    if (IsOkToImport(item, data))
                        data.Add(item);
                    else
                        failList.Add(item);
                }
                return Import(file.FileName, data, failList);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                throw;
            }
        }

        /// <summary>   Loads departments from excel file. </summary>
        /// <remarks>   Edward, 2020-03-03. </remarks>
        /// <exception cref="InvalidFormatException">   Thrown when an Invalid Format error condition
        ///                                             occurs. </exception>
        /// <param name="file"> file to import. </param>
        /// <param name="data"> The data. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private bool LoadDepartmentsFromExcelFile(IFormFile file, List<DepartmentModel> data, out int total, ref List<DepartmentModel> failList)
        {
            try
            {
                using (var package = new ExcelPackage(FileHelpers.ConvertToStream(file)))
                {
                    ExcelWorksheet worksheet;
                    int columnCount;
                    try
                    {
                        worksheet = package.Workbook.Worksheets[1];
                        columnCount = worksheet.Dimension.End.Column;
                    }
                    catch (Exception)
                    {
                        throw new InvalidFormatException();
                    }

                    if (columnCount != _header.Length)
                    {
                        throw new InvalidFormatException();
                    }

                    total = 0;

                    for (int i = worksheet.Dimension.Start.Row + 1;
                        i <= worksheet.Dimension.End.Row;
                        i++)
                    {
                        var item = ReadLineFromExcel(worksheet, i);

                        total++;

                        if (IsOkToImport(item, data))
                            data.Add(item);
                        else
                            failList.Add(item);
                    }
                }

                var fileName = file.FileName;
                return Import(file.FileName, data, failList);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}:{Environment.NewLine} {e.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Check the DepartmentModel to see if it can be imported into the system.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private Boolean IsOkToImport(DepartmentModel item, List<DepartmentModel> data)
        {
            if (string.IsNullOrEmpty(item.Number) || string.IsNullOrEmpty(item.Name))
            {
                return false;
            }

            item.Number = item.Number.TrimStart().TrimEnd();
            item.Name = item.Name.TrimStart().TrimEnd();

            var companyId = _httpContext.User.GetCompanyId();

            var department = _unitOfWork.DepartmentRepository.GetByNameAndCompany(item.Name, companyId);
            if (department != null)
            {
                return false;
            }

            foreach (var eachData in data)
            {
                if (eachData.Name.Equals(item.Name))
                    return false;
            }

            department = _unitOfWork.DepartmentRepository.GetByNumberAndCompany(item.Number, companyId);
            if (department != null)
            {
                return false;
            }

            foreach (var eachData in data)
            {
                if (eachData.Number.Equals(item.Number))
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Read txt file
        /// </summary>
        /// <param name="csvLine"></param>
        /// <returns></returns>
        private DepartmentModel ReadLineFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            var columnCount = values.Length;

            if (columnCount != _header.Length)
            {
                throw new InvalidFormatException();
            }

            var colIndex = 0;

            var model = new DepartmentModel
            {
                Number = Convert.ToString(values[colIndex++]),
                Name = Convert.ToString(values[colIndex]),
                ParentId = Constants.DefaultDepartmentId
            };
            return model;
        }

        /// <summary>
        /// Read Line from Excel
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private DepartmentModel ReadLineFromExcel(ExcelWorksheet worksheet, int row)
        {
            var colIndex = 1;
            var cells = worksheet.Cells;
            var model = new DepartmentModel
            {
                Number = (Convert.ToString(cells[row, colIndex++].Value)),
                Name = (Convert.ToString(cells[row, colIndex++].Value)),
            };
            var parentDepartmentName = (Convert.ToString(cells[row, colIndex].Value));
            var parentDepartment =
                _unitOfWork.DepartmentRepository.GetByNameAndCompany(parentDepartmentName, _httpContext.User.GetCompanyId());
            model.ParentId = parentDepartment?.Id ?? 0;

            return model;
        }

        #endregion Helpers

        public Account GetAccount(int companyId, int accountId)
        {
            return _unitOfWork.AccountRepository.GetByIdAndCompanyId(companyId, accountId);
        }

        /// <summary>   Gets user count. </summary>
        /// <remarks>   Edward, 2020-03-17. </remarks>
        /// <param name="companyId">    Identifier for the company. </param>
        /// <param name="departmentId"> Identifier for the department. </param>
        /// <returns>   The user count. </returns>
        public int GetUserCount(int companyId, int departmentId)
        {
            return _unitOfWork.UserRepository.GetCountByDepartmentId(companyId, departmentId);
        }


        public string GetUserNameByEmail(int companyId, string emailAddress)
        {
            if (emailAddress == null)
            {
                return null;
            }

            var user = _unitOfWork.UserRepository.GetUserByEmail(companyId, emailAddress);

            if (user != null)
            {
                return user.FirstName + user.LastName;
            }
            else
            {
                return emailAddress;
            }

        }

        public Department GetById(int id)
        {
            return _unitOfWork.DepartmentRepository.GetById(id);
        }
    }
}