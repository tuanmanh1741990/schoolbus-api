﻿using System;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;

namespace DeMasterProCloud.Service
{
    public interface INotificationService
    {
        void SendMessage(string messageType, string notificationType, string msgUser, string msgBody, string companyCode);
    }

    public class NotificationService : INotificationService
    {
        private readonly IQueueService _queueService;
        public NotificationService(IQueueService queueService)
        {
            _queueService = queueService;
        }

        /// <summary>
        /// Send notification
        /// </summary>
        public void SendMessage(string messageType, string notificationType, string msgUser, string msgBody, string companyCode)
        {
            var topic = Constants.RabbitMq.NotificationTopic;
            if (companyCode!="") topic = $"{Constants.RabbitMq.NotificationTopic}.{companyCode}";
            var notification = new NotificationProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.Notification,
                Data = new NotificationProtocolDataDetail
                {
                    MessageType = messageType,
                    NotificationType = notificationType,
                    User = msgUser,
                    Message = msgBody
                }
            };
            var message = notification.ToString();
            _queueService.Publish(topic, message);
        }
    }
}
