﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealSetting;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DeMasterProCloud.Service
{
    public interface IMealSettingService
    {
        ReponseStatus AddMealSetting(AddMealSettingModel model);
        ReponseStatus UpdateMealSetting(int id, AddMealSettingModel model);
        ReponseStatus DeleteMealSetting(int id);
        List<MealSettingModel> GetListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        List<ListMealSettingModel> ListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        MealSettingModel GetMealSetting(int Id);
        //MealSettingModel CheckAcessTime(string rId, string accessTime);
        //bool CheckDeviceIsRestaurant(string rId);
    }
    public class MealSettingService : IMealSettingService
    {
        private readonly IUnitOfWork _unitOfWork;
        DeMasterProCloud.DataAccess.Models.MealSetting obj = new DataAccess.Models.MealSetting();
        private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public MealSettingService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContext = contextAccessor.HttpContext;
        }

        public ReponseStatus AddMealSetting(AddMealSettingModel model)
        {

            try
            {

                var icuDeviceId = _unitOfWork.AppDbContext.IcuDevice.Where(x => x.Id == model.icuId).FirstOrDefault();
                var lstMealSetting = _unitOfWork.MealSettingRepository.GetMealSettingByIcuId(icuDeviceId.Id);
                if (lstMealSetting != null)
                {
                    foreach (var item in lstMealSetting)
                    {
                        if ((item.Start <= Convert.ToDecimal(TimeSpan.Parse(model.start).TotalHours) && Convert.ToDecimal(TimeSpan.Parse(model.start).TotalHours) <= item.End)
                            || (item.Start <= Convert.ToDecimal(TimeSpan.Parse(model.end).TotalHours) && Convert.ToDecimal(TimeSpan.Parse(model.end).TotalHours) <= item.End))
                        {
                            res.message = "";
                            res.statusCode = true;
                            return res;
                        }
                    }
                }

                obj.IcuDeviceId = icuDeviceId.Id;
                obj.CornerId = model.cornerId;
                obj.MealTypeId = model.mealTypeId;
                obj.Start = Convert.ToDecimal(TimeSpan.Parse(model.start).TotalHours);
                obj.End = Convert.ToDecimal(TimeSpan.Parse(model.end).TotalHours);
                obj.Price = model.price;
                var add = _unitOfWork.MealSettingRepository.AddMealSetting(obj);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }

                else if (add.statusCode == true && add.message == null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                else if (add.statusCode == true && add.message == "")
                {
                    res.message = "";
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = add.data;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;

        }

        public ReponseStatus UpdateMealSetting(int id, AddMealSettingModel model)
        {

            try
            {
                var lstMealSetting = _unitOfWork.MealSettingRepository.GetMealSettingByIcuIdandMealTypId(model.icuId, model.mealTypeId);
                if (lstMealSetting != null)
                {
                    foreach (var item in lstMealSetting)
                    {
                        if ((item.Start <= Convert.ToDecimal(TimeSpan.Parse(model.start).TotalHours) && Convert.ToDecimal(TimeSpan.Parse(model.start).TotalHours) <= item.End)
                            || (item.Start <= Convert.ToDecimal(TimeSpan.Parse(model.end).TotalHours) && Convert.ToDecimal(TimeSpan.Parse(model.end).TotalHours) <= item.End))
                        {
                            res.message = "";
                            res.statusCode = true;
                            return res;
                        }
                    }
                }


                obj.Start = Convert.ToDecimal(TimeSpan.Parse(model.start).TotalHours);
                obj.End = Convert.ToDecimal(TimeSpan.Parse(model.end).TotalHours);
                obj.Price = model.price;
                obj.Id = id;

                var update = _unitOfWork.MealSettingRepository.UpdateMealSetting(obj);
                if (update.statusCode == false)
                {
                    res.message = Constants.CornerSetting.UpdateFailed;
                    res.statusCode = false;
                    return res;
                }
                else if (update.statusCode == true && update.message == "")
                {
                    res.message = "";
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.CornerSetting.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.UpdateFailed;
                res.statusCode = false;
            }
            return res;

        }
        public ReponseStatus DeleteMealSetting(int id)
        {

            try
            {
                var delete = _unitOfWork.MealSettingRepository.DeleteMealSetting(id);
                if (delete.statusCode == false)
                {
                    res.message = Constants.CornerSetting.DeleteFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.DeleteFailed;
                res.statusCode = false;
            }
            return res;

        }

        public List<MealSettingModel> GetListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var listMealSetting = _unitOfWork.MealSettingRepository.GetListMealSetting(filter, pageNumber, pageSize, sortColumn,
             sortDirection, out totalRecords, out recordsFiltered, _httpContext.User.GetCompanyId(), _httpContext.User.GetAccountType());

            return listMealSetting;
        }

        public List<ListMealSettingModel> ListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var listMealSetting = _unitOfWork.MealSettingRepository.ListMealSetting(filter, pageNumber, pageSize, sortColumn,
             sortDirection, out totalRecords, out recordsFiltered, _httpContext.User.GetCompanyId(), _httpContext.User.GetAccountType());

            return listMealSetting;
        }

        public MealSettingModel CheckAcessTime(string rId, string accessTime)
        {
            
            var checkIcuIsRes = _unitOfWork.MealSettingRepository.CheckAcessTime(rId, accessTime);
            

            return checkIcuIsRes;
        }



        public MealSettingModel GetMealSetting(int Id)
        {
            var data = _unitOfWork.MealSettingRepository.GetMealSetting(Id);
            return data;
        }
    }
}
