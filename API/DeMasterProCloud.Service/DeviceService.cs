﻿using AutoMapper;
using Bogus;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.Holiday;
using DeMasterProCloud.DataModel.Timezone;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;
using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DeMasterProCloud.Service
{
    public interface IDeviceService : IPaginationService<IcuDevice>
    {
        int Add(DeviceModel model);

        DeviceDataModel InitData(DeviceDataModel model, int companyId = 0);

        DeviceInitModel InitializeData();

        void Update(DeviceModel model);

        IcuDevice GetByIdAndCompany(int id, int companyId);

        IcuDevice GetById(int id);

        List<IcuDevice> GetByIds(List<int> ids);

        void Delete(IcuDevice device);

        List<IcuDevice> GetByIdsAndCompany(List<int> idArr, int companyId);

        void DeleteRange(List<IcuDevice> devices);

        bool IsDeviceAddressExist(DeviceModel model);

        void UpdateDeviceStatus(IcuDevice device, bool status);

        List<IcuDevice> GenerateTestData(int numberOfDevice);

        void SendDeviceInfo(string deviceAddress, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType = "");

        int SendInstruction(List<IcuDevice> devices, DeviceInstruction model);

        bool HasTimezone(int timezoneId, int companyId);

        IEnumerable<IcuDevice> GetDoorList();

        void Reinstall(List<IcuDevice> devices, bool isAddDevice = false, List<ReinstallDeviceDetail> reinstallDevices = null);

        void CopyDevices(IcuDevice deviceCopy, List<IcuDevice> devices);

        DeviceTypeList GetListDeviceType(List<IcuDevice> devices);

        void UploadFile(IFormFileCollection files);

        void AssignToFullAccessGroup(int doorId, int companyId);

        int SendDisconnectDeviceNotification(string deviceAddress);

        List<IcuDevice> GetOnlineDevices(List<IcuDevice> devices, ref string firstOffAddr);

        IQueryable<AccessibleUserModel> GetPaginatedAccessibleUsers(int id, string filter, int pageNumber, int pageSize, int sortColumn, string sortDirection, out int totalRecords, out int recordsFiltered);

        IQueryable<IcuDevice> GetPaginatedDeviceValid(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, List<int> companyId, List<int> connectionStatus, List<int> deviceType);

        byte[] ExportAccessibleUsers(IcuDevice icuDevice, string type, string search, out int totalRecords,
            out int recordsFiltered, int sortColumn = 0,
            string sortDirection = "desc");

        byte[] ExportUserSetting(IcuDevice device);

        byte[] ExportUserMasterCard(IcuDevice device);

        List<MasterCardModel> GetPaginatedDeviceMasterCard(string filter, int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        IcuDevice GetByIdAndCompanyIncludeTimezone(int id);

        CheckDeviceInfoDetail LoadDeviceSetting(IcuDevice device, string msgId);

        TimezoneInfoModel LoadTimezone(IcuDevice device, string msgId);

        HolidayInfoModel LoadHoliday(IcuDevice devicem, string msgId);

        DeviceMsgIdModel LoadUser(IcuDevice device);

        TransmitInfoModel GetTransmitAllData();

        void TransmitData(TransmitDataModel models, List<IcuDevice> devices);

        UserInfoModel GetUserInfo(IcuDevice device, string cardId);

        UserInfoByCardIdModel GetUserInfoByCardId(IcuDevice device, string cardId);

        UserMasterCardModel GetUserMasterCard(IcuDevice device, int sortColumn = 0, string sortDirection = "desc");

        string SendDeviceInstruction(IcuDevice device,
                                     string command,
                                     string groupMsgId = "",
                                     int groupIndex = 0,
                                     int groupLength = 1,
                                     string processId = "",
                                     string actionType = "",
                                     int openPeriod = 0,
                                     string openUtilTime = "",
                                     string target = "",
                                     string fwType = "");

        List<CardModel> GetCardModelsByUserId(int userId);

        int GetCardCountByDeviceId(int deviceId);

        void RequestBackup();

        void RegisterConsumerService(IcuDevice device);

        void StopProcess(List<IcuDevice> devices, List<string> processIds);

        string SendDeviceConfig(IcuDevice icuDevice,
                                            string protocolType,
                                            string groupMsgId = "",
                                            int groupIndex = 0,
                                            int groupLength = 1,
                                            string processId = "",
                                            string actionType = "");

        List<IcuDevice> GetByCompanyId(int companyId);

        IEnumerable<DeviceHistoryModel> GetHistory(int deviceId, int pageNumber, int pageSize, out int totalRecords);

        IQueryable<IcuDevice> GetPaginatedDevices(string filter, int deviceType, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, short operationType, List<int> companyId, List<int> connectionStatus, List<int> _deviceType);
        void UpdateUpTimeToDevice(int deviceId);
        int ReUpdateUpTimOnlineDevice();
        void ReUpdateUpTimOnlineDeviceById(int id);
        object ListFilterDeviceMonitoring();
    }

    public class DeviceService : IDeviceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;
        private readonly IConnection _connection;
        private readonly IAccessGroupService _accessGroupService;
        private readonly ITimezoneService _timezoneService;
        private readonly IHolidayService _holidayService;
        private readonly INotificationService _notificationService;
        private readonly IDeviceMessageService _deviceMessageService;
        private List<string> _pendingMessages;
        /// <summary>
        /// String array display in header sheet when export file
        /// </summary>
        private readonly string[] _headerForAccessibleUser =
        {
            UserResource.lblIndex,
            UserResource.lblCardId,
            UserResource.lblName,
            UserResource.lblDepartment,
            UserResource.lblEmployeeNumber,
            UserResource.lblExpiredDate,
            UserResource.lblCardStatus,
        };

        private readonly string[] _userSettingHeaders = {
            UserResource.lblCardId,
            UserResource.lblName,
            DepartmentResource.lblDepartment,
            UserResource.lblEmployeeNumber,
            UserResource.lblExpiredDate,
            UserResource.lblIssueCount,
            UserResource.lblIsMasterCard,
            UserResource.lblEffectiveDate,
            UserResource.lblCardStatus,
            UserResource.lblKeyPadPassword,
            TimezoneResource.lblTimezone
        };

        private readonly string[] _userMasterCardHeaders =
        {
            "No",
            "Card ID Number - DB",
            "Name - DB",
            "Card ID Number - Device",
            "Name - Device"
        };

        public DeviceService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public DeviceService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor, IConfiguration configuration,
            IQueueService queueService, IAccessGroupService accessGroupService, ITimezoneService timezoneService, IHolidayService holidayService,
            INotificationService notificationService, IDeviceMessageService deviceMessageService, ISystemLogService systemLogService)
        {
            _unitOfWork = unitOfWork;
            _httpContext = contextAccessor.HttpContext;
            _configuration = configuration;
            _logger = ApplicationVariables.LoggerFactory.CreateLogger<DeviceService>();
            _queueService = queueService;
            _accessGroupService = accessGroupService;
            _connection = ApplicationVariables.getAvailableConnection(_configuration);
            _timezoneService = timezoneService;
            _holidayService = holidayService;
            _notificationService = notificationService;
            _deviceMessageService = deviceMessageService;
            _pendingMessages = new List<string>();
        }

        /// <summary>
        /// Initial data
        /// </summary>
        /// <param name="model"></param>
        public DeviceDataModel InitData(DeviceDataModel model, int companyId = 0)
        {
            var currentCompanyId = _httpContext.User.GetCompanyId();

            if (companyId != 0)
            {
                var timezoneList = _unitOfWork.TimezoneRepository
                    .GetMany(m => !m.IsDeleted && m.CompanyId == companyId).Select(
                        m =>
                            new SelectListItemModel
                            {
                                Id = m.Id,
                                Name = m.Name
                            }).ToList();
                model.ActiveTimezoneItems = timezoneList;

                if (model.ActiveTimezoneId == 0 && model.ActiveTimezoneItems.Any())
                {
                    model.ActiveTimezoneId = _unitOfWork.TimezoneRepository
                        .GetDefaultTzByCompanyId(Constants.Settings.DefaultPositionActiveTimezone, companyId).Id;
                }

                model.PassageTimezoneItems = timezoneList;

                if (model.PassageTimezoneId == null || model.PassageTimezoneId == 0 && model.PassageTimezoneItems.Any())
                {
                    model.PassageTimezoneId = _unitOfWork.TimezoneRepository
                        .GetDefaultTzByCompanyId(Constants.Settings.DefaultPositionPassageTimezone, companyId).Id;
                }

                model.BuildingItems = _unitOfWork.BuildingRepository
                    .GetByCompanyId(companyId)
                    .Select(
                        m =>
                        new SelectListItemModel
                        {
                            Id = m.Id,
                            Name = m.Name
                        }).ToList();

                model.AccessTzItems = _unitOfWork.TimezoneRepository
                    .GetMany(m => !m.IsDeleted && m.CompanyId == companyId)
                    .Select(
                        m =>
                        new SelectListItemModel
                        {
                            Id = m.Id,
                            Name = m.Name
                        }).ToList();

                if (model.BuildingId == 0 && model.BuildingItems.Any())
                {
                    model.BuildingId = _unitOfWork.BuildingRepository
                        .GetDefaultByCompanyId(companyId).Id;
                }

                model.CompanyId = companyId;
            }
            else
            {
                var defaultList = _unitOfWork.TimezoneRepository
                    .GetMany(m => !m.IsDeleted && m.CompanyId == companyId).Select(
                        m =>
                            new SelectListItemModel
                            {
                                Id = m.Id,
                                Name = m.Name
                            }).ToList();

                var noneList = new SelectListItemModel
                {
                    Id = 1,
                    Name = "None"
                };

                model.ActiveTimezoneItems = defaultList;
                model.ActiveTimezoneItems = model.ActiveTimezoneItems.Concat(noneList);
                model.ActiveTimezoneId = 1;
                model.PassageTimezoneItems = defaultList;
                model.PassageTimezoneItems = model.PassageTimezoneItems.Concat(noneList);
                model.PassageTimezoneId = 1;
                model.BuildingItems = defaultList;
                model.BuildingItems = model.BuildingItems.Concat(noneList);
                model.BuildingId = 1;
                model.AccessTzItems = defaultList;
                model.AccessTzItems = model.AccessTzItems.Concat(noneList);
            }

            if (currentCompanyId != 0)
            {
                model.CompanyItems = _unitOfWork.CompanyRepository.GetMany(m => !m.IsDeleted && m.Id == companyId).Select(m => new SelectListItemModel { Id = m.Id, Name = m.Name }).ToList();
            }
            else
            {
                model.CompanyItems = _unitOfWork.CompanyRepository.GetMany(m => !m.IsDeleted).Select(m => new SelectListItemModel { Id = m.Id, Name = m.Name }).ToList();
            }

            model.DeviceTypeItems = EnumHelper.ToEnumList<DeviceType>();

            model.VerifyModeItems = EnumHelper.ToEnumList<VerifyMode>();
            if (model.VerifyMode == 0 && model.VerifyModeItems.Any())
            {
                model.VerifyMode = model.VerifyModeItems.First().Id;
            }

            if (model.BackupPeriod == 0)
            {
                model.BackupPeriod = Constants.Settings.DefaultBackupPeriod;
            }

            model.OperationTypeItems = EnumHelper.ToEnumList<OperationType>();

            model.PassbackItems = EnumHelper.ToEnumList<PassbackRules>();
            model.Passback = model.Passback ?? 0;

            model.RoleItems = EnumHelper.ToEnumList<RoleRules>();
            if (model.Id == 0 && model.RoleItems.Any())
            {
                model.RoleReader0 = (short)model.RoleItems.First().Id;
                model.RoleReader1 = 1;
            }
            else
            {
                model.RoleReader0 = model.RoleReader0 == null ? 0 : model.RoleReader0;
                model.RoleReader1 = model.RoleReader1 == null ? 1 : model.RoleReader1;
            }

            //Init card reader led items
            model.CardReaderLedItems = EnumHelper.ToEnumList<CardReaderLed>();
            if (model.Id == 0 && model.CardReaderLedItems.Any())
            {
                model.LedReader0 = (short)model.CardReaderLedItems.First().Id;
                model.LedReader1 = (short)model.CardReaderLedItems.First().Id;
            }
            else
            {
                model.LedReader0 = model.LedReader0 == null ? 0 : model.LedReader0;
                model.LedReader1 = model.LedReader1 == null ? 0 : model.LedReader1;
            }

            model.BuzzerReaderItems = EnumHelper.ToEnumList<BuzzerReader>();
            if (model.Id == 0 && model.BuzzerReaderItems.Any())
            {
                model.BuzzerReader0 = (short)model.BuzzerReaderItems.First().Id;
                model.BuzzerReader1 = (short)model.BuzzerReaderItems.First().Id;

                model.DeviceBuzzer = (short)BuzzerReader.ON;
            }
            else
            {
                model.BuzzerReader0 = model.BuzzerReader0 == null ? 0 : model.BuzzerReader0;
                model.BuzzerReader1 = model.BuzzerReader1 == null ? 0 : model.BuzzerReader1;
            }

            model.UseCardReaderItems = EnumHelper.ToEnumList<UseCardReader>();
            model.UseCardReader = model.UseCardReader ?? (short)1;

            // Init
            // A. If click the add button, model.Id == 0
            // B. If double-click the device(for editing), model.Id == device.Id
            if (model.Id == 0)
            {
                model.SensorType = Constants.Settings.DefaultSensorType;

                model.LockOpenDuration = Constants.Settings.DefaultLockOpenDurationSeconds;
            }

            model.SensorTypeItems = EnumHelper.ToEnumList<SensorType>();
            if (model.SensorType != 0 && model.SensorType != 1 && model.SensorType != 2)
            {
                model.SensorType = Constants.Settings.DefaultSensorType;
            }

            if (model.SensorDuration == 0 || model.SensorDuration == null)
            {
                model.SensorDuration = Constants.Settings.DefaultSensorDuration;
            }

            if (model.MPRCount == null || model.MPRCount < Constants.Settings.DefaultMprAuthCount)
            {
                model.MPRCount = Constants.Settings.DefaultMprAuthCount;
            }

            if (model.MPRInterval == 0)
            {
                model.MPRInterval = Constants.Settings.DefaultMprInterval;
            }

            //if ()

            return model;
        }

        /// <summary>
        /// Add device
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Add(DeviceModel model)
        {
            IcuDevice deviceSaved = null;
            int deviceId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var device = Mapper.Map<IcuDevice>(model);
                        device.Status = (short)Status.Valid;

                        if (device.CompanyId == null)
                        {
                            device.ActiveTzId = null;
                            device.PassageTzId = null;
                            device.BuildingId = null;
                        }

                        _unitOfWork.IcuDeviceRepository.Add(device);
                        _unitOfWork.Save();
                        deviceSaved = device;

                        //RabbitMQ consumer registration
                        RegisterConsumerService(device);

                        var unregistedDevice =
                            _unitOfWork.UnregistedDevicesRepository.GetByDeviceAddress(device.DeviceAddress);
                        if (unregistedDevice != null)
                        {
                            _unitOfWork.UnregistedDevicesRepository.Delete(unregistedDevice);
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                        deviceId = device.Id;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        deviceId = 0;
                        throw;
                    }
                }
            });

            if (deviceSaved.CompanyId != null)
            {
                // Assign company
                AssignToCompany(new List<int> { deviceSaved.Id }, deviceSaved.CompanyId ?? 0);
            }

            if (deviceSaved.DeviceType != (short)DeviceType.DesktopApp)
            {
                //Send device to rabbit mq server
                SendDeviceInstruction(deviceSaved, Constants.CommandType.UpdateDeviceState);
            }
            return deviceId;
        }

        /// <summary>
        /// Assign the door(s) to company
        /// This function is used on the Edit or Add page of the device settings.
        /// </summary>
        /// <param name="deviceIds">list of device id</param>
        /// <param name="companyId">company id to assign</param>
        public void AssignToCompany(List<int> deviceIds, int companyId)
        {
            List<IcuDevice> AssignedDevices = null;

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var devices = GetByIds(deviceIds);

                        foreach (var eachDevice in devices)
                        {
                            var existCompanyId = eachDevice.CompanyId;

                            //Assign device
                            eachDevice.CompanyId = companyId;

                            eachDevice.ActiveTzId = eachDevice.ActiveTzId ?? _unitOfWork.TimezoneRepository
                                .GetDefaultTzByCompanyId(Constants.Settings.DefaultPositionActiveTimezone, companyId).Id;
                            eachDevice.PassageTzId = eachDevice.PassageTzId ?? _unitOfWork.TimezoneRepository
                                .GetDefaultTzByCompanyId(Constants.Settings.DefaultPositionPassageTimezone, companyId).Id;

                            var building = _unitOfWork.BuildingRepository
                                .GetByCompanyId(companyId).OrderBy(c => c.Id).FirstOrDefault();

                            if (building != null)
                            {
                                eachDevice.BuildingId = eachDevice.BuildingId ?? building.Id;
                            }
                            else
                            {
                                eachDevice.BuildingId = null;
                            }

                            _unitOfWork.IcuDeviceRepository.Update(eachDevice);

                            // If the device is already assigned to a company, save the unassign system logs to an existing company.
                            if (existCompanyId != null)
                            {
                                var contents = $"{ActionLogTypeResource.UnassignDoor} : {eachDevice.Name}";

                                _unitOfWork.SystemLogRepository.Add(eachDevice.Id, SystemLogType.DeviceSetting,
                                ActionLogType.Delete, contents, null, null, existCompanyId);

                                _unitOfWork.Save();
                            }
                        }

                        AssignedDevices = devices;

                        //Save system log
                        var device = devices.First();
                        var content = $"{ActionLogTypeResource.AssignDoor}";
                        var deviceAddresses = devices.Select(c => c.DeviceAddress).ToList();
                        var contentDetails = $"{DeviceResource.lblDeviceCount} : {devices.Count()} \n"
                        + $"{DeviceResource.lblDeviceAddress}: {string.Join(", ", deviceAddresses)}";

                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.Add,
                            content, contentDetails, deviceIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            foreach (var device in AssignedDevices)
            {
                //Assign to Full Access Group.
                AssignToFullAccessGroup(device.Id, companyId);

                // Instead of the RegisterConsumerService function, the Reinstall is executed.
                Reinstall(new List<IcuDevice> { device });
            }
        }

        /// <summary>
        /// Update device
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public void Update(DeviceModel model)
        {
            IcuDevice device = null;
            int? oldCompanyId = null;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        device = GetById(model.Id);
                        List<string> changes = new List<string>();
                        CheckChange(device, model, ref changes);


                        var existingName = device.Name;
                        oldCompanyId = device.CompanyId;
                        Mapper.Map(model, device);

                        // If device type is iTouchPop2A, useCardReader should be checked.
                        if (device.DeviceType == (short)DeviceType.ItouchPop)
                        {
                            if (device.UseCardReader == 1)
                            {
                                device.RoleReader1 = null;
                                device.LedReader1 = null;
                                device.BuzzerReader1 = null;
                            }
                        }

                        _unitOfWork.IcuDeviceRepository.Update(device);

                        // Save system log
                        if (device.CompanyId != null)
                        {
                            var content = string.Format(DeviceResource.lblUpdateDevice, existingName);
                            var contentsDetails = string.Join("\n", changes);
                            _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.Update,
                                content, contentsDetails, null, device.CompanyId);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            // Assign company
            if (device.CompanyId != null && oldCompanyId != device.CompanyId)
            {
                // Assign company
                AssignToCompany(new List<int> { device.Id }, device.CompanyId ?? 0);
            }

            if (device.ConnectionStatus != (short)IcuStatus.Connected)
            {
                SendDisconnectDeviceNotification(device.DeviceAddress);
            }
            else
            {
                //Send device to rabbit mq server
                var msgId = SendDeviceConfig(device, Constants.Protocol.UpdateDeviceConfig);
            }
        }

        /// <summary>
        /// Delete device
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public void Delete(IcuDevice device)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Delete device
                        device.IsDeleted = true;
                        device.Status = (short)Status.Invalid;
                        _unitOfWork.IcuDeviceRepository.Update(device);

                        //Save system log
                        if (device.Company != null)
                        {
                            var content =
                                $"{ActionLogTypeResource.Delete} : {device.Name} ({device.DeviceAddress})";

                            _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.Delete,
                                content, null, null, device.CompanyId);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            //Send device to rabbit mq server
            // [Edward] 2019.09.18
            // 존재 이유가 불명확하여 임시 주석
            //SendDeviceInstruction(device, Constants.CommandType.UpdateDeviceState);
            UnBindDeviceFromQueue(device.DeviceAddress);
        }

        /// <summary>
        /// Delete a list of device
        /// </summary>
        /// <param name="devices"></param>
        /// <returns></returns>
        public void DeleteRange(List<IcuDevice> devices)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var device in devices)
                        {
                            //Delete device
                            device.IsDeleted = true;
                            device.Status = (short)Status.Invalid;
                            _unitOfWork.IcuDeviceRepository.Update(device);

                            if (device.Company != null)
                            {
                                var content = $"{ActionLogTypeResource.Delete} : {device.Name} ({device.DeviceAddress})";

                                _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.Delete,
                                    content, null, null, device.CompanyId);
                            }
                        }

                        ////Save system log
                        //foreach (var device in devices)
                        //{
                        //    if (device.Company != null)
                        //    {
                        //        var content = $"{ActionLogTypeResource.Delete}: {device.Name} ({device.DeviceAddress})";

                        //        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting,
                        //            ActionLogType.Add, content, null, null, device.CompanyId);
                        //    }
                        //}

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            //Send device to rabbit mq server
            foreach (var device in devices)
            {
                // [Edward] 2019.09.18
                // 존재 이유가 불명확하여 임시 주석
                //SendDeviceConfig(device, Constants.Protocol.DeleteDevice);
                //SendDeviceInstruction(device, Constants.CommandType.UpdateDeviceState);
                UnBindDeviceFromQueue(device.DeviceAddress);
            }
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<IcuDevice> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.IcuDevice
                .Include(m => m.ActiveTz)
                .Include(m => m.PassageTz)
                .Include(m => m.Building)
                .Include(m => m.Company)
                .Where(m => !m.IsDeleted);

            if (_httpContext.User.GetAccountType() != (short)AccountType.SystemAdmin)
            {
                data = data.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Name.ToLower().Contains(filter.ToLower())
                                       || x.DeviceAddress.ToLower().Contains(filter.ToLower())
                                       || x.Building.Name.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Devices.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Devices[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            var accountTimezone = _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone;

            foreach (var device in data)
            {
                device.LastCommunicationTime = Helpers.ConvertToUserTimeZoneReturnDate(device.LastCommunicationTime, accountTimezone);
            }
            return data;
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<IcuDevice> GetPaginatedDeviceValid(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, List<int> companyId, List<int> connectionStatus, List<int> deviceType)
        {
            var allData = _unitOfWork.AppDbContext.IcuDevice
                .Include(m => m.ActiveTz)
                .Include(m => m.PassageTz)
                .Include(m => m.Building)
                .Include(m => m.Company)
                .Where(m => !m.IsDeleted && m.Status == (short)Status.Valid);

            if (_httpContext.User.GetAccountType() != (short)AccountType.SystemAdmin)
            {
                allData = allData.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());
            }

            totalRecords = allData.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                allData = allData.Where(x => x.Name.ToLower().Contains(filter.ToLower())
                                       || x.DeviceAddress.ToLower().Contains(filter.ToLower()));
            }

            if (companyId.Count() > 0)
            {
                allData = allData.Where(x => companyId.Contains(Convert.ToInt32(x.CompanyId)));
            }
            if (connectionStatus.Count() > 0)
            {
                allData = allData.Where(x => connectionStatus.Contains(Convert.ToInt32(x.ConnectionStatus)));
            }
            if (deviceType.Count() > 0)
            {
                allData = allData.Where(x => deviceType.Contains(Convert.ToInt32(x.DeviceType)));
            }

            recordsFiltered = allData.Count();
            sortColumn = sortColumn > ColumnDefines.DeviceValid.Length - 1 ? 0 : sortColumn;
            allData = allData.OrderBy($"{ColumnDefines.DeviceValid[sortColumn]} {sortDirection}");
            allData = allData.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            var accountTimezone = _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone;

            foreach (var device in allData)
            {
                device.LastCommunicationTime = Helpers.ConvertToUserTimeZoneReturnDate(device.LastCommunicationTime, accountTimezone);

            }

            return allData;
        }

        public object ListFilterDeviceMonitoring()
        {
            //List company
            var lstCompany = _unitOfWork.AppDbContext.Company.Where(x => x.IsDeleted == false).Select(x => new
            {
                x.Id,
                x.Name
            }).ToList();

            //List ConnectionStatus and deviceType
            var lstConnection = _unitOfWork.AppDbContext.IcuDevice.Where(x => x.IsDeleted == false).GroupBy(x => new { x.ConnectionStatus }).Select(x => new
            {
                x.First().ConnectionStatus,
            }).Distinct().ToList();

            var lstDeviceType = _unitOfWork.AppDbContext.IcuDevice.Where(x => x.IsDeleted == false).GroupBy(x => new { x.DeviceType }).Select(x => new IcuDevice
            {
                DeviceType = x.First().DeviceType,
                Id = x.First().DeviceType
            }).Distinct().ToList();

            var _lstDeviceType = lstDeviceType.Select(Mapper.Map<DeviceListModel>).ToList();


            var data = new
            {
                listCompany = lstCompany,
                listConnection = lstConnection.Select(x => new { Id = x.ConnectionStatus, Name = x.ConnectionStatus == 0 ? "Offline" : x.ConnectionStatus == 1 ? "Online" : "Other" }),
                listDeviceType = _lstDeviceType.Select(x => new { Id = x.Id, Name = x.DeviceType }),
            };
            return data;
        }

        public int GetCardCountByDeviceId(int deviceId)
        {
            int companyId = _unitOfWork.IcuDeviceRepository.GetByIcuId(deviceId).CompanyId ?? 0;

            var accessGroupIds = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(companyId, deviceId).Select(x => x.AccessGroupId).ToList();

            var data = _unitOfWork.UserRepository.GetAssignUsersByAccessGroupIds(companyId, accessGroupIds);

            var cardCount = 0;

            foreach (var user in data)
            {
                cardCount += _unitOfWork.CardRepository.GetCountByUserId(companyId, user.Id);
            }

            return cardCount;
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<MasterCardModel> GetPaginatedDeviceMasterCard(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            int companyId = _httpContext.User.GetCompanyId();

            var data = _unitOfWork.AppDbContext.IcuDevice
                .Where(m => !m.IsDeleted);

            if (companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Name.ToLower().Contains(filter.ToLower())
                                       || x.DeviceAddress.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.DeviceListModelForUser.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.DeviceListModelForUser[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            var listData = data.ToList();
            var returnListData = new List<MasterCardModel>();
            foreach (var device in listData)
            {
                var masterCardDevice = Mapper.Map<MasterCardModel>(device);
                var accessGroupIds = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(companyId, device.Id).Select(x => x.AccessGroupId).ToList();
                masterCardDevice.McNumber = _unitOfWork.UserRepository.GetCountUserMasterCard(companyId, accessGroupIds);
                returnListData.Add(masterCardDevice);
            }

            return returnListData;
        }

        /// <summary>
        /// get user master card
        /// </summary>
        /// <param name="device"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public UserMasterCardModel GetUserMasterCard(IcuDevice device, int sortColumn = 0, string sortDirection = "desc")
        {
            var msgId = MakeLoadUserProtocolData(device);
            var companyId = device.CompanyId ?? 0;
            var accessGroupIds = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(companyId, device.Id).Select(x => x.AccessGroupId).ToList();

            var usersFromDb = new List<UserMasterCardModelDetail>();

            var masterUsers = _unitOfWork.UserRepository.GetUserMasterCard(companyId, accessGroupIds)
                .AsEnumerable<User>().AsQueryable().ToList();

            foreach (var user in masterUsers)
            {
                var cards = _unitOfWork.CardRepository.GetByUserId(companyId, user.Id);

                foreach (var card in cards)
                {
                    var userMasterCardModelDetail = new UserMasterCardModelDetail
                    {
                        CardId = card.CardId ?? null,
                        UserName = user.FirstName
                    };
                    usersFromDb.Add(userMasterCardModelDetail);
                }
            }

            //queryable을 위한 변환과정
            var usersFromDbQueryable = usersFromDb.AsQueryable();

            sortColumn = sortColumn > ColumnDefines.UserMasterCard.Length - 1 ? 1 : sortColumn;
            usersFromDbQueryable = usersFromDbQueryable.OrderBy($"{ColumnDefines.UserMasterCard[sortColumn]} {sortDirection}");

            var userMasterCard = new UserMasterCardModel
            {
                UserMasterCard = usersFromDbQueryable.ToList(),
                MsgId = msgId
            };
            return userMasterCard;
        }

        /// <summary>
        /// Get paginated accessible users
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<AccessibleUserModel> GetPaginatedAccessibleUsers(int id, string filter, int pageNumber,
            int pageSize, int sortColumn, string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var accessGroupIds = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(companyId, id).Select(x => x.AccessGroupId).ToList();
            var data = _unitOfWork.UserRepository.GetAssignUsersByAccessGroupIds(companyId, accessGroupIds);

            data = data.Include(m => m.Card);
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.FirstName.ToLower().Contains(filter) ||
                    x.LastName.ToLower().Contains(filter) ||
                    x.Department.DepartName.ToLower().Contains(filter) ||
                    (!string.IsNullOrEmpty(x.EmpNumber) && x.EmpNumber.ToLower().Contains(filter)) ||
                    x.ExpiredDate.ToString().ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.RegisteredUser.Length - 1 ? 0 : sortColumn;

            var test = $"{ColumnDefines.RegisteredUser[sortColumn]} {sortDirection}";

            data = data.OrderBy(test);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            var result = data.AsEnumerable<User>().Select(Mapper.Map<AccessibleUserModel>).AsQueryable();

            return result;
        }

        /// <summary>
        /// Toggle device status
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public void UpdateDeviceStatus(IcuDevice device, bool status)
        {
            var actionType = ActionLogType.InvalidDoor;
            if (status == true)
            {
                device.Status = (short)Status.Valid;
                actionType = ActionLogType.ValidDoor;
            }
            else
            {
                device.Status = (short)Status.Invalid;
            }

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        _unitOfWork.IcuDeviceRepository.Update(device);

                        //Save system log
                        var content = device.Status == (short)Status.Valid
                        ? string.Format(DeviceResource.msgValidDoor, device.Name)
                        : string.Format(DeviceResource.msgInvalidDoor, device.Name);
                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, actionType,
                            content, null, null, device.CompanyId ?? 0);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            if (device.DeviceType != (short)DeviceType.DesktopApp)
            {
                SendDeviceInstruction(device, Constants.CommandType.UpdateDeviceState);
                if (actionType == ActionLogType.ValidDoor)
                {
                    Reinstall(new List<IcuDevice> { device }, true);
                }
            }
        }

        /// <summary>
        /// Get active devcie by company and device address
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="icuAddr"></param>
        /// <returns></returns>
        public IcuDevice GetActiveDeviceByCompanyAndAddress(int companyId, string icuAddr)
        {
            return _unitOfWork.IcuDeviceRepository.GetActiveDeviceByCompanyAndAddress(companyId, icuAddr);
        }

        /// <summary>
        /// Get devide by id and company
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IcuDevice GetByIdAndCompany(int id, int companyId)
        {
            var devices = _unitOfWork.AppDbContext.IcuDevice.Include(m => m.Company).Include(m => m.ActiveTz).Include(m => m.Building)
                .Where(m =>
                    m.Id == id && !m.IsDeleted);

            if (companyId != 0)
            {
                return devices.FirstOrDefault(m => m.CompanyId == companyId && !m.Company.IsDeleted);
            }
            else
            {
                return devices.FirstOrDefault();
            }
        }

        /// <summary>
        /// Get devide by id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IcuDevice GetById(int id)
        {
            return _unitOfWork.AppDbContext.IcuDevice
                .Where(m =>
                    id == m.Id && !m.IsDeleted).FirstOrDefault();
        }

        /// <summary>
        /// Get devide by ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<IcuDevice> GetByIds(List<int> ids)
        {
            return _unitOfWork.AppDbContext.IcuDevice
                .Where(m =>
                    ids.Contains(m.Id) && !m.IsDeleted).ToList();
        }

        /// <summary>
        /// Get devide by id and company
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IcuDevice GetByIdAndCompanyIncludeTimezone(int id)
        {
            return _unitOfWork.IcuDeviceRepository.GetByIdAndCompanyIncludeTimezone(id,
                _httpContext.User.GetCompanyId());
        }

        /// <summary>
        /// Check if device address is exist
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsDeviceAddressExist(DeviceModel model)
        {
            return _unitOfWork.AppDbContext.IcuDevice.Include(m => m.Company).Any(m =>
                m.DeviceAddress == model.DeviceAddress && m.Id != model.Id &&
                !m.Company.IsDeleted && !m.IsDeleted);
        }

        /// <summary>
        /// Get device by company and device address
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="icuAddr"></param>
        /// <returns></returns>
        public IcuDevice GetDeviceByCompanyAndAddress(int companyId, string icuAddr)
        {
            return _unitOfWork.IcuDeviceRepository.GetDeviceByCompanyAndAddress(companyId, icuAddr);
        }

        /// <summary>
        /// Get list of device by ids and company
        /// </summary>
        /// <param name="idArr"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<IcuDevice> GetByIdsAndCompany(List<int> idArr, int companyId)
        {
            var devices = _unitOfWork.AppDbContext.IcuDevice.Include(m => m.Company).Include(m => m.ActiveTz).Include(m => m.Building)
               .Where(m =>
                   idArr.Contains(m.Id) && !m.IsDeleted);

            if (companyId != 0)
            {
                return devices.Where(m => m.CompanyId == companyId && !m.Company.IsDeleted).ToList();
            }
            else
            {
                return devices.ToList();
            }
        }

        public List<IcuDevice> GetByCompanyId(int companyId)
        {
            var devices = _unitOfWork.AppDbContext.IcuDevice.Include(m => m.Company).Include(m => m.ActiveTz).Include(m => m.Building)
                .Where(m => !m.IsDeleted);

            if (companyId != 0)
            {
                return devices.Where(m => m.CompanyId == companyId && !m.Company.IsDeleted).ToList();
            }
            else
            {
                return devices.ToList();
            }
        }

        /// <summary>
        /// Generate icu devices
        /// </summary>
        /// <param name="numberOfDevice"></param>
        /// <returns></returns>
        public List<IcuDevice> GenerateTestData(int numberOfDevice)
        {
            var icuDevices = new List<IcuDevice>();
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var random = new Random();
                        for (var i = 0; i < numberOfDevice; i++)
                        {
                            var icuAddress = new string(Enumerable.Repeat(Constants.HexNumber, 6)
                                .Select(s => s[random.Next(s.Length)]).ToArray());
                            var fakeIcu = new Faker<IcuDevice>()
                                .RuleFor(u => u.DeviceAddress, f => icuAddress)
                                .RuleFor(u => u.Name, f => icuAddress)
                                .RuleFor(u => u.CompanyId, f => 1)
                                .RuleFor(u => u.BuildingId, f => 1)
                                .RuleFor(u => u.ActiveTzId, f => 1)
                                .RuleFor(u => u.LedReader0, f => (short)1)
                                .RuleFor(u => u.LedReader1, f => (short)1)
                                .RuleFor(u => u.IpAddress, (f, u) => f.Internet.Ip());

                            var icuDevice = fakeIcu.Generate();
                            _unitOfWork.IcuDeviceRepository.Add(icuDevice);
                            _unitOfWork.Save();

                            icuDevices.Add(icuDevice);
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            return icuDevices;
        }

        /// <summary>
        /// Publish a message to get device info
        /// <param name="deviceAddress"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="groupIndex"></param>
        /// <param name="groupLength"></param>
        /// <param name="processId"></param>
        /// </summary>
        public void SendDeviceInfo(string deviceAddress, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType = "")
        {
            var deviceInfo = new DeviceInfoProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Sender = _httpContext.User.GetUsername(),
                Type = Constants.Protocol.LoadDeviceInfo,
                Data = new DeviceInfoProtocolDataHeader()
            };
            var message = deviceInfo.ToString();
            var topic = Constants.RabbitMq.DeviceInfoTopic + "." + deviceAddress;
            IcuDevice device = _unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);
            if (groupLength == 1)
            {
                _queueService.Publish(topic, message);
            }
            else
            {
                _queueService.SendGroupMessage(deviceAddress: deviceAddress, deviceInfo.MsgId, message: message, topic: topic,
                    groupMsgId: groupMsgId, groupIndex: groupIndex, groupLength: groupLength, processId: processId, actionType: actionType);
            }
        }

        /// <summary>
        /// Register consumer for the changed device.
        /// </summary>
        /// <param name="device"></param>
        public void RegisterConsumerService(IcuDevice device)
        {
            var channel1 = _connection.CreateModel();

            ConsumerService.HandleResponse(device, channel1, _configuration, _queueService, _pendingMessages);
            var channel2 = _connection.CreateModel();
            RegisterMultipleMessageTaskQueue(device, channel2);
        }

        private void RegisterMultipleMessageTaskQueue(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.MultipleMessagesTaskQueue}_{device.DeviceAddress}";

            channel.QueueDeclare(queue: deviceRoutingKey,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var discardMessageGroup = new List<string>();
            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, ea) =>
            {
                var messageConsumer = new MultipleMessagesConsumer(_configuration, _queueService, _pendingMessages, discardMessageGroup);
                messageConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };

            channel.BasicConsume(queue: $"{Constants.RabbitMq.MultipleMessagesTaskQueue}_{device.DeviceAddress}",
                autoAck: false,
                consumer: consumer);

            // Add channel to device channel list
            if (ApplicationVariables.DeviceChannelList.ContainsKey(device.DeviceAddress))
            {
                ApplicationVariables.DeviceChannelList[device.DeviceAddress].Add(channel);
            }
            else
            {
                ApplicationVariables.DeviceChannelList[device.DeviceAddress] = new List<IModel>();
                ApplicationVariables.DeviceChannelList[device.DeviceAddress].Add(channel);
            }
        }

        /// <summary>
        /// Register device response consumer
        /// </summary>
        /// <param name="device"></param>
        internal void RegisterReaderConfigurationResponseConsumer(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.ConfigurationResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var readerConfigurationResponseConsumer = new DeviceConfigConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                readerConfigurationResponseConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for device instruction
        /// </summary>
        /// <param name="device"></param>
        private void RegisterDeviceInstructionResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.DeviceInstructionResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceInstructionConsumer = new DeviceInstructionConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                deviceInstructionConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for device setting response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterDeviceSettingResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.DeviceSettingResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceSettingConsumer = new DeviceSettingConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                deviceSettingConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for Load User Response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterLoadUserResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.LoadUserResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var userInfoConsumer = new UserInfoConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                userInfoConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for Device Message Response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterDeviceMessageResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.DeviceMessageResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceMessageConsumer = new DeviceMessageConsumer(_configuration);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                deviceMessageConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for Event Count Response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterEventCountResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.EventLogEventCountResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var eventCountConsumer = new EventCountConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                eventCountConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for Event Recovery Response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterEventRecoveryResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.EventRecoveryResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var eventRecoveryConsumer = new EventRecoveryConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                eventRecoveryConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for Load Holiday Response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterLoadHolidayResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.LoadHolidayResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var checkHolidayConsumer = new CheckHolidayConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                checkHolidayConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for Load Timezone Response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterLoadTimezoneResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.LoadTimezoneResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var checkTimezoneConsumer = new CheckTimezoneConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                checkTimezoneConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for device instruction
        /// </summary>
        /// <param name="device"></param>
        private void RegisterHolidayResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.HolidayResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var holidayConsumer = new HolidayConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                holidayConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handler for timezone
        /// </summary>
        /// <param name="device"></param>
        private void RegisterTimezoneResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.TimezoneResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var timezoneConsumer = new TimezoneConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                timezoneConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handle for device instruction
        /// </summary>
        /// <param name="device"></param>
        private void RegisterFileTransferResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.FileTranferResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var fileTransferConsumer = new FileTransferConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                fileTransferConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Handler for device info response
        /// </summary>
        /// <param name="device"></param>
        private void RegisterDeviceInfoResponse(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.DeviceInfoResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var deviceInfoConsumer = new DeviceInfoConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                deviceInfoConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Register event log consumer
        /// </summary>
        /// <param name="device"></param>
        internal void RegisterEventLogConsumer(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.EventLogTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var eventLogConsumer = new EventLogConsumer(_configuration, _queueService);
                await eventLogConsumer.DoWork(ea.Body);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Register access control response consumer
        /// </summary>
        /// <param name="device"></param>
        internal void RegisterAccessControlResponseConsumer(IcuDevice device, IModel channel)
        {
            var queueName = channel.QueueDeclare().QueueName;
            var deviceRoutingKey = $"{Constants.RabbitMq.AccessControlResponseTopic}.{device.DeviceAddress}";
            channel.QueueBind(queue: queueName,
                exchange: Constants.RabbitMq.ExchangeName,
                routingKey: deviceRoutingKey);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var accessLevelLogConsumer = new AccessControlConsumer(_configuration, _queueService);
                Helpers.RemoveMsgIdFromPendingList(ea.Body, ref _pendingMessages);
                accessLevelLogConsumer.DoWork(ea.Body, ea.RoutingKey);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            channel.BasicConsume(queue: queueName,
                autoAck: false,
                consumer: consumer);
            //Add queue and routing key to tracking list
            ApplicationVariables.QueueAndRoutingKeyList.Add(new QueueAndRoutingKey
            {
                Queue = queueName,
                RoutingKey = deviceRoutingKey
            });
        }

        /// <summary>
        /// Notify to unbind device
        /// </summary>
        /// <param name="deviceAddress"></param>
        internal void UnBindDeviceFromQueue(string deviceAddress)
        {
            //Close all the channels that received message from device
            if (ApplicationVariables.DeviceChannelList.ContainsKey(deviceAddress))
            {
                foreach (var channel in ApplicationVariables.DeviceChannelList[deviceAddress])
                {
                    channel.Close();
                }
            }

            var icuAddress = new IcuAddress
            {
                Address = deviceAddress
            };
            var queueAndRoutingKeys = ApplicationVariables.QueueAndRoutingKeyList
                .Where(c => c.RoutingKey.EndsWith(icuAddress.Address)).ToList();
            if (queueAndRoutingKeys.Any())
            {
                if (_connection != null && _connection.IsOpen)
                {
                    using (var channel = _connection.CreateModel())
                    {
                        foreach (var queueAndRoutingKey in queueAndRoutingKeys)
                        {
                            channel.QueueDelete(queueAndRoutingKey.Queue);
                            ApplicationVariables.QueueAndRoutingKeyList.Remove(queueAndRoutingKey);
                        }
                    }
                }
            }
        }

        public void RequestBackup()
        {
            var devices = _unitOfWork.IcuDeviceRepository.GetDevicesByCompany(_httpContext.User.GetCompanyId());

            foreach (var device in devices)
            {
                var deviceRoutingKey = $"{Constants.RabbitMq.EventLogTopic}.{device.DeviceAddress}";

                var queueAndRoutingKeys = ApplicationVariables.QueueAndRoutingKeyList
                .Where(c => c.RoutingKey.Equals(deviceRoutingKey)).ToList();

                if (queueAndRoutingKeys.Any())
                {
                    if (_connection != null && _connection.IsOpen)
                    {
                        using (var channel = _connection.CreateModel())
                        {
                            foreach (var queueAndRoutingKey in queueAndRoutingKeys)
                            {
                                channel.QueueDelete(queueAndRoutingKey.Queue);
                                ApplicationVariables.QueueAndRoutingKeyList.Remove(queueAndRoutingKey);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Make Device Message protocol data
        /// </summary>
        /// <param name="messages"></param>
        /// <param name="protocolType"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        public DeviceMessageProtocolData MakeUpdateDeviceMessageProtocolData(IcuDevice device, List<DeviceMessage> messages, string protocolType)
        {
            //var messageList = new List<DeviceMessage>(messages.ToArray());
            var messageList = new List<DeviceMessage>();

            foreach (var message in messages)
            {
                messageList.Add(new DeviceMessage()
                {
                    Company = message.Company,
                    CompanyId = message.CompanyId,
                    Content = message.Content,
                    CreatedOn = message.CreatedOn,
                    CreatedBy = message.CreatedBy,
                    Id = message.Id,
                    MessageId = message.MessageId,
                    Remark = message.Remark,
                    UpdatedBy = message.UpdatedBy,
                    UpdatedOn = message.UpdatedOn
                });
            }

            var buildingName = _unitOfWork.BuildingRepository.GetById(device.BuildingId ?? Constants.DefaultBuildingId).Name;
            foreach (var message in messageList)
            {
                if (message.Content.Contains(Constants.FormatBuilding))
                {
                    message.Content = message.Content.Replace(Constants.FormatBuilding, buildingName);
                }

                if (message.Content.Contains(Constants.FormatDoor))
                {
                    message.Content = message.Content.Replace(Constants.FormatDoor, device.Name);
                }
            }

            var deviceMessageProtocolData = new DeviceMessageProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };

            var deviceMessageProtocolHeaderData = new DeviceMessageProtocolDataHeader()
            {
                Messages = Mapper.Map<List<DeviceMessageProtocolDataDetail>>(messageList)
            };

            deviceMessageProtocolData.Data = deviceMessageProtocolHeaderData;
            return deviceMessageProtocolData;
        }

        /// <summary>
        /// Send device's data to rabbit mq server
        /// </summary>
        /// <param name="icuDevice"></param>
        /// <param name="protocolType"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="isPublish"></param>
        /// <param name="isSave"></param>
        /// <param name="isAdd"></param>
        public string SendDeviceConfig(IcuDevice icuDevice, string protocolType, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType = "")
        {
            //Send devices
            var protocolData = MakeDeviceProtocolData(icuDevice, protocolType);
            var message = protocolData.ToString();
            var topic = $"{Constants.RabbitMq.ConfigurationTopic}.{icuDevice.DeviceAddress}";

            if (groupLength == 1)
            {
                _queueService.Publish(topic, message);
            }
            else
            {
                _queueService.SendGroupMessage(deviceAddress: icuDevice.DeviceAddress, protocolData.MsgId, message: message, topic: topic,
                    groupMsgId: groupMsgId, groupIndex: groupIndex, groupLength: groupLength, processId: processId, actionType: actionType);
            }

            return protocolData.MsgId;
        }

        /// <summary>
        /// Make device protocol data
        /// </summary>
        /// <param name="icuDevice"></param>
        /// <param name="protocolType"></param>
        /// <returns></returns>
        public IcuDeviceProtocolData MakeDeviceProtocolData(IcuDevice icuDevice, string protocolType)
        {
            var data = Mapper.Map<IcuDeviceProtocolDetailData>(icuDevice);
            if (icuDevice.PassageTzId != null)
            {
                var passageTz =
                    _unitOfWork.TimezoneRepository.GetByIdAndCompany(icuDevice.PassageTzId.Value, icuDevice.CompanyId ?? 0);
                data.PassageTimezone = passageTz.Position;
            }

            var activeTz = _unitOfWork.TimezoneRepository.GetByIdAndCompany(icuDevice.ActiveTzId, icuDevice.CompanyId ?? 0);
            if (activeTz != null)
            {
                data.ActiveTimezone = activeTz.Position;
            }

            if (icuDevice.DeviceType == (short)DeviceType.Icu300N)
            {
                data.ReaderCount = 2;
                data.ReaderConfig = new List<int>();

                var configReader0 = icuDevice.RoleReader0 << 0;
                configReader0 += icuDevice.LedReader0 << 1;
                configReader0 += icuDevice.BuzzerReader0 << 2;

                data.ReaderConfig.Add(configReader0 ?? 0);

                var configReader1 = icuDevice.RoleReader1 << 0;
                configReader1 += icuDevice.LedReader1 << 1;
                configReader1 += icuDevice.BuzzerReader1 << 2;

                data.ReaderConfig.Add(configReader1 ?? 0);
            }
            else if (icuDevice.DeviceType == (short)DeviceType.ItouchPop)
            {
                data.ReaderCount = icuDevice.UseCardReader == 0 ? 2 : 1;
                data.ReaderConfig = new List<int>();

                var configReader0 = icuDevice.RoleReader0 << 0;
                configReader0 += icuDevice.UseCardReader << 1;
                configReader0 += icuDevice.BuzzerReader0 << 2;

                data.ReaderConfig.Add(configReader0 ?? 0);

                if (data.ReaderCount == 2)
                {
                    var configReader1 = icuDevice.RoleReader1 << 0;
                    configReader1 += icuDevice.LedReader1 << 1;
                    configReader1 += icuDevice.BuzzerReader1 << 2;

                    data.ReaderConfig.Add(configReader1 ?? 0);
                }
            }
            data.qrAesKey = Helpers.EncryptSecretCode(data.qrAesKey);

            var deviceProtocolData = new IcuDeviceProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType,
                Sender = _httpContext.User.GetUsername(),
                Data = data
            };
            return deviceProtocolData;
        }

        /// <summary>
        /// Send device instruction with multiple device
        /// </summary>
        /// <param name="devices"></param>
        /// <param name="model"></param>
        public int SendInstruction(List<IcuDevice> devices, DeviceInstruction model)
        {
            int errorDeviceCnt = 0;
            string groupMsgId = "";

            foreach (var device in devices)
            {
                if (model.Command == Constants.CommandType.SetTime)
                {
                    groupMsgId = Guid.NewGuid().ToString();
                    // Send settime message
                    SendDeviceInstruction(device, model.Command, groupMsgId, groupIndex: 0, groupLength: 2);
                    // Send Device Info
                    SendDeviceInfo(device.DeviceAddress, groupMsgId: groupMsgId, groupIndex: 1, groupLength: 2);
                }
                else
                {
                    SendDeviceInstruction(device, model.Command, groupMsgId, openPeriod: model.OpenPeriod,
                    openUtilTime: model.OpenUntilTime);
                }
            }
            return errorDeviceCnt;
        }




        /// <summary>
        /// Make device instruction to ICU
        /// </summary>
        /// <param name="device"></param>
        /// <param name="command"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="groupIndex"></param>
        /// <param name="groupLength"></param>
        /// <param name="processId"></param>
        /// <param name="openPeriod"></param>
        /// <param name="openUtilTime"></param>
        /// <param name="target"></param>
        /// <param name="fwType"></param>
        public string MakeDeviceInstruction(IcuDevice device,
                                            string command,
                                            string msgId = "",
                                            string groupMsgId = "",
                                            int groupIndex = 0,
                                            int groupLength = 1,
                                            string processId = "",
                                            int openPeriod = 0,
                                            string openUntilTime = "",
                                            string target = "",
                                            string fwType = "")
        {
            if (string.IsNullOrEmpty(msgId))
                msgId = Guid.NewGuid().ToString();

            string message;
            string sender = _httpContext.User.GetUsername();
            string type = Constants.Protocol.DeviceInstruction;

            var content = "";
            var contentsDetails = "";
            List<string> details;

            if (command == Constants.CommandType.Open)
            {
                var systemLogs = _unitOfWork.AppDbContext.SystemLog
                        .Include(m => m.CreatedByNavigation)
                        .Include(m => m.Company)
                        .Where(m => m.CompanyId == device.CompanyId
                        && m.Type == (int)SystemLogType.Emergency
                        && m.Content.Contains(device.DeviceAddress))
                        .OrderBy($"{ColumnDefines.SystemLog[1]} desc")
                        .FirstOrDefault();

                //        .AsQueryable();
                //systemLogs = systemLogs.Where(m => m.CompanyId == device.CompanyId);
                //systemLogs = systemLogs.Where(m => m.Type == (int)SystemLogType.Emergency && m.Content.Contains(device.DeviceAddress));
                //systemLogs = systemLogs.OrderBy($"{ColumnDefines.SystemLog[1]} desc");
                var lastEmergencyCommand = systemLogs?.Action ?? (int)ActionLogType.Release;
                if (lastEmergencyCommand != (int)ActionLogType.Release)
                {
                    // Forced open or close state.
                    return "";
                }
            }

            switch (command)
            {
                case Constants.CommandType.Open:
                    var newOpenUtilTime = "";
                    if (!String.IsNullOrEmpty(openUntilTime))
                    {
                        newOpenUtilTime =
                            Helpers.ConvertDateTimeToStringByTimeZone(
                                DateTime.ParseExact(openUntilTime, Constants.DateTimeFormat.ddMMyyyyHHmmsszzz, null),
                                device.Building.TimeZone);
                    }
                    else
                    {
                        newOpenUtilTime = openUntilTime;
                    }

                    var dataDetailOpen = new DeviceInstructionOpenDetail
                    {
                        Command = command,
                        UserName = _httpContext.User.GetUsername(),
                        Options =
                        {
                            OpenPeriod = openPeriod,
                            OpenUntilTime = newOpenUtilTime
                        }
                    };
                    var deviceOpenInstruction = new DeviceInstructionOpenProtocolData
                    {
                        MsgId = msgId,
                        Sender = sender,
                        Type = type,
                        Data = dataDetailOpen
                    };
                    message = deviceOpenInstruction.ToString();

                    if (!string.IsNullOrEmpty(openUntilTime))
                        openUntilTime = DateTime.ParseExact(openUntilTime, Constants.DateTimeFormat.ddMMyyyyHHmmsszzz, null).ToSettingDateTimeString();

                    //Save system log
                    content = string.Format(DeviceResource.msgSendDeviceCommand, command);
                    details = new List<string>()
                    {
                        $"{DeviceResource.lblCommand} : {DeviceResource.lblOpenDoor}",
                        $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                        $"{DeviceResource.lblDoorName} : {device.Name}",
                        $"{DeviceResource.lblOpenPeriod } : {openPeriod}",
                        $"{DeviceResource.lblOpenUntilTime } : {openUntilTime}"
                    };
                    contentsDetails = string.Join("\n", details);

                    _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceMonitoring, ActionLogType.DoorOpen,
                        content, contentsDetails, null, device.CompanyId);

                    break;

                case Constants.CommandType.SetTime:
                    var timezone = _unitOfWork.BuildingRepository.GetById(device.BuildingId.Value).TimeZone;
                    var utcHour = Helpers.GetUtcOffSetHour(timezone);
                    var utcMinute = Helpers.GetUtcOffSetMinute(timezone);
                    var dataDetailSetTime = new DeviceInstructionSetTimeDetail
                    {
                        Command = command,
                        UserName = _httpContext.User.GetUsername(),
                        Options =
                        {
                            Time = Helpers.ConvertDateTimeByTimeZone(timezone),
                            IsSchedule = false
                        },
                        utcHour = utcHour,
                        utcMinute = utcMinute
                    };
                    var deviceSetTimeInstruction = new DeviceInstructionSetTimeProtocolData
                    {
                        MsgId = msgId,
                        Sender = sender,
                        Type = type,
                        Data = dataDetailSetTime
                    };
                    message = deviceSetTimeInstruction.ToString();

                    //Save system log
                    content = string.Format(DeviceResource.msgSendDeviceCommand, command);
                    details = new List<string>()
                    {
                        $"{DeviceResource.lblCommand} : {DeviceResource.lblTransmitCurrentTime}",
                        $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                        $"{DeviceResource.lblDoorName} : {device.Name}"
                    };
                    contentsDetails = string.Join("\n", details);

                    _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceMonitoring, ActionLogType.Sync,
                        content, contentsDetails, null, device.CompanyId);

                    break;

                case Constants.CommandType.UpdateDeviceState:
                    var dataDetailUpdateDeviceState = new DeviceInstructionUpdateDeviceStateDetail
                    {
                        Command = command,
                        UserName = _httpContext.User.GetUsername(),
                        Options =
                        {
                            State = device.Status
                        }
                    };
                    var deviceUpdateDeviceStateInstruction = new DeviceInstructionUpdateDeviceStateProtocolData
                    {
                        MsgId = msgId,
                        Sender = sender,
                        Type = type,
                        Data = dataDetailUpdateDeviceState
                    };
                    message = deviceUpdateDeviceStateInstruction.ToString();
                    break;

                case Constants.CommandType.UpdateFirmware:
                    var mainFirmwareProtocol = new MainFirmwareProtocolData
                    {
                        MsgId = msgId,
                        Sender = sender,
                        Type = Constants.Protocol.DeviceInstruction,
                        Data = new MainFirmwareHeader
                        {
                            Command = Constants.CommandType.UpdateFirmware,
                            Options = new Option
                            {
                                Target = target,
                                FwType = fwType
                            }
                        }
                    };
                    message = mainFirmwareProtocol.ToString();
                    break;

                default:
                    var dataDetail = new DeviceInstructionDetail
                    {
                        Command = command,
                        UserName = _httpContext.User.GetUsername()
                    };
                    var deviceInstruction = new DeviceInstructionProtocolData
                    {
                        MsgId = msgId,
                        Sender = sender,
                        Type = type,
                        Data = dataDetail
                    };
                    message = deviceInstruction.ToString();

                    //Save system log
                    if (command == Constants.CommandType.Reset)
                    {
                        content = string.Format(DeviceResource.msgSendDeviceCommand, command);

                        details = new List<string>
                        {
                            $"{DeviceResource.lblCommand} : {DeviceResource.lblReset}",
                            $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                            $"{DeviceResource.lblDoorName} : {device.Name}"
                        };

                        contentsDetails = string.Join("\n", details);
                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceMonitoring, ActionLogType.Reset,
                                                        content, contentsDetails, null, device.CompanyId);
                    }
                    else if (command == Constants.CommandType.ForceOpen)
                    {
                        content = string.Format(DeviceResource.msgSendDeviceCommand, command);

                        details = new List<string>
                        {
                            $"{DeviceResource.lblCommand} : {DeviceResource.lblForceOpen}",
                            $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                            $"{DeviceResource.lblDoorName} : {device.Name}"
                        };

                        contentsDetails = string.Join("\n", details);
                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.Emergency, ActionLogType.ForcedOpen,
                                                        content, contentsDetails, null, device.CompanyId);
                    }
                    else if (command == Constants.CommandType.ForceClose)
                    {
                        content = string.Format(DeviceResource.msgSendDeviceCommand, command);

                        details = new List<string>
                        {
                            $"{DeviceResource.lblCommand} : {DeviceResource.lblForceClose}",
                            $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                            $"{DeviceResource.lblDoorName} : {device.Name}"
                        };

                        contentsDetails = string.Join("\n", details);
                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.Emergency, ActionLogType.ForcedClose,
                                                        content, contentsDetails, null, device.CompanyId);
                    }
                    else if (command == Constants.CommandType.Release)
                    {
                        content = string.Format(DeviceResource.msgSendDeviceCommand, command);

                        details = new List<string>
                        {
                            $"{DeviceResource.lblCommand} : {DeviceResource.lblRelease}",
                            $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                            $"{DeviceResource.lblDoorName} : {device.Name}"
                        };

                        contentsDetails = string.Join("\n", details);
                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.Emergency, ActionLogType.Release,
                                                        content, contentsDetails, null, device.CompanyId);
                    }

                    break;
            }

            _unitOfWork.Save();

            return message;
        }

        /// <summary>
        /// Send device instruction to ICU
        /// </summary>
        /// <param name="device"></param>
        /// <param name="command"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="groupIndex"></param>
        /// <param name="groupLength"></param>
        /// <param name="processId"></param>
        /// <param name="openPeriod"></param>
        /// <param name="openUtilTime"></param>
        /// <param name="target"></param>
        /// <param name="fwType"></param>
        public string SendDeviceInstruction(IcuDevice device,
                                            string command,
                                            string groupMsgId = "",
                                            int groupIndex = 0,
                                            int groupLength = 1,
                                            string processId = "",
                                            string actionType = "",
                                            int openPeriod = 0,
                                            string openUtilTime = "",
                                            string target = "",
                                            string fwType = "")
        {
            var topic = $"{Constants.RabbitMq.DeviceInstructionTopic}.{device.DeviceAddress}";
            var msgId = Guid.NewGuid().ToString();
            var message = MakeDeviceInstruction(device, command, msgId, groupMsgId, groupIndex, groupLength, processId, openPeriod, openUtilTime, target, fwType);
            // We public message directly to the topic if it is single message
            // Else we publish message to multiple messages queue
            if (groupLength == 1)
            {
                _queueService.Publish(topic, message);
            }
            else
            {
                _queueService.SendGroupMessage(device.DeviceAddress, msgId, message, topic: topic, groupMsgId, groupIndex, groupLength, processId, actionType: actionType);
            }
            return msgId;
        }

        /// <summary>
        /// Assign a door to full access group
        /// </summary>
        /// <param name="doorId"></param>
        /// <param name="tzId"></param>
        public void AssignToFullAccessGroup(int doorId, int companyId)
        {
            var fullAccessGroup =
                _unitOfWork.AccessGroupRepository.GetFullAccessGroup(companyId);

            var tzId = _unitOfWork.TimezoneRepository.GetByPositionAndCompany(Constants.Tz24hPos, companyId).Id;

            var accessGroupId = fullAccessGroup.Id;
            var assignDoorDetail = new AccessGroupAssignDoorDetail
            {
                DoorId = doorId,
                TzId = tzId,
                CompanyId = companyId
            };
            var assignDoor = new AccessGroupAssignDoor
            {
                Doors = new List<AccessGroupAssignDoorDetail> { assignDoorDetail }
            };
            _accessGroupService.AssignDoors(accessGroupId, assignDoor, false);
        }

        /// <summary>
        /// Get list device by userid
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<IcuDevice> GetListIcuDevicesByUserId(int userId)
        {
            var accessGroupId = _unitOfWork.UserRepository.GetById(userId).AccessGroupId;
            var listAccessGroupDevice =
                _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(),
                    accessGroupId);
            var listIcu = listAccessGroupDevice.Select(x => x.Icu).ToList();
            return listIcu;
        }

        /// <summary>
        /// Check if door has a timezone
        /// </summary>
        /// <param name="timezoneId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool HasTimezone(int timezoneId, int companyId)
        {
            var result = false;
            var isUseForDoor = _unitOfWork.IcuDeviceRepository.HasTimezone(timezoneId, companyId);
            var isUseForAg = _unitOfWork.AccessGroupDeviceRepository.HasTimezone(timezoneId);
            if (isUseForDoor || isUseForAg)
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// Get valid door by company ( Logged in account )
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IcuDevice> GetDoorList()
        {
            var companyId = _httpContext.User.GetCompanyId();
            var devices = _unitOfWork.AppDbContext.IcuDevice.Where(m => m.Status == (short)Status.Valid).OrderBy(c => c.Name);

            if (companyId != 0)
            {
                devices = devices.Where(m => m.CompanyId == companyId).OrderBy(c => c.Name);
            }

            return devices;
        }

        /// <summary>
        /// Reinstall the devices
        /// </summary>
        /// <param name="devices"></param>
        /// <param name="isAddDevice"></param>
        public void Reinstall(List<IcuDevice> devices, bool isAddDevice = false, List<ReinstallDeviceDetail> onlineDeviceDetails = null)
        {
            try
            {
                foreach (var device in devices)
                {
                    if (device.CompanyId == null)
                    {
                        _logger.LogError("Device can't do it until Device is assigned to the company.");
                        continue;
                    }
                    var actionType = Constants.ActionType.Reinstall;
                    if (isAddDevice)
                    {
                        actionType = Constants.ActionType.Install;
                    }

                    var groupMsgId = Guid.NewGuid().ToString();
                    var processId = TransmitDataModel.GetProcessIdFromDeviceId(onlineDeviceDetails, device.Id);
                    var groupLength = 0;

                    //Calculate number of message to send
                    var maxSplit = Constants.MaxSendIcuUserCount;
                    if (device.DeviceType == (short)DeviceType.ItouchPop)
                    {
                        maxSplit = Constants.MaxSendITouchPopUserCount;
                    }

                    // This variable is a list of access group device to which this device belongs.
                    // So, the devices in this list have same device address. ( Same device )
                    // But, there are some difference properties.
                    // e.g number of users(in access group), timezone position, etc.
                    var deviceAccessGroups = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(device.CompanyId ?? 0, device.Id).ToList();

                    if (deviceAccessGroups.Any())
                    {
                        foreach (var agDevice in deviceAccessGroups)
                        {
                            var totalCardsCount = 0;

                            // Count of User's cards.
                            var users = _unitOfWork.UserRepository.GetAssignUsers(agDevice.Icu.CompanyId ?? 0, agDevice.AccessGroupId).ToList();
                            foreach (User user in users)
                            {
                                var cardsCount = _unitOfWork.CardRepository.GetByUserId(agDevice.Icu.CompanyId ?? 0, user.Id).Count;
                                totalCardsCount += cardsCount;
                            }

                            // Count of Visitor's cards.
                            var visitors = _unitOfWork.VisitRepository.GetByAccessGroupId(agDevice.Icu.CompanyId ?? 0, agDevice.AccessGroupId).ToList();
                            foreach (Visit visitor in visitors)
                            {
                                var cardsCount = _unitOfWork.CardRepository.GetByVisitId(agDevice.Icu.CompanyId ?? 0, visitor.Id).Count;
                                totalCardsCount += cardsCount;
                            }

                            //groupLength += totalMsg;
                            groupLength += totalCardsCount % maxSplit == 0 ? (totalCardsCount / maxSplit) : (totalCardsCount / maxSplit) + 1;
                        }
                    }
                    var timezones = _unitOfWork.TimezoneRepository.GetByCompany(device.CompanyId);
                    // Plus timezone messages
                    groupLength = groupLength + timezones.Count;
                    // plus 1 holiday message + 1 device config + 1 delete user data + 1 delete events + 1 for set time
                    groupLength += 5;
                    // Plus 1 for device messages
                    if (device.DeviceType == (short)DeviceType.ItouchPop) groupLength += 1;

                    if (timezones != null && timezones.Any())
                    {
                        foreach (var timezone in timezones)
                        {
                            _timezoneService.SendUpdateTimezone(timezone, new List<string> { device.DeviceAddress }, groupMsgId: groupMsgId, groupIndex: timezones.IndexOf(timezone), groupLength: groupLength, processId: processId, actionType: actionType);
                        }
                    }

                    var curIndex = timezones.Count;

                    //Save all holiday
                    _holidayService.SendHolidayToIcu(device.DeviceAddress, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength, processId: processId, actionType: actionType);

                    //Save update device config
                    curIndex += 1;
                    SendDeviceConfig(device, Constants.Protocol.UpdateDeviceConfig, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength, processId: processId, actionType: actionType);

                    //Save delete all user
                    curIndex += 1;
                    SendDeviceInstruction(device, Constants.CommandType.DeleteAllUsers, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength, processId: processId, actionType: actionType);

                    //Save delete all event
                    curIndex += 1;
                    SendDeviceInstruction(device, Constants.CommandType.DeleteAllEvents, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength, processId: processId, actionType: actionType);

                    //Save device message for iTouch device
                    if (device.DeviceType == (short)DeviceType.ItouchPop)
                    {
                        curIndex += 1;
                        _deviceMessageService.SendDeviceMessage(new List<IcuDevice> { device }, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength, processId: processId, actionType: actionType);
                    }

                    //Save assign user

                    // This variable("remainCount") is for making updateFlag as 1.
                    // If this device belongs to multiple access group, the API should send all users from each access group to the device.
                    // So, when API send users from the last access group of list, API makes updateFlag as 1.
                    var remainCount = deviceAccessGroups.Count();

                    var processRange = (decimal)Constants.LongProgressPercentage.ReinstallStep1 / (decimal)remainCount;
                    curIndex += 1;
                    if (deviceAccessGroups.Any())
                    {
                        foreach (var deviceAG in deviceAccessGroups)
                        {
                            var totalMsg = 0;

                            var users = _unitOfWork.UserRepository.GetAssignUsers(deviceAG.Icu.CompanyId ?? 0, deviceAG.AccessGroupId).ToList();
                            foreach (User user in users)
                            {
                                var cardsCount = _unitOfWork.CardRepository.GetByUserId(deviceAG.Icu.CompanyId ?? 0, user.Id).Count;
                                totalMsg += (cardsCount + maxSplit - 1) / maxSplit;
                            }

                            var visitors = _unitOfWork.VisitRepository.GetByAccessGroupId(deviceAG.Icu.CompanyId ?? 0, deviceAG.AccessGroupId).ToList();
                            foreach (Visit visitor in visitors)
                            {
                                var cardsCount = _unitOfWork.CardRepository.GetByVisitId(deviceAG.Icu.CompanyId ?? 0, visitor.Id).Count;
                                totalMsg += (cardsCount + maxSplit - 1) / maxSplit;
                            }

                            _accessGroupService.SendAddOrDeleteUser(deviceAG, users, isAddUser: true, groupMsgId: groupMsgId, groupIndex: curIndex,
                                groupLength: groupLength, processId: processId, actionType: actionType, remainCount: --remainCount, progressRange: processRange);

                            curIndex += (totalMsg + maxSplit - 1) / maxSplit;
                        }
                    }

                    //Save settime
                    SendDeviceInstruction(device, Constants.CommandType.SetTime, groupMsgId, groupIndex: groupLength - 1, groupLength: groupLength, processId: processId, actionType: actionType);

                    // Save System Log
                    if (!isAddDevice)
                    {
                        // This case(!isAddDevice) is for reinstall, not install.
                        var content = DeviceResource.msgReinstallDeviceSetting;
                        List<string> details = new List<string>()
                        {
                            $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}",
                            $"{DeviceResource.lblDoorName} : {device.Name}"
                        };
                        var contentsDetails = string.Join("\n", details);

                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.Reinstall,
                            content, contentsDetails, null, device.CompanyId);

                        _unitOfWork.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}{Environment.NewLine}{ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        /// Copy the devices
        /// </summary>
        /// <param name="deviceCopy"></param>
        /// <param name="devices"></param>
        public void CopyDevices(IcuDevice deviceCopy, List<IcuDevice> devices)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var device in devices)
                        {
                            // TODO update device attribute
                            device.DeviceType = deviceCopy.DeviceType;
                            device.VerifyMode = deviceCopy.VerifyMode;
                            device.BackupPeriod = deviceCopy.BackupPeriod;

                            if (deviceCopy.Company != null && deviceCopy.CompanyId != null)
                            {
                                device.Company = deviceCopy.Company;
                                device.CompanyId = deviceCopy.CompanyId;
                            }
                            device.BuildingId = deviceCopy.BuildingId;
                            device.ActiveTzId = deviceCopy.ActiveTzId;
                            device.PassageTzId = deviceCopy.PassageTzId;

                            device.OperationType = deviceCopy.OperationType;

                            device.RoleReader0 = deviceCopy.RoleReader0;
                            device.RoleReader1 = deviceCopy.RoleReader1;

                            device.LedReader0 = deviceCopy.LedReader0;
                            device.LedReader1 = deviceCopy.LedReader1;

                            device.BuzzerReader0 = deviceCopy.BuzzerReader0;
                            device.BuzzerReader1 = deviceCopy.BuzzerReader1;

                            device.UseCardReader = deviceCopy.UseCardReader;

                            device.DeviceBuzzer = deviceCopy.DeviceBuzzer;

                            device.SensorType = deviceCopy.SensorType;
                            device.OpenDuration = deviceCopy.OpenDuration;
                            device.SensorDuration = deviceCopy.SensorDuration;
                            device.SensorAlarm = deviceCopy.SensorAlarm;
                            device.CloseReverseLockFlag = deviceCopy.CloseReverseLockFlag;
                            device.PassbackRule = deviceCopy.PassbackRule;

                            device.MPRCount = deviceCopy.MPRCount;
                            device.MPRInterval = deviceCopy.MPRInterval;

                            device.Status = deviceCopy.Status;

                            _unitOfWork.IcuDeviceRepository.Update(device);

                            //Save system log
                            var content = string.Format(DeviceResource.msgCopyDeviceSetting, deviceCopy.Name, device.Name);

                            var contentsDetails = $"{DeviceResource.lblDoorName} : {deviceCopy.Name} ({DeviceResource.lblDeviceAddress} : {deviceCopy.DeviceAddress})\n" +
                                                $"{DeviceResource.lblDoorName} : {device.Name} ({DeviceResource.lblDeviceAddress} : {device.DeviceAddress})";

                            _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.CopyDoorSetting,
                                content, contentsDetails, null, device.CompanyId);

                            _unitOfWork.Save();
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            foreach (var device in devices)
            {
                string msgId = SendDeviceConfig(device, Constants.Protocol.UpdateDeviceConfig);
            }
        }

        /// <summary>
        /// Send to notification to fronend
        /// </summary>
        public int SendDisconnectDeviceNotification(string deviceAddress)
        {
            var device = _unitOfWork.IcuDeviceRepository.GetDeviceByAddress(deviceAddress);

            if (device != null && device.ConnectionStatus == (short)IcuStatus.Disconneted)
            {
                _notificationService.SendMessage(Constants.MessageType.Error,
                    Constants.NotificationType.SendDeviceInstructionError, _httpContext.User.GetUsername(),
                    string.Format(MessageResource.msgDeviceDisconnected, device.DeviceAddress), device.Company == null ? "" : device.Company.Code);
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Get device type list
        /// </summary>
        /// <returns></returns>
        public DeviceTypeList GetListDeviceType(List<IcuDevice> devices)
        {
            DeviceTypeModel listIcuDeviceTypeModel = new DeviceTypeModel();
            DeviceTypeModel listITouchDeviceTypeModel = new DeviceTypeModel();

            var listDeviceType = new List<DeviceTypeModel>();

            FileDetail icuMainType = null,
                    iTouchMainType = null;

            foreach (var device in devices)
            {
                // When device type is icu300N
                if (device.DeviceType == (short)DeviceType.Icu300N)
                {
                    listIcuDeviceTypeModel.Name = ((DeviceType)device.DeviceType).GetDescription();
                    if (icuMainType == null)
                    {
                        icuMainType = new FileDetail
                        {
                            //IcuIds = icuIds,
                            Target = IcuDeviceTarget.Icu300N.GetDescription(),
                            Remark = "ICU firmware(.bin)"
                        };
                        icuMainType.IcuIds.Add(device.Id);
                        listIcuDeviceTypeModel.FileList.Add(icuMainType);
                    }
                    else
                    {
                        var indexOf = listIcuDeviceTypeModel.FileList.IndexOf(listIcuDeviceTypeModel.FileList.Find(x =>
                            x.Target == IcuDeviceTarget.Icu300N.GetDescription()));
                        if (!listIcuDeviceTypeModel.FileList[indexOf].IcuIds.Contains(device.Id))
                        {
                            listIcuDeviceTypeModel.FileList[indexOf].IcuIds.Add(device.Id);
                        }
                    }

                    var readerVersion0 = !string.IsNullOrEmpty(device.VersionReader0) ? device.VersionReader0.Split("_")[0] : string.Empty;

                    // reader0
                    if (!string.IsNullOrEmpty(readerVersion0))
                    {
                        var filedetail = listIcuDeviceTypeModel.FileList.Find(x =>
                            x.Target == device.VersionReader0);
                        if (filedetail == null)
                        {
                            var newFileDetail = new FileDetail
                            {
                                Target = readerVersion0,
                                Remark = "Reader firmware(.bin)"
                            };
                            newFileDetail.IcuIds.Add(device.Id);
                            listIcuDeviceTypeModel.FileList.Add(newFileDetail);
                        }
                        else
                        {
                            var indexOf = listIcuDeviceTypeModel.FileList.IndexOf(filedetail);
                            if (!listIcuDeviceTypeModel.FileList[indexOf].IcuIds.Contains(device.Id))
                            {
                                listIcuDeviceTypeModel.FileList[indexOf].IcuIds.Add(device.Id);
                            }
                        }
                    }

                    var readerVersion1 = !string.IsNullOrEmpty(device.VersionReader1) ? device.VersionReader1.Split("_")[0] : string.Empty;

                    // reader1
                    // If reader1 is same as reader0, don't send the reader1 to list
                    if (!string.IsNullOrEmpty(readerVersion1) && !readerVersion1.Equals(readerVersion0))
                    {
                        var outCardReader = listIcuDeviceTypeModel.FileList.Find(x =>
                            x.Target == device.VersionReader1);
                        if (outCardReader == null)
                        {
                            var newOutReader = new FileDetail
                            {
                                Target = readerVersion1,
                                Remark = "Reader firmware(.bin)"
                            };
                            newOutReader.IcuIds.Add(device.Id);
                            listIcuDeviceTypeModel.FileList.Add(newOutReader);
                        }
                        else
                        {
                            var indexOf = listIcuDeviceTypeModel.FileList.IndexOf(outCardReader);
                            if (!listIcuDeviceTypeModel.FileList[indexOf].IcuIds.Contains(device.Id))
                            {
                                listIcuDeviceTypeModel.FileList[indexOf].IcuIds.Add(device.Id);
                            }
                        }
                    }
                }
                // When device type is iTouchPop
                else if (device.DeviceType == (short)DeviceType.ItouchPop)
                {
                    listITouchDeviceTypeModel.Name = ((DeviceType)device.DeviceType).GetDescription();

                    // main firmware ( APK )
                    if (iTouchMainType == null)
                    {
                        iTouchMainType = new FileDetail
                        {
                            Target = ItouchPopDeviceTarget.ItouchPop2A.GetDescription(),
                            Remark = "ITouch firmware(.apk)"
                        };
                        iTouchMainType.IcuIds.Add(device.Id);
                        listITouchDeviceTypeModel.FileList.Add(iTouchMainType);
                    }
                    else
                    {
                        var indexOf = listITouchDeviceTypeModel.FileList.IndexOf(listITouchDeviceTypeModel.FileList.Find(x =>
                            x.Target == ItouchPopDeviceTarget.ItouchPop2A.GetDescription()));
                        if (!listITouchDeviceTypeModel.FileList[indexOf].IcuIds.Contains(device.Id))
                        {
                            listITouchDeviceTypeModel.FileList[indexOf].IcuIds.Add(device.Id);
                        }
                    }

                    // RF module
                    if (!string.IsNullOrEmpty(device.NfcModuleVersion))
                    {
                        var nfcFileType = listITouchDeviceTypeModel.FileList.Find(x =>
                            x.Target == device.NfcModuleVersion);
                        if (nfcFileType == null)
                        {
                            var nfcAbcm = new FileDetail
                            {
                                Target = ItouchPopDeviceTarget.Abcm.GetDescription(),
                                Remark = "RF firmware(.bin)"
                            };
                            nfcAbcm.IcuIds.Add(device.Id);
                            listITouchDeviceTypeModel.FileList.Add(nfcAbcm);
                        }
                        else
                        {
                            var indexOf = listITouchDeviceTypeModel.FileList.IndexOf(nfcFileType);
                            if (!listITouchDeviceTypeModel.FileList[indexOf].IcuIds.Contains(device.Id))
                            {
                                listITouchDeviceTypeModel.FileList[indexOf].IcuIds.Add(device.Id);
                            }
                        }
                    }

                    // Extra
                    if (!string.IsNullOrEmpty(device.ExtraVersion))
                    {
                        var extraAbcm = listITouchDeviceTypeModel.FileList.Find(x =>
                            x.Target == device.ExtraVersion);

                        if (extraAbcm == null)
                        {
                            extraAbcm = new FileDetail
                            {
                                Target = device.ExtraVersion,
                                Remark = "Some description"
                            };
                            extraAbcm.IcuIds.Add(device.Id);
                            listITouchDeviceTypeModel.FileList.Add(extraAbcm);
                        }
                        else
                        {
                            var indexOf = listITouchDeviceTypeModel.FileList.IndexOf(extraAbcm);
                            if (!listITouchDeviceTypeModel.FileList[indexOf].IcuIds.Contains(device.Id))
                            {
                                listITouchDeviceTypeModel.FileList[indexOf].IcuIds.Add(device.Id);
                            }
                        }
                    }
                }
            }
            if (listIcuDeviceTypeModel.FileList.Any())
            {
                listDeviceType.Add(listIcuDeviceTypeModel);
            }

            if (listITouchDeviceTypeModel.FileList.Any())
            {
                listDeviceType.Add(listITouchDeviceTypeModel);
            }
            return new DeviceTypeList { DeviceTypes = listDeviceType };
        }

        /// <summary>
        /// Upload data
        /// </summary>
        /// <param name="files"></param>
        public void UploadFile(IFormFileCollection files)
        {
            var undefinedChar = "_";

            // Alignment and Refinement
            List<string> sortedFileList = new List<string>();
            foreach (var file in files)
            {
                sortedFileList.Add(file.Name);
            }

            sortedFileList.Sort();

            // Making a dictionary of all device that need to update. the schema look like below
            // {
            //"DEVICE_ID_1": [
            //                   {
            //                       "target":"target1",
            //                       "file": file1
            //                       "progressId": progressId1
            //                   },
            //                   {
            //                       "target":"target2",
            //                       "file": file2
            //                       "progressId": progressId2
            //                   }
            //               ]
            // }
            var targetFiles = new Dictionary<string, IFormFile>();
            var updatingDevices = new Dictionary<string, List<Dictionary<string, object>>>();

            foreach (var sortedFile in sortedFileList)
            {
                foreach (var file in files)
                {
                    if (file.Name.Equals(sortedFile))
                    {
                        var names = file.Name.Split("__");
                        if (names.Length != 2)
                        {
                            _logger.LogError("Name file is invalid");
                            return;
                        }
                        var target = names[0];
                        targetFiles[target] = file;

                        var deviceList = names[1].Split("&&");

                        foreach (var item in deviceList)
                        {
                            var deviceId = item.Split("::")[0];
                            var processId = item.Split("::")[1];

                            var deviceItem = new Dictionary<string, object>();
                            deviceItem["target"] = target;
                            deviceItem["processId"] = processId;
                            deviceItem["file"] = file;

                            var maxSplitSize = Constants.Settings.MaxSizeSendToIcu;
                            if (target.Equals(ItouchPopDeviceTarget.ItouchPop2A.GetDescription()) || (ItouchPopDeviceTarget.Abcm.GetDescription().ToLower().Equals(target.ToLower())))
                                maxSplitSize = 16 * Constants.Settings.MaxSizeSendToIcu;//65536

                            var datas = FileHelpers.SplitFile(file, maxSplitSize);
                            deviceItem["totalMessages"] = datas.Count;

                            if (updatingDevices.ContainsKey(deviceId))
                            {
                                updatingDevices[deviceId].Add(deviceItem);
                            }
                            else
                            {
                                updatingDevices[deviceId] = new List<Dictionary<string, object>>();
                                updatingDevices[deviceId].Add(deviceItem);
                            }
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, List<Dictionary<string, object>>> entry in updatingDevices)
            {
                // We run update for each device
                var device = _unitOfWork.IcuDeviceRepository.GetByIcuId(Convert.ToInt32(entry.Key));
                foreach (var targetFile in entry.Value)
                {
                    var groupMsgId = Guid.NewGuid().ToString();
                    var target = (string)targetFile["target"];
                    var updateFile = (IFormFile)targetFile["file"];
                    var processId = (string)targetFile["processId"];
                    // When device is ICU-300N
                    if (device.DeviceType == (short)DeviceType.Icu300N)
                    {
                        // Make protocol - Main firmware file
                        if (target.Equals(DeviceType.Icu300N.GetDescription()))
                        {
                            var nameCompare = DeviceType.Icu300N.GetDescription() + undefinedChar;
                            if (updateFile.FileName.Contains(nameCompare))
                            {
                                MakeProtocolFileTransfer(file: updateFile,
                                    device: device,
                                    target: target,
                                    fwType: IcuFileType.MainFirmware.GetDescription(),
                                    groupMsgId: groupMsgId,
                                    startGroupIndex: 0,
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId);
                                SendDeviceInstruction(device: device,
                                    command: Constants.CommandType.UpdateFirmware,
                                    groupMsgId: groupMsgId,
                                    groupIndex: (int)targetFile["totalMessages"],
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId,
                                    actionType: Constants.ActionType.UpdateDevice,
                                    target: target,
                                    fwType: IcuFileType.MainFirmware.GetDescription());
                            }
                            else
                            {
                                _notificationService.SendMessage(messageType: Constants.MessageType.Error,
                                    notificationType: Constants.NotificationType.FileTransferError,
                                    msgUser: _httpContext.User.GetUsername(),
                                    msgBody: string.Format(MessageResource.msgFileNameError, nameCompare),
                                    companyCode: _httpContext.User.GetCompanyId() == 0 ? "" : device.Company.Code);
                            }
                        }

                        // Make protocol - Reader0 firmware file
                        var cardReader0 = !string.IsNullOrEmpty(device.VersionReader0) ? device.VersionReader0.Split("_")[0] : string.Empty;
                        if (cardReader0.Equals(target))
                        {
                            var nameCompare = target.Split("-").Last() + undefinedChar;
                            if (updateFile.FileName.Contains(nameCompare))
                            {
                                MakeProtocolFileTransfer(file: updateFile,
                                    device: device,
                                    target: target,
                                    fwType: device.RoleReader0 == (short)RoleRules.In ? IcuFileType.InReader.GetDescription() : IcuFileType.OutReader.GetDescription(),
                                    groupMsgId: groupMsgId,
                                    startGroupIndex: 0,
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId);
                                SendDeviceInstruction(device: device,
                                    command: Constants.CommandType.UpdateFirmware,
                                    groupMsgId: groupMsgId,
                                    groupIndex: (int)targetFile["totalMessages"],
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId,
                                    actionType: Constants.ActionType.UpdateDevice,
                                    target: target,
                                    fwType: device.RoleReader0 == (short)RoleRules.In ? IcuFileType.InReader.GetDescription() : IcuFileType.OutReader.GetDescription());
                            }
                            else
                            {
                                _notificationService.SendMessage(messageType: Constants.MessageType.Error,
                                    notificationType: Constants.NotificationType.FileTransferError,
                                    msgUser: _httpContext.User.GetUsername(),
                                    msgBody: string.Format(MessageResource.msgFileNameError, nameCompare),
                                    companyCode: _httpContext.User.GetCompanyId() == 0 ? "" : device.Company.Code);
                            }
                        }

                        // Make protocol - Reader1 firmware file
                        var cardReader1 = !string.IsNullOrEmpty(device.VersionReader1) ? device.VersionReader1.Split("_")[0] : string.Empty;
                        if (cardReader1.Equals(target, StringComparison.OrdinalIgnoreCase))
                        {
                            var nameCompare = target.Split("-").Last() + undefinedChar;
                            if (updateFile.FileName.Contains(nameCompare))
                            {
                                MakeProtocolFileTransfer(file: updateFile,
                                    device: device,
                                    target: target,
                                    fwType: device.RoleReader1 == (short)RoleRules.In ? IcuFileType.InReader.GetDescription() : IcuFileType.OutReader.GetDescription(),
                                    groupMsgId: groupMsgId,
                                    startGroupIndex: 0,
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId);
                                SendDeviceInstruction(device: device,
                                    command: Constants.CommandType.UpdateFirmware,
                                    groupMsgId: groupMsgId,
                                    groupIndex: (int)targetFile["totalMessages"],
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId,
                                    actionType: Constants.ActionType.UpdateDevice,
                                    target: target,
                                    fwType: device.RoleReader1 == (short)RoleRules.In ? IcuFileType.InReader.GetDescription() : IcuFileType.OutReader.GetDescription());
                            }
                            else
                            {
                                _notificationService.SendMessage(messageType: Constants.MessageType.Error,
                                    notificationType: Constants.NotificationType.FileTransferError,
                                    msgUser: _httpContext.User.GetUsername(),
                                    msgBody: string.Format(MessageResource.msgFileNameError, nameCompare),
                                    companyCode: _httpContext.User.GetCompanyId() == 0 ? "" : device.Company.Code);
                            }
                        }
                    }
                    // When device is ITouchPop2A
                    else if (device.DeviceType == (short)DeviceType.ItouchPop)
                    {
                        if (target.Equals(ItouchPopDeviceTarget.ItouchPop2A.GetDescription()))
                        {
                            var nameCompare = DeviceType.ItouchPop.GetDescription() + undefinedChar;
                            if (updateFile.FileName.Contains(nameCompare))
                            {
                                MakeProtocolFileTransfer(file: updateFile,
                                    device: device,
                                    target: target,
                                    fwType: ItouchPopDeviceTarget.ItouchPop2A.GetDescription(),
                                    groupMsgId: groupMsgId,
                                    startGroupIndex: 0,
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId);
                                SendDeviceInstruction(device: device,
                                    command: Constants.CommandType.UpdateFirmware,
                                    groupMsgId: groupMsgId,
                                    groupIndex: (int)targetFile["totalMessages"],
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId,
                                    actionType: Constants.ActionType.UpdateDevice,
                                    target: target,
                                    fwType: ItouchPopFileType.MainFirmware.GetDescription());
                            }
                            else
                            {
                                _notificationService.SendMessage(messageType: Constants.MessageType.Error,
                                    notificationType: Constants.NotificationType.FileTransferError,
                                    msgUser: _httpContext.User.GetUsername(),
                                    msgBody: string.Format(MessageResource.msgFileNameError, nameCompare),
                                    companyCode: _httpContext.User.GetCompanyId() == 0 ? "" : device.Company.Code);
                            }
                        }

                        if (ItouchPopDeviceTarget.Abcm.GetDescription().ToLower().Equals(target.ToLower()))
                        {
                            var nameCompare = target.Split("-").Last() + undefinedChar;
                            if (updateFile.FileName.Contains(nameCompare))
                            {
                                MakeProtocolFileTransfer(file: updateFile,
                                    device: device,
                                    target: target,
                                    fwType: ItouchPopDeviceTarget.Abcm.GetDescription(),
                                    groupMsgId: groupMsgId,
                                    startGroupIndex: 0,
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId);
                                SendDeviceInstruction(device: device,
                                    command: Constants.CommandType.UpdateFirmware,
                                    groupMsgId: groupMsgId,
                                    groupIndex: (int)targetFile["totalMessages"],
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId,
                                    actionType: Constants.ActionType.UpdateDevice,
                                    target: target,
                                    fwType: ItouchPopFileType.NfcModule.GetDescription());
                            }
                            else
                            {
                                _notificationService.SendMessage(messageType: Constants.MessageType.Error,
                                    notificationType: Constants.NotificationType.FileTransferError,
                                    msgUser: _httpContext.User.GetUsername(),
                                    msgBody: string.Format(MessageResource.msgFileNameError, nameCompare),
                                    companyCode: _httpContext.User.GetCompanyId() == 0 ? "" : device.Company.Code);
                            }
                        }

                        var extraVersion = !string.IsNullOrEmpty(device.ExtraVersion) ? device.ExtraVersion.Split("_")[0] : string.Empty;
                        if (extraVersion.Equals(target))
                        {
                            var nameCompare = target.Split("-").Last() + undefinedChar;
                            if (updateFile.FileName.Contains(nameCompare))
                            {
                                MakeProtocolFileTransfer(file: updateFile,
                                    device: device,
                                    target: target,
                                    fwType: ItouchPopDeviceTarget.SoundTrack01.GetDescription(),
                                    groupMsgId: groupMsgId,
                                    startGroupIndex: 0,
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId);
                                SendDeviceInstruction(device: device,
                                    command: Constants.CommandType.UpdateFirmware,
                                    groupMsgId: groupMsgId,
                                    groupIndex: (int)targetFile["totalMessages"],
                                    groupLength: (int)targetFile["totalMessages"] + 1,
                                    processId: processId,
                                    actionType: Constants.ActionType.UpdateDevice,
                                    target: target,
                                    fwType: ItouchPopFileType.Extra.GetDescription());
                            }
                            else
                            {
                                _notificationService.SendMessage(messageType: Constants.MessageType.Error,
                                    notificationType: Constants.NotificationType.FileTransferError,
                                    msgUser: _httpContext.User.GetUsername(),
                                    msgBody: string.Format(MessageResource.msgFileNameError, nameCompare),
                                    companyCode: _httpContext.User.GetCompanyId() == 0 ? "" : device.Company.Code);
                            }
                        }
                    }

                    // [Edward] 2020.03.04
                    // API should sends 2 commands automatically after FW updating.
                    // 1. Send current time
                    // 2. Load device info

                    var tempGroupMsgId = Guid.NewGuid().ToString();
                    // Send settime message
                    SendDeviceInstruction(device, Constants.CommandType.SetTime, tempGroupMsgId, groupIndex: 0, groupLength: 2);
                    // Send Device Info
                    SendDeviceInfo(device.DeviceAddress, groupMsgId: tempGroupMsgId, groupIndex: 1, groupLength: 2);


                    //Save system log
                    var content = string.Format(DeviceResource.msgFirmwareUpdate);
                    var contentsDetails = $"{DeviceResource.lblDeviceAddress} : {device.DeviceAddress}" +
                                        $"\n{DeviceResource.lblDoorName} : {device.Name}" +
                                        $"\n{CommonResource.lblFileName} : {FileHelpers.GetFileNameWithoutExtension(updateFile)}";

                    _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceUpdate, ActionLogType.UpdateDoor, content, contentsDetails, null, device.CompanyId);
                    _unitOfWork.Save();
                }


            }
        }

        /// <summary>
        /// Make protocol for file transfer
        /// </summary>
        /// <param name="file"></param>
        /// <param name="device"></param>
        /// <param name="target"></param>
        /// <param name="progressId"></param>
        /// <param name="fwType"></param>
        private int MakeProtocolFileTransfer(IFormFile file, IcuDevice device, string target, string fwType, string groupMsgId, int startGroupIndex, int groupLength, string processId)
        {
            var routingKey = Constants.RabbitMq.FileTranferTopic + "." + device.DeviceAddress;

            if (file.Length <= 0) return 0;

            int progressIndex = 0;

            var maxSplitSize = Constants.Settings.MaxSizeSendToIcu;

            if (target.Equals(ItouchPopDeviceTarget.ItouchPop2A.GetDescription()) || (ItouchPopDeviceTarget.Abcm.GetDescription().ToLower().Equals(target.ToLower())))
                maxSplitSize = 16 * Constants.Settings.MaxSizeSendToIcu;//65536

            var datas = FileHelpers.SplitFile(file, maxSplitSize);

            if (datas.Count > 0)
            {
                var firstMessage = "";
                for (var i = 0; i < datas.Count; i++)
                {
                    var fileUploadDetail = new DeviceUploadFileDetail
                    {
                        FrameIndex = i,
                        TotalIndex = datas.Count,
                        Extension = FileHelpers.GetFileExtension(file),

                        Target = target.Equals(ItouchPopDeviceTarget.ItouchPop2A.GetDescription()) ||
                                 (ItouchPopDeviceTarget.Abcm.GetDescription().ToLower().Equals(target.ToLower()))
                                         ? FileHelpers.GetFileNameWithoutExtension(file) : target,
                        FwType = fwType,
                        Data = datas[i]
                    };
                    var protocolData = new DeviceUploadFileProtocolData
                    {
                        MsgId = Guid.NewGuid().ToString(),
                        Sender = _httpContext.User.GetUsername(),
                        Type = Constants.Protocol.FileDownLoad,
                        Data = fileUploadDetail
                    };
                    var message = protocolData.ToString();
                    if (i == 0)
                    {
                        firstMessage = message;
                    }

                    _queueService.SendGroupMessage(deviceAddress: device.DeviceAddress,
                                                    protocolData.MsgId,
                                                    message: message,
                                                    topic: routingKey,
                                                    groupMsgId: groupMsgId,
                                                    groupIndex: startGroupIndex + i,
                                                    groupLength: groupLength,
                                                    processId: processId,
                                                    actionType: Constants.ActionType.UpdateDevice);
                    progressIndex++;
                }
            }

            return progressIndex;
        }

        /// <summary>
        /// Export accessible users
        /// </summary>
        /// <param name="device"></param>
        /// <param name="type"></param>
        /// <param name="search"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <returns></returns>
        public byte[] ExportAccessibleUsers(IcuDevice device, string type, string search, out int totalRecords,
            out int recordsFiltered, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accessibleUserModels = FilterAccessibleUserDataWithOrder(device, search, sortColumn, sortDirection, out totalRecords,
                out recordsFiltered).ToList();

            foreach (var user in accessibleUserModels)
            {
                user.CardList = GetCardModelsByUserId(user.Id);

                if (user.CardList.Any())
                {
                    var cardIdList = user.CardList.Select(m => m.CardId).ToList();

                    //user.CardId = user.CardList.FirstOrDefault().CardId;
                    user.CardId = string.Join(", ", cardIdList);
                    user.CardStatus = ((CardStatus)user.CardList.FirstOrDefault().CardStatus).GetDescription();
                }
            }

            var fileByte = type == Constants.Excel
                ? ExportAccessibleUsersWithExcel(accessibleUserModels)
                : ExportAccessibleUsersWithTxt(accessibleUserModels);

            //Save system log
            var companyId = device.CompanyId;
            var content = string.Format(DeviceResource.msgExportAccessibleUserList);
            var contentsDetails = $"{DeviceResource.lblDoorName} : {device.Name}" +
                $"\n{AccountResource.lblUsername} : {_httpContext.User.GetUsername()}";

            _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.Report, ActionLogType.Export, content, contentsDetails, null, companyId);
            _unitOfWork.Save();

            return fileByte;
        }

        /// <summary>
        /// Filter and order data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        internal IQueryable<AccessibleUserModel> FilterAccessibleUserDataWithOrder(IcuDevice icuDevice, string filter, int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered)
        {
            var companyId = icuDevice.CompanyId ?? 0;
            var accessGroupIds = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(companyId, icuDevice.Id).Select(x => x.AccessGroupId).ToList();
            var data = _unitOfWork.UserRepository.GetAssignUsersByAccessGroupIds(companyId, accessGroupIds);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.FirstName.ToLower().Contains(filter) ||
                    x.LastName.ToLower().Contains(filter) ||
                    x.Department.DepartName.ToLower().Contains(filter) ||
                    string.IsNullOrEmpty(x.EmpNumber) || x.EmpNumber.ToLower().Contains(filter) ||
                    x.ExpiredDate.ToString().ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.RegisteredUser.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.RegisteredUser[sortColumn]} {sortDirection}");
            return data.AsEnumerable<User>().Select(Mapper.Map<AccessibleUserModel>).AsQueryable();
        }

        /// <summary>
        /// Export Accessible doors data to txt file
        /// </summary>
        /// <param name="accessibleDoorModels"></param>
        /// <returns></returns>
        public byte[] ExportAccessibleUsersWithExcel(List<AccessibleUserModel> accessibleUserModels)
        {
            byte[] result;

            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(DeviceResource.lblAccessibleUsers); //Worksheet name

                //First add the headers for Accessible Door sheet
                for (var i = 0; i < _headerForAccessibleUser.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _headerForAccessibleUser[i];
                }

                var recordIndex = 2;
                foreach (var accessibleUserModel in accessibleUserModels)
                {
                    //For the Accessible Door sheet
                    var colIndex = 1;
                    worksheet.Cells[recordIndex, colIndex++].Value = recordIndex - 1;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleUserModel.CardId;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleUserModel.Name;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleUserModel.Department;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleUserModel.EmployeeNumber;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleUserModel.ExpiredDate;
                    worksheet.Cells[recordIndex, colIndex].Value = accessibleUserModel.CardStatus;

                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }
            return result;
        }

        /// <summary>
        /// Export Accessible doors data to txt file
        /// </summary>
        /// <param name="accessibleDoorModels"></param>
        /// <returns></returns>
        public byte[] ExportAccessibleUsersWithTxt(List<AccessibleUserModel> accessibleUserModels)
        {
            var cnt = 1;
            var accessibleUsers = accessibleUserModels.Select(x => new object[]
            {
                x.Id = cnt++,
                x.CardId,
                x.Name,
                x.Department,
                x.EmployeeNumber,
                x.ExpiredDate,
                x.CardStatus
                }).ToList();

            var accessibleUserTxt = new StringBuilder();
            accessibleUsers.ForEach(line => { accessibleUserTxt.AppendLine(string.Join(",", line)); });

            byte[] buffer = Encoding.UTF8.GetBytes($"{string.Join(",", _headerForAccessibleUser)}\r\n{accessibleUserTxt}");
            return buffer;
        }

        /// <summary>
        /// Send load device setting
        /// </summary>
        /// <param name="device"></param>
        /// <param name="msgId"></param>
        public CheckDeviceInfoDetail LoadDeviceSetting(IcuDevice device, string msgId)
        {
            MakeDeviceSettingProtocol(device, msgId);
            var deviceSettingModel = Mapper.Map<CheckDeviceSettingModel>(device);

            if (device.ActiveTzId != null)
            {
                deviceSettingModel.ActiveTimezone = _unitOfWork.TimezoneRepository.GetById(device.ActiveTzId ?? 0).Name;
            }

            if (device.PassageTzId != null)
            {
                deviceSettingModel.PassageTimezone = _unitOfWork.TimezoneRepository.GetById(device.PassageTzId ?? 0).Name;
            }

            deviceSettingModel.CardCount = GetCardCountByDeviceId(device.Id);

            var deviceInfo = new CheckDeviceInfoDetail
            {
                MsgId = msgId,
                Data = deviceSettingModel
            };
            return deviceInfo;
        }

        /// <summary>
        /// Make protoccol setting
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        private void MakeDeviceSettingProtocol(IcuDevice device, string msgId)
        {
            var deviceSettingProtocol = new DeviceSettingProtocolData
            {
                MsgId = msgId,
                Type = Constants.Protocol.LoadDeviceSetting,
                Data = new DeviceSettingProtocolDataHeader()
            };
            var message = deviceSettingProtocol.ToString();
            var routingKey = Constants.RabbitMq.DeviceSettingTopic + "." + device.DeviceAddress;
            _queueService.Publish(routingKey, message);
        }

        /// <summary>
        /// Send load holiday
        /// </summary>
        /// <param name="device"></param>
        public TimezoneInfoModel LoadTimezone(IcuDevice device, string msgId)
        {
            var timezones = _unitOfWork.TimezoneRepository.GetByCompany(device.CompanyId);

            MakeTimezoneProtocolData(device, msgId, timezones);
            var tzDetails = new List<TimezoneDetailModel>();
            if (timezones.Any())
            {
                foreach (var tz in timezones)
                {
                    var timezoneDetail = Mapper.Map<TimezoneDetailModel>(tz);
                    if (device.ActiveTz.Position == timezoneDetail.Position)
                    {
                        timezoneDetail.Type = TimezoneType.Active.GetDescription();
                    }
                    else if (device.PassageTz.Position == timezoneDetail.Position)
                    {
                        timezoneDetail.Type = TimezoneType.Passage.GetDescription();
                    }
                    else
                    {
                        timezoneDetail.Type = TimezoneType.Normal.GetDescription();
                    }
                    tzDetails.Add(timezoneDetail);
                }
            }

            var timezoneInfo = new TimezoneInfoModel
            {
                MsgId = msgId,
                Timezones = tzDetails
            };
            return timezoneInfo;
        }

        /// <summary>
        /// Make timezone protocol data
        /// </summary>
        /// <param name="device"></param>
        /// <param name="timezones"></param>
        private void MakeTimezoneProtocolData(IcuDevice device, string msgId, List<Timezone> timezones)
        {
            var timezonePositions = timezones.Select(x => x.Position);
            var tzProtocols = timezonePositions
                .Select(position => new TimezoneProtocolDetailData { TimezonePosition = position }).ToList();

            var loadTimezoneProtocol = new TimezoneProtocolData
            {
                MsgId = msgId,
                Type = Constants.Protocol.LoadTimezone,
                Data = new TimezoneProtocolHeaderData
                {
                    Total = timezones.Count,
                    Timezone = tzProtocols
                }
            };

            var message = loadTimezoneProtocol.ToString();
            var routingKey = Constants.RabbitMq.LoadTimezoneTopic + "." + device.DeviceAddress;
            _queueService.Publish(routingKey, message);
        }

        /// <summary>
        /// Send load holiday
        /// </summary>
        /// <param name="device"></param>
        public HolidayInfoModel LoadHoliday(IcuDevice device, string msgId)
        {
            MakeHolidayProtocolData(device, msgId);
            var holidayInfo = new HolidayInfoModel { MsgId = msgId };
            var holidays = _holidayService.GetHolidayByCompany(device.CompanyId ?? 0);
            if (holidays.Any())
            {
                var holidayInfos = Mapper.Map<List<HolidayDetail>>(holidays);
                holidayInfo.Data = holidayInfos;
            }

            return holidayInfo;
        }

        /// <summary>
        /// Make holiday protocol data
        /// </summary>
        /// <param name="device"></param>
        private void MakeHolidayProtocolData(IcuDevice device, string msgId)
        {
            var loadHolidayProtocol = new SendLoadHolidayProtocolData
            {
                MsgId = msgId,
                Type = Constants.Protocol.LoadHoliday,
                Data = new SendLoadHolidayHeader()
            };

            var message = loadHolidayProtocol.ToString();
            var routingKey = Constants.RabbitMq.LoadHolidayTopic + "." + device.DeviceAddress;
            _queueService.Publish(routingKey, message);
        }

        /// <summary>
        /// Send load holiday
        /// </summary>
        /// <param name="device"></param>
        public DeviceMsgIdModel LoadUser(IcuDevice device)
        {
            var msgId = MakeLoadUserProtocolData(device);
            return new DeviceMsgIdModel { MessageId = msgId };
        }

        /// <summary>
        /// Make holiday protocol data
        /// </summary>
        /// <param name="device"></param>
        private string MakeLoadUserProtocolData(IcuDevice device)
        {
            var loadUserProtocol = new LoadUserProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.LoadAllUser,
                Data = new LoadUserProtocolHeader()
            };
            var message = loadUserProtocol.ToString();
            var routingKey = Constants.RabbitMq.LoadUserTopic + "." + device.DeviceAddress;
            _queueService.Publish(routingKey, message);
            return loadUserProtocol.MsgId;
        }

        /// <summary>
        /// Export user info
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public byte[] ExportUserSetting(IcuDevice device)
        {
            byte[] result;

            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(DeviceResource.lblDevice); //Worksheet name

                var userInfos = GetUser(device);

                //First add the headers for user sheet
                for (var i = 0; i < _userSettingHeaders.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _userSettingHeaders[i];
                }

                var recordIndex = 2;
                foreach (var user in userInfos)
                {
                    //For the User sheet
                    var colIndex = 1;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.CardId;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.UserName;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.Department;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.EmployeeNumber;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.ExpireDate;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.IssueCount;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.IsMasterCard;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.EffectiveDate;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.CardStatus;
                    worksheet.Cells[recordIndex, colIndex++].Value = user.Password;
                    worksheet.Cells[recordIndex, colIndex].Value = user.Timezone;
                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }
            return result;
        }

        /// <summary>
        /// Export user master card
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public byte[] ExportUserMasterCard(IcuDevice device)
        {
            byte[] result;

            //var package = new ExcelPackage();
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(DeviceResource.lblDevice); //Worksheet name

                var userMasterCard = GetUserMasterCard(device);

                //First add the headers for user sheet
                for (var i = 0; i < _userMasterCardHeaders.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _userMasterCardHeaders[i];
                }

                var recordIndex = 2;
                var index = 1;
                if (userMasterCard.UserMasterCard.Any())
                {
                    foreach (var user in userMasterCard.UserMasterCard)
                    {
                        //For the User sheet
                        var colIndex = 1;
                        worksheet.Cells[recordIndex, colIndex++].Value = index;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.CardId;
                        worksheet.Cells[recordIndex, colIndex].Value = user.UserName;
                        index++;
                        recordIndex++;
                    }
                }

                var userFromIcus = GetUser(device);
                var userMasterCardFromIcus = userFromIcus.Where(x => x.IsMasterCard == 1)
                    .Select(Mapper.Map<UserMasterCardModelDetail>).ToList();
                var recordIndex2 = 2;
                if (userMasterCardFromIcus.Any())
                {
                    foreach (var user in userMasterCardFromIcus)
                    {
                        //For the User sheet
                        var colIndex = 4;
                        worksheet.Cells[recordIndex2, colIndex++].Value = user.CardId;
                        worksheet.Cells[recordIndex2, colIndex].Value = user.UserName;
                        recordIndex2++;
                    }
                }

                result = package.GetAsByteArray();
            }
            return result;
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        private List<SendUserDetail> GetUser(IcuDevice device)
        {
            var userDetails = new List<SendUserDetail>();
            var fileName = FileHelpers.GetLastestFileName(Directory.GetCurrentDirectory(), device.DeviceAddress, Constants.ExportType.LoadAllUser);
            if (File.Exists(fileName))
            {
                var lines = File.ReadAllLines(fileName);
                for (var i = 1; i < lines.Length; i++)
                {
                    var line = lines[i].Split(",");
                    var user = new SendUserDetail
                    {
                        CardId = line[0],
                        UserName = line[1],
                        Department = line[2],
                        EmployeeNumber = line[3],
                        ExpireDate = line[4],
                        IssueCount = Convert.ToInt32(line[5]),
                        IsMasterCard = Convert.ToInt32(line[6]),
                        EffectiveDate = line[7],
                        CardStatus = line[8],
                        Password = line[9],
                        Timezone = line[10]
                    };
                    userDetails.Add(user);
                }
            }

            return userDetails;
        }

        /// <summary>
        /// Get user info
        /// </summary>
        /// <param name="device"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public UserInfoModel GetUserInfo(IcuDevice device, string cardId)
        {
            SendUserDetail userDb = null, userDevice = null;
            var userIcusDetails = GetUser(device);
            var user = _unitOfWork.UserRepository.GetByCardIdIncludeDepartment(device.CompanyId, cardId);
            if (user != null)
            {
                var card = _unitOfWork.CardRepository.GetByUserId(device.CompanyId ?? 0, user.Id).Where(m => m.CardId.Equals(cardId)).FirstOrDefault();

                userDb = Mapper.Map<SendUserDetail>(user);
                userDb.CardId = card?.CardId;
                userDb.IssueCount = card != null ? card.IssueCount : 0;
                userDb.CardStatus = card != null ? ((CardStatus)card.CardStatus).GetDescription() : "";
                var agDevice = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(device.CompanyId ?? 0,
                    device.Id).FirstOrDefault(x => x.AccessGroupId == user.AccessGroupId);
                if (agDevice != null) userDb.Timezone = agDevice.Tz.Name;
            }
            if (userIcusDetails.Any())
            {
                userDevice = userIcusDetails.FirstOrDefault(x => x.CardId == cardId);
            }

            var model = new UserInfoModel
            {
                UserDb = userDb,
                UserDevice = userDevice
            };
            return model;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="device"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public UserInfoByCardIdModel GetUserInfoByCardId(IcuDevice device, string cardId)
        {
            SendUserDetail userDb = null;
            var msgId = MakeLoadUserByCardIdProtocolData(device, cardId);
            var user = _unitOfWork.UserRepository.GetByCardIdIncludeDepartment(device.CompanyId, cardId);
            if (user != null)
            {
                userDb = Mapper.Map<SendUserDetail>(user);
                var agDevice = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(device.CompanyId ?? 0,
                    device.Id).FirstOrDefault(x => x.AccessGroupId == user.AccessGroupId);
                if (agDevice != null) userDb.Timezone = agDevice.Tz.Name;
            }
            var model = new UserInfoByCardIdModel
            {
                MsgId = msgId,
                User = userDb
            };
            return model;
        }

        /// <summary>
        /// Make holiday protocol data
        /// </summary>
        /// <param name="device"></param>
        /// <param name="cardId"></param>
        private string MakeLoadUserByCardIdProtocolData(IcuDevice device, string cardId)
        {
            var loadUserProtocol = new LoadUserByCardIdProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.LoadUser,
                Data = new LoadUserByCardIdProtocolHeader
                {
                    CardId = cardId
                }
            };
            var message = loadUserProtocol.ToString();
            var routingKey = Constants.RabbitMq.LoadUserTopic + "." + device.DeviceAddress;
            _queueService.Publish(routingKey, message);
            return loadUserProtocol.MsgId;
        }

        /// <summary>
        /// Get all transmit data model
        /// </summary>
        /// <returns></returns>
        public TransmitInfoModel GetTransmitAllData()
        {
            var data = EnumHelper.ToEnumList<TransmitType>();
            return new TransmitInfoModel { Data = data };
        }

        /// <summary>
        /// Transmit data
        /// </summary>
        /// <param name="models"></param>
        public void TransmitData(TransmitDataModel models, List<IcuDevice> devices)
        {
            if (models.IsAllDevice)
            {
                devices = _unitOfWork.IcuDeviceRepository.GetDevicesByCompany(_httpContext.User.GetCompanyId());
            }

            if (devices != null && devices.Any())
            {
                foreach (var device in devices)
                {
                    //Save system log
                    var content = $"{DeviceResource.msgTransmitData} : {device.Name}({device.DeviceAddress})";
                    List<string> details = new List<string>();
                    var contentsDetail = "";
                    var groupMsgId = Guid.NewGuid().ToString();

                    var processId = TransmitDataModel.GetProcessIdFromDeviceId(models.Devices, device.Id);
                    var maxSplit = Constants.MaxSendIcuUserCount;
                    if (device.DeviceType == (short)DeviceType.ItouchPop)
                    {
                        maxSplit = Constants.MaxSendITouchPopUserCount;
                    }

                    // Calculate total number of message should be sent (for progress bar)
                    int groupLength = 0;
                    foreach (var transId in models.TransmitIds)
                    {
                        switch (transId)
                        {
                            case (short)TransmitType.CurrentTime:
                                groupLength += 1;
                                break;

                            case (short)TransmitType.DeviceSetting:
                                groupLength += 1;
                                break;

                            case (short)TransmitType.TimezoneSetting:
                                //Save all timezone
                                var timezones = _unitOfWork.TimezoneRepository.GetByCompany(device.CompanyId);
                                groupLength += timezones.Count;
                                break;

                            case (short)TransmitType.HolidaySetting:
                                groupLength += 1;
                                break;

                            case (short)TransmitType.UserInfo:
                                var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(device.CompanyId ?? 0, device.Id).ToList();

                                if (agDevices.Any())
                                {
                                    foreach (var agDevice in agDevices)
                                    {
                                        // Calculate number of messages
                                        //var totalMsg = 0;
                                        var totalCardsCount = 0;
                                        var users = _unitOfWork.UserRepository.GetAssignUsers(agDevice.Icu.CompanyId ?? 0, agDevice.AccessGroupId).ToList();
                                        foreach (User user in users)
                                        {
                                            var cardsCount = _unitOfWork.CardRepository.GetByUserId(agDevice.Icu.CompanyId ?? 0, user.Id).Count;
                                            //totalMsg += (cardsCount + maxSplit - 1) / maxSplit;
                                            totalCardsCount += cardsCount;
                                        }
                                        groupLength += (totalCardsCount / maxSplit) + 1;
                                    }
                                }

                                if (models.IsDeleteAllUser)
                                {
                                    groupLength += 1;
                                }

                                break;

                            case (short)TransmitType.MessageSetting:
                                if (device.DeviceType == (short)DeviceType.ItouchPop)
                                {
                                    groupLength += 1;
                                }
                                break;
                        }
                    }
                    int curIndex = 0;
                    // Build messages list for each device
                    if (models.TransmitIds.Contains((short)TransmitType.CurrentTime))
                    {
                        SendDeviceInstruction(device, Constants.CommandType.SetTime, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength,
                            processId: processId, actionType: Constants.ActionType.TransmitData);
                        curIndex += 1;
                        details.Add(DeviceResource.lblTransmitCurrentTime);
                    }

                    if (models.TransmitIds.Contains((short)TransmitType.DeviceSetting))
                    {
                        SendDeviceConfig(device, Constants.Protocol.UpdateDeviceConfig, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength,
                            processId: processId, actionType: Constants.ActionType.TransmitData);
                        curIndex += 1;
                        details.Add(DeviceResource.lblTransmitDeviceSetting);
                    }

                    if (models.TransmitIds.Contains((short)TransmitType.TimezoneSetting))
                    {
                        //Save all timezone
                        var timezones = _unitOfWork.TimezoneRepository.GetByCompany(device.CompanyId);
                        if (timezones != null && timezones.Any())
                        {
                            foreach (var timezone in timezones)
                            {
                                _timezoneService.SendUpdateTimezone(timezone, new List<string> { device.DeviceAddress }, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength,
                            processId: processId, actionType: Constants.ActionType.TransmitData);
                                curIndex += 1;
                            }
                        }
                        details.Add(DeviceResource.lblTransmitTimezoneSetting);
                    }

                    if (models.TransmitIds.Contains((short)TransmitType.HolidaySetting))
                    {
                        _holidayService.SendHolidayToIcu(device.DeviceAddress, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength,
                            processId: processId, actionType: Constants.ActionType.TransmitData);
                        curIndex += 1;
                        details.Add(DeviceResource.lblTransmitHolidaySetting);
                    }
                    if (models.TransmitIds.Contains((short)TransmitType.UserInfo))
                    {
                        var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(device.CompanyId ?? 0, device.Id).ToList();

                        var remainCount = agDevices.Count() > 0 ? agDevices.Count() : 1;

                        var progressRange = (decimal)Constants.LongProgressPercentage.ReinstallStep1 / (decimal)remainCount;

                        if (models.IsDeleteAllUser)
                        {
                            SendDeviceInstruction(device, Constants.CommandType.DeleteAllUsers, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength,
                            processId: processId, actionType: Constants.ActionType.TransmitData);
                            curIndex += 1;
                        }

                        if (agDevices.Any())
                        {
                            foreach (var agDevice in agDevices)
                            {
                                var users = _unitOfWork.UserRepository.GetAssignUsers(agDevice.Icu.CompanyId ?? 0, agDevice.AccessGroupId).ToList();
                                var totalMsg = 0;
                                foreach (User user in users)
                                {
                                    var cardsCount = _unitOfWork.CardRepository.GetByUserId(agDevice.Icu.CompanyId ?? 0, user.Id).Count;
                                    totalMsg += (cardsCount + maxSplit - 1) / maxSplit;
                                }
                                _accessGroupService.SendAddOrDeleteUser(agDevice, users, true, groupIndex: curIndex, groupLength: groupLength,
                        processId: processId, actionType: Constants.ActionType.TransmitData, remainCount: --remainCount, progressRange: progressRange);
                                curIndex += (totalMsg + maxSplit - 1) / maxSplit;
                            }
                        }

                        details.Add(DeviceResource.lblTransmitUserInfo);
                    }
                    if (models.TransmitIds.Contains((short)TransmitType.MessageSetting))
                    {
                        if (device.DeviceType == (short)DeviceType.ItouchPop)
                        {
                            _deviceMessageService.SendDeviceMessage(new List<IcuDevice> { device }, null, groupMsgId: groupMsgId, groupIndex: curIndex, groupLength: groupLength,
                            processId: processId, actionType: Constants.ActionType.TransmitData);
                        }
                        curIndex += 1;
                        details.Add(DeviceResource.lblTransmitMessageSetting);
                    }

                    contentsDetail = string.Join("\n", details);

                    //Save system log
                    _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.TransmitAllData, ActionLogType.Transmit,
                        content, contentsDetail, null, device.CompanyId);
                    _unitOfWork.Save();
                }
            }
        }

        /// <summary>
        /// Init data
        /// </summary>
        /// <returns></returns>
        public DeviceInitModel InitializeData()
        {
            var data = new DeviceInitModel
            {
                VerifyModeItems = EnumHelper.ToEnumList<VerifyMode>(),
                SensorTypeItems = EnumHelper.ToEnumList<SensorType>(),
                PassbackItems = EnumHelper.ToEnumList<PassbackRules>(),
                HolidayItems = EnumHelper.ToEnumList<HolidayType>()
            };
            return data;
        }

        public List<CardModel> GetCardModelsByUserId(int userId)
        {
            var cards = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), userId);

            return Mapper.Map<List<CardModel>>(cards);
        }

        /// <summary>
        /// get list of cardId by user id
        /// </summary>
        /// <param name="userId"> the identifier of user. </param>
        /// <returns> list of card id </returns>
        public List<string> GetCardIdListByUserId(int userId)
        {
            var cardIds = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), userId).Select(m => m.CardId).ToList();

            return cardIds;
        }

        public List<IcuDevice> GetOnlineDevices(List<IcuDevice> devices, ref string offlineDeviceAddr)
        {
            List<IcuDevice> onlineDevices = new List<IcuDevice>();
            List<string> offlineDevices = new List<string>();

            foreach (var device in devices)
            {
                if (device.ConnectionStatus != (short)IcuStatus.Connected)
                {
                    offlineDevices.Add(device.DeviceAddress);
                }
                else
                {
                    onlineDevices.Add(device);
                }
            }

            offlineDeviceAddr = string.Join(", ", offlineDevices);

            return onlineDevices;
        }

        /// <summary>
        /// Stop Firmware updating
        /// </summary>
        /// <param name="devices">device list to stop</param>
        /// <param name="processIds">process id list</param>
        /// <returns></returns>
        public void StopProcess(List<IcuDevice> devices, List<string> processIds)
        {
            try
            {
                if (devices.Any())
                {
                    foreach (var device in devices)
                    {
                        var deviceAddress = device.DeviceAddress;

                        _queueService.ClearSpecificQueue($"{Constants.RabbitMq.MultipleMessagesTaskQueue}_{deviceAddress}");
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="device">Device that contains existing information</param>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool CheckChange(IcuDevice device, DeviceModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                if (device.Name != model.DoorName)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorName, device.Name, model.DoorName));
                }

                if (device.DeviceType != model.DeviceType)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDeviceType,
                        ((DeviceType)device.DeviceType).GetDescription(), ((DeviceType)model.DeviceType).GetDescription()));
                }

                if (device.VerifyMode != model.VerifyMode)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblVerifyMode,
                        ((VerifyMode)device.VerifyMode).GetDescription(), ((VerifyMode)model.VerifyMode).GetDescription()));
                }

                if (device.BackupPeriod != model.BackupPeriod)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblBackupPeriod, device.BackupPeriod, model.BackupPeriod));
                }

                if (device.ActiveTzId != model.ActiveTimezoneId && device.ActiveTzId != null && model.ActiveTimezoneId != null)
                {
                    var tz = _unitOfWork.TimezoneRepository.GetById(device.ActiveTzId ?? 0);
                    if (tz != null)
                    {
                        changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorActiveTimezone,
                            _unitOfWork.TimezoneRepository.GetById(device.ActiveTzId ?? 0).Name,
                            _unitOfWork.TimezoneRepository.GetById(model.ActiveTimezoneId ?? 0).Name));
                    }
                    else
                    {
                        changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorActiveTimezone,
                            null, _unitOfWork.TimezoneRepository.GetById(model.ActiveTimezoneId ?? 0).Name));
                    }
                }

                if (device.PassageTzId != model.PassageTimezoneId && device.PassageTzId != null && model.PassageTimezoneId != null)
                {
                    var pz = _unitOfWork.TimezoneRepository.GetById(device.PassageTzId ?? 0);
                    if (pz != null)
                    {
                        changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorPassageTimezone,
                            _unitOfWork.TimezoneRepository.GetById(device.PassageTzId ?? 0).Name,
                            _unitOfWork.TimezoneRepository.GetById(model.PassageTimezoneId ?? 0).Name));
                    }
                    else
                    {
                        changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorPassageTimezone,
                            null,
                            _unitOfWork.TimezoneRepository.GetById(model.PassageTimezoneId ?? 0).Name));
                    }
                }

                if (device.OperationType != model.OperationType)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblOperationType,
                        ((OperationType)device.OperationType).GetDescription(), ((OperationType)model.OperationType).GetDescription()));
                }

                //if (device.RoleReader0 != model.RoleReader0)
                //{
                //    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblOperationType,
                //        ((OperationType)device.OperationType).GetDescription(), ((OperationType)model.OperationType).GetDescription()));
                //}

                if (device.SensorType != model.SensorType)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorSensorType,
                         ((SensorType)device.SensorType).GetDescription(), ((SensorType)model.SensorType).GetDescription()));
                }

                if (device.OpenDuration != model.LockOpenDuration)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblLockOpenDuration,
                         device.OpenDuration ?? 0, model.LockOpenDuration));
                }

                if (device.SensorDuration != model.SensorDuration)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblStatusDelay,
                         device.SensorDuration ?? 0, model.SensorDuration ?? 0));
                }

                if (device.SensorAlarm != model.Alarm)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblAlarm,
                         device.SensorAlarm ? CommonResource.Use : CommonResource.NotUse, model.Alarm ? CommonResource.Use : CommonResource.NotUse));
                }

                if (device.CloseReverseLockFlag != model.CloseReverseLock)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblCloseReverseLock,
                         device.CloseReverseLockFlag ? CommonResource.Use : CommonResource.NotUse, model.CloseReverseLock ? CommonResource.Use : CommonResource.NotUse));
                }

                if (device.PassbackRule != model.Passback)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblAntiPassback,
                         ((PassbackRules)device.PassbackRule).GetDescription(), ((PassbackRules)model.Passback).GetDescription()));
                }

                if (device.MPRCount != model.MPRCount)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorSensorType,
                         device.MPRCount, model.MPRCount ?? 1));
                }

                if (device.MPRInterval != model.MPRInterval)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, DeviceResource.lblDoorSensorType,
                         device.MPRInterval, model.MPRInterval));
                }
            }

            return changes.Count() > 0;
        }

        /// <summary>   View history. \n
        ///             This function is for displaying the device history on web.</summary>
        /// <param name="deviceId"> Device Id to check history. </param>
        /// <param name="pageNumber"> page number </param>
        /// <param name="pageSize"> data count in one page </param>
        /// <param name="totalRecords"> total records count </param>
        /// <remarks>   Edward, 2020-02-29. </remarks>
        public IEnumerable<DeviceHistoryModel> GetHistory(int deviceId, int pageNumber, int pageSize, out int totalRecords)
        {
            IcuDevice device = _unitOfWork.IcuDeviceRepository.GetByIcuId(deviceId);

            var companyId = device.CompanyId;

            //var accountTimezone = _unitOfWork.AccountRepository.Get(x =>
            //                       x.Id == _httpContext.User.GetAccountId() && !x.IsDeleted).TimeZone;

            var systemLogData = _unitOfWork.AppDbContext.SystemLog
                .Include(m => m.CreatedByNavigation)
                .Where(c => c.CompanyId == companyId)
                .Where(c => JObject.Parse(c.ContentIds)["Id"].ToString().Equals(deviceId + "")
                    || JObject.Parse(c.ContentIds)["assigned_ids"].ToString().Contains(deviceId + ""))
                .Where(c => c.Type == (short)SystemLogType.DeviceMonitoring
                    || c.Type == (short)SystemLogType.DeviceSetting
                    || c.Type == (short)SystemLogType.DeviceUpdate
                    || c.Type == (short)SystemLogType.TransmitAllData
                    || c.Type == (short)SystemLogType.Emergency)
                .Select(c => new SystemLog
                {
                    //OpeTime = Helpers.ConvertToUserTimeZoneReturnDate(c.OpeTime, accountTimezone),
                    OpeTime = c.OpeTime,
                    CreatedByNavigation = c.CreatedByNavigation,
                    Action = c.Action,
                    Content = c.Content,
                    ContentDetails = c.ContentDetails
                }).Select(Mapper.Map<DeviceHistoryModel>).ToList();

            var eventLogData = _unitOfWork.AppDbContext.EventLog
                .Where(c => c.CompanyId == companyId)
                .Where(c => c.IcuId == deviceId)
                .Where(c => c.EventType == (short)EventType.CommunicationSucceed
                    || c.EventType == (short)EventType.CommunicationFailed)
                .Select(c => new EventLog
                {
                    //EventTime = Helpers.ConvertToUserTimeZoneReturnDate(c.EventTime, accountTimezone),
                    EventTime = c.EventTime,
                    EventType = c.EventType
                })
                .Select(Mapper.Map<DeviceHistoryModel>).ToList();

            var data = systemLogData.Concat(eventLogData);

            totalRecords = data.Count();

            data = data.OrderByDescending(c => c.EventTime);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }



        public IQueryable<IcuDevice> GetPaginatedDevices(string filter, int deviceType, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, short operationType, List<int> companyId, List<int> connectionStatus, List<int> _deviceType)
        {
            var data = _unitOfWork.AppDbContext.IcuDevice
                .Include(m => m.ActiveTz)
                .Include(m => m.PassageTz)
                .Include(m => m.Building)
                .Include(m => m.Company)
                .Where(m => !m.IsDeleted);

            if (_httpContext.User.GetAccountType() != (short)AccountType.SystemAdmin)
            {
                data = data.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Name.ToLower().Contains(filter.ToLower())
                                       || x.DeviceAddress.ToLower().Contains(filter.ToLower())
                                       || x.Building.Name.ToLower().Contains(filter.ToLower()));
            }

            if (deviceType != 0)
            {
                data = data.Where(x => x.DeviceType == deviceType);
            }

            if (operationType != 0)
            {
                data = data.Where(x => x.OperationType == operationType);
            }

            if (companyId.Count() > 0)
            {
                data = data.Where(x => companyId.Contains(Convert.ToInt32(x.CompanyId)));
            }
            if (connectionStatus.Count() > 0)
            {
                data = data.Where(x => connectionStatus.Contains(Convert.ToInt32(x.ConnectionStatus)));
            }
            if (_deviceType.Count() > 0)
            {
                data = data.Where(x => _deviceType.Contains(Convert.ToInt32(x.DeviceType)));
            }
            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Devices.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Devices[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            var accountTimezone = _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone;

            foreach (var device in data)
            {
                device.LastCommunicationTime = Helpers.ConvertToUserTimeZoneReturnDate(device.LastCommunicationTime, accountTimezone);
            }
            return data;
        }


        public void UpdateUpTimeToDevice(int deviceId)
        {
            var start = DateTime.Now;
            var device = _unitOfWork.IcuDeviceRepository.GetByIcuId(deviceId);
            List<EventLog> eventLogData = _unitOfWork.IcuDeviceRepository.GetEventLogData(deviceId).ToList();

            //Get Monitoring up time to the device
            List<MonitoringUpTimeDevice> listUpTime = new List<MonitoringUpTimeDevice>();

            float Count = 0;
            double _timeOnline = 0;
            if (eventLogData.Count() > 0)
            {
                for (int i = 0; i < eventLogData.Count; i++)
                {
                    var listEventLogData = eventLogData.Where(x => x.EventTime >= x.CreatedOn).OrderBy(x => x.EventTime).ToList();
                    var item = listEventLogData[i];

                    var checkData = listUpTime.OrderByDescending(x => x.id).FirstOrDefault();
                    if (checkData != null && checkData.TimeFailed == null)
                    {
                        if (item.EventType == (short)EventType.CommunicationFailed)
                        {
                            checkData.TimeFailed = Convert.ToDateTime(item.EventTime);
                        }
                    }
                    else
                    {
                        if (item.EventType == (short)EventType.CommunicationSucceed)
                        {
                            MonitoringUpTimeDevice timeUp = new MonitoringUpTimeDevice();
                            timeUp.id = checkData == null ? (i + 1) : (checkData.id + 1);
                            timeUp.TimeSuccess = Convert.ToDateTime(item.EventTime);
                            timeUp.TimeFailed = null;
                            listUpTime.Add(timeUp);
                        }
                    }


                }

                var exitsFailed = listUpTime.Where(x => x.TimeFailed == null).FirstOrDefault();
                var exitsSuccess = listUpTime.OrderBy(x => x.id).FirstOrDefault();

                if (exitsFailed != null)
                {
                    exitsFailed.TimeFailed = device.LastCommunicationTime;

                }
                else
                {
                    if (device.ConnectionStatus == (short)IcuStatus.Connected)
                        _timeOnline = Math.Abs(Convert.ToDateTime(DateTime.Now).Subtract(Convert.ToDateTime(exitsSuccess.TimeSuccess)).TotalMinutes);
                }
                var lstUpTime = listUpTime.ToList();

                foreach (var item in lstUpTime)
                {
                    var CountUpTimeDevice = Math.Abs(Convert.ToDateTime(item.TimeFailed).Subtract(Convert.ToDateTime(item.TimeSuccess)).TotalMinutes);
                    Count += (float)CountUpTimeDevice;
                }
            }
            else
            {


                if (device.ConnectionStatus == (short)IcuStatus.Connected)
                {
                    var lastSuccessTime = _unitOfWork.AppDbContext.EventLog.Where(x => x.IcuId == device.Id && x.EventType == (short)EventType.CommunicationSucceed)
                    .GroupBy(x => new { x.IcuId }).Select(x => new EventLog { Id = x.First().Id ,IcuId = x.First().IcuId, EventTime = x.Max(c => c.EventTime) }).FirstOrDefault();

                    _timeOnline = Math.Abs(Convert.ToDateTime(DateTime.Now).Subtract(Convert.ToDateTime(lastSuccessTime.EventTime)).TotalMinutes);
                    var eventLog = _unitOfWork.AppDbContext.EventLog.Where(x=>x.Id == lastSuccessTime.Id).FirstOrDefault();
                    eventLog.EventTime = DateTime.Now;

                    _unitOfWork.AppDbContext.EventLog.Update(eventLog);
                    _unitOfWork.AppDbContext.SaveChanges();
                }


            }





            device.CreateTimeOnlineDevice = device.LastCommunicationTime.ToString();
            device.UpTimeOnlineDevice = device.UpTimeOnlineDevice + ((int)Math.Ceiling(Count)) + Convert.ToInt32(_timeOnline);

            _unitOfWork.IcuDeviceRepository.UpdateDevice(device);
            var end = DateTime.Now;
        }


        public int ReUpdateUpTimOnlineDevice()
        {
            int Count = 0;
            _unitOfWork.IcuDeviceRepository.ReUpdateUpTimOnlineDevice();
            foreach (var item in _unitOfWork.AppDbContext.IcuDevice.Where(c => c.IsDeleted == false))
            {
                UpdateUpTimeToDevice(item.Id);
                Count++;
            }
            return Count;
        }
        public void ReUpdateUpTimOnlineDeviceById(int id)
        {
            _unitOfWork.IcuDeviceRepository.ReUpdateUpTimOnlineDeviceById(id);
            UpdateUpTimeToDevice(id);
        }

    }
}