﻿using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// AccessLevel service interface
    /// </summary>
    public interface IAccessGroupDeviceService
    {
        IQueryable<AccessGroupDevice> GetByAccessGroupId(int accessGroupId);
        IQueryable<AccessGroupDevice> GetUnAssignAccessGroupId(int accessGroupId);
        List<AccessGroupDevice> GetByTimezoneId(int timezoneId);
    }

    public class AccessGroupDeviceService : IAccessGroupDeviceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;

        public AccessGroupDeviceService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor)
        {
            _unitOfWork = unitOfWork;
            _httpContext = contextAccessor.HttpContext;
        }

        /// <summary>
        /// Get by access group id
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public IQueryable<AccessGroupDevice> GetByAccessGroupId(int accessGroupId)
        {
            return _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(), accessGroupId);
        }

        /// <summary>
        /// Get unassign access group id
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public IQueryable<AccessGroupDevice> GetUnAssignAccessGroupId(int accessGroupId)
        {
            return _unitOfWork.AccessGroupDeviceRepository.GetByUnAssignAccessGroupId(_httpContext.User.GetCompanyId(), accessGroupId);
        }

        /// <summary>
        /// Get by timezone id
        /// </summary>
        /// <param name="timezoneId"></param>
        /// <returns></returns>
        public List<AccessGroupDevice> GetByTimezoneId(int timezoneId)
        {
            return _unitOfWork.AccessGroupDeviceRepository.GetByTimezoneId(_httpContext.User.GetCompanyId(),
                timezoneId);
        }
    }
}
