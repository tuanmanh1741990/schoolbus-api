﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service.Csv
{
    public class CsvHelper
    {
        public static void Write(string[] headers, List<string[]> contents, string csvFile)
        {
            using (var writer = File.AppendText(csvFile))
            {
                var csvBuilder = new StringBuilder();
                if (new FileInfo(csvFile).Length == 0 && headers != null)
                {
                    csvBuilder.AppendLine(string.Join(",", headers));
                }
                foreach (var content in contents)
                {
                    csvBuilder.AppendLine(string.Join(",", content));
                }
                writer.Write(csvBuilder.ToString());
            }
        }
    }
}
