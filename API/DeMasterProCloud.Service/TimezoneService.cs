﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Timezone;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using DeMasterProCloud.Service.Protocol;
using RabbitMQ.Client;

namespace DeMasterProCloud.Service
{
    public interface ITimezoneService : IPaginationService<TimezoneListModel>
    {
        List<Timezone> GetTimezones(int companyId);
        Timezone GetDefaultTzByCompanyId(int tzPosition, int companyId);
        IEnumerable<SelectListItem> GetSelectListTimezone();
        void Add(TimezoneModel model);
        void Update(TimezoneModel model, Timezone timezone);
        TimezoneModel InitData(Timezone timezone);
        Timezone GetByIdAndCompany(int timezoneId, int companyId);
        void Delete(Timezone timezone);
        void DeleteRange(List<Timezone> timezones);
        List<Timezone> GetByIdsAndCompany(List<int> ids, int companyId);
        Timezone GetTimezoneByNameAndCompany(int companyId, string name);
        bool IsDefaultTimezone(int companyId, int timezoneId);
        int GetTimezoneCount(int companyId);
        bool IsExistedName(int timezoneId, string name);
        void SendUpdateTimezone(Timezone timezone, List<string> deviceAddress, string groupMsgId = "", int groupIndex=0, int groupLength=1, string processId="", string actionType="");
        UpdateTimezoneProtocolData MakeUpdateTimezoneProtocolData(Timezone timezone, string protocolType);
    }

    public class TimezoneService : ITimezoneService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IQueueService _queueService;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;
        private readonly IConnection _connection;

        public TimezoneService(IUnitOfWork unitOfWork, IQueueService queueService,
            IHttpContextAccessor httpContextAccessor, ILogger<TimezoneService> logger)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
            _httpContext = httpContextAccessor.HttpContext;
            _connection = ApplicationVariables.RabbitMqConsumerConnection;
            _logger = logger;
        }

        /// <summary>
        /// Init data in page
        /// </summary>
        /// <param name="timezone"></param>
        /// <returns></returns>
        public TimezoneModel InitData(Timezone timezone)
        {
            if (timezone == null)
            {
                return null;
            }

            var model = new TimezoneModel
            {
                Id = timezone.Id,
                Name = timezone.Name,
                Remarks = timezone.Remarks,
                CompanyId = timezone.CompanyId,
                Position = timezone.Position,
                Monday = ConvertStringToDayDetail(new[] { timezone.MonTime1, timezone.MonTime2, timezone.MonTime3, timezone.MonTime4 }),
                Tuesday = ConvertStringToDayDetail(new[] { timezone.TueTime1, timezone.TueTime2, timezone.TueTime3, timezone.TueTime4 }),
                Wednesday = ConvertStringToDayDetail(new[] { timezone.WedTime1, timezone.WedTime2, timezone.WedTime3, timezone.WedTime4 }),
                Thursday = ConvertStringToDayDetail(new[] { timezone.ThurTime1, timezone.ThurTime2, timezone.ThurTime3, timezone.ThurTime4 }),
                Friday = ConvertStringToDayDetail(new[] { timezone.FriTime1, timezone.FriTime2, timezone.FriTime3, timezone.FriTime4 }),
                Saturday = ConvertStringToDayDetail(new[] { timezone.SatTime1, timezone.SatTime2, timezone.SatTime3,timezone.SatTime4 }),
                Sunday = ConvertStringToDayDetail(new[] { timezone.SunTime1, timezone.SunTime2, timezone.SunTime3,timezone.SunTime4 }),
                HolidayType1 = ConvertStringToDayDetail(new[] { timezone.HolType1Time1, timezone.HolType1Time2, timezone.HolType1Time3, timezone.HolType1Time4 }),
                HolidayType2 = ConvertStringToDayDetail(new[] { timezone.HolType2Time1, timezone.HolType2Time2, timezone.HolType2Time3, timezone.HolType2Time4 }),
                HolidayType3 = ConvertStringToDayDetail(new[] { timezone.HolType3Time1, timezone.HolType3Time2, timezone.HolType3Time3, timezone.HolType3Time4 }),
                CreatedBy = timezone.CreatedBy,
                CreatedOn = timezone.CreatedOn,
                UpdatedBy = timezone.UpdatedBy,
                UpdatedOn = timezone.UpdatedOn
            };

            return model;
        }

        /// <summary>
        /// Add timezone
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public void Add(TimezoneModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var positions = GetPositionsByCompany(_httpContext.User.GetCompanyId());
                        var timezone = new Timezone
                        {
                            CompanyId = _httpContext.User.GetCompanyId(),
                            Name = model.Name,
                            Remarks = model.Remarks
                        };
                        //Calculate position
                        var curPositions = positions.OrderByDescending(c => c)
                            .ToList();

                        var maxRange = curPositions.FirstOrDefault();

                        var rangePosition = Enumerable.Range(1, maxRange);
                        var availablePositions = rangePosition.Except(curPositions)
                            .OrderByDescending(c => c)
                            .ToList();
                        if (availablePositions.Any())
                        {
                            timezone.Position = availablePositions.FirstOrDefault();
                        }
                        else
                        {
                            timezone.Position = maxRange + 1;
                        }

                        MappingData(model, timezone);
                        _unitOfWork.TimezoneRepository.Add(timezone);
                        

                        //Save system log
                        var content =
                            $"{ActionLogTypeResource.Add} {TimezoneResource.lblTimezone} : {timezone.Name} ({TimezoneResource.lblTimezoneName})";
                        _unitOfWork.SystemLogRepository.Add(timezone.Id, SystemLogType.Timezone, ActionLogType.Add,
                            content, null, null, _httpContext.User.GetCompanyId());
                        _unitOfWork.Save();

                        //TODO: Send to topic timezone of the device belong to current company
                        var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(_httpContext.User.GetCompanyId());
                        List<String> deviceAddress = new List<String>();

                        foreach (var device in devices)
                        {
                            deviceAddress.Add(device.DeviceAddress);
                        }

                        SendUpdateTimezone(timezone, deviceAddress);

                        transaction.Commit();

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Update timezone
        /// </summary>
        /// <param name="model"></param>
        /// <param name="timezone"></param>
        /// <returns></returns>
        public void Update(TimezoneModel model, Timezone timezone)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        MappingData(model, timezone);
                        timezone.Name = model.Name;
                        timezone.Remarks = model.Remarks;
                        //Mapper.Map(model, timezone);
                        _unitOfWork.TimezoneRepository.Update(timezone);

                        //Save system log
                        var content = $"{ActionLogTypeResource.Update} : {timezone.Name} ({TimezoneResource.lblTimezoneName})";
                        _unitOfWork.SystemLogRepository.Add(timezone.Id, SystemLogType.Timezone, ActionLogType.Update,
                            content, null, null, _httpContext.User.GetCompanyId());

                        var devices = _unitOfWork.IcuDeviceRepository.GetByCompany(_httpContext.User.GetCompanyId());
                        List<String> deviceAddress = new List<String>();

                        foreach (var device in devices)
                        {
                            deviceAddress.Add(device.DeviceAddress);
                        }

                        //Send to icu
                        SendUpdateTimezone(timezone, deviceAddress);

                        //Save all changes
                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }
        /// <summary>
        /// Delete timezone
        /// </summary>
        /// <param name="timezone"></param>
        /// <returns></returns>
        public void Delete(Timezone timezone)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        timezone.IsDeleted = true;
                        _unitOfWork.TimezoneRepository.Update(timezone);
                        _unitOfWork.Save();

                        //Save system log
                        var content = $"{ActionLogTypeResource.Delete} : {timezone.Name} ({TimezoneResource.lblTimezoneName})";
                        _unitOfWork.SystemLogRepository.Add(timezone.Id, SystemLogType.Timezone, ActionLogType.Delete,
                            content, null, null, _httpContext.User.GetCompanyId());

                        var icuDevices = GetListDeviceAddress(timezone.Id);

                        //Send delete json to queue and save message log
                        SendDeleteTimezone(timezone, icuDevices);

                        //Save all changes
                        _unitOfWork.Save();

                        transaction.Commit();

                        //Send notification
                        //_QueueService.PushNotifyDevice(icuDevices, PushType.Timezone);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a list of timezone
        /// </summary>
        /// <param name="timezones"></param>
        /// <returns></returns>
        public void DeleteRange(List<Timezone> timezones)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var timezone in timezones)
                        {
                            timezone.IsDeleted = true;
                            _unitOfWork.TimezoneRepository.Update(timezone);
                            _unitOfWork.Save();

                            var icuDevices = GetListDeviceAddress(timezone.Id);

                            //Send delete json to queue and save message log
                            SendDeleteTimezone(timezone, icuDevices);
                        }

                        //Save system log
                        var timezoneIds = timezones.Select(c => c.Id).ToList();
                        var timezoneNames = timezones.Select(c => c.Name).ToList();
                        var content = string.Format(ActionLogTypeResource.DeleteMultipleType, TimezoneResource.lblTimezone);
                        var contentDetails = $"{TimezoneResource.lblTimezoneName} : {string.Join(", ", timezoneNames)}";

                        _unitOfWork.SystemLogRepository.Add(timezoneIds.First(), SystemLogType.Timezone, ActionLogType.DeleteMultiple, 
                            content, contentDetails, timezoneIds, _httpContext.User.GetCompanyId());

                        //Save all changes
                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        internal string[] ConvertDayDetailToString(List<DayDetail> dayDetails)
        {
            var result = new string[Constants.Settings.NumberTimezoneOfDay];
            for (int i = 0; i < result.Length; i++)
            {
                if (dayDetails.ElementAtOrDefault(i) != null)
                {
                    result[i] = JsonConvert.SerializeObject(dayDetails[i]);
                }
            }
            return result;
        }

        internal List<DayDetail> ConvertStringToDayDetail(string[] dayDetails)
        {
            return dayDetails.Where(x => !string.IsNullOrEmpty(x)).Select(JsonConvert.DeserializeObject<DayDetail>).ToList();
        }

        /// <summary>
        /// Get timezone count by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public int GetTimezoneCount(int companyId)
        {
            return _unitOfWork.TimezoneRepository.GetTimezoneCount(companyId);
        }

        /// <summary>
        /// Get timezone by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Timezone> GetTimezones(int companyId)
        {
            return _unitOfWork.TimezoneRepository.GetByCompany(companyId);
        }

        /// <summary>
        /// Get list of position of timezone by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<int> GetPositionsByCompany(int companyId)
        {
            return _unitOfWork.TimezoneRepository.GetPositionsByCompany(companyId);
        }

        /// <summary>
        /// Get select list item of timezone
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetSelectListTimezone()
        {
            var companyId = _httpContext.User.GetCompanyId();
            return _unitOfWork.TimezoneRepository.GetByCompany(companyId).Select(m => new SelectListItem
            {
                Value = m.Id.ToString(),
                Text = m.Name
            });
        }

        /// <summary>
        /// Get timezone with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<TimezoneListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.AppDbContext.Timezone.Where(m =>
                !m.IsDeleted && m.CompanyId == companyId)
                .Select(m => new TimezoneListModel
                {
                    Id = m.Id,
                    TimezoneName = (m.Id == 1 || m.Id == 2) ? ((DefaultTimezoneType)m.Id).GetDescription() : m.Name,
                    Remark = m.Remarks,
                    Position = m.Position
                });
            totalRecords = data.Count();
            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.TimezoneName.ToLower().Contains(filter.ToLower())
                                       || x.Remark.ToLower().Contains(filter.ToLower()));
            }
            recordsFiltered = data.Count();

            data = data.OrderBy($"{ColumnDefines.Timezone[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }

        //Get timezone by id and company
        public Timezone GetByIdAndCompany(int timezoneId, int companyId)
        {
            var timzone = _unitOfWork.TimezoneRepository.GetByIdAndCompany(timezoneId, companyId);
            timzone.CreatedOn = Helpers.ConvertToUserTimeZoneReturnDate(timzone.CreatedOn, _unitOfWork.AccountRepository.Get(m =>
                m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
            timzone.UpdatedOn = Helpers.ConvertToUserTimeZoneReturnDate(timzone.UpdatedOn, _unitOfWork.AccountRepository.Get(m =>
                m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
            return timzone;
        }

        /// <summary>
        /// Mapping data
        /// </summary>
        /// <param name="tzPosition"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Timezone GetDefaultTzByCompanyId(int tzPosition, int companyId)
        {
            return _unitOfWork.TimezoneRepository.GetDefaultTzByCompanyId(tzPosition, companyId);
        }

        /// <summary>
        /// Mapping data
        /// </summary>
        /// <param name="model"></param>
        /// <param name="timezone"></param>
        /// <returns></returns>
        internal void MappingData(TimezoneModel model, Timezone timezone)
        {
            if (model.Monday != null && model.Monday.Any())
            {
                var monday = ConvertDayDetailToString(model.Monday);
                timezone.MonTime1 = monday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.MonTime2 = monday[1];
                timezone.MonTime3 = monday[2];
                timezone.MonTime4 = monday[3];
            }

            if (model.Tuesday != null && model.Tuesday.Any())
            {
                var tuesday = ConvertDayDetailToString(model.Tuesday);
                timezone.TueTime1 = tuesday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.TueTime2 = tuesday[1];
                timezone.TueTime3 = tuesday[2];
                timezone.TueTime4 = tuesday[3];
            }

            if (model.Wednesday != null && model.Wednesday.Any())
            {
                var wednesday = ConvertDayDetailToString(model.Wednesday);
                timezone.WedTime1 = wednesday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.WedTime2 = wednesday[1];
                timezone.WedTime3 = wednesday[2];
                timezone.WedTime4 = wednesday[3];
            }

            if (model.Thursday != null && model.Thursday.Any())
            {
                var thursday = ConvertDayDetailToString(model.Thursday);
                timezone.ThurTime1 = thursday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.ThurTime2 = thursday[1];
                timezone.ThurTime3 = thursday[2];
                timezone.ThurTime4 = thursday[3];
            }

            if (model.Friday != null && model.Friday.Any())
            {
                var friday = ConvertDayDetailToString(model.Friday);
                timezone.FriTime1 = friday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.FriTime2 = friday[1];
                timezone.FriTime3 = friday[2];
                timezone.FriTime4 = friday[3];
            }

            if (model.Saturday != null && model.Saturday.Any())
            {
                var saturday = ConvertDayDetailToString(model.Saturday);

                timezone.SatTime1 = saturday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.SatTime2 = saturday[1];
                timezone.SatTime3 = saturday[2];
                timezone.SatTime4 = saturday[3];
            }

            if (model.Sunday != null && model.Sunday.Any())
            {
                var sunday = ConvertDayDetailToString(model.Sunday);
                timezone.SunTime1 = sunday[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.SunTime2 = sunday[1];
                timezone.SunTime3 = sunday[2];
                timezone.SunTime4 = sunday[3];
            }

            if (model.HolidayType1 != null && model.HolidayType1.Any())
            {
                var holidayType1 = ConvertDayDetailToString(model.HolidayType1);
                timezone.HolType1Time1 = holidayType1[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.HolType1Time2 = holidayType1[1];
                timezone.HolType1Time3 = holidayType1[2];
                timezone.HolType1Time4 = holidayType1[3];
            }

            if (model.HolidayType2 != null && model.HolidayType2.Any())
            {
                var holidayType2 = ConvertDayDetailToString(model.HolidayType2);

                timezone.HolType2Time1 = holidayType2[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.HolType2Time2 = holidayType2[1];
                timezone.HolType2Time3 = holidayType2[2];
                timezone.HolType2Time4 = holidayType2[3];
            }

            if (model.HolidayType3 != null && model.HolidayType3.Any())
            {
                var holidayType3 = ConvertDayDetailToString(model.HolidayType3);

                timezone.HolType3Time1 = holidayType3[0] ?? Constants.Settings.DefaultTimezoneNotUse;
                timezone.HolType3Time2 = holidayType3[1];
                timezone.HolType3Time3 = holidayType3[2];
                timezone.HolType3Time4 = holidayType3[3];
            }
        }

        /// <summary>
        /// Get a list of timezone
        /// </summary>
        /// <param name="ids"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Timezone> GetByIdsAndCompany(List<int> ids, int companyId)
        {
            return _unitOfWork.TimezoneRepository.GetByIdsAndCompany(ids, companyId);
        }

        /// <summary>
        /// Get timezone by name and company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Timezone GetTimezoneByNameAndCompany(int companyId, string name)
        {
            return _unitOfWork.TimezoneRepository.GetTimezoneByNameAndCompany(companyId, name);
        }

        /// <summary>
        /// Check if timezone is default
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="timezoneId"></param>
        /// <returns></returns>
        public bool IsDefaultTimezone(int companyId, int timezoneId)
        {
            return _unitOfWork.TimezoneRepository.Get(c => c.CompanyId == companyId
                        && c.Id == timezoneId && c.Position == 1) != null;
        }

        /// <summary>
        /// Check if timezone name is existed
        /// </summary>
        /// <param name="timezoneId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsExistedName(int timezoneId, string name)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var timezone = GetTimezoneByNameAndCompany(companyId, name);
            if (timezone != null && timezoneId != timezone.Id)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Send update timezone to rabbit mq server
        /// </summary>
        /// <param name="timezone"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="isPublish"></param>
        /// <param name="isSave"></param>
        public void SendUpdateTimezone(Timezone timezone, List<string> deviceAddress, string groupMsgId = "", int groupIndex=0, int groupLength=1, string processId="", string actionType="")
        {
            var protocolType = Constants.Protocol.UpdateTimezone;
            var protocolData = MakeUpdateTimezoneProtocolData(timezone, protocolType);

            if (deviceAddress != null && deviceAddress.Any())
            {
                foreach (var device in deviceAddress)
                {
                    var msgId = Guid.NewGuid().ToString();
                    protocolData.MsgId = msgId;
                    protocolData.Sender = _httpContext.User.GetUsername();

                    var message = protocolData.ToString();
                    var routingKey = Constants.RabbitMq.TimezoneTopic + "." + device;
                    if (groupLength==1)
                    {
                        _queueService.Publish(routingKey, message);
                    }
                    else
                    {
                        _queueService.SendGroupMessage(device, msgId, message, topic: routingKey, groupMsgId, groupIndex, groupLength, processId, actionType: actionType);
                    }

                }
            }
        }

        /// <summary>
        /// Send delete timezone to rabbitmq
        /// </summary>
        /// <param name="timezone"></param>
        /// <param name="deviceAddress"></param>
        public void SendDeleteTimezone(Timezone timezone, List<string> deviceAddress)
        {
            var protocolType = Constants.Protocol.DeleteTimezone;
            var protocolData = MakeDeleteTimezoneProtocolData(timezone, protocolType);
            var message = protocolData.ToString();
            if (deviceAddress != null && deviceAddress.Any())
            {
                foreach (var device in deviceAddress)
                {
                    var routingKey = Constants.RabbitMq.TimezoneTopic + "." + device;
                    _queueService.Publish(routingKey, message);
                }
            }
        }

        /// <summary>
        /// Make timezone protocol data
        /// </summary>
        /// <param name="timezone"></param>
        /// <param name="protocolType"></param>
        /// <returns></returns>
        public UpdateTimezoneProtocolData MakeUpdateTimezoneProtocolData(Timezone timezone, string protocolType)
        {
            var timezoneProtocolData = new UpdateTimezoneProtocolData
            {
                Type = protocolType
            };
            var timezoneProtocolHeaderData = new UpdateTimezoneProtocolHeaderData()
            {
                Total = 1,
                Timezone = new List<UpdateTimezoneProtocolDetailData> { Mapper.Map<UpdateTimezoneProtocolDetailData>(timezone) }
            };
            timezoneProtocolData.Data = timezoneProtocolHeaderData;
            return timezoneProtocolData;
        }

        /// <summary>
        /// Make timezone protocol data
        /// </summary>
        /// <param name="timezone"></param>
        /// <param name="protocolType"></param>
        /// <returns></returns>
        public TimezoneProtocolData MakeDeleteTimezoneProtocolData(Timezone timezone, string protocolType)
        {
            var timezoneProtocolData = new TimezoneProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };

            var deleteTimezoneProtocolHeaderData = new TimezoneProtocolHeaderData()
            {
                Total = 1,
                Timezone = new List<TimezoneProtocolDetailData> { Mapper.Map<TimezoneProtocolDetailData>(timezone) }
            };
            timezoneProtocolData.Data = deleteTimezoneProtocolHeaderData;
            return timezoneProtocolData;
        }

        /// <summary>
        /// Get all icudevice by timezone id
        /// </summary>
        /// <returns></returns>
        public List<string> GetListDeviceAddress(int timezoneId)
        {
            var listDevice1 = new List<string>();
            var listDevice2 = new List<string>();
            var devicesFromDoor =
                _unitOfWork.IcuDeviceRepository.GetByTimezoneId(_httpContext.User.GetCompanyId(), timezoneId).ToList();
            if (devicesFromDoor.Any())
            {
                listDevice1 = devicesFromDoor.Select(x => x.DeviceAddress).ToList();
            }
            var devicesFromAgDevice =
                _unitOfWork.AccessGroupDeviceRepository.GetByTimezoneId(_httpContext.User.GetCompanyId(), timezoneId)
                    .Select(x => x.Icu).ToList();
            if (devicesFromAgDevice.Any())
            {
                listDevice2 = devicesFromAgDevice.Select(x => x.DeviceAddress).ToList();
            }

            var result = listDevice1.Union(listDevice2);
            return result.ToList();
        }
    }
}
