﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.CornerSetting;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Service
{
    public interface ICornerSettingService
    {
        ReponseStatus AddCornerSetting(AddCornerSettingModel model);
        ReponseStatus UpdateCornerSetting(int id, AddCornerSettingModel model);
        ReponseStatus DeleteCornerSetting(int id);
        List<CornerSettingModel> GetListCornerSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        CornerSettingModel GetCornerSetting(int Id);
    }
    public class CornerSettingService : ICornerSettingService
    {

        private readonly IUnitOfWork _unitOfWork;
        DeMasterProCloud.DataAccess.Models.CornerSetting obj = new DataAccess.Models.CornerSetting();
        ReponseStatus res = new ReponseStatus();
        public CornerSettingService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ReponseStatus AddCornerSetting(AddCornerSettingModel model)
        {

            try
            {
                obj.Code = model.code;
                obj.Description = model.description;
                obj.Name = model.name;
                var add = _unitOfWork.CornerSettingRepository.AddCornerSetting(obj);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }

                if (add.statusCode == true && add.message == null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = add.data;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;

        }

        public ReponseStatus UpdateCornerSetting(int id, AddCornerSettingModel model)
        {

            try
            {
                obj.Code = model.code;
                obj.Description = model.description;
                obj.Name = model.name;
                obj.Id = id;
                var update = _unitOfWork.CornerSettingRepository.UpdateCornerSetting(obj);
                if (update.statusCode == false)
                {
                    res.message = Constants.CornerSetting.UpdateFailed;
                    res.statusCode = false;
                    return res;
                }
                if (update.statusCode == true && update.message == null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.CornerSetting.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.UpdateFailed;
                res.statusCode = false;
            }
            return res;

        }
        public ReponseStatus DeleteCornerSetting(int id)
        {

            try
            {
                var delete = _unitOfWork.CornerSettingRepository.DeleteCornerSetting(id);
                if (delete.statusCode == false)
                {
                    res.message = Constants.CornerSetting.DeleteFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.DeleteFailed;
                res.statusCode = false;
            }
            return res;

        }

        public List<CornerSettingModel> GetListCornerSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var listCornerSetting = _unitOfWork.CornerSettingRepository.GetListCornerSetting(filter, pageNumber, pageSize, sortColumn,
             sortDirection, out totalRecords, out recordsFiltered).ToList();

            return listCornerSetting;
        }

        public CornerSettingModel GetCornerSetting(int Id)
        {
            var data = _unitOfWork.CornerSettingRepository.GetCornerSetting(Id);
            return data;
        }

    }
}
