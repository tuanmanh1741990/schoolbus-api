using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DeMasterProCloud.DataModel.Visit;
using AutoMapper;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.AccessGroup;
using System.Linq.Dynamic.Core;
using System.Text;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.User;
using Newtonsoft.Json;
using OfficeOpenXml;
using Z.EntityFramework.Plus;
using DeMasterProCloud.DataModel.EventLog;
using System.Drawing;
using System.IO;
using System.Globalization;
using System.Drawing.Imaging;
using System.Security.Cryptography;
using QRCoder;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Visit Interface 
    /// </summary>
    public interface IVisitService
    {
        IQueryable<EventLog> VisitReport(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);



        IQueryable<Visit> GetPaginated(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        int Add(VisitModel visitModel);
        void PreRegister(VisitModel visitModel);
        void Update(VisitModel visitModel);
        void ChangeStatus(int statusId, List<int> visitIds, string reason);
        Visit GetById(int id);
        List<Visit> GetByIds(List<int> ids);
        Visit GetByCardId(string cardId);
        void InitData(VisitDataModel visitModel);
        VisitViewModel InitListData();

        VisitReportViewModel InitReportData();

        //VisitPreRegisterModel InitPreRegisterData();

        string GetBuildingNameByRid(string deviceAddress);

        int GetCardStatus(string cardId);

        byte[] Export(string type, string opeDateFrom, string opeDateTo, string opeTimeFrom,
            string opeTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered);

        byte[] ExportVisitReport(string type, string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered);

        bool IsCardIdValid(VisitModel visitModel);

        bool IsProcedureOmission(int companyId);

        Visit GetByCardIdExceptThis(int id, string cardId);

        Card GetCardByCardId(string cardId);

        void ReturnCard(string cardId);
        void PassOnCard(int visitId, string cardId);

        VisitSetting GetVisitSettingCompany();
        void UpdateVisitSettingCompany(VisitSettingModel model);
        void UpdateApprovalVisitor(int id , short status);

        Card GetCardByVisitor(int id);
        int AssignedCardVisitor(int visitId, CardModel model);
        
        void DeletedAssignedCardVisitor(int id);
        bool AssignedDoorVisitor(int id, List<int> door);

        bool RejectVisitor(int id, RejectedModel model);
        
        void FinishVisitor(int id, TakeBackCardModel model);
        
        IQueryable<VisitListHistoryModel> GetPaginatedVisitHistoryLog(int id, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        bool IsCardIdExist(string cardId, int companyId);

        int LengthVisitNeedToReview();
        int GetRequestApprovalCount();

        void ReturnCardVisitor(string cardId, string reason);
        
        void ReturnVisitor(List<int> visitIds, string reason);
        
        void Delete(int visitId);
        
        int GetLastApproval(int visitId);
        int GetAccountIdCreateVisitor(int visitId);
        IEnumerable<EventLog> GetHistoryVisitor(int id, int pageNumber, int pageSize, out int totalRecords);
    }

    /// <summary>
    /// Service provider for user
    /// </summary>
    public class VisitService : IVisitService
    {

        // Inject dependency
        private readonly IUnitOfWork _unitOfWork;

        private readonly HttpContext _httpContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IAccessGroupService _accessGroupService;
        private readonly IAccountService _accountService;
        private readonly ISettingService _settingService;

        public VisitService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor,
            IHostingEnvironment hostingEnvironment, IAccessGroupService accessGroupService,
            IAccountService accountService,
            IConfiguration configuration, ILogger<VisitService> logger)
        {
            _unitOfWork = unitOfWork;
            _hostingEnvironment = hostingEnvironment;
            _httpContext = contextAccessor.HttpContext;
            _configuration = configuration;
            _logger = logger;
            _accessGroupService = accessGroupService;
            _accountService = accountService;
            _settingService = new SettingService(_unitOfWork, _configuration);
        }


        private readonly string[] _header2 =
        {

            //한아름 수정
            VisitResource.lblIDX,//Idx
            VisitResource.lblAccessTime,
            VisitResource.lblUserName,
            VisitResource.lblBirthDay,
            VisitResource.lblDepartment,
            VisitResource.lblCardID,
            VisitResource.lblRID,
            VisitResource.lblDoorName,
            VisitResource.IblSite2,
            VisitResource.lblEventDetail,
            VisitResource.lblIssueCount,
            VisitResource.lblCardStatus,
            VisitResource.lblInOut,

        };

        /// <summary>
        /// Initial data
        /// </summary>
        /// <returns></returns>
        public VisitViewModel InitListData()
        {
            var model =
                new VisitViewModel
                {
                    ProcessStatus = EnumHelper.ToEnumList<VisitChangeStatusType>(),
                };
            return model;
        }


        public VisitReportViewModel InitReportData()
        {
            var model =
            new VisitReportViewModel
            {
                EventTypeList = EnumHelper.ToEnumList<EventType>(),
                InOutList = EnumHelper.ToEnumList<Antipass>(),
                ListCardStatus = EnumHelper.ToEnumList<CardStatus>(),
            };
            //var buildings = _unitOfWork.BuildingRepository.GetByCompanyId(_httpContext.User.GetCompanyId());
            var companyId = 1;
            var buildings = _unitOfWork.BuildingRepository.GetByCompanyId(companyId);
            var doors = _unitOfWork.IcuDeviceRepository.GetDoors();


            //doors = doors.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());

            if (doors != null)
            {
                model.DoorList = doors.Select(m => new SelectListItemModel
                {
                    Id = m.Id,
                    Name = m.Name
                }).ToList();
            }

            model.BuildingList = buildings.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.Name
            }).ToList();


            return model;

        }

        //public VisitPreRegisterModel InitPreRegisterData()
        //{
        //    var model =
        //        new VisitPreRegisterModel
        //        {
        //            ListCardStatus = EnumHelper.ToEnumList<VisitPreRegisterCardStatus>(),
        //        };
        //    return model;
        //}


        /// <summary>
        /// Add a visit
        /// </summary>
        /// <param name="visitModel"></param>
        public int Add(VisitModel visitModel)
        {
            Visit visit = null;
            VisitHistory logVisitHistory = null;

            visit = Mapper.Map<Visit>(visitModel);
            // Add date
            // start date is stored in DB as "DATE + 00:00:00"
            // end date is stored in DB as "DATE + 23:59:59"
            visit.StartDate = Helpers.GetFromToDateTime(visitModel.StartDate, "", false);
            visit.EndDate = Helpers.GetFromToDateTime(visitModel.EndDate, "", true);

            visit.ApproverId1 = visitModel.ApproverId1;
            visit.ApplyDate = DateTime.Now;
            visit.CompanyId = _httpContext.User.GetCompanyId();
            visit.IsDeleted = false;
            visit.CreatedBy = _httpContext.User.GetAccountId();
          
            visit.Status = (short) VisitChangeStatusType.Approved;

            var visitSetting = GetVisitSettingCompany();
            if (visitSetting.ApprovalStepNumber != (short) VisitSettingType.NoStep)
            {
                visit.Status = (short) VisitChangeStatusType.Waiting;
            }
                        
            _unitOfWork.VisitRepository.Add(visit);
            _unitOfWork.Save();
                        
            var companyId = _httpContext.User.GetCompanyId();

            var accessGroup = new AccessGroup
            {
                Name = Constants.Settings.NameAccessGroupVisitor + visit.Id,
                CompanyId = companyId,
                Type =  (short)AccessGroupType.VisitAccess
            };
            _unitOfWork.AccessGroupRepository.Add(accessGroup);
            _unitOfWork.Save();

            visit.AccessGroupId = accessGroup.Id;
            _unitOfWork.VisitRepository.Update(visit);
            _unitOfWork.Save();
            
            if (visitSetting.ApprovalStepNumber != (short) VisitSettingType.NoStep)
            {
                logVisitHistory = new VisitHistory
                {
                    VisitorId = visit.Id,
                    CompanyId = _httpContext.User.GetCompanyId(),
                    OldStatus  = null,
                    NewStatus = (short) VisitChangeStatusType.Waiting,
                    UpdatedBy = _httpContext.User.GetAccountId(),
                    CreatedOn = DateTime.UtcNow
                };
            }
            else
            {
                logVisitHistory = new VisitHistory
                {
                    VisitorId = visit.Id,
                    CompanyId = _httpContext.User.GetCompanyId(),
                    OldStatus  = null,
                    NewStatus = (short) VisitChangeStatusType.Approved,
                    UpdatedBy = _httpContext.User.GetAccountId(),
                    CreatedOn = DateTime.UtcNow
                };
            }
            
            _unitOfWork.AppDbContext.VisitHistory.Add(logVisitHistory);
            _unitOfWork.Save();

            // Send mail to approver
            _accountService.SendRequestApprovalMail(visit);

            return visit.Id;
        }


        public void PreRegister(VisitModel visitModel)
        {
            Visit visit = null;
            User visitor = null;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        visit = Mapper.Map<Visit>(visitModel);

                        visit.Status = (short)VisitStatus.PreRegister;
                        visit.ApplyDate = DateTime.Now;
                        visit.CompanyId = Constants.DefaultCompanyId;
                        visit.IsDeleted = false;


                        if (visitModel.CardStatus == (short)VisitingCardStatusType.NotUse)
                        {
                            visit.Status = (short)VisitStatus.NotUse;
                        }
                        else
                        {
                            visit.Status = (short)VisitStatus.PreRegister;
                        }

                        _unitOfWork.VisitRepository.Add(visit);

                        ////visit = _unitOfWork.VisitRepository.Get(m => m.StartDate == visit.StartDate && m.EndDate == visit.EndDate && m.CardId == visit.CardId && m.VisitorName == visit.VisitorName &&

                        //if (isProcedureOmission && visitModel.CardStatus == (short)VisitingCardStatusType.Delivered)
                        //{

                        //    //카드 존재 여부 확인
                        //    var isCardExist = _unitOfWork.CardRepository.IsCardIdExist(_httpContext.User.GetCompanyId(), visit.CardId);

                        //    if (isCardExist)
                        //    {
                        //        var card = _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(),
                        //            visit.CardId);

                        //        //방문자에게 '발급되지 않은' '방문증'일 경우에 처리 
                        //        if ((card.CardType == (int)PassType.VisitCard) && (card.VisitId == null))
                        //        {
                        //            card.VisitId = visit.Id;
                        //        }

                        //        visitor = _unitOfWork.UserRepository.GetByUserId(_httpContext.User.GetCompanyId(),
                        //            card.UserId);
                        //        _unitOfWork.CardRepository.Update(card);
                        //    }
                        //    else
                        //    {
                        //        //임시로 Default AccessGroup으로 할당
                        //        var defaultAccessGroup =
                        //            _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(_httpContext.User
                        //                .GetCompanyId());

                        //        var newUserCode =
                        //            _unitOfWork.UserRepository.GetNewUerCode(_httpContext.User.GetCompanyId());
                        //        //신규 방문자 User 등록
                        //        visitor = new User()
                        //        {
                        //            CompanyId = _httpContext.User.GetCompanyId(),
                        //            EffectiveDate = visit.StartDate,
                        //            ExpiredDate = visit.EndDate,
                        //            EmpNumber = visit.VisitorEmpNumber,
                        //            FirstName = VisitResource.lblVisit,
                        //            PassType = (short)PassType.VisitCard,
                        //            IssuedDate = DateTime.Now,
                        //            AccessGroupId = defaultAccessGroup.Id,
                        //            UserCode = String.Format("{0:000000}", newUserCode),
                        //            DepartmentId = 1,
                        //            PermissionType = (short)PermissionType.NotUse,

                        //        };
                        //        _unitOfWork.UserRepository.Add(visitor);
                        //        //카드 추가
                        //        var visitCard = new Card()
                        //        {
                        //            CompanyId = _httpContext.User.GetCompanyId(),
                        //            CardId = visit.CardId,
                        //            IssueCount = 0,
                        //            CardStatus = (short)CardStatus.Normal,
                        //            UserId = visitor.Id,
                        //            VisitId = visit.Id,
                        //            CardType = (int)(PassType.VisitCard),
                        //        };
                        //        _unitOfWork.CardRepository.Add(visitCard);


                        //    }

                        //}



                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            //전송
            if (visitor != null)
                SendUpdateUsersToAllDoors(visitor, true);
        }

        /// <summary>
        /// Update visit
        /// </summary>
        /// <param name="visitModel"></param>
        /// <returns></returns>
        public void Update(VisitModel visitModel)
        {
            Visit visit = null;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        visit = _unitOfWork.VisitRepository.GetByVisitId(_httpContext.User.GetCompanyId(), visitModel.Id);
                        
                        Mapper.Map(visitModel, visit);
                        visit.ApproverId1 = visitModel.ApproverId1;
                        _unitOfWork.VisitRepository.Update(visit);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            var card = _unitOfWork.AppDbContext.Card.FirstOrDefault(c => c.VisitId == visit.Id && !c.IsDeleted);
            if (card != null)
            {
                SendIdentificationToDevice(visit, card, true);
            }
        }

        /// <summary>
        /// change visit state
        /// </summary>
        /// <param name="actionId"></param>
        /// <returns></returns>
        public void ChangeStatus(int statusId, List<int> visitIds, string reason)
        {

            User visitor = null;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visits = _unitOfWork.VisitRepository.GetByVisitIds(_httpContext.User.GetCompanyId(), visitIds);

                        foreach (var visit in visits)
                        {
                            visit.Status = (short)statusId;

                            //if(statusId == (VisitStatus).ApprovalWaiting2)

                            switch (visit.Status)
                            {
                                case (short)VisitStatus.ApprovalWaiting2:
                                    visit.ApproverId1 = _httpContext.User.GetAccountId();
                                    visit.ApprovDate1 = DateTime.Now;
                                    break;
                                case (short)VisitStatus.Issueing:
                                    visit.ApproverId2 = _httpContext.User.GetAccountId();
                                    visit.ApprovDate2 = DateTime.Now;

                                    var card = _unitOfWork.CardRepository.GetByCardId(visit.CompanyId, visit.CardId);
                                    if (card != null)
                                        card.VisitId = visit.Id;

                                    if (!string.IsNullOrEmpty(visit.CardId))
                                    {
                                        visit.IssuerId = _httpContext.User.GetAccountId();
                                        visit.IssuedDate = DateTime.Now;
                                        visit.Status = (short)VisitStatus.DeliveryOk;
                                    }

                                    break;
                                //case (short)VisitStatus.DeliveryOk:
                                //    visit.IssuerId = _httpContext.User.GetAccountId();
                                //    visit.IssuedDate = DateTime.Now;
                                //    break;
                                case (short)VisitStatus.Return:
                                    visit.RejectorId = _httpContext.User.GetAccountId().ToString();
                                    visit.RejectReason = reason;
                                    visit.ReturnDate = DateTime.Now;
                                    break;
                                case (short)VisitStatus.Reject:
                                    visit.RejectorId = _httpContext.User.GetAccountId().ToString();
                                    visit.RejectReason = reason;
                                    visit.ReturnDate = DateTime.Now;
                                    break;
                                //case (short)VisitStatus.Reclamation:
                                //    visit.ReclaimerId = _httpContext.User.GetAccountId();
                                //    //visit.Re = DateTime.Now.ToString(Constants.DateTimeFormat.YyyyMMddHHmmss);
                                //        break;
                                default:
                                    break;
                            }

                            if (visit.Status == (short)VisitStatus.DeliveryOk)
                            {
                                var companyId = _httpContext.User.GetCompanyId();

                                //카드 존재 여부 확인
                                var isCardExist = _unitOfWork.CardRepository.IsCardIdExist(companyId, visit.CardId);

                                if (isCardExist)
                                {
                                    var card = _unitOfWork.CardRepository.GetByCardId(companyId, visit.CardId);

                                    //방문자에게 '발급되지 않은' '방문증'일 경우에 처리 
                                    if ((card.CardType == (int)PassType.VisitCard) && (card.VisitId == null))
                                    {
                                        card.VisitId = visit.Id;
                                    }

                                    visitor = _unitOfWork.UserRepository.GetByUserId(companyId, card.UserId.Value);

                                    _unitOfWork.CardRepository.Update(card);
                                }
                                else
                                {
                                    //Visit AccessGroup으로 할당
                                    var visitAccessGroup =
                                        _unitOfWork.AccessGroupRepository.GetVisitAccessGroup(companyId);

                                    var newUserCode =
                                        _unitOfWork.UserRepository.GetNewUserCode(companyId);
                                    //신규 방문자 User 등록
                                    visitor = new User()
                                    {
                                        CompanyId = companyId,
                                        EffectiveDate = visit.StartDate,
                                        ExpiredDate = visit.EndDate,
                                        EmpNumber = visit.VisitorEmpNumber,
                                        FirstName = VisitResource.lblVisit,
                                        PassType = (short)PassType.VisitCard,
                                        IssuedDate = DateTime.Now,
                                        AccessGroupId = visitAccessGroup.Id,
                                        UserCode = String.Format("{0:000000}", newUserCode),
                                        DepartmentId = 1,
                                        BirthDay = DateTime.Now

                                    };
                                    _unitOfWork.UserRepository.Add(visitor);
                                    //카드 추가
                                    var visitCard = new Card()
                                    {
                                        CompanyId = companyId,
                                        CardId = visit.CardId,
                                        IssueCount = 0,
                                        CardStatus = (short)CardStatus.Normal,
                                        UserId = visitor.Id,
                                        VisitId = visit.Id,
                                        CardType = (int)(PassType.VisitCard),
                                    };
                                    _unitOfWork.CardRepository.Add(visitCard);


                                }
                            }
                            else if (visit.Status >= (short)VisitStatus.Return)
                            {
                                //카드 존재 여부 확인
                                var isCardExist = _unitOfWork.CardRepository.IsCardIdExist(_httpContext.User.GetCompanyId(), visit.CardId);

                                if (isCardExist)
                                {
                                    var card = _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(),
                                        visit.CardId);

                                    if ((card.CardType == (int)PassType.VisitCard) && (card.VisitId == null))
                                    {
                                        card.VisitId = null;
                                    }


                                    _unitOfWork.CardRepository.Update(card);
                                }
                            }
                            _unitOfWork.VisitRepository.Update(visit);
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            //전송
            if (visitor != null)
                SendUpdateUsersToAllDoors(visitor, true);
        }


        public void ReturnCard(string cardId)
        {

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {

                        var visit = _unitOfWork.VisitRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);
                        var card = _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);



                        visit.ReclaimerId = _httpContext.User.GetAccountId();
                        visit.Status = (short)VisitStatus.Reclamation;



                        card.VisitId = null;

                        _unitOfWork.VisitRepository.Update(visit);
                        _unitOfWork.CardRepository.Update(card);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public void PassOnCard(int visitId, string cardId)
        {
            User visitor = null;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();

                        var visit = _unitOfWork.VisitRepository.GetById(visitId);
                        //var card = _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);

                        visit.IssuerId = _httpContext.User.GetAccountId();
                        visit.Status = (short)VisitStatus.DeliveryOk;

                        visit.CardId = cardId;
                        //카드 존재 여부 확인
                        var isCardExist = _unitOfWork.CardRepository.IsCardIdExist(companyId, cardId);

                        if (isCardExist)
                        {
                            var card = _unitOfWork.CardRepository.GetByCardId(companyId, visit.CardId);

                            //방문자에게 '발급되지 않은' '방문증'일 경우에 처리 
                            if ((card.CardType == (int)PassType.VisitCard) && (card.VisitId == null))
                            {
                                card.VisitId = visit.Id;
                            }

                            visitor = _unitOfWork.UserRepository.GetByUserId(companyId, card.UserId.Value);

                            _unitOfWork.CardRepository.Update(card);
                        }
                        else
                        {

                            //Visit AccessGroup으로 할당
                            var visitAccessGroup =
                                _unitOfWork.AccessGroupRepository.GetVisitAccessGroup(companyId);

                            var newUserCode =
                                _unitOfWork.UserRepository.GetNewUserCode(companyId);
                            //신규 방문자 User 등록
                            visitor = new User()
                            {
                                CompanyId = companyId,
                                EffectiveDate = visit.StartDate,
                                ExpiredDate = visit.EndDate,
                                EmpNumber = visit.VisitorEmpNumber,
                                FirstName = VisitResource.lblVisit,
                                PassType = (short)PassType.VisitCard,
                                IssuedDate = DateTime.Now,
                                AccessGroupId = visitAccessGroup.Id,
                                UserCode = String.Format("{0:000000}", newUserCode),
                                DepartmentId = 1,
                                BirthDay = DateTime.Now,


                            };
                            _unitOfWork.UserRepository.Add(visitor);

                            //카드 추가
                            var visitCard = new Card()
                            {
                                CompanyId = companyId,
                                CardId = visit.CardId,
                                IssueCount = 0,
                                CardStatus = (short)CardStatus.Normal,
                                UserId = visitor.Id,
                                VisitId = visit.Id,
                                CardType = (int)(PassType.VisitCard),
                            };
                            _unitOfWork.CardRepository.Add(visitCard);
                        }

                        _unitOfWork.VisitRepository.Update(visit);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            //전송
            if (visitor != null)
                SendUpdateUsersToAllDoors(visitor, true);
        }


        /// <summary>
        /// Get visit by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Visit GetById(int id)
        {
            var timezone = _unitOfWork.AccountRepository.Get(m =>
                m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone;

            var visit = _unitOfWork.VisitRepository.GetByVisitId(_httpContext.User.GetCompanyId(), id);

            //visit.BirthDay = Helpers.ConvertToUserTimeZoneReturnDate(visit.BirthDay, timezone);
            //visit.StartDate = Helpers.ConvertToUserTimeZoneReturnDate(visit.StartDate, timezone);
            //visit.EndDate = Helpers.ConvertToUserTimeZoneReturnDate(visit.EndDate, timezone);

            return visit;
        }

        public Visit GetByCardId(string cardId)
        {
            return _unitOfWork.VisitRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);
        }

        public Visit GetByCardIdExceptThis(int id, string cardId)
        {
            return _unitOfWork.VisitRepository.GetByCardIdExceptThis(_httpContext.User.GetCompanyId(), id, cardId);
        }


        public List<Visit> GetByIds(List<int> ids)
        {
            return _unitOfWork.VisitRepository.GetByVisitIds(_httpContext.User.GetCompanyId(), ids);
        }

        /// <inheritdoc />
        public void InitData(VisitDataModel visitModel)
        {
            visitModel.VisitTypes = EnumHelper.ToEnumList<VisitType>();

            visitModel.ListCardStatus = EnumHelper.ToEnumList<VisitingCardStatusType>();

            //visitModel.ListVisitPreregisterCardStatus = EnumHelper.ToEnumList<VisitPreRegisterCardStatus>();

            var accessGroups = _accessGroupService.GetListAccessGroups()
                .Select(Mapper.Map<AccessGroupModelForUser>).ToList();

            var companyId = _httpContext.User.GetCompanyId();

            var accessGroupDefault =
                _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(companyId);
            if (visitModel.AccessGroupId == 0 && accessGroupDefault != null)
            {
                visitModel.AccessGroupId = accessGroupDefault.Id;
                var index = accessGroups.FindIndex(x => x.Id == accessGroupDefault.Id);
                var item = accessGroups[index];
                accessGroups[index] = accessGroups[0];
                accessGroups[0] = item;
            }

            visitModel.AccessGroups = accessGroups;
            if (visitModel.Id == 0 && string.IsNullOrEmpty(visitModel.CardId))
            {
                var cardId = _httpContext.Request.Query["cardId"].ToString();
                if (!string.IsNullOrEmpty(cardId))
                {
                    visitModel.CardId = cardId;
                }

                visitModel.VisitType = 1;
            }
        }





        /// <summary>
        /// Get data with pagenation
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<Visit> GetPaginated(string opeDateFrom,
            string opeDateTo, string opeTimeFrom, string opeTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrder(opeDateFrom,
            opeDateTo, opeTimeFrom, opeTimeTo, visitorName,
            birthDay, visitorDepartment, position, visiteeSite, visitReason,
            visiteeName, phone, cardStatus, approverName1, approverName2,
            rejectReason, cardId, sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }

        /// <summary>
        /// Filter and order data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        internal IQueryable<Visit> FilterDataWithOrder(string opeDateFrom, string opeDateTo, string opeTimeFrom,
            string opeTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> status, string approverName1, string approverName2,
            string rejectReason, string cardId, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.VisitRepository.GetByCompanyId(companyId);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(opeDateFrom))
            {
                var opeDateTimeFrom = Helpers.GetFromToDateTime(opeDateFrom, opeTimeFrom, false);
                data = data.Where(m =>
                    m.ApplyDate >= opeDateTimeFrom);
            }

            if (!string.IsNullOrEmpty(opeDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(opeDateTo, opeTimeTo, true);
                data = data.Where(m =>
                    m.ApplyDate <= accessDateTimeTo);
            }

            if (!string.IsNullOrEmpty(visitorName))
            {
                data = data.Where(m => m.VisitorName.Contains(visitorName));
            }

            if (!string.IsNullOrEmpty(birthDay))
            {
                data = data.Where(m => m.BirthDay.ToSettingDateString().Contains(birthDay));
            }

            if (!string.IsNullOrEmpty(visitorDepartment))
            {
                data = data.Where(m => m.VisitorDepartment.Contains(visitorDepartment));
            }

            if (!string.IsNullOrEmpty(position))
            {
                data = data.Where(m => m.Position.Contains(position));
            }

            if (!string.IsNullOrEmpty(visiteeSite))
            {
                data = data.Where(m => m.VisiteeSite.Contains(visiteeSite));
            }

            if (!string.IsNullOrEmpty(visitReason))
            {
                data = data.Where(m => m.VisitReason.Contains(visitReason));
            }

            if (!string.IsNullOrEmpty(visiteeName))
            {
                data = data.Where(m => m.VisiteeName.Contains(visiteeName));
            }

            if (!string.IsNullOrEmpty(phone))
            {
                data = data.Where(m => m.Phone.Contains(phone));
            }

            if (status != null && status.Any())
            {
                data = data.Where(m => status.Contains(m.Status));
            }
            
            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.Visit.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Visit[sortColumn]} {sortDirection}");

            foreach (var visit in data)
            {
                if (visit.Status == (short) VisitChangeStatusType.Approved)
                {
                    visit.IsDecision = false;
                }
                else
                {
                    var visitSetting = GetVisitSettingCompany();
                    if (visitSetting.ApprovalStepNumber == (short) VisitSettingType.FirstStep)
                    {
                        if (visit.Status == (short) VisitChangeStatusType.Waiting)
                        {
                            if (visit.ApproverId1 != _httpContext.User.GetAccountId())
                            {
                                visit.IsDecision = false;
                            }
                            else
                            {
                                visit.IsDecision = true;
                            }
                        }
                        else
                        {
                            visit.IsDecision = false;
                        }
                        
                    }
                    if (visitSetting.ApprovalStepNumber == (short) VisitSettingType.SecondsStep)
                    {
                        if (visit.Status == (short) VisitChangeStatusType.Waiting)
                        {
                            if (visit.ApproverId1 != _httpContext.User.GetAccountId())
                            {
                                visit.IsDecision = false;
                            }
                            else
                            {
                                visit.IsDecision = true;
                            }
                        }
                        else if (visit.Status == (short) VisitChangeStatusType.Approved1)
                        {
                            var secondApprovalAccounts = JsonConvert.DeserializeObject<List<int>>(visitSetting.SecondsApproverAccounts);
                            if (!secondApprovalAccounts.Contains(_httpContext.User.GetAccountId()))
                            {
                                visit.IsDecision = false;
                            }
                            else
                            {
                                visit.IsDecision = true;
                            }
                        }
                        else
                        {
                            visit.IsDecision = false;
                        }
                    }
                }

                var card = _unitOfWork.AppDbContext.Card.Where(c => c.VisitId == visit.Id && !c.IsDeleted).FirstOrDefault();
                visit.CardId = card != null ? card.CardId : "";
                visit.BirthDay = Helpers.ConvertToUserTimeZoneReturnDate(visit.BirthDay, _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
                visit.StartDate = Helpers.ConvertToUserTimeZoneReturnDate(visit.StartDate, _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
                visit.EndDate = Helpers.ConvertToUserTimeZoneReturnDate(visit.EndDate, _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
            }

            return data;
        }


        public IQueryable<EventLog> VisitReport(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName,
            string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int pageNumber,
            int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrderForVisitReport(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, doorIds, visitorName,
                cardId, inOutType, eventType, visiteeSite, cardStatus, birthDay, visitorDepartment, pageNumber, pageSize,
                sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }

        /// <summary>
        /// Filter data
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="doorIds"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public IQueryable<EventLog> FilterDataWithOrderForVisitReport(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName,
            string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int pageNumber,
            int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.EventLog
                .Include(m => m.Visit)
                .Include(m => m.Icu)
                .Include(m => m.Icu.Building)
                .Include(m => m.Company)
                .Include(m => m.Visit)
                .AsQueryable();

            data = data.Where(m => m.CompanyId == _httpContext.User.GetCompanyId() && m.IsVisit && m.VisitId != null);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
                data = data.Where(m => m.EventTime >= accessDateTimeFrom);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
                data = data.Where(m => m.EventTime <= accessDateTimeTo);
            }

            if (eventType != null && eventType.Any())
            {
                data = data.Where(m => eventType.Contains(m.EventType));
            }

            if (!string.IsNullOrEmpty(visitorName))
            {
                data = data.Where(m => m.Visit.VisitorName.Contains(visitorName));
            }

            if (!string.IsNullOrEmpty(inOutType))
            {
                var inOutTypeInt = Convert.ToInt32(inOutType);
                data = data.Where(m => m.Antipass == ((Antipass)inOutTypeInt).GetDescription());
            }

            if (!string.IsNullOrEmpty(cardId))
            {
                data = data.Where(m => m.CardId.Contains(cardId));
            }

            if (doorIds != null && doorIds.Any())
            {
                data = data.Where(m => doorIds.Contains(m.IcuId));
            }

            if (!string.IsNullOrEmpty(visiteeSite))
            {
                data = data.Where(m => m.Visit.VisiteeSite.Contains(visiteeSite));
            }

            if (!string.IsNullOrEmpty(birthDay))
            {
                data = data.Where(m => m.Visit.BirthDay.ToSettingDateString().Contains(birthDay));
            }

            if (!string.IsNullOrEmpty(visitorDepartment))
            {
                data = data.Where(m => m.Visit.VisitorDepartment.Contains(visitorDepartment));
            }


            //data= data.Where(m => cardStatus.Contains(GetCardStatus(m.CardId)));

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.VisitReport.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.VisitReport[sortColumn]} {sortDirection}");
            return data;
        }


        public int GetCardStatus(string cardId)
        {

            var user = _unitOfWork.UserRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);


            if (user != null)
                return _unitOfWork.CardRepository.GetCardStatusByUserIdAndCardId(_httpContext.User.GetCompanyId(),
                    user.Id, cardId);
            else
                return (int)(CardStatus.Normal);
        }


        public string GetBuildingNameByRid(string deviceAddress)
        {
            return _unitOfWork.BuildingRepository.GetBuildingNameByRid(_httpContext.User.GetCompanyId(), deviceAddress);
        }


        /// <summary>
        /// Send user to device when the user is added
        /// </summary>
        private void SendUpdateUsersToAllDoors(User user, bool isAddUser)
        {
            if (user == null)
            {
                _logger.LogError("Can not find this user in system");
                return;
            }
            var agDevices =
                _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(),
                    user.AccessGroupId);

            var userList = new List<User>();

            if (agDevices != null && agDevices.Any())
            {
                foreach (var agDevice in agDevices)
                {
                    _accessGroupService.SendAddOrDeleteUser(agDevice, new List<User> { user }, isAddUser:isAddUser);
                    
                }
            }
        }

        /// <summary> 
        /// Export department data to txt file 
        /// </summary> 
        /// <param name="filter"></param> 
        /// <param name="sortColumn"></param> 
        /// <param name="sortDirection"></param> 
        /// <param name="totalRecords"></param> 
        /// <param name="recordsFiltered"></param> 
        /// <returns></returns> 
        public byte[] ExportTxt(string opeDateFrom, string opeDateTo, string opeTimeFrom,
            string opeTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var visits = FilterDataWithOrder(opeDateFrom,
            opeDateTo, opeTimeFrom, opeTimeTo, visitorName,
            birthDay, visitorDepartment, position, visiteeSite, visitReason,
            visiteeName, phone, cardStatus, approverName1, approverName2,
            rejectReason, cardId, sortColumn, sortDirection, out totalRecords, out recordsFiltered)
                .ToList().Select(x => new object[]
                {
                    x.Id,
                    x.ApplyDate,//신청기간 
                    x.VisitorName,//이름 
                    x.BirthDay,//생년월일 
                    x.VisitorDepartment,//소속 
                    x.Position,//직책 
                    x.StartDate,//방문시작일 
                    x.EndDate,//방문종료일 
                    x.VisiteeSite,//방문장소 
                    x.VisitReason,//방문목적 
                    x.VisiteeName,//방문대상 
                    x.Phone,//연락처 
                    x.VisitingCardState,//상태 
                    //x.ApproverId1,//결재자 
                    _unitOfWork.AccountRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(),
                        x.ApproverId1)?.Username,
                    //x.ApproverId2,//승인자 
                    _unitOfWork.AccountRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(),
                        x.ApproverId2)?.Username,
                    x.RejectReason,//반려및 거절사유 
                    x.CardId,//cardid 
 
 
                 }).ToList();

            // Build the file content 
            var visitTxt = new StringBuilder();
            visits.ForEach(line =>
            {
                visitTxt.AppendLine(string.Join(",", line));
            });

            byte[] buffer = Encoding.GetEncoding("euc-kr").GetBytes($"{string.Join(",", _header)}\r\n{visitTxt}");
            return buffer;
        }
        public byte[] Export(string type, string opeDateFrom, string opeDateTo, string opeTimeFrom,
            string opeTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var fileByte = type == Constants.Excel
                ? ExportExcel(opeDateFrom, opeDateTo, opeTimeFrom,
            opeTimeTo, visitorName,
            birthDay, visitorDepartment, position, visiteeSite, visitReason,
            visiteeName, phone, cardStatus, approverName1, approverName2,
             rejectReason, cardId, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered)
                : ExportTxt(opeDateFrom, opeDateTo, opeTimeFrom,
            opeTimeTo, visitorName,
            birthDay, visitorDepartment, position, visiteeSite, visitReason,
            visiteeName, phone, cardStatus, approverName1, approverName2,
            rejectReason, cardId, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered);

            //Save system log 
            var companyId = _httpContext.User.GetCompanyId();
            var content =
                VisitResource.msgExportVisitList + ":" + DateTime.Now.ToSettingDateString() + "(" +
                VisitResource.msgLogin + ":" + _httpContext.User.GetUsername() + ")";
            _unitOfWork.SystemLogRepository.Add(companyId, SystemLogType.Department, ActionLogType.Export, content, null, null, _httpContext.User.GetCompanyId());
            _unitOfWork.Save();

            return fileByte;
        }
        private readonly string[] _header =
        {

            VisitResource.lblID,
            VisitResource.lblApplyDate,
            VisitResource.lblVisitName,
            VisitResource.lblBirthDay,
            VisitResource.lblDepartment,
            VisitResource.lblPosition,
            VisitResource.lblStartDate,
            VisitResource.lblEndDate,
            VisitResource.lblSite,
            VisitResource.lblReason,
            VisitResource.lblVisitPerson,
            VisitResource.lblPhone,
            VisitResource.lblVisitStatus,
            VisitResource.lblApproval1,
            VisitResource.lblApproval2,
            VisitResource.lblRejectReason,
            VisitResource.lblRejectCardID,

        };

        public byte[] ExportExcel(string opeDateFrom, string opeDateTo, string opeTimeFrom,
            string opeTimeTo, string visitorName,
            string birthDay, string visitorDepartment, string position, string visiteeSite, string visitReason,
            string visiteeName, string phone, List<int> cardStatus, string approverName1, string approverName2,
            string rejectReason, string cardId, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            byte[] result;
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(DepartmentResource.lblDepartment); //Worksheet name

                var visits = FilterDataWithOrder(opeDateFrom,
            opeDateTo, opeTimeFrom, opeTimeTo, visitorName,
            birthDay, visitorDepartment, position, visiteeSite, visitReason,
            visiteeName, phone, cardStatus, approverName1, approverName2,
            rejectReason, cardId, sortColumn, sortDirection, out totalRecords, out recordsFiltered)
                    .ToList();

                //First add the headers for user sheet 
                for (var i = 0; i < _header.Length; i++)
                {
                    //worksheet.Cells[1, i + 1].Value = _header[i];
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }
                var recordIndex = 2;

                foreach (var visit in visits)
                {
                    var approver1 =
                        _unitOfWork.AccountRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(),
                            visit.ApproverId1);
                    var approver2 =
                        _unitOfWork.AccountRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(),
                            visit.ApproverId2);

                    //For the Department sheet
                    var colIndex = 1;
                    //worksheet.Cells[recordIndex, colIndex++].Value = department.DepartNo; 
                    //worksheet.Cells[recordIndex, colIndex].Value = department.DepartName; 
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Id;//진행번호
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.ApplyDate.ToSettingDateTimeString();//신청기간
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.VisitorName;//이름
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.BirthDay.ToSettingDateString();//생년월일
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.VisitorDepartment;//소속
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Position;//직책
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.StartDate.ToSettingDateString();//방문시작일
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.EndDate.ToSettingDateString();//방문종료일 
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.VisiteeSite;//방문장소 
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.VisitReason;//방문목적
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.VisiteeName;//방문대상 
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Phone;//연락처
                    worksheet.Cells[recordIndex, colIndex++].Value = ((VisitStatus)visit.Status).GetDescription();//상태 
                    //worksheet.Cells[recordIndex, colIndex++].Value = visit.ApproverId1;//결재자
                    //worksheet.Cells[recordIndex, colIndex++].Value = visit.ApproverId2;//승인자 
                    worksheet.Cells[recordIndex, colIndex++].Value = approver1?.Username;//결재자
                    worksheet.Cells[recordIndex, colIndex++].Value = approver2?.Username;//승인자 
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.RejectReason;//반려및 거절사유
                    worksheet.Cells[recordIndex, colIndex].Value = visit.CardId;//cardid




                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        public byte[] ExportVisitReport(string type, string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var fileByte = type == Constants.Excel
                ? ExportVisitReportExcel(accessDateFrom, accessDateTo, accessTimeFrom,
                    accessTimeTo, doorIds,
                    visitorName, cardId, inOutType, eventType, visiteeSite,
                    cardStatus, birthDay, visitorDepartment, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered)
                : ExportVisitReportTxt(accessDateFrom, accessDateTo, accessTimeFrom,
                    accessTimeTo, doorIds,
                    visitorName, cardId, inOutType, eventType, visiteeSite,
                    cardStatus, birthDay, visitorDepartment, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered);

            //Save system log 
            //var companyId = _httpContext.User.GetCompanyId();
            //var content =
            //    VisitResource.msgExportVisitReportList + ":" + DateTime.Now.ToSettingDateString() + "(" +
            //    VisitResource.msgLogin + ":" + _httpContext.User.Identity.Name + ")";
            //_unitOfWork.SystemLogRepository.Add(companyId, SystemLogType.Department, ActionLogType.Export, content);//Department 추후 수정 필요
            //_unitOfWork.Save();

            return fileByte;
        }

        public byte[] ExportVisitReportExcel(string accessDateFrom,
           string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType,
           string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int sortColumn, string sortDirection, out int totalRecords,
           out int recordsFiltered)
        {
            byte[] result;
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(DepartmentResource.lblDepartment); //Worksheet name

                var visits = FilterDataWithOrderForVisitReport(accessDateFrom,
            accessDateTo, accessTimeFrom, accessTimeTo, doorIds,
            visitorName, cardId, inOutType, eventType, visiteeSite,
            cardStatus, birthDay, visitorDepartment, 0, 0, sortColumn, sortDirection, out totalRecords, out recordsFiltered)
                    .ToList();

                //First add the headers for user sheet 
                for (var i = 0; i < _header2.Length; i++)
                {
                    //worksheet.Cells[1, i + 1].Value = _header[i];
                    worksheet.Cells[1, i + 1].Value = _header2[i];
                }
                var recordIndex = 2;

                foreach (var visit in visits)
                {
                    //For the Department sheet
                    var colIndex = 1;
                    //worksheet.Cells[recordIndex, colIndex++].Value = department.DepartNo; 
                    //worksheet.Cells[recordIndex, colIndex].Value = department.DepartName; 
                    worksheet.Cells[recordIndex, colIndex++].Value = recordIndex - 1;//idx
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.EventTime.ToSettingDateTimeString();//출입시간
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Visit?.VisitorName;//이름
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Visit?.BirthDay.ToSettingDateString();//생년월일
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Visit?.VisitorDepartment;//소속
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.CardId;//카드ID
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Icu.DeviceAddress;//RID
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Icu.Name;//출입문 이름 
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Visit?.VisiteeSite;//장소
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Antipass;//입/출
                    worksheet.Cells[recordIndex, colIndex++].Value = ((EventType)visit.EventType).GetDescription();//이벤트 종류
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.IssueCount;//발급차수
                    worksheet.Cells[recordIndex, colIndex++].Value = visit.Visit != null ? ((CardStatus)visit.Visit?.CardStatus).GetDescription() : "";//카드상태 


                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        public byte[] ExportVisitReportTxt(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string visitorName, string cardId, string inOutType, List<int> eventType,
            string visiteeSite, List<int> cardStatus, string birthDay, string visitorDepartment, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var visit = FilterDataWithOrderForVisitReport(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, doorIds, visitorName,
                cardId, inOutType, eventType, visiteeSite, cardStatus, birthDay, visitorDepartment, 0, 0,
                sortColumn, sortDirection, out totalRecords, out recordsFiltered)
                .ToList().Select(x => new object[]
                {
                    /*x.Id,
                    x.,//신청기간 
                    x.VisitorName,//이름 
                    x.BirthDay,//생년월일 
                    x.VisitorDepartment,//소속 
                    x.Position,//직책 
                    x.StartDate,//방문시작일 
                    x.EndDate,//방문종료일 
                    x.VisiteeSite,//방문장소 
                    x.VisitReason,//방문목적 
                    x.VisiteeName,//방문대상 
                    x.Phone,//연락처 
                    x.VisitingCardState,//상태 
                    x.ApproverId1,//결재자 
                    x.ApproverId2,//승인자 
                    x.RejectReason,//반려및 거절사유 
                    x.CardId,//cardid */
                    
                     
                    x.EventTime.ToSettingDateTimeString(),//출입시간 
                    x.Visit?.VisitorName,//이름 
                    x.Visit?.BirthDay.ToSettingDateString(),//생년월일 
                    x.Visit?.VisitorDepartment,//소속 
                    x.CardId,//카드ID 
                    x.Icu?.DeviceAddress,//RID 
                    x.Icu?.Name,//출입문 이름  
                    x.Visit?.VisiteeSite,//장소 
                    x.Antipass,//입/출 
                    ((EventType)x.EventType).GetDescription(),//이벤트 종류 
                    x.IssueCount,//발급차수 
                    x.Visit != null ? ((CardStatus)x.Visit?.CardStatus).GetDescription() : "",
                    //((CardStatus)x.Visit?.CardStatus).GetDescription(),//카드상태  
 
 
                    }).ToList();

            // Build the file content 
            var visitTxt = new StringBuilder();
            visit.ForEach(line =>
            {
                visitTxt.AppendLine(string.Join(",", line));
            });

            byte[] buffer = Encoding.GetEncoding("euc-kr").GetBytes($"{string.Join(",", _header)}\r\n{visitTxt}");
            return buffer;
        }


        public bool IsCardIdValid(VisitModel visitModel)
        {
            if (visitModel.CardStatus != (short)VisitingCardStatusType.NotUse && string.IsNullOrEmpty(visitModel.CardId))
            {
                return false;
            }

            return true;
        }

        //public bool IsCardIdValid(string cardId)
        //{
        //    if (visitModel.CardStatus != (short)VisitingCardStatusType.NotUse && string.IsNullOrEmpty(visitModel.CardId))
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        public bool IsProcedureOmission(int companyId)
        {
            return Convert.ToBoolean(_settingService.GetValueFromKey(Constants.Settings.ProcedureOmission, companyId));
        }

        public Card GetCardByCardId(string cardId)
        {
            return _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);
        }

        public VisitSetting GetVisitSettingCompany()
        {
            return _unitOfWork.VisitRepository.GetVisitSetting(_httpContext.User.GetCompanyId());
        }

        public void UpdateVisitSettingCompany(VisitSettingModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var setting = GetVisitSettingCompany();
                        setting.CompanyId = _httpContext.User.GetCompanyId();
                        setting.FirstApproverAccounts = model.FirstApproverAccounts;
                        setting.SecondsApproverAccounts = model.SecondsApproverAccounts;
                        setting.ApprovalStepNumber = model.ApprovalStepNumber;
                        setting.OutSide = model.OutSide;
                        _unitOfWork.VisitRepository.UpdateVisitSetting(setting);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public void UpdateApprovalVisitor(int id, short status)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visitor = GetById(id);
                        var oldStatus = visitor.Status;
                        visitor.Status = status;
                        _unitOfWork.VisitRepository.Update(visitor);

                        var log = new VisitHistory
                        {
                            VisitorId = visitor.Id,
                            CompanyId = _httpContext.User.GetCompanyId(),
                            OldStatus  = oldStatus,
                            NewStatus = visitor.Status,
                            UpdatedBy = _httpContext.User.GetAccountId(),
                            CreatedOn = DateTime.UtcNow
                        };
                        _unitOfWork.AppDbContext.VisitHistory.Add(log);
                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public Card GetCardByVisitor(int id)
        {
            return _unitOfWork.AppDbContext.Card.FirstOrDefault(c => !c.IsDeleted && c.VisitId == id);
        }

        /// <summary>
        /// Assign card to visitor
        /// </summary>
        /// <param name="visitId"> identifier of visitor </param>
        /// <param name="model"> CardModel </param>
        /// <returns></returns>
        public int AssignedCardVisitor(int visitId, CardModel model)
        {
            int cardId = 0;
            var visitor = GetById(visitId);
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        // Add card
                        var card = new Card
                        {
                            CardId = model.CardId,
                            IssueCount = 0,
                            CompanyId = visitor.CompanyId,
                            VisitId = visitId,
                            CardType = (short) CardType.NFC
                        };
                        _unitOfWork.CardRepository.Add(card);

                        //// Send QR to visitor through email.
                        //string qrId = GenQrId();
                        //card.CardId = model.CardId;

                        //var company = _unitOfWork.CompanyRepository.GetById(visitor.CompanyId);
                        //var companyLanguage = _unitOfWork.SettingRepository.GetLanguage(visitor.CompanyId);
                        //var culture = new CultureInfo(Helpers.GetStringFromValueSetting(companyLanguage.Value));
                        //byte[] qrCodeAsPngByteArr = EncryptQrCode(company.SecretCode, qrId);

                        //using (var ms = new MemoryStream(qrCodeAsPngByteArr))
                        //{
                        //    Image qrCodeImage = new Bitmap(ms);

                        //    _accountService.SendQRMail(visitor, qrCodeImage);
                        //}

                        //// Add QR
                        //var qrCode = new Card
                        //{
                        //    CardId = qrId,
                        //    IssueCount = 0,
                        //    CompanyId = visitor.CompanyId,
                        //    VisitId = visitId,
                        //    CardType = (short)CardType.QrCode
                        //};
                        //_unitOfWork.CardRepository.Add(qrCode);

                        _unitOfWork.Save();

                        var oldStatus = visitor.Status;
                        visitor.Status = (short) VisitChangeStatusType.CardIssued;
                        visitor.CardId = model.CardId;
                        
                        var log = new VisitHistory
                        {
                            VisitorId = visitor.Id,
                            CompanyId = visitor.CompanyId,
                            OldStatus = oldStatus,
                            NewStatus = visitor.Status,
                            UpdatedBy = _httpContext.User.GetAccountId(),
                            CreatedOn = DateTime.UtcNow
                        };
                        _unitOfWork.AppDbContext.VisitHistory.Add(log);
                        
                        _unitOfWork.VisitRepository.Update(visitor);
                        _unitOfWork.Save();
                        
                        transaction.Commit();
                        cardId = card.Id;
                        
                        var agDevices =
                            _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(visitor.CompanyId,
                                visitor.AccessGroupId);
                        
                        if (agDevices != null && agDevices.Any())
                        {
                            foreach (var agDevice in agDevices)
                            {
                                _accessGroupService.SendVisitor(agDevice, true, visitor);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            
            return cardId;
        }
        
        private void SendIdentificationToDevice(Visit visitor, Card newCard, bool isAdd)
        {
            if (visitor == null)
            {
                _logger.LogError("Can not find this visitor in system");
                return;
            }

            var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(), visitor.AccessGroupId);

            if (agDevices != null && agDevices.Any())
            {
                foreach (var agDevice in agDevices)
                {
                    _accessGroupService.SendIdentificationToDeviceVisitor(agDevice, visitor, newCard, isAdd);
                }
            }
        }

        /// <summary>
        /// Generate QR
        /// </summary>
        /// <returns></returns>
        internal string GenQrId()
        {
            var chars = Constants.Settings.CharacterGenQrCode;
            var stringChars = new char[Constants.Settings.LengthCharacterGenQrCode];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            if (_unitOfWork.CardRepository.CheckIsExistedQrCode(finalString) != true)
            {
                GenQrId();
            }
            return finalString;
        }

        /// <summary>
        /// Encrypt QR code
        /// </summary>
        /// <param name="companySecretCode"></param>
        /// <param name="qrId"></param>
        /// <returns></returns>
        internal byte[] EncryptQrCode(string companySecretCode, string qrId)
        {
            RijndaelManaged aes = new RijndaelManaged
            {
                BlockSize = Constants.AutoRenew.BlockSize,
                KeySize = Constants.AutoRenew.KeySize,
                Mode = CipherMode.ECB
            };

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData genQrCode =
                qrGenerator.CreateQrCode(qrId,
                    QRCodeGenerator.ECCLevel.Q);
            PngByteQRCode qrCode = new PngByteQRCode(genQrCode);
            byte[] qrCodeAsPngByteArr = qrCode.GetGraphic(20);
            return qrCodeAsPngByteArr;
        }

        /// <summary>
        /// Assign doors to visitor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="doors"></param>
        /// <returns></returns>
        public bool AssignedDoorVisitor(int id, List<int> doors)
        {
            var visitor = GetById(id);
            var card = _unitOfWork.AppDbContext.Card.Where(c => !c.IsDeleted && c.VisitId == id).ToList();

            if (card.Count > 1 || card.Count == 0)
            {
                return false;
            }
            else
            {
                _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
                {
                    using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                    {
                        try
                        {
                            var companyId = _httpContext.User.GetCompanyId();

                            var accessGroup = new AccessGroup
                            {
                                Name = Constants.Settings.NameAccessGroupVisitor + id,
                                CompanyId = companyId,
                                Type =  (short)AccessGroupType.VisitAccess
                            };
                            _unitOfWork.AccessGroupRepository.Add(accessGroup);
                            _unitOfWork.Save();
                            
                            visitor.AccessGroupId = accessGroup.Id;
                            _unitOfWork.VisitRepository.Update(visitor);
                            _unitOfWork.Save();

                            foreach (var door in doors)
                            {
                                var detailDoor = _unitOfWork.IcuDeviceRepository.GetById(door);
                                var detailModel = new AccessGroupDevice
                                {
                                    IcuId = detailDoor.Id,
                                    TzId = detailDoor.ActiveTzId.Value,
                                    AccessGroupId = visitor.AccessGroupId
                                };
                                _unitOfWork.AccessGroupDeviceRepository.Add(detailModel);
                            }
                            
                            _unitOfWork.Save();

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                });
                SendIdentificationToDevice(visitor, card.FirstOrDefault(), true);
                return true;
            }
        }

        public bool RejectVisitor(int id, RejectedModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visitor = GetById(id);
                        
                        var oldStatus = visitor.Status;
                        
                        visitor.RejectDate = DateTime.Now;
                        if (!string.IsNullOrEmpty(model.Reason))
                        {
                            visitor.RejectReason = model.Reason;
                        }
                        visitor.Status = (short) VisitChangeStatusType.Rejected;
                        _unitOfWork.VisitRepository.Update(visitor);
                        
                        var log = new VisitHistory
                        {
                            VisitorId = visitor.Id,
                            CompanyId = _httpContext.User.GetCompanyId(),
                            OldStatus  = oldStatus,
                            NewStatus = visitor.Status,
                            UpdatedBy = _httpContext.User.GetAccountId(),
                            CreatedOn = DateTime.UtcNow,
                            Reason = model.Reason
                        };
                        _unitOfWork.AppDbContext.VisitHistory.Add(log);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                        
                    }
                }
            });
            return true;
        }

        public IQueryable<VisitListHistoryModel> GetPaginatedVisitHistoryLog(int id, int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.VisitHistory
                .Where(c => c.CompanyId == _httpContext.User.GetCompanyId() && c.VisitorId == id )
                .Select(m => new VisitListHistoryModel()
                {
                    Id = m.Id,
                    CompanyId = m.CompanyId,
                    VisitorId = m.VisitorId,
                    OldStatus = m.OldStatus != null ? m.OldStatus : null,
                    OldStatusString = m.OldStatus != null ? Enum.GetName(typeof(VisitChangeStatusType),  m.OldStatus): "",
                    NewStatusString = Enum.GetName(typeof(VisitChangeStatusType),  m.NewStatus),
                    NewStatus = m.NewStatus,
                    CreatedOn = m.CreatedOn,
                    UpdatedBy = m.UpdatedBy,
                    Reason = m.Reason
                });

            totalRecords = data.Count();
            

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Company.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Company[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber -1)  * pageSize).Take(pageSize);
            return data; 
        }

        public void FinishVisitor(int id, TakeBackCardModel model)
        {
            var card = _unitOfWork.AppDbContext.Card.FirstOrDefault(c =>
                c.CardId == model.CardId && !c.IsDeleted);
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visitor = GetById(id);
                        
                        var oldStatus = visitor.Status;

                        if (visitor.Status == (short) VisitChangeStatusType.Waiting ||
                            visitor.Status == (short) VisitChangeStatusType.Approved1 ||
                            visitor.Status == (short) VisitChangeStatusType.Approved)
                        {
                            visitor.Status = (short) VisitChangeStatusType.FinishedWithoutReturnCard;
                        }
                        else if(visitor.Status == (short) VisitChangeStatusType.CardIssued)
                        {
                            if (card != null)
                            {
                                visitor.Status = (short) VisitChangeStatusType.Finished;
                                _unitOfWork.VisitRepository.Update(visitor);
                                _unitOfWork.CardRepository.Delete(card);
                                _unitOfWork.Save();
                            }
                            else
                            {
                                visitor.Status = (short) VisitChangeStatusType.FinishedWithoutReturnCard;
                                _unitOfWork.VisitRepository.Update(visitor);
                            }
                        }
                        var log = new VisitHistory
                        {
                            VisitorId = visitor.Id,
                            CompanyId = _httpContext.User.GetCompanyId(),
                            OldStatus  = oldStatus,
                            NewStatus = visitor.Status,
                            UpdatedBy = _httpContext.User.GetAccountId(),
                            CreatedOn = DateTime.UtcNow
                        };
                        _unitOfWork.AppDbContext.VisitHistory.Add(log);

                        _unitOfWork.Save();
                        transaction.Commit();
                        
                        SendIdentificationToDevice(visitor, card, false);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public void DeletedAssignedCardVisitor(int id)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var cards = _unitOfWork.AppDbContext.Card.Where(c => !c.IsDeleted && c.VisitId == id);
                        if (cards.Any())
                        {
                            foreach (var card in cards)
                            {
                                _unitOfWork.CardRepository.Delete(card);
                            }
                            _unitOfWork.Save();
                            transaction.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Check if the card already exists.
        /// </summary>
        /// <param name="cardId"> card Id(Uid, etc..) </param>
        /// <returns></returns>
        public bool IsCardIdExist(string cardId, int companyId) 
        {
            var data = _unitOfWork.AppDbContext.Card.Where(c => !c.IsDeleted && c.CardId == cardId && c.CompanyId == companyId);

            return  data.Count() > 0;
        }

        /// <summary>
        /// Count visitor that is waiting to review(approval).
        /// </summary>
        /// <returns></returns>
        public int LengthVisitNeedToReview()
        {
            var currentAccountId = _httpContext.User.GetAccountId();

            var visitors = _unitOfWork.AppDbContext.Visit.Include(v => v.VisitArmy)
                                                    .Where(v => v.CompanyId == _httpContext.User.GetCompanyId()
                                                                && ( (v.Status == (short)VisitChangeStatusType.Waiting && v.ApproverId1 == currentAccountId ) 
                                                                   || (v.Status == (short)VisitChangeStatusType.Approved1 && v.ApproverId2 == currentAccountId) )
                                                                && !v.IsDeleted);

            // Except army visitor
            visitors = visitors.Where(v => !v.VisitArmy.Select(m => m.VisitId).Contains(v.Id));

            return visitors.Count();
        }

        /// <summary>
        /// Get the number of visitors waiting for approval
        /// </summary>
        /// <returns></returns>
        public int GetRequestApprovalCount()
        {
            var companyId = _httpContext.User.GetCompanyId();
            var accountId = _httpContext.User.GetAccountId();

            var visitorCount = _unitOfWork.VisitRepository.GetByFirstApprovalId(companyId, accountId).Count();

            return visitorCount;
        }

        public void ReturnCardVisitor(string cardId, string reason)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var cards = _unitOfWork.AppDbContext.Card.Where(c => !c.IsDeleted && c.CardId == cardId && c.CompanyId == _httpContext.User.GetCompanyId());
                        foreach (var card in cards)
                        {
                            var visit = _unitOfWork.AppDbContext.Visit.FirstOrDefault(c => !c.IsDeleted && c.Id == card.VisitId && c.CompanyId==_httpContext.User.GetCompanyId());
                            
                            if (visit != null)
                            {
                                if (visit.Status != (short) VisitChangeStatusType.Finished)
                                {
                                    var oldStatus = visit.Status;
                                    visit.Status = (short) VisitChangeStatusType.Finished;
                                    var log = new VisitHistory
                                    {
                                        VisitorId = visit.Id,
                                        CompanyId = _httpContext.User.GetCompanyId(),
                                        OldStatus  = oldStatus,
                                        NewStatus = visit.Status,
                                        UpdatedBy = _httpContext.User.GetAccountId(),
                                        CreatedOn = DateTime.UtcNow,
                                        Reason = reason
                                    };
                                    _unitOfWork.AppDbContext.VisitHistory.Add(log);
                                    _unitOfWork.CardRepository.Delete(card);
                                    _unitOfWork.VisitRepository.Update(visit);

                                    SendIdentificationToDevice(visit, card, false);
                                }
                            }
                            
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public void ReturnVisitor(List<int> visitIds, string reason)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var visitId in visitIds)
                        {
                            var visitor = _unitOfWork.AppDbContext.Visit.FirstOrDefault(c => !c.IsDeleted && c.Id == visitId);
                            if (visitor != null)
                            {
                                if (visitor.Status != (short) VisitChangeStatusType.Finished)
                                {
                                    var cards = _unitOfWork.AppDbContext.Card.Where(c => !c.IsDeleted && c.VisitId == visitId);

                                    foreach (var card in cards)
                                    {
                                        SendIdentificationToDevice(visitor, card, false);
                                        _unitOfWork.CardRepository.Delete(card);
                                    }

                                    var oldStatus = visitor.Status;
                                    visitor.Status = (short) VisitChangeStatusType.Finished;
                                    var log = new VisitHistory
                                    {
                                        VisitorId = visitor.Id,
                                        CompanyId = _httpContext.User.GetCompanyId(),
                                        OldStatus  = oldStatus,
                                        NewStatus = visitor.Status,
                                        UpdatedBy = _httpContext.User.GetAccountId(),
                                        CreatedOn = DateTime.UtcNow,
                                        Reason = reason
                                    };
                                    _unitOfWork.AppDbContext.VisitHistory.Add(log);
                                    _unitOfWork.VisitRepository.Update(visitor);
                                }
                            }
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public void Delete(int visitId)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visit = _unitOfWork.AppDbContext.Visit.Single(v => v.Id == visitId && !v.IsDeleted);
                        var accessGroup = _unitOfWork.AppDbContext.AccessGroup.FirstOrDefault(a =>
                            a.Id == visit.AccessGroupId && !a.IsDeleted && a.CompanyId == _httpContext.User.GetCompanyId());
                        var agDevices = _unitOfWork.AppDbContext.AccessGroupDevice.Where(ag => ag.AccessGroupId ==accessGroup.Id);
                        if (agDevices.Count() > 0)
                        {
                            _unitOfWork.AccessGroupDeviceRepository.DeleteRange(agDevices);
                        }
                        _unitOfWork.AccessGroupRepository.Delete(accessGroup);
                        _unitOfWork.VisitRepository.Delete(visit);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        public int GetLastApproval(int visitId)
        {
            var visitors = _unitOfWork.AppDbContext.VisitHistory.FirstOrDefault(v => v.CompanyId == _httpContext.User.GetCompanyId() 
                                                                     && v.NewStatus == (short) VisitChangeStatusType.Approved &&
                                                                     v.VisitorId == visitId);
            if (visitors != null)
            {
                return visitors.UpdatedBy;
            }

            return 0;
        }
        
        public int GetAccountIdCreateVisitor(int visitId)
        {
            var visitSetting = GetVisitSettingCompany();
            if (visitSetting.ApprovalStepNumber == (short) VisitSettingType.NoStep)
            {
                var visitors = _unitOfWork.AppDbContext.VisitHistory.FirstOrDefault(v => v.CompanyId == _httpContext.User.GetCompanyId() 
                                                                                         && v.NewStatus == (short) VisitChangeStatusType.Approved ||
                                                                                         v.VisitorId == visitId);
                if(visitors != null)
                    return visitors.UpdatedBy;
                return 0;
            }
            else
            {
                var visitors = _unitOfWork.AppDbContext.VisitHistory.FirstOrDefault(v => v.CompanyId == _httpContext.User.GetCompanyId() 
                                                                                         && v.NewStatus == (short) VisitChangeStatusType.Waiting ||
                                                                                         v.VisitorId == visitId);
                if(visitors != null)
                    return visitors.UpdatedBy;
                return 0;
            }
        }
        public IEnumerable<EventLog> GetHistoryVisitor(int id, int pageNumber, int pageSize, out int totalRecords)
        {
            var data = _unitOfWork.VisitRepository.GetHistoryVisitor(id, pageNumber, pageSize, out totalRecords).ToList();

            return data;
        }
    }
    
}


