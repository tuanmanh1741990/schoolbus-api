﻿namespace DeMasterProCloud.Service.Protocol
{
    public class DeviceConnectionStatusProtocolData : ProtocolData<DeviceConnectionStatusDetail>
    {
    }

    public class DeviceConnectionStatusDetail
    {
        public string DeviceAddress { get; set; }
        public int Status { get; set; }
        public string IpAddress { get; set; }
        public string DeviceType { get; set; }
        public string MacAddress { get; set; }
    }

    public class DeviceStatusProtocolData : ProtocolData<DeviceStatusDetail> { }

    public class DeviceStatusDetail
    {
        public string DeviceAddress { get; set; }
        public string IpAddress { get; set; }
        public int Status { get; set; }
        public string DeviceType { get; set; }
        public int EventCount { get; set; }
        public int UserCount { get; set; }
        public int DbIdCount { get; set; }
        public string LastCommunicationTime { get; set; }
        public string DoorStatus { get; set; }
        public string Version { get; set; }
        public string InReader { get; set; }
        public string OutReader { get; set; }
        public string NfcModule { get; set; }
        
    }
}
