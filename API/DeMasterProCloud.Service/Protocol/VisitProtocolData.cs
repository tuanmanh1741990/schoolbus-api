using System.Collections.Generic;
using DeMasterProCloud.DataAccess.Models;

namespace DeMasterProCloud.Service.Protocol
{
    public class VisitProtocolData : ProtocolData<VisitProtocolHeaderData>
    {
        public VisitProtocolData()
        {
        }
    }
    
    public class VisitProtocolHeaderData
    {
        public VisitProtocolHeaderData()
        {
            Visit = new List<VisitProtocolDetailData>();
        }
        public int Total { get; set; }

        //김창환 - User 프로토콜 변경 오후 4:21 2019-09-16
        //UpdateFlag 속성 추가 - // 2019.09.05 유저 전송의 끝을 알 수 있는 플래그. 0: 뒤의 데이터가 더 있음, 1: 마지막 데이터(권승재 주임)
        public int UpdateFlag { get; set; }
        public List<VisitProtocolDetailData> Visit { get; set; }
    }

    public class VisitProtocolDetailData
    {
        public string UserName { get; set; }
        public string CardId { get; set; }
        public int IssueCount { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpireDate { get; set; }
        public int Timezone { get; set; }
    }
}