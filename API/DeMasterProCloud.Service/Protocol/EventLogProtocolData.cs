﻿using System;
using System.Collections.Generic;

namespace DeMasterProCloud.Service.Protocol
{
    /// <summary>
    /// Receive event log from icu
    /// </summary>
    public class ReceiveEventLogProtocolData : ProtocolData<ReceiveEventLogHeaderData>
    {
    }

    public class ReceiveEventLogHeaderData
    {
        public ReceiveEventLogHeaderData()
        {
            Events = new List<ReceiveEventLogDetailData>();
        }
        public int Total { get; set; }
        public List<ReceiveEventLogDetailData> Events { get; set; }
    }

    public class ReceiveEventLogDetailData
    {
        public string DeviceAddress { get; set; }
        public string AccessTime { get; set; }
        public string CardId { get; set; }
        public int? IssueCount { get; set; }
        public string UserName { get; set; }
        public string UpdateTime { get; set; }
        public string InOut { get; set; }
        public int EventType { get; set; }
    }

    /// <summary>
    /// Send event log to webapp
    /// </summary>
    public class SendEventLogListModelData : ProtocolData<SendEventLogHeaderData>
    {
    }

    public class SendEventLogHeaderData
    {
        public SendEventLogHeaderData()
        {
            Events = new List<SendEventLogDetailData>();
        }
        public int Total { get; set; }
        public List<SendEventLogDetailData> Events { get; set; }
    }

    public class SendEventLogDetailData
    {
        public Guid Id { get; set; }
        public int IcuId { get; set; }
        public int? UserId { get; set; }
        /// <summary>
        /// identifier of visit
        /// </summary>
        public int? VisitId { get; set; }
        public string AccessTime { get; set; }
        public string Device { get; set; }
        public string DoorName { get; set; }
        public string EventDetail { get; set; }
        public int EventDetailCode { get; set; }
        public string ExpireDate { get; set; }
        public int? CardStatus { get; set; }
        public string CardId { get; set; }
        public string IssueCount { get; set; }
        public string UserName { get; set; }
        public string Department { get; set; }

        public string InOut { get; set; }
        
        public double UnixTime { get; set; }
        
        public int BuildingId { get; set; }

        /// <summary>
        /// Variables that distinguish user types of events
        /// 0 : Nomal
        /// 1 : Visit
        /// </summary>
        public int UserType { get; set; }
    }

    /// <summary>
    /// Response event log
    /// </summary>
    public class ResponseEventLogProtocolData : ProtocolData<ResponseEventLogHeader>
    {
    }

    public class ResponseEventLogHeader
    {
        public int Total { get; set; }
    }

}
