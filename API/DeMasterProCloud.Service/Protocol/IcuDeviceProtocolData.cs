﻿using System.Collections.Generic;
using DeMasterProCloud.DataAccess.Models;

namespace DeMasterProCloud.Service.Protocol
{
    /// <summary>
    /// Sending device config to icu
    /// </summary>
    public class IcuDeviceProtocolData : ProtocolData<IcuDeviceProtocolDetailData>
    {
    }

    //public class IcuDeviceProtocolDetailData
    //{
    //    public short Led { get; set; }
    //    public short Buzzer { get; set; }
    //    public short InOutSet { get; set; }
    //    public string Parameter { get; set; }
    //    public short VerifyMode { get; set; }
    //    public int ActiveTimezone { get; set; }
    //    public int PassageTimezone { get; set; }
    //    //public short OpenDuration { get; set; }
    //    public int LockOpenDuration { get; set; }
    //    public short CloseReverseLockFlag { get; set; }
    //    public short SensorType { get; set; }
    //    public short Valid { get; set; }
    //    public short AntiPassback { get; set; }
    //    public short HardAntiPassback { get; set; }
    //    public int StatusDelay { get; set; }
    //    public int Condition { get; set; }
    //    public int TapRange { get; set; }
    //    public int BackupPeriod { get; set; }
    //    public short DoorSensorType { get; set; }
    //}

    public class IcuDeviceProtocolDetailData
    {
        //public short Led { get; set; }
        //public short Buzzer { get; set; }
        //public short InOutSet { get; set; }
        public int ReaderCount { get; set; }
        public List<int> ReaderConfig { get; set; }
        //public string Parameter { get; set; }
        public short VerifyMode { get; set; }
        public int? ActiveTimezone { get; set; }
        public int? PassageTimezone { get; set; }
        //public short OpenDuration { get; set; }
        public int LockOpenDuration { get; set; }
        public bool CloseReverseLockFlag { get; set; }
        //public short SensorType { get; set; }
        public short Valid { get; set; }
        public short AntiPassback { get; set; }
        //public short HardAntiPassback { get; set; }
        public bool SensorAlarm { get; set; }
        public int SensorDuration { get; set; }
        public int MPRCount { get; set; }
        public int MPRInterval { get; set; }
        public int BackupPeriod { get; set; }
        public short DoorSensorType { get; set; }
        public string qrAesKey { get; set; }

        /// <summary>   Gets or sets the device buzzer. </summary>
        /// <value> The device buzzer. </value>
        public short DeviceBuzzer { get; set; }
    }

    /// <summary>
    /// Device config response
    /// </summary>
    public class IcuDeviceResponseProtocolData : ProtocolData<IcuDeviceResponseDetail>
    {
    }

    public class IcuDeviceResponseDetail
    {
        public string Status { get; set; }
    }
}
