﻿namespace DeMasterProCloud.Service.Protocol
{
    public class NotificationProtocolData : ProtocolData<NotificationProtocolDataDetail>
    {
    }

    public class NotificationProtocolDataDetail
    {
        public string MessageType { get; set; }
        public string NotificationType { get; set; }
        public string User { get; set; }
        public string Message { get; set; }
    }
}
