﻿namespace DeMasterProCloud.Service.Protocol
{
    public class DeviceInfoProtocolData : ProtocolData<DeviceInfoProtocolDataHeader>
    {
    }

    public class DeviceInfoProtocolDataHeader
    {
        public DeviceInfoProtocolDataHeader() { }
    }

    public class DeviceInfoResponse : ProtocolData<DeviceInfoResponseDetail>
    {
    }

    public class DeviceInfoResponseDetail
    {
        public string DeviceAddress { get; set; }
        public string MacAddress { get; set; }

        public string Version { get; set; }
        public string Reader0Version { get; set; }
        public string Reader1Version { get; set; }
        public string NfcModuleVersion { get; set; }
        public string ExtraVersion { get; set; }

        public string IpAddress { get; set; }
        public string Subnet { get; set; }
        public string Gateway { get; set; }
        public string ServerIp { get; set; }
        public string ServerPort { get; set; }
        public string DeviceTime { get; set; }
        public int EventCount { get; set; }
        public int UserCount { get; set; }
        public int EventNotTransCount { get; set; }
        //public int LedReader0 { get; set; }
        //public int LedReader1 { get; set; }
        //public int BuzzerReader0 { get; set; }
        //public int BuzzerReader1 { get; set; }
        ////public int Parameter { get; set; }
        //public int VerifyMode { get; set; }
        //public int ActiveTimezone { get; set; }
        //public int PassageTimezone { get; set; }
        //public int LockOpenDuration { get; set; }
        //public string CloseReserveClock { get; set; }
        //public int Valid { get; set; }
        //public int AntiPassback { get; set; }
        ////public int HardAntiPassback { get; set; }
        //public int SensorDuration { get; set; }
        //public string MPRCount { get; set; }
        //public string MPRInterval { get; set; }
        //public string BackupPeriod { get; set; }
        //public int DoorSensorType { get; set; }
    }
}
