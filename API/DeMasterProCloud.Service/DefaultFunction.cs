﻿using AutoMapper.Configuration;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace DeMasterProCloud.Service
{
    public interface IDefaultFunction
    {
        Account GetCurrentAccount();
    }

    public class DefaultFunction
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IQueueService _queueService;
        private readonly IConnection _connection;
        private readonly IAccessGroupService _accessGroupService;
        private readonly ITimezoneService _timezoneService;
        private readonly IHolidayService _holidayService;
        private readonly INotificationService _notificationService;
        private readonly IDeviceMessageService _deviceMessageService;
        private readonly IAccountService _accountSersvice;

        public DefaultFunction(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor, IConfiguration configuration,
            ILogger<DeviceService> logger, IQueueService queueService, IAccessGroupService accessGroupService,
            ITimezoneService timezoneService, IHolidayService holidayService,
            INotificationService notificationService, IDeviceMessageService deviceMessageService,
            IAccountService accountService)
        {
            _unitOfWork = unitOfWork;
            _httpContext = contextAccessor.HttpContext;
            _configuration = configuration;
            _logger = logger;
            _queueService = queueService;
            _accessGroupService = accessGroupService;
            _connection = ApplicationVariables.RabbitMqConsumerConnection;
            _timezoneService = timezoneService;
            _holidayService = holidayService;
            _notificationService = notificationService;
            _deviceMessageService = deviceMessageService;
            _accountSersvice = accountService;
        }

        /// <summary>
        /// Get current account.
        /// </summary>
        public Account GetCurrentAccount()
        {
            var account = _accountSersvice.GetAccountByEmail(_httpContext.User.GetUsername());
            return account;
        }

    }

    
}
