using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using DeMasterProCloud.DataModel.Visit;
using AutoMapper;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.AccessGroup;
using System.Linq.Dynamic.Core;
using System.Text;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel.User;
using Newtonsoft.Json;
using OfficeOpenXml;
using Z.EntityFramework.Plus;
using DeMasterProCloud.DataModel.EventLog;
using DeMasterProCloud.DataModel.VisitArmy;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Visit Interface 
    /// </summary>
    public interface IVisitArmyService
    {
        IQueryable<Visit> GetPaginated(string accessDateFrom, string accessDateTo, string accessTimeFrom, string accessTimeTo, 
            string visitorName, string birthDay, List<int> status, int pageNumber, int pageSize, int sortColumn, string sortDirection, 
            out int totalRecords, out int recordsFiltered);

        void Add(int visitId, VisitArmyModel visitArmyModel);
        void Update(VisitArmyModel visitArmyModel);
        void Delete(int visitId);

        void InitData(VisitArmyDataModel visitModel);

        int GetArmyReviewCount();


    }

    /// <summary>
    /// Service provider for user
    /// </summary>
    public class VisitArmyService : IVisitArmyService
    {

        // Inject dependency
        private readonly IUnitOfWork _unitOfWork;

        private readonly HttpContext _httpContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IAccessGroupService _accessGroupService;
        private readonly IAccountService _accountService;
        private readonly ISettingService _settingService;

        public VisitArmyService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor,
            IHostingEnvironment hostingEnvironment, IAccessGroupService accessGroupService,
            IAccountService accountService,
            IConfiguration configuration, ILogger<VisitService> logger)
        {
            _unitOfWork = unitOfWork;
            _hostingEnvironment = hostingEnvironment;
            _httpContext = contextAccessor.HttpContext;
            _configuration = configuration;
            _logger = logger;
            _accessGroupService = accessGroupService;
            _accountService = accountService;
            _settingService = new SettingService(_unitOfWork, _configuration);
        }


        private readonly string[] _header2 =
        {

            //한아름 수정
            VisitResource.lblIDX,//Idx
            VisitResource.lblAccessTime,
            VisitResource.lblUserName,
            VisitResource.lblBirthDay,
            VisitResource.lblDepartment,
            VisitResource.lblCardID,
            VisitResource.lblRID,
            VisitResource.lblDoorName,
            VisitResource.IblSite2,
            VisitResource.lblEventDetail,
            VisitResource.lblIssueCount,
            VisitResource.lblCardStatus,
            VisitResource.lblInOut,

        };

        public IQueryable<Visit> GetPaginated(string accessDateFrom, string accessDateTo, string accessTimeFrom, string accessTimeTo,
            string visitorName, string birthDay, List<int> status, int pageNumber, int pageSize, int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrder(accessDateFrom,
            accessDateTo, accessTimeFrom, accessTimeTo, visitorName,
            birthDay, status, sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }

        /// <summary>
        /// Add a visitArmy
        /// </summary>
        /// <param name="visitId"> identifier of visit </param>
        /// <param name="visitArmyModel"> visitArmy model </param>
        public void Add(int visitId, VisitArmyModel visitArmyModel)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();

                        VisitArmy visitArmy = Mapper.Map<VisitArmy>(visitArmyModel);
                        visitArmy.VisitId = visitId;

                        _unitOfWork.VisitArmyRepository.Add(visitArmy);

                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Update visitArmy
        /// </summary>
        /// <param name="visitArmyModel"></param>
        /// <returns></returns>
        public void Update(VisitArmyModel visitArmyModel)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visitArmyData = _unitOfWork.VisitArmyRepository.GetArmyByVisitId(visitArmyModel.Id);

                        if (visitArmyData != null && visitArmyData.Count() > 0)
                        {
                            VisitArmy visitArmy = visitArmyData.First();

                            Mapper.Map(visitArmyModel, visitArmy);

                            _unitOfWork.VisitArmyRepository.Update(visitArmy);
                        }
                        else
                        {
                            VisitArmy visitArmy = Mapper.Map<VisitArmy>(visitArmyModel);

                            _unitOfWork.VisitArmyRepository.Add(visitArmy);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// initialize data about visitArmy
        /// </summary>
        /// <param name="visitArmyModel"></param>
        public void InitData(VisitArmyDataModel visitArmyModel)
        {
            // VisitArmy is different visitTypes with Visit.
            visitArmyModel.VisitTypes = EnumHelper.ToEnumList<VisitArmyType>();

            visitArmyModel.ListCardStatus = EnumHelper.ToEnumList<VisitingCardStatusType>();

            visitArmyModel.Genders = EnumHelper.ToEnumList<SexType>();

            var accessGroups = _accessGroupService.GetListAccessGroups()
                .Select(Mapper.Map<AccessGroupModelForUser>).ToList();

            var companyId = _httpContext.User.GetCompanyId();

            var accessGroupDefault =
                _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(companyId);
            if (visitArmyModel.AccessGroupId == 0 && accessGroupDefault != null)
            {
                visitArmyModel.AccessGroupId = accessGroupDefault.Id;
                var index = accessGroups.FindIndex(x => x.Id == accessGroupDefault.Id);
                var item = accessGroups[index];
                accessGroups[index] = accessGroups[0];
                accessGroups[0] = item;
            }

            visitArmyModel.AccessGroups = accessGroups;
            if (visitArmyModel.Id == 0 && string.IsNullOrEmpty(visitArmyModel.CardId))
            {
                var cardId = _httpContext.Request.Query["cardId"].ToString();
                if (!string.IsNullOrEmpty(cardId))
                {
                    visitArmyModel.CardId = cardId;
                }
            }

            if(visitArmyModel.Id != 0)
            {
                // Add army information
                var visitArmyData = _unitOfWork.VisitArmyRepository.GetArmyByVisitId(visitArmyModel.Id);

                if (visitArmyData != null && visitArmyData.Count() > 0)
                {
                    VisitArmy visitArmy = visitArmyData.First();

                    visitArmyModel.MilitaryNumber = visitArmy.MilitaryNumber;
                    visitArmyModel.BackGroundCheckNumber = visitArmy.BackGroundCheckNumber;
                    visitArmyModel.Relationship = visitArmy.Relationship;
                    visitArmyModel.Gender = visitArmy.Gender;
                    visitArmyModel.VisitorRank = visitArmy.VisitorRank;
                    visitArmyModel.VisiteeRank = visitArmy.VisiteeRank;
                }
            }
            else
            {
                visitArmyModel.VisitType = (int) VisitArmyType.Civilian;
            }
        }

        /// <summary>
        /// Filter and order data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        internal IQueryable<Visit> FilterDataWithOrder(string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo, string visitorName,
            string birthDay, List<int> status, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.VisitRepository.GetByCompanyId(companyId);

            data = data.Include(m => m.VisitArmy).Where(m => m.VisitArmy.Select(c => c.VisitId).Contains(m.Id));

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
                data = data.Where(m =>
                    m.ApplyDate >= accessDateTimeFrom);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
                data = data.Where(m =>
                    m.ApplyDate <= accessDateTimeTo);
            }

            if (!string.IsNullOrEmpty(visitorName))
            {
                data = data.Where(m => m.VisitorName.Contains(visitorName));
            }

            if (!string.IsNullOrEmpty(birthDay))
            {
                data = data.Where(m => m.BirthDay.ToSettingDateString().Contains(birthDay));
            }

            if (status != null && status.Any())
            {
                data = data.Where(m => status.Contains(m.Status));
            }
            
            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.Visit.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Visit[sortColumn]} {sortDirection}");

            foreach (var visit in data)
            {
                if (visit.Status == (short) VisitChangeStatusType.Approved)
                {
                    visit.IsDecision = false;
                }
                else
                {
                    var visitSetting = GetVisitSettingCompany();
                    if (visitSetting.ApprovalStepNumber == (short) VisitSettingType.FirstStep)
                    {
                        if (visit.Status == (short) VisitChangeStatusType.Waiting)
                        {
                            if (visit.ApproverId1 != _httpContext.User.GetAccountId())
                            {
                                visit.IsDecision = false;
                            }
                            else
                            {
                                visit.IsDecision = true;
                            }
                        }
                        else
                        {
                            visit.IsDecision = false;
                        }
                        
                    }
                    if (visitSetting.ApprovalStepNumber == (short) VisitSettingType.SecondsStep)
                    {
                        if (visit.Status == (short) VisitChangeStatusType.Waiting)
                        {
                            if (visit.ApproverId1 != _httpContext.User.GetAccountId())
                            {
                                visit.IsDecision = false;
                            }
                            else
                            {
                                visit.IsDecision = true;
                            }
                        }
                        else if (visit.Status == (short) VisitChangeStatusType.Approved1)
                        {
                            var secondApprovalAccounts = JsonConvert.DeserializeObject<List<int>>(visitSetting.SecondsApproverAccounts);
                            if (!secondApprovalAccounts.Contains(_httpContext.User.GetAccountId()))
                            {
                                visit.IsDecision = false;
                            }
                            else
                            {
                                visit.IsDecision = true;
                            }
                        }
                        else
                        {
                            visit.IsDecision = false;
                        }
                    }
                }

                var card = _unitOfWork.AppDbContext.Card.Where(c => c.VisitId == visit.Id && !c.IsDeleted).FirstOrDefault();

                visit.CardId = card != null ? card.CardId : "";

                visit.BirthDay = Helpers.ConvertToUserTimeZoneReturnDate(visit.BirthDay, _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);

                visit.StartDate = Helpers.ConvertToUserTimeZoneReturnDate(visit.StartDate, _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);

                visit.EndDate = Helpers.ConvertToUserTimeZoneReturnDate(visit.EndDate, _unitOfWork.AccountRepository.Get(m =>
                    m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone);
            }

            return data;
        }

        /// <summary>
        /// Get visitSettings.
        /// </summary>
        /// <returns></returns>
        public VisitSetting GetVisitSettingCompany()
        {
            return _unitOfWork.VisitRepository.GetVisitSetting(_httpContext.User.GetCompanyId());
        }

        /// <summary>
        /// Delete visitArmy
        /// </summary>
        /// <param name="visitId"></param>
        public void Delete(int visitId)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var visitArmy = _unitOfWork.VisitArmyRepository.GetArmyByVisitId(visitId);

                        if (visitArmy != null && visitArmy.Count() > 0)
                        {
                            _unitOfWork.VisitArmyRepository.Delete(visitArmy.First());
                            _unitOfWork.Save();
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Count visitor in army that is waiting to review(approval).
        /// </summary>
        /// <returns></returns>
        public int GetArmyReviewCount()
        {
            var currentAccountId = _httpContext.User.GetAccountId();

            var visitors = _unitOfWork.AppDbContext.Visit.Where(v => v.CompanyId == _httpContext.User.GetCompanyId()
                                                                    && ((v.Status == (short)VisitChangeStatusType.Waiting && v.ApproverId1 == currentAccountId)
                                                                      || (v.Status == (short)VisitChangeStatusType.Approved1 && v.ApproverId2 == currentAccountId))
                                                                    && v.VisitArmy.Select(m => m.VisitId).Contains(v.Id)
                                                                    && !v.IsDeleted).Count();

            return visitors;
        }

    }
    
}


