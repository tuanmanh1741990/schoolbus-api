using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.Api;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DeMasterProCloud.Service
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class CheckAddOnAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        private readonly string _addOn;
        
        public CheckAddOnAttribute(string addOn)
        {
            _addOn = addOn;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string authorHeader = context.HttpContext.Request.Headers["Authorization"];
            var token = authorHeader.Remove(0, 7);
                   
            var handler = new JwtSecurityTokenHandler();
               
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
               
            var accountType = tokenS.Claims.First(claim => claim.Type == Constants.ClaimName.AccountType).Value;

            if (Int32.Parse(accountType) != (short) AccountType.SystemAdmin)
            {
                var companyId = tokenS.Claims.First(claim => claim.Type == Constants.ClaimName.CompanyId).Value;
               
                IPluginService service = (IPluginService)context.HttpContext.RequestServices.GetService(typeof(IPluginService));
                var valid = service.CheckPluginCondition(_addOn, Int32.Parse(companyId));
                if (valid)
                {
                    return;
                }

                context.Result = new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            return;
        }
    }
    
    public class CheckMultipleAddOnAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        private readonly string [] _addOn;
        
        public CheckMultipleAddOnAttribute(string [] addOn)
        {
            _addOn = addOn;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            string authorHeader = context.HttpContext.Request.Headers["Authorization"];
            var token = authorHeader.Remove(0, 7);
                   
            var handler = new JwtSecurityTokenHandler();
               
            var tokenS = handler.ReadToken(token) as JwtSecurityToken;
               
            var accountType = tokenS.Claims.First(claim => claim.Type == Constants.ClaimName.AccountType).Value;

            if (Int32.Parse(accountType) != (short) AccountType.SystemAdmin)
            {
                var companyId = tokenS.Claims.First(claim => claim.Type == Constants.ClaimName.CompanyId).Value;
               
                IPluginService service = (IPluginService)context.HttpContext.RequestServices.GetService(typeof(IPluginService));
                bool validPlugIn = false;
                foreach (var eachAddOn in _addOn)
                {
                    validPlugIn = service.CheckPluginCondition(eachAddOn, Int32.Parse(companyId));
                    if (validPlugIn)
                    {
                        return;
                    }
                }
                

                context.Result = new ApiErrorResult(StatusCodes.Status404NotFound);
            }
            return;
        }
    }
}