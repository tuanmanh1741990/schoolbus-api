﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.EventLog;
using DeMasterProCloud.Repository;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Text;
using AutoMapper;
using OfficeOpenXml;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Bogus;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// EventLog service interface
    /// </summary>
    public interface IEventLogService
    {
        IQueryable<EventLog> GetPaginated(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> eventType,
            string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);


        IEnumerable<EventLogReportListModel> GetPaginatedEventLogReport(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string userName,
            string cardId, string department, List<int> inOutType, List<int> eventType, List<int> buildingList, List<int> cardType, string culture, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        IQueryable<EventLog> GetPaginatedOpenEventLogReport(int userId, out int totalRecords, out int recordsFiltered);

        bool HasData(int? companyId);

        byte[] Export(string type, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company);

        byte[] ExportReport(string type, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<int> inOutType, string cardId, List<int> doorIds,
            List<int> building, string department, List<int> cardtype, int? company);

        /// <summary>
        /// Export to excel
        /// </summary>
        /// <returns></returns>
        byte[] ExportExcel(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company);

        BaseReportModel<EventReportPdfModel> ExportPdf(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company);

        /// <summary>
        /// Export to Text file
        /// </summary>
        /// <returns></returns>
        byte[] ExportTxt(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company);

        EventLogViewModel InitData();
        EventLogReportViewModel InitReportData();

        void SaveSystemLogExport(string fileName);

        IQueryable<EventLog> FilterDataWithOrder(string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo, List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered);



        EventTypeListModel GetEventTypeList();

        IQueryable<IcuDevice> GetPaginatedRecoveryDevices(string filter, int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<RecoveryDeviceModel> GetPaginatedEventRecoveryInquireDevices(List<int> deviceIds, string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, string filter, int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);


        //List<EventCountByDeviceModel> EventCountInquiry(List<int> ids, string accessDateFrom,
        //    string accessDateTo, string accessTimeFrom, string accessTimeTo);

        void EventRecovery(List<EventRecoveryProgressModel> models, string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo);

        int GetCardStatus(string cardId);
        
        IQueryable<EventLog> GetAccessHistoryAttendance(int userId, DateTime start, DateTime end,  int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        void CreateEventLogForDesktopApp(string rid, User user, string message, string action);

        void GenerateTestData(int numberOfEvent);



        // For Duali Korea
        IQueryable<EventLog> GetAttendenceForDuali(string accessDateFrom, string accessDateTo,
            List<int> userIds, List<int> departmentIds);
    }

    public class EventLogService : IEventLogService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly IQueueService _queueService;
        private readonly IConfiguration _configuration;

        private readonly List<string> _header = new List<string>
        {
            EventLogResource.lblIDX,
            EventLogResource.lblAccessTime,
            EventLogResource.lblUserName,
            EventLogResource.lblBirthDay,
            EventLogResource.lblUserCode,
            EventLogResource.lblDepartment,
            EventLogResource.lblCardId,
            EventLogResource.lblRID,
            EventLogResource.lblDoorName,
            EventLogResource.lblBuilding,
            EventLogResource.lblInOut,
            EventLogResource.lblEventDetail,
            EventLogResource.lblIssueCount,
            EventLogResource.lblCardStatus,
        };

        public EventLogService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor,
            IQueueService queueService, IConfiguration configuration)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _configuration = configuration;
            _queueService = queueService;
        }

        /// <summary>
        /// Initial data
        /// </summary>
        /// <returns></returns>
        public EventLogViewModel InitData()
        {
            var model =
                new EventLogViewModel
                {
                    EventTypeList = EnumHelper.ToEnumList<EventType>(),
                    InOutList = EnumHelper.ToEnumList<Antipass>(),
                    VerifyModeList = EnumHelper.ToEnumList<VerifyMode>()
                };

            var doors = _unitOfWork.IcuDeviceRepository.GetDoors();
            var buildings = _unitOfWork.BuildingRepository.GetByCompanyId(_httpContext.User.GetCompanyId());
            var departments = _unitOfWork.DepartmentRepository.GetByCompanyId(_httpContext.User.GetCompanyId());
            if (_httpContext.User.GetAccountType() == (short) AccountType.SuperAdmin)
            {
                model.CompanyItems = _unitOfWork.CompanyRepository.GetCompanies()
                    .Select(c => new SelectListItemModel
                    {
                        Id = c.Id,
                        Name = c.Name
                    }).ToList();
            }
            else
            {
                doors = doors.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());
            }

            model.DoorList = doors.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.Name
            }).ToList();

            model.BuildingList = buildings.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.Name
            }).ToList();

            model.DepartmentList = departments.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.DepartName
            }).ToList();

            return model;
        }


        public EventLogReportViewModel InitReportData()
        {
            var model =
                new EventLogReportViewModel
                {
                    EventTypeList = EnumHelper.ToEnumList<EventType>(),
                    InOutList = EnumHelper.ToEnumList<Antipass>(),
                    CardTypeList = EnumHelper.ToEnumList<PassType>(),
                };

            var doors = _unitOfWork.IcuDeviceRepository.GetDoors();
            var buildings = _unitOfWork.BuildingRepository.GetByCompanyId(_httpContext.User.GetCompanyId());
            var departments = _unitOfWork.DepartmentRepository.GetByCompanyId(_httpContext.User.GetCompanyId());
            if (_httpContext.User.GetAccountType() == (short) AccountType.SuperAdmin)
            {
                model.CompanyItems = _unitOfWork.CompanyRepository.GetCompanies()
                    .Select(c => new SelectListItemModel
                    {
                        Id = c.Id,
                        Name = c.Name
                    }).ToList();
            }
            else
            {
                doors = doors.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());
            }

            model.DoorList = doors.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.Name
            }).ToList();

            model.BuildingList = buildings.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.Name
            }).ToList();

            model.DepartmentList = departments.Select(m => new SelectListItemModel
            {
                Id = m.Id,
                Name = m.DepartName
            }).ToList();


            return model;
        }


        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="doorIds"></param>
        /// <param name="company"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <param name="verifyMode"></param>
        /// <returns></returns>
        public IQueryable<EventLog> GetPaginated(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> eventType,
            string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company, int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrder(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, eventType,
                userName, inOutType,
                cardId, doorIds, building, department, verifyMode, company, sortColumn, sortDirection, out totalRecords,
                out recordsFiltered);

            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }

        public IEnumerable<EventLogReportListModel> GetPaginatedEventLogReport(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string userName,
            string cardId, string department, List<int> inOutType, List<int> eventType, List<int> buildingList,
            List<int> cardType, string culture, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrderEventLogReport(accessDateFrom,
                accessDateTo, accessTimeFrom, accessTimeTo, doorIds, userName,
                cardId, department, inOutType, eventType, buildingList,
                cardType, culture, sortColumn, sortDirection, pageNumber, pageSize, out totalRecords, out recordsFiltered);

            //data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            var accountTimezone = _unitOfWork.AccountRepository.Get(m =>
                m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone;

            foreach (var eventLog in data)
            {
                if (eventLog.AccessTime != null)
                {
                    DateTime even = DateTime.Parse(eventLog.AccessTime);
                    eventLog.AccessTime = Helpers.ConvertToUserTimeZoneReturnDate(even, accountTimezone).ToSettingDateTimeString();
                }
            }

            return data;
        }


        public IQueryable<EventLog> GetPaginatedOpenEventLogReport(int userId, out int totalRecords,
            out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.EventLog
                .Include(m => m.User)
                .Include(m => m.User.Department)
                .Include(m => m.Icu)
                .Include(m => m.Icu.Building)
                .Include(m => m.Company)
                .Include(m => m.Visit)
                .AsQueryable();


            data = data.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());

            var cardList = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), userId);

            totalRecords = data.Count();

            data = data.Where(m =>
                cardList.Contains(_unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(), m.CardId)));




            //if (!string.IsNullOrEmpty(accessDateFrom))
            //{
            //    var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
            //    data = data.Where(m =>
            //        m.EventTime >= accessDateTimeFrom);
            //}

            //if (!string.IsNullOrEmpty(accessDateTo))
            //{
            //    var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
            //    data = data.Where(m =>
            //        m.EventTime <= accessDateTimeTo);
            //}

            //if (doorIds != null && doorIds.Any())
            //{
            //    data = data.Where(m => doorIds.Contains(m.IcuId));
            //}

            //if (!string.IsNullOrEmpty(userName))
            //{
            //    data = data.Where(m => m.User.FirstName.Contains(userName) || m.Visit.VisitorName.Contains(userName));
            //}

            //if (!string.IsNullOrEmpty(cardId))
            //{
            //    data = data.Where(m => m.CardId.Contains(cardId));
            //}

            //if (!string.IsNullOrEmpty(department))
            //{
            //    data = data.Where(m => m.User.Department.DepartName.Contains(department) || m.Visit.VisitorDepartment.Contains(department) || m.Visit.VisiteeDepartment.Contains(department));
            //}

            //if (inOutType != null && inOutType.Any())
            //{
            //    foreach (var inOut in inOutType)
            //    {
            //        data = data.Where(m => m.Antipass == ((Antipass)inOut).GetDescription());
            //    }
            //}

            //if (eventType != null && eventType.Any())
            //{
            //    data = data.Where(m => eventType.Contains(m.EventType));
            //}

            //if (buildingList != null && buildingList.Any())
            //{
            //    data = data.Where(m => buildingList.Contains(m.Icu.Building.Id));
            //}

            //if (cardType != null && cardType.Any())
            //{
            //    data = data.Where(m => cardType.Contains(m.CardType));
            //}


            recordsFiltered = data.Count();
            return data;
        }




        /// <summary>
        /// Filter data
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="doorIds"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public IQueryable<EventLog> FilterDataWithOrder(string accessDateFrom, string accessDateTo,
            string accessTimeFrom,
            string accessTimeTo, List<int> eventType, string userName, List<string> inOutType, string cardId,
            List<int> doorIds,
            string building, string department, string verifymode, int? company, int sortColumn, string sortDirection,
            out int totalRecords,
            out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.EventLog
                .Include(m => m.User)
                .Include(m => m.User.Department)
                .Include(m => m.Icu)
                .Include(m => m.Icu.Building)
                .Include(m => m.Company)
                .AsQueryable();

            if (_httpContext.User.GetAccountType() == (short) AccountType.SystemAdmin)
            {
                if (company.HasValue)
                {
                    data = data.Where(m => m.CompanyId == company);
                }
            }
            else
            {
                data = data.Where(m => m.CompanyId == _httpContext.User.GetCompanyId());
            }

            totalRecords = data.Count();

            data = FilterData(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, eventType, userName,
                inOutType, cardId, doorIds, building, department, verifymode, data);

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.EventLog.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.EventLog[sortColumn]} {sortDirection}");
            return data;
        }

        /// <summary>   Filter data with order event log report. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="accessDateFrom">   Start date. </param>
        /// <param name="accessDateTo">     End date. </param>
        /// <param name="accessTimeFrom">   Start time. </param>
        /// <param name="accessTimeTo">     Start time. </param>
        /// <param name="doorIds">          List of door id. </param>
        /// <param name="userName">         User name. </param>
        /// <param name="cardId">           Card Id. </param>
        /// <param name="department">       name of department. </param>
        /// <param name="inOutType">        In or Out. </param>
        /// <param name="eventType">        Event type. </param>
        /// <param name="buildingList">     List of buildings. </param>
        /// <param name="cardType">         Type of the card. </param>
        /// <param name="sortColumn">       column using in sorting. </param>
        /// <param name="sortDirection">    Ascending or Descending. </param>
        /// <param name="totalRecords">     [out] total records count. </param>
        /// <param name="recordsFiltered">  [out] filtered records count. </param>
        /// <returns>   An IQueryable&lt;EventLog&gt; </returns>
        public IEnumerable<EventLogReportListModel> FilterDataWithOrderEventLogReport(string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> doorIds, string userName,
            string cardId, string department, List<int> inOutType, List<int> eventType, List<int> buildingList,
            List<int> cardType, string culture, int sortColumn, string sortDirection, int pageNumber, int pageSize,
            out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            //var data = _unitOfWork.AppDbContext.EventLog
            //    .Include(m => m.User)
            //    .Include(m => m.User.Department)
            //    .Include(m => m.Icu)
            //    .Include(m => m.Icu.Building)
            //    .Include(m => m.Company)
            //    .Include(m => m.Visit)
            //    .Where(m => m.CompanyId == companyId);
            //    //.Skip(0).Take(250);
            //    //.Where(m => m.CompanyId != _httpContext.User.GetCompanyId())
            //    //.AsQueryable();

            //totalRecords = data.Count();

            DateTime accessDateTimeFrom;
            DateTime accessDateTimeTo;

            // Set "FROM" date
            if (!string.IsNullOrEmpty(accessDateFrom))
                accessDateTimeFrom = Helpers.GetFromToDateTimeConvert(accessDateFrom, accessTimeFrom, false);
            else
            {
                accessDateFrom = DateTime.Today.AddDays(-1).ToString("yyyy-MM-dd");
                accessDateTimeFrom = Helpers.GetFromToDateTimeConvert(accessDateFrom, accessTimeFrom, false);
            }
            //data = data.Where(m => m.EventTime >= accessDateTimeFrom);

            // Set "TO" date
            if (!string.IsNullOrEmpty(accessDateTo))
                accessDateTimeTo = Helpers.GetFromToDateTimeConvert(accessDateTo, accessTimeTo, true);
            else
            {
                accessDateTo = DateTime.Today.ToString("yyyy-MM-dd");
                accessDateTimeTo = Helpers.GetFromToDateTimeConvert(accessDateTo, accessTimeTo, true);
            }
            //data = data.Where(m => m.EventTime <= accessDateTimeTo);

            var data = _unitOfWork.AppDbContext.EventLog
                .Include(m => m.User)
                .Include(m => m.User.Department)
                .Include(m => m.Icu)
                .Include(m => m.Icu.Building)
                .Include(m => m.Company)
                .Include(m => m.Visit)
                .Where(m => m.CompanyId == companyId)
                .Where(m => !m.IsVisit)
                .Where(m => m.EventTime >= accessDateTimeFrom)
                .Where(m => m.EventTime <= accessDateTimeTo);
            //.Skip(0).Take(250);
            //.Where(m => m.CompanyId != _httpContext.User.GetCompanyId())
            //.AsQueryable();

            totalRecords = data.Count();

            // Set list of device indentifier.
            if (doorIds != null && doorIds.Any())
            {
                data = data.Where(m => doorIds.Contains(m.IcuId));
            }
            else
            {
                doorIds = _unitOfWork.IcuDeviceRepository.GetByCompany(companyId).Select(m => m.Id).ToList();
            }

            if (!string.IsNullOrEmpty(userName))
            {
                data = data.Where(m => m.UserName.ToLower().Contains(userName.ToLower()));
            }

            if (!string.IsNullOrEmpty(cardId))
            {
                data = data.Where(m => m.CardId.Contains(cardId));
            }

            if (!string.IsNullOrEmpty(department))
            {
                data = data.Where(m => m.User.Department.DepartName.Contains(department));
            }

            if (inOutType != null && inOutType.Any())
            {
                List<string> inOutTypeDescription = new List<string>();

                foreach (var type in inOutType)
                {
                    inOutTypeDescription.Add(((Antipass) type).GetDescription());
                }

                data = data.Where(m => inOutTypeDescription.Contains(m.Antipass));
            }

            if (eventType != null && eventType.Any())
            {
                data = data.Where(m => eventType.Contains(m.EventType));
            }

            if (buildingList != null && buildingList.Any())
            {
                data = data.Where(m => buildingList.Contains(m.Icu.Building.Id));
            }

            if (cardType != null && cardType.Any())
            {
                data = data.Where(m => cardType.Contains(m.CardType));
            }

            recordsFiltered = data.Count();

            var listData = _unitOfWork.EventLogRepository.GetPaginatedEventLog(data, pageNumber, pageSize, sortDirection, sortColumn, culture);

            //data = data.Skip((1 - 1) * 25).Take(25);

            //var listData = data.Select(Mapper.Map<EventLogReportListModel>);
            //listData = SortList(listData, sortColumn, sortDirection);
            //listData = listData.Skip((1 - pageNumber) * pageSize).Take(pageSize);

            //listData = SortList(listData, sortColumn, sortDirection);

            //listData = listData.Skip((1 - 1) * 25).Take(25).ToList();
            //listData = listData.Skip((1 - 1) * 25).Take(25);

            //var test = listData.Count();

            return listData;
        }

        /// <summary>   Enumerates sort list in this collection. </summary>
        /// <remarks>   Edward, 2020-03-19. </remarks>
        /// <param name="data">             EventLogReportListModel list. </param>
        /// <param name="sortColumn">       column number that want to sort. </param>
        /// <param name="sortDirection">    sort method(asc or desc). </param>
        /// <returns>
        ///     Sorted EventLogReportListModel list(IEnumerable).
        /// </returns>
        public IEnumerable<EventLogReportListModel> SortList(IEnumerable<EventLogReportListModel> data, int sortColumn, string sortDirection)
        {
            if (sortDirection.Equals("desc"))
            {
                switch (sortColumn)
                {
                    case 0:
                    case 1:
                        data = data.OrderByDescending(c => c.AccessTime);
                        break;
                    case 2:
                        data = data.OrderByDescending(c => c.UserName);
                        break;
                    case 3:
                        data = data.OrderByDescending(c => c.BirthDay);
                        break;
                    case 4:
                        data = data.OrderByDescending(c => c.UserCode);
                        break;
                    case 5:
                        data = data.OrderByDescending(c => c.Department);
                        break;
                    case 6:
                        data = data.OrderByDescending(c => c.CardId);
                        break;
                    case 7:
                        data = data.OrderByDescending(c => c.DeviceAddress);
                        break;
                    case 8:
                        data = data.OrderByDescending(c => c.DoorName);
                        break;
                    case 9:
                        data = data.OrderByDescending(c => c.Building);
                        break;
                    case 10:
                        data = data.OrderByDescending(c => c.InOut);
                        break;
                    case 11:
                        data = data.OrderByDescending(c => c.EventDetail);
                        break;
                    case 12:
                        data = data.OrderByDescending(c => c.IssueCount);
                        break;
                    case 13:
                        data = data.OrderByDescending(c => c.CardStatus);
                        break;
                    case 14:
                        data = data.OrderByDescending(c => c.CardType);
                        break;

                    default:
                        break;
                };
            }
            else if (sortDirection.Equals("asc"))
            {
                switch (sortColumn)
                {
                    case 0:
                    case 1:
                        data = data.OrderBy(c => c.AccessTime);
                        break;
                    case 2:
                        data = data.OrderBy(c => c.UserName);
                        break;
                    case 3:
                        data = data.OrderBy(c => c.BirthDay);
                        break;
                    case 4:
                        data = data.OrderBy(c => c.UserCode);
                        break;
                    case 5:
                        data = data.OrderBy(c => c.Department);
                        break;
                    case 6:
                        data = data.OrderBy(c => c.CardId);
                        break;
                    case 7:
                        data = data.OrderBy(c => c.DeviceAddress);
                        break;
                    case 8:
                        data = data.OrderBy(c => c.DoorName);
                        break;
                    case 9:
                        data = data.OrderBy(c => c.Building);
                        break;
                    case 10:
                        data = data.OrderBy(c => c.InOut);
                        break;
                    case 11:
                        data = data.OrderBy(c => c.EventDetail);
                        break;
                    case 12:
                        data = data.OrderBy(c => c.IssueCount);
                        break;
                    case 13:
                        data = data.OrderBy(c => c.CardStatus);
                        break;
                    case 14:
                        data = data.OrderBy(c => c.CardType);
                        break;

                    default:
                        break;
                };
            }

            return data;
        }


        /// <summary>
        /// Filter data
        /// </summary>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="verifyMode"></param>
        /// <param name="data"></param>
        /// <param name="doorIds"></param>
        /// <param name="buildingId"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        private IQueryable<EventLog> FilterData(string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string buildingId, string departmentId, string verifyMode, IQueryable<EventLog> data)
        {
            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
                data = data.Where(m =>
                    m.EventTime >= accessDateTimeFrom);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
                data = data.Where(m =>
                    m.EventTime <= accessDateTimeTo);
            }

            if (eventType != null && eventType.Any())
            {
                data = data.Where(m => eventType.Contains(m.EventType));
            }

            if (!string.IsNullOrEmpty(userName))
            {
                data = data.Where(m => m.User.FirstName.ToLower().Contains(userName.ToLower()));
            }

            if (inOutType != null && inOutType.Any())
            {
                //var inOutTypeInt = Convert.ToInt32(inOutType);
                List<string> inOutTypeDescription = new List<string>();

                foreach (var type in inOutType)
                {
                    inOutTypeDescription.Add(((Antipass) Convert.ToInt32(type)).GetDescription());
                }

                data = data.Where(m => inOutTypeDescription.Contains(m.Antipass));
            }

            if (!string.IsNullOrEmpty(cardId))
            {
                data = data.Where(m => m.CardId.Contains(cardId));
            }

            if (doorIds != null && doorIds.Any())
            {
                data = data.Where(m => doorIds.Contains(m.IcuId));
            }

            if (!string.IsNullOrEmpty(buildingId))
            {
                data = data.Where(m => m.Icu.Building.Id == Convert.ToInt32(buildingId));
            }
            //if (doorIds != null && doorIds.Any())
            //{
            //    data = data.Where(m => doorIds.Contains(m.IcuId));
            //}

            if (!string.IsNullOrEmpty(verifyMode))
            {
                data = data.Where(m =>
                    ((VerifyMode) m.Icu.VerifyMode).GetDescription().Replace(" ", "")
                    .Equals(verifyMode, StringComparison.InvariantCultureIgnoreCase));
            }

            if (!string.IsNullOrEmpty(departmentId))
            {
                data = data.Where(m => m.User.Department.Id == Convert.ToInt32(departmentId));
            }

            return data;
        }


        private IQueryable<EventLog> FilterData2(string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, string inOutType, string cardId, List<int> doorIds,
            string buildingId, string departmentId, List<int> cardType, IQueryable<EventLog> data)
        {
            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
                data = data.Where(m =>
                    m.EventTime >= accessDateTimeFrom);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
                data = data.Where(m =>
                    m.EventTime <= accessDateTimeTo);
            }

            if (eventType != null && eventType.Any())
            {
                data = data.Where(m => eventType.Contains(m.EventType));
            }

            if (!string.IsNullOrEmpty(userName))
            {
                data = data.Where(m => m.User.FirstName.Contains(userName));
            }

            if (!string.IsNullOrEmpty(inOutType))
            {
                var inOutTypeInt = Convert.ToInt32(inOutType);
                data = data.Where(m => m.Antipass == ((Antipass) inOutTypeInt).GetDescription());
            }

            if (!string.IsNullOrEmpty(cardId))
            {
                data = data.Where(m => m.CardId.Contains(cardId));
            }

            if (doorIds != null && doorIds.Any())
            {
                data = data.Where(m => doorIds.Contains(m.IcuId));
            }

            if (!string.IsNullOrEmpty(buildingId))
            {
                data = data.Where(m => m.Icu.Building.Id == Convert.ToInt32(buildingId));
            }

            if (doorIds != null && doorIds.Any())
            {
                data = data.Where(m => doorIds.Contains(m.IcuId));
            }


            if (cardType != null && cardType.Any())
            {
                data = data.Where(m => cardType.Contains(m.CardType));
            }


            if (!string.IsNullOrEmpty(departmentId))
            {
                data = data.Where(m => m.User.Department.Id == Convert.ToInt32(departmentId));
            }

            return data;
        }

        /// <summary>
        /// Check if there is any data in eventlog table
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool HasData(int? companyId)
        {
            if (_httpContext.User.GetAccountType() == (short) AccountType.SuperAdmin)
            {
                if (companyId.HasValue)
                {
                    return _unitOfWork.AppDbContext.EventLog.Any(m => m.CompanyId == companyId);
                }
                else
                {
                    return _unitOfWork.AppDbContext.EventLog.Any();
                }
            }

            return _unitOfWork.AppDbContext.EventLog.Any(
                m => m.CompanyId == _httpContext.User.GetCompanyId());
        }

        /// <summary>
        /// Export data in excel or txt format
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="doorIds"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public byte[] Export(string type, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company)
        {
            return type == Constants.Excel
                ? ExportExcel(sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered, accessDateFrom, accessDateTo,
                    accessTimeFrom, accessTimeTo, eventType, userName, inOutType, cardId,
                    doorIds, building, department, verifyMode, company)
                : ExportTxt(sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered, accessDateFrom, accessDateTo,
                    accessTimeFrom, accessTimeTo, eventType, userName, inOutType, cardId,
                    doorIds, building, department, verifyMode, company);
        }

        public byte[] ExportReport(string type, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<int> inOutType, string cardId, List<int> doorIds,
            List<int> building, string department, List<int> cardtype, int? company)
        {
            return type == Constants.Excel
                ? ExportExcelReport(sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered, accessDateFrom, accessDateTo,
                    accessTimeFrom, accessTimeTo, eventType, userName, inOutType, cardId,
                    doorIds, building, department, cardtype, company)
                : ExportTxtReport(sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered, accessDateFrom, accessDateTo,
                    accessTimeFrom, accessTimeTo, eventType, userName, inOutType, cardId,
                    doorIds, building, department, cardtype, company);
        }

        /// <summary>
        /// Export data in excel
        /// </summary>
        /// 
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="department"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="doorIds"></param>
        /// <param name="building"></param>
        /// <returns></returns>
        public byte[] ExportExcel(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company)
        {
            byte[] result;

            //var package = new ExcelPackage();
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(EventLogResource.lblEventLog); //Worksheet name

                //First add the headers
                for (var i = 0; i < _header.Count; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }

                var data = FilterDataWithOrder(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, eventType,
                    userName, inOutType, cardId, doorIds, building, department, verifyMode, company, sortColumn,
                    sortDirection,
                    out totalRecords, out recordsFiltered).ToList();

                var recordIndex = 2;
                foreach (var report in data)
                {
                    var colIndex = 1;

                    worksheet.Cells[recordIndex, colIndex++].Value =
                        report.EventTime.ToSettingDateTimeString();
                    worksheet.Cells[recordIndex, colIndex++].Value =
                        report.EventType != (int) EventType.UnRegisteredCard
                            ? report.User?.FirstName + " " + report.User?.LastName
                            : string.Empty;
                    worksheet.Cells[recordIndex, colIndex++].Value = report.CardId;
                    worksheet.Cells[recordIndex, colIndex++].Value = report.Icu?.DeviceAddress;
                    worksheet.Cells[recordIndex, colIndex++].Value =
                        report.DoorName;
                    worksheet.Cells[recordIndex, colIndex++].Value =
                        report.Icu?.Building.Name;
                    worksheet.Cells[recordIndex, colIndex++].Value = report.Icu != null
                        ? ((VerifyMode) report.Icu.VerifyMode).GetDescription()
                        : string.Empty;
                    worksheet.Cells[recordIndex, colIndex++].Value = Constants.AntiPass.Contains(report.Antipass)
                        ? ((Antipass) Enum.Parse(typeof(Antipass), report.Antipass))
                        .GetDescription()
                        : EventLogResource.lblUnknown;
                    worksheet.Cells[recordIndex, colIndex].Value = ((EventType) report.EventType).GetDescription();
                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        public byte[] ExportExcelReport(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<int> inOutType, string cardId, List<int> doorIds,
            List<int> buildingList, string department, List<int> cardType, int? company)
        {
            byte[] result;

            //var package = new ExcelPackage();
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(EventLogResource.lblEventLog); //Worksheet name

                //First add the headers
                for (var i = 0; i < _header.Count; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }

                var data = FilterDataWithOrderEventLogReport(accessDateFrom,
                    accessDateTo, accessTimeFrom, accessTimeTo, doorIds, userName,
                    cardId, department, inOutType, eventType, buildingList,
                    cardType, "", sortColumn, sortDirection, 0,0, out totalRecords, out recordsFiltered).ToList();

                var recordIndex = 2;
                foreach (var report in data)
                {
                    var card = _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(), report.CardId);
                    var colIndex = 1;

                    worksheet.Cells[recordIndex, colIndex++].Value = recordIndex - 1; //idx
                    worksheet.Cells[recordIndex, colIndex++].Value = report.AccessTime; //출입시간
                    worksheet.Cells[recordIndex, colIndex++].Value = report.UserName; //이름
                    worksheet.Cells[recordIndex, colIndex++].Value = report.BirthDay; //생년월일
                    worksheet.Cells[recordIndex, colIndex++].Value = report.EmployeeNumber; //사원번호
                    worksheet.Cells[recordIndex, colIndex++].Value = report.Department; //소속
                    worksheet.Cells[recordIndex, colIndex++].Value = report.CardId; //카드ID
                    worksheet.Cells[recordIndex, colIndex++].Value = report.DeviceAddress; //RID
                    worksheet.Cells[recordIndex, colIndex++].Value = report.DoorName; //출입문 이름 
                    worksheet.Cells[recordIndex, colIndex++].Value = report.Building; //장소
                    worksheet.Cells[recordIndex, colIndex++].Value = report.InOut; //입/출
                    worksheet.Cells[recordIndex, colIndex++].Value = report.EventDetail; //이벤트 종류
                    worksheet.Cells[recordIndex, colIndex++].Value = report.IssueCount; //발급차수
                    worksheet.Cells[recordIndex, colIndex++].Value = report.CardStatus; //카드상태
                    worksheet.Cells[recordIndex, colIndex++].Value = report.CardType; //카드구분

                    //worksheet.Cells[recordIndex, colIndex++].Value = !report.IsVisit ? ((CardStatus)report.User?.CardStatus).GetDescription() : "";//카드상태 

                    /*  worksheet.Cells[recordIndex, colIndex++].Value =
                          report.EventTime?.ToSettingDateTimeString();
                      worksheet.Cells[recordIndex, colIndex++].Value =
                          report.EventType != (int)EventType.UnRegisteredCard ? report.User?.FirstName + " " + report.User?.LastName : string.Empty;
                      worksheet.Cells[recordIndex, colIndex++].Value = report.CardId;
                      worksheet.Cells[recordIndex, colIndex++].Value = report.Icu?.DeviceAddress;
                      worksheet.Cells[recordIndex, colIndex++].Value =
                          report.DoorName;
                      worksheet.Cells[recordIndex, colIndex++].Value =
                          report.Icu?.Building.Name;
                      worksheet.Cells[recordIndex, colIndex++].Value = report.Icu != null ? ((VerifyMode)report.Icu.VerifyMode).GetDescription() : string.Empty;
                      worksheet.Cells[recordIndex, colIndex++].Value = Constants.AntiPass.Contains(report.Antipass) ?
                          ((Antipass)Enum.Parse(typeof(Antipass), report.Antipass))
                          .GetDescription() : EventLogResource.lblUnknown;
                      worksheet.Cells[recordIndex, colIndex].Value = ((EventType)report.EventType).GetDescription();*/
                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        /// <summary>
        /// Export data in pdf
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param>
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="doorIds"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public BaseReportModel<EventReportPdfModel> ExportPdf(int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered,
            string accessDateFrom, string accessDateTo, string accessTimeFrom, string accessTimeTo, List<int> eventType,
            string userName, List<string> inOutType, string cardId, List<int> doorIds, string building,
            string department, string verifyMode,
            int? company)
        {
            var data = FilterDataWithOrder(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, eventType,
                userName, inOutType, cardId, doorIds, building, department, verifyMode, company, sortColumn,
                sortDirection,
                out totalRecords, out recordsFiltered).ToList();

            var companyData = _unitOfWork.CompanyRepository.GetById(_httpContext.User.GetCompanyId());
            var result = new BaseReportModel<EventReportPdfModel>(companyData);
            foreach (var report in data)
            {
                result.Rows.Add(new EventReportPdfModel
                {
                    EventTime = report.EventTime.ToSettingDateTimeString(),
                    //UserCode = report.EventType != (int)EventType.UnRegisteredCard
                    //    ? report.User?.UserCode
                    //    : null,
                    FirstName = report.EventType != (int) EventType.UnRegisteredCard
                        ? report.User?.FirstName
                        : string.Empty,
                    CardId = report.CardId,
                    DeviceAddress = report.Icu?.DeviceAddress,
                    DoorName = report.DoorName,
                    CardType = ((CardType) report.CardType).GetDescription(),
                    InOutStatus = ((Antipass) Enum.Parse(typeof(Antipass), report.Antipass)).GetDescription(),
                    EventType = ((EventType) report.EventType).GetDescription(),
                    Company = report.Company.Name
                });
            }

            return result;
        }

        /// <summary>
        /// Export data in txt format
        /// </summary>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <param name="eventType"></param>
        /// <param name="userName"></param
        /// <param name="inOutType"></param>
        /// <param name="cardId"></param>
        /// <param name="doorIds"></param>
        /// <param name="verifyMode"></param>
        /// <param name="company"></param>
        /// <param name="building"></param>
        /// <param name="department"></param>
        /// <returns></returns>
        public byte[] ExportTxt(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<string> inOutType, string cardId, List<int> doorIds,
            string building, string department, string verifyMode, int? company)
        {
            var data = FilterDataWithOrder(accessDateFrom, accessDateTo, accessTimeFrom, accessTimeTo, eventType,
                userName, inOutType, cardId, doorIds, building, department, verifyMode, company, sortColumn,
                sortDirection,
                out totalRecords, out recordsFiltered).ToList();

            var accountType = _httpContext.User.GetAccountType();
            var reportTxt = new StringBuilder();
            // Build the file content
            foreach (var report in data)
            {
                var obj = new List<object>
                {
                    report.EventTime.ToSettingDateTimeString(),
                    report.EventType != (int) EventType.UnRegisteredCard ? report.User?.FirstName : string.Empty,
                    report.CardId,
                    report.Icu?.DeviceAddress,
                    report.DoorName,
                    ((CardType) report.CardType).GetDescription(),
                    Constants.AntiPass.Contains(report.Antipass)
                        ? ((Antipass) Enum.Parse(typeof(Antipass), report.Antipass))
                        .GetDescription()
                        : EventLogResource.lblUnknown,
                    ((EventType) report.EventType).GetDescription()
                };
                if (accountType == (short) AccountType.SuperAdmin)
                {
                    obj.Insert(0, report.Company.Name);
                }

                reportTxt.AppendLine(string.Join(",", obj));
            }

            if (accountType == (short) AccountType.SuperAdmin)
            {
                _header.Insert(0, EventLogResource.lblCompany);
            }

            byte[] buffer = Encoding.UTF8.GetBytes($"{string.Join(",", _header)}\r\n{reportTxt}");
            return buffer;
        }

        /// <summary>   Export text 2. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="sortColumn">       column using in sorting. </param>
        /// <param name="sortDirection">    Ascending or Descending. </param>
        /// <param name="totalRecords">     [out] total records count. </param>
        /// <param name="recordsFiltered">  [out] filtered records count. </param>
        /// <param name="accessDateFrom">   Start date. </param>
        /// <param name="accessDateTo">     End date. </param>
        /// <param name="accessTimeFrom">   Start time. </param>
        /// <param name="accessTimeTo">     End time. </param>
        /// <param name="eventType">        Event type. </param>
        /// <param name="userName">         User name. </param>
        /// <param name="inOutType">        In or Out. </param>
        /// <param name="cardId">           Card Id. </param>
        /// <param name="doorIds">          Door Id list. </param>
        /// <param name="buildingList">     List of buildings. </param>
        /// <param name="department">       Name of department. </param>
        /// <param name="CardType">         Type of the card. </param>
        /// <param name="company">          Company Id. </param>
        /// <returns>   A byte[]. </returns>
        public byte[] ExportTxtReport(int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered, string accessDateFrom, string accessDateTo, string accessTimeFrom,
            string accessTimeTo,
            List<int> eventType, string userName, List<int> inOutType, string cardId, List<int> doorIds,
            List<int> buildingList, string department, List<int> CardType, int? company)
        {
            var data = FilterDataWithOrderEventLogReport(accessDateFrom,
                accessDateTo, accessTimeFrom, accessTimeTo, doorIds, userName,
                cardId, department, inOutType, eventType, buildingList,
                CardType, "", sortColumn, sortDirection, 0, 0, out totalRecords, out recordsFiltered).ToList();

            var accountType = _httpContext.User.GetAccountType();
            var reportTxt = new StringBuilder();
            int idx = 1;
            // Build the file content
            foreach (var report in data)
            {
                var card = _unitOfWork.CardRepository.GetByCardId(_httpContext.User.GetCompanyId(), report.CardId);
                var obj = new List<object>
                {
                    /*report.EventTime?.ToSettingDateTimeString(),
                    report.EventType != (int) EventType.UnRegisteredCard ? report.User?.FirstName : string.Empty,
                    report.CardId,
                    report.Icu?.DeviceAddress,
                    report.DoorName,
                    ((CardType) report.CardType).GetDescription(),
                    Constants.AntiPass.Contains(report.Antipass) ?
                        ((Antipass)Enum.Parse(typeof(Antipass), report.Antipass))
                        .GetDescription() : EventLogResource.lblUnknown,
                    ((EventType) report.EventType).GetDescription()

    */ idx++,
                    report.AccessTime,
                    report.UserName,
                    report.BirthDay,
                    report.EmployeeNumber,
                    report.Department,
                    report.CardId,
                    report.DeviceAddress,
                    report.DoorName,
                    report.Building,
                    report.InOut,
                    report.EventDetail,
                    report.IssueCount,
                    report.CardStatus,
                    report.CardType

                };

                //if (accountType == (short) AccountType.SuperAdmin)
                //{
                //    obj.Insert(0, report.Company.Name);
                //}

                reportTxt.AppendLine(string.Join(",", obj));
            }

            if (accountType == (short) AccountType.SuperAdmin)
            {
                _header.Insert(0, EventLogResource.lblCompany);
            }

            byte[] buffer = Encoding.UTF8.GetBytes($"{string.Join(",", _header)}\r\n{reportTxt}");
            return buffer;
        }

        /// <summary>   Saves a system log export. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="fileName"> Filename of the file. </param>
        public void SaveSystemLogExport(string fileName)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Save system log
                        var content = $"{ActionLogTypeResource.Export} {EventLogResource.lblEventLog}\n" +
                                      $"{CommonResource.lblFileName} : {fileName}";

                        _unitOfWork.SystemLogRepository.Add(1, SystemLogType.Report, ActionLogType.Export,
                            content, null, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>   Saves a system log recovery. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="deviceAddress">  Device address. </param>
        public void SaveSystemLogRecovery(string deviceAddress)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Save system log
                        var content = $"{EventLogResource.lblEventRecovery}\n" +
                                      $"{DeviceResource.lblDeviceAddress} : {deviceAddress}";
                        _unitOfWork.SystemLogRepository.Add(1, SystemLogType.EventRecovery, ActionLogType.Recovery,
                            content, null, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>   Gets event type list. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <returns>   The event type list. </returns>
        public EventTypeListModel GetEventTypeList()
        {
            var listEvent = EnumHelper.ToEnumList<EventType>();
            return new EventTypeListModel {EventTypeList = listEvent};
        }


        /// <summary>
        /// Get paginated event recovery devices
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<IcuDevice> GetPaginatedRecoveryDevices(string filter, int pageNumber, int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            //var data = _unitOfWork.IcuDeviceRepository.GetActiveDevicesByCompany(companyId).Include(m => m.Building);

            var data = _unitOfWork.AppDbContext.IcuDevice
                .Include(m => m.Building)
                .Where(m => m.CompanyId == companyId && !m.IsDeleted);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.DeviceAddress.ToLower().Contains(filter.ToLower()) ||
                    x.Building.Name.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Device.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Device[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        /// <summary>   Gets paginated event recovery inquire devices. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="deviceIds">        List of identifiers for the devices. </param>
        /// <param name="accessDateFrom">   Start date. </param>
        /// <param name="accessDateTo">     End date. </param>
        /// <param name="accessTimeFrom">   Start time. </param>
        /// <param name="accessTimeTo">     End time. </param>
        /// <param name="filter">           Word for searching. </param>
        /// <param name="pageNumber">       Page number. </param>
        /// <param name="pageSize">         Page size. </param>
        /// <param name="sortColumn">       column using in sorting. </param>
        /// <param name="sortDirection">    Ascending or Descending. </param>
        /// <param name="totalRecords">     [out] total records count. </param>
        /// <param name="recordsFiltered">  [out] filtered records count. </param>
        /// <returns>   The paginated event recovery inquire devices. </returns>
        public List<RecoveryDeviceModel> GetPaginatedEventRecoveryInquireDevices(List<int> deviceIds,
            string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo, string filter, int pageNumber,
            int pageSize,
            int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var data = _unitOfWork.AppDbContext.IcuDevice
                .Include(m => m.Building)
                .Where(m => m.CompanyId == companyId && !m.IsDeleted);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.DeviceAddress.ToLower().Contains(filter.ToLower()) ||
                    x.Building.Name.ToLower().Contains(filter.ToLower()));
            }

            if (deviceIds.Count > 0)
            {
                data = data.Where(x =>
                    deviceIds.Contains(x.Id));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Device.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Device[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            //filter device complete

            //Convert to RecoveryDeviceModel
            var recoveryDeviceModels = data.AsEnumerable<IcuDevice>().Select(Mapper.Map<RecoveryDeviceModel>)
                .AsQueryable().ToList();
            var rand = new Random();

            //Set Process ID
            foreach (var recoveryDeviceModel in recoveryDeviceModels)
            {
                var processId = rand.Next(1000000000).ToString();
                recoveryDeviceModel.ProcessId = processId;
            }

            var eventCountByDeviceModel = EventCountInquiry(recoveryDeviceModels, accessDateFrom, accessDateTo,
                accessTimeFrom, accessTimeTo);


            foreach (var recoveryDeviceModel in recoveryDeviceModels)
            {
                recoveryDeviceModel.DB = eventCountByDeviceModel.Where(m => m.DeviceId == recoveryDeviceModel.Id)
                    .Select(m => m.Count).ToList().FirstOrDefault();

            }


            return recoveryDeviceModels;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recoveryDeviceModels"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        /// <returns></returns>
        public List<EventCountByDeviceModel> EventCountInquiry(List<RecoveryDeviceModel> recoveryDeviceModels,
            string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo)
        {
            var fromDateTime = "00000000000000";
            var toDateTime = "00000000000000";
            var data = _unitOfWork.AppDbContext.EventLog
                .Include(m => m.User)
                .Include(m => m.User.Department)
                .Include(m => m.Icu)
                .Include(m => m.Icu.Building)
                .Include(m => m.Company)
                .AsQueryable();

            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
                data = data.Where(m =>
                    m.EventTime >= accessDateTimeFrom);
                fromDateTime = accessDateTimeFrom.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
                data = data.Where(m =>
                    m.EventTime <= accessDateTimeTo);
                toDateTime = accessDateTimeTo.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
            }

            var dbCountList = data
                .Where(d => recoveryDeviceModels.Count <= 0 || recoveryDeviceModels.Select(m => m.Id).Contains(d.IcuId))
                .GroupBy(g => g.IcuId).Select(r => new EventCountByDeviceModel
                {
                    DeviceId = r.Key,
                    Count = r.Count()
                }).ToList();

            foreach (var recoveryDeviceModel in recoveryDeviceModels)
            {
                var deviceAddress = _unitOfWork.IcuDeviceRepository.GetByIcuId(recoveryDeviceModel.Id).DeviceAddress;
                var processId = recoveryDeviceModel.ProcessId;
                SendRequestEventCountToIcu(fromDateTime, toDateTime, deviceAddress, processId);
            }

            return dbCountList;
        }


        /// <summary>
        /// Make Event Count protocol data
        /// </summary>
        /// <param name="protocolType"></param>
        /// <param name="fromDateTime"></param>
        /// <param name="toDateTime"></param>
        /// <param name="processId"></param>
        /// <returns></returns>
        public EventCountProtocolData MakeRequestEventCountProtocolData(string fromDateTime, string toDateTime,
            string processId, string protocolType)
        {
            var eventCountProtocolData = new EventCountProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };

            var eventCountProtocolDataHeader = new EventCountProtocolDataHeader()
            {
                FromTime = fromDateTime,
                ToTime = toDateTime,
                ProcessId = processId
            };

            eventCountProtocolData.Data = eventCountProtocolDataHeader;
            return eventCountProtocolData;
        }

        /// <summary>
        /// Send update holiday to rabbit mq server
        /// <param name="deviceAddress"></param>
        /// <param name="groupMsgId"></param> 
        /// <param name="isPublish"></param>
        /// <param name="isSave"></param>
        /// </summary>
        public void SendRequestEventCountToIcu(string fromDateTime, string toDateTime, string deviceAddress,
            string processId, string groupMsgId = "", bool isPublish = true, bool isSave = true)
        {
            string routingKey = Constants.RabbitMq.EventLogEventCountTopic + "." + deviceAddress;
            var protocolData = MakeRequestEventCountProtocolData(fromDateTime, toDateTime, processId,
                Constants.Protocol.EventLogEventCount);
            var message = protocolData.ToString();
            if (isPublish)
            {
                _queueService.Publish(routingKey, message);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="models"></param>
        /// <param name="accessDateFrom"></param>
        /// <param name="accessDateTo"></param>
        /// <param name="accessTimeFrom"></param>
        /// <param name="accessTimeTo"></param>
        public void EventRecovery(List<EventRecoveryProgressModel> models, string accessDateFrom,
            string accessDateTo, string accessTimeFrom, string accessTimeTo)
        {

            var fromDateTime = "00000000000000";
            var toDateTime = "00000000000000";
            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTime(accessDateFrom, accessTimeFrom, false);
                fromDateTime = accessDateTimeFrom.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTime(accessDateTo, accessTimeTo, true);
                toDateTime = accessDateTimeTo.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
            }

            foreach (var model in models)
            {
                var id = model.DeviceId;
                var device = _unitOfWork.IcuDeviceRepository.GetByIcuId(id);

                var processId = model.ProcessId;
                SendEventRecoveryToIcu(processId, fromDateTime, toDateTime, device.DeviceAddress);
                SaveSystemLogRecovery(device.DeviceAddress);
            }
        }

        /// <summary>
        /// Make Event Count protocol data
        /// </summary>
        /// <param name="processId"></param>
        /// <param name="fromDateTime"></param>
        /// <param name="toDateTime"></param>
        /// <param name="protocolType"></param>
        /// <returns></returns>
        public EventRecoveryProtocolData MakeEventRecoveryProtocolData(string processId, string fromDateTime,
            string toDateTime, string protocolType)
        {
            var eventRecoveryProtocolData = new EventRecoveryProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };

            var eventRecoveryProtocolDataHeader = new EventRecoveryProtocolDataHeader()
            {
                FromTime = fromDateTime,
                ToTime = toDateTime,
                FrameIndex = 0,
                ProcessId = processId
            };

            eventRecoveryProtocolData.Data = eventRecoveryProtocolDataHeader;
            return eventRecoveryProtocolData;
        }

        /// <summary>
        /// Send update holiday to rabbit mq server
        /// <param name="processId"></param>
        /// <param name="fromDateTime"></param> 
        /// <param name="toDateTime"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="groupMsgId"></param>
        /// <param name="isPublish"></param>
        /// <param name="isSave"></param>
        /// </summary>
        private void SendEventRecoveryToIcu(string processId, string fromDateTime, string toDateTime,
            string deviceAddress = null, string groupMsgId = "", bool isPublish = true, bool isSave = true)
        {
            string routingKey;
            if (!string.IsNullOrEmpty(deviceAddress))
            {
                routingKey = Constants.RabbitMq.EventRecoveryTopic + "." + deviceAddress;
            }
            else
            {
                routingKey = Constants.RabbitMq.EventRecoveryTopic;
            }

            var protocolData = MakeEventRecoveryProtocolData(processId, fromDateTime, toDateTime,
                Constants.Protocol.EventLogEventRecovery);
            var message = protocolData.ToString();
            if (isPublish)
            {
                _queueService.Publish(routingKey, message);
            }
        }

        /// <summary>   Gets card status. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="cardId">   Card ID. </param>
        /// <returns>   The card status. </returns>
        public int GetCardStatus(string cardId)
        {
            var user = _unitOfWork.UserRepository.GetByCardId(_httpContext.User.GetCompanyId(), cardId);
            if (user != null)
                return _unitOfWork.CardRepository.GetCardStatusByUserIdAndCardId(_httpContext.User.GetCompanyId(),
                    user.Id, cardId);
            else
                return (int) (CardStatus.Normal);
        }

        /// <summary>   Gets access history attendance. </summary>
        /// <remarks>   Edward, 2020-03-02. </remarks>
        /// <param name="userId">           Identifier for the user. </param>
        /// <param name="start">            The start Date/Time. </param>
        /// <param name="end">              The end Date/Time. </param>
        /// <param name="pageNumber">       Page Number. </param>
        /// <param name="pageSize">         Page size. </param>
        /// <param name="sortColumn">       column using in sorting. </param>
        /// <param name="sortDirection">    ascending or descending. </param>
        /// <param name="totalRecords">     [out] total records count. </param>
        /// <param name="recordsFiltered">  [out] filtered record count. </param>
        /// <returns>   The access history attendance. </returns>
        public IQueryable<EventLog> GetAccessHistoryAttendance(int userId,
            DateTime start, DateTime end, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var endDate = start.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
            var startDate = start.Date.AddHours(0).AddMinutes(0).AddSeconds(0);
            var data = _unitOfWork.AppDbContext.EventLog.Where(c =>
                    c.CompanyId == _httpContext.User.GetCompanyId()
                    && c.UserId == userId && c.EventTime >= startDate && c.EventTime <= endDate &&
                    c.EventType == (short) EventType.NormalAccess)
                .Include(m => m.User)
                .Include(m => m.User.Department)
                .Include(m => m.Icu)
                .Include(m => m.Icu.Building)
                .Include(m => m.Company)
                .Include(m => m.Visit)
                .AsQueryable();

            totalRecords = data.Count();

            recordsFiltered = data.Count();
            data = data.OrderByDescending(m => m.EventTime);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        public void CreateEventLogForDesktopApp(string rid, User user, string message, string actionType)
        {
            var eventType = (short) EventType.NormalAccess;
            if (message == MessageResource.msgDynamicQrExpired)
            {
                eventType = (short) EventType.ExpiredQrCode;
            }
            else if (message == MessageResource.msgUserIsInValid)
            {
                eventType = (short) EventType.UnRegisteredCard;
            }

            var identification = _unitOfWork.CardRepository.GetQrCode(user.Id);
            var icuDevice = _unitOfWork.IcuDeviceRepository.GetDeviceByRid(_httpContext.User.GetCompanyId(), rid);
            var buildingId = _unitOfWork.IcuDeviceRepository.GetByIcuId(icuDevice.Id).BuildingId;
            var eventLog = new EventLog
            {
                IcuId = icuDevice.Id,
                DoorName = icuDevice.Name,
                CompanyId = icuDevice.CompanyId,
                CardId = identification.CardId,
                IssueCount = 0,
                UserId = user?.Id,
                UserName = user?.FirstName + user?.LastName,
                DeptId = user?.DepartmentId,
                Antipass = actionType,
                EventType = eventType,
                EventTime = DateTime.Now,
                CardType = (short) CardType.QrCode,
            };

            if (_unitOfWork.EventLogRepository.IsDuplicated(eventLog))
            {
                //eventLogs.Add(eventLog);
                return;
            }

            _unitOfWork.EventLogRepository.Add(eventLog);
            
            if (eventType != (short) EventType.ExpiredQrCode && eventType != (short) EventType.UnRegisteredCard)
            {
                if (actionType == Constants.Attendance.In || actionType == Constants.Attendance.Out)
                {
                    new AttendanceService(new HttpContextAccessor(), _configuration).AddClockInOut(user, actionType, DateTime.Now, buildingId.Value);
                }
            }
            var eventLogJsonString = JsonConvert.SerializeObject(eventLog);
            var eventLogs = new List<EventLog>{eventLog};
            SendDataToFe(eventLogs, user,icuDevice);
            AppLog.EventSave(eventLogJsonString);
        }
        
        private void SendDataToFe( List<EventLog> eventLogs, User user, IcuDevice icuDevice)
        {
            for(int i = 0; i < eventLogs.Count(); i++)
            {
                var department =
                    _unitOfWork.DepartmentRepository.GetByIdAndCompanyId(user.DepartmentId,
                        _httpContext.User.GetCompanyId());
                int minus = eventLogs.Count() == 2 ? 2 : 1;

                var eventLog = eventLogs.ElementAt(eventLogs.Count()-minus+i);
                
                var eventLogDetail = Mapper.Map<SendEventLogDetailData>(eventLog);
                eventLogDetail.Id = Guid.NewGuid();
                eventLogDetail.Device = icuDevice.DeviceAddress;
                eventLogDetail.UnixTime = (DateTimeOffset.Parse(eventLogDetail.AccessTime).UtcDateTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                eventLogDetail.DoorName = icuDevice.Name;

                eventLogDetail.Department = department.DepartName;
                eventLogDetail.UserName = user?.FirstName + " " + user?.LastName;
                eventLogDetail.ExpireDate = user?.ExpiredDate.ToSettingDateString();
                eventLogDetail.UserId = user?.Id;
                eventLogDetail.BuildingId = icuDevice.BuildingId.Value;
                eventLogDetail.InOut = eventLog.Antipass;

                var events = new List<SendEventLogDetailData> { eventLogDetail };

                var result = new SendEventLogListModelData
                {
                    MsgId = Guid.NewGuid().ToString(),
                    Type = Constants.Protocol.EventLogWebApp,
                    Data = new SendEventLogHeaderData
                    {
                        Total = 1,
                        Events = events
                    }
                };

                var jsonString = result.ToString();

                var companyCode = icuDevice.CompanyId != null ? icuDevice.Company.Code : "";

                //Send data to the frontend
                var message = Encoding.UTF8.GetBytes(jsonString);

                var routingKey = $"{Constants.RabbitMq.EventLogJsonTopic}.{companyCode}";

                //_queueService.Publish(Constants.RabbitMq.EventLogJsonTopic, message, 1);
                _queueService.Publish(routingKey, message, 1);

                if (eventLogs.Count() != 2)  break;
            }
           
        }


        /// <summary>
        /// Generate test data
        /// </summary>
        /// <param name="numberOfEvent"></param>
        public void GenerateTestData(int numberOfEvent)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var defaultDepartment =
                _unitOfWork.DepartmentRepository.GetDefautDepartmentByCompanyId(companyId);

            var device = _unitOfWork.IcuDeviceRepository.GetByCompany(companyId).First();

            var user = _unitOfWork.UserRepository.GetByCompanyId(companyId).FirstOrDefault();

            var card = _unitOfWork.CardRepository.GetByUserId(companyId, user.Id).FirstOrDefault();

            for (var i = 0; i < numberOfEvent; i++)
            {
                var fakeEvent = new Faker<EventLog>()
                    .RuleFor(u => u.Antipass, f => Antipass.In.GetDescription())
                    .RuleFor(u => u.CardId, f => card.CardId)
                    .RuleFor(u => u.IssueCount, f => 0)
                    .RuleFor(u => u.CardType, f => (short) 0)
                    .RuleFor(u => u.CompanyId, f => companyId)
                    .RuleFor(u => u.DeptId, f => 1)
                    .RuleFor(u => u.DoorName, f => device.Name)
                    .RuleFor(u => u.EventTime, f => DateTime.UtcNow)
                    .RuleFor(u => u.EventType, f => (int) EventType.NormalAccess)
                    .RuleFor(u => u.IcuId, f => device.Id)
                    .RuleFor(u => u.Index, f => (long) 0)

                    .RuleFor(u => u.UserName, f => user.FirstName)
                    .RuleFor(u => u.UserId, f => user.Id)
                    .RuleFor(u => u.IsVisit, f => false)

                    .RuleFor(u => u.CardStatus, f => (short) 0);

                var testEvent = fakeEvent.Generate();
                _unitOfWork.EventLogRepository.Add(testEvent);
            }
            _unitOfWork.Save();
        }


        //=====================================================================================================================================//
        // For Duali-Korea


        public IQueryable<EventLog> GetAttendenceForDuali(string accessDateFrom, string accessDateTo,
            List<int> userIds, List<int> departmentIds)
        {
            var data = _unitOfWork.AppDbContext.EventLog
                .Include(m => m.User)
                .Include(m => m.User.Department)
                .AsQueryable();

            var timezone = _unitOfWork.AccountRepository.Get(m =>
               m.Id == _httpContext.User.GetAccountId() && !m.IsDeleted).TimeZone;

            foreach (var eachData in data)
            {
                eachData.EventTime = Helpers.ConvertToUserTimeZoneReturnDate(eachData.EventTime, timezone);
            }

            if (!string.IsNullOrEmpty(accessDateFrom))
            {
                var accessDateTimeFrom = Helpers.GetFromToDateTimeConvert(accessDateFrom, "", false);

                data = data.Where(m =>
                    m.EventTime >= accessDateTimeFrom);
            }

            if (!string.IsNullOrEmpty(accessDateTo))
            {
                var accessDateTimeTo = Helpers.GetFromToDateTimeConvert(accessDateTo, "", true);

                data = data.Where(m =>
                    m.EventTime <= accessDateTimeTo);
            }

            if (userIds != null && userIds.Count() > 0)
            {
                data = data.Where(m => userIds.Contains(m.UserId ?? 0));
            }

            if (departmentIds != null && departmentIds.Count() > 0)
            {
                data = data.Where(m => departmentIds.Contains(m.DeptId ?? 0));
            }

            return data;
        }
    }
}
