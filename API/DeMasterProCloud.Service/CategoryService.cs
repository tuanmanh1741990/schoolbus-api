﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Category;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MoreLinq.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Category service interface
    /// </summary>
    public interface ICategoryService
    {
        List<CategoryListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<CategoryOptionListModel> GetOptionsPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection);

        void Add(List<CategoryModel> model);
        void Update(List<CategoryModel> model);
        void Delete(Category category);

        void SetUserOptions(int userId, List<int> optionIds);
        void DeleteUserOptions(int userId, List<int> optionIds = null);

        Category GetCategoryByIdAndCompanyId(int id, int companyId);
        CategoryOption GetCategoryOptionById(int optionId);

        List<Category> GetCategoriesByParentIdAndCompanyId(int parentId, int companyId);
        List<CategoryOption> GetCategoryOptionsByUserId(int userId);
        List<CategoryOption> GetCategoryOptionsByCategoryId(int categoryId);
        List<int> GetCategoryOptionIdsByUserId(int userId);
        List<HeaderData> GetCategoryHeadersByCompanyId(int companyId, int startId);


        bool IsCategoryNameExist(string categoryName);

        bool CheckCategoryOption(int? parentCategoryId, List<CategoryOptionModel> optionModels);

        IEnumerable<Node> GetCategoryHierarchy();
        IEnumerable<Node> GetOptionHierarchy();
    }

    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;

        public CategoryService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor,
            ILogger<DepartmentService> logger)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
        }

        /// <summary>
        /// Add categories
        /// </summary>
        /// <param name="model"> list of categoryModel </param>
        public void Add(List<CategoryModel> model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {

                        foreach (var categoryModel in model)
                        {
                            var companyId = _httpContext.User.GetCompanyId();
                            var category = Mapper.Map<Category>(categoryModel);
                            category.CompanyId = companyId;

                            string parentName = null;

                            if (category.ParentCategoryId != null)
                            {
                                // In this case, the category has parent.
                                // So, category.ParentCa
                                parentName = GetCategoryNameByIdAndCompany(category.ParentCategoryId.Value, companyId);
                            }
                            else
                            {
                                parentName = "None";
                            }

                            _unitOfWork.CategoryRepository.Add(category);
                            _unitOfWork.Save();

                            var optionModelList = categoryModel.CategoryOptions;
                            List<string> optionNames = new List<string>();

                            foreach (var optionModel in optionModelList)
                            {
                                var option = Mapper.Map<CategoryOption>(optionModel);
                                option.CategoryId = category.Id;

                                optionNames.Add(option.Name);

                                _unitOfWork.CategoryOptionRepository.Add(option);
                            }

                            //Save system log
                            var content = $"{CategoryResource.lblAddNew}";
                            var contentsDetails = $"{CategoryResource.lblCategoryName} : {category.Name}" +
                                                $"\n{CategoryResource.lblParentCategoryName} : {parentName}" +
                                                $"\n{CategoryResource.lblOptionName} : {String.Join(", ", optionNames)}";

                            _unitOfWork.SystemLogRepository.Add(category.Id, SystemLogType.Category, ActionLogType.Add,
                                content, contentsDetails, null, category.CompanyId);

                            _unitOfWork.Save();
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Update categories
        /// </summary>
        /// <param name="model"> list of CategoryModel </param>
        public void Update(List<CategoryModel> model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var categoryModel in model)
                        {

                            var companyId = _httpContext.User.GetCompanyId();
                            var category = GetCategoryByIdAndCompanyId(categoryModel.Id, companyId);

                            List<string> changes = new List<string>();
                            CheckChange(category, categoryModel, ref changes);

                            // Check categoryOption Id
                            List<int> oldOptions = new List<int>();
                            oldOptions = GetCategoryOptionsByCategoryId(category.Id).Select(m => m.Id).ToList();
                            List<int> newOptions = new List<int>();
                            newOptions = categoryModel.CategoryOptions.Select(m => m.Id).ToList();

                            // option ids to be deleted.
                            var firstNotSecond = oldOptions.Except(newOptions).ToList();
                            // option ids to be added.
                            var secondNotFirst = newOptions.Except(oldOptions).ToList();

                            DeleteOptions(firstNotSecond);

                            Mapper.Map(categoryModel, category);
                            category.CompanyId = companyId;

                            _unitOfWork.CategoryRepository.Update(category);
                            _unitOfWork.Save();

                            var optionModelList = categoryModel.CategoryOptions;

                            foreach (var optionModel in optionModelList)
                            {
                                if (secondNotFirst.Contains(optionModel.Id))
                                {
                                    var option = Mapper.Map<CategoryOption>(optionModel);
                                    option.CategoryId = category.Id;

                                    _unitOfWork.CategoryOptionRepository.Add(option);
                                }
                                else
                                {
                                    var option = GetCategoryOptionById(optionModel.Id);
                                    Mapper.Map(optionModel, option);
                                    option.CategoryId = category.Id;

                                    _unitOfWork.CategoryOptionRepository.Update(option);
                                }
                            }

                            //Save system log
                            var content = $"{CategoryResource.lblUpdate}";
                            var contentsDetails = String.Join(", ", changes);

                            _unitOfWork.SystemLogRepository.Add(category.Id, SystemLogType.Category, ActionLogType.Update,
                                content, contentsDetails, null, category.CompanyId);

                            _unitOfWork.Save();

                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();

                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a category
        /// </summary>
        /// <param name="category"> category to be deleted </param>
        public void Delete(Category category)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        // update category that has this deleted category's id value as parentCategoryId.
                        // ( parentCategoryId : int value -> null )
                        List<Category> categories = GetCategoriesByParentIdAndCompanyId(category.Id, companyId);

                        foreach (var categoryToUpdate in categories)
                        {
                            categoryToUpdate.ParentCategoryId = null;

                            _unitOfWork.CategoryRepository.Update(categoryToUpdate);

                            // update option that has this deletec category's option id value as parentOptionId.
                            // ( parentOptionId : int value -> null )
                            List<CategoryOption> options = GetCategoryOptionsByCategoryId(categoryToUpdate.Id);

                            foreach (var option in options)
                            {
                                option.ParentOptionId = null;

                                _unitOfWork.CategoryOptionRepository.Update(option);
                            }
                        }
                        _unitOfWork.Save();

                        // Delete category
                        _unitOfWork.CategoryRepository.Delete(category);

                        //Save system log
                        var content = $"{CategoryResource.lblDelete}";
                        var contentDetails = $"{CategoryResource.lblCategoryName} : {category.Name}";

                        _unitOfWork.SystemLogRepository.Add(category.Id, SystemLogType.Category,
                            ActionLogType.Delete, content, contentDetails, null, category.CompanyId);

                        //Save to database
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Get category data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<CategoryListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            var result = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return result.ToList();
        }

        /// <summary>
        /// Get category option data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<CategoryOptionListModel> GetOptionsPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection)
        {
            var data = FilterOptionDataWithOrder(filter, sortColumn, sortDirection);

            //var result = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return data;
        }


        /// <summary>
        /// Filter and order category data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<CategoryListModel> FilterDataWithOrder(string filter, int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var categoryData = _unitOfWork.AppDbContext.Category.Include(m => m.CategoryOption)
                .Where(m => m.CompanyId == companyId).OrderBy(m => m.Id).AsQueryable();

            totalRecords = categoryData.Count();

            // TODO [Edward]
            // Search function is needed in category??
            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                categoryData = categoryData.Where(x =>
                    x.Name.ToLower().Contains(filter));
            }

            recordsFiltered = categoryData.Count();

            var result = categoryData.AsEnumerable()
                .Select(m =>
                {
                    CategoryListModel listModel = new CategoryListModel
                    {
                        Id = m.Id,
                        Name = m.Name,
                        ParentCategoryId = m.ParentCategoryId,
                        OptionList = _unitOfWork.AppDbContext.CategoryOption.Where(c => c.CategoryId == m.Id).OrderBy(c => c.Id).Select(c => c.Id).ToList()
                    };
                    return listModel;
                });

            List<CategoryListModel> resultList = new List<CategoryListModel>();

            CategoryListModel noneModel = new CategoryListModel()
            {
                Id = 0,
                Name = "None",
                ParentCategoryId = null,
                OptionList = null
            };

            resultList.Add(noneModel);

            resultList = resultList.Concat(result.ToList()).ToList();

            return resultList;
        }

        /// <summary>
        /// Filter and order category option data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<CategoryOptionListModel> FilterOptionDataWithOrder(string filter, int sortColumn, string sortDirection)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var categoryOptionData = _unitOfWork.AppDbContext.CategoryOption.Include(m => m.Category)
                .Where(m => m.Category.CompanyId == companyId).OrderBy(m => m.CategoryId).ThenBy(m => m.Id).AsQueryable();

            var result = categoryOptionData.AsEnumerable()
                .Select(m =>
                {
                    CategoryOptionListModel listModel = new CategoryOptionListModel
                    {
                        Id = m.Id,
                        Name = m.Name,
                        ParentOptionId = m.ParentOptionId
                    };
                    return listModel;
                });

            List<CategoryOptionListModel> resultOption = new List<CategoryOptionListModel>();

            CategoryOptionListModel noneOption = new CategoryOptionListModel()
            {
                Id = 0,
                Name = "None",
                ParentOptionId = null
            };

            resultOption.Add(noneOption);

            resultOption = resultOption.Concat(result.ToList()).ToList();

            return resultOption;
        }

        /// <summary>
        /// Get category by id and company id
        /// </summary>
        /// <param name="id"> identifier of Category </param>
        /// <param name="companyId"> identifier of Company </param>
        /// <returns> Category </returns>
        public Category GetCategoryByIdAndCompanyId(int id, int companyId)
        {
            return _unitOfWork.CategoryRepository.GetCategoryByIdAndCompanyId(id, companyId);
        }

        /// <summary>
        /// Get categories by parentId and companyId
        /// </summary>
        /// <param name="parentId"> identifier of parent category </param>
        /// <param name="companyId"> identifier of Company </param>
        /// <returns> list of Category </returns>
        public List<Category> GetCategoriesByParentIdAndCompanyId(int parentId, int companyId)
        {
            var categories = _unitOfWork.CategoryRepository.GetCategoriesByParentIdAndCompanyId(parentId, companyId);

            return categories.ToList();
        }

        /// <summary>
        /// Get category header data by companyId
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<HeaderData> GetCategoryHeadersByCompanyId(int companyId, int startId)
        {
            var headerId = startId;

            var categories = _unitOfWork.CategoryRepository.GetCategoriesByCompanyId(companyId).OrderBy(m => m.Id)
                            .Select(m => new HeaderData() {
                                HeaderId = headerId + m.Id,
                                HeaderName = m.Name,
                                HeaderVariable = m.Id.ToString(),
                                IsCategory = true
                            }).ToList();

            return categories;
        }

        /// <summary>
        /// Get category option by option id
        /// </summary>
        /// <param name="optionId"> identifier of option </param>
        /// <returns> CategoryOption </returns>
        public CategoryOption GetCategoryOptionById(int optionId)
        {
            var option = _unitOfWork.CategoryOptionRepository.GetById(optionId);

            return option;
        }

        /// <summary>
        /// Get category option(s) by category id
        /// </summary>
        /// <param name="categoryId"> identifier of Category </param>
        /// <returns> list of CategoryOption </returns>
        public List<CategoryOption> GetCategoryOptionsByCategoryId(int categoryId)
        {
            var options = _unitOfWork.CategoryOptionRepository.GetOptionByCategoryId(categoryId);

            return options.ToList();
        }


        /// <summary>
        /// Get category option(s) by user Id
        /// </summary>
        /// <param name="userId"> identifier of User </param>
        /// <returns> list of CategoryOption id </returns>
        public List<CategoryOption> GetCategoryOptionsByUserId(int userId)
        {
            var optionIds = _unitOfWork.UserCategoryOptionRepository.GetByUserId(userId).Select(m => m.CategoryOptionId).ToList();

            var options = _unitOfWork.CategoryOptionRepository.GetByIds(optionIds);

            return options.ToList();
        }


        /// <summary>
        /// Get category option(s) id by user Id
        /// </summary>
        /// <param name="userId"> identifier of User </param>
        /// <returns> list of CategoryOption id </returns>
        public List<int> GetCategoryOptionIdsByUserId(int userId)
        {
            var optionIds = _unitOfWork.UserCategoryOptionRepository.GetByUserId(userId).Select(m => m.CategoryOptionId).ToList();

            return optionIds.ToList();
        }

        /// <summary>
        /// Get name of category by Id and company Id
        /// </summary>
        /// <param name="id"> identifier of category </param>
        /// <param name="companyId"> identitier of company </param>
        /// <returns> name of category </returns>
        public string GetCategoryNameByIdAndCompany(int id, int companyId)
        {
            var category = _unitOfWork.CategoryRepository.GetCategoryByIdAndCompanyId(id, companyId);

            if (category != null)
            {
                return category.Name;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get name of category option by Id
        /// </summary>
        /// <param name="id"> identifier of category option </param>
        /// <returns> name of category </returns>
        public string GetOptionNameById(int id)
        {
            var option = _unitOfWork.CategoryOptionRepository.GetById(id);

            if (option != null)
            {
                return option.Name;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Check if category name is exist
        /// </summary>
        /// <param name="categoryName"> name of category </param>
        /// <returns> True if category is not null, False if category is null </returns>
        public bool IsCategoryNameExist(string categoryName)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var category = _unitOfWork.CategoryRepository.GetCategoryByNameAndCompanyId(categoryName, companyId);

            return category != null;
        }

        /// <summary>
        /// Check if category options is valid
        /// </summary>
        /// <param name="parentCategoryId"> identifier of parent category </param>
        /// <param name="categoryOptions"> list of categoryOptionModel </param>
        /// <returns> True if valid, False if not </returns>
        public bool CheckCategoryOption(int? parentCategoryId, List<CategoryOptionModel> optionModels)
        {
            var companyId = _httpContext.User.GetCompanyId();
            List<string> optionNames = new List<string>();

            foreach (var optionModel in optionModels)
            {
                if (optionNames.Contains(optionModel.Name))
                {
                    return false;
                }

                if (parentCategoryId != null && optionModel.ParentOptionId != null)
                {
                    var parentOption = GetCategoryOptionById(optionModel.ParentOptionId.Value);
                    if (parentOption.CategoryId != parentCategoryId)
                    {
                        return false;
                    }
                }

                optionNames.Add(optionModel.Name);
            }

            return true;
        }

        /// <summary>
        /// Get Category hierarchy
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Node> GetCategoryHierarchy()
        {
            var companyId = _httpContext.User.GetCompanyId();

            var categories = _unitOfWork.CategoryRepository.GetCategoriesByCompanyId(companyId)
                            .OrderBy(m => m.ParentCategoryId).ToList();

            var nodeItems = categories.Select(
                m => new Node
                {
                    Id = m.Id,
                    Text = m.Name,
                    Options = _unitOfWork.CategoryOptionRepository.GetOptionByCategoryId(m.Id)
                            .Select(c => new SimpleData() { Id = c.Id, Name = c.Name }).ToList(),
                    ParentId = m.ParentCategoryId
                });

            var nodes = nodeItems.BuildTree();

            return nodes;
        }

        /// <summary>
        /// Get category option hierarchy
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Node> GetOptionHierarchy()
        {
            var companyId = _httpContext.User.GetCompanyId();

            var categoryIds = _unitOfWork.CategoryRepository.GetCategoriesByCompanyId(companyId)
                            .OrderBy(m => m.ParentCategoryId).Select(m => m.Id).ToList();

            var options = _unitOfWork.CategoryOptionRepository.GetOptionByCategoryIds(categoryIds).ToList();

            var nodeItems = options.Select(
                m => new Node
                {
                    Id = m.Id,
                    Text = m.Name,
                    ParentId = m.ParentOptionId
                });

            var nodes = nodeItems.BuildTree();

            return nodes;
        }

        /// <summary>
        /// Delete CategoryOption
        /// </summary>
        /// <param name="optionIds"></param>
        public void DeleteOptions(List<int> optionIds)
        {
            foreach (var optionId in optionIds)
            {
                var childOptions = _unitOfWork.CategoryOptionRepository.GetOptionByParentId(optionId).ToList();


                // [Edward] 2020-05-06
                // before : Make childOption's parentOptionId as null if there are any child options.
                // after  : Category option cannot be deleted if there are any child options.
                //foreach(var childOption in childOptions)
                //{
                //    childOption.ParentOptionId = null;
                //    _unitOfWork.CategoryOptionRepository.Update(childOption);
                //}

                if(childOptions == null || !childOptions.Any())
                {
                    var categoryOption = _unitOfWork.CategoryOptionRepository.GetById(optionId);
                    _unitOfWork.CategoryOptionRepository.Delete(categoryOption);
                }
            }

            _unitOfWork.Save();
        }

        /// <summary>
        /// Delete CategoryOption of User
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="optionIds"></param>
        public void DeleteUserOptions(int userId, List<int> optionIds = null)
        {
            if (optionIds == null)
            {
                // Delete all options
                var userOption = _unitOfWork.UserCategoryOptionRepository.GetByUserId(userId);
                _unitOfWork.UserCategoryOptionRepository.DeleteRange(userOption);
            }
            else
            {
                foreach (var optionId in optionIds)
                {
                    var userOption = _unitOfWork.UserCategoryOptionRepository.GetByUserIdAndOptionId(userId, optionId).First();
                    _unitOfWork.UserCategoryOptionRepository.Delete(userOption);
                }
            }

            _unitOfWork.Save();
        }

        /// <summary>
        /// Set options to user with id
        /// </summary>
        /// <param name="userId"> Identifier of user </param>
        /// <param name="optionIds"> list of identifier of option </param>
        public void SetUserOptions(int userId, List<int> optionIds)
        {
            if (optionIds != null && optionIds.Any())
            {
                foreach (var optionId in optionIds)
                {
                    var userOption = new UserCategoryOption
                    {
                        UserId = userId,
                        CategoryOptionId = optionId
                    };

                    _unitOfWork.UserCategoryOptionRepository.Add(userOption);

                    _unitOfWork.Save();
                }

            }
        }

        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool CheckChange(Category category, CategoryModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                var companyId = _httpContext.User.GetCompanyId();
                var optionList = model.CategoryOptions;

                if (!category.Name.Equals(model.Name))
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, CategoryResource.lblCategoryName, category.Name, model.Name));
                }

                if (category.ParentCategoryId != model.ParentCategoryId)
                {
                    var oldParentName = category.ParentCategoryId == null
                                        ? ""
                                        : GetCategoryNameByIdAndCompany(category.ParentCategoryId.Value, companyId);
                    var newParentName = model.ParentCategoryId == null
                                        ? ""
                                        : GetCategoryNameByIdAndCompany(model.ParentCategoryId.Value, companyId);
                    changes.Add(string.Format(MessageResource.msgChangeInfo, CategoryResource.lblParentCategoryName, oldParentName, newParentName));
                }

                foreach (var option in optionList)
                {
                    var oldOption = GetCategoryOptionById(option.Id);

                    if(oldOption != null)
                    {
                        if (!oldOption.Name.Equals(option.Name))
                        {
                            changes.Add(string.Format(MessageResource.msgChangeInfo, CategoryResource.lblOptionName, oldOption.Name, option.Name));
                        }

                        if (oldOption.ParentOptionId != option.ParentOptionId)
                        {
                            var oldParentName = oldOption.ParentOptionId == null
                                                ? ""
                                                : GetOptionNameById(oldOption.ParentOptionId.Value);
                            var newParentName = option.ParentOptionId == null
                                                ? ""
                                                : GetOptionNameById(option.ParentOptionId.Value);
                            changes.Add(string.Format(MessageResource.msgChangeInfo, CategoryResource.lblParentOptionName, oldParentName, newParentName));
                        }
                    }
                    
                }
            }

            return changes.Count() > 0;
        }
    }
}