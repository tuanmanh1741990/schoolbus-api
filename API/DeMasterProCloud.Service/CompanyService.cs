﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Company;
using DeMasterProCloud.DataModel.Setting;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.Account;
using DeMasterProCloud.DataModel.PlugIn;
using EasyNetQ.Management.Client;
using EasyNetQ.Management.Client.Model;
using RabbitMQ.Client.Exceptions;
using User = DeMasterProCloud.DataAccess.Models.User;

namespace DeMasterProCloud.Service
{
    public interface ICompanyService
    {
        bool IsValidCompany(Company company);
        string MakeCompanyCode();
        int Add(CompanyModel model);
        void Update(CompanyModel model);
        void Delete(Company company);
        void DeleteRange(List<Company> companies);
        Company GetById(int? id);
        List<Company> Gets();
        void AssignToCompany(List<int> deviceIds, int companyId);

        IQueryable<CompanyListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<Company> GetExpiredCompaniesByDay(int numberOfDay = 0, bool bExactDay = true);
        void UpdateCompaniesToExpire(List<Company> companies);

        //void SendCompanyAccountMail(Company company, string originalPass);
        //void SendExpiredCompanyMail(List<Company> companies, int numberOfExpiredDay);
        Company GetRootCompany();
        bool IsExistCompanyCode(string companyCode);
        bool IsExistedCompanyAccount(int companyId, string userName);
        List<Company> GetByIds(List<int> ids);
        List<Company> GetCompanies();
        void UpdateLogo(string base64String);
        string GetCurrentLogo(Company company);
        CompanyDataModel GetDefaultInfoById(int companyId);
        string GetCodeById(int companyId);
        void ResetAesKeyCompanies();
        void ResetAesKeyCompany(int companyId);
        PlugIn GetPluginByCompany(int companyId);
        
        PlugIn GetPluginByCompanyAllowShowing(int companyId);
        int UpdatePluginByCompany(int companyId, PlugIns model);
    }

    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly HttpContext _httpContext;
        private readonly IMailService _mailService;
        private readonly IQueueService _queueService;
        private readonly ILogger _logger;
        private readonly IDeviceService _deviceService;

        public CompanyService(IUnitOfWork unitOfWork, IConfiguration configuration, IQueueService queueService,
            IMailService mailService, IHttpContextAccessor contextAccessor, ILogger<CompanyService> logger, IDeviceService deviceService)
        {
            _unitOfWork = unitOfWork;
            _configuration = configuration;
            _queueService = queueService;
            _httpContext = contextAccessor.HttpContext;
            _mailService = mailService;
            _logger = logger;
            _deviceService = deviceService;
        }

        /// <summary>
        /// Add company
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Add(CompanyModel model)
        {
            var companyId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Add company
                        var company = Mapper.Map<Company>(model);

                        if (!model.LogoEnable)
                        {
                            company.Logo = null;
                        }

                        if (!model.MiniLogoEnable)
                        {
                            company.MiniLogo = null;
                        }

                        //Add company code.
                        company.Code = MakeCompanyCode();
                        //Add company name.
                        company.Name = !string.IsNullOrEmpty(model.Name) ? model.Name : null;
                        //Add company Remark.
                        company.Remarks = !string.IsNullOrEmpty(model.Remarks) ? model.Remarks : null;
                        
                        
                        const string chars = Constants.DynamicQr.AllowChars;

                        var secretCode = new string(Enumerable.Repeat(chars, Constants.DynamicQr.LenghtOfSecretCode)
                            .Select(s => s[random.Next(s.Length)]).ToArray());
                        
                        
                        // Get 16 character from Secret Code
                        company.SecretCode = Helpers.GenerateCompanyKey();

                        _unitOfWork.CompanyRepository.Add(company);
                        _unitOfWork.Save();

                        //Add default department
                        _unitOfWork.DepartmentRepository.AddDefault(company);

                        //Add default timezone.
                        _unitOfWork.TimezoneRepository.AddDefaultTimezone(company.Id);
                        _unitOfWork.Save();

                        //Add default Access group
                        _unitOfWork.AccessGroupRepository.AddDefault(company, _configuration);
                        _unitOfWork.Save();

                        //Add default building.
                        _unitOfWork.BuildingRepository.AddDefault(company);
                        _unitOfWork.Save();
                        
                        //Add default working type.
                        _unitOfWork.WorkingRepository.AddWorkingTypeDefault(company.Id);
                        _unitOfWork.Save();

                        //Add default message setting.
                        _unitOfWork.DeviceMessageRepository.AddDefaultDeviceMessage(company.Id);
                        _unitOfWork.Save();
                        
                        //Add plug-Ins
                        _unitOfWork.PlugInRepository.AddPlugInDefault(company.Id);
                        _unitOfWork.Save();
                        
                        //Add default visit setting.
                        _unitOfWork.VisitRepository.CreateDefaultVisitSetting(company.Id);
                        _unitOfWork.Save();
                        
                        var settings = _configuration.GetSection(Constants.Settings.FileSettings)?.Get<List<FileSetting>>();
                        if (settings != null && settings.Any())
                        {
                            foreach (var unApplySetting in settings)
                            {
                                var setting = new Setting
                                {
                                    Key = unApplySetting.Key,
                                    Value = JsonConvert.SerializeObject(unApplySetting.Values),
                                    CompanyId = company.Id
                                };
                                _unitOfWork.SettingRepository.Add(setting);
                            }
                            _unitOfWork.Save();
                        }

                        //Save system log
                        //var content = $"{ActionLogTypeResource.Add}: {company.Code} ({CompanyResource.lblCompanyCode})";
                        var content = CompanyResource.lblAddNew;
                        var contentsDetails = $"{CompanyResource.lblCompanyName} : {company.Name} (code : {company.Code})";
                        _unitOfWork.SystemLogRepository.Add(company.Id, SystemLogType.Company, ActionLogType.Add, content, contentsDetails, null, company.Id);

                        var task = AddAccountCompanyMessageQueue(company, _configuration);
                        task.Wait();
                        _unitOfWork.Save();
                        transaction.Commit();
                        companyId = company.Id;
                        //Send mail
                        //SendCompanyAccountMail(company, model.Password);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            return companyId;
        }
        
        /// <summary>
        /// Update company
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public void Update(CompanyModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Update company
                        var company = _unitOfWork.CompanyRepository.GetById(model.Id);
                        var existingName = company.Name;

                        List<string> changes = new List<string>();

                        var isChange = CheckChange(company, model, ref changes);

                        if (!model.LogoEnable)
                        {
                            company.Logo = null;
                        }

                        if (!model.MiniLogoEnable)
                        {
                            company.MiniLogo = null;
                        }

                        if (!string.IsNullOrEmpty(model.Name))
                        {
                            company.Name = model.Name;
                        }

                        if (!string.IsNullOrEmpty(model.Remarks))
                        {
                            company.Remarks = model.Remarks;
                        }
                        
                        if (!string.IsNullOrEmpty(model.WebsiteUrl))
                        {
                            company.WebsiteUrl = model.WebsiteUrl;
                        }
                        if (!string.IsNullOrEmpty(model.ContactWEmail))
                        {
                            company.ContactWEmail = model.ContactWEmail;
                        }
                        if (!string.IsNullOrEmpty(model.Phone))
                        {
                            company.Phone = model.Phone;
                        }
                        if (!string.IsNullOrEmpty(model.Industries))
                        {
                            company.Industries = model.Industries;
                        }
                        if (!string.IsNullOrEmpty(model.Location))
                        {
                            company.Location = model.Location;
                        }
                        
                        _unitOfWork.CompanyRepository.Update(company);

                        //Save system log
                        var content = $"{CompanyResource.lblUpdateCompany}\n{CompanyResource.lblCompanyName} : {existingName}";
                        var contentsDetails = string.Join("\n", changes);
                        _unitOfWork.SystemLogRepository.Add(company.Id, SystemLogType.Company, ActionLogType.Update, content, contentsDetails, null, company.Id);

                        _unitOfWork.Save();
                        transaction.Commit();
                        
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Assign a door(s) to company
        /// This function is for Assign company button.
        /// </summary>
        /// <param name="deviceIds"></param>
        /// <param name="companyId"></param>
        public void AssignToCompany(List<int> deviceIds, int companyId)
        {
            List<IcuDevice> AssignedDevices = new List<IcuDevice>();

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var devices = _deviceService.GetByIds(deviceIds);

                        foreach (var eachDevice in devices)
                        {
                            var existCompanyId = eachDevice.CompanyId;

                            // Check whether the company is changed or not.
                            if (eachDevice.CompanyId == companyId)
                            {
                                continue;
                            }
                            else
                            {
                                AssignedDevices.Add(eachDevice);
                            }

                            // Assign device
                            eachDevice.CompanyId = companyId;

                            eachDevice.ActiveTzId = _unitOfWork.TimezoneRepository
                                .GetDefaultTzByCompanyId(Constants.Settings.DefaultPositionActiveTimezone, companyId).Id;
                            eachDevice.PassageTzId = _unitOfWork.TimezoneRepository
                                .GetDefaultTzByCompanyId(Constants.Settings.DefaultPositionPassageTimezone, companyId).Id;

                            var building = _unitOfWork.BuildingRepository
                                .GetByCompanyId(companyId).OrderBy(c => c.Id).FirstOrDefault();

                            if (building != null)
                            {
                                eachDevice.BuildingId = building.Id;
                            }
                            else
                            {
                                eachDevice.BuildingId = null;
                            }

                            _unitOfWork.IcuDeviceRepository.Update(eachDevice);

                            // If the device is already assigned to a company, save the unassign system logs to an existing company.
                            if (existCompanyId != null)
                            {
                                var contents = $"{ActionLogTypeResource.UnassignDoor} : {eachDevice.Name}";

                                _unitOfWork.SystemLogRepository.Add(eachDevice.Id, SystemLogType.DeviceSetting,
                                ActionLogType.Delete, contents, null, null, existCompanyId);

                                _unitOfWork.Save();
                            }
                        }

                        //AssignedDevices = devices;

                        //Save system log
                        var device = devices.First();
                        var content = $"{ActionLogTypeResource.AssignDoor}";
                        var deviceAddresses = devices.Select(c => c.DeviceAddress).ToList();
                        var contentDetails = $"{DeviceResource.lblDeviceCount} : {devices.Count()} \n"
                        + $"{DeviceResource.lblDeviceAddress}: {string.Join(", ", deviceAddresses)}";

                        _unitOfWork.SystemLogRepository.Add(device.Id, SystemLogType.DeviceSetting, ActionLogType.Add, 
                            content, contentDetails, deviceIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            foreach(var device in AssignedDevices)
            {
                //Assign to Full Access Group.
                _deviceService.AssignToFullAccessGroup(device.Id, companyId);

                // Instead of the RegisterConsumerService function, the Reinstall is executed.
                _deviceService.Reinstall(new List<IcuDevice> { device });
            }
            
        }

        /// <summary>
        /// Update logo
        /// </summary>
        /// <param name="base64String"></param>
        public void UpdateLogo(string base64String)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var company = _unitOfWork.CompanyRepository.GetById(_httpContext.User.GetCompanyId());
                        company.Logo = System.Text.Encoding.UTF8.GetBytes(base64String);
                        _unitOfWork.CompanyRepository.Update(company);
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Get logo by company
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public string GetCurrentLogo(Company company)
        {
            var base64Logo = "";
            if (company.Logo != null)
            {
                base64Logo = System.Text.Encoding.UTF8.GetString(company.Logo);
            }

            return base64Logo;
        }

        /// <summary>
        /// Get default infomation about company by Id
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public CompanyDataModel GetDefaultInfoById(int companyId)
        {
            var companyInfo = new CompanyDataModel
            {
                BuildingItems = _unitOfWork.BuildingRepository.GetMany(c => !c.IsDeleted && c.CompanyId == companyId)
                                        .Select(m => new SelectListItemModel { Id = m.Id, Name = m.Name }).ToList(),
                DefaultBuilding = _unitOfWork.BuildingRepository.GetDefaultByCompanyId(companyId).Id,

                ActiveTzItems = _unitOfWork.TimezoneRepository.GetMany(c => !c.IsDeleted && c.CompanyId == companyId)
                                        .Select(m => new SelectListItemModel { Id = m.Id, Name = m.Name }).ToList(),
                DefaultActiveTz = _unitOfWork.TimezoneRepository.GetDefaultTzByCompanyId(Constants.Tz24hPos, companyId).Id,

                PassiveTzItems = _unitOfWork.TimezoneRepository.GetMany(c => !c.IsDeleted && c.CompanyId == companyId)
                                        .Select(m => new SelectListItemModel { Id = m.Id, Name = m.Name }).ToList(),
                DefaultPassiveTz = _unitOfWork.TimezoneRepository.GetDefaultTzByCompanyId(Constants.TzNotUsePos, companyId).Id,
            };

            return companyInfo;
        }
        

        /// <summary>
        /// Get logo by company
        /// </summary>
        /// <param name="cocompanyIdmpany"></param>
        /// <returns></returns>
        public string GetCodeById(int companyId)
        {
            return _unitOfWork.CompanyRepository.GetCompanyCodeByCompanyId(companyId);
        }
        

        /// <summary>
        /// Delete company
        /// </summary>
        /// <param name="company"></param>
        public void Delete(Company company)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        company.IsDeleted = true;
                        _unitOfWork.CompanyRepository.Update(company);

                        //Save system log
                        var content = $"{CompanyResource.msgDelete}";
                        var contentsDetails = $"{CompanyResource.lblCompanyName} : {company.Name} (code : {company.Code})";
                        _unitOfWork.SystemLogRepository.Add(company.Id, SystemLogType.Company, ActionLogType.Delete, content, contentsDetails, null, company.Id);

                        _unitOfWork.Save();
                        transaction.Commit();

                        //Push company changed (account changed) notify
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a list of company
        /// </summary>
        /// <param name="companies"></param>
        public void DeleteRange(List<Company> companies)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var company in companies)
                        {
                            company.IsDeleted = true;

                            //Save system log
                            var content = $"{CompanyResource.msgDelete}";
                            var contentsDetails = $"{CompanyResource.lblCompanyName} : {company.Name} (code : {company.Code})";
                            _unitOfWork.SystemLogRepository.Add(company.Id, SystemLogType.Company, ActionLogType.Delete, content, contentsDetails, null, company.Id);

                            _unitOfWork.CompanyRepository.Update(company);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();

                        //Push company changed (account changed) notify
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Update company to the expiration
        /// </summary>
        /// <param name="companies"></param>
        /// <returns></returns>
        public void UpdateCompaniesToExpire(List<Company> companies)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //Update whole doors of each company to invalid as well.
                        foreach (var company in companies)
                        {
                            company.IsDeleted = true;
                            _unitOfWork.CompanyRepository.Update(company);

                            var icuDevices = _unitOfWork.IcuDeviceRepository.GetValidDoorsByCompany(company.Id);
                            foreach (var icuDevice in icuDevices)
                            {
                                icuDevice.Status = (short)Status.Invalid;
                                _unitOfWork.IcuDeviceRepository.Update(icuDevice);
                            }

                            _unitOfWork.Save();
                            transaction.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<CompanyListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.AppDbContext.Company
                //.Include(m => m.Account)
                .Where(c => !c.IsDeleted)
                .Select(m => new CompanyListModel
                {
                    Id = m.Id,
                    Code = m.Code,
                    Name = m.Name,
                    Remarks = m.Remarks,
                    //Admin = m.Account.FirstOrDefault(c => c.RootFlag && c.CompanyId == m.Id).Username,
                    //ExpiredDate = m.ExpiredTo,
                    Createdon = m.CreatedOn.ConvertToUserTimeZone(
                        _unitOfWork.AppDbContext.Account.FirstOrDefault(c => c.Id == _httpContext.User.GetAccountId() 
                                                                             && !m.IsDeleted).TimeZone),
                    //RootFlag = m.RootFlag
                });

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Code.ToLower().Contains(filter.ToLower())
                                       || x.Name.ToLower().Contains(filter.ToLower())
                                       /*|| x.Admin.ToLower().Contains(filter.ToLower())*/);
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.Company.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.Company[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber -1)  * pageSize).Take(pageSize);
            return data;
        }

        /// <summary>
        /// Get companies
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        public List<Company> Gets()
        {
            return _unitOfWork.CompanyRepository.GetCompanies();
        }

        /// <summary>
        /// Get company by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Company GetById(int? id)
        {
            return _unitOfWork.CompanyRepository.GetCompanyById(id);
        }

        /// <summary>
        /// Get expired companies by day
        /// </summary>
        /// <param name="numberOfDay"></param>
        /// <param name="bExactDay"></param>
        /// <returns></returns>
        public List<Company> GetExpiredCompaniesByDay(int numberOfDay, bool bExactDay)
        {
            return _unitOfWork.CompanyRepository.GetExpiredCompaniesByDay(numberOfDay, bExactDay);
        }

        /// <summary>
        /// Check if company code is exist
        /// </summary>
        /// <param name="companyCode"></param>
        /// <returns></returns>
        public bool IsExistCompanyCode(string companyCode)
        {
            return _unitOfWork.AppDbContext.Company.Any(m =>
                !m.IsDeleted && m.Code == companyCode);
        }

        /// <summary>
        /// Check if company account is exist
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool IsExistedCompanyAccount(int companyId, string userName)
        {
            var account = _unitOfWork.AccountRepository.GetRootAccountByCompany(companyId);
            if (account != null && _unitOfWork.AccountRepository.IsExist(account.Id, userName, companyId))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get list of company by ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<Company> GetByIds(List<int> ids)
        {
            return _unitOfWork.CompanyRepository.GetByIds(ids);
        }

        ///// <summary>
        ///// Send new company account email
        ///// </summary>
        ///// <param name="company"></param>
        ///// <param name="newPass"></param>
        //public void SendCompanyAccountMail(Company company, string newPass)
        //{
        //    var companyUrl = Helpers.GetLoginPath(_httpContext, company.Code);
        //    var account = _unitOfWork.AccountRepository.GetRootAccountByCompany(company.Id);
        //    if (account != null)
        //    {
        //        var thread = new Thread(delegate ()
        //        {
        //            var subject = MailContentResource.SubjectCompanyAccount;
        //            var body = string.Format(MailContentResource.BodyCompanyAccount
        //                , account.Username
        //                , companyUrl
        //                , companyUrl
        //                , account.Username
        //                , newPass);

        //            _mailService.SendMail(account.Username, null
        //                , subject
        //                , body);
        //        });
        //        thread.Start();
        //    }
        //}

        ///// <summary>
        ///// Send updated company account email
        ///// </summary>
        ///// <param name="company"></param>
        ///// <param name="newUsername"></param>
        ///// <param name="newContact"></param>
        //public void SendUpdatedCompanyAccountMail(Company company, string newUsername, string newContact)
        //{
        //    var companyUrl = Helpers.GetLoginPath(_httpContext, company.Code);
        //    var account = _unitOfWork.AccountRepository.GetRootAccountByCompany(company.Id);
        //    if (account != null)
        //    {
        //        var thread = new Thread(delegate ()
        //        {
        //            var subject = MailContentResource.SubjectCompanyUpdate;
        //            var body = string.Format(MailContentResource.BodyCompanyUpdate
        //                , account.Username
        //                , companyUrl
        //                , companyUrl
        //                , newUsername
        //                , newContact);

        //            _mailService.SendMail(account.Username, null
        //                , subject
        //                , body);
        //        });
        //        thread.Start();
        //    }
        //}

        ///// <summary>
        ///// Send email report to old account
        ///// </summary>
        ///// <param name="company"></param>
        ///// <param name="oldUsername"></param>
        //public void SendOldCompanyAccountMail(Company company, string oldUsername)
        //{
        //    if (oldUsername != null)
        //    {
        //        var companyUrl = Helpers.GetLoginPath(_httpContext, company.Code);
        //        var thread = new Thread(delegate ()
        //        {
        //            var subject = MailContentResource.SubjectCompanyOldAccount;
        //            var body = string.Format(MailContentResource.BodyCompanyOldAccount
        //                , oldUsername
        //                , oldUsername
        //                , oldUsername
        //                , companyUrl
        //                , companyUrl);

        //            _mailService.SendMail(oldUsername, null
        //                , subject
        //                , body);
        //        });
        //        thread.Start();
        //    }
        //}

        ///// <summary>
        ///// Send expired mail
        ///// </summary>
        ///// <param name="companies"></param>
        ///// <param name="numberOfExpiredDay"></param>
        //public void SendExpiredCompanyMail(List<Company> companies, int numberOfExpiredDay)
        //{
        //    var primeCompany = _unitOfWork.CompanyRepository.Get(c => c.RootFlag);
        //    var emails = companies.Select(c => c.Account.FirstOrDefault(a => a.RootFlag)?.Username)
        //        .ToList();
        //    if (emails.Any())
        //    {
        //        var thread = new Thread(delegate ()
        //        {
        //            var subject = string.Format(MailContentResource.SubjectCompanyExpired, numberOfExpiredDay);
        //            foreach (var email in emails)
        //            {
        //                var body = string.Format(MailContentResource.BodyCompanyExpired, email, numberOfExpiredDay,
        //                    primeCompany?.Contact, primeCompany?.Contact);
        //                _mailService.SendMail(email, null, subject, body);
        //            }
        //        });
        //        thread.Start();
        //    }
        //}

        /// <summary>
        /// Get root company
        /// </summary>
        /// <returns></returns>
        public Company GetRootCompany()
        {
            return _unitOfWork.CompanyRepository.GetRootCompany();
        }

        /// <summary>
        /// Check company is valid
        /// </summary>
        /// <param name="company"></param>
        /// <returns></returns>
        public bool IsValidCompany(Company company)
        {
            if (company == null)
            {
                return false;
            }

            if (company.RootFlag)
            {
                return true;
            }

            var result = !company.IsDeleted /*&&
                         DateTime.Now.Subtract(company.ExpiredFrom.Date).TotalSeconds >= 0
                         && company.ExpiredTo.Subtract(DateTime.Now.Date).TotalSeconds >= 0*/;
            return result;
        }

        /// <summary>
        /// Get company code
        /// </summary>
        /// <returns></returns>
        public string MakeCompanyCode()
        {
            return _unitOfWork.CompanyRepository.MakeCompanyCode();
        }

        /// <summary>
        /// Get list of company
        /// </summary>
        /// <returns></returns>
        public List<Company> GetCompanies()
        {
            return _unitOfWork.CompanyRepository.GetCompanies();
        }
        
        // Create account for company in rabbit message queue 
        private static async Task AddAccountCompanyMessageQueue(Company company, IConfiguration configuration)
        {
            var initial = new ManagementClient(
                configuration.GetSection("QueueConnectionSettingsApi:Host").Value,
                configuration.GetSection("QueueConnectionSettingsApi:UserName").Value, 
                configuration.GetSection("QueueConnectionSettingsApi:Password").Value);
            
            var vHost = await initial.GetVhostAsync(
                configuration.GetSection("QueueConnectionSettingsApi:VirtualHost").Value);
            var userInfor = new UserInfo(company.Code, Convert.ToBase64String(Encoding.UTF8.GetBytes(company.Code)));
            
            // Add tag permission for user
            userInfor.AddTag(Constants.RabbitMq.Permission);
            
            // Create user info
            var user = await initial.CreateUserAsync(userInfor);
            
            var topicPermissionInfo = new TopicPermissionInfo(user, vHost);
            
            topicPermissionInfo.SetExchangeType(Constants.RabbitMq.ExChangeType)
                .SetWrite(".*")
                .SetRead(string.Format(Constants.RabbitMq.TopicPermissionNormalUserList, company.Code));
            await initial.CreateTopicPermissionAsync(topicPermissionInfo).ConfigureAwait(true);
            

            var permissionInfo = new PermissionInfo(user, vHost);
            permissionInfo.SetConfigure(".*")
                .SetWrite(".*")
                .SetRead(".*");
            await initial.CreatePermissionAsync(permissionInfo).ConfigureAwait(true);
        }
        
        private static Random random = new Random();
        
        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="company">Company that contains existing information</param>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool CheckChange(Company company, CompanyModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                if (company.Name != model.Name)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, CompanyResource.lblCompanyName, company.Name, model.Name));
                }

                if (company.Remarks != model.Remarks)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, CompanyResource.lblRemarks, company.Remarks, model.Remarks));
                }
            }

            return changes.Count() > 0;
        }

        public void ResetAesKeyCompanies()
        {
            var companies = _unitOfWork.CompanyRepository.GetCompanies();
            foreach (var company in companies)
            {
                company.SecretCode = Helpers.GenerateCompanyKey();
                _unitOfWork.CompanyRepository.Update(company);
                _unitOfWork.Save();
                var devices = _deviceService.GetByCompanyId(company.Id);
                foreach (var device in devices)
                {
                    _deviceService.SendDeviceConfig(device, Constants.Protocol.UpdateDeviceConfig);
                }
                
            }
        }
        
        public void ResetAesKeyCompany(int companyId)
        {
            var company = _unitOfWork.CompanyRepository.GetById(companyId);
            company.SecretCode = Helpers.GenerateCompanyKey();
            _unitOfWork.CompanyRepository.Update(company);
            _unitOfWork.Save();
            var devices = _deviceService.GetByCompanyId(company.Id);
            foreach (var device in devices)
            {
                _deviceService.SendDeviceConfig(device, Constants.Protocol.UpdateDeviceConfig);
            }
        }
        
        public PlugIn GetPluginByCompany(int companyId)
        {
            var plugin = _unitOfWork.PlugInRepository.GetPlugInByCompany(companyId);
            var acceptedPlugin = _configuration.GetSection("DefaultPlugin")?.Get<List<string>>();
            var pluginPolicy = JsonConvert.DeserializeObject<Dictionary<string, bool>>(plugin.PlugIns);
            var pluginDescription = JsonConvert.DeserializeObject<Dictionary<string, string>>(plugin.PlugInsDescription);

            foreach (KeyValuePair<string, bool> keyPlug in pluginPolicy.ToList())
            {
                if (!acceptedPlugin.Contains(keyPlug.Key))
                {
                    pluginPolicy.Remove(keyPlug.Key);
                }
            }
            
            foreach (var keyPlug in pluginDescription.ToList())
            {
                if (!acceptedPlugin.Contains(keyPlug.Key))
                {
                    pluginDescription.Remove(keyPlug.Key);
                }
            }

            plugin.PlugIns = JsonConvert.SerializeObject(pluginPolicy);
            plugin.PlugInsDescription = JsonConvert.SerializeObject(pluginDescription);
            return plugin;
        }

        public int UpdatePluginByCompany(int companyId, PlugIns model)
        {
            var pluginId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var acceptedPlugin = _configuration.GetSection("DefaultPlugin")?.Get<List<string>>();
                        foreach (PropertyInfo pi in model.GetType().GetProperties())
                        {
                            Console.WriteLine(pi.Name);
                            if (!acceptedPlugin.Contains(pi.Name))
                            {
                                model.GetType().GetProperty(pi.Name)?.SetValue(model, false);
                            }

                        }
                        
                        var plugIn = _unitOfWork.PlugInRepository.GetPlugInByCompany(companyId);
                        var json = JsonConvert.SerializeObject(model);
                        plugIn.PlugIns = json;
                        _unitOfWork.PlugInRepository.Update(plugIn);
                        _unitOfWork.Save();
                        transaction.Commit();
                        pluginId = plugIn.Id;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            return pluginId;
        }

        public PlugIn GetPluginByCompanyAllowShowing(int companyId)
        {
            var plugin = _unitOfWork.PlugInRepository.GetPlugInByCompany(companyId);
            var pluginPolicy = JsonConvert.DeserializeObject<Dictionary<string, bool>>(plugin.PlugIns);
            foreach (var keyPlug in pluginPolicy.ToList())
            {
                if (keyPlug.Value == false)
                {
                    pluginPolicy.Remove(keyPlug.Key);
                }
            }
            plugin.PlugIns = JsonConvert.SerializeObject(pluginPolicy);
            return plugin;
        }
    }
}