﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Account;
using DeMasterProCloud.DataModel.Category;
using DeMasterProCloud.DataModel.Department;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MoreLinq.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Army user service interface
    /// </summary>
    public interface IUserArmyService
    {
        List<UserArmyListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, out List<HeaderData> userHeader, bool isValid = true);

        List<HeaderData> GetUserHeaderData();

        void Add(int userId, UserArmyModel userModel);
        void InitData(UserArmyDataModel userArmyModel);
        void Update(UserArmyModel userArmyModel);
        void Delete(int userId);
        void DeleteRange(List<int> userIds);
        
    }

    public class UserArmyService : IUserArmyService
    {
        private readonly IUserService _userService;
        private readonly IAccountService _accountService;
        private readonly ICategoryService _categoryService;
        private readonly IDepartmentService _departmentService;
        private readonly IAccessGroupService _accessGroupService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;

        public UserArmyService(IUserService userService, 
                            IAccountService accountService, 
                            ICategoryService categoryService, 
                            IDepartmentService departmentService, 
                            IAccessGroupService accessGroupService,
            IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor, ILogger<DepartmentService> logger)
        {
            _userService = userService;
            _accountService = accountService;
            _categoryService = categoryService;
            _departmentService = departmentService;
            _accessGroupService = accessGroupService;
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
        }

        /// <summary>
        /// Add a userArmy information
        /// </summary>
        /// <param name="userArmyModel"></param>
        public void Add(int userId, UserArmyModel userArmyModel)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();

                        UserArmy userArmy = Mapper.Map<UserArmy>(userArmyModel);
                        userArmy.UserId = userId;

                        _unitOfWork.UserArmyRepository.Add(userArmy);

                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Get army user data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<UserArmyListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, out List<HeaderData> userHeader, bool isValid = true)
        {
            var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered, out userHeader, isValid);

            var result = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return result.ToList();
        }

        /// <summary>
        /// Filter and order army user data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<UserArmyListModel> FilterDataWithOrder(string filter, int sortColumn, string sortDirection,
            out int totalRecords, out int recordsFiltered, out List<HeaderData> userHeader, bool isValid)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = isValid
                ? _unitOfWork.UserRepository.GetByCompanyId(companyId)
                : _unitOfWork.UserRepository.GetUserByCompany(companyId);

            var result = data.Include(m => m.UserArmy).Where(m => m.UserArmy.Select(c => c.UserId).Contains(m.Id))
                .Select(m => new UserArmyListModel()
                {
                    Id = m.Id,
                    UserCode = m.UserCode,
                    FirstName = m.FirstName,
                    LastName = m.LastName,
                    DepartmentName = m.Department.DepartName,
                    MilitaryNo = m.UserArmy.Select(c => c.MilitaryNumber).FirstOrDefault(),
                    ExpiredDate = m.ExpiredDate.ToSettingDateString(),
                    AccessGroupName = m.AccessGroup.Name
                });

            totalRecords = result.Count();

            var resultList = result.ToList();

            // Add category data to each person
            foreach (var userArmy in resultList)
            {
                var options = _categoryService.GetCategoryOptionsByUserId(userArmy.Id);
                List<UserCategoryDataModel> userCategory = new List<UserCategoryDataModel>();

                foreach (var option in options)
                {
                    var userCategoryData = new UserCategoryDataModel()
                    {
                        Category = { Id = option.CategoryId,
                                    Name = _categoryService.GetCategoryByIdAndCompanyId(option.CategoryId, companyId).Name },
                        OptionName = option.Name
                    };
                    userCategory.Add(userCategoryData);
                }
                userArmy.CategoryOptions = userCategory;
            }

            // Filter data
            List<UserArmyListModel> userArmyList = new List<UserArmyListModel>();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();

                foreach (PropertyInfo property in resultList.First().GetType().GetProperties())
                {
                    if (property.PropertyType.Equals(typeof(System.String)))
                    {
                        var filteredResult = resultList.Where(x =>
                            !string.IsNullOrEmpty(property.GetValue(x, null)?.ToString()) &&
                            property.GetValue(x, null).ToString().ToLower().Contains(filter)).ToList();

                        userArmyList = userArmyList.Union(filteredResult).ToList();
                    }
                    else if (property.PropertyType.Equals(typeof(List<UserCategoryDataModel>)))
                    {
                        var filteredResult = resultList.Where(x => x.CategoryOptions
                                                        .Where(c => c.OptionName.ToLower().Contains(filter)).Count() > 0).ToList();

                        userArmyList = userArmyList.Union(filteredResult).ToList();
                    }
                }
            }
            else
            {
                userArmyList = resultList;
            }

            recordsFiltered = userArmyList.Count();

            // Get header data
            userHeader = GetUserHeaderData();

            // Sort data
            string columnName = userHeader.Where(m => m.HeaderId == sortColumn).Select(m => m.HeaderVariable).FirstOrDefault();

            if (columnName != null && userArmyList != null && userArmyList.Count > 0)
            {
                foreach (PropertyInfo property in userArmyList.First().GetType().GetProperties())
                {
                    if (property.Name.Equals(columnName))
                    {
                        if (sortDirection.Equals("src"))
                            userArmyList = userArmyList.OrderBy(m => property.GetValue(m, null)).ToList();
                        else
                            userArmyList = userArmyList.OrderByDescending(m => property.GetValue(m, null)).ToList();

                        break;
                    }
                    else
                    {
                        int categoryId = 0;
                        try { categoryId = int.Parse(columnName); } catch (Exception) { continue; }

                        if (sortDirection.Equals("src"))
                            userArmyList = userArmyList.OrderBy(m => m.CategoryOptions.Where(c => c.Category.Id == categoryId)
                                                                  .Select(c => c.OptionName).FirstOrDefault()).ToList();
                        else
                            userArmyList = userArmyList.OrderByDescending(m => m.CategoryOptions.Where(c => c.Category.Id == categoryId)
                                                                      .Select(c => c.OptionName).FirstOrDefault()).ToList();
                        break;
                    }
                }

            }

            // Convert time according to accountTimezone
            var accountTimezone = _userService.GetAccountById(_httpContext.User.GetAccountId()).TimeZone;

            foreach (var i in userArmyList)
            {
                if (i.ExpiredDate != null)
                {
                    DateTime newSelectedDate = DateTime.Parse(i.ExpiredDate);
                    i.ExpiredDate = Helpers.ConvertToUserTimeZoneReturnDate(newSelectedDate, accountTimezone).ToSettingDateTimeString();
                }

            }

            return userArmyList;
        }


        /// <summary>
        /// Initialize the userArmyDataModel
        /// </summary>
        /// <param name="userArmyModel"></param>
        public void InitData(UserArmyDataModel userArmyModel)
        {
            var companyId = _httpContext.User.GetCompanyId();

            userArmyModel.Genders = EnumHelper.ToEnumList<SexType>();

            userArmyModel.ListCardStatus = EnumHelper.ToEnumList<CardStatus>();
            userArmyModel.ListPassType = EnumHelper.ToEnumList<PassType>();
            if (userArmyModel.CardType == 0 && userArmyModel.ListPassType.Any())
            {
                userArmyModel.CardType = (short)PassType.PassCard;
            }
            userArmyModel.ListWorkType = EnumHelper.ToEnumList<Army_WorkType>();
            userArmyModel.ListUserStatus = EnumHelper.ToEnumList<UserStatus>();
            userArmyModel.ListPermissionType = EnumHelper.ToEnumList<PermissionType>();
            userArmyModel.ListAccessGroupType = EnumHelper.ToEnumList<AccessGroupType>();

            if (userArmyModel.DepartmentId == null)
            {
                var defaultDepartment = _unitOfWork.DepartmentRepository
                    .GetDefautDepartmentByCompanyId(companyId);
                if (defaultDepartment != null)
                {
                    userArmyModel.DepartmentId = defaultDepartment.Id;
                }
            }

            userArmyModel.Departments = _departmentService
                .GetByCompanyId(companyId).Select(Mapper.Map<DepartmentModelForUser>).ToList();

            if (userArmyModel.Departments.Count() == 1)
            {
                // If there is only one department(maybe default), the department is automatically set up when manager add users.
                userArmyModel.DepartmentId = userArmyModel.Departments.First().Id;
            }

            var accessGroups = _accessGroupService.GetListAccessGroupsExceptForVisitor()
                .Select(Mapper.Map<AccessGroupModelForUser>).ToList();

            var accessGroupDefault =
                _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(companyId);

            if (userArmyModel.AccessGroupId == 0 && accessGroupDefault != null)
            {
                userArmyModel.AccessGroupId = accessGroupDefault.Id;
                var index = accessGroups.FindIndex(x => x.Id == accessGroupDefault.Id);
                var item = accessGroups[index];
                accessGroups[index] = accessGroups[0];
                accessGroups[0] = item;
            }

            userArmyModel.AccessGroups = accessGroups;

            if (userArmyModel.Id != 0)
            {
                var cards = _unitOfWork.CardRepository.GetByUserId(companyId, userArmyModel.Id);

                var cardModelList = new List<CardModel>();
                foreach (var card in cards)
                {
                    var cardModel = Mapper.Map<CardModel>(card);
                    cardModelList.Add(cardModel);
                }

                userArmyModel.CardList = cardModelList;

                // Add army information
                var userArmyData = _unitOfWork.UserArmyRepository.GetArmyByUserId(userArmyModel.Id);

                if(userArmyData != null && userArmyData.Count() > 0)
                {
                    UserArmy userArmy = _unitOfWork.UserArmyRepository.GetArmyByUserId(userArmyModel.Id).First();

                    userArmyModel.MilitaryNumber = userArmy.MilitaryNumber;
                    userArmyModel.BackGroundCheckNumber = userArmy.BackGroundCheckNumber;
                    userArmyModel.EnlistmentDate = userArmy.EnlistmentDate.ToSettingDateString();
                    userArmyModel.SecretIssuanceBasis = userArmy.SecretIssuanceBasis;
                }
            }
            else//Id is 0
            {
                var users = _unitOfWork.UserRepository.GetByCompanyId(companyId);
                var userCode = 1;
                if (users.Count() > 0)
                {
                    userCode = _unitOfWork.UserRepository.GetNewUserCode(companyId);
                }
                userArmyModel.UserCode = String.Format("{0:000000}", userCode);

                userArmyModel.Gender = true;
            }

            userArmyModel.Categories = _categoryService.GetCategoryHierarchy();
            userArmyModel.CategoryOptions = _categoryService.GetOptionHierarchy();
            userArmyModel.CategoryOptionIds = _categoryService.GetCategoryOptionIdsByUserId(userArmyModel.Id);
        }


        public List<HeaderData> GetUserHeaderData()
        {
            List<HeaderData> headers = new List<HeaderData>();

            foreach(var column in ColumnDefines.UserArmyHeader)
            {
                foreach (var element in Enum.GetValues(typeof(UserHeaderColumn)))
                {
                    if (column.Equals(element.GetName()))
                    {
                        HeaderData header = new HeaderData()
                        {
                            HeaderId = (int)element,
                            HeaderName = element.GetDescription(),
                            HeaderVariable = element.GetName(),
                            IsCategory = false
                        };

                        headers.Add(header);
                    }
                }
            }

            // Get category header data
            var companyId = _httpContext.User.GetCompanyId();

            var categoryHeaders = _categoryService.GetCategoryHeadersByCompanyId(companyId, headers.Count());

            headers = headers.Concat(categoryHeaders).ToList();

            return headers;
        }

        /// <summary>
        /// Update UserArmy
        /// </summary>
        /// <param name="userArmyModel"></param>
        /// <returns></returns>
        public void Update(UserArmyModel userArmyModel)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var userArmyData = _unitOfWork.UserArmyRepository.GetArmyByUserId(userArmyModel.Id);

                        if(userArmyData != null && userArmyData.Count() > 0)
                        {
                            UserArmy userArmy = userArmyData.First();

                            Mapper.Map(userArmyModel, userArmy);

                            _unitOfWork.UserArmyRepository.Update(userArmy);
                        }
                        else
                        {
                            UserArmy userArmy = Mapper.Map<UserArmy>(userArmyModel);

                            _unitOfWork.UserArmyRepository.Add(userArmy);
                        }
                        
                        ////Save system log
                        //var content = $"{ActionLogTypeResource.Update} : {user.FirstName + " " + user.LastName} ({user.UserCode})";

                        //var contentDetails = $"{UserResource.lblUpdate}: {string.Join("\n", changes)}";
                        //_unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Update,
                        //    content, contentDetails, null, user.CompanyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a UserArmy
        /// </summary>
        /// <param name="userId"> identifier of user </param>
        /// <returns></returns>
        public void Delete(int userId)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //UserArmy userArmy = new UserArmy();

                        var userArmy = _unitOfWork.UserArmyRepository.GetArmyByUserId(userId);

                        if(userArmy.Count() > 0 && userArmy != null)
                        {
                            _unitOfWork.UserArmyRepository.Delete(userArmy.First());
                            _unitOfWork.Save();

                        }

                        ////Save system log
                        //var content = $"{ActionLogTypeResource.Update} : {user.FirstName + " " + user.LastName} ({user.UserCode})";

                        //var contentDetails = $"{UserResource.lblUpdate}: {string.Join("\n", changes)}";
                        //_unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Update,
                        //    content, contentDetails, null, user.CompanyId);

                        //_unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete UserArmys
        /// </summary>
        /// <param name="userIds"> List of user identifier </param>
        /// <returns></returns>
        public void DeleteRange(List<int> userIds)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var userArmy = _unitOfWork.UserArmyRepository.GetArmysByUserIds(userIds);

                        _unitOfWork.UserArmyRepository.DeleteRange(userArmy);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }
    }
}