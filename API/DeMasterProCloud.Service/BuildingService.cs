﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DeMasterProCloud.DataModel.Building;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// Building service interface
    /// </summary>
    public interface IBuildingService : IPaginationService<BuildingListModel>
    {

        int Add(BuildingModel model);
        void AddDoors(Building building, List<int> doorIds);
        IQueryable<BuildingDoorModel> GetPaginatedDoors(int id, string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        IQueryable<BuildingUnAssignDoorModel> GetPaginatedUnAssignDoors(int id, string filter, int pageNumber,
            int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        void Delete(int id);
        void DeleteRange(List<Building> buildings);
        void DeleteDoors(int id, List<int> doorIds);
        void Update(int id, BuildingModel model);
        bool IsExistedBuildingName(int id, string name);
        Building GetById(int id);
        List<Building> GetByIds(List<int> ids);

    }

    public class BuildingService : IBuildingService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly HttpContext _httpContext;
        private readonly ILogger _logger;
        private readonly IDeviceService _deviceService;

        public BuildingService(IUnitOfWork unitOfWork, IHttpContextAccessor httpContextAccessor, ILogger<BuildingService> logger, IDeviceService deviceService)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContextAccessor.HttpContext;
            _logger = logger;
            _deviceService = deviceService;
        }


        /// <summary>
        /// Add Building
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Add(BuildingModel model)
        {
            int buildingId = 0;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var building = Mapper.Map<Building>(model);
                        building.CompanyId = _httpContext.User.GetCompanyId();
                        if (String.IsNullOrEmpty(model.TimeZone))
                        {
                            building.TimeZone = Helpers.GetLocalTimeZone();
                        }
                        _unitOfWork.BuildingRepository.Add(building);
                        _unitOfWork.Save();

                        //Save system log
                        var content = BuildingResource.msgAdd;
                        List<string> details = new List<string>
                        {
                            $"{BuildingResource.lblBuildingName} : {building.Name}",
                            $"{BuildingResource.lblAddress} : {building.Address}",
                            $"{BuildingResource.lblCity} : {building.City}",
                            $"{BuildingResource.lblCountry} : {building.Country}",
                            $"{BuildingResource.lblPostalCode} : {building.PostalCode}"
                        };
                        var contentsDetails = string.Join("\n", details);

                        _unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.Add,
                            content, contentsDetails, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                        buildingId = building.Id;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        buildingId = 0;
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
            return buildingId;
        }

        /// <summary>
        /// Add list door to building
        /// </summary>
        /// <param name="building"></param>
        /// <param name="doorIds"></param>
        public void AddDoors(Building building, List<int> doorIds)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        //is doorIds exists
                        //is id(buildingId) exists
                        var companyId = _httpContext.User.GetCompanyId();
                        var icuDevices = _unitOfWork.IcuDeviceRepository.GetByIdsAndCompany(doorIds, companyId);
                        string groupMsgId = "";
                        List<string> doorNames = new List<string>();

                        foreach (var icuDevice in icuDevices)
                        {
                            icuDevice.BuildingId = building.Id;
                            _unitOfWork.IcuDeviceRepository.Update(icuDevice);
                            _unitOfWork.Save();

                            groupMsgId = Guid.NewGuid().ToString();
                            var msgId = _deviceService.SendDeviceInstruction(icuDevice, Constants.CommandType.SetTime, groupMsgId, groupIndex: 0, groupLength: 2);

                            doorNames.Add(icuDevice.Name);

                            // Send Device Info
                            _deviceService.SendDeviceInfo(icuDevice.DeviceAddress, groupMsgId: groupMsgId, groupIndex: 1, groupLength: 2);
                        }

                        //Save system log
                        var buildingName = building.Name;
                        var assignedDoorIds = icuDevices.Select(c => c.Id).ToList();
                        var content = string.Format(BuildingResource.msgAssignDoors, buildingName);
                        var contentDetails = $"{DeviceResource.lblDeviceCount} : {assignedDoorIds.Count}\n" +
                                            $"{BuildingResource.lblAssignDoor}: {string.Join(", ", doorNames)}";

                        _unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.AssignDoor, 
                            content, contentDetails, assignedDoorIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Update building
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        public void Update(int id, BuildingModel model)
        {
            //var isSuccess = true;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var building = _unitOfWork.BuildingRepository.GetById(id);
                        var currentBuildingName = building.Name;
                        if (String.IsNullOrEmpty(model.TimeZone))
                        {
                            building.TimeZone = Helpers.GetLocalTimeZone();
                        }
                        var current = building.TimeZone;

                        List<string> changes = new List<string>();

                        var isChange = CheckChange(building, model, ref changes);

                        Mapper.Map(model, building);
                        _unitOfWork.BuildingRepository.Update(building);

                        if (isChange)
                        {
                            // Save the system log type as update.
                            var content = $"{BuildingResource.msgUpdate}\n{BuildingResource.lblBuildingName} : {currentBuildingName}";
                            var contentsDetails = string.Join("\n", changes);
                            _unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.Update,
                                content, contentsDetails, null, _httpContext.User.GetCompanyId());
                        }

                        _unitOfWork.Save();

                        if (model.TimeZone != current)
                        {
                            var devices = _unitOfWork.AppDbContext.IcuDevice
                                .Where(m => m.BuildingId == id && !m.IsDeleted).ToList();
                            if (devices.Any())
                            {
                                string groupMsgId = "";
                                foreach (var device in devices)
                                {
                                    groupMsgId = Guid.NewGuid().ToString();
                                    _deviceService.SendDeviceInstruction(device, Constants.CommandType.SetTime, groupMsgId);
                                }
                            }

                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
        }


        /// <summary>
        /// Delete building
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void Delete(int id)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        var icuDevices = _unitOfWork.IcuDeviceRepository.GetDoorsByBuildingId(companyId, id)
                            .ToList();
                        foreach (var icuDevice in icuDevices)
                        {
                            icuDevice.BuildingId = Constants.DefaultBuildingId;
                            _unitOfWork.IcuDeviceRepository.Update(icuDevice);
                        }

                        var building = _unitOfWork.BuildingRepository.GetById(id);
                        building.IsDeleted = true;
                        _unitOfWork.BuildingRepository.Update(building);

                        //Save system log
                        var content = BuildingResource.msgDelete;
                        var contentsDetails = $"{BuildingResource.lblBuildingName} : {building.Name}";

                        _unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.Delete,
                            content, contentsDetails, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
        }

        /// <summary>
        /// Delete a list buildings
        /// </summary>
        /// <param name="buildings"></param>
        public void DeleteRange(List<Building> buildings)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var building in buildings)
                        {
                            var companyId = _httpContext.User.GetCompanyId();
                            var icuDevices = _unitOfWork.IcuDeviceRepository.GetDoorsByBuildingId(companyId, building.Id)
                                .ToList();
                            foreach (var icuDevice in icuDevices)
                            {
                                icuDevice.BuildingId = Constants.DefaultBuildingId;
                                _unitOfWork.IcuDeviceRepository.Update(icuDevice);
                            }
                            building.IsDeleted = true;
                            _unitOfWork.BuildingRepository.Update(building);
                        }


                        if (buildings.Count == 1)
                        {
                            //Save system log
                            var building = buildings.First();
                            var content = BuildingResource.msgDelete;
                            var contentsDetails = $"{BuildingResource.lblBuildingName} : {building.Name}";

                            _unitOfWork.SystemLogRepository.Add(building.Id, SystemLogType.Building, ActionLogType.Delete,
                                content, contentsDetails, null, _httpContext.User.GetCompanyId());
                        }
                        else
                        {
                            //Save system log
                            var buildingIds = buildings.Select(c => c.Id).ToList();
                            var buildingNames = buildings.Select(c => c.Name).ToList();
                            var content = string.Format(ActionLogTypeResource.DeleteMultipleType, BuildingResource.lblBuilding);
                            var contentDetails = $"{BuildingResource.lblBuildingName}: {string.Join(", ", buildingNames)}";

                            _unitOfWork.SystemLogRepository.Add(buildingIds.First(), SystemLogType.Building, ActionLogType.DeleteMultiple, 
                                content, contentDetails, buildingIds, _httpContext.User.GetCompanyId());
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete buildingId a door
        /// </summary>
        /// <param name="buildingId"></param>
        /// <param name="doorIds"></param>
        /// <returns></returns>
        public void DeleteDoors(int buildingId, List<int> doorIds)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        //GetAll doors by building
                        var icuDevices = _unitOfWork.IcuDeviceRepository.GetByIdsAndBuildingAndCompany(doorIds, buildingId, companyId);
                        List<string> doorNames = new List<string>();

                        foreach (var icuDevice in icuDevices)
                        {
                            icuDevice.BuildingId = 1;
                            doorNames.Add(icuDevice.Name);

                            _unitOfWork.IcuDeviceRepository.Update(icuDevice);
                            _unitOfWork.Save();
                        }

                        //Save system log
                        var buildingName = _unitOfWork.BuildingRepository.GetById(buildingId);
                        var unAssignedDoorIds = icuDevices.Select(c => c.Id).ToList();

                        var content = string.Format(BuildingResource.msgUnAssignDoors, buildingName);
                        var contentDetails = $"{DeviceResource.lblDeviceCount} : {unAssignedDoorIds.Count}\n" +
                                            $"{BuildingResource.lblUnAssignDoor}: {string.Join(", ", doorNames)}";

                        _unitOfWork.SystemLogRepository.Add(buildingId, SystemLogType.Building, ActionLogType.UnassignDoor, 
                            content, contentDetails, unAssignedDoorIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                    }
                }
            });
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<BuildingListModel> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.AppDbContext.Building
                .Where(m => !m.IsDeleted && m.CompanyId == companyId);
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.Name.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.BuildingList.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.BuildingList[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.AsEnumerable<Building>().Select(Mapper.Map<BuildingListModel>).AsQueryable();
        }


        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<BuildingDoorModel> GetPaginatedDoors(int id, string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.IcuDeviceRepository.GetDoorsByBuildingId(companyId, id);
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.DeviceAddress.ToLower().Contains(filter.ToLower()) ||
                    //x.Building.Name.ToLower().Contains(filter.ToLower()) ||
                    ((DeviceType)x.DeviceType).GetDescription().ToLower().Contains(filter.ToLower()) ||
                    x.ActiveTz.Name.ToLower().Contains(filter.ToLower()) ||
                    x.PassageTz.Name.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.BuildingDoorList.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.BuildingDoorList[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.AsEnumerable<IcuDevice>().Select(Mapper.Map<BuildingDoorModel>).AsQueryable();
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="id"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<BuildingUnAssignDoorModel> GetPaginatedUnAssignDoors(int id, string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.IcuDeviceRepository.GetUnAssignDoorsByBuildingId(companyId, id);
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.DeviceAddress.ToLower().Contains(filter.ToLower()) ||
                    x.Building.Name.ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.BuildingUnAssignDoorList.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.BuildingUnAssignDoorList[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.AsEnumerable<IcuDevice>().Select(Mapper.Map<BuildingUnAssignDoorModel>).AsQueryable();
        }

        /// <summary>
        /// Check exist building
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool IsExistedBuildingName(int id, string name)
        {
            return _unitOfWork.AppDbContext.Building.Any(c => !c.IsDeleted && c.Id != id && c.Name == name);
        }

        /// <summary>
        /// Get building by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Building GetById(int id)
        {
            return _unitOfWork.BuildingRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(), id);
        }

        /// <summary>
        /// Get get building
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<Building> GetByIds(List<int> ids)
        {
            return _unitOfWork.BuildingRepository.GetByIdsAndCompanyId(_httpContext.User.GetCompanyId(), ids);
        }

        /// <summary>
        /// Get get building
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Building GetByNameAndCompanyId(string companyName, int companyId)
        {
            return _unitOfWork.BuildingRepository.GetByNameAndCompanyId(companyName, companyId);
        }


        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="building">Building that contains existing information</param>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool CheckChange(Building building, BuildingModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                if (building.Name != model.Name)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, BuildingResource.lblBuildingName, building.Name, model.Name));
                }

                if (building.Address != model.Address)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, BuildingResource.lblAddress, building.Address, model.Address));
                }

                if (building.City != model.City)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, BuildingResource.lblCity, building.City, model.City));
                }

                if (building.Country != model.Country)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, BuildingResource.lblCountry, building.Country, model.Country));
                }

                if (building.PostalCode != model.PostalCode)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, BuildingResource.lblPostalCode, building.PostalCode, model.PostalCode));
                }
            }

            return changes.Count() > 0;
        }

    }
}