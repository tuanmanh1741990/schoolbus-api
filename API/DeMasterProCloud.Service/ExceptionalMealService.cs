﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.ExceptionalMeal;
using DeMasterProCloud.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.Service
{
    public interface IExceptionalMealService
    {
        ReponseStatus AddExceptionalMeal(AddExceptionalMealModel model);
        ReponseStatus UpdateExceptionalMeal(int id, AddExceptionalMealModel model);
        ReponseStatus DeleteExceptionalMeal(int id);
        List<ExceptionalMealModel> GetListExceptionalMeal(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        ExceptionalMealModel GetExceptionalMeal(int Id);
    }
    public class ExceptionalMealService : IExceptionalMealService
    {
        private readonly IUnitOfWork _unitOfWork;
        DeMasterProCloud.DataAccess.Models.ExceptionalMeal obj = new DataAccess.Models.ExceptionalMeal();
        ReponseStatus res = new ReponseStatus();
        public ExceptionalMealService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ReponseStatus AddExceptionalMeal(AddExceptionalMealModel model)
        {
            try
            {
                obj.Start = model.start; 
                obj.End = model.end; 
                obj.Price = model.price;
                obj.MealSettingId = model.mealSettingId;
                var add = _unitOfWork.ExceptionalMealRepository.AddExceptionalMeal(obj);
                if (add.statusCode == false)
                {
                    res.message = Constants.CornerSetting.AddFailed;
                    res.statusCode = false;
                    return res;
                }

                if (add.statusCode == true && add.message == null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = add.data;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;
        }

        public ReponseStatus UpdateExceptionalMeal(int id, AddExceptionalMealModel model)
        {

            try
            {
                obj.Start = model.start;
                obj.End = model.end;
                obj.Price = model.price;
                obj.Id = id;
                var update = _unitOfWork.ExceptionalMealRepository.UpdateExceptionalMeal(obj);
                if (update.statusCode == false)
                {
                    res.message = Constants.CornerSetting.UpdateFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.UpdateFailed;
                res.statusCode = false;
            }
            return res;

        }

        public ReponseStatus DeleteExceptionalMeal(int id)
        {

            try
            {
                var delete = _unitOfWork.ExceptionalMealRepository.DeleteExceptionalMeal(id);
                if (delete.statusCode == false)
                {
                    res.message = Constants.CornerSetting.DeleteFailed;
                    res.statusCode = false;
                    return res;
                }
                res.message = Constants.CornerSetting.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.DeleteFailed;
                res.statusCode = false;
            }
            return res;

        }

        public List<ExceptionalMealModel> GetListExceptionalMeal(string filter, int pageNumber, int pageSize, int sortColumn,
           string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var listExceptionalMeal = _unitOfWork.ExceptionalMealRepository.GetListExceptionalMeal(filter, pageNumber, pageSize, sortColumn,
             sortDirection, out totalRecords, out recordsFiltered);

            return listExceptionalMeal;
        }

        public ExceptionalMealModel GetExceptionalMeal(int Id)
        {
            var data = _unitOfWork.ExceptionalMealRepository.GetExceptionalMeal(Id);
            return data;
        }
        public ExceptionalMealModel ListExceptionalMealByDateTime(int companyId, int icuId, string accessTime)
        {
            var data = _unitOfWork.ExceptionalMealRepository.ListExceptionalMealByDateTime(companyId, icuId, accessTime);
            return data;
        }
    }
}
