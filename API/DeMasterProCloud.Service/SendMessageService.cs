﻿using System;
using System.Linq;
using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;

namespace DeMasterProCloud.Service
{
    public interface ISendMessageService
    {
        void SendConnectionStatus(IcuDevice device, string doorStatus = "");
        void SendDeviceInfo(string deviceAddress);
        void SendResponseNotification(string msgId);
    }

    public class SendMessageService : ISendMessageService
    {
        private readonly IQueueService _queueService;
        
        public SendMessageService(IQueueService queueService)
        {
            _queueService = queueService;
        }

        /// <summary>
        /// Send connection status
        /// </summary>
        /// <param name="device"></param>
        /// <param name="doorStatus"></param>
        public void SendConnectionStatus(IcuDevice device, string doorStatus = "")
        {
            var data = Mapper.Map<IcuDevice, DeviceStatusDetail>(device);

            if (doorStatus == "")
                //data.DoorStatus = device.DoorStatus;
                data.DoorStatus = null;
            else
                data.DoorStatus = doorStatus;

            var protocolData = new DeviceStatusProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.ConnectionStatus,
                Data = data
            };

            var message = protocolData.ToString();
            var routingKey = Constants.RabbitMq.DeviceStatus;

            // Send to admin user
            _queueService.Publish(routingKey, message);

            var companyCode = device.Company != null ? device.Company.Code : "";

            if(companyCode != "")
            {
                // Send to company user
                protocolData.MsgId = Guid.NewGuid().ToString();
                routingKey = $"{Constants.RabbitMq.DeviceStatus}.{companyCode}";
                _queueService.Publish(routingKey, message);
            }

            //SendConnectionStatusForCompany(device, doorStatus);
        }

        /// <summary>
        /// Send connection status for admin 
        /// </summary>
        /// <param name="device"></param>
        /// <param name="doorStatus"></param>
        public void SendConnectionStatusForCompany(IcuDevice device, string doorStatus = "")
        {
            var data = Mapper.Map<IcuDevice, DeviceStatusDetail>(device);

            if (doorStatus == "")
                data.DoorStatus = device.DoorStatus;
            else
                data.DoorStatus = doorStatus;
            var protocolData = new DeviceStatusProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.ConnectionStatus,
                Data = data
            };

            var companyCode = device.Company != null ? device.Company.Code : "";

            var message = protocolData.ToString();
            var routingKey = $"{Constants.RabbitMq.DeviceStatus}.{companyCode}";
            _queueService.Publish(routingKey, message);
        }

        /// <summary>
        /// Publish a message to get device info
        /// <param name="deviceAddress"></param>
        /// </summary>
        public void SendDeviceInfo(string deviceAddress)
        {
            var deviceInfo = new DeviceInfoProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = Constants.Protocol.LoadDeviceInfo,
                Data = new DeviceInfoProtocolDataHeader()
            };
            var message = deviceInfo.ToString();
            var topic = Constants.RabbitMq.DeviceInfoTopic + "." + deviceAddress;

            _queueService.Publish(topic, message);
        }

        //TODO SendResponseNotification위치 변경
        /// <summary>
        /// Send response check to fronend
        /// </summary>
        public void SendResponseNotification(string msgId)
        {
            
            if (ApplicationVariables.SendingMessages.ContainsKey(msgId))
            {
                var notificationService = new NotificationService(_queueService);
                notificationService.SendMessage(Constants.MessageType.Error,
                    Constants.NotificationType.SendDeviceInstructionError, "",
                    string.Format("Command Timeout. Device command is not published to device)"), "0");
                ApplicationVariables.SendingMessages.TryRemove(msgId, out _);
            }
        }



    }
}

