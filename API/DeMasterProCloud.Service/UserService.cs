﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Repository;
using System.Linq.Dynamic.Core;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using DeMasterProCloud.Common.Resources;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Bogus;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.Department;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Drawing.Imaging;
using OfficeOpenXml;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using DeMasterProCloud.Common.Infrastructure.Exceptions;
using System.Threading;
using DeMasterProCloud.DataModel.Device;
using Microsoft.EntityFrameworkCore.Internal;
using DeMasterProCloud.DataModel.Account;
using QRCoder;
using System.Net.Mail;
using System.Text.RegularExpressions;
using DeMasterProCloud.DataModel.PlugIn;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// User Interface 
    /// </summary>
    public interface IUserService
    {
        byte[] Export(string type, string filter, int sortColumn, string sortDirection, out int totalRecords, out int recordsFiltered);
        byte[] ExportAccessibleDoors(User user, string type, string search, out int totalRecords, out int recordsFiltered, int sortColumn = 0, string sortDirection = "desc");

        IQueryable<User> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, bool isValid = true, int departmentId = 0);
        List<AccessibleDoorModel> GetPaginatedAccessibleDoors(User user, string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);
        bool CheckData();
        void InitData(UserDataModel userModel);
        int Add(UserModel userModel);
        User GetById(int id);
        User GetByIdAndCompany(int id, int companyId);
        void Update(UserModel userModel);

        Card AddIdentification(User user, CardModel cardModel);
        void Delete(User user);
        void DeleteRange(List<User> users);
        List<User> GetByIdsAndCompany(List<int> idArr, int companyId);
        ResultImported ImportFile(string type, IFormFile filePath);
        bool IsCardIdExist(UserModel model);
        bool IsCardIdExist(string cardId);

        //Card AddCardByUser(int userId, CardModel model);

        void DeleteCardByUser(User user, int cardId);

        void UpdateCardByUser(int userId, int cardId, CardModel model);
        bool IsKeyPadPasswordExist(int userId, string enctypedKeyPadPassword);
        void GenerateTestData(int numberOfUser);
        User GetByCardId(string cardId);
        Card GetCardByUser(int userId, int cardId);

        bool IsExistGetCardByUser(int userId, int cardId);
        List<CardModel> GetCardListByUserId(int userId);

        bool IsAccountExist(int id, string username, int? companyId = null);
        Account GetAccountByEmail(string username, int? companyId = null);
        Account GetAccountById(int userId);

        string GetDynamicQrCode(int userId, string qrId);

        int GetCardCount(int accessGroupId);

        void AssignUserToDefaultWorkingTime();
        //void SendVerifyAddAccountMailForUser(string email, string token);

        bool GetQrCodeBelongToUser(int userId);

        Account GetAccountByUserName(int companyId, string userName);
        User GetAccountAlreadyLinkToUser(int companyId, int accountId);


        Card GetPassCodeByUser(int userId, int companyId);


        bool IsEmailValid(string emailAddress);

        User GetUserByAccountId(int accountId);

        string ValidationDynamicQr(string dynamicQr);

        bool GetDeviceFromCompany(string rid);
        Card GetQrByUserId(int userId);
        bool IsDuplicatedEmail(int userId, string email);
        bool IsDuplicatedAccountCreated(string email);
        bool IsDuplicatedUserCode(int id, string userCode);

        void SendUpdateUsersToAllDoors(User user, bool isAddUser);
        void AutomationCreateQrCode(User user);
        User GetUserByUserId(int userId);
    }

    /// <summary>
    /// Service provider for user
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IMailService _mailService;
        private readonly IJwtHandler _jwtHandler;


        /// <summary>
        /// String array display in header sheet when export file
        /// </summary>
        /// 


        private readonly string[] _header =
        {
            UserResource.lblUserCode,
            UserResource.lblAccessGroup,
            UserResource.lblIsMasterCard,
            UserResource.lblCardStatus,
            UserResource.lblCardType,
            UserResource.lblCardId,
            UserResource.lblFirstName,
            UserResource.lblLastName,
            UserResource.lblSex,
            UserResource.lblEmail,
            UserResource.lblBirthday,
            //UserResource.lblIssuedDate,
            UserResource.lblEffectiveDate,
            UserResource.lblExpiredDate,
            UserResource.lblDepartmentName,
            UserResource.lblDepartmentNumber,
            UserResource.lblEmployeeNumber,
            UserResource.lblPosition,
            //UserResource.lblKeyPadPassword,
            UserResource.lblPostCode,
            UserResource.lblJob,
            UserResource.lblResponsibility,
            UserResource.lblCompanyPhone,
            UserResource.lblHomePhone,
            UserResource.lblAddress,
            UserResource.lblNationality,
            UserResource.lblCity,
            UserResource.lblRemarks,

        };
        //private string _loadUserFromIcu =
        //    "{0}" + $"_{DateTime.Now.ToString(Constants.DateTimeFormat.YyyyMMddhh, CultureInfo.InvariantCulture)}.csv";
        private readonly string[] _userSettingHeaders = {
            UserResource.lblCardId,
            UserResource.lblName,
            DepartmentResource.lblDepartment,
            UserResource.lblEmployeeNumber,
            UserResource.lblExpiredDate,
            UserResource.lblIssueCount,
            UserResource.lblIsMasterCard,
            UserResource.lblEffectiveDate,
            UserResource.lblCardStatus,
            UserResource.lblKeyPadPassword,
            TimezoneResource.lblTimezone
        };

        private readonly string[] _header1 =
        {
            UserResource.lblCardId,
            UserResource.lblExpiredDate,
            UserResource.lblDepartmentName
        };

        /// <summary>
        /// String array display in header sheet when export file
        /// </summary>
        private readonly string[] _headerForAccessibleDoor =
        {
            DeviceResource.lblIndex,
            DeviceResource.lblDoorName,
            DeviceResource.lblRID,
            DeviceResource.lblDoorActiveTimezone,
            DeviceResource.lblDoorPassageTimezone,
            DeviceResource.lblVerifyMode,
            DeviceResource.lblAntiPassback,
            DeviceResource.lblDeviceType,
            DeviceResource.lblMpr,
         };

        // Inject dependency
        private readonly IUnitOfWork _unitOfWork;

        private readonly HttpContext _httpContext;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IDepartmentService _departmentService;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly IAccessGroupService _accessGroupService;
        private readonly IDeviceService _deviceService;
        private readonly IAccountService _accountService;
        private readonly ICategoryService _categoryService;

        public UserService(IUnitOfWork unitOfWork, IHttpContextAccessor contextAccessor,
            IHostingEnvironment hostingEnvironment,
            IDepartmentService departmentService, IAccessGroupService accessGroupService, IDeviceService deviceService,
            IMailService mailService,
            IConfiguration configuration, ILogger<UserService> logger,
            IJwtHandler jwtHandler,
            IAccountService accountService,
            ICategoryService categoryService)
        {
            _unitOfWork = unitOfWork;
            _hostingEnvironment = hostingEnvironment;
            _httpContext = contextAccessor.HttpContext;
            _departmentService = departmentService;
            _accessGroupService = accessGroupService;
            _deviceService = deviceService;
            _configuration = configuration;
            _logger = logger;
            _mailService = mailService;
            _jwtHandler = jwtHandler;
            _accountService = accountService;
            _categoryService = categoryService;
        }

        /// <inheritdoc />
        public void InitData(UserDataModel userModel)
        {
            var companyId = _httpContext.User.GetCompanyId();

            userModel.Genders = EnumHelper.ToEnumList<SexType>();
            userModel.ListCardStatus = EnumHelper.ToEnumList<CardStatus>();
            userModel.ListPassType = EnumHelper.ToEnumList<PassType>();
            if (userModel.CardType == 0 && userModel.ListPassType.Any())
            {
                userModel.CardType = (short)PassType.PassCard;
            }
            userModel.ListWorkType = EnumHelper.ToEnumList<WorkType>();
            userModel.ListUserStatus = EnumHelper.ToEnumList<UserStatus>();
            userModel.ListPermissionType = EnumHelper.ToEnumList<PermissionType>();
            userModel.ListAccessGroupType = EnumHelper.ToEnumList<AccessGroupType>();

            if (userModel.DepartmentId == null)
            {
                var defaultDepartment = _unitOfWork.DepartmentRepository
                    .GetDefautDepartmentByCompanyId(companyId);
                if (defaultDepartment != null)
                {
                    userModel.DepartmentId = defaultDepartment.Id;
                }
            }

            userModel.Departments = _departmentService
                .GetByCompanyId(companyId).Select(Mapper.Map<DepartmentModelForUser>).ToList();

            if (userModel.Departments.Count() == 1)
            {
                // If there is only one department(maybe default), the department is automatically set up when manager add users.
                userModel.DepartmentId = userModel.Departments.First().Id;
            }

            var accessGroups = _accessGroupService.GetListAccessGroupsExceptForVisitor()
                .Select(Mapper.Map<AccessGroupModelForUser>).ToList();

            var accessGroupDefault =
                _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(companyId);
            if (userModel.AccessGroupId == 0 && accessGroupDefault != null)
            {
                userModel.AccessGroupId = accessGroupDefault.Id;
                var index = accessGroups.FindIndex(x => x.Id == accessGroupDefault.Id);
                var item = accessGroups[index];
                accessGroups[index] = accessGroups[0];
                accessGroups[0] = item;
            }

            userModel.AccessGroups = accessGroups;

            if (userModel.Id != 0)
            {

                var cards = _unitOfWork.CardRepository.GetByUserId(companyId, userModel.Id);

                var cardModelList = new List<CardModel>();
                foreach (var card in cards)
                {
                    var cardModel = Mapper.Map<CardModel>(card);
                    cardModelList.Add(cardModel);
                }

                userModel.CardList = cardModelList;
            }
            else//Id is 0
            {
                var users = _unitOfWork.UserRepository.GetByCompanyId(companyId);
                var userCode = 1;
                if (users.Count() > 0)
                {
                    userCode = _unitOfWork.UserRepository.GetNewUserCode(companyId);

                }
                userModel.UserCode = String.Format("{0:000000}", userCode);
            }

            userModel.Categories = _categoryService.GetCategoryHierarchy();
            userModel.CategoryOptions = _categoryService.GetOptionHierarchy();
            userModel.CategoryOptionIds = _categoryService.GetCategoryOptionIdsByUserId(userModel.Id);
        }


        public int GetCardCount(int accessGroupId)
        {
            int cardCount = 0;
            var accessGroup = _unitOfWork.AccessGroupRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(), accessGroupId);

            var users = _unitOfWork.UserRepository.GetAssignUsersByAccessGroupIds(_httpContext.User.GetCompanyId(), new List<int> { accessGroup.Id });

            if (users.Any())
            {
                foreach (var user in users)
                {
                    var cards = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), user.Id);
                    cardCount += cards.Count();
                }

            }


            return cardCount;
        }



        /// <summary>
        /// Check if account is exist
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool IsAccountExist(int id, string username, int? companyId = null)
        {
            if (companyId == null)
            {
                companyId = _httpContext.User.GetCompanyId();
            }

            var userLogin = _unitOfWork.AccountRepository.Get(m =>
                m.Username == username && !m.IsDeleted &&
                m.CompanyId == companyId && m.Id != id);

            return userLogin != null;
        }



        public Account GetAccountByEmail(string username, int? companyId)
        {
            if (companyId == null)
            {
                companyId = _httpContext.User.GetCompanyId();
            }

            var userLogin = _unitOfWork.AccountRepository.Get(m =>
                m.Username == username && !m.IsDeleted &&
                m.CompanyId == companyId);

            return userLogin;
        }

        public Account GetAccountById(int userId)
        {
            var account = _unitOfWork.AccountRepository.Get(m =>
                m.Id == userId && !m.IsDeleted);

            return account;
        }



        /// <summary>
        /// Add a user
        /// </summary>
        /// <param name="userModel"></param>
        public int Add(UserModel userModel)
        {
            // variable declaration
            User user = new User();

            var isValid = userModel.Status == (short)Status.Valid;
            //var userCardModelList = new List<UserCardModel>();
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();

                        user = Mapper.Map<User>(userModel);
                        user.CompanyId = companyId;

                        //user.Email register
                        var company = _unitOfWork.CompanyRepository.GetCompanyById(companyId);
                        var account = _unitOfWork.AccountRepository.GetByUserNameAndCompanyId(companyId, userModel.Email);

                        if (account != null)
                        {
                            // If account is not null, this email is already registered in system as 'Primary manager or Secondary manager or System admin'.
                            // Therefore, it is necessary to add the appropriate permission type for the user.
                            user.AccountId = account.Id;

                            user.PermissionType = account.Type;

                            _unitOfWork.UserRepository.Add(user);
                        }
                        else
                        {
                            var accountModel = new AccountModel()
                            {
                                Username = userModel.Email,
                                CompanyId = companyId,
                                Password = Helpers.GenerateRandomPassword(),
                                ConfirmPassword = Helpers.GenerateRandomPassword(),
                                Role = (short)AccountType.Employee
                            };

                            var newAccount = Mapper.Map<Account>(accountModel);
                            newAccount.CompanyId = companyId;
                            _unitOfWork.AccountRepository.Add(newAccount);
                            _unitOfWork.Save();

                            user.AccountId = newAccount.Id;
                            user.PermissionType = (short)AccountType.Employee;

                            _unitOfWork.UserRepository.Add(user);

                            var token = _accountService.GetTokenByAccount(newAccount);
                            _accountService.SendWelcomeMail(newAccount.Username, user.FirstName, token, companyId);

                        }

                        _unitOfWork.Save();

                        // save categoryOption
                        List<int> options = userModel.CategoryOptionIds;
                        _categoryService.SetUserOptions(user.Id, options);

                        // Save system log
                        // include accessgroup, cardstatus, master card, Department
                        var details = new List<string>();

                        var accessGroup = _unitOfWork.AccessGroupRepository.GetById(user.AccessGroupId);

                        var detail = $"{UserResource.lblAccessGroup} : {accessGroup?.Name}";
                        details.Add(detail);

                        var masterCard = user.IsMasterCard ? "Master Card" : "Normal Card";
                        detail = $"{UserResource.lblIsMasterCard} : {masterCard}";
                        details.Add(detail);

                        var department = _unitOfWork.DepartmentRepository.GetById(user.DepartmentId);
                        detail = $"{UserResource.lblDepartment} : { department?.DepartName }";
                        details.Add(detail);

                        var content =
                            $"{ActionLogTypeResource.Add} : {user.FirstName + " " + user.LastName} ({UserResource.lblUser})";
                        var contentDetails = $"{UserResource.lblAddNew} :\n{string.Join("\n", details)}";

                        _unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Add,
                            content, contentDetails, null, user.CompanyId);
                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            return user.Id;
        }

        /// <summary>
        /// Update user
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        public void Update(UserModel userModel)
        {
            // variable declaration
            User user = new User();

            var isValid = userModel.Status == (short)Status.Valid;

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        var company = _unitOfWork.CompanyRepository.GetCompanyById(companyId);
                        user = GetByIdAndCompany(userModel.Id, companyId);

                        List<string> changes = new List<string>();
                        var isChagned = CheckChange(user, userModel, ref changes);

                        if (userModel.Email != user.Email)
                        {
                            var account = _unitOfWork.AccountRepository.GetByUserNameAndCompanyId(companyId, userModel.Email);
                            if (account != null)
                            {
                                user.AccountId = account.Id;
                                _unitOfWork.UserRepository.Update(user);
                            }
                            else
                            {
                                var accountModel = new AccountModel()
                                {
                                    Username = userModel.Email,
                                    CompanyId = companyId,
                                    Password = Helpers.GenerateRandomPassword(),
                                    ConfirmPassword = Helpers.GenerateRandomPassword(),
                                    Role = (short)AccountType.Employee
                                };

                                var newAccount = Mapper.Map<Account>(accountModel);
                                newAccount.CompanyId = companyId;
                                _unitOfWork.AccountRepository.Add(newAccount);
                                _unitOfWork.Save();

                                user.AccountId = newAccount.Id;
                                _unitOfWork.UserRepository.Update(user);

                                var token = _accountService.GetTokenByAccount(newAccount);
                                _accountService.SendUpdateMail(newAccount.Username, userModel.FirstName, token, companyId);
                            }
                        }
                        else
                        {
                            var account = _unitOfWork.AccountRepository.GetByUserNameAndCompanyId(companyId, userModel.Email);
                            if (account != null)
                            {
                                user.AccountId = account.Id;
                                _unitOfWork.UserRepository.Update(user);
                            }
                        }

                        // update categoryOptions
                        List<int> oldOptions = new List<int>();
                        oldOptions = _categoryService.GetCategoryOptionIdsByUserId(user.Id);
                        List<int> newOptions = new List<int>();
                        newOptions = userModel.CategoryOptionIds;

                        // option ids to be deleted.
                        var firstNotSecond = oldOptions.Except(newOptions).ToList();
                        // option ids to be added.
                        var secondNotFirst = newOptions.Except(oldOptions).ToList();

                        _categoryService.DeleteUserOptions(user.Id, firstNotSecond);
                        _categoryService.SetUserOptions(user.Id, secondNotFirst);

                        // Send message to device for deleting exiting users(cards). -> isAddUser is false.
                        SendUpdateUsersToAllDoors(user, false);

                        Mapper.Map(userModel, user);

                        //Save system log
                        var content = $"{ActionLogTypeResource.Update} : {user.FirstName + " " + user.LastName} ({user.UserCode})";

                        var contentDetails = $"{UserResource.lblUpdate}: {string.Join("\n", changes)}";
                        _unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Update,
                            content, contentDetails, null, user.CompanyId);

                        _unitOfWork.UserRepository.Update(user);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            //var discardList = new[]
            //    {(short) CardStatus.InValid, (short) CardStatus.Retire, (short) CardStatus.Lost};
            //if (!discardList.Contains(user.CardStatus))
            //{
            //    SendUpdateUsersToAllDoors(user, true);
            //}

            if (isValid)
                SendUpdateUsersToAllDoors(user, true);
        }

        ///// <summary>
        ///// Add new card to exist user
        ///// </summary>
        ///// <param name="user"></param>
        ///// <param name="cardModel"></param>
        ///// <returns></returns>
        //public void AddCard(User user, CardModel cardModel)
        //{
        //    //User user = null;

        //    _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
        //    {
        //        using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                //var oldCardList = _unitOfWork.CardRepository.GetByUserId(user.CompanyId, user.Id);

        //                // Send an existing card delete command.
        //                //if (oldCardList.Any())
        //                //{
        //                //    SendUpdateUsersToAllDoors(user, false);
        //                //}

        //                // update card status to deleted -> Why does this have to be done?
        //                // It is just add a new card to exist user. I think that It is unnecessary to delete exist card.
        //                //foreach (var oldCard in oldCardList)
        //                //{
        //                //    oldCard.IsDeleted = true;
        //                //    _unitOfWork.CardRepository.Update(oldCard);
        //                //}

        //                //Add Cards for a User
        //                //foreach (var cardModel in cardModels)
        //                //{
        //                var card = new Card()
        //                {
        //                    CardId = cardModel.CardId,
        //                    CardStatus = cardModel.CardStatus,
        //                    IssueCount = cardModel.IssueCount,
        //                    Note = cardModel.Description,
        //                    UserId = user.Id,
        //                    CompanyId = user.CompanyId,
        //                    // I added below code, but I think it doesn't need.
        //                    // Because AccessGroupId, IsMasterCard are only required by the User.
        //                    // So, I think that deleting these 2 attributes from Card table is better.
        //                    AccessGroupId = user.AccessGroupId,
        //                    IsMasterCard = user.IsMasterCard
        //                };

        //                _unitOfWork.CardRepository.Add(card);

        //                // API must send a new card to device that the user is included.
        //                SendNewCardToDevice(user, card);

        //                //var oldUser = user;

        //                // It is not necessary, because it is just add, not update.
        //                //include accessgroup, cardstatus, master card, Department
        //                //var details = new List<string>();
        //                //if (user.AccessGroupId != oldUser.AccessGroupId)
        //                //{
        //                //    var detail = $"Access Group : {oldUser.AccessGroup.Name} -> {user.AccessGroup.Name}";
        //                //    details.Add(detail);
        //                //}

        //                //if (user.IsMasterCard != oldUser.IsMasterCard)
        //                //{
        //                //    var change = user.IsMasterCard ? "Normal Card -> Master Card" : "Master Card -> Normal Card";
        //                //    var detail = $"Master Card : {change}";
        //                //    details.Add(detail);
        //                //}

        //                //if (user.DepartmentId != oldUser.DepartmentId)
        //                //{
        //                //    var detail = $"Department : { oldUser.Department.DepartName } -> { user.Department.DepartName }";
        //                //    details.Add(detail);
        //                //}

        //                //Save system log
        //                var content = string.Format(UserResource.msgAddNewCertification, CommonResource.Card, user.UserCode);
        //                //var contentDetails = $"{UserResource.lblUpdate}: {string.Join(", ", details)}";
        //                _unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Update,
        //                    content, null, null, _httpContext.User.GetCompanyId());

        //                // Is it about updaing user? Or just updating card?
        //                // I think it needs to update card
        //                //_unitOfWork.UserRepository.Update(user);
        //                _unitOfWork.Save();
        //                transaction.Commit();
        //            }
        //            catch (Exception)
        //            {
        //                transaction.Rollback();
        //                throw;
        //            }
        //        }
        //    });

        //    // TODO
        //    // This should be changed to send only new card to device,
        //    // But API must send a new card to device that the user is included.
        //    //SendUpdateUsersToAllDoors(user, true);
        //}

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public void Delete(User user)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {

                        var oldCardList = _unitOfWork.CardRepository.GetByUserId(user.CompanyId, user.Id);
                        if (oldCardList.Any())
                        {
                            SendUpdateUsersToAllDoors(user, false);
                        }
                        //Delete all Cards for a User
                        foreach (var oldCard in oldCardList)
                        {
                            oldCard.IsDeleted = true;
                            _unitOfWork.CardRepository.Delete(oldCard);
                        }

                        if (user.PermissionType != (short)PermissionType.NotUse &&
                            !string.IsNullOrEmpty(user.Email))
                        {
                            var account =
                                _unitOfWork.AccountRepository.GetByUserNameAndCompanyId(
                                    _httpContext.User.GetCompanyId(), user.Email);

                            if (account != null)
                            {
                                account.IsDeleted = true;
                                _unitOfWork.AccountRepository.Update(account);
                            }
                        }

                        user.IsDeleted = true;
                        _unitOfWork.UserRepository.Update(user);

                        //Save system log
                        var content =
                            $"{ActionLogTypeResource.Delete} : {user.FirstName} {user.LastName} ({UserResource.lblUserCode} : {user.UserCode})";

                        _unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Delete,
                            content, null, new List<int> { user.Id }, user.CompanyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

        }

        /// <summary>
        /// Delete a list of user
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public void DeleteRange(List<User> users)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        List<string> userNames = new List<string>();

                        foreach (var user in users)
                        {
                            var attendance =
                                _unitOfWork.AppDbContext.Attendance.Where(a => a.CompanyId == user.CompanyId && a.UserId == user.Id);
                            foreach (var i in attendance)
                            {
                                _unitOfWork.AttendanceRepository.Delete(i);
                            }
                            var oldCardList = _unitOfWork.CardRepository.GetByUserId(user.CompanyId, user.Id);
                            if (oldCardList.Any())
                            {
                                SendUpdateUsersToAllDoors(user, false);
                            }
                            //Delete all Cards for a User
                            foreach (var oldCard in oldCardList)
                            {
                                oldCard.IsDeleted = true;
                                _unitOfWork.CardRepository.Delete(oldCard);
                            }

                            user.IsDeleted = true;

                            userNames.Add(user.FirstName + " " + user.LastName);

                            if (user.PermissionType != (short)PermissionType.NotUse &&
                                !string.IsNullOrEmpty(user.Email))
                            {
                                var account =
                                    _unitOfWork.AccountRepository.GetByUserNameAndCompanyId(
                                        _httpContext.User.GetCompanyId(), user.Email);

                                if (account != null && account.Type == (short) AccountType.Employee)
                                {
                                    account.IsDeleted = true;
                                    _unitOfWork.AccountRepository.Update(account);
                                }
                            }

                            _unitOfWork.UserRepository.Update(user);

                        }

                        //Save system log
                        if (users.Count == 1)
                        {
                            var user = users.First();
                            var content = $"{ActionLogTypeResource.Delete}: {user.FirstName} {user.LastName} ({UserResource.lblUserCode} : {user.UserCode})";

                            _unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Delete,
                                content, null, null, user.CompanyId);
                        }
                        else
                        {
                            var content = string.Format(ActionLogTypeResource.DeleteMultipleType, UserResource.lblUser);
                            var userIds = users.Select(c => c.Id).ToList();

                            var contentDetails = $"{UserResource.lblUserCount} : {userIds.Count}\n" +
                                                $"{UserResource.lblName}: {string.Join(", ", userNames)}";

                            _unitOfWork.SystemLogRepository.Add(userIds.First(), SystemLogType.User, ActionLogType.DeleteMultiple,
                                content, contentDetails, userIds, _httpContext.User.GetCompanyId());
                        }

                        foreach (var user in users)
                        {
                            SendUpdateUsersToAllDoors(user, false);
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

        }

        /// <summary>
        /// Get user by id and company
        /// </summary>
        /// <param name="id"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public User GetByIdAndCompany(int id, int companyId)
        {
            var data = _unitOfWork.AppDbContext.User.Include(m => m.Department).Include(m => m.Company).Include(m => m.AccessGroup).Where(m => m.Id == id && !m.IsDeleted);

            if (companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            //return _unitOfWork.AppDbContext.User.Include(m => m.Department).Include(m => m.Company).Include(m => m.AccessGroup).Where(m => m.Id == id && m.CompanyId == companyId && !m.IsDeleted).First();

            return data.First();
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public User GetById(int id)
        {
            var user = _unitOfWork.UserRepository.GetByUserId(_httpContext.User.GetCompanyId(), id);

            if (user != null)
            {
                if (user.ExpiredDate != null)
                {
                    DateTime newSelectedDate = user.ExpiredDate.Value;
                    user.ExpiredDate = newSelectedDate;
                }
                if (user.BirthDay != null)
                {
                    DateTime newSelectedDate = user.BirthDay.Value;
                    user.BirthDay = newSelectedDate;
                }
                if (user.EffectiveDate != null)
                {
                    DateTime newSelectedDate = user.EffectiveDate.Value;
                    user.EffectiveDate = newSelectedDate;
                }
            }

            return user;
        }

        /// <summary>
        /// Get by cardid
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public User GetByCardId(string cardId)
        {
            //return _unitOfWork.UserRepository.Get(m => m.CardId.Equals(cardId) && m.CompanyId == _httpContext.User.GetCompanyId() && !m.IsDeleted);
            var card = _unitOfWork.CardRepository.Get(m =>
                m.CardId.Equals(cardId) && m.CompanyId == _httpContext.User.GetCompanyId() && !m.IsDeleted);
            return _unitOfWork.UserRepository.Get(m => m.Id == card.UserId && m.CompanyId == _httpContext.User.GetCompanyId() && !m.IsDeleted);
        }

        public Card GetCardByUser(int userId, int cardId)
        {
            //return _unitOfWork.UserRepository.Get(m => m.CardId.Equals(cardId) && m.CompanyId == _httpContext.User.GetCompanyId() && !m.IsDeleted);
            var card = _unitOfWork.CardRepository.Get(m =>
                m.Id == cardId && m.UserId == userId && !m.IsDeleted);
            return card;
        }


        public User GetByEmail(string email)
        {
            var user = _unitOfWork.UserRepository.Get(c => c.Email.Equals(email) && !c.IsDeleted);

            return user;
        }

        /// <summary>
        /// Check whether a valid user exist
        /// </summary>
        /// <returns></returns>
        public bool CheckData()
        {
            var companyId = _httpContext.User.GetCompanyId();
            return _unitOfWork.AppDbContext.User.Any(x => x.CompanyId == companyId && !x.IsDeleted);
        }

        /// <summary>
        /// Check if some field of user have changes
        /// </summary>
        /// <param name="user"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        internal bool HasChange(User user, UserModel model)
        {
            var expiredDate = user.ExpiredDate != null
                ? user.ExpiredDate.Value.ToString(CultureInfo.InvariantCulture)
                : "";
            //if (user.CardId != model.CardId || !expiredDate.Equals(model.ExpiredDate ?? "") ||
            //    (!string.IsNullOrEmpty(model.KeyPadPassword) && string.IsNullOrEmpty(user.KeyPadPw)) ||
            //    (!string.IsNullOrEmpty(model.KeyPadPassword) && !string.IsNullOrEmpty(user.KeyPadPw) &&
            //     Encryptor.Decrypt(user.KeyPadPw, _configuration[Constants.Settings.EncryptKey]) !=
            //     model.KeyPadPassword))
            //{
            //    return true;
            //}

            return false;
        }

        /// <summary>
        /// Get list user by ids and company
        /// </summary>
        /// <param name="userIds"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<User> GetByIdsAndCompany(List<int> userIds, int companyId)
        {
            return _unitOfWork.UserRepository.GetMany(m => userIds.Contains(m.Id) && m.CompanyId == companyId && !m.IsDeleted).ToList();
        }

        ///// <summary>
        ///// Export user data to excel or txt file
        ///// </summary>
        ///// <param name="type"></param>
        ///// <param name="filter"></param>
        ///// <param name="sortColumn"></param>
        ///// <param name="sortDirection"></param>
        ///// <param name="totalRecords"></param>
        ///// <param name="recordsFiltered"></param>
        ///// <returns></returns>
        public byte[] Export(string type, string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var fileByte = type == Constants.Excel
                ? ExportExcel(filter, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered)
                : ExportTxt(filter, sortColumn, sortDirection, out totalRecords,
                    out recordsFiltered);
            //var fileByte =
            //    ExportExcel(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered);

            //Save system log
            //var companyId = _httpContext.User.GetCompanyId();
            //var content = string.Format(UserResource.msgExportUserList, DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss"),
            //    _httpContext.User.Identity.Name);
            //_unitOfWork.SystemLogRepository.Add(companyId, SystemLogType.User, ActionLogType.Export, content);
            //_unitOfWork.Save();

            return fileByte;
        }

        public byte[] ExportAccessibleDoors(User user, string type, string search, out int totalRecords,
            out int recordsFiltered, int sortColumn = 0,
            string sortDirection = "desc")
        {
            var accessibleDoorModels = FilterAccessibleDoorDataWithOrder(user, search, sortColumn, sortDirection, out totalRecords,
               out recordsFiltered).ToList();

            var fileByte = type == Constants.Excel
                ? ExportAccessibleDoorsWithExcel(accessibleDoorModels)
                : ExportAccessibleDoorsWithTxt(accessibleDoorModels);

            //Save system log
            var companyId = _httpContext.User.GetCompanyId();
            var content = string.Format(UserResource.msgExportAccessibleDoorList, user.FirstName, DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss"),
                _httpContext.User.GetUsername());
            _unitOfWork.SystemLogRepository.Add(companyId, SystemLogType.User, ActionLogType.Export, content, null, null, _httpContext.User.GetCompanyId());
            _unitOfWork.Save();

            return fileByte;
        }

        ///// <summary>
        ///// Send new account email
        ///// </summary>
        ///// <param name="email"></param>
        ///// <param name="originalPass"></param>
        //public void SendAccountMail(string email, string originalPass)
        //{
        //    var thread = new Thread(delegate ()
        //    {
        //        var companyUrl = Helpers.GetLoginPath(_httpContext, _httpContext.User.GetCompanyCode());
        //        var formatBody = MailContentResource.BodyCompanyAccount;
        //        var subject = MailContentResource.SubjectCompanyAccount;
        //        var body = string.Format(formatBody
        //            , email
        //            , companyUrl
        //            , companyUrl
        //            , email
        //            , originalPass);

        //        _mailService.SendMail(email, null
        //            , subject
        //            , body);
        //    });
        //    thread.Start();
        //}

        //public void SendAccountMailToAdmin(string email, string useremail, string originalPass)
        //{
        //    var thread = new Thread(delegate ()
        //    {
        //        var companyUrl = Helpers.GetLoginPath(_httpContext, _httpContext.User.GetCompanyCode());
        //        var formatBody = MailContentResource.BodyCompanyAccountAdmin;
        //        var subject = MailContentResource.SubjectCompanyAccountAdmin;
        //        var body = string.Format(formatBody
        //            , email
        //            , companyUrl
        //            , companyUrl
        //            , useremail
        //                );

        //        _mailService.SendMail(email, null
        //            , subject
        //            , body);
        //    });
        //    thread.Start();
        //}

        ///// <summary>
        ///// Export Accessible doors data to txt file
        ///// </summary>
        ///// <param name="accessibleDoorModels"></param>
        ///// <returns></returns>
        public byte[] ExportAccessibleDoorsWithExcel(List<AccessibleDoorModel> accessibleDoorModels)
        {
            byte[] result;

            //var package = new ExcelPackage();
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(UserResource.lblAccessibleDoors); //Worksheet name

                //First add the headers for Accessible Door sheet
                for (var i = 0; i < _headerForAccessibleDoor.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _headerForAccessibleDoor[i];
                }

                var recordIndex = 2;
                foreach (var accessibleDoorModel in accessibleDoorModels)
                {
                    //For the Accessible Door sheet
                    var colIndex = 1;
                    //worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.Id;
                    worksheet.Cells[recordIndex, colIndex++].Value = recordIndex - 1;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.DoorName;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.DeviceAddress;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.ActiveTz;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.PassageTz;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.VerifyMode;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.AntiPassback;
                    worksheet.Cells[recordIndex, colIndex++].Value = accessibleDoorModel.DeviceType;
                    worksheet.Cells[recordIndex, colIndex].Value = accessibleDoorModel.Mpr;

                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }
            return result;
        }

        ///// <summary>
        ///// Export Accessible doors data to txt file
        ///// </summary>
        ///// <param name="accessibleDoorModels"></param>
        ///// <returns></returns>
        public byte[] ExportAccessibleDoorsWithTxt(List<AccessibleDoorModel> accessibleDoorModels)
        {
            // Build the file content

            var cnt = 1;
            var accessibleDoors = accessibleDoorModels.Select(x => new object[]
            {
                x.Id = cnt++,
                x.DoorName,
                x.DeviceAddress,
                x.ActiveTz,
                x.PassageTz,
                x.VerifyMode,
                x.AntiPassback,
                x.DeviceType,
                x.Mpr
            }).ToList();

            var accessibleDoorTxt = new StringBuilder();
            accessibleDoors.ForEach(line => { accessibleDoorTxt.AppendLine(string.Join(",", line)); });

            byte[] buffer = Encoding.UTF8.GetBytes($"{string.Join(",", _headerForAccessibleDoor)}\r\n{accessibleDoorTxt}");
            return buffer;
        }




        /// <summary>
        /// Export user data to excel file
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public byte[] ExportExcel(string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            byte[] result;

            //var package = new ExcelPackage();
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(UserResource.lblUser); //Worksheet name

                var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords,
                        out recordsFiltered, true);

                foreach (var i in data)
                {
                    if (i.ExpiredDate != null)
                    {
                        DateTime newSelectedDate = i.ExpiredDate.Value;
                        i.ExpiredDate = Helpers.ConvertToUserTimeZoneReturnDate(newSelectedDate, GetAccountById(_httpContext.User.GetAccountId()).TimeZone);
                    }

                }
                var users= data.ToList();

                //First add the headers for user sheet
                for (var i = 0; i < _header.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }

                var recordIndex = 2;
                foreach (var user in users)
                {
                    //For the User sheet
                    var cards = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), user.Id);

                    if (cards == null || !cards.Any())
                    {
                        var colIndex = 1;
                        var accessGroup = _unitOfWork.AccessGroupRepository.GetById(user.AccessGroupId);
                        worksheet.Cells[recordIndex, colIndex++].Value = user.UserCode;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.AccessGroupId < 3 ? ((AccessGroupType)user.AccessGroupId).GetDescription() : accessGroup.Name;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.IsMasterCard;
                        worksheet.Cells[recordIndex, colIndex++].Value = "";
                        worksheet.Cells[recordIndex, colIndex++].Value = Enum.GetName(typeof(CardType), 0);
                        worksheet.Cells[recordIndex, colIndex++].Value = "";
                        worksheet.Cells[recordIndex, colIndex++].Value = user.FirstName;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.LastName;
                        worksheet.Cells[recordIndex, colIndex++].Value = ((SexType)Convert.ToInt32(user.Sex)).GetDescription();
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Email;
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.BirthDay;
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex].Value = "";
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.EffectiveDate?.ToOADate();
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.ExpiredDate?.ToOADate();
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Department.DepartName;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Department.DepartNo;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.EmpNumber;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Position;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.PostCode;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Job;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Responsibility;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.OfficePhone;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.HomePhone;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Address;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Nationality;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.City;
                        worksheet.Cells[recordIndex, colIndex].Value = user.Remarks;

                        recordIndex++;
                    }
                    else
                    {
                        var cardIdsString = "";

                        foreach (var card in cards)
                        {
                            if (card.CardType == (short)CardType.NFC)
                            {
                                if (cardIdsString == "")
                                {
                                    cardIdsString += card.CardId;
                                }
                                else
                                {
                                    cardIdsString += "," + card.CardId;
                                }

                            }

                        }
                        var colIndex = 1;
                        var accessGroup = _unitOfWork.AccessGroupRepository.GetById(user.AccessGroupId);
                        worksheet.Cells[recordIndex, colIndex++].Value = user.UserCode;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.AccessGroupId < 3 ? ((AccessGroupType)user.AccessGroupId).GetDescription() : accessGroup.Name;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.IsMasterCard;
                        worksheet.Cells[recordIndex, colIndex++].Value = (short)CardStatus.Normal;
                        worksheet.Cells[recordIndex, colIndex++].Value = Enum.GetName(typeof(CardType), 0);
                        worksheet.Cells[recordIndex, colIndex++].Value = cardIdsString;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.FirstName;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.LastName;
                        worksheet.Cells[recordIndex, colIndex++].Value = ((SexType)Convert.ToInt32(user.Sex)).GetDescription();
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Email;
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex].Value = user.BirthDay;
                        worksheet.Cells[recordIndex, colIndex++].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex].Value = "";
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.EffectiveDate?.ToOADate();
                        worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.ExpiredDate?.ToOADate();

                        worksheet.Cells[recordIndex, colIndex++].Value = user.Department.DepartName;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Department.DepartNo;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.EmpNumber;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Position;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.PostCode;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Job;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Responsibility;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.OfficePhone;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.HomePhone;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Address;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.Nationality;
                        worksheet.Cells[recordIndex, colIndex++].Value = user.City;
                        worksheet.Cells[recordIndex, colIndex].Value = user.Remarks;

                        recordIndex++;

                    }

                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        ///// <summary>
        ///// Export user data to txt file
        ///// </summary>
        ///// <param name="filter"></param>
        ///// <param name="sortColumn"></param>
        ///// <param name="sortDirection"></param>
        ///// <param name="totalRecords"></param>
        ///// <param name="recordsFiltered"></param>
        ///// <returns></returns>
        public byte[] ExportTxt(string filter, int sortColumn, string sortDirection, out int totalRecords,
            out int recordsFiltered)
        {
            var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords,
                        out recordsFiltered, true);

            foreach (var i in data)
            {
                if (i.ExpiredDate != null)
                {
                    DateTime newSelectedDate = i.ExpiredDate.Value;
                    i.ExpiredDate = Helpers.ConvertToUserTimeZoneReturnDate(newSelectedDate, GetAccountById(_httpContext.User.GetAccountId()).TimeZone);
                }

            }
            var temp = data.ToList();
            var userCardList = new List<object[]>();




            foreach (var user in temp)
            {
                //For the User sheet
                var cards = user.Card.ToList().Where(m => !m.IsDeleted);

                if (cards == null || !cards.Any())
                {
                    userCardList.Add(
                        new object[]
                        {
                            user.UserCode,
                            user.AccessGroupId,
                            0,//IssueCount
                            user.IsMasterCard,
                            0,//x.CardStatus,
                            //((CardStatus)x.CardStatus).GetDescription(),
                            Enum.GetName(typeof(CardType), 0),
                            "",//x.CardId,
                            user.FirstName,
                            user.LastName,
                            ((SexType)Convert.ToInt32(user.Sex)).GetDescription(),
                            user.BirthDay?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            //x.IssuedDate?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            user.EffectiveDate?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            user.ExpiredDate?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            user.Department.DepartName,
                            user.Department.DepartNo,
                            user.EmpNumber,
                            user.Position,
                            //string.Empty,
                            user.PostCode,
                            user.Job,
                            user.Responsibility,
                            user.OfficePhone,
                            user.HomePhone,
                            user.Address,
                            user.Nationality,
                            user.City,
                            user.Remarks
                        });
                }
                else
                {
                    var cardIdsString = "";
                    foreach (var card in cards)
                    {
                        if (card.CardType == (short)CardType.NFC)
                        {
                            if (cardIdsString == "")
                            {
                                cardIdsString += card.CardId;
                            }
                            else
                            {
                                cardIdsString += "-" + card.CardId;
                            }
                        }
                    }

                    userCardList.Add(
                        new object[]
                        {
                            user.UserCode,
                            user.AccessGroupId,
                            "",//IssueCount
                            user.IsMasterCard,
                            //0,//x.CardStatus,
                            (short) CardStatus.Normal,
                            Enum.GetName(typeof(CardType), 0),
                            cardIdsString,//"",//x.CardId,
                            user.FirstName,
                            user.LastName,
                            ((SexType)Convert.ToInt32(user.Sex)).GetDescription(),
                            user.BirthDay?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            //x.IssuedDate?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            user.EffectiveDate?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            user.ExpiredDate?.ToString(Constants.DateTimeFormat.YyyyMMdd),
                            user.Department.DepartName,
                            user.Department.DepartNo,
                            user.EmpNumber,
                            user.Position,
                            //string.Empty,
                            user.PostCode,
                            user.Job,
                            user.Responsibility,
                            user.OfficePhone,
                            user.HomePhone,
                            user.Address,
                            user.Nationality,
                            user.City,
                            user.Remarks
                        });


                }
            }

            // Build the file content
            var userTxt = new StringBuilder();
            userCardList.ForEach(line =>
            {
                //var items = string.Join(",", line).Split(",");
                //int accessGroupId = Int32.Parse(items[0]);

                int accessGroupId = (int)line[1];
                var accessGroup = _unitOfWork.AccessGroupRepository.GetById(accessGroupId);
                //line[0] = accessGroup.Name;
                line[1] = accessGroupId < 3 ? ((AccessGroupType)accessGroupId).GetDescription() : accessGroup.Name;
                userTxt.AppendLine(string.Join(",", line));

            });

            byte[] buffer = Encoding.UTF8.GetBytes($"{string.Join(",", _header)}\r\n{userTxt}");
            return buffer;
        }

        /// <summary>
        /// Get data with pagenation
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="isValid"></param>
        /// <returns></returns>
        public IQueryable<User> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, bool isValid = true, int departmentId = 0)
        {
            var data = FilterDataWithOrder(filter, sortColumn, sortDirection, out totalRecords, out recordsFiltered, isValid, departmentId);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            foreach (var i in data)
            {
                if (i.ExpiredDate != null)
                {
                    DateTime newSelectedDate = i.ExpiredDate.Value;
                    i.ExpiredDate = Helpers.ConvertToUserTimeZoneReturnDate(newSelectedDate, GetAccountById(_httpContext.User.GetAccountId()).TimeZone);
                }

            }
            return data;
        }

        /// <summary>
        /// Get paginated accessible doors
        /// </summary>
        /// <param name="user"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<AccessibleDoorModel> GetPaginatedAccessibleDoors(User user, string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            //var companyId = _httpContext.User.GetCompanyId();
            var companyId = user.CompanyId;
            var data = _unitOfWork.IcuDeviceRepository.GetDevicesByAccessGroup(companyId, user.AccessGroupId);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.DeviceAddress.ToLower().Contains(filter.ToLower()) ||
                    ((DeviceType)x.DeviceType).GetDescription().ToLower().Contains(filter.ToLower()) ||
                    x.ActiveTz.Name.ToLower().Contains(filter.ToLower()) ||
                    x.PassageTz.Name.ToLower().Contains(filter.ToLower()) ||
                    ((VerifyMode)x.VerifyMode).GetDescription().ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.AccessibleDoor.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.AccessibleDoor[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            var accessibleDoors = data.AsEnumerable<IcuDevice>().Select(Mapper.Map<AccessibleDoorModel>).ToList();
            foreach (var accessDoor in accessibleDoors)
            {
                var accessGroupDevice =
                    _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupIdAndDeviceId(companyId,
                        user.AccessGroupId, accessDoor.Id);
                accessDoor.AccessGroupTz = accessGroupDevice.Tz.Name;
            }

            return accessibleDoors;
        }

        /// <summary>
        /// Filter and order data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        internal IQueryable<AccessibleDoorModel> FilterAccessibleDoorDataWithOrder(User user, string filter, int sortColumn, string sortDirection,
            out int totalRecords,
            out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.IcuDeviceRepository.GetDevicesByAccessGroup(companyId, user.AccessGroupId);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter.ToLower()) ||
                    x.DeviceAddress.ToLower().Contains(filter.ToLower()) ||
                    ((DeviceType)x.DeviceType).GetDescription().ToLower().Contains(filter.ToLower()) ||
                    x.ActiveTz.Name.ToLower().Contains(filter.ToLower()) ||
                    x.PassageTz.Name.ToLower().Contains(filter.ToLower()) ||
                    ((VerifyMode)x.VerifyMode).GetDescription().ToLower().Contains(filter.ToLower()));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.AccessibleDoor.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.AccessibleDoor[sortColumn]} {sortDirection}");
            return data.AsEnumerable<IcuDevice>().Select(Mapper.Map<AccessibleDoorModel>).AsQueryable();
        }

        /// <summary>
        /// Filter and order data
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <param name="isValid"></param>
        /// <returns></returns>
        internal IQueryable<User> FilterDataWithOrder(string filter, int sortColumn, string sortDirection,
            out int totalRecords,
            out int recordsFiltered, bool isValid, int departmentId = 0)
        {
            var start = DateTime.Now;

            var companyId = _httpContext.User.GetCompanyId();
            var data = isValid
                ? _unitOfWork.UserRepository.GetByCompanyId(companyId)
                : _unitOfWork.UserRepository.GetUserByCompany(companyId);

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                var cardStatusVal = GetCardStatusValue(filter);
                data = data.Where(x =>
                    x.FirstName.ToLower().Contains(filter)
                    || x.LastName.ToLower().Contains(filter)
                    || x.Department.DepartName.ToLower().Contains(filter)
                    || (!string.IsNullOrEmpty(x.EmpNumber) && x.EmpNumber.ToLower().Contains(filter))
                    || x.ExpiredDate.ToString().ToLower().Contains(filter));
                //|| cardStatusVal != -1 && x.CardStatus == cardStatusVal);

                //|| x.CardStatus).GetDescription().ToLower().Contains(filter));
                //|| x.IssueCount.ToString().ToLower().Contains(filter));

                //

                //data = data.Where(x => 
                //        ((CardStatus)x.CardStatus).GetDescription().ToLower().Contains(filter));



            }

            recordsFiltered = data.Count();
            var start1 = DateTime.Now;
            sortColumn = sortColumn > ColumnDefines.UserList.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.UserList[sortColumn]} {sortDirection}");
    
            if(departmentId > 0)
            {
                data = data.Where(x => x.DepartmentId == departmentId);
            }

            var start3 = DateTime.Now;
            return data;
        }

        short GetCardStatusValue(string cardStatus)
        {
            var temp = CardStatus.Normal.GetDescription();
            if (CardStatus.Normal.GetDescription().Contains(cardStatus))
                return 0;
            else if (CardStatus.Temp.GetDescription().Contains(cardStatus))
                return 1;
            else if (CardStatus.Retire.GetDescription().Contains(cardStatus))
                return 2;
            else if (CardStatus.Lost.GetDescription().Contains(cardStatus))
                return 3;
            else if (CardStatus.InValid.GetDescription().Contains(cardStatus))
                return 4;
            return -1;
        }


        public List<CardModel> GetCardListByUserId(int userId)
        {
            var cards = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), userId);
            var cardModelList = new List<CardModel>();
            foreach (var card in cards)
            {
                var cardModel = Mapper.Map<CardModel>(card);
                if (cardModel.CardType == (short)CardType.PassCode)
                {
                    cardModel.CardId = Constants.DynamicQr.PassCodeString;
                }
                cardModelList.Add(cardModel);
            }
            return cardModelList;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public ResultImported ImportFile(string type, IFormFile file)
        {
            var data = new List<UserImportExportModel>();
            var fileByte = LoadUsersFromExcelFile(file, data);

            ////Save system log
            //var companyId = _httpContext.User.GetCompanyId();
            //var content = string.Format(UserResource.msgImportUserList, DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss"),
            //    _httpContext.User.Identity.Name);
            //_unitOfWork.SystemLogRepository.Add(companyId, SystemLogType.User, ActionLogType.Import, content);
            //_unitOfWork.Save();

            //return fileByte;
            return fileByte;
        }
        /// <summary>
        /// Import file to DB
        /// </summary>
        /// <param name="listImportUser"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public ResultImported Import(List<UserImportExportModel> listImportUser, string fileName)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var validUsers = listImportUser.ToList();

            if (validUsers.Any())
            {
                //Add()
                var result = Add(validUsers, companyId);
                return result;
            }

            var res = new ResultImported
            {
                Result = false,
                Message = MessageResource.NoContentInFile
            };

            return res;
        }

        ///// <summary>
        ///// Load user from text file
        ///// </summary>
        ///// <param name="filePath"></param>
        ///// <param name="datas"></param>
        public ResultImported LoadUsersFromTextFile(IFormFile file, List<UserImportExportModel> data)
        {
            try
            {
                var csvfilerecord = FileHelpers.ConvertToStringArray(file);
                foreach (var row in csvfilerecord.Skip(1))
                {
                    if (string.IsNullOrEmpty(row) || row.Equals("\r")) continue;
                    var item = ReadDataFromCsv(row);
                    data.Add(item);
                }
                return Import(data, file.FileName);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                throw;
            }
        }

        ///// <summary>
        ///// Load users from excel file
        ///// </summary>
        ///// <param name="filePath"></param>
        ///// <param name="datas"></param>
        public ResultImported LoadUsersFromExcelFile(IFormFile file, List<UserImportExportModel> data)
        {
            try
            {
                using (var package = new ExcelPackage(FileHelpers.ConvertToStream(file)))
                {
                    ExcelWorksheet worksheet;
                    int columnCount;
                    try
                    {
                        worksheet = package.Workbook.Worksheets[1];
                        columnCount = worksheet.Dimension.End.Column;
                    }
                    catch (Exception)
                    {
                        throw new InvalidFormatException();
                    }

                    for (int i = worksheet.Dimension.Start.Row + 1; i <= worksheet.Dimension.End.Row; i++)
                    {
                        var cells = worksheet.Cells;
                        var cardStatus = Convert.ToString(cells[i, _header.IndexOf(UserResource.lblCardStatus) + 1].Value ?? "");
                        var cardId = Convert.ToString(cells[i, _header.IndexOf(UserResource.lblCardId) + 1].Value ?? "");

                        if (cardStatus.Equals("") || cardId.Equals(""))
                            continue;

                        var item = ReadDataFromExcel(worksheet, i);
                        data.Add(item);
                    }
                }

                var fileName = file.FileName;
                var result = Import(data, fileName);
                _unitOfWork.Save();
                return result;
            }
            catch (Exception e)
            {
                _logger.LogError($"{e.Message}:{Environment.NewLine} {e.StackTrace}");
                throw;
            }
        }

        ///// <summary>
        ///// Read data from Excel
        ///// </summary>
        ///// <param name="worksheet"></param>
        ///// <param name="row"></param>
        ///// <returns></returns>
        private UserImportExportModel ReadDataFromExcel(ExcelWorksheet worksheet, int row)
        {
            var colIndex = 1;
            var cells = worksheet.Cells;
            var model = new UserImportExportModel();
            model.SetUserCode(Convert.ToString(cells[row, colIndex++].Value));

            model.SetIsMasterCard(Convert.ToString(cells[row, colIndex++].Value));
            model.SetCardStatus(Convert.ToString(cells[row, colIndex++].Value));
            model.SetCardType(Convert.ToString(cells[row, colIndex++].Value));
            model.SetCardId(Convert.ToString(cells[row, colIndex++].Value));
            model.SetFirstName(Convert.ToString(cells[row, colIndex++].Value));
            model.SetLastName(Convert.ToString(cells[row, colIndex++].Value));
            model.SetSex(Convert.ToString(cells[row, colIndex++].Value));
            model.SetEmail(Convert.ToString(cells[row, colIndex++].Value));
            model.SetBirthdayDate(Convert.ToString(cells[row, colIndex++].Value));
            model.SetEffectiveDate(cells[row, colIndex++].Value.ToString());
            model.SetExpiredDate(Convert.ToString(cells[row, colIndex++].Value));
            model.SetDepartmentNo(Convert.ToString(cells[row, colIndex++].Value));
            model.SetEmployeeNumber(Convert.ToString(cells[row, colIndex++].Value));
            model.SetPosition(Convert.ToString(cells[row, colIndex++].Value));
            model.SetPostCode(Convert.ToString(cells[row, colIndex++].Value));
            model.SetJob(Convert.ToString(cells[row, colIndex++].Value));
            model.SetResponsibility(Convert.ToString(cells[row, colIndex++].Value));
            model.SetCompanyPhone(Convert.ToString(cells[row, colIndex++].Value));
            model.SetHomePhone(Convert.ToString(cells[row, colIndex++].Value));
            model.SetAddress(Convert.ToString(cells[row, colIndex++].Value));
            model.SetNationality(Convert.ToString(cells[row, colIndex++].Value));
            model.SetCity(Convert.ToString(cells[row, colIndex++].Value));
            model.SetRemarks(Convert.ToString(cells[row, colIndex].Value));
            return model;
        }

        //private int GetAccessGroupId(string accessGroupName)
        //{
        //    worksheet.Cells[recordIndex, colIndex++].Value = user.AccessGroupId < 3 ? ((AccessGroupType)user.AccessGroupId).GetDescription() : accessGroup.Name;
        //}

        /// <summary>
        /// Upload file to server
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string UploadFile(IFormFile file)
        {
            if (file.Length <= 0) return null;
            var fullPath = GetOkFilePath(_httpContext.User.GetCompanyCode(), _httpContext.User.GetAccountId(),
                Path.GetExtension(file.FileName));
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }

            return fullPath;
        }

        /// <summary>
        /// Get OK file path
        /// </summary>
        /// <param name="companyCode"></param>
        /// <param name="accountId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public string GetOkFilePath(string companyCode, int accountId, string type)
        {
            var ngFolder = Path.Combine(Constants.UploadDir, Constants.Ok);
            var webRootPath = _hostingEnvironment.WebRootPath;
            var newPath = Path.Combine(webRootPath, ngFolder);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            var fullPath = Path.Combine(newPath, $"{companyCode}_{accountId}{type}");
            return fullPath;
        }

        ///// <summary>
        ///// Update a NG file
        ///// </summary>
        ///// <param name="byteArray"></param>
        ///// <param name="companyCode"></param>
        ///// <param name="accountId"></param>
        ///// <returns></returns>
        public string UploadNgFile(byte[] byteArray, string companyCode, int accountId)
        {
            if (byteArray.Length <= 0) return null;
            var fullPath = GetNgFilePath(companyCode, accountId);
            using (var stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write))
            {
                stream.Write(byteArray, 0, byteArray.Length);
                return fullPath;
            }
        }

        ///// <summary>
        ///// Get NG file path
        ///// </summary>
        ///// <param name="companyCode"></param>
        ///// <param name="accountId"></param>
        ///// <returns></returns>
        public string GetNgFilePath(string companyCode, int accountId)
        {
            var ngFolder = Path.Combine(Constants.UploadDir, Constants.Ng);
            var webRootPath = _hostingEnvironment.WebRootPath;
            var newPath = Path.Combine(webRootPath, ngFolder);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            var fullPath = Path.Combine(newPath, $"{companyCode}_{accountId}.xlsx");
            return fullPath;
        }

        ///// <summary>
        ///// Add valide users to database and export invalid users to excel file.
        ///// </summary>
        ///// <param name="validUsers"></param>
        ///// <param name="invalidUsers"></param>
        ///// <param name="companyId"></param>
        private ResultImported Add(List<UserImportExportModel> validUsers, int companyId)
        {
            var workingType = _unitOfWork.WorkingRepository.GetWorkingTypeDefault(companyId);
            var result = new ResultImported
            {
                Result = false,
                Message = MessageResource.NotFoundDepartment
            };
            var plugin = _unitOfWork.AppDbContext.PlugIn.Single(p => p.CompanyId == companyId).PlugIns;
            var qrPlugin = JsonConvert.DeserializeObject<PlugIns>(plugin);
            var emailRequired = qrPlugin.QrCode;

            if (emailRequired)
            {
                foreach (var importedUser in validUsers)
                {
                    if (string.IsNullOrEmpty(importedUser.Email.Value))
                    {
                        result.Result = false;
                        result.Message = MessageResource.RequiredEmail;
                        return result;
                    }
                }
            }

            var count = 0;
            var processedUserCode = new List<string>();

            foreach (var importedUser in validUsers)
            {
                var department = _unitOfWork.AppDbContext.Department.FirstOrDefault(d => d.CompanyId == companyId && d.DepartNo == importedUser.DepartmentNo.Value && !d.IsDeleted);
                if (department == null)
                {
                    result.Result = false;
                    result.Message = String.Format(MessageResource.DepartmentNumberNotFound);
                    return result;
                }
            }

            foreach (var importedUser in validUsers)
            {
                // Check data excel department 
                if (importedUser.DepartmentNo != null)
                {
                    var department = _unitOfWork.AppDbContext.Department.FirstOrDefault(d => d.CompanyId == companyId && d.DepartNo == importedUser.DepartmentNo.Value && !d.IsDeleted);
                    var accessGroup = _unitOfWork.AppDbContext.AccessGroup.Single(
                        d => d.CompanyId == companyId && d.Type == 1 && !d.IsDeleted);

                    // Check department is not null
                    if (department != null)
                    {
                        var user = _unitOfWork.UserRepository.GetByUserCode(_httpContext.User.GetCompanyId(), importedUser.UserCode.Value);
                        // Check user is null will be created
                        if (user == null)
                        {
                            if (string.IsNullOrEmpty(importedUser.Email.Value))
                            {
                                var newUser = Mapper.Map<User>(importedUser);
                                newUser.DepartmentId = department.Id;
                                newUser.AccessGroupId = accessGroup.Id;
                                newUser.CompanyId = companyId;
                                newUser.BirthDay = DateTime.Now;
                                newUser.FirstName = importedUser.FirstName.Value;
                                newUser.WorkingTypeId = workingType;

                                _unitOfWork.UserRepository.Add(newUser);
                                _unitOfWork.Save();

                                if (!string.IsNullOrEmpty(importedUser.CardId.Value))
                                {
                                    String[] strListCard = importedUser.CardId.Value.Split(",");
                                    if (strListCard.Length > 1)
                                    {
                                        foreach (var cardId in strListCard)
                                        {
                                            CreatedCardWithNewUser(newUser, importedUser, companyId, cardId);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine(importedUser.CardId.Value);
                                        CreatedCardWithNewUser(newUser, importedUser, companyId, importedUser.CardId.Value);
                                    }
                                }
                                count = count + 1;

                            }
                            else if (EmailIsValid(importedUser.Email.Value))
                            {
                                // Check whether account has already been created in system.
                                var account = _unitOfWork.AppDbContext.Account.FirstOrDefault(a => a.Username == importedUser.Email.Value);
                                if (account == null)
                                {
                                    var newAccount = new Account
                                    {
                                        Username = importedUser.Email.Value,
                                        CompanyId = companyId,
                                        Type = (short)AccountType.Employee,
                                        Password = Helpers.GenerateRandomPassword(),
                                        IsDeleted = false
                                    };
                                    _unitOfWork.AccountRepository.Add(newAccount);
                                    _unitOfWork.Save();

                                    var newUser = Mapper.Map<User>(importedUser);
                                    newUser.DepartmentId = department.Id;
                                    newUser.AccessGroupId = accessGroup.Id;
                                    newUser.CompanyId = companyId;
                                    newUser.BirthDay = DateTime.Now;
                                    newUser.Email = importedUser.Email.Value;
                                    newUser.AccountId = newAccount.Id;
                                    newUser.FirstName = importedUser.FirstName.Value;
                                    newUser.WorkingTypeId = workingType;
                                    newUser.PermissionType = newAccount.Type;

                                    _unitOfWork.UserRepository.Add(newUser);
                                    _unitOfWork.Save();

                                    // send welcome email to employee user
                                    var token = _accountService.GetTokenByAccount(newAccount);
                                    _accountService.SendWelcomeMail(newAccount.Username, newUser.FirstName, token, companyId);

                                    if (!string.IsNullOrEmpty(importedUser.CardId.Value))
                                    {
                                        String[] strListCard = importedUser.CardId.Value.Split(",");
                                        if (strListCard.Length > 1)
                                        {
                                            foreach (var cardId in strListCard)
                                            {
                                                CreatedCardWithNewUser(newUser, importedUser, companyId, cardId);
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine(importedUser.CardId.Value);
                                            CreatedCardWithNewUser(newUser, importedUser, companyId, importedUser.CardId.Value);
                                        }
                                    }
                                    count = count + 1;

                                }
                                else
                                {
                                    var userAccount = _unitOfWork.AppDbContext.User.Where(u =>
                                        u.CompanyId == companyId && u.AccountId == account.Id && !u.IsDeleted);
                                    // Check account Already created in company
                                    if (account.CompanyId == companyId)
                                    {
                                        // Check account already link to user
                                        if (!userAccount.Any())
                                        {
                                            var newUser = Mapper.Map<User>(importedUser);
                                            newUser.DepartmentId = department.Id;
                                            newUser.AccessGroupId = accessGroup.Id;
                                            newUser.CompanyId = companyId;
                                            newUser.BirthDay = DateTime.Now;
                                            newUser.Email = importedUser.Email.Value;
                                            newUser.AccountId = account.Id;
                                            newUser.WorkingTypeId = workingType;
                                            _unitOfWork.UserRepository.Add(newUser);

                                            if (!string.IsNullOrEmpty(importedUser.CardId.Value))
                                            {
                                                String[] strListCard = importedUser.CardId.Value.Split(",");
                                                if (strListCard.Length > 1)
                                                {
                                                    foreach (var cardId in strListCard)
                                                    {
                                                        CreatedCardWithNewUser(newUser, importedUser, companyId, cardId);
                                                    }
                                                }
                                                else
                                                {
                                                    CreatedCardWithNewUser(newUser, importedUser, companyId, importedUser.CardId.Value);
                                                }
                                            }
                                        }
                                        count = count + 1;
                                    }
                                }
                            }
                        }
                    }
                }


                processedUserCode.Add(importedUser.UserCode.Value);
                _unitOfWork.Save();
            }



            var accessGroups = _unitOfWork.AccessGroupRepository.GetListAccessGroups(companyId);
            foreach (var accessGroup in accessGroups)
            {
                var devices = _unitOfWork.IcuDeviceRepository.GetDevicesByAccessGroup(companyId, accessGroup.Id).ToList();
                foreach (var device in devices)
                {
                    //var groupMsgId = Guid.NewGuid().ToString();

                    //Save delete all user
                    _deviceService.SendDeviceInstruction(device, Constants.CommandType.DeleteAllUsers);

                    //Save assign user
                    var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByIcuId(_httpContext.User.GetCompanyId(),
                        device.Id).ToList();

                    //var remainCount = agDevices.Count();

                    if (agDevices.Any())
                    {
                        foreach (var agDevice in agDevices)
                        {
                            _accessGroupService.SendAddOrDeleteUser(agDevice, null, isAddUser: true);
                        }
                    }
                }
            }
            if (count == validUsers.Count)
            {
                result.Result = true;
                result.Message = String.Format(MessageResource.ImportSuccess, count);
            }
            else
            {
                result.Message = String.Format(MessageResource.ImportError, count, validUsers.Count - count);
            }

            return result;
        }

        private void CreatedCardWithNewUser(User user, UserImportExportModel importedUser, int companyId, string cardId)
        {
            var card = _unitOfWork.CardRepository.GetByCardId(companyId, importedUser.CardId.Value);
            if (card == null)
            {
                Console.WriteLine(user);

                var newCard = new Card()
                {
                    CompanyId = companyId,
                    CardId = cardId,
                    CardStatus = (short)(importedUser.CardStatus.Value ?? 1),
                    UserId = user.Id,
                    CardType = (short)CardType.NFC,
                };


                _unitOfWork.CardRepository.Add(newCard);

            }
        }

        private bool EmailIsValid(string email)
        {
            string expression = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";

            if (Regex.IsMatch(email, expression))
            {
                if (Regex.Replace(email, expression, string.Empty).Length == 0)
                {
                    return true;
                }
            }
            return false;
        }

        ///// <summary>
        ///// Assign deparment to an user
        ///// </summary>
        ///// <param name="departments"></param>
        ///// <param name="item"></param>
        ///// <param name="user"></param>
        ///// <param name="companyId"></param>
        ///// <param name="rootDepartment"></param>
        private void AssignDepartment(IQueryable<Department> departments,
            UserImportExportModel item, User user,
            int companyId, Department rootDepartment)
        {
            var department = departments.FirstOrDefault(m =>
                m.DepartName == item.DepartmentName.Value);
            if (department != null)
            {
                user.DepartmentId = department.Id;
            }
            else
            {
                department = new Department
                {
                    DepartName = item.DepartmentName.Value,
                    DepartNo = item.DepartmentNo.Value,
                    CompanyId = companyId,
                    IsDeleted = false,
                    ParentId = rootDepartment?.Id,
                    CreatedBy = _httpContext.User.GetAccountId(),
                    CreatedOn = DateTime.UtcNow,
                    UpdatedBy = _httpContext.User.GetAccountId(),
                    UpdatedOn = DateTime.UtcNow
                };
                user.DepartmentId = department.Id;
                user.Department = department;
            }

            user.CompanyId = companyId;
        }

        /// <summary>
        /// Export invalid data to file
        /// </summary>
        /// <param name="users"></param>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public byte[] ExportErrorData(List<UserImportExportModel> users, string loginName)
        {
            byte[] result;
            using (var package = new ExcelPackage())
            {
                // add a new worksheet to the empty workbook
                var worksheet =
                    package.Workbook.Worksheets.Add(Constants.WorkSheetName); //Worksheet name

                //First add the headers
                for (var i = 0; i < _header.Length; i++)
                {
                    worksheet.Cells[1, i + 1].Value = _header[i];
                }

                //Add values
                var recordIndex = 2;
                foreach (var user in users)
                {
                    var colIndex = 1;
                    ExportStringHandler(user.AccessGroupName, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.IssueCount, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.IsMasterCard, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.CardStatus, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.CardId, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.FirstName, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.LastName, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Sex, worksheet, recordIndex, colIndex++, loginName);
                    //ExportDateHandler(user.IssuedDate, worksheet, recordIndex, colIndex++, loginName);
                    ExportDateHandler(user.EffectiveDate, worksheet, recordIndex, colIndex++, loginName);
                    ExportDateHandler(user.ExpiredDate, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.DepartmentName, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.DepartmentNo, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.EmployeeNumber, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Position, worksheet, recordIndex, colIndex++, loginName);
                    //ExportStringHandler(user.KeyPadPassword, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.PostCode, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Job, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Responsibility, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.CompanyPhone, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.HomePhone, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Address, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Nationality, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.City, worksheet, recordIndex, colIndex++, loginName);
                    ExportStringHandler(user.Remarks, worksheet, recordIndex, colIndex, loginName);
                    recordIndex++;
                }

                result = package.GetAsByteArray();
            }

            return result;
        }

        private void ExportStringHandler<T>(Field<T> field, ExcelWorksheet worksheet,
            int recordIndex, int colIndex, string loginName)
        {
            worksheet.Cells[recordIndex, colIndex].Value = field.ToString();
            if (!field.IsValid)
            {
                worksheet.Cells[recordIndex, colIndex].Style.Fill.PatternType =
                    ExcelFillStyle.Solid;
                worksheet.Cells[recordIndex, colIndex].Style.Fill.BackgroundColor
                    .SetColor(Color.Yellow);
                worksheet.Cells[recordIndex, colIndex].AddComment(field.Error, loginName);
            }
        }

        /// <summary>
        /// Export invalid field with description
        /// </summary>
        /// <param name="field"></param>
        /// <param name="worksheet"></param>
        /// <param name="recordIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="loginName"></param>
        private void ExportDateHandler(Field<DateTime?> field, ExcelWorksheet worksheet,
            int recordIndex, int colIndex, string loginName)
        {
            worksheet.Cells[recordIndex, colIndex].Value = field.Value?.ToOADate();
            worksheet.Cells[recordIndex, colIndex].Style.Numberformat.Format = Constants.DateTimeFormat.YyyyMMdd;
            if (!field.IsValid)
            {
                worksheet.Cells[recordIndex, colIndex].Value = field.ToString();
                worksheet.Cells[recordIndex, colIndex].Style.Fill.PatternType =
                    ExcelFillStyle.Solid;
                worksheet.Cells[recordIndex, colIndex].Style.Fill.BackgroundColor
                    .SetColor(Color.Yellow);
                worksheet.Cells[recordIndex, colIndex].AddComment(field.Error, loginName);
            }
        }

        /// <summary>
        /// Check whether if card id is existed in the company
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsCardIdExist(UserModel model)
        {
            return _unitOfWork.CardRepository.IsCardIdExist(_httpContext.User.GetCompanyId(), model.CardList);
        }

        /// <summary>
        /// Check whether user has this card
        /// </summary>
        /// <param name="userId">user index number</param>
        /// <param name="cardId">index number of card, not cardId</param>
        /// <returns> If user has the card, true. 
        ///           If user doesn't have the card, false. </returns>
        public bool IsExistGetCardByUser(int userId, int cardId)
        {
            return _unitOfWork.CardRepository.IsCardIdByUserIdExist(_httpContext.User.GetCompanyId(), userId, cardId);
        }

        /// <summary>
        /// Check whether if card id is existed in the company
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public bool IsCardIdExist(string cardId)
        {
            return _unitOfWork.CardRepository.IsCardIdExist(_httpContext.User.GetCompanyId(), cardId);
        }

        /// <summary>
        /// Get by cardid include department
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public User GetByCardIdIncludeDepartment(string cardId)
        {
            return _unitOfWork.UserRepository.GetByCardIdIncludeDepartment(_httpContext.User.GetCompanyId(), cardId);
        }
        /// <summary>
        /// Check whether keypad password is existed in the company
        /// </summary>
        /// <param name="userId"></param>
        /// 
        /// <param name="enctypedKeyPadPassword"></param>
        /// <returns></returns>
        public bool IsKeyPadPasswordExist(int userId, string enctypedKeyPadPassword)
        {
            return _unitOfWork.UserRepository.IsKeyPadPasswordExist(_httpContext.User.GetCompanyId(), userId,
                enctypedKeyPadPassword);
        }

        /// <summary>
        /// Change users to another department
        /// </summary>
        /// <param name="users"></param>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public bool ChangeDepartment(List<User> users, int departmentId)
        {
            var isSuccess = true;
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var department =
                            _unitOfWork.DepartmentRepository.GetByIdAndCompanyId(departmentId,
                                _httpContext.User.GetCompanyId());
                        if (department != null)
                        {
                            foreach (var user in users)
                            {
                                user.DepartmentId = departmentId;
                                _unitOfWork.UserRepository.Update(user);
                            }

                            //Save system log
                            //var userIds = users.Select(c => c.Id).ToList();
                            ////var userCodes = users.Select(c => c.UserCode).ToList();
                            //var content = $"{ActionLogTypeResource.MoveDepartment}: {department.DepartName}";
                            //var contentDetails = $"{UserResource.lblUserCode}: {string.Join(", ", userCodes)}";
                            //_unitOfWork.SystemLogRepository.Add(departmentId, SystemLogType.User,
                            //    ActionLogType.MoveDepartment, content, contentDetails, userIds);

                            _unitOfWork.Save();
                            transaction.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        _logger.LogError($"{ex.Message}:{Environment.NewLine} {ex.StackTrace}");
                        isSuccess = false;
                    }
                }
            });
            return isSuccess;
        }


        ///// <summary>
        ///// Read txt file
        ///// </summary>
        ///// <param name="csvLine"></param>
        ///// <returns></returns>
        private UserImportExportModel ReadDataFromCsv(string csvLine)
        {
            var values = csvLine.Split(',');
            var columnCount = values.Length;
            if (columnCount != _header.Length + 1)
            {
                throw new InvalidFormatException();
            }

            var colIndex = 0;
            var model = new UserImportExportModel();

            model.SetUserCode(values[colIndex++]);

            var defaultAccessGroup =
                _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(_httpContext.User.GetCompanyId());

            if (Convert.ToString(values[colIndex]).Equals(""))
            {
                model.SetAccessGroupName(defaultAccessGroup.Name);
                model.SetAccessGroupId(defaultAccessGroup.Id.ToString());
            }
            else
            {
                var accessGroupName = Convert.ToString(values[colIndex]);
                var accessGroupId = 0;
                switch (accessGroupName)
                {
                    case Constants.AccessGroup.FullAccess_EN:
                    case Constants.AccessGroup.FullAccess_KR:
                    case Constants.AccessGroup.FullAccess_JP:
                        accessGroupId = 1;
                        break;
                    case Constants.AccessGroup.NoAccess_EN:
                    case Constants.AccessGroup.NoAccess_KR:
                    case Constants.AccessGroup.NoAccess_JP:
                        accessGroupId = 2;
                        break;
                    default:
                        accessGroupId = _unitOfWork.AccessGroupRepository.GetByNameAndCompanyId(_httpContext.User.GetCompanyId(), model.AccessGroupName.Value).Id;
                        break;
                }

                //model.SetAccessGroupName(Convert.ToString(cells[row, colIndex].Value));
                //model.SetAccessGroupId(accessGroupId.ToString());



                model.SetAccessGroupName(Convert.ToString(values[colIndex]));
                //var accessGroup = _unitOfWork.AccessGroupRepository.GetByNameAndCompanyId(_httpContext.User.GetCompanyId(), model.AccessGroupName.Value);
                model.SetAccessGroupId(accessGroupId.ToString());

            }

            colIndex++;


            model.SetIssueCount(Convert.ToString(values[colIndex++]));
            model.SetIsMasterCard(Convert.ToString(values[colIndex++]));
            model.SetCardStatus(Convert.ToString(values[colIndex++]));
            model.SetCardType(Convert.ToString(values[colIndex++]));
            model.SetCardId(Convert.ToString(values[colIndex++]));
            model.SetFirstName(Convert.ToString(values[colIndex++]));
            model.SetLastName(Convert.ToString(values[colIndex++]));
            model.SetSex(Convert.ToString(values[colIndex++]));


            var effectiveDate = Convert.ToString(values[colIndex++]);
            try
            {
                var effectiveDateValue = DateTime.ParseExact(effectiveDate, Constants.DateTimeFormat.YyyyMMdd,
                        CultureInfo.InvariantCulture)
                    .ToOADate().ToString(CultureInfo.InvariantCulture);
                model.SetEffectiveDate(effectiveDateValue);
            }
            catch
            {
                model.SetEffectiveDate(effectiveDate);
            }

            var expiredDate = Convert.ToString(values[colIndex++]);
            try
            {
                string expiredDateValue = null;
                if (!string.IsNullOrEmpty(expiredDate))
                {
                    expiredDateValue = DateTime.ParseExact(expiredDate, Constants.DateTimeFormat.YyyyMMdd,
                            CultureInfo.InvariantCulture)
                        .ToOADate().ToString(CultureInfo.InvariantCulture);
                }

                model.SetExpiredDate(expiredDateValue);
            }
            catch
            {
                model.SetExpiredDate(expiredDate);
            }
            model.SetDepartment(Convert.ToString(values[colIndex++]));
            model.SetDepartmentNo(Convert.ToString(values[colIndex++]));
            model.SetEmployeeNumber(Convert.ToString(values[colIndex++]));
            model.SetPosition(Convert.ToString(values[colIndex++]));
            //model.SetKeyPadPassword(Convert.ToString(values[colIndex++]));
            model.SetPostCode(Convert.ToString(values[colIndex++]));
            model.SetJob(Convert.ToString(values[colIndex++]));
            model.SetResponsibility(Convert.ToString(values[colIndex++]));
            model.SetCompanyPhone(Convert.ToString(values[colIndex++]));
            model.SetHomePhone(Convert.ToString(values[colIndex++]));
            model.SetAddress(Convert.ToString(values[colIndex++]));
            model.SetNationality(Convert.ToString(values[colIndex++]));
            model.SetCity(Convert.ToString(values[colIndex++]));
            model.SetRemarks(Convert.ToString(values[colIndex]));
            return model;
        }

        /// <summary>
        /// Generate test data for user
        /// </summary>
        /// <param name="numberOfUser"></param>
        public void GenerateTestData(int numberOfUser)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var defaultAccessGroup =
                _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(companyId);

            for (var i = 0; i < numberOfUser; i++)
            {
                var fakeUser = new Faker<User>()
                    .RuleFor(u => u.CompanyId, f => companyId)
                    .RuleFor(u => u.AccessGroupId, f => defaultAccessGroup.Id)
                    //.RuleFor(u => u.IssueCount, f => 0)
                    .RuleFor(u => u.IsMasterCard, f => false)
                    //.RuleFor(u => u.CardStatus, f => (short)CardStatus.Normal)
                    .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                    .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                    .RuleFor(u => u.KeyPadPw, f => Encryptor.Encrypt(f.Random.Replace("########"), _configuration[Constants.Settings.EncryptKey]))
                    //.RuleFor(u => u.CardId, f => f.Random.Replace("****************"))
                    .RuleFor(u => u.DepartmentId, f => 1)
                    .RuleFor(u => u.EffectiveDate, f => DateTime.UtcNow)
                    .RuleFor(u => u.IssuedDate, f => DateTime.UtcNow)
                    .RuleFor(u => u.ExpiredDate, f => DateTime.UtcNow.AddMonths(12));

                var user = fakeUser.Generate();
                _unitOfWork.UserRepository.Add(user);
            }
            _unitOfWork.Save();
        }

        /// <summary>
        /// Send add user when add new user
        /// </summary>
        public void SendUpdateUsersToAllDoors(User user, bool isAddUser)
        {
            if (user == null)
            {
                _logger.LogError("Can not find this user in system");
                return;
            }

            var agDevices =
                _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(),
                    user.AccessGroupId);
            //var userList = new List<User>();

            if (agDevices != null && agDevices.Any())
            {
                //var remainCount = agDevices.Count();

                foreach (var agDevice in agDevices)
                {
                    _accessGroupService.SendAddOrDeleteUser(agDevice, new List<User> { user }, isAddUser: isAddUser);
                }
            }
        }

        /// <summary>
        /// Send Identification to device that has specific user
        /// </summary>
        private void SendIdentiToDevice(User user, Card newCard, bool isAdd)
        {
            if (user == null)
            {
                _logger.LogError("Can not find this user in system");
                return;
            }

            var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(), user.AccessGroupId);

            if (agDevices != null && agDevices.Any())
            {
                foreach (var agDevice in agDevices)
                {
                    _accessGroupService.SendIdentificationToDevice(agDevice, user, newCard, isAdd);
                }
            }
        }

        public byte[] EncryptQrCode(string companySecretCode, string qrId)
        {
            RijndaelManaged aes = new RijndaelManaged
            {
                BlockSize = Constants.AutoRenew.BlockSize,
                KeySize = Constants.AutoRenew.KeySize,
                Mode = CipherMode.ECB
            };

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData genQrCode =
                qrGenerator.CreateQrCode(qrId,
                    QRCodeGenerator.ECCLevel.Q);
            PngByteQRCode qrCode = new PngByteQRCode(genQrCode);
            byte[] qrCodeAsPngByteArr = qrCode.GetGraphic(20);
            return qrCodeAsPngByteArr;
        }

        //public Card AddCardByUser(int userId, CardModel model)
        //{
        //    var user = _unitOfWork.UserRepository.GetById(userId);
        //    var card = new Card()
        //    {
        //        CompanyId = _httpContext.User.GetCompanyId(),
        //        CardId = model.CardId,
        //        IssueCount = model.IssueCount,
        //        CardStatus = model.CardStatus,
        //        UserId = userId,
        //        CardType = model.CardType,
        //    };

        //    if (model.CardType != (short) CardType.QrCode)
        //    {
        //        card.CardId = model.CardId;
        //    }
        //    else
        //    {
        //        string qrId = GenQrId();
        //        card.CardId = qrId;
        //        var company =
        //            _unitOfWork.CompanyRepository.GetById(_httpContext.User.GetCompanyId());
        //        var companyLanguage =
        //            _unitOfWork.SettingRepository.GetLanguage(_httpContext.User.GetCompanyId());
        //        var culture = new CultureInfo(Helpers.GetStringFromValueSetting(companyLanguage.Value));
        //        byte[] qrCodeAsPngByteArr = EncryptQrCode(company.SecretCode, qrId);
        //        using (var ms = new MemoryStream(qrCodeAsPngByteArr))
        //        {
        //            Image qrCodeImage = new Bitmap(ms);
        //            var stream = new MemoryStream();
        //            qrCodeImage.Save(stream, ImageFormat.Jpeg);
        //            stream.Position = 0;
        //            // Send qr code to user by email
        //            if (!string.IsNullOrEmpty(user.Email))
        //            {
        //                _mailService.SendMail(user.Email, null,
        //                    string.Format(SettingResource.ResourceManager.GetString("gen_qr_code_for_user", culture)),
        //                    string.Format(SettingResource.ResourceManager.GetString("gen_qr_code_for_user_content", culture).Replace("\\n", "<br>"),
        //                        user.FirstName, 
        //                        company.Name), 
        //                    stream, "QRCode", "image/jpg"
        //                );
        //            }
        //        }
        //    }

        //    _unitOfWork.CardRepository.Add(card);
        //    _unitOfWork.Save();
        //    SendNewCardToDevice(user, card);
        //    return card;
        //}

        /// <summary>
        /// Add new identification to exist user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cardModel"></param>
        /// <returns></returns>
        public Card AddIdentification(User user, CardModel cardModel)
        {
            var companyId = _httpContext.User.GetCompanyId();

            var card = new Card();

            var contentsDetails = "";

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        card = new Card()
                        {
                            CardId = cardModel.CardId,
                            CardStatus = cardModel.CardStatus,
                            IssueCount = cardModel.IssueCount,
                            CardType = cardModel.CardType,
                            Note = cardModel.Description,
                            UserId = user.Id,
                            CompanyId = user.CompanyId,
                            // I added below code, but I think it doesn't need.
                            // Because AccessGroupId, IsMasterCard are only required by the User.
                            // So, I think that deleting these 2 attributes from Card table is better.
                            AccessGroupId = user.AccessGroupId,
                            IsMasterCard = user.IsMasterCard
                        };

                        var identification = "";

                        var checkQrCodeCompany = _unitOfWork.AppDbContext.PlugIn.Where(x => x.CompanyId == user.CompanyId).Select(x => x.PlugIns).FirstOrDefault();
                        var Value = JsonConvert.DeserializeObject<PlugIns>(checkQrCodeCompany);
                        if (cardModel.CardType != (short)CardType.QrCode)
                        {
                            if (cardModel.CardType == (short)CardType.NFC)
                            {
                                identification = "Card";
                                card.CardId = cardModel.CardId;
                            }
                            else if (cardModel.CardType == (short)CardType.PassCode)
                            {
                                identification = "PassCode";
                                card.CardId = Helpers.GenerateRandomPasswordNumber(Constants.DynamicQr.LenPassCode);
                            }

                            contentsDetails = $"{UserResource.lblCardId} : {card.CardId}\n" +
                            $"{UserResource.lblIssueCount} : {card.IssueCount}";
                        }
                        else if (cardModel.CardType == (short)CardType.QrCode || Value.QrCode == true)
                        {
                            identification = "QR";
                            string qrId = GenQrId();
                            card.CardId = qrId;
                        }

                        _unitOfWork.CardRepository.Add(card);

                        //Save system log
                        var content = string.Format(UserResource.msgAddNewCertification, identification, user.UserCode);

                        _unitOfWork.SystemLogRepository.Add(user.Id, SystemLogType.User, ActionLogType.Update,
                            content, contentsDetails, null, user.CompanyId);

                        _unitOfWork.Save();
                        transaction.Commit();

                        // API must send a new card to device that the user is included.
                        SendIdentiToDevice(user, card, true);

                        if (cardModel.CardType == (short)CardType.PassCode)
                        {
                            card.CardId = Constants.DynamicQr.PassCodeString;
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            return card;
        }


        public void AutomationCreateQrCode(User user)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var createdBy = _httpContext.User.GetAccountId();
            var checkQrCodeCompany = _unitOfWork.AppDbContext.PlugIn.Where(x => x.CompanyId == companyId).Select(x => x.PlugIns).FirstOrDefault();
            var Value = JsonConvert.DeserializeObject<PlugIns>(checkQrCodeCompany);
            //var identification = "";
            Card cardModel = new Card();
            if (Value.QrCode == true)
            {
                //identification = "QR";
                string qrId = GenQrId();
                cardModel.CardId = qrId;
                cardModel.CompanyId = companyId;
                cardModel.CreatedOn = DateTime.Now;
                cardModel.CreatedBy = createdBy;
                cardModel.UserId = user.Id;
                cardModel.CardType = (short)CardType.QrCode;
                _unitOfWork.AppDbContext.Card.Add(cardModel);
                _unitOfWork.AppDbContext.SaveChanges();
                // API must send a new card to device that the user is included.
                SendIdentiToDevice(user, cardModel, true);
            }
           
        }

        public void UpdateCardByUser(int userId, int cardId, CardModel model)
        {
            var user = _unitOfWork.UserRepository.GetById(userId);
            var card = _unitOfWork.CardRepository.GetById(cardId);

            card.CompanyId = _httpContext.User.GetCompanyId();
            card.IssueCount = model.IssueCount;
            card.CardStatus = model.CardStatus;
            card.UserId = userId;
            card.CardType = model.CardType;

            if (model.CardType != (short)CardType.QrCode)
            {
                card.CardId = model.CardId;
            }
            else
            {
                string qrId = GenQrId();
                card.CardId = model.CardId;

                var company =
                    _unitOfWork.CompanyRepository.GetById(_httpContext.User.GetCompanyId());
                var companyLanguage =
                    _unitOfWork.SettingRepository.GetLanguage(_httpContext.User.GetCompanyId());
                var culture = new CultureInfo(Helpers.GetStringFromValueSetting(companyLanguage.Value));
                byte[] qrCodeAsPngByteArr = EncryptQrCode(company.SecretCode, qrId);

                using (var ms = new MemoryStream(qrCodeAsPngByteArr))
                {
                    Image qrCodeImage = new Bitmap(ms);
                    var stream = new MemoryStream();
                    qrCodeImage.Save(stream, ImageFormat.Png);
                    stream.Position = 0;
                    // Send qr code to user by email
                    if (!string.IsNullOrEmpty(user.Email))
                    {
                        //_mailService.SendMail(user.Email, null,
                        //    string.Format(SettingResource.ResourceManager.GetString("gen_qr_code_for_user"), culture),
                        //    string.Format(
                        //        SettingResource.ResourceManager.GetString("gen_qr_code_for_user_content", culture)
                        //            .Replace("\\n", "<br>"), 
                        //        user.FirstName, 
                        //        company.Name), 
                        //    stream, "QRCode.png", "image/jpg"
                        //);

                        var subject = string.Format(MailContentResource.ResourceManager.GetString("SubjecSendQRtoUser", culture));
                        var contents = string.Format(MailContentResource.ResourceManager.GetString("BodySendQRtoUser", culture),
                                                        user.FirstName,
                                                        company.Name);

                        _mailService.SendQRMail(user.Email, subject, contents, culture, stream, "QRCode.png", "image/jpg");
                    }
                }
            }
            _unitOfWork.CardRepository.Update(card);
            _unitOfWork.Save();
        }

        /// <summary>
        /// Delete user's card
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cardId">index number of card, not cardId</param>
        public void DeleteCardByUser(User user, int cardId)
        {
            var card = _unitOfWork.CardRepository.GetById(cardId);

            card.IsDeleted = true;

            //_unitOfWork.CardRepository.Delete(card);
            _unitOfWork.CardRepository.Update(card);
            _unitOfWork.Save();

            // TODO
            // send deleted identification to device
            SendIdentiToDevice(user, card, false);
        }

        public string GenQrId()
        {
            var chars = Constants.Settings.CharacterGenQrCode;
            var stringChars = new char[Constants.Settings.LengthCharacterGenQrCode];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);
            if (_unitOfWork.CardRepository.CheckIsExistedQrCode(finalString) != true)
            {
                GenQrId();
            }
            return finalString;
        }

        /// <summary>
        /// Checking if there are any changes.
        /// </summary>
        /// <param name="user">User that contains existing information</param>
        /// <param name="model">Model that contains new information</param>
        /// <param name="changes">List of changes</param>
        /// <returns></returns>
        internal bool CheckChange(User user, UserModel model, ref List<string> changes)
        {
            if (model.Id != 0)
            {
                if (user.AccessGroupId != model.AccessGroupId)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, UserResource.lblAccessGroup, user.AccessGroupId, model.AccessGroupId));
                }

                if (user.IsMasterCard != model.IsMasterCard)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, UserResource.lblIsMasterCard,
                        user.IsMasterCard ? CommonResource.Use : CommonResource.NotUse, model.IsMasterCard ? CommonResource.Use : CommonResource.NotUse));
                }

                if (user.DepartmentId != model.DepartmentId)
                {
                    changes.Add(string.Format(MessageResource.msgChangeInfo, UserResource.lblDepartment,
                        _unitOfWork.DepartmentRepository.GetById(user.DepartmentId).DepartName,
                        _unitOfWork.DepartmentRepository.GetById(model.DepartmentId ?? 0).DepartName));
                }
            }

            return changes.Count() > 0;
        }

        public string GetDynamicQrCode(int userId, string qrId)
        {
            var company = _unitOfWork.CompanyRepository.GetById(_httpContext.User.GetCompanyId());
            var timeNow = DateTime.Now.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
            var secondNow = timeNow.Substring(timeNow.Length - Constants.DynamicQr.SubStringSeconds);
            // Create plain text
            var plaintText = qrId + "_" +
                             DateTime.Now.AddSeconds(Constants.DynamicQr.TimePeriod).ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss) + "_" + secondNow;
            int len = plaintText.Length;
            var salt = Helpers.GenerateSalt(len);

            var data = plaintText + "_" + salt;

            var key = company.SecretCode;
            var text = EncryptStringToBytes(data, Encoding.UTF8.GetBytes(key),
                Encoding.UTF8.GetBytes(Helpers.ReverseString(Constants.DynamicQr.Key)));

            // Create random text to test 

            return Constants.DynamicQr.NameProject + "_" + text;
        }

        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        static string EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.
            var result = Combine(IV, encrypted);
            return BitConverter.ToString(result).Replace("-", "");
        }

        private static Random random = new Random();

        public void AssignUserToDefaultWorkingTime()
        {
            var users = _unitOfWork.UserRepository.GetAllUserInSystemNoWorkingTime();
            foreach (var user in users)
            {
                var workingTime = _unitOfWork.WorkingRepository.GetWorkingTypeDefault(user.CompanyId);
                user.WorkingTypeId = workingTime;
                _unitOfWork.UserRepository.Update(user);
                _unitOfWork.Save();

            }

        }

        //public void SendVerifyAddAccountMailForUser(string email, string token)
        //{
        //    if (!string.IsNullOrEmpty(email))
        //    {
        //        var fontendURL = _configuration.GetSection("WebApp:Host").Value;
        //        var linkReset = fontendURL + "/reset-password/" + token;
        //        var thread = new Thread(delegate ()
        //        {
        //            var subject = MailContentResource.SubjectResetAccount;
        //            var body = string.Format(MailContentResource.BodyResetPasswordNewAccount
        //                , email
        //                , linkReset);

        //            _mailService.SendMail(email, null
        //                , subject
        //                , body);
        //        });
        //        thread.Start();
        //    }
        //}

        public bool GetQrCodeBelongToUser(int userId)
        {
            return _unitOfWork.CardRepository.GetQrCodeBelongToUser(userId);
        }

        public Account GetAccountByUserName(int companyId, string userName)
        {
            return _unitOfWork.AccountRepository.GetByUserNameAndCompanyId(companyId, userName);
        }

        public User GetAccountAlreadyLinkToUser(int companyId, int accountId)
        {
            return _unitOfWork.UserRepository.GetByAccountId(companyId, accountId);
        }


        public Card GetPassCodeByUser(int userId, int companyId)
        {
            return _unitOfWork.CardRepository.GetPassCode(userId, companyId);
        }

        /// <summary>
        /// Check whether email address is valid or not.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public bool IsEmailValid(string emailAddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailAddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>   Query if 'email' is duplicated. </summary>
        /// <remarks>   Edward, 2020-03-17. </remarks>
        /// <param name="userId">   user index number. </param>
        /// <param name="email">    e-mail address. </param>
        /// <returns>   True if duplicated, false if not. </returns>
        public bool IsDuplicatedEmail(int userId, string email)
        {
            User user = GetByEmail(email);

            if (user != null && user.Id != userId)
            {
                return true;
            }

            return false;
        }

        /// <summary>   Query if 'id' is duplicated user code. </summary>
        /// <remarks>   Edward, 2020-03-27. </remarks>
        /// <param name="userId">   The identifier of user. </param>
        /// <param name="userCode"> The user code. </param>
        /// <returns>   True if duplicated user code, false if not. </returns>
        public bool IsDuplicatedUserCode(int userId, string userCode)
        {
            int companyId = _httpContext.User.GetCompanyId();

            int userCount = _unitOfWork.AppDbContext.User.Where(m => m.Id != userId && m.UserCode == userCode && m.CompanyId == companyId).Count();

            if (userCount == 0)
                return false;
            else
                return true;
        }


        public bool IsDuplicatedAccountCreated(string email)
        {
            var account = _unitOfWork.AppDbContext.Account.FirstOrDefault(a => a.Username == email && !a.IsDeleted);
            if (account != null)
            {
                var userLinkAccount = _unitOfWork.AppDbContext.User.FirstOrDefault(u => u.AccountId == account.Id && !u.IsDeleted);
                if (userLinkAccount != null)
                {
                    return false;
                }

                return true;
            }

            return true;
        }

        public User GetUserByAccountId(int accountId)
        {
            var user = _unitOfWork.AppDbContext.User.Where(u => u.AccountId == accountId && u.CompanyId == _httpContext.User.GetCompanyId() && !u.IsDeleted).FirstOrDefault();
            return user;
        }

        public User GetUserByUserId(int userId)
        {
            var user = _unitOfWork.AppDbContext.User.Where(u => u.Id == userId && u.CompanyId == _httpContext.User.GetCompanyId() && !u.IsDeleted).FirstOrDefault();
            return user;
        }

        public string ValidationDynamicQr(string dynamicQr)
        {
            var message = "";

            var company = _unitOfWork.CompanyRepository.GetById(_httpContext.User.GetCompanyId());

            var companySecretCode = company.SecretCode;

            var data = StringToByteArray(dynamicQr.Remove(0, 32));
            var valid = new ValidationQr();
            try
            {
                var strDecrypt = DecryptStringToBytes(data, Encoding.UTF8.GetBytes(companySecretCode),
                    Encoding.UTF8.GetBytes(Helpers.ReverseString(Constants.DynamicQr.Key)));

                String[] parsData = strDecrypt.Split("_");

                var qrId = parsData[0];
                var dateTimeString = parsData[1];


                CultureInfo provider = CultureInfo.InvariantCulture;
                DateTime expireDate = DateTime.ParseExact(dateTimeString, Constants.DateTimeFormat.DdMMyyyyHHmmss, provider);



                var card = _unitOfWork.AppDbContext.Card.Single(i => i.CardId == qrId &&
                                                                     !i.IsDeleted &&
                                                                     i.CompanyId == _httpContext.User.GetCompanyId() &&
                                                                     i.CardType == (short)CardType.QrCode);

                if (card != null)
                {
                    var user = _unitOfWork.UserRepository.GetByUserId(_httpContext.User.GetCompanyId(), card.UserId.Value);
                    message = expireDate >= DateTime.Now
                        ? string.Format(MessageResource.msgValidationUser, user.FirstName)
                        : MessageResource.msgDynamicQrExpired;
                    return message;
                }

            }
            catch
            {
                valid.Messages = MessageResource.msgUserIsInValid;
            }
            return message;
        }

        private static string DecryptStringToBytes(byte[] inputBytes, byte[] Key, byte[] IV)
        {
            byte[] outputBytes = inputBytes;

            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                string plaintext = string.Empty;

                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream(outputBytes))
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csEncrypt))
                        {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
                return plaintext;
            }
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public bool GetDeviceFromCompany(string rid)
        {
            var device = _unitOfWork.IcuDeviceRepository.GetDeviceByRid(_httpContext.User.GetCompanyId(), rid) != null;
            return device;
        }

        public Card GetQrByUserId(int userId)
        {
            var identification = _unitOfWork.CardRepository.GetQrCode(userId);
            return identification;
        }
    }
}


