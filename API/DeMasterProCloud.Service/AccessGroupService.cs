﻿using AutoMapper;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.Common.Resources;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.AccessGroup;
using DeMasterProCloud.DataModel.AccessGroupDevice;
using DeMasterProCloud.DataModel.Device;
using DeMasterProCloud.DataModel.User;
using DeMasterProCloud.Repository;
using DeMasterProCloud.Service.Protocol;
using DeMasterProCloud.Service.RabbitMq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MoreLinq.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace DeMasterProCloud.Service
{
    /// <summary>
    /// AccessLevel service interface
    /// </summary>
    public interface IAccessGroupService
    {
        IQueryable<AccessGroup> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<AccessGroupDeviceDoor> GetPaginatedForDoors(int accessGroupId, string filter,
            int pageNumber,
            int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<AccessGroupDeviceUnAssignDoor> GetPaginatedForUnAssignDoors(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<Node> GetPaginatedAllDoorsForVisit(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<UserForAccessGroup> GetPaginatedForUsers(int accessGroupId, string filter,
            int pageNumber,
            int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        List<UnAssignUserForAccessGroup> GetPaginatedForUnAssignUsers(int accessGroupId, string filter,
            int pageNumber,
            int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered);

        AccessGroup GetById(int accessGroupId);

        List<AccessGroup> GetByIds(List<int> accessGroupIds);

        List<AccessGroup> GetListAccessGroups();
        List<AccessGroup> GetListAccessGroupsExceptForVisitor();

        void Add(AccessGroupModel model);
        AccessGroup AddForVisitor(AccessGroupModel model);

        void Update(AccessGroupModel model);

        void Delete(AccessGroup accessGroup);

        void DeleteRange(List<AccessGroup> accessLevels);

        string AssignUsers(int accessGroupId, List<int> userIds);

        string AssignDoors(int accessGroupId, AccessGroupAssignDoor model, bool isPublish = true);

        bool HasData(int accessGroupId);

        void ChangeTimezone(int accessGroupId, AccessGroupAssignDoor model);

        void UnAssignUsers(int accessGroupId, List<int> userIds);

        void UnAssignDoors(int accessGroupId, List<int> doorIds);

        bool HasExistName(int accessGroupId, string name);

        string GetNameAccessGroupCouldNotUpdateOrDelete(List<int> accessGroupId);

        void SendDeleteAllCardsForUser(AccessGroupDevice accessGroupDevice, List<User> users = null,
            string groupMsgId = "", bool isPublish = true, bool isSave = true);

        void SendIdentificationToDevice(AccessGroupDevice agDevice, User user, Card card, bool isAdd);
        void SendIdentificationToDeviceVisitor(AccessGroupDevice agDevice, Visit visitor, Card card, bool isAdd);

        List<CardModel> GetCardListByUserId(int userId);

        void SendAddOrDeleteUser(AccessGroupDevice accessGroupDevice, List<User> users = null, bool isAddUser = true, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType = "", int remainCount = 0, decimal progressRange = 0);
        void SendVisitor(AccessGroupDevice accessGroupDevice, bool isAddUser = true, Visit visit = null);
    }

    public class AccessGroupService : IAccessGroupService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;
        private readonly HttpContext _httpContext;
        private readonly IQueueService _queueService;
        private readonly ISettingService _settingService;

        public AccessGroupService(IUnitOfWork unitOfWork,
            IQueueService queueService, IConfiguration configuration,
            ILogger<AccessGroupService> logger, IHttpContextAccessor contextAccessor, ISettingService settingService)
        {
            _unitOfWork = unitOfWork;
            _queueService = queueService;
            _configuration = configuration;
            _logger = logger;
            _httpContext = contextAccessor.HttpContext;
            _settingService = settingService;
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public IQueryable<AccessGroup> GetPaginated(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.AppDbContext.AccessGroup
                .Where(m => m.IsDeleted == false && m.CompanyId == companyId && m.Type != (short)AccessGroupType.VisitAccess);
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                data = data.Where(x =>
                    x.Name.ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.AccessGroup.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.AccessGroup[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data;
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<AccessGroupDeviceDoor> GetPaginatedForDoors(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(companyId, accessGroupId)
                .AsEnumerable<AccessGroupDevice>().Select(Mapper.Map<AccessGroupDeviceDoor>).AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                data = data.Where(x => x.DeviceAddress.ToLower().Contains(filter) ||
                                       x.DoorName.ToLower().Contains(filter) ||
                                       x.Timezone.ToLower().Contains(filter) ||
                                       x.Building.ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.AccessGroupForDoors.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.AccessGroupForDoors[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.ToList();
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<AccessGroupDeviceUnAssignDoor> GetPaginatedForUnAssignDoors(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.IcuDeviceRepository.GetUnAssignDevicesByCompany(companyId, accessGroupId)
                    .AsEnumerable<IcuDevice>().Select(Mapper.Map<AccessGroupDeviceUnAssignDoor>).AsQueryable();
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                data = data.Where(x => x.DeviceAddress.ToLower().Contains(filter) ||
                                       x.DoorName.ToLower().Contains(filter) ||
                                       x.Building.ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.AccessGroupForUnAssignDoors.Length - 1 ? 0 : sortColumn;
            data = data.OrderBy($"{ColumnDefines.AccessGroupForUnAssignDoors[sortColumn]} {sortDirection}");
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.ToList();
        }

        /// <summary>
        /// Get unassignDoor data with pagination for visit
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<Node> GetPaginatedAllDoorsForVisit(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var companyId = _httpContext.User.GetCompanyId();
            var data = _unitOfWork.IcuDeviceRepository.GetActiveDevicesByCompany(companyId);

            totalRecords = data.Count();

            var treeData = _unitOfWork.IcuDeviceRepository.GetAGDeviceHierarchy(data.ToList(), accessGroupId);

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();
                treeData = treeData.Where(x => x.BuildingName.ToLower().Contains(filter));
            }

            if(treeData != null)
            {
                recordsFiltered = treeData.Count();
                treeData = treeData.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            }
            else
            {
                recordsFiltered = 0;
                treeData = new List<Node>();
            }
            
            return treeData.ToList();
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<UserForAccessGroup> GetPaginatedForUsers(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var data = _unitOfWork.UserRepository.GetAssignUsers(_httpContext.User.GetCompanyId(), accessGroupId);
            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();

                var userIdsFilteredByCardId = _unitOfWork.CardRepository.GetFilteredCards(_httpContext.User.GetCompanyId(), filter).Select(c => c.UserId).ToList();

                data = data.Where(x =>
                    userIdsFilteredByCardId.Contains(x.Id) ||
                    (x.FirstName + " " + x.LastName).ToLower().Contains(filter) ||
                    x.Department.DepartName.ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.AccessGroupForUsers.Length - 1 ? 0 : sortColumn;
            var orderBy = $"{ColumnDefines.AccessGroupForUsers[sortColumn]} {sortDirection}";
            //If sort column is Full name
            if (sortColumn == 2)
            {
                orderBy = $"FirstName {sortDirection}, LastName {sortDirection}";
            }

            data = data.OrderBy(orderBy);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            var result = data.AsEnumerable<User>().Select(Mapper.Map<UserForAccessGroup>).AsQueryable();
            return result.ToList();
        }

        /// <summary>
        /// Get data with pagination
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="filter"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortDirection"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="recordsFiltered"></param>
        /// <returns></returns>
        public List<UnAssignUserForAccessGroup> GetPaginatedForUnAssignUsers(int accessGroupId, string filter,
            int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var isFullAccessGroup =
                _unitOfWork.AccessGroupRepository.GetFullAccessGroup(_httpContext.User.GetCompanyId()).Id ==
                accessGroupId;

            var data = isFullAccessGroup
                ? _unitOfWork.UserRepository
                    .GetUnAssignUsersForMasterCard(_httpContext.User.GetCompanyId(), accessGroupId)
                    .AsEnumerable<User>().Select(Mapper.Map<UnAssignUserForAccessGroup>).AsQueryable()
                : _unitOfWork.UserRepository.GetUnAssignUsers(_httpContext.User.GetCompanyId(), accessGroupId)
                    .AsEnumerable<User>().Select(Mapper.Map<UnAssignUserForAccessGroup>).AsQueryable();

            totalRecords = data.Count();

            var tempData = data.ToList();
            foreach (var user in tempData)
                user.CardList = GetCardListByUserId(user.Id);

            data = tempData.AsQueryable();

            if (!string.IsNullOrEmpty(filter))
            {
                filter = filter.ToLower();

                data = data.Where(x =>
                    x.CardList.Any(c => c.CardId.ToLower().Contains(filter)) ||
                    //x.CardId.ToLower().Contains(filter) ||
                    x.FullName.ToLower().Contains(filter) ||
                    x.DepartmentName.ToLower().Contains(filter) ||
                    x.AccessGroupName.ToLower().Contains(filter));
            }

            recordsFiltered = data.Count();

            sortColumn = sortColumn > ColumnDefines.AccessGroupForUnAssignUsers.Length - 1 ? 0 : sortColumn;
            var orderBy = $"{ColumnDefines.AccessGroupForUnAssignUsers[sortColumn]} {sortDirection}";
            data = data.OrderBy(orderBy);
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
            return data.ToList();
        }

        /// <summary>
        /// Get list access group by list id and company
        /// </summary>
        /// <param name="accessGroupIds"></param>
        /// <returns></returns>
        public List<AccessGroup> GetByIds(List<int> accessGroupIds)
        {
            return _unitOfWork.AccessGroupRepository.GetByIdsAndCompanyId(_httpContext.User.GetCompanyId(), accessGroupIds);
        }

        /// <summary>
        /// Get access group by id and company
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public AccessGroup GetById(int accessGroupId)
        {
            return _unitOfWork.AccessGroupRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(),
                accessGroupId);
        }

        /// <summary>
        /// Get list access group is valid
        /// </summary>
        /// <returns></returns>
        public List<AccessGroup> GetListAccessGroups()
        {
            return _unitOfWork.AccessGroupRepository.GetListAccessGroups(_httpContext.User.GetCompanyId());
        }

        public List<AccessGroup> GetListAccessGroupsExceptForVisitor()
        {
            var accessGroups = _unitOfWork.AccessGroupRepository.GetListAccessGroups(_httpContext.User.GetCompanyId());

            accessGroups = accessGroups.AsQueryable().Where(m => m.Type != (short)AccessGroupType.VisitAccess).ToList();

            return accessGroups;
        }

        /// <summary>
        /// Add a access group
        /// </summary>
        /// <param name="model"></param>
        public void Add(AccessGroupModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var accessGroup = Mapper.Map<AccessGroup>(model);
                        accessGroup.CompanyId = _httpContext.User.GetCompanyId();
                        accessGroup.Type = (short)AccessGroupType.NormalAccess;
                        _unitOfWork.AccessGroupRepository.Add(accessGroup);
                        _unitOfWork.Save();

                        var content = $"{AccessGroupResource.msgAddAccessGroup}";
                        List<string> details = new List<string>();
                        details.Add($"{AccessGroupResource.lblAccessGroupName} : {accessGroup.Name}");

                        if (accessGroup.IsDefault)
                        {
                            SetAllAccessGroupToNotDefault(accessGroup.CompanyId, accessGroup.Id);
                            details.Add(string.Format(AccessGroupResource.msgChangeDefaultAccessGroup, accessGroup.Name));
                        }

                        var contentsDetail = string.Join("\n", details);

                        // save system log
                        _unitOfWork.SystemLogRepository.Add(accessGroup.Id, SystemLogType.AccessGroup, ActionLogType.Add,
                                content, contentsDetail, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }


        /// <summary>
        /// Add a access group for visitor
        /// </summary>
        /// <param name="model"></param>
        public AccessGroup AddForVisitor(AccessGroupModel model)
        {
            AccessGroup accessGroup = null;

            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        accessGroup = Mapper.Map<AccessGroup>(model);
                        var companyId = _httpContext.User.GetCompanyId();

                        accessGroup.CompanyId = companyId;
                        accessGroup.Type = (short)AccessGroupType.VisitAccess;
                        accessGroup.Name = Constants.Settings.NameAccessGroupVisitor + "_" + accessGroup.Name + DateTime.UtcNow;
                        accessGroup.IsDefault = false;

                        _unitOfWork.AccessGroupRepository.Add(accessGroup);
                        _unitOfWork.Save();

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            return accessGroup;
        }


        /// <summary>
        /// update a access group
        /// </summary>
        /// <param name="model"></param>
        public void Update(AccessGroupModel model)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var accessGroup = _unitOfWork.AccessGroupRepository.GetByIdAndCompanyId(_httpContext.User.GetCompanyId(), model.Id);
                        var existAccessGroupName = accessGroup.Name;
                        var existDefaultStatus = accessGroup.IsDefault;

                        Mapper.Map(model, accessGroup);
                        _unitOfWork.AccessGroupRepository.Update(accessGroup);
                        _unitOfWork.Save();

                        // Save system log
                        // This list contains all the changes.
                        List<string> changes = new List<string>();
                        if (existAccessGroupName != accessGroup.Name)
                        {
                            // AccessGroup name is changed.
                            changes.Add(string.Format(AccessGroupResource.msgChangeAccessGroupName, existAccessGroupName, accessGroup.Name));
                        }

                        if (existDefaultStatus == false && accessGroup.IsDefault == true)
                        {
                            // This AccessGroup has become Default Access Group.
                            SetAllAccessGroupToNotDefault(_httpContext.User.GetCompanyId(), accessGroup.Id);
                            changes.Add(string.Format(AccessGroupResource.msgChangeDefaultAccessGroup, accessGroup.Name));
                        }

                        var content = $"{AccessGroupResource.msgChangeAccessGroup}";
                        var contentsDetail = $"{string.Join("\n", changes)}";
                        _unitOfWork.SystemLogRepository.Add(accessGroup.Id, SystemLogType.AccessGroup, ActionLogType.Update,
                            content, contentsDetail, null, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Delete a access group
        /// </summary>
        /// <param name="accessGroup"></param>
        public void Delete(AccessGroup accessGroup)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        accessGroup.IsDeleted = true;
                        _unitOfWork.AccessGroupRepository.Update(accessGroup);

                        //Save system log
                        var content = $"{AccessGroupResource.msgDeleteAccessGroup}\n" +
                                        $"{AccessGroupResource.lblAccessGroupName} : {accessGroup.Name}";

                        _unitOfWork.SystemLogRepository.Add(accessGroup.Id, SystemLogType.AccessGroup, ActionLogType.Delete,
                                content, null, null, _httpContext.User.GetCompanyId());
                        //Save to database
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            // Do we neeed the below code?
            UnAssignUserAndAddToDefaultAccessGroup(new List<int> { accessGroup.Id });
            UnAssigrDoorAndAddToDefaultAccessGroup(new List<int> { accessGroup.Id });
        }

        /// <summary>
        /// Delete a list access group
        /// </summary>
        /// <param name="accessGroups"></param>
        public void DeleteRange(List<AccessGroup> accessGroups)
        {
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var accessGroup in accessGroups)
                        {
                            accessGroup.IsDeleted = true;
                            _unitOfWork.AccessGroupRepository.Update(accessGroup);
                        }

                        //Save system log
                        if (accessGroups.Count == 1)
                        {
                            var accessGroup = accessGroups.First();
                            var content = $"{AccessGroupResource.msgDeleteAccessGroup}\n" +
                                        $"{AccessGroupResource.lblAccessGroupName} : {accessGroup.Name}";

                            _unitOfWork.SystemLogRepository.Add(accessGroup.Id, SystemLogType.AccessGroup, ActionLogType.Delete,
                                content, null, null, _httpContext.User.GetCompanyId());
                        }
                        else
                        {
                            var content = $"{AccessGroupResource.msgDeleteAccessGroup}";

                            var accessGroupIds = accessGroups.Select(c => c.Id).ToList();
                            var accessGroupNames = accessGroups.Select(c => c.Name).ToList();

                            var contentDetails = $"{AccessGroupResource.lblAccessGroupCount} : {accessGroups.Count}\n" +
                                                $"{AccessGroupResource.lblAccessGroupName} : {string.Join(", ", accessGroupNames)}";

                            _unitOfWork.SystemLogRepository.Add(accessGroupIds.First(), SystemLogType.AccessGroup, ActionLogType.DeleteMultiple,
                                content, contentDetails, accessGroupIds, _httpContext.User.GetCompanyId());
                        }

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            // Do we neeed the below code?
            var agIds = accessGroups.Select(x => x.Id).ToList();
            UnAssignUserAndAddToDefaultAccessGroup(agIds);
            UnAssigrDoorAndAddToDefaultAccessGroup(agIds);
        }

        /// <summary>
        /// Assign list users
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="userIds"></param>
        public string AssignUsers(int accessGroupId, List<int> userIds)
        {
            var returnValue = string.Empty;
            var startTime = DateTime.Now;
            var addingTime = DateTime.Now;
            var savingTime = DateTime.Now;
            var addUserCount = 0;
            var removeUserCount = 0;
            var userLogsProtocol = new List<UserLogProtocolData>();
            var companyId = _httpContext.User.GetCompanyId();
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var usersToAssign = _unitOfWork.UserRepository.GetByIds(companyId, userIds);
                        var accessGroupDevices = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(companyId, accessGroupId);
                        if (accessGroupDevices.Any())
                        {
                            var maxIcuUser = Constants.Settings.DefaultMaxIcuUser;
                            if (!string.IsNullOrEmpty(_configuration[Constants.Settings.MaxIcuUser]))
                            {
                                maxIcuUser = Convert.ToInt32(_configuration[Constants.Settings.MaxIcuUser]);
                            }

                            foreach (var accessGroupDevice in accessGroupDevices)
                            {
                                var userCount = _unitOfWork.AccessGroupDeviceRepository.GetUserCount(accessGroupDevice.IcuId);
                                if (userCount + usersToAssign.Count > maxIcuUser)
                                {
                                    returnValue = string.Format(AccessGroupResource.msgUnableToAssignOverMaxUser,
                                        maxIcuUser,
                                        $"{accessGroupDevice.Icu.Name} ({accessGroupDevice.Icu.DeviceAddress})");

                                    _logger.LogWarning(returnValue);
                                    return;
                                }

                                //Add user log
                                var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, usersToAssign, ActionType.Add);

                                //Add user log protocol data
                                var userLogProtocol = new UserLogProtocolData
                                {
                                    IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                                    UserLogs = userLogs,
                                    ProtocolType = Constants.Protocol.AddUser
                                };
                                userLogsProtocol.Add(userLogProtocol);
                                addUserCount += userLogs.Count;
                            }
                        }

                        //Un-assign users from other access groups
                        var usersToUnAssign = UnAssignUsersFromUserList(usersToAssign);
                        userLogsProtocol.AddRange(usersToUnAssign);
                        removeUserCount = usersToUnAssign.SelectMany(c => c.UserLogs).Count();

                        //Update users to current access group
                        foreach (var user in usersToAssign)
                        {
                            user.AccessGroupId = accessGroupId;
                        }

                        //Save system log
                        var assignedUserIds = usersToAssign.Select(c => c.Id).ToList();
                        var assignedUserNames = usersToAssign.Select(c => c.FirstName + " " + c.LastName + "(" + c.UserCode + ")").ToList();
                        var content = AccessGroupResource.msgAssignUsers;
                        var contentDetails = $"{UserResource.lblUserCount} : {assignedUserIds.Count}\n" +
                                        $"{AccessGroupResource.lblAssignUser} ({UserResource.lblUserCode}) : {string.Join(", ", assignedUserNames)}";

                        _unitOfWork.SystemLogRepository.Add(accessGroupId, SystemLogType.AccessGroup, ActionLogType.AssignUser,
                            content, contentDetails, assignedUserIds, _httpContext.User.GetCompanyId());

                        _unitOfWork.Save();
                        addingTime = DateTime.Now;
                        _unitOfWork.Save();
                        transaction.Commit();
                        savingTime = DateTime.Now;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            if (!string.IsNullOrEmpty(returnValue))
            {
                return returnValue;
            }

            //Send user data to rabbit mq server
            userLogsProtocol.Reverse();
            var publishTime = DateTime.Now;
            SendUserDataToQueue(userLogsProtocol, addUserCount, removeUserCount, startTime, addingTime, savingTime, publishTime);
            //WritePerformanceCsvData(addUserCount, removeUserCount, startTime, addingTime, savingTime, publishTime);
            return returnValue;
        }

        /// <summary>
        /// //Un-assign users from other access groups
        /// </summary>
        /// <param name="users"></param>
        /// <returns></returns>
        public List<UserLogProtocolData> UnAssignUsersFromUserList(List<User> users)
        {
            var userLogsProtocol = new List<UserLogProtocolData>();
            if (users.Any())
            {
                var accessGroupDevicesForUnAssignUser = users.SelectMany(c => c.AccessGroup.AccessGroupDevice)
                    .DistinctBy(c => new { c.AccessGroupId, c.IcuId })
                    .ToList();
                foreach (var accessGroupDevice in accessGroupDevicesForUnAssignUser)
                {
                    //Add user log data
                    var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, users,
                        ActionType.Delete);

                    //Add user log protocol data
                    var userLogProtocol = new UserLogProtocolData
                    {
                        IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                        UserLogs = userLogs,
                        ProtocolType = Constants.Protocol.DeleteUser
                    };
                    userLogsProtocol.Add(userLogProtocol);
                }
            }

            return userLogsProtocol;
        }

        /// <summary>
        /// //Un-assign users from other access groups
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public List<UserLogProtocolData> UnAssignUsersFromAccessGroup(int accessGroupId, List<User> users)
        {
            var userLogsProtocol = new List<UserLogProtocolData>();
            if (users.Any())
            {
                var accessGroupDevicesForUnAssignUser = users.Where(c => c.AccessGroupId == accessGroupId)
                    .SelectMany(c => c.AccessGroup.AccessGroupDevice)
                    .DistinctBy(c => new { c.AccessGroupId, c.IcuId })
                    .ToList();
                foreach (var accessGroupDevice in accessGroupDevicesForUnAssignUser)
                {
                    //Add user log data
                    var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, users,
                        ActionType.Delete);

                    //Add user log protocol data
                    var userLogProtocol = new UserLogProtocolData
                    {
                        IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                        UserLogs = userLogs,
                        ProtocolType = Constants.Protocol.DeleteUser
                    };
                    userLogsProtocol.Add(userLogProtocol);
                }
            }

            return userLogsProtocol;
        }

        /// <summary>
        /// Assign list doors
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="model"></param>
        public string AssignDoors(int accessGroupId, AccessGroupAssignDoor model, bool isPublish = true)
        {
            var returnValue = string.Empty;
            var startTime = DateTime.Now;
            var addingTime = DateTime.Now;
            var savingTime = DateTime.Now;
            var addUserCount = 0;
            var companyId = model.Doors.Select(c => c.CompanyId).FirstOrDefault();
            var doorIds = model.Doors.Select(c => c.DoorId).ToList();
            var icuDevicesToAssign =
                _unitOfWork.IcuDeviceRepository.GetByIds(doorIds);

            var accessGroup = _unitOfWork.AppDbContext.AccessGroup.Include(x => x.User)
                .FirstOrDefault(x => x.CompanyId == companyId && x.Id == accessGroupId && !x.IsDeleted);

            if (icuDevicesToAssign != null && icuDevicesToAssign.Any())
            {
                var userLogsProtocol = new List<UserLogProtocolData>();
                _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
                {
                    using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                    {
                        try
                        {

                            var details = new List<string>();
                            foreach (var icuDevice in icuDevicesToAssign)
                            {
                                var tzId = model.Doors.First(c => c.DoorId == icuDevice.Id).TzId;

                                //var tzPos = accessGroup?.Type == (short) AccessGroupType.FullAccess
                                //    ? Constants.Tz24hPos
                                //    : model.Doors.First(c => c.DoorId == icuDevice.Id).TzId;

                                var timezone = _unitOfWork.TimezoneRepository.GetById(tzId);
                                var tzName = timezone != null ? timezone.Name : string.Empty;
                                var detail = $"{icuDevice.Name} - {tzName}";
                                details.Add(detail);

                                //기존에 Access Group device에 있었나 확인해야함
                                //있었으면 지우고
                                //없으면 다음으로 넘어감
                                var existAccessGroupDevice = _unitOfWork.AccessGroupDeviceRepository.GetByIcuIdInOtherCompany(companyId, icuDevice.Id);

                                if (existAccessGroupDevice.Count() > 0)
                                {
                                    _unitOfWork.AccessGroupDeviceRepository.DeleteRange(existAccessGroupDevice);
                                }

                                var accessGroupDevice = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupIdAndDeviceId(companyId, accessGroupId, icuDevice.Id);

                                if (accessGroupDevice == null)
                                {
                                    //Add access group device
                                    AddAccessGroupDevice(accessGroupId, icuDevice.Id, timezone.Id);
                                }

                                if (accessGroup?.User != null && accessGroup.User.Any())
                                {
                                    var usersToAssign = accessGroup.User.ToList();

                                    var maxDeviceUser = icuDevice.DeviceType == (short)DeviceType.ItouchPop ? Constants.Settings.DefaultMaxPopUser : Constants.Settings.DefaultMaxIcuUser;

                                    var userList = _unitOfWork.AccessGroupDeviceRepository.GetUserList(icuDevice.Id);
                                    var cardCount = 0;

                                    foreach (var user in userList)
                                    {
                                        cardCount += _unitOfWork.CardRepository.GetCountByUserId(_httpContext.User.GetCompanyId(), user.Id);
                                    }

                                    foreach (var user in usersToAssign)
                                    {
                                        cardCount += _unitOfWork.CardRepository.GetCountByUserId(_httpContext.User.GetCompanyId(), user.Id);
                                    }

                                    if (cardCount > maxDeviceUser)
                                    {
                                        returnValue = string.Format(AccessGroupResource.msgUnableToAssignOverMaxUser,
                                            maxDeviceUser,
                                            $"{icuDevice.Name} ({icuDevice.DeviceAddress})");

                                        _logger.LogWarning(returnValue);
                                        return;
                                    }
                                    //Add user log data
                                    var userLogs = AddUserLog(icuDevice.Id, timezone.Id, usersToAssign, ActionType.Add);
                                    addUserCount += userLogs.Count;
                                    //Add user log protocol data
                                    var userLogProtocol = new UserLogProtocolData
                                    {
                                        IcuAddress = icuDevice.DeviceAddress,
                                        UserLogs = userLogs,
                                        ProtocolType = Constants.Protocol.AddUser
                                    };
                                    userLogsProtocol.Add(userLogProtocol);
                                }
                            }
                            addingTime = DateTime.Now;

                            //Save system log
                            var assignedDoorIds = icuDevicesToAssign.Select(c => c.Id).ToList();
                            var content = AccessGroupResource.msgAssignDoors;
                            var contentDetails = $"{DeviceResource.lblDeviceCount} : {assignedDoorIds.Count}\n" +
                                            $"{AccessGroupResource.lblAssignDoor} - {TimezoneResource.lblTimezone} :\n{string.Join("\n", details)}";

                            _unitOfWork.SystemLogRepository.Add(accessGroupId, SystemLogType.AccessGroup, ActionLogType.AssignDoor,
                                content, contentDetails, assignedDoorIds, companyId);

                            _unitOfWork.Save();

                            transaction.Commit();
                            savingTime = DateTime.Now;
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            throw;
                        }
                    }
                });
                if (!string.IsNullOrEmpty(returnValue))
                {
                    return returnValue;
                }
                var publishTime = DateTime.Now;
                //Send user data to rabbit mq server

                if (isPublish)
                {
                    SendUserDataToQueue(userLogsProtocol, addUserCount, 0, startTime, addingTime, savingTime, publishTime);
                }

                if (accessGroup.Name.Contains(Constants.Settings.NameAccessGroupVisitor))
                {
                    var visitor = _unitOfWork.AppDbContext.Visit.FirstOrDefault(v =>
                        v.AccessGroupId == accessGroup.Id);
                    var card = _unitOfWork.AppDbContext.Card.FirstOrDefault(c => c.VisitId == visitor.Id && !c.IsDeleted);
                    if (card != null)
                    {
                        var agDevices = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(), visitor.AccessGroupId);

                        if (agDevices != null && agDevices.Any())
                        {
                            foreach (var agDevice in agDevices)
                            {
                                SendIdentificationToDeviceVisitor(agDevice, visitor, card, true);
                            }
                        }
                    }
                }

                //WritePerformanceCsvData();
            }
            return returnValue;
        }

        /// <summary>
        /// Send users to rabbit mq server
        /// </summary>
        /// <param name="userLogsProtocolData"></param>
        /// <param name="addUserCount"></param>
        /// <param name="removeUserCount"></param>
        /// <param name="startTime"></param>
        /// <param name="addingTime"></param>
        /// <param name="savingTime"></param>
        /// <param name="publishTime"></param>
        public void SendUserDataToQueue(List<UserLogProtocolData> userLogsProtocolData, int addUserCount,
            int removeUserCount, DateTime startTime, DateTime addingTime, DateTime savingTime, DateTime publishTime)
        {
            if (userLogsProtocolData == null || !userLogsProtocolData.Any())
            {
                return;
            }

            //Send users
            foreach (var userLogProtocolData in userLogsProtocolData)
            {
                if (userLogProtocolData.UserLogs == null || !userLogProtocolData.UserLogs.Any())
                {
                    continue;
                }

                var device = _unitOfWork.IcuDeviceRepository.GetDeviceByAddress(userLogProtocolData.IcuAddress);

                IEnumerable<List<UserLog>> splitUsers;

                if (device != null && device.DeviceType == (short)DeviceType.ItouchPop)
                    splitUsers = Helpers.SplitList(userLogProtocolData.UserLogs, Constants.Settings.MaxUserSendToITouch);
                else
                    splitUsers = Helpers.SplitList(userLogProtocolData.UserLogs, Constants.MaxSendIcuUserCount);

                var publishCount = splitUsers.Count();
                foreach (var sendUsers in splitUsers)
                {
                    var userLogs = new List<UserLog>();
                    foreach (var userLog in sendUsers)
                    {
                        //var discardList = new[]
                        //    {(short) CardStatus.InValid, (short) CardStatus.Retire, (short) CardStatus.Lost};
                        //if (!discardList.Contains(userLog.User.CardStatus))
                        //{
                        //    userLogs.Add(userLog);
                        //}
                        userLogs.Add(userLog);
                    }
                    var protocolData = publishCount == 1 ? MakeUserProtocolData(sendUsers, userLogProtocolData.ProtocolType, (int)UpdateFlag.Stop) : MakeUserProtocolData(sendUsers, userLogProtocolData.ProtocolType);
                    var message = protocolData.ToString();
                    var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{userLogProtocolData.IcuAddress}";
                    _queueService.Publish(routingKey, message);
                    //Todo
                    WritePerformanceCsvData(protocolData.MsgId, protocolData.Type, userLogProtocolData.IcuAddress, sendUsers.Count, addUserCount, 0, startTime, addingTime, savingTime, publishTime);
                }
            }
        }

        /// <summary>
        /// Add access group device
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="icuId"></param>
        /// <param name="tzId"></param>
        public void AddAccessGroupDevice(int accessGroupId, int icuId, int tzId)
        {
            var accessGroupDevice = new AccessGroupDevice
            {
                AccessGroupId = accessGroupId,
                IcuId = icuId,
                TzId = tzId
            };
            _unitOfWork.AccessGroupDeviceRepository.Add(accessGroupDevice);
        }

        /// <summary>
        /// Add user information into UserLog
        /// </summary>
        /// <param name="icuId"></param>
        /// <param name="tzId"></param>
        /// <param name="users"></param>
        /// <param name="actionType"></param>
        /// <returns></returns>
        public List<UserLog> AddUserLog(int icuId, int tzId, List<User> users, ActionType actionType, string processId = null, decimal progressStart = 0, decimal progressRange = 0)
        {
            var userLogs = new List<UserLog>();

            var companyId = _unitOfWork.IcuDeviceRepository.GetByIcuId(icuId)?.CompanyId;

            var userIdList = _unitOfWork.CardRepository.GetUserIdHasCards(companyId ?? 0);
            var usersHasCard = users.Where(c => userIdList.Contains(c.Id)).ToList();

            var tz = _unitOfWork.TimezoneRepository.GetByIdAndCompany(tzId, companyId ?? 0);
            var totalUserHasCard = usersHasCard.Count;

            foreach (var user in usersHasCard)
            {
                var cards = _unitOfWork.CardRepository.GetByUserId(companyId ?? 0, user.Id);
                var department = _unitOfWork.DepartmentRepository.GetByIdAndCompanyId(user.DepartmentId, companyId ?? 0);
                var departmentName = string.Empty;
                if (!string.IsNullOrEmpty(department.DepartName))
                {
                    departmentName = department.DepartName;
                }
                foreach (var card in cards)
                {
                    var issueCount = card != null ? card.IssueCount : 0;

                    var userLog = new UserLog
                    {
                        IcuId = icuId,
                        CardId = card.CardId,
                        UserId = user.Id,
                        User = user,
                        EffectiveDate = user.EffectiveDate,
                        ExpiredDate = user.ExpiredDate,
                        KeyPadPw = user.KeyPadPw,
                        TzPosition = tz?.Position ?? 0,
                        Action = (short)actionType,
                        TransferStatus = (short)TransferType.SavedToServerSuccess,
                        DepartmentName = departmentName,
                        IssueCount = issueCount
                    };
                    userLogs.Add(userLog);
                }
                // Update progress to frontend
                //var progress = (totalMsg - publishCount)*100 / (decimal)(totalMsg*progressRange) + (decimal)10 + progressRange*(remainCount);
                //if (processId != null)
                //{
                //    i++;
                //    var progress = progressStart + (i) * progressRange / (decimal)(totalUserHasCard);
                //    _queueService.Publish(Constants.RabbitMq.LongProcessProgressTopic, ProcessProgressProtocolData.MakeLongProcessProgressMessage(processId, progress, Constants.LongProgressName.Preparing));
                //}
            }
            return userLogs;
        }

        public List<UserLog> AddVisitorLog(int icuId, int tzId, List<Visit> visitors, ActionType actionType)
        {
            var userLogs = new List<UserLog>();

            var companyId = _unitOfWork.IcuDeviceRepository.GetByIcuId(icuId)?.CompanyId;

            var visitIdList = _unitOfWork.CardRepository.GetVisitIdHasCards(companyId ?? 0);
            var visitorsHasCard = visitors.Where(c => visitIdList.Contains(c.Id)).ToList();

            var tz = _unitOfWork.TimezoneRepository.GetByIdAndCompany(tzId, companyId ?? 0);
            var totalVisitHasCard = visitorsHasCard.Count;

            foreach (var visit in visitorsHasCard)
            {
                var cards = _unitOfWork.CardRepository.GetByVisitId(companyId ?? 0, visit.Id);

                foreach (var card in cards)
                {
                    var issueCount = card != null ? card.IssueCount : 0;

                    var userLog = new UserLog
                    {
                        IcuId = icuId,
                        CardId = card.CardId,
                        UserId = visit.Id,
                        User = Mapper.Map<User>(visit),
                        EffectiveDate = visit.StartDate,
                        ExpiredDate = visit.EndDate,
                        KeyPadPw = null,
                        TzPosition = tz?.Position ?? 0,
                        Action = (short)actionType,
                        TransferStatus = (short)TransferType.SavedToServerSuccess,
                        DepartmentName = visit.VisiteeDepartment,
                        IssueCount = issueCount
                    };
                    userLogs.Add(userLog);
                }
            }
            return userLogs;
        }

        /// <summary>
        /// Un-assign users from access group
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="userIds"></param>
        public void UnAssignUsers(int accessGroupId, List<int> userIds)
        {
            var userLogsProtocol = new List<UserLogProtocolData>();
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        var defaultAccessGroup = _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(companyId);
                        var usersToUnAssign = _unitOfWork.UserRepository.GetByIds(companyId, userIds);
                        //Un-assign users from current access group
                        userLogsProtocol.AddRange(UnAssignUsersFromAccessGroup(accessGroupId, usersToUnAssign));

                        //Update users to no access group
                        foreach (var user in usersToUnAssign)
                        {
                            user.AccessGroupId = defaultAccessGroup.Id;
                        }

                        //Save system log
                        var unAssignedUserIds = usersToUnAssign.Select(c => c.Id).ToList();
                        var unAssignedUserNames = usersToUnAssign.Select(c => c.FirstName + " " + c.LastName).ToList();
                        var content = AccessGroupResource.msgUnAssignUsers;
                        var contentDetails = $"{UserResource.lblUserCount} : {unAssignedUserIds.Count}\n" +
                                            $"{AccessGroupResource.lblUnAssignUser}: {string.Join(", ", unAssignedUserNames)}";

                        _unitOfWork.SystemLogRepository.Add(accessGroupId, SystemLogType.AccessGroup, ActionLogType.UnassignUser,
                            content, contentDetails, unAssignedUserIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
            //Send user data to rabbit mq server
            SendUserDataToQueue(userLogsProtocol, 0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime());
        }

        public void UnAssignDoors(int accessGroupId, List<int> doorIds)
        {
            var userLogsProtocol = new List<UserLogProtocolData>();
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var companyId = _httpContext.User.GetCompanyId();
                        var users = _unitOfWork.AppDbContext.User.Where(u => u.CompanyId == _httpContext.User.GetCompanyId()
                                                                             && u.AccessGroupId == accessGroupId && !u.IsDeleted).ToList();
                        var accessGroupDevices =
                            _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupIdAndDeviceIds(companyId, accessGroupId,
                                doorIds);
                        var accessGroup = _unitOfWork.AppDbContext.AccessGroup.FirstOrDefault(a => a.Id == accessGroupId);

                        List<string> doorNames = new List<string>();
                        if (accessGroup != null)
                        {
                            foreach (var accessGroupDevice in accessGroupDevices)
                            {
                                if (accessGroup.Name.Contains(Constants.Settings.NameAccessGroupVisitor))
                                {
                                    String[] str = accessGroup.Name.Split("-");
                                    var visitId = Int32.Parse(str[1]);
                                    var visit = _unitOfWork.AppDbContext.Visit.Single(v => v.Id == visitId);
                                    if (visit != null)
                                    {
                                        var card = _unitOfWork.AppDbContext.Card.FirstOrDefault(c => c.VisitId == visitId);
                                        if (card != null)
                                        {
                                            SendIdentificationToDeviceVisitor(accessGroupDevice, visit, card, false);
                                        }

                                    }
                                }
                                else
                                {
                                    //Add user log data
                                    if (users.Count != 0)
                                    {
                                        var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, users,
                                            ActionType.Delete);

                                        //Add user log protocol data
                                        var userLogProtocol = new UserLogProtocolData
                                        {
                                            IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                                            UserLogs = userLogs,
                                            ProtocolType = Constants.Protocol.DeleteUser
                                        };
                                        userLogsProtocol.Add(userLogProtocol);
                                    }
                                }

                                doorNames.Add(_unitOfWork.IcuDeviceRepository.GetByIcuId(accessGroupDevice.IcuId).Name);

                                //Remove relationship from access group device
                                _unitOfWork.AccessGroupDeviceRepository.Delete(accessGroupDevice);
                            }
                        }
                        //Save system log
                        var content = AccessGroupResource.msgUnAssignDoors;
                        var contentDetails = $"{DeviceResource.lblDeviceCount} : {doorIds.Count}\n" +
                                            $"{AccessGroupResource.lblUnAssignDoor}: {string.Join(", ", doorNames)}";

                        _unitOfWork.SystemLogRepository.Add(accessGroupId, SystemLogType.AccessGroup, ActionLogType.UnassignDoor,
                            content, contentDetails, doorIds, companyId);

                        _unitOfWork.Save();
                        transaction.Commit();

                        if (users.Count != 0)
                        {
                            SendUserDataToQueue(userLogsProtocol, 0, 0, new DateTime(), new DateTime(), new DateTime(),
                                new DateTime());
                        }

                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });
        }

        /// <summary>
        /// Change timezone for the doors in the access group
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        public void ChangeTimezone(int id, AccessGroupAssignDoor model)
        {
            var userLogsProtocol = new List<UserLogProtocolData>();
            _unitOfWork.AppDbContext.Database.CreateExecutionStrategy().Execute(() =>
            {
                using (var transaction = _unitOfWork.AppDbContext.Database.BeginTransaction())
                {
                    try
                    {
                        var doorIds = model.Doors.Select(c => c.DoorId).ToList();
                        var accessGroupDevices =
                            _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupIdAndDeviceIds(_httpContext.User.GetCompanyId(),
                                id, doorIds);
                        foreach (var door in model.Doors)
                        {
                            var accessGroupDevice = accessGroupDevices.Find(c => c.AccessGroupId == id && c.IcuId == door.DoorId);
                            accessGroupDevice.TzId = door.TzId;

                            //Add user log data
                            var users = accessGroupDevice.AccessGroup.User.ToList();
                            if (users.Any())
                            {
                                var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, users,
                                    ActionType.Update);

                                //Add user log protocol data
                                var userLogProtocol = new UserLogProtocolData
                                {
                                    IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                                    UserLogs = userLogs,
                                    //ProtocolType = Constants.Protocol.AddUser
                                    ProtocolType = Constants.Protocol.UpdateUser
                                };
                                userLogsProtocol.Add(userLogProtocol);
                            }
                        }
                        _unitOfWork.Save();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            });

            //Send user data to rabbit mq server
            SendUserDataToQueue(userLogsProtocol, 0, 0, new DateTime(), new DateTime(), new DateTime(), new DateTime());
        }

        /// <summary>
        /// Make user protocol data
        /// </summary>
        /// <param name="userLogs"></param>
        /// <param name="protocolType"></param>
        /// <returns></returns>
        public UserProtocolData MakeUserProtocolData(List<UserLog> userLogs, string protocolType, int updateFlag = (int)UpdateFlag.Continue)
        {
            var userProtocolData = new UserProtocolData
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = protocolType
            };
            var userProtocolHeaderData = new UserProtocolHeaderData
            {
                Total = userLogs == null ? 0 : userLogs.Count,
                UpdateFlag = updateFlag,
                Users = Mapper.Map<List<UserProtocolDetailData>>(userLogs)
            };
            userProtocolData.Data = userProtocolHeaderData;
            return userProtocolData;
        }

        /// <summary>
        /// If this access group(acceddGroupId) has become default AG, then other AG should be set not default AG.
        /// </summary>
        /// <param name="companyId">the id of the company to which this AG belongs</param>
        /// <param name="accessGroupId">access group id that become as default AG</param>
        internal void SetAllAccessGroupToNotDefault(int companyId, int accessGroupId)
        {
            var accessGroups = _unitOfWork.AccessGroupRepository.GetAccessGroupUnSetDefault(companyId, accessGroupId);
            foreach (var access in accessGroups)
            {
                access.IsDefault = false;
            }
        }

        /// <summary>
        /// Check access group is already db
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public bool HasData(int accessGroupId)
        {
            return _unitOfWork.AccessGroupDeviceRepository.HasData(_httpContext.User.GetCompanyId(), accessGroupId);
        }

        /// <summary>
        /// Check name is already exist on db
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasExistName(int accessGroupId, string name)
        {
            return _unitOfWork.AccessGroupRepository.HasExistName(accessGroupId, name);
        }

        /// <summary>
        /// Write performance csv data
        /// </summary>
        /// <param name="msgId"></param>
        /// <param name="addedUserCount"></param>
        /// <param name="removedUserCount"></param>
        /// <param name="startTime"></param>
        /// <param name="addDataTime"></param>
        /// <param name="saveDataTime"></param>
        /// <param name="publishTime"></param>
        /// <param name="userCount"></param>
        /// <param name="msgType"></param>
        /// <param name="deviceAddress"></param>
        private void WritePerformanceCsvData(string msgId, string msgType, string deviceAddress, int userCount, int addedUserCount, int removedUserCount, DateTime startTime,
            DateTime addDataTime, DateTime saveDataTime, DateTime publishTime)
        {
            var content = new[]
            {
                "Send_User_Log",
                _httpContext.Request.GetEncodedUrl(),
                msgId,
                msgType,
                deviceAddress,
                addedUserCount.ToString(),
                removedUserCount.ToString(),
                userCount.ToString(),
                startTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSsFfffff),
                addDataTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSsFfffff),
                saveDataTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSsFfffff),
                publishTime.ToString(Constants.DateTimeFormat.YyyyMmDdHhMmSsFfffff),
                "",
                addDataTime.Subtract(startTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),
                saveDataTime.Subtract(addDataTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),
                publishTime.Subtract(saveDataTime).TotalMilliseconds.ToString(CultureInfo.InvariantCulture),
                ""
            };
            _queueService.PublishToSpecificQueue(Constants.RabbitMq.SetUserTaskQueue, string.Join(@",", content));
        }

        /// <summary>
        /// Check access group can be delete
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public string GetNameAccessGroupCouldNotUpdateOrDelete(List<int> accessGroupId)
        {
            var result = "";
            var accessGroups = GetByIds(accessGroupId);
            foreach (var group in accessGroups)
            {
                var user = group.User;
                if (group.IsDefault || group.Type == (short)AccessGroupType.FullAccess || group.Type == (short)AccessGroupType.NoAccess || group.Type == (short)AccessGroupType.VisitAccess || user.Any())
                {
                    result = group.Name;
                }
            }

            return result;
        }

        /// <summary>
        /// Unassign all user in a access group and add them to default access group
        /// </summary>
        /// <param name="accessGroupIds"></param>
        private void UnAssignUserAndAddToDefaultAccessGroup(List<int> accessGroupIds)
        {
            foreach (var accessGroupId in accessGroupIds)
            {
                var listUser = _unitOfWork.UserRepository.GetAssignUsers(_httpContext.User.GetCompanyId(), accessGroupId).ToList();
                if (!listUser.Any())
                {
                    return;
                }
                var listIds = listUser.Select(x => x.Id).ToList();
                UnAssignUsers(accessGroupId, listIds);
                var accessGroupDefaultId =
                    _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(_httpContext.User.GetCompanyId()).Id;
                AssignUsers(accessGroupDefaultId, listIds);
            }
        }

        /// <summary>
        /// Unassign all user in a access group and add them to default access group
        /// </summary>
        /// <param name="accessGroupIds"></param>
        private void UnAssigrDoorAndAddToDefaultAccessGroup(List<int> accessGroupIds)
        {
            foreach (var accessGroupId in accessGroupIds)
            {
                var listDoors = _unitOfWork.AccessGroupDeviceRepository.GetByAccessGroupId(_httpContext.User.GetCompanyId(), accessGroupId).ToList();
                if (!listDoors.Any())
                {
                    return;
                }
                var listIds = listDoors.Select(x => x.IcuId).ToList();
                UnAssignDoors(accessGroupId, listIds);
                var accessGroupDefaultId =
                    _unitOfWork.AccessGroupRepository.GetDefaultAccessGroup(_httpContext.User.GetCompanyId()).Id;
                var doors = new List<AccessGroupAssignDoorDetail>();
                foreach (var doorId in listIds)
                {
                    var accessGroupAssignDoorDetail = new AccessGroupAssignDoorDetail
                    {
                        DoorId = doorId,
                        TzId = Constants.DefaultActiveTimezoneId
                    };
                    doors.Add(accessGroupAssignDoorDetail);
                }

                var accessGroupAssignDoor = new AccessGroupAssignDoor
                {
                    Doors = doors
                };
                AssignDoors(accessGroupDefaultId, accessGroupAssignDoor);
            }
        }

        /// <summary>
        /// Send a new card to device
        /// </summary>
        public void SendIdentificationToDevice(AccessGroupDevice agDevice, User user, Card card, bool isAdd)
        {
            var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{agDevice.Icu.DeviceAddress}";

            var protocolData = new UserProtocolData()
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = isAdd ? Constants.Protocol.AddUser : Constants.Protocol.DeleteUser,
                Sender = _httpContext.User.GetUsername()
            };
            var userProtocolHeaderData = new UserProtocolHeaderData
            {
                Total = 1,
                UpdateFlag = 1,
                Users = new List<UserProtocolDetailData>
                {
                    new UserProtocolDetailData()
                    {
                        EmployeeNumber = user.EmpNumber,
                        UserName = user.FirstName,
                        DepartmentName = user.Department.DepartName,
                        CardId = card.CardId,
                        IssueCount = 0,
                        AdminFlag = (short) (user.IsMasterCard ? 1 : 0),
                        EffectiveDate = user.EffectiveDate.HasValue ? user.EffectiveDate.Value.ToString(Constants.DateTimeFormat.DdMMyyyy) : null,
                        ExpireDate = user.ExpiredDate.HasValue ? user.ExpiredDate.Value.ToString(Constants.DateTimeFormat.DdMMyyyy) : null,
                        AntiPassBack = card.CardStatus,
                        Timezone =  agDevice.Tz?.Position ?? 1,
                        Password = user.KeyPadPw
                    }
                }
            };
            protocolData.Data = userProtocolHeaderData;

            _queueService.Publish(routingKey, protocolData.ToString());
        }

        /// <summary>
        /// Publish the Add or Delete user message to Device.
        /// </summary>
        public void SendAddOrDeleteUser(AccessGroupDevice accessGroupDevice, List<User> users = null,
            bool isAddUser = true, string groupMsgId = "", int groupIndex = 0, int groupLength = 1, string processId = "", string actionType = "", int remainCount = 0, decimal progressRange = 0)
        {
            if (users == null)
            {
                users = _unitOfWork.UserRepository
                    .GetAssignUsers(accessGroupDevice.Icu.CompanyId ?? 0, accessGroupDevice.AccessGroupId).ToList();
            }

            var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, users,
                isAddUser ? ActionType.Add : ActionType.Delete);

            var visitors = _unitOfWork.VisitRepository.GetByAccessGroupId(accessGroupDevice.Icu.CompanyId ?? 0, accessGroupDevice.AccessGroupId).ToList();

            var visitorLogs = AddVisitorLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, visitors,
                isAddUser ? ActionType.Add : ActionType.Delete);

            userLogs = userLogs.Concat(visitorLogs).ToList();

            //Add user log protocol data
            var userLogProtocol = new UserLogProtocolData
            {
                IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                UserLogs = userLogs,
                ProtocolType = isAddUser ? Constants.Protocol.AddUser : Constants.Protocol.DeleteUser
            };
            // When there is no user need to send to device, we send stop flag???
            if ((userLogProtocol.UserLogs == null || !userLogProtocol.UserLogs.Any()) && remainCount == 0)
            {
                var protocolData = MakeUserProtocolData(new List<UserLog>(), userLogProtocol.ProtocolType,
                    (int)UpdateFlag.Stop);
                var message = protocolData.ToString();
                var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{userLogProtocol.IcuAddress}";

                if (groupLength == 1)
                {
                    _queueService.Publish(routingKey, message);
                }
                else
                {
                    _queueService.SendGroupMessage(deviceAddress: accessGroupDevice.Icu.DeviceAddress,
                        protocolData.MsgId, message: message, topic: routingKey,
                        groupMsgId: groupMsgId, groupIndex: groupIndex, groupLength: groupLength,
                        processId: processId, actionType: actionType);
                }

                return;
            }

            var maxSplit = Constants.MaxSendIcuUserCount;

            if (accessGroupDevice.Icu.DeviceType == (short)DeviceType.ItouchPop)
            {
                maxSplit = Constants.MaxSendITouchPopUserCount;
            }

            var splitUsers = Helpers.SplitList(userLogProtocol.UserLogs, maxSplit);
            var publishCount = splitUsers.Count();
            var totalMsg = publishCount;
            var i = 0;
            foreach (var sendUsers in splitUsers)
            {
                var protocolData = remainCount == 0 && publishCount-- == 1
                    ? MakeUserProtocolData(sendUsers, userLogProtocol.ProtocolType, (int)UpdateFlag.Stop)
                    : MakeUserProtocolData(sendUsers, userLogProtocol.ProtocolType);

                var message = protocolData.ToString();
                var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{userLogProtocol.IcuAddress}";

                if (groupLength == 1)
                {
                    _queueService.Publish(routingKey, message);
                }
                else
                {
                    _queueService.SendGroupMessage(deviceAddress: accessGroupDevice.Icu.DeviceAddress,
                        protocolData.MsgId, message: message, topic: routingKey,
                        groupMsgId: groupMsgId, groupIndex: groupIndex + i, groupLength: groupLength,
                        processId: processId, actionType: actionType);
                }

                i++;
            }
        }

        /// <summary>
        /// Send visitor information to device
        /// </summary>
        /// <param name="accessGroupDevice"></param>
        /// <param name="isAddUser"></param>
        /// <param name="visit"></param>
        public void SendVisitor(AccessGroupDevice accessGroupDevice, bool isAddUser = true, Visit visit = null)
        {
            if (visit != null)
            {
                var card = _unitOfWork.AppDbContext.Card.FirstOrDefault(c => !c.IsDeleted && c.VisitId == visit.Id);
                var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{accessGroupDevice.Icu.DeviceAddress}";

                var protocolData = new UserProtocolData()
                {
                    MsgId = Guid.NewGuid().ToString(),
                    Type = isAddUser ? Constants.Protocol.AddUser : Constants.Protocol.DeleteUser,
                    Sender = _httpContext.User.GetUsername()
                };
                var userProtocolHeaderData = new UserProtocolHeaderData
                {
                    Total = 1,
                    UpdateFlag = 1,
                    Users = new List<UserProtocolDetailData>
                    {
                        new UserProtocolDetailData()
                        {
                            EmployeeNumber = visit.VisitorName,
                            UserName = visit.VisitorName,
                            DepartmentName = visit.VisitorDepartment,
                            CardId = card.CardId,
                            IssueCount = 0,
                            AdminFlag = (short) (card.IsMasterCard ? 1 : 0),
                            EffectiveDate = visit.StartDate.ToString(Constants.DateTimeFormat.DdMMyyyy),
                            ExpireDate = visit.EndDate.ToString(Constants.DateTimeFormat.DdMMyyyy),
                            AntiPassBack = card.CardStatus,
                            Timezone =  accessGroupDevice.Tz?.Position ?? 1,
                            Password = ""
                        }
                    }
                };
                protocolData.Data = userProtocolHeaderData;

                _queueService.Publish(routingKey, protocolData.ToString());

            }
        }

        /// <summary>
        /// Publish the delete all user message
        /// </summary>
        public void SendDeleteAllCardsForUser(AccessGroupDevice accessGroupDevice, List<User> users = null,
            string groupMsgId = "", bool isPublish = true, bool isSave = true)
        {
            if (users == null)
            {
                users = _unitOfWork.UserRepository
                        .GetAssignUsers(_httpContext.User.GetCompanyId(), accessGroupDevice.AccessGroupId).ToList();
            }

            var userLogs = AddUserLog(accessGroupDevice.IcuId, accessGroupDevice.TzId, users, ActionType.Delete);
            //Add user log protocol data
            var userLogProtocol = new UserLogProtocolData
            {
                IcuAddress = accessGroupDevice.Icu.DeviceAddress,
                UserLogs = userLogs,
                ProtocolType = Constants.Protocol.DeleteUser
            };
            if (userLogProtocol.UserLogs == null || !userLogProtocol.UserLogs.Any())
            {
                return;
            }

            var maxSplit = Convert.ToInt32(_configuration[Constants.MaxSplitUserInICU]);
            if (accessGroupDevice.Icu.DeviceType == (short)DeviceType.ItouchPop)
            {
                maxSplit = 1000;
            }

            var splitUsers = Helpers.SplitList(userLogProtocol.UserLogs, maxSplit);
            var publishCount = splitUsers.Count();
            foreach (var sendUsers in splitUsers)
            {
                var protocolData = publishCount == 1 ? MakeUserProtocolData(sendUsers, userLogProtocol.ProtocolType, (int)UpdateFlag.Stop) : MakeUserProtocolData(sendUsers, userLogProtocol.ProtocolType);
                var message = protocolData.ToString();
                var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{userLogProtocol.IcuAddress}";

                if (isPublish)
                {
                    _queueService.Publish(routingKey, message);
                }
            }
        }

        public List<CardModel> GetCardListByUserId(int userId)
        {
            var cards = _unitOfWork.CardRepository.GetByUserId(_httpContext.User.GetCompanyId(), userId);
            var cardModelList = new List<CardModel>();
            foreach (var card in cards)
            {
                var cardModel = Mapper.Map<CardModel>(card);
                cardModelList.Add(cardModel);
            }
            return cardModelList;
        }

        public void SendIdentificationToDeviceVisitor(AccessGroupDevice agDevice, Visit visitor, Card card, bool isAdd)
        {

            var routingKey = $"{Constants.RabbitMq.AccessControlTopic}.{agDevice.Icu.DeviceAddress}";

            var protocolData = new UserProtocolData()
            {
                MsgId = Guid.NewGuid().ToString(),
                Type = isAdd ? Constants.Protocol.AddUser : Constants.Protocol.DeleteUser,
                Sender = _httpContext.User.GetUsername()
            };
            var visitProtocolHeaderData = new UserProtocolHeaderData
            {
                Total = 1,
                UpdateFlag = 1,
                Users = new List<UserProtocolDetailData>
                {
                    new UserProtocolDetailData()
                    {
                        EmployeeNumber = visitor.VisitorName,
                        UserName = visitor.VisitorName,
                        DepartmentName = visitor.VisitorDepartment,
                        CardId = card.CardId,
                        IssueCount = 0,
                        AdminFlag = (short) (card.IsMasterCard ? 1 : 0),
                        EffectiveDate = visitor.StartDate.ToString(Constants.DateTimeFormat.DdMMyyyy),
                        ExpireDate = visitor.EndDate.ToString(Constants.DateTimeFormat.DdMMyyyy),
                        AntiPassBack = card.CardStatus,
                        Timezone =  agDevice.Tz?.Position ?? 1,
                        Password = ""
                    }
                }
            };
            protocolData.Data = visitProtocolHeaderData;

            _queueService.Publish(routingKey, protocolData.ToString());
        }
    }
}