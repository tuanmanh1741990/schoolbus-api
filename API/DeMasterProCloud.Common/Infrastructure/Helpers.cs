﻿using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Text;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using System.Globalization;
using System.Security.Cryptography;
using System.Web;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json.Linq;

namespace DeMasterProCloud.Common.Infrastructure
{
    public static class Helpers
    {
        private static readonly Random Random = new Random();
        /// <summary>
        /// Check a file extension is in valid extensions or not.
        /// </summary>
        /// <param name="extension"></param>
        /// <param name="validExtensions"></param>
        /// <returns></returns>
        public static bool IsValidImage(string extension, params string[] validExtensions)
        {
            if (string.IsNullOrEmpty(extension))
            {
                return false;
            }
            return validExtensions.Contains(extension);
        }

        /// <summary>
        /// Parse file to byte array
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static byte[] ParseToByArrayAsync(IFormFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                file.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// Convert date  to specify setting date string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateString(this DateTime date)
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];
            return date.ToString(dateFormat);
        }
        
        public static DateTime ConvertToUserTimeZoneReturnDate(this DateTime date, string userTimeZone = "")
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];

            if (!String.IsNullOrEmpty(userTimeZone))
            {
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(userTimeZone);
                    DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(date.ToUniversalTime(), cstZone);
                    return cstTime;
                }
                catch (TimeZoneNotFoundException)
                {
                }
            }
            return date; 
        }
        
        public static DateTime? ConvertToUserTimeZoneReturnNullDate(this DateTime date, string userTimeZone = "")
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];

            if (!String.IsNullOrEmpty(userTimeZone))
            {
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(userTimeZone);
                    DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(date.ToUniversalTime(), cstZone);
                    return cstTime;
                }
                catch (TimeZoneNotFoundException)
                {
                }
            }
            return date; 
        }
        
        public static string ConvertToUserTimeZone(this DateTime date, string userTimeZone = "")
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];

            if (!String.IsNullOrEmpty(userTimeZone))
            {
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(userTimeZone);
                    DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(date.ToUniversalTime(), cstZone);
                    return cstTime.ToString(dateFormat);
                }
                catch (TimeZoneNotFoundException)
                {
                }
            }
            return date.ToString(dateFormat); 
        }

        public static double ConvertDateTimeToEpoch(this DateTime date, string userTimeZone = "")
        {
            if (!String.IsNullOrEmpty(userTimeZone))
            {
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(userTimeZone);
                    DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(date.ToUniversalTime(), cstZone);
                    return (cstTime - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
                }
                catch (TimeZoneNotFoundException)
                {
                }
            }
            return (date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds; 
        }

        /// <summary>
        /// Convert date  to specify setting date string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateTimeString(this DateTime date)
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateTimeServerFormat + ":" + culture];
            return date.ToString(dateFormat);
        }
        
        public static double ToSettingDateTimeUnique(this DateTime date)
        {
            var epoch = (date.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            return epoch;
        }


        /// <summary>
        /// Convert date  to specify setting date without second string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateTimeWithoutSecString(this DateTime date)
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateTimeWithoutSecServerFormat + ":" + culture];
            return date.ToString(dateFormat);
        }

        /// <summary>
        /// Convert date  to specify setting date string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateTimeString(this DateTime? date)
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateTimeServerFormat + ":" + culture];
            return date?.ToString(dateFormat);
        }


        /// <summary>
        /// Convert date  to specify setting date string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateTimeStringLocal(this DateTime? date)
        {
            var dateFormat = Constants.DateTimeFormat.DatetimeServerFormat;
            return date?.ToString(dateFormat);
        }

        /// <summary>
        /// Convert date  to specify setting date string
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateTimeStringLocal(this DateTime date)
        {
            var dateFormat = Constants.DateTimeFormat.DatetimeServerFormat;
            return date.ToString(dateFormat);
        }

        /// <summary>
        /// Convert date  to specify setting date string
        /// </summary>
        /// <param name="date"></param>_httpContext.User.GetTimZone()
        /// <returns></returns>
        public static string ToSettingDateTimeExpireDate(this DateTime? date)
        {
            var dateFormat = Constants.DateTimeFormat.DatetimeServerFormat;
            return date?.ToString(dateFormat);
        }

        /// <summary>
        /// Convert date to specify setting date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string ToSettingDateString(this DateTime? date)
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];
            return date?.ToString(dateFormat);
        }

        /// <summary>
        /// Convert date only to end date
        /// </summary>
        /// <param name="dateOnly"></param>
        /// <returns></returns>
        public static DateTime ToEndDateTime(string dateOnly)
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            string dateFormat = ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];
            var dateTime = DateTime.ParseExact(dateOnly, dateFormat, CultureInfo.InvariantCulture);
            return dateTime.AddHours(23).AddMinutes(59).AddSeconds(59);
        }

        /// <summary>
        /// Get date server format by current culture
        /// </summary>
        /// <returns></returns>
        public static string GetDateServerFormat()
        {
            var culture = Thread.CurrentThread.CurrentCulture.Name;
            return ApplicationVariables.Configuration[Constants.DateServerFormat + ":" + culture];
        }

        /// <summary>
        /// Get company id
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int GetCompanyId(this ClaimsPrincipal user)
        {
            return int.Parse(user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.CompanyId)?.Value);
        }

        /// <summary>
        /// Get company code
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetCompanyCode(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.CompanyCode)?.Value;
        }

        /// <summary>
        /// Get company code
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetUsername(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.Username)?.Value;
        }

        /// <summary>
        /// Get company code
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetCurrentUsername(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(m => m.Type == ClaimTypes.Name)?.Value;
        }

        /// <summary>
        /// Get company name
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetCompanyName(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.CompanyName)?.Value;
        }

        /// <summary>
        /// Get account type
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static short GetAccountType(this ClaimsPrincipal user)
        {
            short.TryParse(
                user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.AccountType)?.Value, out var value);
            return value;
        }

        /// <summary>
        /// Get account id
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static int GetAccountId(this ClaimsPrincipal user)
        {
            int.TryParse(
                user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.AccountId)?.Value, out var value);
            return value;
        }

        /// <summary>
        /// Get bearer token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string GetBearerToken(this ClaimsPrincipal user)
        {
            return user.Claims.FirstOrDefault(m => m.Type == Constants.ClaimName.BearerToken)?.Value;
        }

        /// <summary>
        /// To Ascii
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string ToAscii(string text)
        {
            var asciiBytes = Encoding.ASCII.GetBytes(text);
            return asciiBytes.Aggregate<byte, string>(null, (current, ascii) => current + ascii.ToString());
        }

        /// <summary>
        /// Generate company code
        /// </summary>
        /// <returns></returns>
        public static string GenerateCompanyCode()
        {
            var index = Random.Next(Constants.Alphabet.Length);
            var firstChar = Constants.Alphabet[index];

            var randomNumber = Enumerable.Range(0, 9)
                .OrderBy(x => Random.Next())
                .Take(6);

            return $"{firstChar}{string.Join(string.Empty, randomNumber)}";
        }

        /// <summary>
        /// Generate random string
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string GenerateRandomString(int length)
        {
            return new string(Enumerable.Repeat(Constants.AlphabetAndNumber, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        /// <summary>
        /// Get status list with default value
        /// </summary>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static IEnumerable<SelectListItem> GetStatusList(short? selected = null)
        {
            return EnumHelper.ToSelectList<Status>(selected);
        }

        /// <summary>
        /// Generate encrypt key
        /// </summary>
        /// <param name="icuAddress"></param>
        /// <returns></returns>
        public static string GenerateEncryptKey(string icuAddress)
        {
            var encryptKey = ApplicationVariables.Configuration[Constants.Settings.EncryptKey];
            EnsureMapLengthString(ref encryptKey, ref icuAddress);
            var combineString = CombineString(encryptKey, icuAddress);
            var binaryString = StringToBinaryString(combineString);

            var lastChar = binaryString[binaryString.Length - 1];
            var result = binaryString.Remove(binaryString.Length - 1);
            result = result.Insert(0, lastChar.ToString());
            return BinaryStringToHexString(result);
        }

        /// <summary>
        /// Hex to Ascii
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static string Hex2Ascii(string hex)
        {
            var res = string.Empty;
            for (var a = 0; a < hex.Length; a = a + 2)
            {
                var char2Convert = hex.Substring(a, 2);
                var n = Convert.ToInt32(char2Convert, 16);
                var c = (char)n;
                res += c.ToString();
            }
            return res;
        }

        /// <summary>
        /// Ascii to hex
        /// </summary>
        /// <param name="ascii"></param>
        /// <returns></returns>
        public static string AsciiToHex(string ascii)
        {
            var sb = new StringBuilder();
            var inputBytes = Encoding.UTF8.GetBytes(ascii);
            foreach (var b in inputBytes)
            {
                sb.Append(string.Format("{0:x2}", b));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Ensure map length of 2 strings
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        public static void EnsureMapLengthString(ref string str1, ref string str2)
        {
            if (str1.Length > str2.Length)
            {
                var subLength = str1.Length - str2.Length;

                for (var i = 0; i < subLength; i++)
                {
                    str2 += str2[i];
                }
                EnsureMapLengthString(ref str1, ref str2);
            }
            else
            {
                var subLength = str2.Length - str1.Length;
                if (subLength == 0)
                    return;

                for (var i = 0; i < subLength; i++)
                {
                    str1 += str1[i];
                }
                EnsureMapLengthString(ref str2, ref str1);
            }
        }

        /// <summary>
        /// Combine string
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        public static string CombineString(string str1, string str2)
        {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(str1) && !string.IsNullOrEmpty(str2))
            {
                for (var i = 0; i < str1.Length; i++)
                {
                    sb.Append(str1[i]);
                    sb.Append(str2[i]);
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// String to binary string
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string StringToBinaryString(string data)
        {
            var sb = new StringBuilder();
            foreach (char c in data)
            {
                sb.Append(Convert.ToString(c, 2).PadLeft(8, '0'));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Binary string to hex string
        /// </summary>
        /// <param name="binary"></param>
        /// <returns></returns>
        public static string BinaryStringToHexString(string binary)
        {
            var sb = new StringBuilder(binary.Length / 8 + 1);
            var mod4Len = binary.Length % 8;
            if (mod4Len != 0)
            {
                // pad to length multiple of 8
                binary = binary.PadLeft(((binary.Length / 8) + 1) * 8, '0');
            }
            for (var i = 0; i < binary.Length; i += 8)
            {
                var eightBits = binary.Substring(i, 8);
                sb.AppendFormat("{0:X2}", Convert.ToByte(eightBits, 2));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Hex string to byte array
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] HexStringToByteArray(string hex)
        {
            var numberChars = hex.Length;
            var bytes = new byte[numberChars / 2];
            for (var i = 0; i < numberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            }
            return bytes;
        }

        /// <summary>
        /// Convert byte array to little endian
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static int ByteArrayToIntBigEndian(byte[] data, int startIndex)
        {
            return (data[startIndex] << 24)
                   | (data[startIndex + 1] << 16)
                   | (data[startIndex + 2] << 8)
                   | data[startIndex + 3];
        }

        /// <summary>
        /// Convert byte array to big endian
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        public static int ByteArrayToIntLittleEndian(byte[] data, int startIndex)
        {
            return (data[startIndex + 3] << 24)
                   | (data[startIndex + 2] << 16)
                   | (data[startIndex + 1] << 8)
                   | data[startIndex];
        }

        /// <summary>
        /// Generates a Random Password
        /// respecting the given strength requirements.
        /// </summary>
        /// <param name="opts">A valid PasswordOptions object
        /// containing the password strength requirements.</param>
        /// <returns>A random password</returns>
        public static string GenerateRandomPassword(PasswordOptions opts = null)
        {
            if (opts == null) opts = new PasswordOptions()
            {
                RequiredLength = 8,
                RequiredUniqueChars = 4,
                RequireDigit = true,
                RequireLowercase = true,
                RequireNonAlphanumeric = false,
                RequireUppercase = true
            };

            var randomChars = Constants.RandomChars;
            var rand = new Random(Environment.TickCount);
            var chars = new List<char>();

            if (opts.RequireUppercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

            if (opts.RequireLowercase)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);

            if (opts.RequireDigit)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);

            if (opts.RequireNonAlphanumeric)
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (var i = chars.Count; i < opts.RequiredLength
                || chars.Distinct().Count() < opts.RequiredUniqueChars; i++)
            {
                var rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }

        /// <summary>
        /// Comparre date
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static bool CompareDate(string startDate, string endDate)
        {
            if (DateTimeHelper.IsDateTime(startDate) && DateTimeHelper.IsDateTime(endDate))
            {
                return Convert.ToDateTime(endDate).Date.Subtract(Convert.ToDateTime(startDate).Date).Days >= 0;
            }
            return true;
        }

        /// <summary>
        /// Check string is contains unicode
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsUnicode(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return false;
            }
            var asciiBytesCount = Encoding.ASCII.GetByteCount(input);
            var unicodeBytesCount = Encoding.UTF8.GetByteCount(input);
            return asciiBytesCount != unicodeBytesCount;
        }
        
        /// <summary>
        /// Check string is valid timezone
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsValidTimeZone(string input)
        {
            try
            {
                if (String.IsNullOrEmpty(input))
                {
                    return true;
                }
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(input);
                return true;
            }
            catch (TimeZoneNotFoundException)
            {
                return false;
            }
        }

        /// <summary>
        /// Validate user name
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static bool IsValidUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                return false;
            }
            return new Regex(@"^[A-Za-z\d_-]+$").IsMatch(userName);
        }

        /// <summary>
        /// Check string is IP Address
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <returns></returns>
        public static bool IsIpAddress(string ipAddress)
        {
            //Match pattern for IP address    
            var pattern =
                @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
            //Regular Expression object    
            var regex = new Regex(pattern);
            //check to make sure an ip address was provided
            if (string.IsNullOrEmpty(ipAddress))
                //returns false if IP is not provided
                return false;
            return regex.IsMatch(ipAddress, 0);
        }

        /// <summary>
        /// Get from, to datetime for report
        /// </summary>
        /// <param name="fromToDate"></param>
        /// <param name="fromToTime"></param>
        /// <param name="isToDateTime"></param>
        /// <returns></returns>
        public static DateTime GetFromToDateTime(string fromToDate, string fromToTime, bool isToDateTime)
        {
            var formatDatetime =
                   ApplicationVariables.Configuration[
                       Constants.DateServerFormat + ":" + Thread.CurrentThread.CurrentCulture.Name];

            var dateTimeFromTo = fromToDate;
            if (DateTime.TryParse(fromToTime, out _))
            {
                dateTimeFromTo += " " + fromToTime;
                formatDatetime += " h:mm tt";
            }
            else
            {
                if (!isToDateTime)
                {
                    dateTimeFromTo += " 00:00:00";
                }
                else
                {
                    dateTimeFromTo += " 23:59:59";
                }
                formatDatetime += " HH:mm:ss";
            }

            //formatDatetime = "mm.dd.yyyy hh:mm:ss";
            return DateTime.ParseExact(dateTimeFromTo, formatDatetime,
                CultureInfo.InvariantCulture);
        }
        
        public static DateTime GetFromToDateTimeConvert(string fromToDate, string fromToTime, bool isToDateTime)
        {
            var formatDatetime = "yyyy-MM-dd";

                var dateTimeFromTo = fromToDate;
            if (DateTime.TryParse(fromToTime, out _))
            {
                dateTimeFromTo += " " + fromToTime;
                formatDatetime += " HH:mm:ss";
            }
            else
            {
                if (!isToDateTime)
                {
                    dateTimeFromTo += " 00:00:00";
                }
                else
                {
                    dateTimeFromTo += " 23:59:59";
                }
                formatDatetime += " HH:mm:ss";
            }

            //formatDatetime = "mm.dd.yyyy hh:mm:ss";
            return DateTime.ParseExact(dateTimeFromTo, formatDatetime,
                CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get min of missing number from list
        /// </summary>
        /// <param name="curUserCodes"></param>
        /// <returns></returns>
        public static int GetMinMissingNumberFromList(List<int> curUserCodes)
        {
            if (!curUserCodes.Any())
            {
                return 1;
            }

            var maxRange = 0;
            if (curUserCodes.Any())
            {
                maxRange = curUserCodes.Max();
            }

            var rangeUserCode = Enumerable.Range(1, maxRange);
            var availableUserCodes = rangeUserCode.Except(curUserCodes).ToList();
            if (availableUserCodes.Any())
            {
                return availableUserCodes.Min();
            }

            return maxRange + 1;
        }

        /// <summary>
        /// Get real value of string in the case it's a number having leading Zeros
        /// </summary>
        /// <param name="numberString"></param>
        /// <returns></returns>
        public static string GetRealValueOfNumberString(string numberString)
        {
            if (int.TryParse(numberString, out int _))
            {
                return numberString.TrimStart('0');
            }
            return numberString;
        }

        /// <summary>
        /// Get url login
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="companyCode"></param>
        /// <returns></returns>
        public static string GetLoginPath(HttpContext httpContext, string companyCode)
        {
            return httpContext.Request.Scheme
                   + "://" + httpContext.Request.Host.Value
                   + "/account/login?companycode=" + companyCode;
        }

        /// <summary>
        /// Get current domain
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static string GetCurrentDomain(HttpContext httpContext)
        {
            return httpContext.Request.Scheme + "://" + httpContext.Request.Host.Value;
        }

        /// <summary>
        /// Get domain frontend
        /// </summary>
        /// <returns></returns>
        public static string GetDomainFrontEnd(HttpContext httpContext)
        {
            return httpContext.Request.Scheme + "://" + ApplicationVariables.Configuration[Constants.Settings.DomainFrontEnd];
        }

        /// <summary>
        /// Replacing query string elements
        /// </summary>
        /// <param name="currentPageUrl"></param>
        /// <param name="paramToReplace"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public static string ReplaceQueryStringParam(string currentPageUrl, string paramToReplace, string newValue)
        {
            var urlWithoutQuery = currentPageUrl.IndexOf('?') >= 0
                ? currentPageUrl.Substring(0, currentPageUrl.IndexOf('?'))
                : currentPageUrl;

            var queryString = currentPageUrl.IndexOf('?') >= 0
                ? currentPageUrl.Substring(currentPageUrl.IndexOf('?'))
                : null;

            var queryParamList = queryString != null
                ? HttpUtility.ParseQueryString(queryString)
                : HttpUtility.ParseQueryString(string.Empty);

            if (queryParamList[paramToReplace] != null)
            {
                queryParamList[paramToReplace] = newValue;
            }
            else
            {
                queryParamList.Add(paramToReplace, newValue);
            }
            return $"{urlWithoutQuery}?{queryParamList}";
        }

        /// <summary>
        /// Build query string to url
        /// </summary>
        /// <param name="currentPageUrl"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string BuildQueryStringParam(string currentPageUrl, Dictionary<string, object> parameters)
        {
            var query =
                $"?{string.Join("&", parameters.Select(kvp => $"{kvp.Key}={kvp.Value}"))}";
            return $"{currentPageUrl}{query}";
        }

        /// <summary>
        /// Compare two files
        /// </summary>
        /// <param name="file1"></param>
        /// <param name="file2"></param>
        /// <returns></returns>
        public static bool FileEquals(byte[] file1, byte[] file2)
        {
            return file1.Length == file2.Length && !file1.Where((t, i) => t != file2[i]).Any();
        }

        /// <summary>
        /// Compare two files
        /// </summary>
        /// <param name="path1"></param>
        /// <param name="path2"></param>
        /// <returns></returns>
        public static bool FileEquals(string path1, string path2)
        {
            var file1 = File.ReadAllBytes(path1);
            var file2 = File.ReadAllBytes(path2);
            return FileEquals(file1, file2);
        }

        /// <summary>
        /// Splitting a list or collection into chunks
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="locations"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static IEnumerable<List<T>> SplitList<T>(List<T> locations, int size = Constants.Settings.MaxUserSendToIcu)
        {
            for (var i = 0; i < locations.Count; i += size)
            {
                yield return locations.GetRange(i, Math.Min(size, locations.Count - i));

            }
        }

        /// <summary>
        /// To demonstrate extraction of file extension from base64 string.
        /// </summary>
        /// <param name="base64String">base64 string.</param>
        /// <returns>Henceforth file extension from string.</returns>
        public static string GetFileExtension(string base64String)
        {
            try
            {
                if (!string.IsNullOrEmpty(base64String))
                {
                    if (base64String.StartsWith("data:"))
                    {
                        var regex = new Regex(@"/(.+?);");
                        var mc = regex.Matches(base64String);
                        return $".{mc[0].Groups[1].Value}";
                    }

                    var data = base64String.Substring(0, 5);
                    switch (data.ToUpper())
                    {
                        case "IVBOR":
                            return ".png";
                        case "/9J/4":
                            return ".jpg";
                        case "AAAAF":
                            return ".mp4";
                        case "JVBER":
                            return ".pdf";
                        case "AAABA":
                            return ".ico";
                        case "UMFYI":
                            return ".rar";
                        case "E1XYD":
                            return ".rtf";
                        case "U1PKC":
                            return ".txt";
                        case "MQOWM":
                        case "77U/M":
                            return ".srt";
                        default:
                            return string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get status for device
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public static string GetIcuStatus(int status)
        {
            switch (status)
            {
                case 1:
                    return IcuStatus.Connected.GetDescription();
                default:
                    return IcuStatus.Disconneted.GetDescription();
            }
        }

        /// <summary>
        /// Get string from value
        /// </summary>
        /// <param name="value"></param>
        public static string GetStringFromValueSetting(string value)
        {
            try
            {
                var list = JArray.Parse(value).ToList();
                return list[0].ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Check format for date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static bool DateFormatCheck(string strDate)
        {
            try
            {
                Convert.ToDateTime(strDate);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// Get device address from topic
        /// </summary>
        /// <param name="topic"></param>
        /// <returns></returns>
        public static string GetDeviceAddress(string topic)
        {
            var result = string.Empty;
            if (!string.IsNullOrEmpty(topic))
            {
                var splits = topic.Split(".");
                result = splits.Last();
            }

            return result;
        }

        /// <summary>
        /// Get event type from command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="state"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static EventType GetEventType(string command,int state,string status)
        {
            var eventType = EventType.DeviceInstructionOpen;
            switch (command)
            {
                case Constants.CommandType.Open:
                    eventType = EventType.DeviceInstructionOpen;
                    break;
                case Constants.CommandType.SetTime:
                    eventType = EventType.DeviceInstructionSettime;
                    break;
                case Constants.CommandType.Reset:
                    eventType = EventType.DeviceInstructionReset;
                    break;
                case Constants.CommandType.ForceOpen:
                    eventType = EventType.DeviceInstructionForceOpen;
                    break;
                case Constants.CommandType.ForceClose:
                    eventType = EventType.DeviceInstructionForceClose;
                    break;
                case Constants.CommandType.Release:
                    eventType = EventType.DeviceInstructionRelease;
                    break;
                case Constants.CommandType.DeleteAllUsers:
                    eventType = EventType.DeviceInstructionDeleteAllUserEvent;
                    break;
                case Constants.CommandType.DeleteAllEvents:
                    eventType = EventType.DeviceInstructionDeleteAllUserEvent;
                    break;
                case Constants.CommandType.UpdateDeviceState:
                    switch (state)
                    {
                        case 0:
                            eventType = EventType.ValidDoor;
                            break;
                        case 1:
                            eventType = EventType.InvalidDoor;
                            break;
                    }
                    break;
                case Constants.CommandType.UpdateFirmware:
                    if (status.Equals(Constants.MessageType.Success,StringComparison.OrdinalIgnoreCase))
                    {
                        eventType = EventType.FirmwareApplicationUpdate;
                    }
                    else if (status.Equals(Constants.MessageType.Failure, StringComparison.OrdinalIgnoreCase))
                    {
                        eventType = EventType.FirmwareDownloadFailed;
                    }
                    break;
            }

            return eventType;
        }

        /// <summary>
        /// Mask keypad
        /// </summary>
        /// <param name="keyPadPw"></param>
        /// <returns></returns>
        public static string MaskKeyPadPw(string keyPadPw)
        {
            if (string.IsNullOrEmpty(keyPadPw))
            {
                return string.Empty;
            }
            const int numShow = 1;
            var maskKeypad = keyPadPw.Substring(numShow, keyPadPw.Length - numShow);
            var visible = keyPadPw.Substring(0, numShow);
            return visible + RenderStar(maskKeypad);
        }

        /// <summary>
        /// Mask keypad
        /// </summary>
        /// <param name="keyPadPw"></param>
        /// <returns></returns>
        public static string MaskAndDecrytorKeyPadPw(string keyPadPw)
        {
            if (string.IsNullOrEmpty(keyPadPw))
            {
                return string.Empty;
            }

            var deKeyPad = Encryptor.Decrypt(keyPadPw,
                ApplicationVariables.Configuration[Constants.Settings.EncryptKey]);
            return MaskKeyPadPw(deKeyPad);
        }

        /// <summary>
        /// Render star
        /// </summary>
        /// <param name="pass"></param>
        /// <returns></returns>
        private static string RenderStar(string pass)
        {
            var result = string.Empty;
            foreach (var unused in pass)
            {
                result = result + "*";
            }
            return result;
        }

        /// <summary>
        /// Remove message id from current pending list when receive response
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pendingList"></param>
        /// <returns></returns>
        public static void RemoveMsgIdFromPendingList(byte[] data,ref List<string> pendingList)
        {
            var json = Encoding.UTF8.GetString(data);
            JObject dataObj = JObject.Parse(json);
            var msgId = (string)dataObj["msgId"];
            //Check if msgID in current pending list messages then remove it 
            if (pendingList.Contains(msgId))
            {
                //Remove item from list;
                pendingList.Remove(msgId);
            }
        }

        public static string GetLocalTimeZone()
        {
            TimeZoneInfo localZone = TimeZoneInfo.Local;
            return localZone.Id;
        }
        
        // Convert Date to universal time and convert by timezone
        public static string ConvertDateTimeByTimeZone(string timeZone)
        {
            if (!String.IsNullOrEmpty(timeZone)){
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                    DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), cstZone);
                    return cstTime.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmssFff);
                }
                catch (TimeZoneNotFoundException)
                {
                    return DateTime.Now.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmssFff);
                }
            }
            return DateTime.Now.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmssFff);
        }
        
        // Convert Date Time to string by timezone
        public static string ConvertDateTimeToStringByTimeZone(DateTime datetime, string timeZone)
        {
            if (!String.IsNullOrEmpty(timeZone)){
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                    DateTime cstTime = TimeZoneInfo.ConvertTime(datetime.ToUniversalTime(), cstZone);
                    return cstTime.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
                }
                catch (TimeZoneNotFoundException)
                {
                    return datetime.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
                }
            }
            return datetime.ToString(Constants.DateTimeFormat.DdMMyyyyHHmmss);
        }
        
        public static string EncryptSecretCode(string plainText)
        {
            byte[] encrypted;

            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Encoding.UTF8.GetBytes(Constants.DynamicQr.Key);
                rijAlg.IV = Encoding.UTF8.GetBytes(ReverseString(Constants.DynamicQr.Key));
                rijAlg.Mode = CipherMode.CBC;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            // Return the encrypted bytes from the memory stream.
            var result = Combine(Encoding.UTF8.GetBytes(ReverseString(Constants.DynamicQr.Key)), encrypted);
            return BitConverter.ToString(result).Replace("-","");
        }
        
        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
        
        private static Random random = new Random();
        
        public static bool TryValidateWorkingType(string type)
        {
            if (String.IsNullOrEmpty(type))
            {
                return false;
            }
            
            String[] validDay = Constants.Attendance.ValidType.Split(',', ' ');
            if (!validDay.Contains(type))
            {
                return false;
            }
            return true;
        }

        public static bool TryValidateWorkingTime(string startDate, string endDate)
        {
            if (String.IsNullOrEmpty(startDate) || String.IsNullOrEmpty(endDate))
            {
                return false;
            }
            String[] strStart = startDate.Split(':', ' ');
            String[] strEnd = endDate.Split(':', ' ');
            var hourStart = Convert.ToInt32(strStart.FirstOrDefault());
            var hourEnd = Convert.ToInt32(strEnd.FirstOrDefault());
            if (hourStart < 0 || hourStart > 24)
            {
                return false;
            }

            if (hourEnd < hourStart || hourEnd > 24)
            {
                
                return false;
            }

            var minutesStart = Convert.ToInt32(strStart[strStart.Length - 1]);
            var minutesEnd = Convert.ToInt32(strEnd[strEnd.Length - 1]);
            
            if (minutesStart < 0 || minutesStart > 60)
            {
                return false;
            }
            
            if (minutesEnd < 0 || minutesEnd > 60)
            {
                return false;
            }
            
            return true;
        }

        public static bool TryValidateWorkingDay(string day)
        {
            if (String.IsNullOrEmpty(day))
            {
                return false;
            }
            
            String[] validDay = Constants.Attendance.ValidDays.Split(',', ' ');
            if (!validDay.Contains(day))
            {
                return false;
            }
            return true;
        }
        
        public static bool TryValidateWorkingDayDistinct(List<string> listDay)
        {
            String[] validDay = Constants.Attendance.ValidDays.Split(',', ' ');
            
            if(listDay.Count != listDay.Distinct().Count())
            {
                // Duplicates exist
                return false;
            }

            if (listDay.Count != validDay.Length)
            {
                return false;
            }
            return true;
        }
        public static int GetUtcOffSetHour(string timeZone)
        {
            if (!String.IsNullOrEmpty(timeZone)){
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                    var utc = cstZone.BaseUtcOffset.ToString().Split(":");
                    var newUtc = "";
                    if (utc[0].StartsWith("-"))
                    {
                        newUtc = utc[0] ;
                    }
                    else
                    {
                        newUtc = "+" + utc[0];
                    }
                    return Convert.ToInt32(newUtc);
                }
                catch (TimeZoneNotFoundException)
                {
                    return 0;
                }
            }

            return 0;
        }
        
        public static int GetUtcOffSetMinute(string timeZone)
        {
            if (!String.IsNullOrEmpty(timeZone)){
                try
                {
                    TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
                    var utc = cstZone.BaseUtcOffset.ToString().Split(":");
                    var newUtc = "";
                    if (utc[0].StartsWith("-"))
                    {
                        newUtc = "-" + utc[1] ;
                    }
                    else
                    {
                        newUtc = "+" + utc[1];
                    }
                    return Convert.ToInt32(newUtc);
                }
                catch (TimeZoneNotFoundException)
                {
                    return 0;
                }
            }

            return 0;
        }
        
        public static string GenerateCompanyKey()
        {
            const string chars = Constants.DynamicQr.AllowChars;

            var secretCode = new string(Enumerable.Repeat(chars, Constants.DynamicQr.LenghtOfSecretCode)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return secretCode;
        }
        
        public static string GenerateSalt(int len)
        {
            const string chars = Constants.DynamicQr.Salt;

            var secretCode = new string(Enumerable.Repeat(chars, Constants.DynamicQr.MaxByte-len)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return secretCode;
        }
        
        public static string GetNextWorkingDay(string day)
        {
            String[] validDay = Constants.Attendance.ValidDays.Split(',', ' ');
            int index = validDay.IndexOf(day);

            return validDay[index + 1];
        }
        
        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static int CheckStatusAttendance(double start, double end, double clockIn, double clockOut)
        {
            DateTime date = UnixTimeToDateTime(start);

            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                return (short) AttendanceType.Holiday;
            }
            else
            {
                // user late in 
                if(clockIn > start )
                {
                    if (clockOut.Equals(0.0) || clockOut > end)
                    {
                        return (short) AttendanceType.LateIn;
                    }
                    else if (clockOut < end)
                    {
                        return (short) AttendanceType.LateInEarlyOut;
                    }
                
                }
            
                // user clockin normal
                if (clockIn <= start)
                {
                    if (clockIn.Equals(0.0))
                    {
                        if (clockOut.Equals(0.0))
                        {
                            return (short) AttendanceType.AbsentNoReason;
                        }
                        else
                        {
                            return (short) AttendanceType.AbNormal;
                        }
                    }
                    else
                    {
                        if(clockOut.Equals(0.0))
                        {
                            return (short) AttendanceType.Normal;
                        }
                        else 
                        {
                            if(clockOut >= end)
                            {
                                return (short) AttendanceType.Normal;
                            }
                            if (clockOut < end)
                            {
                                return (short) AttendanceType.EarlyOut;
                            }
                        }
                    }
                }

            }
            
            
            return (short) AttendanceType.AbsentNoReason;
        }
        
        public static string GenerateRandomPassword(int len)
        {
            const string chars = Constants.DynamicQr.Salt;

            var secretCode = new string(Enumerable.Repeat(chars, Constants.DynamicQr.MaxByte-len)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return secretCode;
        }
        
        public static string GenerateRandomPasswordNumber(int len)
        {
            const string chars = Constants.DynamicQr.AllowCharsNumber;

            var secretCode = new string(Enumerable.Repeat(chars, len)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return secretCode;
        }
        
        public static DateTime UnixTimeToDateTime(double unixTime)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTime);
            return dtDateTime;
        }
    }
}
