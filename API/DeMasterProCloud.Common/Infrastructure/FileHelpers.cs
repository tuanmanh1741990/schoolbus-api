﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;

namespace DeMasterProCloud.Common.Infrastructure
{
    public static class FileHelpers
    {
        /// <summary>
        /// Convert from file to base 64
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string ConvertToBase64(IFormFile file)
        {
            string base64String;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();
                base64String = Convert.ToBase64String(fileBytes);
            }
            return base64String;
        }

        /// <summary>
        /// Convert from file to base 64
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string ConvertToHex(IFormFile file)
        {
            string hexString;
            using (var ms = new MemoryStream())
            {
                file.CopyTo(ms);
                var fileBytes = ms.ToArray();
                hexString = ByteArrayToString(fileBytes);
            }
            return hexString;
        }

        /// <summary>
        /// Convert from file to base 64
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static MemoryStream ConvertToStream(IFormFile file)
        {
            var ms = new MemoryStream();
            file.CopyTo(ms);
            return ms;
        }

        /// <summary>
        /// Convert from file to string array
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string[] ConvertToStringArray(IFormFile file)
        {
            string result;
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                result = reader.ReadToEnd();
            }

            return string.IsNullOrEmpty(result)
                ? null
                : result.Split('\n');
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        /// <summary>
        /// Get file extension from file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string GetFileExtension(IFormFile file)
        {
            var fileExtension = "";
            var fileName = file.FileName;
            if (fileName.Contains("."))
            {
                fileExtension = "." + fileName.Split(".").Last();
            }

            return fileExtension;
        }

        /// <summary>
        /// Get file name without extension and fullpath
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string GetFileNameWithoutExtension(IFormFile file)
        {
            
            //var fileExtension = "";
            var fileName = file.FileName;

            if (fileName.Contains("\\"))
            {
                fileName = fileName.Split("\\").Last();
            }

            if (fileName.Contains("."))
            {
                var fileExtension = "." + fileName.Split(".").Last();
                fileName = fileName.Substring(0, fileName.Length - fileExtension.Length);
            }

            return fileName;
        }

        /// <summary>
        /// Split file return list hex
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="chunkSize"></param>
        /// <returns></returns>
        public static List<string> SplitFile(IFormFile inputFile, int chunkSize)
        {
            var buffer = new byte[chunkSize];
            var listHex = new List<string>();
            using (var input = inputFile.OpenReadStream())
            {
                while (input.Position < input.Length)
                {
                    using (var ms = new MemoryStream())
                    {
                        int remaining = chunkSize, bytesRead;
                        while (remaining > 0 && (bytesRead = input.Read(buffer, 0,
                                   Math.Min(remaining, chunkSize))) > 0)
                        {
                            ms.Write(buffer, 0, bytesRead);
                            remaining -= bytesRead;
                        }
                        var fileBytes = ms.ToArray();
                        var hexString = ByteArrayToString(fileBytes);
                        listHex.Add(hexString);
                    }
                }
            }

            return listHex;
        }

        /// <summary>
        /// Get lastest fileName
        /// </summary>
        /// <param name="inputDirectoryPath"></param>
        /// <param name="deviceAddress"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetLastestFileName(string inputDirectoryPath, string deviceAddress,string type)
        {
            var reg = $"{Constants.ExportType.LoadAllUser}_{deviceAddress}";
            var directoryInfo = new DirectoryInfo(inputDirectoryPath);
            var files = directoryInfo.GetFiles().Where(f => f.Extension == ".csv" && f.Name.Contains(reg)).ToArray();

            var fileResult = files.OrderByDescending(x => x.CreationTime).FirstOrDefault();
            return fileResult != null ? fileResult.Name : string.Empty;
        }
    }
}
