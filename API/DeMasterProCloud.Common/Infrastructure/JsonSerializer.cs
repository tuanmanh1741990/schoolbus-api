﻿using System.Text;
using Newtonsoft.Json;

namespace DeMasterProCloud.Common.Infrastructure
{
    public class JsonSerializer : ISerializer
    {
        /// <summary>
        /// Serialize an object to byte array
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public byte[] Serialize(object obj)
        {
            if (obj == null)
            {
                return null;
            }

            var json = JsonConvert.SerializeObject(obj);
            return Encoding.UTF8.GetBytes(json);
        }

        /// <summary>
        /// DeSerialize byte array to an object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="arrBytes"></param>
        /// <returns></returns>
        public T DeSerialize<T>(byte[] arrBytes)
        {
            var json = Encoding.UTF8.GetString(arrBytes);
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
