﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Security;
using System.Transactions;
using DeMasterProCloud.Common.Infrastructure;
using EasyNetQ.Management.Client;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace DeMasterProCloud.Common.Infrastructure
{
    public class ApplicationVariables
    {
        public static List<QueueAndRoutingKey> QueueAndRoutingKeyList = new List<QueueAndRoutingKey>();
        // Dictionary stored channel that is used for handling messages from the device
        // We added item to this dictionary when device is register, then remove them when deleting device
        public static ConcurrentDictionary<string, List<IModel>> DeviceChannelList = new ConcurrentDictionary<string, List<IModel>>();
        public static IConnection RabbitMqConsumerConnection;
        public static IList<IConnection> RabbitMQConnections = new List<IConnection>();
        public static IConfiguration Configuration;
        public static ILoggerFactory LoggerFactory;
        // Dictionary that store message temporary when it is being sent to device.
        // Message will be remove from the dictionary when server received the response
        public static ConcurrentDictionary<string, string> SendingMessages = new ConcurrentDictionary<string, string>();

        // Dictionary to store pending list message that is sent via group messages
        // Key are device ID, value is list of message that send to device
        public static ConcurrentDictionary<string, List<string>> PendingGroupMessages = new ConcurrentDictionary<string, List<string>>();
        //public static List<string> DiscardMessageGroup = new List<string>;
        public static IHostingEnvironment env;
        public static void DebugSendingMessages()
        {
            foreach (KeyValuePair<string, string> entry in SendingMessages)
            {
                Console.Out.WriteLine(entry.Value);
            }
        }

        public static IConnection getAvailableConnection(IConfiguration configuration)
        {
            var factory = QueueHelper.GetConnectionFactory(configuration);
            if (RabbitMQConnections.Count == 0)
            {
                var connection = factory.CreateConnection("connection_name_1");
                RabbitMQConnections.Add(connection);

            }

            var managementClient = new ManagementClient(configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsHost),
                configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsUserName),
                configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsPassword));
            //var connections = managementClient.GetConnectionsAsync();
            //var connections = managementClient.GetConnectionsAsync();
            var task = managementClient.GetConnectionsAsync();
            task.Wait();
            var connections = task.Result;
            foreach (var conn in connections)
            {
                try
                {
                    long channels = conn.Channels;
                    string connectionName = (string)conn.ClientProperties.PropertiesDictionary["ConnectionName"];
                    // If the last connection has number of channels < 1000 create new connections
                    if (connectionName == ("connection_name_" + RabbitMQConnections.Count.ToString()))
                    {
                        Console.Out.WriteLine("connection.name = {0}", conn.Name);
                        Console.WriteLine("channels:\t{0}", conn.Channels);
                        Console.WriteLine("connection_name:\t{0}", conn.ClientProperties.PropertiesDictionary["ConnectionName"]);

                        if (channels >= Constants.RabbitMq.MaxChannelsPerConnection)
                        {
                            var connection = factory.CreateConnection("connection_name_" + (RabbitMQConnections.Count + 1).ToString());
                            RabbitMQConnections.Add(connection);
                        }

                    }
                }
                catch (Exception)
                {
                }
                    
            }
            Console.Out.WriteLine("connections.count = {0}", RabbitMQConnections.Count);
            //Always return the last connections
            return RabbitMQConnections[RabbitMQConnections.Count - 1];
            
        }
    }

    public class QueueAndRoutingKey
    {
        public string Queue { get; set; }
        public string RoutingKey { get; set; }
    }
}
