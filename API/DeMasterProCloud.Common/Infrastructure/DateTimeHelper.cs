﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;

namespace DeMasterProCloud.Common.Infrastructure
{
    public static class DateTimeHelper
    {
        public static double TimeSpanToDouble(string timeSpan)
        {
            var hour = timeSpan.Split(':')[0];
            var minute = timeSpan.Split(':')[1];
            if (!string.IsNullOrEmpty(hour) && !string.IsNullOrEmpty(minute))
            {
                if (double.Parse(hour) >= 24)
                {
                    hour = Constants.Settings.MaxTimezoneTimeHour;
                }
                if (double.Parse(minute) >= 60)
                {
                    minute = Constants.Settings.MaxTimezoneTimeMinute;
                }
                timeSpan = $"{hour}:{minute}";
            }
            return Convert.ToDouble(TimeSpan.Parse(timeSpan).TotalHours);
        }

        public static string DoubleToTimeSpan(double dou)
        {
            if (dou >= 24)
            {
                dou = Constants.Settings.MaxTimezoneTimeHourMinute;
            }
            return TimeSpan.FromHours(dou).ToString("h\\.mm");
        }

        /// <summary>
        /// Method checks if passed string is datetime
        /// </summary>
        /// <param name="dateTime">string text for checking</param>
        /// <returns>bool - if text is datetime return true, else return false</returns>
        public static bool IsDateTime(string dateTime)
        {
            // Allow DateTime string is null
            if (string.IsNullOrEmpty(dateTime))
            {
                return true;
            }

            return DateTime.TryParse(dateTime, out _);
        }

        /// <summary>
        /// Method checks if passed string is datetime
        /// </summary>
        /// <param name="dateTime">string text for checking</param>
        /// <param name="format"></param>
        /// <returns>bool - if text is datetime return true, else return false</returns>
        public static bool IsDateTime(string dateTime, string format)
        {

            // Allow DateTime string is null
            if (string.IsNullOrEmpty(dateTime))
            {
                return true;
            }

            return DateTime.TryParseExact(dateTime, format, CultureInfo.InvariantCulture, DateTimeStyles.None,
                out _);
        }

        /// <summary>
        /// Check expireDate and effective date
        /// </summary>
        /// <param name="expireDate"></param>
        /// <param name="effectiveDate"></param>
        /// <returns></returns>
        public static bool CheckDateTime(string expireDate, string effectiveDate)
        {
            var expire = Convert.ToDateTime(expireDate);
            var effective = Convert.ToDateTime(effectiveDate);
            return expire > effective;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        public static DateTime ConvertAccessTime(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return new DateTime();
            }
            return DateTime.ParseExact(date, Constants.DateTimeFormat.DdMMyyyyHHmmss, null);
        }
        
        // Convert Date Time to universal time and convert by timezone
        public static DateTime ConvertDateTimeByTimeZoneToSystemTimeZone(string date, string timeZone)
        {
            if (string.IsNullOrEmpty(date))
            {
                return new DateTime();
            }
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            var utc = cstZone.BaseUtcOffset.ToString().Split(":");
            var newUtc = "";
            if (utc[0].StartsWith("-"))
            {
                newUtc = utc[0] + utc[1];
            }
            else
            {
                newUtc = "+" + utc[0] + utc[1];
            }

            var newDate = date + newUtc;
            var convertDateTime = DateTime.ParseExact(newDate, Constants.DateTimeFormat.ddMMyyyyHHmmsszzz, null).ToUniversalTime();
            return convertDateTime;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        public static DateTime ConvertRecoveryDateTime(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return new DateTime();
            }
            return DateTime.ParseExact(date, Constants.DateTimeFormat.MMddyyyyHHmmss, null);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        public static DateTime ConvertToDateTime(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return new DateTime();
            }
            return DateTime.ParseExact(date, Constants.DateTimeFormat.DdMMyyyy, null);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static string ChangeFormatDatetime(string datetime)
        {
            if (string.IsNullOrEmpty(datetime))
            {
                return string.Empty;
            }
            var date = DateTime.ParseExact(datetime, Constants.DateTimeFormat.DdMdYyyy, null);
            return date.ToString(Constants.DateTimeFormat.DdMdYyyyFormat);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        public static DateTime ConvertUpdateTime(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return new DateTime();
            }
            return DateTime.ParseExact(date, Constants.DateTimeFormat.DdMMyyyyHHmmssFfffff, null);
        }


        public static DateTime ConverStringToDateTime(string date)
        {
            if (string.IsNullOrEmpty(date))
            {
                return new DateTime();
            }
            return DateTime.ParseExact(date, Constants.DateTimeFormat.YyyyyMdDdFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Get list date from range date
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static List<DateTime> GetListRangeDate(DateTime startDate, DateTime endDate)
        {
            return Enumerable.Range(0, 1 + endDate.Subtract(startDate).Days)
                .Select(offset => startDate.AddDays(offset))
                .ToList();
        }

        public static DateTime ConvertIsoToDateTime(string startDate)
        {
            CultureInfo culture = CultureInfo.InvariantCulture;
            DateTime time;
            DateTime.TryParse(startDate, culture, DateTimeStyles.None, out time);
            return time;
        }


    }
}