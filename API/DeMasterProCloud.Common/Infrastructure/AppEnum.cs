﻿using DeMasterProCloud.Common.Resources;

namespace DeMasterProCloud.Common.Infrastructure
{
    public enum DeviceType
    {
        [Localization(nameof(DeviceResource.lblIcu300N), typeof(DeviceResource))]
        Icu300N,
        [Localization(nameof(DeviceResource.lbliTouchPop), typeof(DeviceResource))]
        ItouchPop,
        [Localization(nameof(DeviceResource.lblDesktopApp), typeof(DeviceResource))]
        DesktopApp,
    }

    public enum IcuDeviceTarget
    {
        [Localization(nameof(DeviceResource.lblIcu300N), typeof(DeviceResource))]
        Icu300N,
        [Localization(nameof(DeviceResource.lblDe950), typeof(DeviceResource))]
        De950,
        [Localization(nameof(DeviceResource.lblDe960), typeof(DeviceResource))]
        De960
    }

    public enum ItouchPopDeviceTarget
    {
        [Localization(nameof(DeviceResource.lblITouchPop2A), typeof(DeviceResource))]
        ItouchPop2A,
        [Localization(nameof(DeviceResource.lblAbcm), typeof(DeviceResource))]
        Abcm,
        [Localization(nameof(DeviceResource.lblSoundTrack01), typeof(DeviceResource))]
        SoundTrack01
    }
    public enum ItouchPopFileType
    {
        [Localization(nameof(DeviceResource.lblMainFirmware), typeof(DeviceResource))]
        MainFirmware,
        [Localization(nameof(DeviceResource.lblNfcModule), typeof(DeviceResource))]
        NfcModule,
        [Localization(nameof(DeviceResource.lblExtra), typeof(DeviceResource))]
        Extra
    }

    public enum IcuFileType
    {
        [Localization(nameof(DeviceResource.lblMainFirmware), typeof(DeviceResource))]
        MainFirmware,
        [Localization(nameof(DeviceResource.lblInReader), typeof(DeviceResource))]
        InReader,
        [Localization(nameof(DeviceResource.lblOutReader), typeof(DeviceResource))]
        OutReader,
    }

    public enum TimezoneType
    {
        [Localization(nameof(TimezoneResource.lblActive), typeof(TimezoneResource))]
        Active = 1,
        [Localization(nameof(TimezoneResource.lblPassage), typeof(TimezoneResource))]
        Passage,
        [Localization(nameof(TimezoneResource.lblNormal), typeof(TimezoneResource))]
        Normal,
    }

    public enum DefaultTimezoneType
    {
        [Localization(nameof(TimezoneResource.lblDefaultNotUseTimezoneName), typeof(TimezoneResource))]
        DefaultNotUseTimezone = 1,
        [Localization(nameof(TimezoneResource.lblDefaultTimezoneName), typeof(TimezoneResource))]
        DefaultTimezone,
    }

    public enum TransmitType
    {
        [Localization(nameof(DeviceResource.lblTransmitCurrentTime), typeof(DeviceResource))]
        CurrentTime = 1,
        [Localization(nameof(DeviceResource.lblTransmitDeviceSetting), typeof(DeviceResource))]
        DeviceSetting,
        [Localization(nameof(DeviceResource.lblTransmitTimezoneSetting), typeof(DeviceResource))]
        TimezoneSetting,
        [Localization(nameof(DeviceResource.lblTransmitHolidaySetting), typeof(DeviceResource))]
        HolidaySetting,
        [Localization(nameof(DeviceResource.lblTransmitUserInfo), typeof(DeviceResource))]
        UserInfo,
        [Localization(nameof(DeviceResource.lblTransmitMessageSetting), typeof(DeviceResource))]
        MessageSetting
    }

    public enum ProtocolType
    {
        [Localization(nameof(MessageLogResource.lblAddUser), typeof(MessageLogResource))]
        AddUser = 1,
        [Localization(nameof(MessageLogResource.lblDeleteUser), typeof(MessageLogResource))]
        DeleteUser,
        [Localization(nameof(MessageLogResource.lblUpdateDeviceConfig), typeof(MessageLogResource))]
        UpdateDeviceConfig,
        [Localization(nameof(MessageLogResource.lblDeleteDevice), typeof(MessageLogResource))]
        DeleteDevice,
        [Localization(nameof(MessageLogResource.lblUpdateTimezone), typeof(MessageLogResource))]
        UpdateTimezone,
        [Localization(nameof(MessageLogResource.lblDeleteTimezone), typeof(MessageLogResource))]
        DeleteTimezone,
        [Localization(nameof(MessageLogResource.lblUpdateHoliday), typeof(MessageLogResource))]
        UpdateHoliday,
        [Localization(nameof(MessageLogResource.lblNotification), typeof(MessageLogResource))]
        Notification,
        [Localization(nameof(MessageLogResource.lblEventLogWebApp), typeof(MessageLogResource))]
        EventLogWebApp,
        [Localization(nameof(MessageLogResource.lblEventLogResponse), typeof(MessageLogResource))]
        EventLogResponse,
        [Localization(nameof(MessageLogResource.lblLoadDeviceInfo), typeof(MessageLogResource))]
        LoadDeviceInfo,
        [Localization(nameof(MessageLogResource.lblDeviceInstruction), typeof(MessageLogResource))]
        DeviceInstruction,
        [Localization(nameof(MessageLogResource.lblFileDownload), typeof(MessageLogResource))]
        FileDownLoad,
        [Localization(nameof(MessageLogResource.lblLongProcessProgress), typeof(MessageLogResource))]
        LongProcessProgress,
        [Localization(nameof(MessageLogResource.lblConnectionStatus), typeof(MessageLogResource))]
        ConnectionStatus,
    }

    public enum Topic
    {
        [Localization(nameof(MessageLogResource.lblEventLogTopic), typeof(MessageLogResource))]
        EventLogTopic = 1,
        [Localization(nameof(MessageLogResource.lblEventLogJsonTopic), typeof(MessageLogResource))]
        EventLogJsonTopic,
        [Localization(nameof(MessageLogResource.lblEventLogResponseTopic), typeof(MessageLogResource))]
        EventLogResponseTopic,
        [Localization(nameof(MessageLogResource.lblConfigTopic), typeof(MessageLogResource))]
        ConfigurationTopic,
        [Localization(nameof(MessageLogResource.lblConfigResponseTopic), typeof(MessageLogResource))]
        ConfigurationResponseTopic,
        [Localization(nameof(MessageLogResource.lblAccessControlTopic), typeof(MessageLogResource))]
        AccessControlTopic,
        [Localization(nameof(MessageLogResource.lblAccessControlResponseTopic), typeof(MessageLogResource))]
        AccessControlResponseTopic,
        [Localization(nameof(MessageLogResource.lblHolidayTopic), typeof(MessageLogResource))]
        HolidayTopic,
        [Localization(nameof(MessageLogResource.lblHolidayResponseTopic), typeof(MessageLogResource))]
        HolidayResponseTopic,
        [Localization(nameof(MessageLogResource.lblOnlineTopic), typeof(MessageLogResource))]
        DeviceOnlineTopic,
        [Localization(nameof(MessageLogResource.lblDeviceStatusTopic), typeof(MessageLogResource))]
        DeviceStatus,
        [Localization(nameof(MessageLogResource.lblDeviceInfoResponseTopic), typeof(MessageLogResource))]
        DeviceInfoResponseTopic,
        [Localization(nameof(MessageLogResource.lblDeviceInfoTopic), typeof(MessageLogResource))]
        DeviceInfoTopic,
        [Localization(nameof(MessageLogResource.lblTimezoneTopic), typeof(MessageLogResource))]
        TimezoneTopic,
        [Localization(nameof(MessageLogResource.lblTimezoneResponseTopic), typeof(MessageLogResource))]
        TimezoneResponseTopic,
        [Localization(nameof(MessageLogResource.lblNotificationTopic), typeof(MessageLogResource))]
        NotificationTopic,
        [Localization(nameof(MessageLogResource.lblDeviceInstructionTopic), typeof(MessageLogResource))]
        DeviceInstructionTopic,
        [Localization(nameof(MessageLogResource.lblDeviceInstructionResponseTopic), typeof(MessageLogResource))]
        DeviceInstructionResponseTopic,
        [Localization(nameof(MessageLogResource.lblFileTranferTopic), typeof(MessageLogResource))]
        FileTranferTopic,
        [Localization(nameof(MessageLogResource.lblFileTranferResponseTopic), typeof(MessageLogResource))]
        FileTranferResponseTopic,
        [Localization(nameof(MessageLogResource.lblLongProcessProgressTopic), typeof(MessageLogResource))]
        LongProcessProgressTopic

    }

    public enum OperationType
    {
        [Localization(nameof(DeviceResource.lblEntrance), typeof(DeviceResource))]
        Entrance,
        [Localization(nameof(DeviceResource.lblRestaurent), typeof(DeviceResource))]
        Restaurant
    }

    public enum Antipass
    {
        [Localization(nameof(EventLogResource.lblInDevice), typeof(EventLogResource))]
        In = 1,
        [Localization(nameof(EventLogResource.lblOutDevice), typeof(EventLogResource))]
        Out
    }

    public enum CommandType
    {
        [Localization(nameof(CommonResource.Open), typeof(CommonResource))]
        Open = 1,
        [Localization(nameof(CommonResource.SetTime), typeof(CommonResource))]
        SetTime,
        [Localization(nameof(CommonResource.Reset), typeof(CommonResource))]
        Reset,
        [Localization(nameof(CommonResource.ForceOpen), typeof(CommonResource))]
        ForceOpen,
        [Localization(nameof(CommonResource.ForceClose), typeof(CommonResource))]
        ForceClose,
        [Localization(nameof(CommonResource.Release), typeof(CommonResource))]
        Release
    }

    public enum SensorType
    {
        //[Localization(nameof(CommonResource.None), typeof(CommonResource))]
        //None = 0,
        [Localization(nameof(CommonResource.NormalOpen), typeof(CommonResource))]
        NormalOpen = 0,
        [Localization(nameof(CommonResource.NormalClose), typeof(CommonResource))]
        NormalClose,
        [Localization(nameof(CommonResource.NotUse), typeof(CommonResource))]
        NotUse
    }

    public enum Status
    {
        [Localization(nameof(CommonResource.Valid), typeof(CommonResource))]
        Valid,
        [Localization(nameof(CommonResource.Invalid), typeof(CommonResource))]
        Invalid
    }

    public enum FileStatus
    {
        [Localization(nameof(CommonResource.Success), typeof(CommonResource))]
        Success,
        [Localization(nameof(CommonResource.Failure), typeof(CommonResource))]
        Failure
    }

    public enum IcuStatus
    {
        [Localization(nameof(CommonResource.Disconnected), typeof(CommonResource))]
        Disconneted,
        [Localization(nameof(CommonResource.Connected), typeof(CommonResource))]
        Connected
    }

    public enum CardStatus
    {
        [Localization(nameof(CommonResource.Normal), typeof(CommonResource))]
        Normal,
        [Localization(nameof(CommonResource.Temp), typeof(CommonResource))]
        Temp,
        [Localization(nameof(CommonResource.Retire), typeof(CommonResource))]
        Retire,
        [Localization(nameof(CommonResource.Lost), typeof(CommonResource))]
        Lost,
        [Localization(nameof(CommonResource.Invalid), typeof(CommonResource))]
        InValid
    }

    public enum DoorStatus
    {
        [Localization(nameof(DeviceResource.dsClosedAndLock), typeof(DeviceResource))]
        ClosedAndLock = 0,
        [Localization(nameof(DeviceResource.dsClosedAndUnlocked), typeof(DeviceResource))]
        ClosedAndUnlocked,
        [Localization(nameof(DeviceResource.dsOpened), typeof(DeviceResource))]
        Opened,
        [Localization(nameof(DeviceResource.dsForceOpened), typeof(DeviceResource))]
        ForceOpened,
        [Localization(nameof(DeviceResource.dsPassageOpened), typeof(DeviceResource))]
        PassageOpened,
        [Localization(nameof(DeviceResource.dsEmergencyOpened), typeof(DeviceResource))]
        EmergencyOpened,
        [Localization(nameof(DeviceResource.dsEmergencyClosed), typeof(DeviceResource))]
        EmergencyClosed,
        [Localization(nameof(DeviceResource.dsUnknown), typeof(DeviceResource))]
        Unknown,
        [Localization(nameof(DeviceResource.dsLock), typeof(DeviceResource))]
        Lock,
        [Localization(nameof(DeviceResource.dsUnlock), typeof(DeviceResource))]
        Unlock
    }

    public enum AccessGroupType
    {
        [Localization(nameof(CommonResource.NoAccess), typeof(CommonResource))]
        NoAccess,
        [Localization(nameof(CommonResource.FullAccess), typeof(CommonResource))]
        FullAccess,
        [Localization(nameof(CommonResource.NormalAccess), typeof(CommonResource))]
        NormalAccess,
        [Localization(nameof(CommonResource.VisitAccess), typeof(CommonResource))]
        VisitAccess,
    }

    public enum SyncStatus
    {
        Synced = 0,
        Syncing = 1
    }

    public enum UpdateFlag
    {
        Continue = 0,
        Stop = 1
    }

    public enum VerifyMode
    {
        [Localization(nameof(CommonResource.lblCardOnly), typeof(CommonResource))]
        CardOnly = 1,
        [Localization(nameof(CommonResource.lblCardAndPw), typeof(CommonResource))]
        CardAndPw
    }

    public enum CardType
    {
        [Localization(nameof(EventLogResource.lblCard), typeof(EventLogResource))]
        NFC = 0,
        [Localization(nameof(EventLogResource.lblQR), typeof(EventLogResource))]
        QrCode,
        [Localization(nameof(EventLogResource.lblPassCode), typeof(EventLogResource))]
        PassCode,
    }

    public enum WiegandType
    {
        Default = 0,
        Predefined
    }

    public enum DeviceAuthority
    {
        General = 1,
        Manager,
        Supervisor
    }

    public enum RoleRules
    {
        //[Localization(nameof(CommonResource.None), typeof(CommonResource))]
        //None,
        [Localization(nameof(CommonResource.In), typeof(CommonResource))]
        In = 0,
        [Localization(nameof(CommonResource.Out), typeof(CommonResource))]
        Out,
        //[Localization(nameof(CommonResource.InOut), typeof(CommonResource))]
        //InOut
    }

    public enum PassbackRules
    {
        [Localization(nameof(CommonResource.NotUse), typeof(CommonResource))]
        NotUse = 0,
        [Localization(nameof(CommonResource.SoftAPB), typeof(CommonResource))]
        SoftAPB,
        [Localization(nameof(CommonResource.HardAPB), typeof(CommonResource))]
        HardAPB,
        //[Localization(nameof(CommonResource.InOut), typeof(CommonResource))]
        //InOut
    }

    public enum AccountType
    {
        [Localization(nameof(CommonResource.SystemAdmin), typeof(CommonResource))]
        SystemAdmin = 0,
        [Localization(nameof(CommonResource.SuperAdmin), typeof(CommonResource))]
        SuperAdmin = 1,
        [Localization(nameof(CommonResource.PrimaryAdmin), typeof(CommonResource))]
        PrimaryManager = 2,
        [Localization(nameof(CommonResource.SecondaryAdmin), typeof(CommonResource))]
        SecondaryManager = 3,
        [Localization(nameof(CommonResource.Employee), typeof(CommonResource))]
        Employee = 5
        
    }

    public enum Role
    {
        [Localization(nameof(CommonResource.Normal), typeof(CommonResource))]
        Normal = 1,
        [Localization(nameof(CommonResource.Admin), typeof(CommonResource))]
        Admin
    }
    public enum InOut
    {
        In = 1, // Link to DB: In: true, Out: false
        Out = 2
    }

    public enum CardReaderLed
    {
        [Localization(nameof(DeviceResource.lblBlue), typeof(DeviceResource))]
        Blue = 0,
        [Localization(nameof(DeviceResource.lblRed), typeof(DeviceResource))]
        Red = 1
    }

    public enum BuzzerReader
    {
        [Localization(nameof(CommonResource.On), typeof(CommonResource))]
        ON = 1,
        [Localization(nameof(CommonResource.Off), typeof(CommonResource))]
        OFF = 0
    }

    public enum UseCardReader
    {
        [Localization(nameof(CommonResource.Use), typeof(CommonResource))]
        Use = 0,
        [Localization(nameof(CommonResource.NotUse), typeof(CommonResource))]
        NotUse = 1
    }

    public enum UserType
    {
        [Localization(nameof(CommonResource.NormalUser), typeof(CommonResource))]
        Normal = 0,
        [Localization(nameof(CommonResource.Visitor), typeof(CommonResource))]
        Visit = 1
    }

    public enum EventType
    {
        [Localization(nameof(EventLogResource.lblNormalAccess), typeof(EventLogResource))]
        NormalAccess = 1,
        [Localization(nameof(EventLogResource.lblUnRegisteredCard), typeof(EventLogResource))]
        UnRegisteredCard = 2,
        [Localization(nameof(EventLogResource.lblExpirationDate), typeof(EventLogResource))]
        ExpirationDate = 3,
        [Localization(nameof(EventLogResource.lblEffactiveDateNotStarted), typeof(EventLogResource))]
        EffectiveDateNotStarted = 4,
        [Localization(nameof(EventLogResource.lblWrongIssueCount), typeof(EventLogResource))]
        WrongIssueCount = 5,
        [Localization(nameof(EventLogResource.lblNoUserAccessTime), typeof(EventLogResource))]
        NoUserAccessTime = 6,
        [Localization(nameof(EventLogResource.lblPassageTimezoneOn), typeof(EventLogResource))]
        PassageTimezoneOn = 7,
        [Localization(nameof(EventLogResource.lblPassageTimezoneOff), typeof(EventLogResource))]
        PassageTimezoneOff = 8,
        [Localization(nameof(EventLogResource.lblNoDoorActiveTime), typeof(EventLogResource))]
        NoDoorActiveTime = 9,
        [Localization(nameof(EventLogResource.lblInvalidDoor), typeof(EventLogResource))]
        InvalidDoor = 10,
        [Localization(nameof(EventLogResource.lblValidDoor), typeof(EventLogResource))]
        ValidDoor = 11,
        [Localization(nameof(EventLogResource.lblInvalidAccessType), typeof(EventLogResource))]
        InvalidAccessType = 12,
        [Localization(nameof(EventLogResource.lblUnregisteredCardAndPwd), typeof(EventLogResource))]
        UnregisteredCardAndPwd = 13,
        [Localization(nameof(EventLogResource.lblWrongPwd), typeof(EventLogResource))]
        WrongPwd = 14,
        [Localization(nameof(EventLogResource.lblAntiPassbackError), typeof(EventLogResource))]
        AntiPassbackError = 15,
        [Localization(nameof(EventLogResource.lblAccessDenied), typeof(EventLogResource))]
        AccessDenied = 16,
        [Localization(nameof(EventLogResource.lblDeviceInstructionOpen), typeof(EventLogResource))]
        DeviceInstructionOpen = 17,
        [Localization(nameof(EventLogResource.lblDeviceInstructionSettime), typeof(EventLogResource))]
        DeviceInstructionSettime = 18,
        [Localization(nameof(EventLogResource.lblDeviceInstructionReset), typeof(EventLogResource))]
        DeviceInstructionReset = 19,
        [Localization(nameof(EventLogResource.lblDeviceInstructionForceOpen), typeof(EventLogResource))]
        DeviceInstructionForceOpen = 20,
        [Localization(nameof(EventLogResource.lblDeviceInstructionForceClose), typeof(EventLogResource))]
        DeviceInstructionForceClose = 21,
        [Localization(nameof(EventLogResource.lblDeviceInstructionRelease), typeof(EventLogResource))]
        DeviceInstructionRelease = 22,
        [Localization(nameof(EventLogResource.lblDeviceInstructionDeleteAllUserEvent), typeof(EventLogResource))]
        DeviceInstructionDeleteAllUserEvent = 23,
        [Localization(nameof(EventLogResource.lblTryAccessInEmergencyClose), typeof(EventLogResource))]
        TryAccessInEmergencyClose = 24,
        [Localization(nameof(EventLogResource.lblTryAccessInInvalidDoor), typeof(EventLogResource))]
        TryAccessInInvalidDoor = 25,
        [Localization(nameof(EventLogResource.lblCommunicationFailed), typeof(EventLogResource))]
        CommunicationFailed = 26,
        [Localization(nameof(EventLogResource.lblCommunicationSucceed), typeof(EventLogResource))]
        CommunicationSucceed = 27,
        [Localization(nameof(EventLogResource.lblDeviceIsRunning), typeof(EventLogResource))]
        DeviceIsRunning = 28,
        [Localization(nameof(EventLogResource.lblMeal1), typeof(EventLogResource))]
        Meal1 = 29,
        [Localization(nameof(EventLogResource.lblMeal2), typeof(EventLogResource))]
        Meal2 = 30,
        [Localization(nameof(EventLogResource.lblMeal3), typeof(EventLogResource))]
        Meal3 = 31,
        [Localization(nameof(EventLogResource.lblMeal4), typeof(EventLogResource))]
        Meal4 = 32,
        [Localization(nameof(EventLogResource.lblMeal5), typeof(EventLogResource))]
        Meal5 = 33,
        [Localization(nameof(EventLogResource.lblOnlyAccessibleAtMealtime), typeof(EventLogResource))]
        OnlyAccessibleAtMealtime = 34,
        [Localization(nameof(EventLogResource.lblFirmwareApplicationUpdate), typeof(EventLogResource))]
        FirmwareApplicationUpdate = 35,
        [Localization(nameof(EventLogResource.lblFirmwareDownloadFailed), typeof(EventLogResource))]
        FirmwareDownloadFailed = 36,
        [Localization(nameof(EventLogResource.lblApplicationIsRunning), typeof(EventLogResource))]
        ApplicationIsRunning = 37,
        [Localization(nameof(EventLogResource.lblExitApplication), typeof(EventLogResource))]
        ExitApplication = 38,
        [Localization(nameof(EventLogResource.lblMPRIntervalExpired), typeof(EventLogResource))]
        MprIntervalExpired = 39,
        [Localization(nameof(EventLogResource.lblMPRAuth), typeof(EventLogResource))]
        MprAuth = 40,
        [Localization(nameof(EventLogResource.lblNormalAccessMPR), typeof(EventLogResource))]
        NormalAccessMpr = 41,
        [Localization(nameof(EventLogResource.lblMPRError), typeof(EventLogResource))]
        MprError = 42,
        [Localization(nameof(EventLogResource.lblMasterCardAccess), typeof(EventLogResource))]
        MasterCardAccess = 43,
        [Localization(nameof(EventLogResource.lblDoorOpenManually), typeof(EventLogResource))]
        DoorOpenManually = 44,
        [Localization(nameof(EventLogResource.lblExpiredQrCode), typeof(EventLogResource))]
        ExpiredQrCode = 45,
        [Localization(nameof(EventLogResource.lblPressedButton), typeof(EventLogResource))]
        PressedButton = 46,
    }


    public enum AvailableMealTypeCode
    {
        [Localization(nameof(EventLogResource.lblMeal1), typeof(EventLogResource))]
        Meal1 = 29,
        [Localization(nameof(EventLogResource.lblMeal2), typeof(EventLogResource))]
        Meal2 = 30,
        [Localization(nameof(EventLogResource.lblMeal3), typeof(EventLogResource))]
        Meal3 = 31,
        [Localization(nameof(EventLogResource.lblMeal4), typeof(EventLogResource))]
        Meal4 = 32,
        [Localization(nameof(EventLogResource.lblMeal5), typeof(EventLogResource))]
        Meal5 = 33,
    }

    public enum DataType
    {
        IcuDeviceIndex = 1,
        TimezoneIndex,
        HolidayIndex,
        WiegandIndex,
        MultiPersonRuleIndex,
        UserIndex
    }

    public enum HolidayType
    {
        [Localization(nameof(HolidayResource.lblHolidayType1), typeof(HolidayResource))]
        HolidayType1 = 1,
        [Localization(nameof(HolidayResource.lblHolidayType2), typeof(HolidayResource))]
        HolidayType2,
        [Localization(nameof(HolidayResource.lblHolidayType3), typeof(HolidayResource))]
        HolidayType3
    }

    public enum ActionType
    {
        Add = 1,
        Update,
        Delete,
        DeleteMultiple
    }

    public enum TransferType
    {
        NotSavedToServer = 0, //Default value
        SavedToServerSuccess,
        SavedIcuSuccess
    }

    public enum Confirmation
    {
        [Localization(nameof(CommonResource.lblNo), typeof(CommonResource))]
        No = 0,
        [Localization(nameof(CommonResource.lblYes), typeof(CommonResource))]
        Yes = 1
    }

    public enum MessageStatus
    {
        //TODO UnSent 필요한지 검토 후 필요없을 경우 삭제 예정
        [Localization(nameof(CommonResource.lblUnSent), typeof(CommonResource))]
        UnSent = 1,
        [Localization(nameof(CommonResource.lblSending), typeof(CommonResource))]
        Sending,
        [Localization(nameof(CommonResource.lblSent), typeof(CommonResource))]
        Sent,
        [Localization(nameof(CommonResource.lblReceived), typeof(CommonResource))]
        Received
    }

    public enum SyncType
    {
        [Localization(nameof(DeviceResource.lblDevice), typeof(DeviceResource))]
        IcuDevice = 1,
        [Localization(nameof(TimezoneResource.lblTimezone), typeof(TimezoneResource))]
        Timezone,
        [Localization(nameof(HolidayResource.lblHoliday), typeof(HolidayResource))]
        Holiday,
        [Localization(nameof(UserResource.lblUser), typeof(UserResource))]
        User,
        [Localization(nameof(EventLogResource.lblEventLog), typeof(EventLogResource))]
        Event,
        [Localization(nameof(DeviceResource.lblOpenDoor), typeof(DeviceResource))]
        OpenDoor
    }

    public enum SyncErrorCode
    {
        [Localization(nameof(SyncErrorCodeResource.StxError), typeof(SyncErrorCodeResource))]
        StxError = 0,
        [Localization(nameof(SyncErrorCodeResource.DeviceNumberMismatch), typeof(SyncErrorCodeResource))]
        DeviceNumberMismatch,
        [Localization(nameof(SyncErrorCodeResource.DataLengthError), typeof(SyncErrorCodeResource))]
        DataLengthError,
        [Localization(nameof(SyncErrorCodeResource.NoCommand), typeof(SyncErrorCodeResource))]
        NoCommand,
        [Localization(nameof(SyncErrorCodeResource.LrcError), typeof(SyncErrorCodeResource))]
        LrcError,
        [Localization(nameof(SyncErrorCodeResource.EtxError), typeof(SyncErrorCodeResource))]
        EtxError,
        [Localization(nameof(SyncErrorCodeResource.DataError), typeof(SyncErrorCodeResource))]
        DataError,
        [Localization(nameof(SyncErrorCodeResource.ErrorsRelatedToTerminalDb), typeof(SyncErrorCodeResource))]
        ErrorsRelatedToTerminalDb,
        [Localization(nameof(SyncErrorCodeResource.CardReadingFailed), typeof(SyncErrorCodeResource))]
        CardReadingFailed,
        [Localization(nameof(SyncErrorCodeResource.TimSettingFailed), typeof(SyncErrorCodeResource))]
        TimSettingFailed,
        [Localization(nameof(SyncErrorCodeResource.NoDataToLookUp), typeof(SyncErrorCodeResource))]
        NoDataToLookUp,
        [Localization(nameof(SyncErrorCodeResource.UserFullError), typeof(SyncErrorCodeResource))]
        UserFullError
    }

    public enum SystemLogType
    {
        
        [Localization(nameof(SystemLogTypeResource.Company), typeof(SystemLogTypeResource))]
        Company = 1,
        [Localization(nameof(SystemLogTypeResource.Building), typeof(SystemLogTypeResource))]
        Building,
        [Localization(nameof(SystemLogTypeResource.Department), typeof(SystemLogTypeResource))]
        Department,
        [Localization(nameof(SystemLogTypeResource.AccountManagement), typeof(SystemLogTypeResource))]
        AccountManagement,
        [Localization(nameof(SystemLogTypeResource.DeviceMonitoring), typeof(SystemLogTypeResource))]
        DeviceMonitoring,
        [Localization(nameof(SystemLogTypeResource.DeviceUpdate), typeof(SystemLogTypeResource))]
        DeviceUpdate,
        [Localization(nameof(SystemLogTypeResource.DeviceSetting), typeof(SystemLogTypeResource))]
        DeviceSetting,
        [Localization(nameof(SystemLogTypeResource.User), typeof(SystemLogTypeResource))]
        User,
        [Localization(nameof(SystemLogTypeResource.AccessGroup), typeof(SystemLogTypeResource))]
        AccessGroup,
        [Localization(nameof(SystemLogTypeResource.Timezone), typeof(SystemLogTypeResource))]
        Timezone,
        [Localization(nameof(SystemLogTypeResource.Holiday), typeof(SystemLogTypeResource))]
        Holiday,      
        [Localization(nameof(SystemLogTypeResource.MessageSetting), typeof(SystemLogTypeResource))]
        MessageSetting,
        [Localization(nameof(SystemLogTypeResource.SystemSetting), typeof(SystemLogTypeResource))]
        SystemSetting,
        [Localization(nameof(SystemLogTypeResource.EventRecovery), typeof(SystemLogTypeResource))]
        EventRecovery,
        [Localization(nameof(SystemLogTypeResource.CheckUserInformation), typeof(SystemLogTypeResource))]
        CheckUserInformation,
        [Localization(nameof(SystemLogTypeResource.CheckDeviceSetting), typeof(SystemLogTypeResource))]
        CheckDeviceSetting,
        [Localization(nameof(SystemLogTypeResource.TransmitAllData), typeof(SystemLogTypeResource))]
        TransmitAllData,
        [Localization(nameof(SystemLogTypeResource.Emergency), typeof(SystemLogTypeResource))]
        Emergency,
        [Localization(nameof(SystemLogTypeResource.Report), typeof(SystemLogTypeResource))]
        Report,
        [Localization(nameof(SystemLogTypeResource.Category), typeof(SystemLogTypeResource))]
        Category,
    }

    public enum ActionLogType
    {
        [Localization(nameof(ActionLogTypeResource.Login), typeof(ActionLogTypeResource))]
        Login = 1,
        [Localization(nameof(ActionLogTypeResource.Logout), typeof(ActionLogTypeResource))]
        Logout,
        [Localization(nameof(ActionLogTypeResource.ChangePassword), typeof(ActionLogTypeResource))]
        ChangePassword,
        [Localization(nameof(ActionLogTypeResource.Add), typeof(ActionLogTypeResource))]
        Add,
        [Localization(nameof(ActionLogTypeResource.Update), typeof(ActionLogTypeResource))]
        Update,
        [Localization(nameof(ActionLogTypeResource.Delete), typeof(ActionLogTypeResource))]
        Delete,
        [Localization(nameof(ActionLogTypeResource.DeleteMultiple), typeof(ActionLogTypeResource))]
        DeleteMultiple,
        [Localization(nameof(ActionLogTypeResource.Export), typeof(ActionLogTypeResource))]
        Export,
        [Localization(nameof(ActionLogTypeResource.Import), typeof(ActionLogTypeResource))]
        Import,
        [Localization(nameof(ActionLogTypeResource.AssignDoor), typeof(ActionLogTypeResource))]
        AssignDoor,
        [Localization(nameof(ActionLogTypeResource.UnassignDoor), typeof(ActionLogTypeResource))]
        UnassignDoor,
        [Localization(nameof(ActionLogTypeResource.AssignUser), typeof(ActionLogTypeResource))]
        AssignUser,
        [Localization(nameof(ActionLogTypeResource.UnassignUser), typeof(ActionLogTypeResource))]
        UnassignUser,
        [Localization(nameof(ActionLogTypeResource.ForcedOpen), typeof(ActionLogTypeResource))]
        ForcedOpen,
        [Localization(nameof(ActionLogTypeResource.ForcedClose), typeof(ActionLogTypeResource))]
        ForcedClose,
        [Localization(nameof(ActionLogTypeResource.Release), typeof(ActionLogTypeResource))]
        Release,
        [Localization(nameof(ActionLogTypeResource.DoorOpen), typeof(ActionLogTypeResource))]
        DoorOpen,
        [Localization(nameof(ActionLogTypeResource.Reset), typeof(ActionLogTypeResource))]
        Reset,
        [Localization(nameof(ActionLogTypeResource.Sync), typeof(ActionLogTypeResource))]
        Sync,
        [Localization(nameof(ActionLogTypeResource.ValidDoor), typeof(ActionLogTypeResource))]
        ValidDoor,
        [Localization(nameof(ActionLogTypeResource.InvalidDoor), typeof(ActionLogTypeResource))]
        InvalidDoor,
        [Localization(nameof(ActionLogTypeResource.Fail), typeof(ActionLogTypeResource))]
        Fail,
        [Localization(nameof(ActionLogTypeResource.Success), typeof(ActionLogTypeResource))]
        Success,
        [Localization(nameof(ActionLogTypeResource.CopyDoorSetting), typeof(ActionLogTypeResource))]
        CopyDoorSetting,
        [Localization(nameof(ActionLogTypeResource.Reinstall), typeof(ActionLogTypeResource))]
        Reinstall,

        //TODO Auto Register / (Add) Missing Device 용어 통일 필요(정해야함)
        [Localization(nameof(ActionLogTypeResource.AutoRegister), typeof(ActionLogTypeResource))]
        AutoRegister,
        [Localization(nameof(ActionLogTypeResource.UpdateDoor), typeof(ActionLogTypeResource))]
        UpdateDoor,
        [Localization(nameof(ActionLogTypeResource.UpdateMultipleUser), typeof(ActionLogTypeResource))]
        UpdateMultipleUser,
        [Localization(nameof(ActionLogTypeResource.BasicInfoTransmit), typeof(ActionLogTypeResource))]
        BasicInfoTransmit,
        [Localization(nameof(ActionLogTypeResource.TimezoneTransmit), typeof(ActionLogTypeResource))]
        TimezoneTransmit,
        [Localization(nameof(ActionLogTypeResource.HolidayTransmit), typeof(ActionLogTypeResource))]
        HolidayTransmit,
        [Localization(nameof(ActionLogTypeResource.Transmit), typeof(ActionLogTypeResource))]
        Transmit,
        [Localization(nameof(ActionLogTypeResource.EventExport), typeof(ActionLogTypeResource))]
        EventExport,
        [Localization(nameof(ActionLogTypeResource.SystemLogExport), typeof(ActionLogTypeResource))]
        SystemLogExport,
        [Localization(nameof(ActionLogTypeResource.AccessibleDoorExport), typeof(ActionLogTypeResource))]
        AccessibleDoorExport,
        [Localization(nameof(ActionLogTypeResource.RegisteredDoorExport), typeof(ActionLogTypeResource))]
        RegisteredDoorExport,
        [Localization(nameof(ActionLogTypeResource.AnalysisExport), typeof(ActionLogTypeResource))]
        AnalysisExport,
        [Localization(nameof(ActionLogTypeResource.Recovery), typeof(ActionLogTypeResource))]
        Recovery,
    }

    public enum SexType
    {
        [Localization(nameof(UserResource.lblFemale), typeof(UserResource))]
        Female,
        [Localization(nameof(UserResource.lblMale), typeof(UserResource))]
        Male
    }

    public enum SettingType
    {
        ResetAppPassword = 1
    }

    public enum LoginUnauthorized
    {
        CompanyNonExist = 1000,
        CompanyExpired = 1001,
        InvalidCredentials = 1002,
        InvalidToken = 1003,
        AccountNonExist = 1004
    }

    public enum VisitStatus
    {
        [Localization(nameof(VisitResource.lblApprovalWaiting1), typeof(VisitResource))]
        ApprovalWaiting1,
        [Localization(nameof(VisitResource.lblApprovalWaiting2), typeof(VisitResource))]
        ApprovalWaiting2,
        [Localization(nameof(VisitResource.lblIssueing), typeof(VisitResource))]
        Issueing,
        [Localization(nameof(VisitResource.lblDeliveryWaiting), typeof(VisitResource))]
        DeliveryWaiting,
        [Localization(nameof(VisitResource.lblDeliveryOk), typeof(VisitResource))]
        DeliveryOk,
        [Localization(nameof(VisitResource.lblReturn), typeof(VisitResource))]
        Return,
        [Localization(nameof(VisitResource.lblReject), typeof(VisitResource))]
        Reject,
        [Localization(nameof(VisitResource.lblReclamation), typeof(VisitResource))]
        Reclamation,
        [Localization(nameof(VisitResource.lblNotUse), typeof(VisitResource))]
        NotUse,
        [Localization(nameof(VisitResource.lblPreRegister), typeof(VisitResource))]
        PreRegister,
    }

    public enum VisitChangeStatusType
    {
        [Localization(nameof(VisitResource.lblWaiting), typeof(VisitResource))]
        Waiting ,
        [Localization(nameof(VisitResource.lblApproved1), typeof(VisitResource))]
        Approved1 ,
        [Localization(nameof(VisitResource.lblApproved), typeof(VisitResource))]
        Approved ,
        [Localization(nameof(VisitResource.lblReject), typeof(VisitResource))]
        Rejected,
        [Localization(nameof(VisitResource.lblCardIssued), typeof(VisitResource))]
        CardIssued,
        [Localization(nameof(VisitResource.lblFinished), typeof(VisitResource))]
        Finished,
        [Localization(nameof(VisitResource.lblFinishedWithoutReturnCard), typeof(VisitResource))]
        FinishedWithoutReturnCard,

    }

    public enum VisitType
    {
        [Localization(nameof(VisitResource.lblInsider), typeof(VisitResource))]
        Insider = 0,
        [Localization(nameof(VisitResource.lblOutsider), typeof(VisitResource))]
        OutSider = 1
    }

    public enum VisitArmyType
    {
        [Localization(nameof(VisitResource.lblCivilian), typeof(VisitResource))]
        Civilian = 10,
        [Localization(nameof(VisitResource.lblResidentCivilian), typeof(VisitResource))]
        ResidentCivilian = 11,
        [Localization(nameof(VisitResource.lblAnotherArmy), typeof(VisitResource))]
        AnotherArmy = 12,
        [Localization(nameof(VisitResource.lblArmyFamily), typeof(VisitResource))]
        ArmyFamily = 13
    }

    public enum VisitingCardStatusType
    {
        [Localization(nameof(VisitResource.lblDelivered), typeof(VisitResource))]
        Delivered,
        [Localization(nameof(VisitResource.lblReturned), typeof(VisitResource))]
        Returned,
        [Localization(nameof(VisitResource.lblNotUse), typeof(VisitResource))]
        NotUse,
        [Localization(nameof(VisitResource.lblRequest), typeof(VisitResource))]
        Request,
    }

    public enum VisitingCardStatusNormalType
    {
        [Localization(nameof(VisitResource.lblDelivered), typeof(VisitResource))]
        Delivered,
        [Localization(nameof(VisitResource.lblReturned), typeof(VisitResource))]
        Returned,
        [Localization(nameof(VisitResource.lblNotUse), typeof(VisitResource))]
        NotUse,
    }

    public enum VisitingCardStatusPreRegisterType
    {
        [Localization(nameof(VisitResource.lblNotUse), typeof(VisitResource))]
        NotUse = 2,
        [Localization(nameof(VisitResource.lblRequest), typeof(VisitResource))]
        Request,

    }


    public enum WorkType
    {
        [Localization(nameof(UserResource.lblPermanentWorker), typeof(UserResource))]
        PermanentWorker,
        [Localization(nameof(UserResource.lblContractWorker), typeof(UserResource))]
        ContractWorker,
        [Localization(nameof(UserResource.lblResidentWorker), typeof(UserResource))]
        ResidentWorker,
        [Localization(nameof(UserResource.lblPartTimeWorker), typeof(UserResource))]
        PartTimeWorker
    }

    public enum Army_WorkType
    {
        [Localization(nameof(UserResource.lblActiveDutySoldier), typeof(UserResource))]
        ActiveDutySoldier,
        [Localization(nameof(UserResource.lblCivilianWorker), typeof(UserResource))]
        CivilianWorker,
        [Localization(nameof(UserResource.lblSoldier), typeof(UserResource))]
        Soldier,
    }

    public enum PassType
    {
        
        [Localization(nameof(UserResource.lblPassCard), typeof(UserResource))]
        PassCard,
        [Localization(nameof(UserResource.lblVisitCard), typeof(UserResource))]
        VisitCard,

    }

    public enum UserStatus
    {
        [Localization(nameof(UserResource.lblUse), typeof(UserResource))]
        Use,
        [Localization(nameof(UserResource.lblNotUse), typeof(UserResource))]
        NotUse
    }

    public enum PermissionType
    {
        [Localization(nameof(UserResource.lblNotUse), typeof(UserResource))]
        NotUse,
        [Localization(nameof(UserResource.lblSystemAdmin), typeof(UserResource))]
        SystemAdmin,
        //[Localization(nameof(CommonResource.SuperAdmin), typeof(CommonResource))]
        //SuperAdmin,
    }
    
    public enum AttendanceType
    {
        [Localization(nameof(UserResource.lblLateIn), typeof(UserResource))]
        LateIn,
        [Localization(nameof(UserResource.lblEarlyOut), typeof(UserResource))]
        EarlyOut,
        [Localization(nameof(UserResource.lblAbsentNoReason), typeof(UserResource))]
        AbsentNoReason,
        [Localization(nameof(UserResource.lblOverTime), typeof(UserResource))]
        OverTime,
        [Localization(nameof(UserResource.lblNormal), typeof(UserResource))]
        Normal,
        [Localization(nameof(UserResource.lblBusinessTrip), typeof(UserResource))]
        BusinessTrip,
        [Localization(nameof(UserResource.lblVacation), typeof(UserResource))]
        Vacation,
        [Localization(nameof(UserResource.lblSickness), typeof(UserResource))]
        Sickness,
        [Localization(nameof(UserResource.lblLateInEarlyOut), typeof(UserResource))]
        LateInEarlyOut,
        [Localization(nameof(UserResource.lblRemote), typeof(UserResource))]
        Remote,
        [Localization(nameof(UserResource.lblAbNormal), typeof(UserResource))]
        AbNormal,
        [Localization(nameof(UserResource.lblHoliday), typeof(UserResource))]
        Holiday,
    }
    
    public enum VisitSettingType
    {
        [Localization(nameof(VisitResource.lblVisitNoStep), typeof(DeviceResource))]
        NoStep,
        [Localization(nameof(VisitResource.lblVisitFirstStep), typeof(DeviceResource))]
        FirstStep,
        [Localization(nameof(VisitResource.lblVisitSecondStep), typeof(DeviceResource))]
        SecondsStep
    }

    public enum UserHeaderColumn
    {
        [Localization(nameof(UserResource.lblName), typeof(UserResource))]
        FirstName = 0,
        [Localization(nameof(UserResource.lblUserCode), typeof(UserResource))]
        UserCode,
        [Localization(nameof(UserResource.lblDepartment), typeof(UserResource))]
        DepartmentName,
        [Localization(nameof(UserResource.lblEmployeeNumber), typeof(UserResource))]
        EmployeeNo,
        [Localization(nameof(UserResource.lblMilitaryNumber), typeof(UserResource))]
        MilitaryNo,
        [Localization(nameof(UserResource.lblExpiredDate), typeof(UserResource))]
        ExpiredDate,
        [Localization(nameof(UserResource.lblCardId), typeof(UserResource))]
        CardList,
        [Localization(nameof(UserResource.lblAction), typeof(UserResource))]
        Action,
    }

    public enum CultureCodes
    {
        [Localization(nameof(CommonResource.lblEnglish), typeof(CommonResource))]
        English = 0,
        [Localization(nameof(CommonResource.lblJapanese), typeof(CommonResource))]
        Japanese = 1,
        [Localization(nameof(CommonResource.lblKorean), typeof(CommonResource))]
        Korean = 2,
        [Localization(nameof(CommonResource.lblVietnamese), typeof(CommonResource))]
        Vietnamese = 3,
    }

}
