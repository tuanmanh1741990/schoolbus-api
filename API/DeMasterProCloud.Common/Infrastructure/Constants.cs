﻿using System.Collections.Generic;
using System.Security.Cryptography.Xml;

namespace DeMasterProCloud.Common.Infrastructure
{
    public class ColumnDefines
    {
        public static readonly string[] MprUser =
        {
            "User.Id",
            "User.UserCode",
            "User.FirstName",
            "User.CardId",
            "User.Department.DepartName"
        };

        public static readonly string[] Account = {
            //"Id",
            "Username",
            "Type",
            //"Status"
        };

        //public static readonly string[] Department = {
        //    "Id",
        //    "DepartNo",
        //    "DepartName",
        //    "ParentId"
        //};
        public static readonly string[] Department = {

            "DepartNo",
            "DepartName"

        };

        public static readonly string[] AccessGroup =
        {
            "Id",
            "Name",
            "IsDefault"
        };

        public static readonly string[] AccessGroupForDoors =
        {
            "DeviceAddress",
            "DoorName",
            "Timezone",
            "Building"
        };

        public static readonly string[] AccessGroupForUnAssignDoors =
        {
            "Id",
            "DeviceAddress",
            "DoorName",
            "Building"
        };

        public static readonly string[] UnregistedDevices =
        {
            "Id",
            "DeviceAddress",
            "Status",
            "IpAddress"
        };

        public static readonly string[] AccessGroupForUsers =
        {
            //"Id",
            //"CardId",
            "FirstName",
            "Department.DepartName"
        };

        public static readonly string[] AccessGroupForUnAssignUsers =
        {
            //"Id",
            //"CardId",
            "FullName",
            "DepartmentName",
            "AccessGroupName"
        };

        public static readonly string[] User = {
            "Id",
            "FirstName",
            "LastName",
            "CardId",
            "Department.DepartName",
            "EmpNumber",
            "ExpiredDate"
        };

        public static readonly string[] RegisteredUser = {
            "0",
            "1",
            "FirstName",
            "Department.DepartName",
            "EmpNumber",
            "ExpiredDate",
            "Card.CardStatus"
        };

        public static readonly string[] NewUser = {
            "FirstName",
            "UserCode",
            "Department.DepartName",
            "EmpNumber",
            "LastName",
            "ExpiredDate"
        };

        public static readonly string[] UserList = {
            //"FirstName",
            //"UserCode",
            //"Department.DepartName",
            //"EmpNumber",
            //"ExpiredDate"
            "0(CardId) - not used",
            "FirstName",
            "Department.DepartName",
            "ExpiredDate",
            "UserCode",
            "EmpNumber",
            "6(IssueCount) - not used",
            "7(CardStatus) - not used",
            "AccessGroup.Name",
            "9(CardList) - not used"
        };

        public static readonly string[] UserArmyList = {
            "0(CardId) - not used",
            "FirstName",
            "DepartmentName",
            "ExpiredDate",
            "UserCode",
            "EmployeeNo",
            "6(IssueCount) - not used",
            "7(CardStatus) - not used",
            "AccessGroupName",
            "9(CardList) - not used"
        };

        public static readonly string[] UserHeader =
        {
            UserHeaderColumn.FirstName.GetName(),
            UserHeaderColumn.UserCode.GetName(),
            UserHeaderColumn.DepartmentName.GetName(),
            UserHeaderColumn.EmployeeNo.GetName(),
            UserHeaderColumn.ExpiredDate.GetName(),
            UserHeaderColumn.CardList.GetName(),
            UserHeaderColumn.Action.GetName(),
        };

        public static readonly string[] UserArmyHeader =
        {
            UserHeaderColumn.FirstName.GetName(),
            UserHeaderColumn.UserCode.GetName(),
            UserHeaderColumn.DepartmentName.GetName(),
            UserHeaderColumn.MilitaryNo.GetName(),
            UserHeaderColumn.ExpiredDate.GetName(),
            UserHeaderColumn.CardList.GetName(),
            UserHeaderColumn.Action.GetName(),
        };

        public static readonly string[] UserMasterCard = {
            "CardId",
            "UserName"
        };

        public static readonly string[] Device = {
            "Id",
            "Name",
            "DeviceAddress",
            "ActiveTz.Name",
            "PassageTz.Name",
            "VerifyMode",
            "Status"
        };

        public static readonly string[] DeviceSetting = {
            "Name",
            "DeviceAddress",
            "ActiveTz.Name",
            "PassageTz.Name",
            "VerifyMode",
            "Building.Name",
            "ConnectionStatus"
        };

        public static readonly string[] Devices = {
            "Building.Name",
            "Name",
            "DeviceAddress",
            "ActiveTz.Name",
            "PassageTz.Name",
            "VerifyMode",
            "Company.Name",
            "ConnectionStatus",
            "RegisterIdNumber",
            "DeviceType"
        };

        public static readonly string[] AccessibleDoor = {
            "Id",
            "Name",
            "DeviceAddress",
            "ActiveTz.Name",
            "PassageTz.Name",
            "VerifyMode",
            "PassbackRule",
            "DeviceType",
            "MPRCount"
        };

        public static readonly string[] DeviceValid = {
            //"Name",
            //"DeviceAddress",
            //"FirmwareVersion",
            //"LastCommunicationTime",
            //"DeviceType",
            //"NumberOfNotTransmittingEvent",
            //"RegisterIdNumber",
            //"ConnectionStatus",
            //"DoorStatus"
            "Company.Name",
            "Building.Name",
            "Name",
            "DeviceAddress",
            "FirmwareVersion",
            "LastCommunicationTime",
            "DeviceType",
            "NumberOfNotTransmittingEvent",
            "RegisterIdNumber",
            "ConnectionStatus",
            "DoorStatus",
            "UpTimeOnlineDevice"
        };


        public static readonly string[] DeviceListModelForUser = {
            "Name",
            "DeviceAddress"
        };

        public static readonly string[] AccessLevelAssignedUsers = {
            "User.UserCode",
            "User.FirstName",
            "User.Department.DepartName",
            "Tz.Name",
            "User.CardId"
        };

        public static readonly string[] AccessLevelUnAssignedUsers = {
            "UserCode",
            "FirstName",
            "Department.DepartName"
        };

        public static readonly string[] Company = {
            "Id",
            "Code",
            "Name",
            "Admin",
            "ExpiredDate",
            "Status",
            "CreatedDate"
        };

        public static readonly string[] CornerSetting = {
            "Id",
            "CompanyId",
            "Code",
            "Name",
            "Description"
        };

        public static readonly string[] MealSetting = {
            "Id",
            "IcuDeviceId",
            "CornerId",
            "MealTypeId",
            "Start",
            "End",
            "Price"
        };

        public static readonly string[] MealEventLog =
        {
             "eventTime",
             "icuId",
             "userName",
             "userId",
             "mealCode",
             "mealType",
             "Price",
             "doorName",
             "mealEventLogId",
             "eventLogId",
             "deviceName",
             "companyId",
             "isManual",
             "deptId",
             "tring departmentName",
             "userCode",
             "cardId",
             "totalPrice",
             "employeeNumber",
             "birthayDay",
             "buildingId",
             "deviceAddress",
             "buildingName"
        };

        public static readonly string[] ListMealSetting = {
            "icuDeviceName",
            "cornerName",
            "icuDevice",
            "rId",
            "data"
        };

        public static readonly string[] ExceptionalMeal = {
            "Id",
            "MealSettingId",
            "Start",
            "End",
            "Price"
        };

        public static readonly string[] UserDiscount = {
            "Id",
            "UserId",
            "Amount",
        };

        public static readonly string[] Timezone = {
            "TimezoneName"
        };

        public static readonly string[] Holiday = {
            "Name",
            "HolidayType",
            "StartDateDisp",
            "EndDateDisp",
            "RecursiveDisp",
        };

        public static readonly string[] EventLog =
        {
            "EventTime",
            "User.UserCode",
            "User.FirstName",
            "CardId",
            "KeyPadPw",
            "Icu.DeviceAddress",
            "DoorName",
            "CardType",
            "Antipass",
            "EventType"
        };

        public static readonly string[] EventLogForReport =
        {
            //"Id",
            "EventTime",
            "UserName",
            "User.BirthDay",
            "User.UserCode",
            "User.Department.DepartName",
            "CardId",
            "Icu.DeviceAddress",
            "DoorName",
            "Icu.Building.Name",
            "Antipass",
            "EventType",
            "IssueCount",
            "CardStatus",
            "CardType"
        };

        public static readonly string[] SystemLog =
        {
            "CreatedByNavigation.Username",
            "OpeTime",
            "Type",
            "Action",
            "Content"
        };

        public static readonly string[] SystemLogForReport =
        {
            "OpeTime",
            "CreatedByNavigation.Username",
            "Type",
            "Action",
            "Content"
        };

        public static readonly string[] SystemLogListModelForReport =
        {
            "OperationTime",
            "UserAccount",
            "OperationType",
            "Action",
            "Message"
        };

        public static readonly string[] MessageLog =
        {
            "Id",
            "MsgId",
            "Topic",
            "PayLoad",
            "Status",
            "PublishedTime",
            "ResponseTime",
            "Type",
            "GroupMsgId"
        };

        public static readonly string[] WgSetting = {
            "Id",
            "Name",
            "TotalBits",
            "TypeDisp",
            "CreatedOnDisp"
        };
        public static readonly string[] BuildingList = {
            "Id",
            "Name",
            "Edit",
            "Delete"
        };
        public static readonly string[] BuildingUnAssignDoorList = {
            "Id",
            "Building",
            "DoorName",
            "DeviceAddress"
        };
        public static readonly string[] BuildingDoorList = {
            "Name",
            "DeviceAddress",
            "DeviceType",
            "ActiveTz.Name",
            "PassageTz.Name",
        };

        public static readonly string[] DeviceMessageList = {

            "MessageId",
            "Content"

        };

        public static readonly string[] Visit = {

            "ApplyDate",
            "VisitorName",
            "BirthDay",
            "VisitorDepartment",
            "Position",
            "StartDate",
            "EndDate",
            "VisiteeSite",
            "VisiteeReason",
            "VisiteeName",
            "Phone",
            "Status",
            "ApproverId1",
            "ApproverId2",
            "RejectReason",
            "CardId"

        };

        public static readonly string[] VisitReport =
        {
            "Id",
            "EventTime",
            "Visit.VisitorName",
            "Visit.BirthDay",
            "Visit.VisitorDepartment",
            "CardId",
            "Icu.DeviceAddress",
            "Icu.Name",
            "Building.BuildingName",
            "Icu.HardAntiFlag",
            "EventDetails",
            "IssueCount",
            "CardStatus",
        };


        // canteen

        public static readonly string[] MealType =
    {
            "Id",
            "Name",
            "Code",
            "Description"
        };
    }

    /// <summary>
    /// Define all Constants
    /// </summary>
    public class Constants
    {
        public static int MaxSendIcuUserCount = 20;
        public static int MaxSendITouchPopUserCount = 1000;

        public static int MaxSendUserDelay = 0;

        public const int MaxDeviceAddressDigit = 6;
        public const string MaxSplitUserInICU = "MaxSplitUserInICU";
        public const string CommaString = ",";
        public const char Colon = ':';
        public const string AllowImageType = "AllowImageType";
        public const string DateServerFormat = "DateServerFormat";
        public const string DateTimeServerFormat = "DateTimeServerFormat";
        public const string DateTimeWithoutSecServerFormat = "DateTimeWithoutSecServerFormat";
        public const string TimeToSendSetTimeEveryDay = "TimeToSendSetTimeEveryDay";
        public const string TimeToSendSetTimeHours = "TimeToSendSetTimeHours";
        public const string TimeToSendSetTimeMinutes = "TimeToSendSetTimeMinutes";
        public const string AccessTimeFormat = "AccessTimeFormat";
        public const string UpdateTimeFormat = "UpdateTimeFormat";
        public const string DateClientFortmat = "DateClientFortmat";
        public const string AppName = "DemasterPro";
        public const string CacheBuster = "CacheBuster";
        public const string Alphabet = "abcdefghijklmnopqrstuvwxyz";
        public const string AlphabetAndNumber = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
        public const string HexNumber = "ABCDEF0123456789";
        public const string Ok = "OK";
        public const string Ng = "NG";
        public const string Excel = "excel";
        public const string Txt = "txt";
        public const string WorkSheetName = "User";
        public const string UploadDir = "Upload";
        public const string TutorialsDir = "Tutorials";
        public const string ExportFileFormat = "{0}_{1:yyyyMMddHHmmss}";
        //public const string ExportFileFormat2 = "{0}_{1:yyyyMMddHHmmss}";
        public const string AppSession = ".DesmasterProWeb.Session";
        public const string DefaultConnectionString = "ConnectionStrings:DefaultConnection";
        public const string WeirdCharacter = "ÿ";
        public static readonly string[] RandomChars = {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?_-"                        // non-alphanumeric
            };
        public const string Gps = "<a href=\"{0}\" target=\"_blank\">{1}</a>";
        public const string SpInfo = "<a href=\"{0}\" data-target=\"#spdetailreport\" class=\"btn-detail\" >{1}</a>";
        public static readonly string[] AntiPass = { "In", "Out", "Power", "Normal", "Timezone" };
        public static int DefaultCompanyId = 1;
        public static int DefaultPassageTimezoneId = 1;
        public static int DefaultActiveTimezoneId = 2;
        public static int DefaultDepartmentId = 1;
        public static int DefaultBuildingId = 1;

        public const int TzNotUsePos = 0;
        public const int Tz24hPos = 1;

        public const string FormatBuilding = "{building}";
        public const string FormatDoor = "{door}";

        public const string categoryVariable = "category";

        public class AccessGroup
        {
            //TODO: Full access Group의 경우 언어에 상관없이 FAG라는 이름으로 사용 예정(Import 시에도 FAG로)
            public const string FullAccess_EN = "Full Access";
            public const string FullAccess_KR = "전체 출입";
            public const string FullAccess_JP = "全権アクセス";
            public const string NoAccess_EN = "No Access";
            public const string NoAccess_KR = "출입 불가";
            public const string NoAccess_JP = "アクセスなし";
            public const string VisitAccess_EN = "Visit Access";
            public const string VisitAccess_KR = "방문 출입";
            public const string VisitAccess_JP = "Visit Access";
        }

        public class CardStatus
        {

            public const string Normal_EN = "Normal";
            public const string Normal_KR = "일반";
            public const string Normal_JP = "普通";

            public const string Temp_EN = "Temp";
            public const string Temp_KR = "임시 카드";
            public const string Temp_JP = "温度";

            public const string Retire_EN = "Retire";
            public const string Retire_KR = "퇴직";
            public const string Retire_JP = "引退";

            public const string Lost_EN = "Lost";
            public const string Lost_KR = "분실 카드";
            public const string Lost_JP = "なくした";

            public const string Invalid_EN = "Invalid";
            public const string Invalid_KR = "사용안함";
            public const string Invalid_JP = "無効";
        }

        public class Sex
        {

            public const string Male_EN = "Male";
            public const string Male_KR = "남성";
            public const string Male_JP = "男性";

            public const string Female_EN = "Female";
            public const string Female_KR = "여성";
            public const string Female_JP = "女性";
        }

        public class Auth
        {
            public const string AuthenticationUsername = "Authentication:Username";
            public const string AuthenticationPassword = "Authentication:Password";
            public const string Authorization = "Authorization";
            public const string BasicAuthorization = "Basic Authorization";
            public const string StringType = "string";
            public const string Header = "header";
            public const string Basic = "Basic";
            public const string BearerHeader = "Bearer ";
            public const string BasicHeader = "Basic ";
            public const string Iso88591 = "iso-8859-1";
        }

        public class CommonFields
        {
            public const string CreatedBy = "CreatedBy";
            public const string CreatedOn = "CreatedOn";
            public const string UpdatedBy = "UpdatedBy";
            public const string UpdatedOn = "UpdatedOn";
        }
        public class Policy
        {
            public const string SuperAdmin = "SuperAdmin";
            public const string PrimaryAdmin = "PrimaryAdmin";
            public const string SecondaryAdmin = "SecondaryAdmin";
            public const string Employee = "Employee";
            public const string SystemAdmin = "SystemAdmin";
            public const string SystemAndSuperAdmin = "SystemAndSuperAdmin";
            public const string SystemAndSuperAndPrimaryAdmin = "SystemAndSuperAndPrimaryAdmin";
            public const string SuperAndPrimaryAdmin = "SuperAndPrimaryAdmin";
            public const string PrimaryAndSecondaryAdmin = "PrimaryAndSecondaryAdmin";
            public const string PrimaryAndSecondaryAdminAndEmployee = "PrimaryAndSecondaryAdminAndEmployee";
            public const string SuperAndPrimaryAndSecondaryAdmin = "SuperAndPrimaryAndSecondaryAdmin";
            public const string SuperAndPrimaryAndSecondaryAdminAndEmployee = "SuperAndPrimaryAndSecondaryAdminAndEmployee";
            public const string SystemAndSuperAndPrimaryAndSecondaryAdmin = "SystemAndSuperAndPrimaryAndSecondaryAdmin";
            public const string SystemAndSuperAndPrimaryAndSecondaryAdminAndEmployee = "SystemAndSuperAndPrimaryAndSecondaryAdminAndEmployee";
        }

        public class DateTimeFormat
        {
            public const string YyyyMMdd = "yyyyMMdd";
            public const string YyyyMMddhhmm = "yyyyMMddHHmm";
            public const string DdMMyyyy = "ddMMyyyy";
            public const string YyMMdd = "yyMMdd";
            public const string DdMdYyyy = "ddMMyyyy";
            public const string MmDdYyyy = "MM.dd.yyyy";
            public const string DdMdYyyyFormat = "dd-MM-yyyy";
            public const string YyyyyMdDdFormat = "yyyy-MM-dd";
            public const string YyyyMmDdHhMmSs = "yyyy/MM/dd HH:mm:ss";
            public const string YyyyMmDd = "yyyy/MM/dd";
            public const string YyyyMmDdHhMmSsFfffff = "yyyy/MM/dd HH:mm:ss.FFFFFF";
            public const string YyyyMMddHHmmss = "yyyyMMddHHmmss";
            public const string DdMMyyyyHHmmss = "ddMMyyyyHHmmss";
            public const string MMddyyyyHHmmss = "MMddyyyyHHmmss";
            public const string DdMMyyyyHHmmssFfffff = "ddMMyyyyHHmmssFFFFFF";
            public const string DatetimeServerFormat = "yyyy/MM/dd HH:mm:ss";
            public const string Hhmm = "hhmm";
            public const string DdMMyyyyHHmmssFff = "ddMMyyyyHHmmssFFF";
            public const string ddMMyyyyHHmmsszzz = "ddMMyyyyHHmmsszzz";
        }

        /// <summary>
        /// class define all route in system
        /// </summary>
        public class Route
        {
            public const string ApiAccounts = "/accounts";
            public const string ApiAccountsId = "/accounts/{id}";
            public const string ApiResetPassword = "/accounts/reset-password";
            public const string ApiForgotPassword = "/accounts/forgot-password";
            public const string ApiGetAccountsType = "/accounts/type";
            public const string ApiGetPrimaryAccounts = "/accounts/primary";
            public const string ApiAccountsListTimeZone = "/accounts/get-timezone-by-standard";
            public const string ApiUpdateTimeZoneByAccounts = "/accounts/update-timezone";
            public const string APiRefreshToken = "/refreshToken";

            public const string ApiAccessGroups = "/access-groups";
            public const string ApiAccessGroupsId = "/access-groups/{id}";
            public const string ApiAccessGroupsDoors = "/access-groups/{id}/doors";
            public const string ApiAccessGroupsUsersList = "/access-groups/{id}/users";
            public const string ApiAccessGroupsAssignDoors = "/access-groups/{id}/assign-doors";
            public const string ApiAccessGroupsAssignUsers = "/access-groups/{id}/assign-users";
            public const string ApiAccessGroupsUnAssignedDoors = "/access-groups/{id}/unassigned-doors";
            public const string ApiAccessGroupsUnAssignedUsers = "/access-groups/{id}/unassigned-users";
            public const string ApiAccessGroupsUnAssignUser = "/access-groups/{id}/unassign-user/{userId}";
            public const string ApiAccessGroupsUnAssignUsers = "/access-groups/{id}/unassign-users";
            public const string ApiAccessGroupsUnAssignDoor = "/access-groups/{id}/unassign-door/{doorId}";
            public const string ApiAccessGroupsUnAssignDoors = "/access-groups/{id}/unassign-doors";
            public const string ApiAccessGroupsChangeTimezones = "/access-groups/{id}/change-timezones";
            public const string ApiAccessGroupsChangeTimezone = "/access-groups/{id}/change-timezone";

            // for test
            public const string ApiAccessGroupsAllDoorsForVisit = "/access-groups/{id}/all-doors-visits";

            public const string ApiAccessLevels = "/access-levels";
            public const string ApiAccessLevelsDoorId = "/access-levels/{doorId}";
            public const string ApiAccessLevelsDoorList = "/access-levels/{doorId}/copy-settings/door-list";
            public const string ApiAccessLevelsAssignUsers = "/access-levels/{doorId}/{tzId}/assign-users";
            public const string ApiAccessLevelsAssignedUsers = "/access-levels/{doorId}/assigned-users";
            public const string ApiAccessLevelsChangeTimezone = "/access-levels/{doorId}/change-timezone/{tzId}";
            public const string ApiAccessLevelsUnAssignedUsers = "/access-levels/{doorId}/unassigned-users";
            public const string ApiAccessLevelsCopyDoorSettings = "/access-levels/{doorId}/copy-settings";
            public const string ApiAccessLevelsAssignedUsersInit = "/access-levels/{doorId}/assigned-users/init";

            public const string ApiGetAttendanceByCompany = "/attendances";
            public const string ApiEditAttendance = "/attendances/{id}";
            public const string ApiExportAttendance = "/attendances/export";
            public const string ApiGetAttendanceRecordEachUser = "/attendances/users";
            public const string ApiReCheckAttendanceFromEventLog = "/attendances/recheck";

            public const string ApiBuildings = "/buildings";
            public const string ApiBuildingsId = "/buildings/{id}";
            public const string ApiBuildingsDoorList = "/buildings/{id}/doors";
            public const string ApiBuildingsAssignDoors = "/buildings/{id}/assign-doors";
            public const string ApiBuildingsUnAssignDoors = "/buildings/{id}/unassign-doors";

            public const string ApiCardRequests = "/card-requests";
            public const string ApiCardTypes = "/cards/list-card-type";

            public const string ApiCategories = "/categories";
            public const string ApiCategoriesId = "/categories/{id}";
            public const string ApiCaregoriesHierarchy = "/categories/hierarchy";
            public const string ApiCaregoryOptionsHierarchy = "/categories/options/hierarchy";

            public const string ApiCompanies = "/companies";
            public const string ApiCompanyId = "/company/{id}";
            public const string ApiCompaniesId = "/companies/{id}";
            public const string ApiAssignCompany = "/company/{id}/assign-doors";
            public const string ApiCompaniesGetLogo = "/companies/get-logo";
            public const string ApiCompaniesMiniLogo = "/companies/{id}/mini-logo";
            public const string ApiCompaniesUpdateLogo = "/companies/update-logo";
            public const string ApiRegenerateCompanyKey = "/companies/{id}/reset-aes-key";
            public const string ApiRegenerateCompaniesKey = "/companies/reset-aes-key";
            public const string ApiGetPlugByCompany = "/companies/{id}/plugins";

            public const string ApiCanteenMealTypes = "/meal-types";
            public const string ApiCanteenMealTypeId = "/meal-types/{id}";
            public const string ApiCanteenMealTypeCodeList = "/meal-types/code-list";

            public const string ApiCanteenMealEventLogs = "/meal-event-logs";
            public const string ApiCanteenMealEventLogsId = "/meal-event-logs/{Id}";

            public const string ApiDepartments = "/departments";
            public const string ApiDepartmentsId = "/departments/{id}";
            public const string ApiDepartmentsImport = "/departments/import";
            public const string ApiDepartmentsExport = "/departments/export";
            public const string ApiDepartmentsExportPdf = "/departments/exportpdf";
            public const string ApiDepartmentsHierarchy = "/departments/hierarchy";
            public const string ApiDepartmentsChild = "/departments/get-department-tree";

            public const string ApiDevices = "/devices";
            public const string ApiDevicesList = "/devices/device-list";
            public const string ApiValidDevices = "/devices/valid";
            public const string ApiDeviceTypes = "/devices/list-device-type";
            public const string ApiStopProcess = "/devices/stop-process";
            public const string ApiDeviceUploadFile = "/devices/upload-file";
            public const string ApiReinstallDevices = "/devices/reinstall-devices";
            public const string ApiUnregitedDevices = "/devices/detect-new-devices";
            public const string ApiDeviceInstruction = "/devices/send-device-instruction";
            public const string ApiAddMissingDevices = "/devices/add-missing-devices";
            public const string ApiTransmitInfo = "/devices/transmit-info";
            public const string ApiTransmitData = "/devices/transmit-data";
            public const string ApiRequestOpenDoor = "/devices/request/open";
            public const string ApiDevicesMasterCard = "/devices/master-card";
            public const string ApiDeviceInitialize = "/devices/initialize";
            public const string ApiDeviceGetFilterByCompanyAndIcu = "/devices/list-filter";

            public const string ApiDevicesId = "/devices/{id}";
            public const string ApiDeviceInfo = "/devices/{id}/update-device-info";
            public const string ApiCopyDevices = "/devices/{id}/copy-device-settings";
            public const string ApiDeviceUpdateStatus = "/devices/{id}/toggle-status";
            public const string ApiCheckUserSetting = "/devices/{id}/check-user-info";
            public const string ApiCheckDeviceSetting = "/devices/{id}/check-device-info";
            public const string ApiCheckHolidaySetting = "/devices/{id}/check-holiday-info";
            public const string ApiCheckTimezoneSetting = "/devices/{id}/check-timezone-info";
            public const string ApiDevicesAccessibleUsers = "/devices/{id}/accessible-user";
            public const string ApiDevicesUserInfoExport = "/devices/{id}/export-user-info";
            public const string ApiDevicesAccessibleUsersExport = "/devices/{id}/accessible-user/export";
            public const string ApiDeviceHistory = "/devices/{id}/history";
            public const string ApiReUpdateUpTimeDevice = "devices/recheck-uptime";
            public const string ApiReUpdateUpTimeDeviceById = "devices/{id}/recheck-uptime";
            public const string ApiDevicesUsersInfo = "/devices/{id}/user-info";
            public const string ApiDevicesCondition = "/devices/condition/{mprId}";
            public const string ApiExportUserMasterCard = "/devices/{id}/export-user-master-card";
            public const string ApiDevicesUserMasterCard = "/devices/{id}/user-master-card";
            public const string ApiDevicesUsersInfoByCardId = "/devices/{id}/user-info-by-cardid";

            public const string ApiDeviceMessage = "/device-message";
            public const string ApiUpdateDeviceMessage = "/device-message/{id}";

            public const string ErrorPage = "/error/systemerror";
            public const string NotFoundPage = "/error/pagenotfound";
            public const string AccessDeniedPage = "/error/accessdenied";

            public const string ApiEventLogs = "/event-logs";
            public const string ApiEventLogsInit = "/event-logs/init";
            public const string ApiEventLogsReport = "/event-logs/report";
            public const string ApiEventLogsExport = "/event-logs/export";
            public const string ApiEventLogsRecovery = "/event-logs/recovery";
            public const string ApiEventLogsExportPdf = "/event-logs/exportpdf";
            public const string ApiEventLogsOpenReport = "/event-logs/open/report";
            public const string ApiEventLogsReportInit = "/event-logs/report/init";
            public const string ApiEventLogsEventInquiry = "/event-logs/inquiry";
            public const string ApiEventLogsReportExport = "/event-logs/Report/export";
            public const string ApiEventLogsRecoveryDevices = "/event-logs/recovery-list";

            public const string ApiHolidays = "/holidays";
            public const string ApiHolidaysId = "/holidays/{id}";

            public const string ApiMonitors = "/monitors";
            public const string ApiMonitorsDoorList = "/monitors/door-list";

            public const string ApiSettings = "/settings";
            public const string ApiSettingsId = "/settings/{id}";

            public const string ApiSystemLogs = "/system-logs";
            public const string ApiSystemLogAll = "/system-logs-all";
            public const string ApiSystemLogsExport = "/system-logs/export";
            public const string ApiSystemLogsExportPdf = "/system-logs/exportpdf";
            public const string ApiSystemLogsTypeListItems = "/system-logs/type-list";
            public const string ApiSystemLogsActionListItems = "/system-logs/action-list";

            public const string ApiTimezones = "/timezones";
            public const string ApiTimezonesId = "/timezones/{id}";

            public const string ApiUsers = "/users";
            public const string ApiUsersId = "/users/{id}";
            public const string ApiUsersTest = "/users/reboot-test";
            public const string ApiUsersImport = "/users/import";
            public const string ApiUsersExport = "/users/export";
            public const string ApiUsersCardCount = "/users/count/{id}";
            public const string ApiUsersEditCards = "/users/{id}/edit-cards";
            public const string ApiUsersImportProjectD = "/users/import/projectd";
            public const string ApiUsersExportProjectD = "/users/export/projectd";
            public const string ApiUsersAccessibleDoors = "/users/{id}/accessible-doors";
            public const string ApiUsersAccessibleDoorsExport = "/users/{id}/accessible-doors/export";
            public const string ApiUserIdentification = "/users/{id}/identification";
            public const string ApiCardByUser = "/users/{id}/identification/{cardId}";
            public const string ApiAccessHistoryUser = "/users/{id}/access-history";
            public const string ApiAccessHistoryEachUser = "/users/access-history";
            public const string ApiGetDynamicQrByUser = "/my-qrcode";

            public const string ApiUserArmys = "/army-users";
            public const string ApiUserArmysId = "/army-users/{id}";

            public const string ApiVisits = "/visits";
            public const string ApiVisitsId = "/visits/{id}";
            public const string ApiVisitsAccessHistory = "visits/{id}/access-history";
            public const string ApiVisitsInit = "/visits/init";
            public const string ApiVisitsReport = "/visits/report";
            public const string ApiVisitsReportInit = "/visits/report/init";
            public const string ApiVisitsReleaseCard = "/visits/return";
            public const string ApiVisitsChangeStatus = "/visits/change-status";
            public const string ApiVisitsPreRegister = "/visits/pre-register";
            public const string ApiVisitsPreRegisterInit = "/visits/pre-register/init";
            public const string ApiVisitsExport = "/visits/export";
            public const string ApiVisitsReportExport = "/visits/report/export";
            public const string ApiVisitsSetting = "/settings/visit";
            public const string ApiVisitIdentification = "/visits/{id}/identification";
            public const string ApiGetLengthVisitsReview = "/visits/waiting-for-review";
            public const string ApiApprovedVisitor = "/visits/{id}/approved";
            public const string ApiRejectVisitor = "/visits/{id}/reject";
            public const string ApiReturnCard = "/visits/return";
            public const string ApiAssignedDoorVisitor = "/visits/{id}/assigned-door";
            public const string ApiHistoryVisitor = "/visits/{id}/history";

            public const string ApiVisitArmy = "/army-visits";
            public const string ApiVisitArmyId = "/army-visits/{id}";
            public const string ApiVisitArmyReviewCount = "/army-visits/waiting-for-review";

            public const string ApiWgSettings = "/wgsettings";
            public const string ApiWgSettingsId = "/wgsettings/{id}";
            public const string ApiWgSettingDefaultForIcuDevice = "/wgsettings/default";

            public const string ApiGetWorkingTypeCompany = "/working-types";
            public const string ApiAddWorkingTypeCompany = "/working-types";
            public const string ApiUpdateWorkingTypeCompany = "/working-types/{id}";
            public const string ApiGetWorkingTypeCompanyDetail = "/working-types/{id}";
            public const string AssignMultipleUsersToWorkingTime = "/working-types/{id}/users";
            public const string AssignUserToDeFaultWorkingTime = "/assign/user/working-time/default";


            public const string ApiCornerSetting = "/corners";
            public const string ApiUpdateCornerSetting = "/corners/{id}";
            public const string ApiUserDiscount = "/user-discounts";
            public const string ApiUpdateUserDiscount = "/user-discounts/{id}";
            public const string ApiMealSetting = "/meal-settings";
            public const string ApiUpdateMealSetting = "/meal-settings/{id}";
            public const string ApiExceptionalMeal = "/exceptional-meals";
            public const string ApiUpdateExceptionalMeal = "/exceptional-meals/{id}";

            public const string ApiMealEventLog = "/meal-event-logs";
            public const string ApiUpdateMealEventLog = "/meal-event-logs/{id}";
            public const string ApiReportMealEventLog = "/meal-event-logs-reports";
            public const string ApiReportMealEventLogByBuilding = "/meal-event-log-report-buildings";
            public const string ApiReportMealEventLogByCorner = "/meal-event-log-report-corners";
            public const string ApiMealEventLogExportToExcel = "/meal-event-logs-excels";


            public const string ApiValidationDynamicQrByUser = "/validations/qrcode";

            public const string ApiUploadFile = "/upload/";

            public const string ParameterId = "{id}";

            public const string ApiRoute = "";

            public const string ApiLogin = "/login";

            public const string ApiCompanyExpired = "/company-expired";
            public const string ApiCompanyReminder = "/company-reminder";

            public const string ApiPartTimes = "/parttimes";

            public const string ApiCurrentLogo = "/current-logo";
            public const string ApiEventType = "/event-type";
            public const string ApiCommonSettings = "/common-settings";
            public const string ApiBackupRequest = "/backup-request";
            public const string ApiTestSetSendIcuUserCount = "/set-send-user-count";

            public const string ApiInformationVersionSystem = "/versions";

            public const string ApiGetPlugInCompany = "/plugins";
            public const string ApiUpdatePlugInCompany = "/plugins/{id}";




            // [Edward] 2020-05-12
            // API funciton for Duali-Korea
            public const string ApiAttendance = "/duali/attendance";
        }

        /// <summary>
        /// Config for swagger
        /// </summary>
        public class Swagger
        {
            public const string Header = "header";
            public const string V1 = "v1";
            public const string CorsPolicy = "CorsPolicy";
            public const string SwaggerJsonPath = "/swagger/v1/swagger.json";
            public const string MyApiV1 = "Versioned API v1.0";
        }


        public class CornerSetting
        {
            public const string AddSuccess = "Add new CornerSetting Success";
            public const string AddFailed = "Add new CornerSetting Failed";
            public const string CheckExistsCornerSetting = "This corner already exits";
            public const string UpdateSuccess = "Update CornerSetting Success";
            public const string UpdateFailed = "Update CornerSetting Failed";
            public const string DeleteSuccess = "Delete CornerSetting Success";
            public const string DeleteFailed = "Delete CornerSetting Failed";
        }

        public class UserDiscount
        {
            public const string AddSuccess = "Add new UserDiscount Success";
            public const string AddFailed = "Add new UserDiscount Failed";
            public const string UpdateSuccess = "Update UserDiscount Success";
            public const string UpdateFailed = "Update UserDiscount Failed";
            public const string DeleteSuccess = "Delete UserDiscount Success";
            public const string DeleteFailed = "Delete UserDiscount Failed";
        }
        /// <summary>
        /// Define key for appsetting config 
        /// </summary>
        public class Settings
        {
            public const string FileSettings = "Settings";
            public const string DeviceTypes = "DeviceTypes";
            public const string DefaultCulture = "Cultures:Default";
            public const string OptionCulture = "Cultures:Option";
            public const string PageSize = "Pagination:PageSize";
            public const string LoginExpiredTime = "Login:ExpiredTime";
            public const string ErrorMessage = "ErrorMessage";
            public const string SettingJsonFile = "appsettings.json";
            public const string ConnectionString = "ConnectionString";
            public const string HealthCheckTimeout = "HealthCheck:Timeout";
            public const string DefaultConnection = "DefaultConnection";
            public const string DefaultEnvironmentConnection = "ConnectionStrings__DefaultConnection";
            public const string QueueConnectionSettingsHost = "QueueConnectionSettings:Host";
            public const string QueueConnectionSettingsVirtualHost = "QueueConnectionSettings:VirtualHost";
            public const string QueueConnectionSettingsPort = "QueueConnectionSettings:Port";
            public const string QueueConnectionSettingsUserName = "QueueConnectionSettings:UserName";
            public const string QueueConnectionSettingsPassword = "QueueConnectionSettings:Password";
            public const string QueueConnectionSettingsEnableSsl = "QueueConnectionSettings:EnableSsl";
            public const string QueueConnectionSettingsCertPath = "QueueConnectionSettings:CertPath";
            public const string QueueConnectionSettingsCertPassphrase = "QueueConnectionSettings:CertPassphrase";
            public const string QueueEnvironmentConnectionSettingsHost = "QueueConnectionSettings__Host";
            public const string QueueEnvironmentConnectionSettingsVirtualHost = "QueueConnectionSettings__VirtualHost";
            public const string QueueEnvironmentConnectionSettingsPort = "QueueConnectionSettings__Port";
            public const string QueueEnvironmentConnectionSettingsUserName = "QueueConnectionSettings__UserName";
            public const string QueueEnvironmentConnectionSettingsPassword = "QueueConnectionSettings__Password";
            public const string QueueEnvironmentConnectionSettingsEnableSsl = "QueueConnectionSettings__EnableSsl";
            public const string QueueEnvironmentConnectionSettingsCertPath = "QueueConnectionSettings__CertPath";
            public const string QueueEnvironmentConnectionSettingsCertPassphrase = "QueueConnectionSettings_CertPassphrase";
            public const string QueueSettings = "QueueSettings";
            public const string ResourcesDir = "Resources";
            public const string JwtSection = "jwt";
            public const string Cultures = "Cultures";
            public const string DeMasterProCloudDataAccess = "DeMasterProCloud.DataAccess";
            public const string ExpiredCompanyDays = "ExpiredCompanyDays";
            public const string IosApp = "MobileApp:Ios";
            public const string AndroidApp = "MobileApp:Android";
            public const int DefaultExpiredCompanyDays = 60;
            public const int MaxUserSendToIcu = 5;
            public const int MaxUserSendToITouch = 1000;

            public const string DefaultExpiredTime = "60";
            public const string MaxTimezone = "MaxTimezone";
            public const string MaxHoliday = "MaxHoliday";
            public const int ReinstallTimeout = 60000;
            public const int ResponseTimeout = 10000;
            public const int ReinstallStartTimeout = 1500;
            public const int ReinstallTryCount = 3;
            public const int MainFirmwareSendInstruction = 15000;
            public const int DefaultMaxTimezone = 21;
            public const int DefaultMaxHoliday = 32;
            public const string EncryptKey = "Encryptor:Key";
            public const string EncryptIv = "Encryptor:IV";
            public const string MaxCopySettingDoor = "MaxCopySettingDoor";
            public const int DefaultMaxCopySettingDoor = 5;
            public const string DefaultTimezoneTime = "{\"From\":0, \"To\":1439}";
            public const string DefaultTimezoneNotUse = "{\"From\":0, \"To\":0}";
            public const string MaxTimezoneTimeHour = "23";
            public const string MaxTimezoneTimeMinute = "59";
            public const double MaxTimezoneTimeHourMinute = 23.99;
            public const string MaxMprCondition = "MaxMprCondition";
            public const string ExpiredSessionTime = "ExpiredSessionTime";
            public const int DefaultMprDuration = 5;
            public const int DefaultLockOpenDurationSeconds = 3;
            public const int DefaultInOutSet = 2;
            public const string DefaultCompanyName = "DefaultCompany:CompanyName";
            public const string DefaultAccessGroupFullAccess = "DefaultAccessGroup:AccessGroupNameFullAccess";
            public const string DefaultAccessGroupNoAccess = "DefaultAccessGroup:AccessGroupNameNoAccess";
            public const string DefaultAccessGroupVisitAccess = "DefaultAccessGroup:AccessGroupNameVisitAccess";
            public const string DefaultCompanyCode = "DefaultCompany:CompanyCode";
            public const string DefaultContact = "DefaultCompany:Contact";
            public const string DefaultCompanyUsername = "DefaultCompany:Username";
            public const string DefaultCompanyPassword = "DefaultCompany:Password";
            public const string DefaultAccountUsername = "DefaultAccount:Username";
            public const string DefaultAccountPassword = "DefaultAccount:Password";
            public const string DomainFrontEnd = "Domain:FrontEnd";
            public const string MailSettings = "MailSettings";
            public const string MailDevelopSettings = "MailDevelopSettings";
            public const string IcuDefaultValue = "ffffffff";
            //public const string EnablePerformanceLog = "EnablePerformanceLog";
            public const string MaxIcuUser = "MaxIcuUser";
            public const int DefaultMaxIcuUser = 5000;
            public const int DefaultMaxPopUser = 100000;
            public const int NumberTimezoneOfDay = 4;
            public const int NumOfSplitFile = 10;
            public const int MaxSizeSendToIcu = 4 * 1024;
            public const int DefaultVerifyMode = 1;
            public const int DefaultMprInterval = 60;
            public const int DefaultMprAuthCount = 1;
            public const int DefaultSensorDuration = 1;
            public const int DefaultSensorType = 1;
            public const int DefaultBackupPeriod = 3;
            public const int DefaultPositionPassageTimezone = 0;
            public const int DefaultPositionActiveTimezone = 1;

            public const int Buzzer_ON = 0;
            public const int Buzzer_OFF = 1;


            public const string MonitoringMaxRecordDisplay = "monitoring_max_record_display";
            public const string MonitoringEventTypeDefault = "monitoring_event_type_default";
            public const string EnablePerformanceLog = "enable_performance_log";
            public const string ProcedureOmission = "procedure_omission";
            public const string PaginationPage = "pagination";
            public const string ValidImageExtension = "valid_image_extension";
            public const string HasNotificationEmail = "notification_email";
            public const string ListUserToNotificationEmail = "list_user_to_notification";
            public const string Logo = "logo";

            public const int LengthCharacterGenQrCode = 7;
            public const string CharacterGenQrCode = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            public const string NameAccessGroupVisitor = "VAG-";
            public const int TimeCheckVisitExpired = 24;
        }

        public class RabbitMq
        {
            public const int Scheduler = 120;
            public const string ExchangeName = "amq.topic";
            public const string ExChangeType = "amq.topic";
            public const string EventLogTopic = ".topic.event";
            public const string EventLogJsonTopic = ".topic.event_json";
            public const string EventLogResponseTopic = ".topic.event_log_response";
            public const string EventLogEventCountTopic = ".topic.event_count";
            public const string EventLogEventCountResponseTopic = ".topic.event_count_response";
            public const string EventCountJsonTopic = ".topic.event_count_json";
            public const string EventRecoveryTopic = ".topic.event_recovery";
            public const string EventRecoveryResponseTopic = ".topic.event_recovery_response";
            public const string ConfigurationTopic = ".topic.config";
            public const string ConfigurationResponseTopic = ".topic.config_response";
            public const string AccessControlTopic = ".topic.access_control";
            public const string AccessControlResponseTopic = ".topic.access_control_response";
            public const string HolidayTopic = ".topic.holiday";
            public const string HolidayResponseTopic = ".topic.holiday_response";
            public const string DeviceOnlineTopic = ".topic.online";
            public const string DeviceStatus = ".topic.device_status";
            public const string DeviceInfoResponseTopic = ".topic.device_info_response";
            public const string DeviceInfoTopic = ".topic.device_info";
            public const string DeviceSettingTopic = ".topic.load_device_setting";
            public const string LoadHolidayTopic = ".topic.load_holiday";
            public const string LoadHolidayResponseTopic = ".topic.load_holiday_response";
            public const string LoadTimezoneTopic = ".topic.load_timezone";
            public const string LoadTimezoneResponseTopic = ".topic.load_timezone_response";
            public const string LoadTimezoneWebAppTopic = ".topic.load_timezone_webapp";
            public const string LoadUserTopic = ".topic.load_user";
            public const string LoadUserResponseTopic = ".topic.load_user_response";
            public const string LoadUserWebAppTopic = ".topic.load_user_webapp";
            public const string DeviceSettingResponseTopic = ".topic.load_device_setting_response";
            public const string TimezoneTopic = ".topic.timezone";
            public const string TimezoneResponseTopic = ".topic.timezone_response";
            public const string NotificationTopic = ".topic.notification";
            public const string DeviceInstructionTopic = ".topic.device_instruction";
            public const string DeviceInstructionResponseTopic = ".topic.device_instruction_response";
            public const string EventLogTaskQueue = "event_log_task_queue";
            public const string SetUserTaskQueue = "set_user_task_queue";
            public const string MultipleMessagesTaskQueue = "multiple_messages_task_queue";
            public const string FileTranferTopic = ".topic.file_transfer";
            public const string FileTranferResponseTopic = ".topic.file_transfer_response";
            public const string LongProcessProgressTopic = ".topic.long_process_progress";
            public const string DeviceMessageTopic = ".topic.message";
            public const string DeviceMessageResponseTopic = ".topic.message_response";
            public const string DeviceSettingWebAppTopic = ".topic.device_setting_webapp";
            public const string HolidayWebAppTopic = ".topic.load_holiday_webapp";
            public const string RequestOpenTopic = ".topic.event.request_open";

            public const string DoorStatusTopic = ".topic.door_status";
            public const string NormalUser = "webapp";
            public const string NormalUserPassWord = "webapp123";
            public const int MaxSendingMessageRetry = 3;
            //Message response timout in milliseconds
            public const int MessageResponseTimeout = 30000;
            public const int MessageResponseInterval = 100;
            public static readonly string Permission = "management";
            public const string TopicPermissionNormalUserList = ".topic.notification.{0}|.topic.device_status.{0}|.topic.event_json.{0}|.topic.long_process_progress|.topic.event_count_json.{0}";
            public const string TopicPermissionAdminUserList = ".topic.notification.*|.topic.device_status|.topic.event_json.*|.topic.long_process_progress|.topic.load_user_webapp|.topic.event_count_json{0}|.topic.device_setting_webapp|.topic.load_timezone_webapp|.topic.load_holiday_webapp";
            public const int MaxChannelsPerConnection = 2000;
            public const int Max = 3;
        }

        /// <summary>
        /// Config for logger system
        /// </summary>
        public class Logger
        {
            public const string LogFile = "Logging:LogDir";
            public const string Logging = "Logging";
        }

        public class Pagination
        {
            public const string Start = "start";
            public const string SearchValue = "search[value]";
            public const string OrderColumn = "order[0][column]";
            public const string OrderDirection = "order[0][dir]";
            public const string DefaultOrder = "asc";
        }

        public class ClaimName
        {
            public const string Username = "Username";
            public const string UserCode = "UserCode";
            public const string CompanyCode = "CompanyCode";
            public const string CompanyName = "CompanyName";
            public const string AccountId = "AccountId";
            public const string AccountType = "AccountType";
            public const string CompanyId = "CompanyId";
            public const string BearerToken = "BearerToken";
        }
        public class MailSetting
        {
            public const string DeliveryMethod = "MailSettings:DeliveryMethod";
            public const string From = "MailSettings:From";
            public const string Host = "MailSettings:Host";
            public const string Port = "MailSettings:Port";
            public const string EnableSsl = "MailSettings:EnableSsl";
            public const string DefaultCredentials = "MailSettings:DefaultCredentials";
            public const string UserName = "MailSettings:UserName";
            public const string Password = "MailSettings:Password";
        }

        public class Protocol
        {
            public const string AddUser = "ADD_USER";
            public const string DeleteUser = "DELETE_USER";
            public const string UpdateUser = "UPDATE_USER";

            public const string UpdateDeviceConfig = "UPDATE_DEVICE_CONFIG";
            public const string UpdateDeviceMessage = "UPDATE_DEVICE_MESSAGE";
            public const string DeleteDevice = "DELETE_DEVICE";

            public const string UpdateTimezone = "UPDATE_TIMEZONE";
            public const string DeleteTimezone = "DELETE_TIMEZONE";

            public const string UpdateHoliday = "UPDATE_HOLIDAY";

            public const string Notification = "NOTIFICATION";
            public const string EventLogWebApp = "EVENT_LOG_WEBAPP";
            public const string EventLogResponse = "EVENT_LOG_RESPONSE";
            public const string EventLogEventCount = "EVENT_COUNT";
            public const string EventLogEventRecovery = "EVENT_RECOVERY";
            public const string EventLogEventCountResponse = "EVENT_COUNT_RESPONSE";
            public const string EventCountWebApp = "EVENT_COUNT_WEBAPP";

            public const string LoadDeviceInfo = "LOAD_DEVICE_INFO";
            public const string DeviceInstruction = "DEVICE_INSTRUCTION";

            public const string FileDownLoad = "FILE_DOWNLOAD";
            public const string LongProcessProgress = "LONG_PROCESS_PROGRESS";
            public const string ConnectionStatus = "CONNECTION_STATUS";

            public const string LoadDeviceSetting = "LOAD_DEVICE_SETTING";
            public const string LoadHoliday = "LOAD_HOLIDAY";
            public const string LoadTimezone = "LOAD_TIMEZONE";
            public const string LoadAllUser = "LOAD_ALL_USER";
            public const string LoadUser = "LOAD_USER";
            public const string LoadAllUserWebApp = "LOAD_ALL_USER_WEBAPP";

            public const string LoadDeviceSettingWebApp = "LOAD_DEVICE_SETTING_WEBAPP";
            public const string LoadHolidayWebApp = "LOAD_HOLIDAY_WEBAPP";
            public const string LoadTimezoneWebApp = "LOAD_TIMEZONE_WEBAPP";

            public const string DoorStatus = "DOOR_STATUS";
        }

        public class MessageType
        {
            public const string Info = "info";
            public const string Success = "success";
            public const string Failure = "failure";
            public const string Error = "error";
            public const string Warning = "warning";
        }

        public class ActionType
        {
            public const string Install = "Install";
            public const string Reinstall = "reinstall";
            public const string TransmitData = "transmit data";
            public const string SetTimeAndGetInfo = "refresh";
            public const string UpdateDevice = "update device";
        }

        public class LongProgressName
        {
            public const string Reinstall = "reinstalling";
            public const string TransmitData = "transmitting";
            public const string Downloading = "downloading";
            public const string Recorvering = "Recorvering";
            public const string Preparing = "Preparing";
            public const string Updating = "Updating";
            public const string ReinstallFailed = "failed";
            public const string TransmitDataFailed = "failed";
            public const string DownloadingFailed = "failed";
            public const string RecorveringFailed = "failed";
            public const string PreparingFailed = "failed";
            public const string UpdatingFailed = "failed";
        }
        public class LongProgressPercentage
        {
            public const decimal ReinstallStep0 = 5;
            public const decimal ReinstallStep1 = 30;
        }

        public class NotificationType
        {
            public const string InstallSuccess = "Install success";
            public const string InstallError = "Install error";

            public const string ReinstallSuccess = "Reinstall success";
            public const string ReinstallError = "Reinstall error";

            public const string TransmitDataSuccess = "Transmit data success";
            public const string TransmitDataError = "Transmit data error";

            public const string UploadSuccess = "Upload firmware success";
            public const string UploadError = "Upload firmware error";

            public const string SendDeviceInstructionSuccess = "Send device instruction success";
            public const string SendDeviceInstructionError = "Send device instruction error";

            public const string FileTransferError = "Upload file error";
            public const string SendDeviceConfigError = "Send device config error";
            public const string SendUserError = "Send User error";

        }

        public class CommandType
        {
            public const string Open = "Open";
            public const string SetTime = "SetTime";
            public const string Reset = "Reset";
            public const string ForceOpen = "ForceOpen";
            public const string ForceClose = "ForceClose";
            public const string Release = "Release";
            public const string DeleteAllUsers = "Delete_all_users";
            public const string DeleteAllEvents = "Delete_all_events";
            public const string UpdateDeviceState = "UpdateDeviceState";
            public const string UpdateFirmware = "UpdateFirmware";
            public const string StopUpdateFW = "Stop_Update_Firmware";
        }

        public class ExportType
        {
            public const string LoadAllUser = "LoadAllUser";
            public const string LoadMasterCardUser = "LoadMasterCardUser";
        }

        public class AutoRenew
        {
            public const int SchedulerDay = 1;
            public const int SchedulerHour = 23;
            public const int SchedulerMinute = 59;
            public const int KeySize = 128;
            public const int BlockSize = 128;
        }

        public class DynamicQr
        {
            public const string AllowChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            public const string AllowCharsNumber = "0123456789";
            public const int LenPassCode = 6;
            public const string PassCodeString = "******";
            public const string Salt = "X";
            public const int MaxByte = 31;
            public const int LenghtOfSecretCode = 16;
            public const string NameProject = "DMPW";
            public const int MaxLengthQr = 80;
            public const int TimePeriod = 60;
            public const int SubStringSeconds = 2;
            public const string Key = "DualiDMPWDuali74";
        }

        public class Attendance
        {
            public const string ValidDays = "Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday";
            public const string ValidType = "Half,Full,Holiday";
            public const string DefaultName = "Standard Working Time";
            public const string DefaultWorkingTime = "[{\"Name\":\"Monday\",\"Start\":\"09:00\",\"End\":\"18:30\",\"Type\":\"Full\"},{\"Name\":\"Tuesday\",\"Start\":\"09:00\",\"End\":\"18:30\",\"Type\":\"Full\"},{\"Name\":\"Wednesday\",\"Start\":\"09:00\",\"End\":\"18:30\",\"Type\":\"Full\"},{\"Name\":\"Thursday\",\"Start\":\"09:00\",\"End\":\"18:30\",\"Type\":\"Full\"},{\"Name\":\"Friday\",\"Start\":\"09:00\",\"End\":\"18:30\",\"Type\":\"Full\"},{\"Name\":\"Saturday\",\"Start\":\"00:00\",\"End\":\"00:00\",\"Type\":\"Holiday\"},{\"Name\":\"Sunday\",\"Start\":\"00:00\",\"End\":\"00:00\",\"Type\":\"Holiday\"}]";
            public const int SchedulerAttendance = 4;
            public const string In = "In";
            public const string Out = "Out";
            public const int SchedulerDay = 1;
            public const int SchedulerHour = 23;
            public const int SchedulerMinute = 59;
            public const string RangTimeToDay = "Today";
            public const string RangTimeAllDay = "Allday";
        }

        public class PlugIn
        {
            public const string CardIssuing = "CardIssuing";
            public const string AccessControl = "AccessControl";
            public const string TimeAttendance = "TimeAttendance";
            public const string VisitManagement = "VisitManagement";
            public const string CanteenManagement = "CanteenManagement";
            public const string QrCode = "QrCode";
            public const string ScreenMessage = "ScreenMessage";
            public const string PassCode = "PassCode";
            // [Edward] 2020.04.22
            // Make plugin about military
            public const string ArmyManagement = "ArmyManagement";
        }

        public class PlugInValue
        {
            public const bool CardIssuing = false;
            public const bool AccessControl = false;
            public const bool TimeAttendance = false;
            public const bool VisitManagement = false;
            public const bool CanteenManagement = false;
            public const bool Common = true;
            public const bool ScreenMessage = false;
            public const bool QrCode = false;
            public const bool PassCode = false;
            public const bool ArmyManagement = false;


            public const string CardIssuingDescription = "";
            public const string AccessControlDescription = "This function enable for the company that need to control door. This including door lock management ...";
            public const string TimeAttendanceDescription = "Function related to Time Attendance management. E.g Clockin / clock out report, working time, holiday application";
            public const string VisitManagementDescription = "Provice function for managing visitors and visit such as sending invitation, access level, visit report ...";
            public const string CanteenManagementDescription = "";
            public const string CommonDescription = "The function that apply for all the system";
            public const string ScreenMessageDescription = "This plugins enable for the company that use readers that have screen. E.g iTouchPop 2A";
            public const string QrCodeDescription = "This plugins enable for the company that use QR code as identification";
            public const string PassCodeDescription = "This plugins enable for the company that use Pass Code as identification. Use supported device such as: DE960, iTouchPop 2A";

            public const string ArmyManagementDescription = "";
        }

        public class Link
        {
            public const string Android_Download = "https://play.google.com/store/apps/details?id=com.demasterpro";
            public const string IOS_Download = "https://apps.apple.com/us/app/id1500403553";
        }

    }
}
