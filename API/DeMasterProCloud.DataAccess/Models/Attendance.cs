using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeMasterProCloud.DataAccess.Models
{
    public class Attendance
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Start { get; set; }      // Epoch
        public double End { get; set; }        // Epoch
        public double ClockIn { get; set; }      // Epoch
        public double ClockOut { get; set; }        // Epoch
        
        public DateTime StartD { get; set; }      // Epoch
        public DateTime EndD { get; set; }        // Epoch
        public DateTime ClockInD { get; set; }      // Epoch
        public DateTime ClockOutD { get; set; } 

        public int Type { get; set; }
        public int UserId { get; set; }
        
        public int CompanyId { get; set; }
        
        public virtual User User { get; set; }
        
        public virtual Company Company { get; set; }
        
        [Column(TypeName = "jsonb")]
        public string WorkingTime { get; set; }
        
        public int? EditedBy { get; set; }
    }
}