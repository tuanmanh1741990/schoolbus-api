﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataAccess.Models
{
    public class DeviceMessage
    {
        public int Id { get; set; }
        public int MessageId { get; set; }
        public string Content { get; set; }
        public string Remark { get; set; }
        //public int IcuId { get; set; }
        public int CompanyId { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public Company Company { get; set; }
        //public IcuDevice IcuDevice { get; set; }
    }
}
