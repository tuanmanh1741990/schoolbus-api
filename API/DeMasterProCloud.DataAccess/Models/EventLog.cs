﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class EventLog
    {
        public EventLog()
        {
            MealEventLog = new HashSet<MealEventLog>();
        }

        public int Id { get; set; }
        public string Antipass { get; set; }
        public string CardId { get; set; }
        public int IssueCount { get; set; }
        public short CardType { get; set; }
        /// <summary>
        /// Card Status
        /// EventLog is historical data. It is to check the past log.
        /// Therefore it should be stored according to the data at that point.
        /// So, this attribute should be added to table.
        /// </summary>
        public short CardStatus { get; set; }
        public int? CompanyId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? DeptId { get; set; }
        public string DoorName { get; set; }
        public DateTime EventTime { get; set; }
        public int EventType { get; set; }
        public int IcuId { get; set; }
        public long Index { get; set; }
        public string KeyPadPw { get; set; }
        public string UserName { get; set; }
        public int? UserId { get; set; }
        public bool IsVisit { get; set; }
        public int? VisitId { get; set; }
        [JsonIgnore]
        public Company Company { get; set; }
        [JsonIgnore]
        public IcuDevice Icu { get; set; }
        [JsonIgnore]
        public User User { get; set; }
        [JsonIgnore]
        public Visit Visit { get; set; }

        public ICollection<MealEventLog> MealEventLog { get; set; }
    }
}
