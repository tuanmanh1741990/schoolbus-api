﻿using System;
using System.Collections.Generic;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class VisitArmy
    {
        public VisitArmy()
        {
        }

        public int Id { get; set; }
        public int VisitId { get; set; }
        public string MilitaryNumber { get; set; }
        public string Relationship { get; set; }
        public string BackGroundCheckNumber { get; set; }
        public int Gender { get; set; }
        public string VisitorRank { get; set; }
        public string VisiteeRank { get; set; }

        public Visit Visit { get; set; }
    }
}
