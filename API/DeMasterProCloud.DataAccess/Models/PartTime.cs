﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class PartTime
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public bool IsDeleted { get; set; }

        public int UserId { get; set; }
        public string Contact { get; set; }
        public int HourlyWage { get; set; }
        public string AccountNumber { get; set; }
        public string Bank { get; set; }
        public string AccountHolder { get; set; }

        public Company Company { get; set; }
        public User User { get; set; }

    }
}
