﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class Account
    {
        public Account()
        {
            SystemLog = new HashSet<SystemLog>();
            Department = new HashSet<Department>();
        }

        public int Id { get; set; }
        public int? CompanyId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Password { get; set; }
        public bool RootFlag { get; set; }
        //public short Status { get; set; }
        public short Type { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string Username { get; set; }
        public bool IsDeleted { get; set; }
        public virtual Company Company { get; set; }
        //public string Remarks { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string TimeZone { get; set; }
        public ICollection<SystemLog> SystemLog { get; set; }
        public ICollection<Department> Department { get; set; }
        public string RefreshToken { get; set; }
        public DateTime CreateDateRefreshToken { get; set; }
    }
}
