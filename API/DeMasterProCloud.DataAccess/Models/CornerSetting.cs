﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class CornerSetting
    {
        public int Id { get; set; }
        //public int? CompanyId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Company Company { get; set; }
        [ForeignKey("Company")]
        public int CompanyId { get; set; }

        public ICollection<MealSetting> MealSetting { get; set; }
    }
}
