﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.CompilerServices;
using System.Text;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class ExceptionalMeal
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public decimal Price { get; set; }
        public MealSetting MealSetting { get; set; }
        [ForeignKey("MealSetting")]
        public int MealSettingId { get; set; }
    }
}
