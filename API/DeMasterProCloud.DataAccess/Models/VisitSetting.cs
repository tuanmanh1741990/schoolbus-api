using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class VisitSetting
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int CompanyId { get; set; }
        [Column(TypeName = "jsonb")]
        public string FirstApproverAccounts { get; set; }
        public string SecondsApproverAccounts { get; set; }
        public int ApprovalStepNumber{ get; set; }

        public bool OutSide { get; set; } = false;
        public Company Company { get; set; }
    }
}