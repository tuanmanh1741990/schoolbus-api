﻿using System;
using System.Collections.Generic;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class MealType
    {
        public MealType()
        {
        }

        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
        public Company Company { get; set; }

        public ICollection<MealSetting> MealSetting { get; set; }

    }
}
