﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace DeMasterProCloud.DataAccess.Models
{
    public class MessageLog
    {
        public int Id { get; set; }
        public string MsgId { get; set; }
        public string Topic { get; set; }
        public string Type { get; set; }
        public string PayLoad { get; set; }
        public short Status { get; set; }
        public string GroupMsgId { get; set; }
        public string ProcessId { get; set; }
        public int ProgressIndex { get; set; }
        public DateTime? PublishedTime { get; set; }
        public DateTime? ResponseTime { get; set; }
        public int CompanyId { get; set; }
        public bool IsStopped { get; set; }
        public bool IsNotify { get; set; }
        public int? AccountId { get; set; }
        [JsonIgnore]
        public Company Company { get; set; }
    }
}
