﻿using Microsoft.EntityFrameworkCore;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class AppDbContext : DbContext
    {
        public AppDbContext()
        { }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseNpgsql(Helpers.GetConfigurationRoot().GetConnectionString(Constants.Settings.DefaultConnection));
            }
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CornerSetting> CornerSetting { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<EventLog> EventLog { get; set; }
        public virtual DbSet<Event> Event { get; set; }
        public virtual DbSet<Holiday> Holiday { get; set; }
        public virtual DbSet<IcuDevice> IcuDevice { get; set; }
        public virtual DbSet<Setting> Setting { get; set; }
        public virtual DbSet<SystemLog> SystemLog { get; set; }
        public virtual DbSet<Timezone> Timezone { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<AccessGroup> AccessGroup { get; set; }
        public virtual DbSet<AccessGroupDevice> AccessGroupDevice { get; set; }
        public virtual DbSet<MessageLog> MessageLog { get; set; }
        public virtual DbSet<UnregistedDevice> UnregistedDevice { get; set; }
        public virtual DbSet<Building> Building { get; set; }
        public virtual DbSet<DeviceMessage> DeviceMessage { get; set; }
        public virtual DbSet<Visit> Visit { get; set; }


        public virtual DbSet<Card> Card { get; set; }
        public virtual DbSet<PartTime> PartTime { get; set; }

        public virtual DbSet<WorkingType> WorkingType { get; set; }
        public virtual DbSet<Attendance> Attendance { get; set; }

        public virtual DbSet<PlugIn> PlugIn { get; set; }
        public virtual DbSet<VisitSetting> VisitSetting { get; set; }

        public virtual DbSet<VisitHistory> VisitHistory { get; set; }

        public virtual DbSet<Category> Category { get; set; }

        public virtual DbSet<CategoryOption> CategoryOption { get; set; }

        public virtual DbSet<UserCategoryOption> UserCategoryOption { get; set; }

        public virtual DbSet<UserArmy> UserArmy { get; set; }

        public virtual DbSet<VisitArmy> VisitArmy { get; set; }

        // Canteen

        public virtual DbSet<MealType> MealType { get; set; }
        public virtual DbSet<UserDiscount> UserDiscount { get; set; }
        public virtual DbSet<MealSetting> MealSetting { get; set; }
        public virtual DbSet<MealEventLog> MealEventLog { get; set; }

        public virtual DbSet<ExceptionalMeal> ExceptionalMeal { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Building>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.Property(e => e.Name).IsRequired().HasMaxLength(255);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Building)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Building_Company");

            });

            modelBuilder.Entity<AccessGroup>(entity =>
            {
                //entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).ValueGeneratedOnAdd();
                entity.Property(e => e.Name).IsRequired().HasMaxLength(255);
                entity.Property(e => e.IsDefault);
                entity.Property(e => e.IsDeleted);
                entity.HasIndex(e => e.CompanyId);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.AccessGroup)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccessGroup_Company");
            });

            modelBuilder.Entity<AccessGroupDevice>(entity =>
            {
                entity.HasKey(e => new { e.AccessGroupId, e.IcuId });

                entity.HasIndex(e => e.AccessGroupId);
                entity.HasIndex(e => e.IcuId);

                entity.HasOne(d => d.Icu)
                    .WithMany(p => p.AccessGroupDevice)
                    .HasForeignKey(d => d.IcuId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_AccessGroupDevice_IcuDevice");

                entity.HasOne(d => d.AccessGroup)
                    .WithMany(p => p.AccessGroupDevice)
                    .HasForeignKey(d => d.AccessGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccessGroupDevice_User");

                entity.HasOne(d => d.Tz)
                    .WithMany(p => p.AccessGroupDevice)
                    .HasForeignKey(d => d.TzId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AccessGroupDevice_Timezone");
            });

            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RefreshToken)
                    .HasMaxLength(256);

                entity.HasIndex(e => e.CreateDateRefreshToken);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(7);

                entity.Property(e => e.Contact).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Remarks).HasColumnType("character varying");
            });

            modelBuilder.Entity<Department>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.HasIndex(e => e.ParentId);

                entity.Property(e => e.DepartName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.DepartNo)
                    .IsRequired()
                    .HasColumnType("character varying");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Department)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Department_Company");

                entity.HasOne(d => d.Parent)
                    .WithMany(p => p.InverseParent)
                    .HasForeignKey(d => d.ParentId)
                    .HasConstraintName("FK_Department_Department");

                entity.HasOne(d => d.DepartmentManager)
                    .WithMany(p => p.Department)
                    .HasForeignKey(d => d.DepartmentManagerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Department_Account");

            });

            modelBuilder.Entity<EventLog>(entity =>
            {
                //entity.HasKey(e => e.CardId)
                entity.HasIndex(e => e.CompanyId);

                entity.HasIndex(e => e.IcuId);

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.Antipass).HasMaxLength(10);

                entity.Property(e => e.CardId).HasMaxLength(50);

                entity.Property(e => e.DoorName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.KeyPadPw).HasMaxLength(256);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.EventLog)
                    .HasForeignKey(d => d.CompanyId)
                    .HasConstraintName("FK_EventLog_Company");

                entity.HasOne(d => d.Icu)
                    .WithMany(p => p.EventLog)
                    .HasForeignKey(d => d.IcuId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_EventLog_IcuDevice");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.EventLog)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_EventLog_User");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.EventLog)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_EventLog_Visit");

            });

            modelBuilder.Entity<Holiday>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Remarks).HasColumnType("character varying");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Holiday)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Holiday_Company");
            });

            modelBuilder.Entity<IcuDevice>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.DeviceAddress)
                    .IsRequired()
                    .HasMaxLength(6);
                entity.Property(e => e.IpAddress).HasColumnType("character varying");
                entity.Property(e => e.MacAddress).HasMaxLength(20);
                entity.Property(e => e.ServerIp).HasMaxLength(50);
                entity.Property(e => e.ServerPort).HasMaxLength(8);
                entity.Property(e => e.FirmwareVersion).HasMaxLength(40);
                entity.Property(e => e.VersionReader0).HasMaxLength(40);
                entity.Property(e => e.VersionReader1).HasMaxLength(40);
                entity.Property(e => e.NfcModuleVersion).HasMaxLength(50);
                entity.Property(e => e.ExtraVersion).HasMaxLength(20);
                entity.Property(e => e.DoorStatus).HasMaxLength(20);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.IcuDevice)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IcuDevice_Company");

                entity.HasOne(d => d.Building)
                    .WithMany(p => p.IcuDevice)
                    .HasForeignKey(d => d.BuildingId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IcuDevice_Building");

                entity.HasOne(d => d.ActiveTz)
                    .WithMany(p => p.DoorActiveTz)
                    .HasForeignKey(d => d.ActiveTzId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IcuDevice_Timezone1");

                entity.HasOne(d => d.PassageTz)
                    .WithMany(p => p.DoorPassageTz)
                    .HasForeignKey(d => d.PassageTzId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_IcuDevice_Timezone");
            });

            modelBuilder.Entity<Setting>(entity =>
            {
                entity.Property(e => e.Key)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Value).HasColumnType("character varying");
            });

            modelBuilder.Entity<CornerSetting>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.Name).HasColumnType("character varying");

                entity.Property(e => e.Code).HasColumnType("character varying");

                entity.Property(e => e.Description).HasColumnType("character varying");

                entity.HasOne(d => d.Company)
                   .WithMany(p => p.CornerSetting)
                   .HasForeignKey(d => d.CompanyId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_CornerSetting_Company");
            });

            modelBuilder.Entity<SystemLog>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.HasIndex(e => e.CreatedBy);

                entity.Property(e => e.Content).HasColumnType("character varying");

                entity.Property(e => e.ContentDetails).HasColumnType("character varying");

                entity.Property(e => e.ContentIds).HasColumnType("character varying");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.SystemLog)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SystemLog_Company");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.SystemLog)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_SystemLog_Account");
            });

            modelBuilder.Entity<Timezone>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.FriTime1).HasMaxLength(50);

                entity.Property(e => e.FriTime2).HasMaxLength(50);

                entity.Property(e => e.FriTime3).HasMaxLength(50);

                entity.Property(e => e.HolType1Time1).HasMaxLength(50);

                entity.Property(e => e.HolType1Time2).HasMaxLength(50);

                entity.Property(e => e.HolType1Time3).HasMaxLength(50);

                entity.Property(e => e.HolType2Time1).HasMaxLength(50);

                entity.Property(e => e.HolType2Time2).HasMaxLength(50);

                entity.Property(e => e.HolType2Time3).HasMaxLength(50);

                entity.Property(e => e.HolType3Time1).HasMaxLength(50);

                entity.Property(e => e.HolType3Time2).HasMaxLength(50);

                entity.Property(e => e.HolType3Time3).HasMaxLength(50);

                entity.Property(e => e.MonTime1).HasMaxLength(50);

                entity.Property(e => e.MonTime2).HasMaxLength(50);

                entity.Property(e => e.MonTime3).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Remarks).HasColumnType("character varying");

                entity.Property(e => e.SatTime1).HasMaxLength(50);

                entity.Property(e => e.SatTime2).HasMaxLength(50);

                entity.Property(e => e.SatTime3).HasMaxLength(50);

                entity.Property(e => e.SunTime1).HasMaxLength(50);

                entity.Property(e => e.SunTime2).HasMaxLength(50);

                entity.Property(e => e.SunTime3).HasMaxLength(50);

                entity.Property(e => e.ThurTime1).HasMaxLength(50);

                entity.Property(e => e.ThurTime2).HasMaxLength(50);

                entity.Property(e => e.ThurTime3).HasMaxLength(50);

                entity.Property(e => e.TueTime1).HasMaxLength(50);

                entity.Property(e => e.TueTime2).HasMaxLength(50);

                entity.Property(e => e.TueTime3).HasMaxLength(50);

                entity.Property(e => e.WedTime1).HasMaxLength(50);

                entity.Property(e => e.WedTime2).HasMaxLength(50);

                entity.Property(e => e.WedTime3).HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Timezone)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Timezone_Company");
            });



            modelBuilder.Entity<User>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.HasIndex(e => e.DepartmentId);

                entity.Property(e => e.Address).HasMaxLength(100);

                //entity.Property(e => e.CardId).HasMaxLength(50);

                entity.Property(e => e.City).HasMaxLength(100);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.HomePhone).HasMaxLength(20);

                entity.Property(e => e.Job).HasMaxLength(100);

                entity.Property(e => e.KeyPadPw).HasMaxLength(256);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Nationality).HasMaxLength(100);

                entity.Property(e => e.OfficePhone).HasMaxLength(20);

                entity.Property(e => e.Position).HasMaxLength(100);

                entity.Property(e => e.PostCode).HasMaxLength(20);

                entity.Property(e => e.Remarks).HasColumnType("character varying");

                entity.Property(e => e.Responsibility).HasMaxLength(100);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Company1");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.DepartmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Department");

                entity.HasOne(d => d.AccessGroup)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.AccessGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_AccessGroup");

                entity.HasOne(d => d.WorkingType)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.WorkingTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_WorkingType");
            });

            modelBuilder.Entity<DeviceMessage>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Content).HasMaxLength(255);
                entity.Property(e => e.Remark).HasMaxLength(255);
                entity.HasOne(d => d.Company)
                    .WithMany(p => p.DeviceMessage)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_DeviceMessage_Company");

            });

            modelBuilder.Entity<MessageLog>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasIndex(e => e.Id);

                entity.Property(e => e.MsgId).IsRequired().HasMaxLength(100);
                entity.Property(e => e.Type).IsRequired().HasMaxLength(100);
                entity.Property(e => e.Topic).IsRequired().HasMaxLength(100);
                entity.Property(e => e.PayLoad).IsRequired();
            });

            modelBuilder.Entity<UnregistedDevice>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasIndex(e => e.Id);

                entity.Property(e => e.DeviceAddress).IsRequired().HasMaxLength(6);
                entity.Property(e => e.Status).IsRequired();
                entity.Property(e => e.IpAddress).IsRequired().HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.UnregistedDevice)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UnregistedDevice_Company");
            });

            //KJO - Visit 20190624
            modelBuilder.Entity<Visit>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);
                entity.Property(e => e.Status).IsRequired();
                entity.Property(e => e.CardId).HasMaxLength(50);

                entity.Property(e => e.Phone).HasMaxLength(20);

                entity.Property(e => e.Position).HasMaxLength(100);
                //entity.Property(e => e.VisitorName).IsRequired().HasMaxLength(255);

                //entity.Property(e => e.IsDeleted);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Visit)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Visit_Company");

                entity.HasOne(d => d.AccessGroup)
                    .WithMany(p => p.Visit)
                    .HasForeignKey(d => d.AccessGroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Visit_AccessGroup");
            });

            //KJO - Card 20190701
            modelBuilder.Entity<Card>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);
                entity.Property(e => e.Status).IsRequired();
                entity.Property(e => e.CardId).HasMaxLength(50);


                //entity.Property(e => e.VisitorName).IsRequired().HasMaxLength(255);

                //entity.Property(e => e.IsDeleted);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Card)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Card_Company");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Card)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Card_User");

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.Card)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Card_Visit");
            });

            //KJO - PartTime 오후 4:44 2019-08-14
            modelBuilder.Entity<PartTime>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                //entity.Property(e => e.VisitorName).IsRequired().HasMaxLength(255);

                //entity.Property(e => e.IsDeleted);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.PartTime)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PartTime_Company");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.PartTime)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PartTime_User");

            });

            modelBuilder.Entity<Attendance>(entity =>
            {
                entity.HasIndex(e => e.UserId);
                entity.Property(e => e.Type).IsRequired();
                entity.Property(e => e.Start).IsRequired();
                entity.Property(e => e.End).IsRequired();

                entity.Property(e => e.Date).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Attendance)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Attendance_Company");

            });
            modelBuilder.Entity<WorkingType>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);
                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.WorkingDay).HasColumnType("jsonb");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.WorkingType)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_WorkingType_Company");

            });

            modelBuilder.Entity<PlugIn>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.PlugIns).HasColumnType("jsonb");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.PlugIn)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Solution_Company");

            });

            modelBuilder.Entity<VisitSetting>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.FirstApproverAccounts).HasColumnType("jsonb");
                entity.Property(e => e.SecondsApproverAccounts).HasColumnType("jsonb");

                entity.HasOne(d => d.Company)
                    .WithOne(p => p.VisitSetting)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VisitSetting_Company");

            });

            modelBuilder.Entity<VisitHistory>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.VisitHistory)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VisitHistory_Company");

            });


            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.Category)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Category_Company");
            });

            modelBuilder.Entity<CategoryOption>(entity =>
            {
                entity.HasIndex(e => e.CategoryId);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.CategoryOption)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_CategoryOption_Category");
            });

            modelBuilder.Entity<UserCategoryOption>(entity =>
            {
                entity.HasIndex(e => e.CategoryOptionId);
                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.CategoryOption)
                    .WithMany(p => p.UserCategoryOption)
                    .HasForeignKey(d => d.CategoryOptionId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_UserCategoryOption_CategoryOption");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserCategoryOption)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_UserCategoryOption_User");
            });

            modelBuilder.Entity<UserArmy>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserArmy)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_UserArmy_User");
            });

            modelBuilder.Entity<VisitArmy>(entity =>
            {
                entity.HasIndex(e => e.VisitId);

                entity.HasOne(d => d.Visit)
                    .WithMany(p => p.VisitArmy)
                    .HasForeignKey(d => d.VisitId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_VisitArmy_Visit");
            });

            // Canteen

            modelBuilder.Entity<MealType>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.MealType)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_MealType_company");
            });

            modelBuilder.Entity<UserDiscount>(entity =>
            {
                entity.HasIndex(e => e.UserId);
                entity.Property(e => e.Amount);
                entity.HasOne(d => d.User)
                   .WithMany(p => p.CornerSetting)
                   .HasForeignKey(d => d.UserId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_CornerSetting_Company");
            });

            modelBuilder.Entity<CornerSetting>(entity =>
            {
                entity.HasIndex(e => e.CompanyId);

                entity.Property(e => e.Name).HasColumnType("character varying");

                entity.Property(e => e.Code).HasColumnType("character varying");

                entity.Property(e => e.Description).HasColumnType("character varying");

                entity.HasOne(d => d.Company)
                   .WithMany(p => p.CornerSetting)
                   .HasForeignKey(d => d.CompanyId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_CornerSetting_Company");
            });

            modelBuilder.Entity<MealSetting>(entity =>
            {
                entity.HasIndex(e => e.IcuDeviceId);
                entity.HasIndex(e => e.CornerId);
                entity.HasIndex(e => e.MealTypeId);
                entity.Property(e => e.Start);
                entity.Property(e => e.End);

                entity.HasOne(d => d.IcuDevice)
                   .WithMany(p => p.MealSetting)
                   .HasForeignKey(d => d.IcuDeviceId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_MealSetting_IcuDevice");

                entity.HasOne(d => d.CornerSetting)
                   .WithMany(p => p.MealSetting)
                   .HasForeignKey(d => d.CornerId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_MealSetting_CornerSetting");

                entity.HasOne(d => d.MealType)
                   .WithMany(p => p.MealSetting)
                   .HasForeignKey(d => d.MealTypeId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_MealSetting_MealType");
            });

            modelBuilder.Entity<MealEventLog>(entity =>
            {
                entity.HasIndex(e => e.EventLogId);

                entity.HasOne(d => d.EventLog)
                    .WithMany(p => p.MealEventLog)
                    .HasForeignKey(d => d.EventLogId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_MealEventLog_EventLog");
            });

            modelBuilder.Entity<ExceptionalMeal>(entity =>
            {
                entity.HasIndex(e => e.Id);
                entity.HasIndex(e => e.Start);
                entity.HasIndex(e => e.End);
                entity.Property(e => e.Price);

                entity.HasOne(d => d.MealSetting)
                   .WithMany(p => p.ExceptionalMeal)
                   .HasForeignKey(d => d.MealSettingId)
                   .OnDelete(DeleteBehavior.Cascade)
                   .HasConstraintName("FK_ExceptionalMeal_MealSetting");
            });

        }
    }
}