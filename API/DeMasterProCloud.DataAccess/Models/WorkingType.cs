using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DeMasterProCloud.DataAccess.Models
{
    public class WorkingType
    {
        public WorkingType()
        {
            IsDefault = false;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public int CompanyId { get; set; }
        
        [Column(TypeName = "jsonb")]
        public string WorkingDay { get; set; }
        
        public bool IsDefault { get; set; }
        
        public virtual Company Company { get; set; }
        
        public ICollection<User> User { get; set; }
    }

}