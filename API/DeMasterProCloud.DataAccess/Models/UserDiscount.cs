﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class UserDiscount
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public User User { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
    }
}
