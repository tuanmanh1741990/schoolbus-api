﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class MealSetting
    {
        public int Id { get; set; }
        public decimal Start { get; set; }
        public decimal End { get; set; }
        public decimal Price { get; set; }
        public IcuDevice IcuDevice { get; set; }
        [ForeignKey("IcuDevice")]
        public int IcuDeviceId { get; set; }
        public CornerSetting CornerSetting { get; set; }
        [ForeignKey("CornerSetting")]
        public int CornerId { get; set; }
        public MealType MealType { get; set; }
        [ForeignKey("MealType")]
        public int MealTypeId { get; set; }

        public ICollection<ExceptionalMeal> ExceptionalMeal { get; set; }
    }
}
