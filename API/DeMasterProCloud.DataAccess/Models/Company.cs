﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace DeMasterProCloud.DataAccess.Models
{
    public class Company
    {
        public Company()
        {
            Account = new HashSet<Account>();
            AccessGroup = new HashSet<AccessGroup>();
            Department = new HashSet<Department>();
            EventLog = new HashSet<EventLog>();
            Holiday = new HashSet<Holiday>();
            IcuDevice = new HashSet<IcuDevice>();
            SystemLog = new HashSet<SystemLog>();
            Timezone = new HashSet<Timezone>();
            User = new HashSet<User>();
            Building = new List<Building>();
            DeviceMessage = new HashSet<DeviceMessage>();
            Visit = new HashSet<Visit>();
            Card = new HashSet<Card>();
            PartTime = new HashSet<PartTime>();

            Category = new HashSet<Category>();

            MealType = new HashSet<MealType>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Contact { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ExpiredFrom { get; set; }
        public DateTime ExpiredTo { get; set; }
        public byte[] Logo { get; set; }
        public byte[] MiniLogo { get; set; }
        public string Name { get; set; } = "";
        public string Remarks { get; set; }
        public bool RootFlag { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public bool IsDeleted { get; set; }

        public string SecretCode { get; set; }
        
        [CanBeNull] public string WebsiteUrl { get; set; }
        [CanBeNull] public string ContactWEmail { get; set; }
        [CanBeNull] public string Phone { get; set; }
        [CanBeNull] public string Industries { get; set; }
        [CanBeNull] public string Location { get; set; }

        public ICollection<Account> Account { get; set; }
        public ICollection<AccessGroup> AccessGroup { get; set; }
        public ICollection<Department> Department { get; set; }
        public ICollection<EventLog> EventLog { get; set; }
        public ICollection<Holiday> Holiday { get; set; }
        public ICollection<IcuDevice> IcuDevice { get; set; }
        public ICollection<SystemLog> SystemLog { get; set; }
        public ICollection<Timezone> Timezone { get; set; }
        public ICollection<User> User { get; set; }
        public ICollection<UnregistedDevice> UnregistedDevice { get; set; }
        public ICollection<Building> Building { get; set; }
        public ICollection<DeviceMessage> DeviceMessage { get; set; }
        public ICollection<Visit> Visit { get; set; }
        public ICollection<Card> Card { get; set; }
        public ICollection<PartTime> PartTime { get; set; }
        
        public ICollection<WorkingType> WorkingType { get; set; }
        
        public ICollection<PlugIn> PlugIn { get; set; }
        
        public VisitSetting VisitSetting { get; set; }
        public ICollection<VisitHistory> VisitHistory { get; set; }
        public DateTime UpdatedAttendanceOn { get; set; }

        public ICollection<Category> Category { get; set; }

        public ICollection<MealType> MealType { get; set; }
        public ICollection<CornerSetting> CornerSetting { get; set; }


    }
}
