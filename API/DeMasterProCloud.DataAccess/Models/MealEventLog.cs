﻿using System;
using System.Collections.Generic;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class MealEventLog
    {
        public MealEventLog()
        {
        }

        public int Id { get; set; }
        public int EventLogId { get; set; }
        public string MealType { get; set; }
        public int MealCode { get; set; }
        public string Price { get; set; }

        public bool IsManual { get; set; }
        public int CornerId { get; set; }
        public EventLog EventLog { get; set; }
    }
}
