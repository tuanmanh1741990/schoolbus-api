﻿using System;
using System.Collections.Generic;

namespace DeMasterProCloud.DataAccess.Models
{
    public partial class UserArmy
    {
        public UserArmy()
        {
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public string MilitaryNumber { get; set; }
        public string BackGroundCheckNumber { get; set; }
        public DateTime? EnlistmentDate { get; set; }
        public string SecretIssuanceBasis { get; set; }
        public short WorkType { get; set; }

        public User User { get; set; }
    }
}
