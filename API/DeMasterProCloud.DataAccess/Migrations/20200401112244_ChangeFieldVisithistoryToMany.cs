﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class ChangeFieldVisithistoryToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_VisitHistory_CompanyId",
                table: "VisitHistory");

            migrationBuilder.CreateIndex(
                name: "IX_VisitHistory_CompanyId",
                table: "VisitHistory",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_VisitHistory_CompanyId",
                table: "VisitHistory");

            migrationBuilder.CreateIndex(
                name: "IX_VisitHistory_CompanyId",
                table: "VisitHistory",
                column: "CompanyId",
                unique: true);
        }
    }
}
