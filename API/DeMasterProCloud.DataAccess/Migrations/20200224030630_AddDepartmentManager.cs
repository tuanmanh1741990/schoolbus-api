﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddDepartmentManager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartmentManager",
                table: "Department",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartmentManager",
                table: "Department");
        }
    }
}
