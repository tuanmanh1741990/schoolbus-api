﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class CreateForeinkeyForMealSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MealSetting_MealType",
                table: "MealSetting"
                );
            migrationBuilder.AddForeignKey(

                name: "FK_MealSetting_MealType",
                table: "MealSetting",
                column: "MealTypeId",
                principalTable: "MealType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade
                );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           
            migrationBuilder.DropForeignKey(
                name: "FK_MealSetting_MealType",
                table: "MealSetting"
                );
        }
    }
}
