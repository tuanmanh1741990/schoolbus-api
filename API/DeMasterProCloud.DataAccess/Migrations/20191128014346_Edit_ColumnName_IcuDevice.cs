﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class Edit_ColumnName_IcuDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TapRange",
                table: "IcuDevice",
                newName: "MPRInterval");

            migrationBuilder.RenameColumn(
                name: "Role_Reader_1",
                table: "IcuDevice",
                newName: "RoleReader1");

            migrationBuilder.RenameColumn(
                name: "Role_Reader_0",
                table: "IcuDevice",
                newName: "RoleReader0");

            migrationBuilder.RenameColumn(
                name: "Led_Reader_1",
                table: "IcuDevice",
                newName: "LedReader1");

            migrationBuilder.RenameColumn(
                name: "Led_Reader_0",
                table: "IcuDevice",
                newName: "LedReader0");

            migrationBuilder.RenameColumn(
                name: "Condition",
                table: "IcuDevice",
                newName: "MPRCount");

            migrationBuilder.RenameColumn(
                name: "Buzzer_Reader_1",
                table: "IcuDevice",
                newName: "BuzzerReader1");

            migrationBuilder.RenameColumn(
                name: "Buzzer_Reader_0",
                table: "IcuDevice",
                newName: "BuzzerReader0");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "RoleReader1",
                table: "IcuDevice",
                newName: "Role_Reader_1");

            migrationBuilder.RenameColumn(
                name: "RoleReader0",
                table: "IcuDevice",
                newName: "Role_Reader_0");

            migrationBuilder.RenameColumn(
                name: "MPRInterval",
                table: "IcuDevice",
                newName: "TapRange");

            migrationBuilder.RenameColumn(
                name: "MPRCount",
                table: "IcuDevice",
                newName: "Condition");

            migrationBuilder.RenameColumn(
                name: "LedReader1",
                table: "IcuDevice",
                newName: "Led_Reader_1");

            migrationBuilder.RenameColumn(
                name: "LedReader0",
                table: "IcuDevice",
                newName: "Led_Reader_0");

            migrationBuilder.RenameColumn(
                name: "BuzzerReader1",
                table: "IcuDevice",
                newName: "Buzzer_Reader_1");

            migrationBuilder.RenameColumn(
                name: "BuzzerReader0",
                table: "IcuDevice",
                newName: "Buzzer_Reader_0");
        }
    }
}
