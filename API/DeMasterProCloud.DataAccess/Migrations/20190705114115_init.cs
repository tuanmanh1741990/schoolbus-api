﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Company",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Code = table.Column<string>(maxLength: 7, nullable: false),
                    Contact = table.Column<string>(maxLength: 100, nullable: true),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ExpiredFrom = table.Column<DateTime>(nullable: false),
                    ExpiredTo = table.Column<DateTime>(nullable: false),
                    Logo = table.Column<byte[]>(nullable: true),
                    MiniLogo = table.Column<byte[]>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Remarks = table.Column<string>(type: "character varying", nullable: true),
                    RootFlag = table.Column<bool>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Company", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Setting",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Key = table.Column<string>(maxLength: 50, nullable: false),
                    Value = table.Column<string>(type: "character varying", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Setting", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccessGroup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    IsDefault = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Type = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessGroup", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccessGroup_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Password = table.Column<string>(maxLength: 256, nullable: false),
                    RootFlag = table.Column<bool>(nullable: false),
                    Status = table.Column<short>(nullable: false),
                    Type = table.Column<short>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    Username = table.Column<string>(maxLength: 50, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Account_Company1",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Building",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Building", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Building_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Department",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DepartName = table.Column<string>(maxLength: 100, nullable: false),
                    DepartNo = table.Column<string>(type: "character varying", nullable: false),
                    ParentId = table.Column<int>(nullable: true),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Department", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Department_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Department_Department",
                        column: x => x.ParentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DeviceMessage",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MessageId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(maxLength: 255, nullable: true),
                    Remark = table.Column<string>(maxLength: 255, nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeviceMessage", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DeviceMessage_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Holiday",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Recursive = table.Column<bool>(nullable: false),
                    Remarks = table.Column<string>(type: "character varying", nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Holiday", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Holiday_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MessageLog",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MsgId = table.Column<string>(maxLength: 100, nullable: false),
                    Topic = table.Column<string>(maxLength: 100, nullable: false),
                    Type = table.Column<string>(maxLength: 100, nullable: false),
                    PayLoad = table.Column<string>(nullable: false),
                    Status = table.Column<short>(nullable: false),
                    GroupMsgId = table.Column<string>(nullable: true),
                    ProcessId = table.Column<string>(nullable: true),
                    ProgressIndex = table.Column<int>(nullable: false),
                    PublishedTime = table.Column<DateTime>(nullable: true),
                    ResponseTime = table.Column<DateTime>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    IsStopped = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MessageLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MessageLog_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Timezone",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CompanyId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Position = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    FriTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    FriTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    FriTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    FriTime4 = table.Column<string>(nullable: true),
                    HolType1Time1 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType1Time2 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType1Time3 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType1Time4 = table.Column<string>(nullable: true),
                    HolType2Time1 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType2Time2 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType2Time3 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType2Time4 = table.Column<string>(nullable: true),
                    HolType3Time1 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType3Time2 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType3Time3 = table.Column<string>(maxLength: 50, nullable: true),
                    HolType3Time4 = table.Column<string>(nullable: true),
                    MonTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    MonTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    MonTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    MonTime4 = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(type: "character varying", nullable: true),
                    SatTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    SatTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    SatTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    SatTime4 = table.Column<string>(nullable: true),
                    SunTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    SunTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    SunTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    SunTime4 = table.Column<string>(nullable: true),
                    ThurTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    ThurTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    ThurTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    ThurTime4 = table.Column<string>(nullable: true),
                    TueTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    TueTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    TueTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    TueTime4 = table.Column<string>(nullable: true),
                    WedTime1 = table.Column<string>(maxLength: 50, nullable: true),
                    WedTime2 = table.Column<string>(maxLength: 50, nullable: true),
                    WedTime3 = table.Column<string>(maxLength: 50, nullable: true),
                    WedTime4 = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Timezone", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Timezone_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UnregistedDevice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DeviceAddress = table.Column<string>(maxLength: 6, nullable: false),
                    Status = table.Column<short>(nullable: false),
                    IpAddress = table.Column<string>(maxLength: 50, nullable: false),
                    DeviceType = table.Column<string>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnregistedDevice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UnregistedDevice_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Visit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    ApplyDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<short>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    VisitorName = table.Column<string>(nullable: true),
                    VisitType = table.Column<string>(nullable: true),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    VisitorDepartment = table.Column<string>(nullable: true),
                    VisitorEmpNumber = table.Column<string>(nullable: true),
                    Position = table.Column<string>(maxLength: 100, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    VisiteeSite = table.Column<string>(nullable: true),
                    VisitReason = table.Column<string>(nullable: true),
                    VisiteeName = table.Column<string>(nullable: true),
                    VisiteeDepartment = table.Column<string>(nullable: true),
                    VisiteeEmpNumber = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true),
                    Address = table.Column<string>(nullable: true),
                    IsDecision = table.Column<bool>(nullable: false),
                    VisitingCardState = table.Column<short>(nullable: false),
                    ApproverId1 = table.Column<int>(nullable: false),
                    ApproverId2 = table.Column<int>(nullable: false),
                    CardId = table.Column<string>(maxLength: 50, nullable: true),
                    IssueCount = table.Column<int>(nullable: false),
                    CardStatus = table.Column<short>(nullable: false),
                    AccessGroupId = table.Column<int>(nullable: false),
                    RejectReason = table.Column<string>(nullable: true),
                    ApprovDate1 = table.Column<DateTime>(nullable: false),
                    ApprovDate2 = table.Column<DateTime>(nullable: false),
                    RejectorId = table.Column<string>(nullable: true),
                    RejectDate = table.Column<DateTime>(nullable: false),
                    UserCode = table.Column<string>(nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: false),
                    ReturnDate = table.Column<DateTime>(nullable: false),
                    IssuerId = table.Column<int>(nullable: false),
                    ReclaimerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Visit_AccessGroup",
                        column: x => x.AccessGroupId,
                        principalTable: "AccessGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Visit_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SystemLog",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Action = table.Column<int>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    Content = table.Column<string>(type: "character varying", nullable: true),
                    ContentDetails = table.Column<string>(type: "character varying", nullable: true),
                    ContentIds = table.Column<string>(type: "character varying", nullable: true),
                    CreatedBy = table.Column<int>(nullable: false),
                    OpeTime = table.Column<DateTime>(nullable: false),
                    Type = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SystemLog_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SystemLog_Account",
                        column: x => x.CreatedBy,
                        principalTable: "Account",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Address = table.Column<string>(maxLength: 100, nullable: true),
                    Avatar = table.Column<byte[]>(nullable: true),
                    UserCode = table.Column<string>(nullable: true),
                    City = table.Column<string>(maxLength: 100, nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    EffectiveDate = table.Column<DateTime>(nullable: true),
                    ExpiredDate = table.Column<DateTime>(nullable: true),
                    EmpNumber = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 100, nullable: true),
                    HomePhone = table.Column<string>(maxLength: 20, nullable: true),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    Job = table.Column<string>(maxLength: 100, nullable: true),
                    KeyPadPw = table.Column<string>(maxLength: 256, nullable: true),
                    LastName = table.Column<string>(maxLength: 100, nullable: true),
                    Nationality = table.Column<string>(maxLength: 100, nullable: true),
                    OfficePhone = table.Column<string>(maxLength: 20, nullable: true),
                    Position = table.Column<string>(maxLength: 100, nullable: true),
                    PostCode = table.Column<string>(maxLength: 20, nullable: true),
                    Remarks = table.Column<string>(type: "character varying", nullable: true),
                    Responsibility = table.Column<string>(maxLength: 100, nullable: true),
                    Rfu = table.Column<string>(nullable: true),
                    Sex = table.Column<bool>(nullable: false),
                    AccessGroupId = table.Column<int>(nullable: false),
                    IsMasterCard = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    PassType = table.Column<short>(nullable: false),
                    WorkType = table.Column<short>(nullable: false),
                    IsSystemUseApply = table.Column<bool>(nullable: false),
                    SystemUseApplyReason = table.Column<string>(nullable: true),
                    SystemUsePassword = table.Column<string>(nullable: true),
                    IsSystemUseApproval = table.Column<bool>(nullable: false),
                    SystemAuth = table.Column<string>(nullable: true),
                    IsAccountLock = table.Column<bool>(nullable: false),
                    SystemUseApplyDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<short>(nullable: false),
                    BirthDay = table.Column<DateTime>(nullable: false),
                    Grade = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                    table.ForeignKey(
                        name: "FK_User_AccessGroup",
                        column: x => x.AccessGroupId,
                        principalTable: "AccessGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User_Company1",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_User_Department",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "IcuDevice",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Buzzer = table.Column<bool>(nullable: false),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DeviceAddress = table.Column<string>(maxLength: 6, nullable: false),
                    HardAntiFlag = table.Column<bool>(nullable: false),
                    IpAddress = table.Column<string>(type: "character varying", nullable: true),
                    LastSyncTime = table.Column<DateTime>(nullable: true),
                    MacAddress = table.Column<string>(maxLength: 20, nullable: true),
                    NetworkEnableFlag = table.Column<bool>(nullable: false),
                    PassbackRule = table.Column<short>(nullable: false),
                    Status = table.Column<short>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    ConnectionStatus = table.Column<short>(nullable: false),
                    FirmwareVersion = table.Column<string>(maxLength: 20, nullable: true),
                    InCardReaderVersion = table.Column<string>(maxLength: 20, nullable: true),
                    OutCardReaderVersion = table.Column<string>(maxLength: 20, nullable: true),
                    NfcModuleVersion = table.Column<string>(maxLength: 20, nullable: true),
                    ExtraVersion = table.Column<string>(maxLength: 20, nullable: true),
                    RegisterIdNumber = table.Column<int>(nullable: false),
                    EventCount = table.Column<int>(nullable: false),
                    LastCommunicationTime = table.Column<DateTime>(nullable: false),
                    NumberOfNotTransmittingEvent = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Condition = table.Column<int>(nullable: false),
                    TapRange = table.Column<int>(nullable: false),
                    DeviceType = table.Column<short>(nullable: false),
                    BackupPeriod = table.Column<int>(nullable: false),
                    BuildingId = table.Column<int>(nullable: true),
                    ActiveTzId = table.Column<int>(nullable: false),
                    CloseReverseLockFlag = table.Column<bool>(nullable: false),
                    DoorStatusDelay = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    OpenDuration = table.Column<int>(nullable: true),
                    PassageTzId = table.Column<int>(nullable: true),
                    SensorType = table.Column<short>(nullable: false),
                    LedIn = table.Column<short>(nullable: false),
                    LedOut = table.Column<short>(nullable: false),
                    VerifyMode = table.Column<short>(nullable: false),
                    ServerPort = table.Column<int>(maxLength: 8, nullable: false),
                    ServerIp = table.Column<string>(maxLength: 20, nullable: true),
                    OperationType = table.Column<short>(nullable: false),
                    DoorStatus = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IcuDevice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IcuDevice_Timezone1",
                        column: x => x.ActiveTzId,
                        principalTable: "Timezone",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IcuDevice_Building",
                        column: x => x.BuildingId,
                        principalTable: "Building",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IcuDevice_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IcuDevice_Timezone",
                        column: x => x.PassageTzId,
                        principalTable: "Timezone",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Card",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    IssuedDate = table.Column<DateTime>(nullable: true),
                    CompanyId = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    EffectiveDate = table.Column<DateTime>(nullable: false),
                    CardType = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    VisitId = table.Column<int>(nullable: true),
                    CardName = table.Column<string>(nullable: true),
                    CardId = table.Column<string>(maxLength: 50, nullable: true),
                    IssueCount = table.Column<int>(nullable: false),
                    CardStatus = table.Column<short>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    AccessGroupId = table.Column<int>(nullable: false),
                    IsMasterCard = table.Column<bool>(nullable: false),
                    Status = table.Column<short>(nullable: false),
                    Etc = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Card", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Card_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Card_User",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Card_Visit",
                        column: x => x.VisitId,
                        principalTable: "Visit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccessGroupDevice",
                columns: table => new
                {
                    AccessGroupId = table.Column<int>(nullable: false),
                    IcuId = table.Column<int>(nullable: false),
                    TzId = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedBy = table.Column<int>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccessGroupDevice", x => new { x.AccessGroupId, x.IcuId });
                    table.ForeignKey(
                        name: "FK_AccessGroupDevice_User",
                        column: x => x.AccessGroupId,
                        principalTable: "AccessGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccessGroupDevice_IcuDevice",
                        column: x => x.IcuId,
                        principalTable: "IcuDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccessGroupDevice_Timezone",
                        column: x => x.TzId,
                        principalTable: "Timezone",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EventLog",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Antipass = table.Column<string>(maxLength: 10, nullable: true),
                    CardId = table.Column<string>(maxLength: 50, nullable: true),
                    IssueCount = table.Column<int>(nullable: false),
                    CardType = table.Column<short>(nullable: false),
                    CompanyId = table.Column<int>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    DeptId = table.Column<int>(nullable: true),
                    DoorName = table.Column<string>(maxLength: 100, nullable: false),
                    EventTime = table.Column<DateTime>(nullable: true),
                    EventType = table.Column<int>(nullable: false),
                    IcuId = table.Column<int>(nullable: false),
                    Index = table.Column<long>(nullable: false),
                    KeyPadPw = table.Column<string>(maxLength: 256, nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    IsVisit = table.Column<bool>(nullable: false),
                    VisitId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EventLog_Company",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EventLog_IcuDevice",
                        column: x => x.IcuId,
                        principalTable: "IcuDevice",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventLog_User",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EventLog_Visit",
                        column: x => x.VisitId,
                        principalTable: "Visit",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccessGroup_CompanyId",
                table: "AccessGroup",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_AccessGroupDevice_AccessGroupId",
                table: "AccessGroupDevice",
                column: "AccessGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_AccessGroupDevice_IcuId",
                table: "AccessGroupDevice",
                column: "IcuId");

            migrationBuilder.CreateIndex(
                name: "IX_AccessGroupDevice_TzId",
                table: "AccessGroupDevice",
                column: "TzId");

            migrationBuilder.CreateIndex(
                name: "IX_Account_CompanyId",
                table: "Account",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Building_CompanyId",
                table: "Building",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Card_CompanyId",
                table: "Card",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Card_UserId",
                table: "Card",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Card_VisitId",
                table: "Card",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Department_CompanyId",
                table: "Department",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Department_ParentId",
                table: "Department",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_DeviceMessage_CompanyId",
                table: "DeviceMessage",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EventLog_CompanyId",
                table: "EventLog",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_EventLog_IcuId",
                table: "EventLog",
                column: "IcuId");

            migrationBuilder.CreateIndex(
                name: "IX_EventLog_UserId",
                table: "EventLog",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_EventLog_VisitId",
                table: "EventLog",
                column: "VisitId");

            migrationBuilder.CreateIndex(
                name: "IX_Holiday_CompanyId",
                table: "Holiday",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_IcuDevice_ActiveTzId",
                table: "IcuDevice",
                column: "ActiveTzId");

            migrationBuilder.CreateIndex(
                name: "IX_IcuDevice_BuildingId",
                table: "IcuDevice",
                column: "BuildingId");

            migrationBuilder.CreateIndex(
                name: "IX_IcuDevice_CompanyId",
                table: "IcuDevice",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_IcuDevice_PassageTzId",
                table: "IcuDevice",
                column: "PassageTzId");

            migrationBuilder.CreateIndex(
                name: "IX_MessageLog_CompanyId",
                table: "MessageLog",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_MessageLog_Id",
                table: "MessageLog",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_SystemLog_CompanyId",
                table: "SystemLog",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_SystemLog_CreatedBy",
                table: "SystemLog",
                column: "CreatedBy");

            migrationBuilder.CreateIndex(
                name: "IX_Timezone_CompanyId",
                table: "Timezone",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_UnregistedDevice_CompanyId",
                table: "UnregistedDevice",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_UnregistedDevice_Id",
                table: "UnregistedDevice",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_User_AccessGroupId",
                table: "User",
                column: "AccessGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_User_CompanyId",
                table: "User",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_User_DepartmentId",
                table: "User",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Visit_AccessGroupId",
                table: "Visit",
                column: "AccessGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Visit_CompanyId",
                table: "Visit",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccessGroupDevice");

            migrationBuilder.DropTable(
                name: "Card");

            migrationBuilder.DropTable(
                name: "DeviceMessage");

            migrationBuilder.DropTable(
                name: "EventLog");

            migrationBuilder.DropTable(
                name: "Holiday");

            migrationBuilder.DropTable(
                name: "MessageLog");

            migrationBuilder.DropTable(
                name: "Setting");

            migrationBuilder.DropTable(
                name: "SystemLog");

            migrationBuilder.DropTable(
                name: "UnregistedDevice");

            migrationBuilder.DropTable(
                name: "IcuDevice");

            migrationBuilder.DropTable(
                name: "User");

            migrationBuilder.DropTable(
                name: "Visit");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Timezone");

            migrationBuilder.DropTable(
                name: "Building");

            migrationBuilder.DropTable(
                name: "Department");

            migrationBuilder.DropTable(
                name: "AccessGroup");

            migrationBuilder.DropTable(
                name: "Company");
        }
    }
}
