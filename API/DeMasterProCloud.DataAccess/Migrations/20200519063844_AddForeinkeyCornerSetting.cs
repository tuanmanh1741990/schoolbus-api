﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddForeinkeyCornerSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddForeignKey(
                name: "FK_CornerSetting_Company",
                table: "CornerSetting",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
               name: "FK_UserDiscount_User",
               table: "UserDiscount",
               column: "UserId",
               principalTable: "User",
               principalColumn: "Id",
               onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CornerSetting_Company",
                table: "CornerSetting");

            migrationBuilder.DropForeignKey(
                name: "FK_UserDiscount_User",
                table: "UserDiscount");
        }
    }
}
