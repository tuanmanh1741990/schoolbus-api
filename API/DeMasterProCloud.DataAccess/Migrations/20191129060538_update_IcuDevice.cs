﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class update_IcuDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CardReaderVersion_1",
                table: "IcuDevice",
                newName: "VersionReader1");

            migrationBuilder.RenameColumn(
                name: "CardReaderVersion_0",
                table: "IcuDevice",
                newName: "VersionReader0");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VersionReader1",
                table: "IcuDevice",
                newName: "CardReaderVersion_1");

            migrationBuilder.RenameColumn(
                name: "VersionReader0",
                table: "IcuDevice",
                newName: "CardReaderVersion_0");
        }
    }
}
