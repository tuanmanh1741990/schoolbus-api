﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddTableExceptionalMeal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
              name: "ExceptionalMeal",
              columns: table => new
              {
                  Id = table.Column<int>(nullable: false)
                      .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                  MealSettingId = table.Column<int>(nullable: false),
                  Start = table.Column<DateTime>(nullable: false),
                  End = table.Column<DateTime>(nullable: false),
                  Price = table.Column<decimal>(nullable: false)
              },
              constraints: table =>
              {
                  table.PrimaryKey("PK_ExceptionalMeal", x => x.Id);

                  table.ForeignKey(
                      name: "FK_ExeptionalMeal_MealSetting",
                      column: x => x.MealSettingId,
                      principalTable: "MealSetting",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
              });

            migrationBuilder.CreateIndex(
               name: "IX_ExceptionalMeal_Id",
               table: "ExceptionalMeal",
               column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ExceptionalMeal_Start",
                table: "ExceptionalMeal",
                column: "Start");
            migrationBuilder.CreateIndex(
               name: "IX_ExceptionalMeal_End",
               table: "ExceptionalMeal",
               column: "End");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
              name: "ExceptionalMeal");
        }
    }
}
