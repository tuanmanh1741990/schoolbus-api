﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddTableMealSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "MealSetting",
               columns: table => new
               {
                   Id = table.Column<int>(nullable: false)
                       .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                   IcuDeviceId = table.Column<int>(nullable: false),
                   CornerId = table.Column<int>(nullable: false),
                   MealTypeId = table.Column<int>(nullable: false),
                   Start = table.Column<decimal>(nullable: false),
                   End = table.Column<decimal>(nullable: false),
                   Price = table.Column<decimal>(nullable: false)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_MealSetting", x => x.Id);

                   table.ForeignKey(
                       name: "FK_MealSetting_IcuDevice",
                       column: x => x.IcuDeviceId,
                       principalTable: "IcuDevice",
                       principalColumn: "Id",
                       onDelete: ReferentialAction.Cascade);

                   table.ForeignKey(
                       name: "FK_MealSetting_CornerSetting",
                       column: x => x.CornerId,
                       principalTable: "CornerSetting",
                       principalColumn: "Id",
                       onDelete: ReferentialAction.Cascade);

                   table.ForeignKey(
                       name: "FK_MealSetting_MealType",
                       column: x => x.MealTypeId,
                       principalTable: "MealSetting",
                       principalColumn: "Id",
                       onDelete: ReferentialAction.Cascade);
               });
            
            migrationBuilder.CreateIndex(
                name: "IX_MealSetting_CornerId",
                table: "MealSetting",
                column: "CornerId");

            migrationBuilder.CreateIndex(
                name: "IX_MealSetting_MealTypeId",
                table: "MealSetting",
                column: "MealTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_MealSetting_IcuDeviceId",
                table: "MealSetting",
                column: "IcuDeviceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
               name: "MealSetting");
        }
    }
}
