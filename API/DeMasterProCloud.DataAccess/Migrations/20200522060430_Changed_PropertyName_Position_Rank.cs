﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class Changed_PropertyName_Position_Rank : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "VisitorPosition",
                table: "VisitArmy",
                newName: "VisitorRank");

            migrationBuilder.RenameColumn(
                name: "VisiteePosition",
                table: "VisitArmy",
                newName: "VisiteeRank");

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.RenameColumn(
                name: "VisitorRank",
                table: "VisitArmy",
                newName: "VisitorPosition");

            migrationBuilder.RenameColumn(
                name: "VisiteeRank",
                table: "VisitArmy",
                newName: "VisiteePosition");
        }
    }
}
