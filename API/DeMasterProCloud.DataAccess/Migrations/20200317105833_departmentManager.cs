﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class departmentManager : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DepartmentManager",
                table: "Department",
                newName: "DepartmentManagerId");

            migrationBuilder.AlterColumn<string>(
                name: "ServerIp",
                table: "IcuDevice",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Department_DepartmentManagerId",
                table: "Department",
                column: "DepartmentManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Department_Account",
                table: "Department",
                column: "DepartmentManagerId",
                principalTable: "Account",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Department_Account",
                table: "Department");

            migrationBuilder.DropIndex(
                name: "IX_Department_DepartmentManagerId",
                table: "Department");

            migrationBuilder.RenameColumn(
                name: "DepartmentManagerId",
                table: "Department",
                newName: "DepartmentManager");

            migrationBuilder.AlterColumn<string>(
                name: "ServerIp",
                table: "IcuDevice",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);
        }
    }
}
