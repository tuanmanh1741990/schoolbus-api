﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class clearUp_IcuDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<short>(
                name: "RoleReader1",
                table: "IcuDevice",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<short>(
                name: "RoleReader0",
                table: "IcuDevice",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<short>(
                name: "LedReader1",
                table: "IcuDevice",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AlterColumn<short>(
                name: "LedReader0",
                table: "IcuDevice",
                nullable: true,
                oldClrType: typeof(short));

            migrationBuilder.AddColumn<short>(
                name: "BuzzerReader0",
                table: "IcuDevice",
                nullable: true);

            migrationBuilder.AddColumn<short>(
                name: "BuzzerReader1",
                table: "IcuDevice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuzzerReader0",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "BuzzerReader1",
                table: "IcuDevice");

            migrationBuilder.AlterColumn<short>(
                name: "RoleReader1",
                table: "IcuDevice",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "RoleReader0",
                table: "IcuDevice",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "LedReader1",
                table: "IcuDevice",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);

            migrationBuilder.AlterColumn<short>(
                name: "LedReader0",
                table: "IcuDevice",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);
        }
    }
}
