﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class temp_for_change_Property : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuzzerReader0",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "BuzzerReader1",
                table: "IcuDevice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "BuzzerReader0",
                table: "IcuDevice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "BuzzerReader1",
                table: "IcuDevice",
                nullable: false,
                defaultValue: false);
        }
    }
}
