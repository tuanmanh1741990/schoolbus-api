﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class Add_Properties_To_VisitArmy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BackGroundCheckNumber",
                table: "VisitArmy",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "VisitArmy",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "VisiteePosition",
                table: "VisitArmy",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BackGroundCheckNumber",
                table: "VisitArmy");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "VisitArmy");

            migrationBuilder.DropColumn(
                name: "VisiteePosition",
                table: "VisitArmy");
        }
    }
}
