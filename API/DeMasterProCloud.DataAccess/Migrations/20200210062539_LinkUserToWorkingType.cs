﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class LinkUserToWorkingType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "WorkingTypeId",
                table: "User",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_WorkingTypeId",
                table: "User",
                column: "WorkingTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_User_WorkingType",
                table: "User",
                column: "WorkingTypeId",
                principalTable: "WorkingType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_User_WorkingType",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_User_WorkingTypeId",
                table: "User");

            migrationBuilder.DropColumn(
                name: "WorkingTypeId",
                table: "User");
        }
    }
}
