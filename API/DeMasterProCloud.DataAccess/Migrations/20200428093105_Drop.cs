﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class Drop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                 name: "UpTimeOnlineDevice",
                 table: "IcuDevice",
                 nullable: false,
                 defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                 name: "UpTimeOnlineDevice",
                 table: "IcuDevice");
        }
    }
}
