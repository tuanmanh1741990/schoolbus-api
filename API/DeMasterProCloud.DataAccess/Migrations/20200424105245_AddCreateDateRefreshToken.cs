﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddCreateDateRefreshToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDateRefreshToken",
                table: "Account",
                nullable: true,
                defaultValue: DateTime.Now);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateDateRefreshToken",
                table: "Account");
        }
    }
}
