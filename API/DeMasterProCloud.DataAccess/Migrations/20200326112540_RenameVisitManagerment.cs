﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class RenameVisitManagerment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AccountSecondsApprove",
                table: "VisitSetting",
                newName: "SecondsApproverAccounts");

            migrationBuilder.RenameColumn(
                name: "AccountFirstApprove",
                table: "VisitSetting",
                newName: "FirstApproverAccounts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SecondsApproverAccounts",
                table: "VisitSetting",
                newName: "AccountSecondsApprove");

            migrationBuilder.RenameColumn(
                name: "FirstApproverAccounts",
                table: "VisitSetting",
                newName: "AccountFirstApprove");
        }
    }
}
