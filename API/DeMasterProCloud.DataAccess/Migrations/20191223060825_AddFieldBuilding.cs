﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddFieldBuilding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PostalCode",
                table: "Building",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeZone",
                table: "Building",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "PostalCode",
                table: "Building");

            migrationBuilder.DropColumn(
                name: "TimeZone",
                table: "Building");
        }
    }
}
