﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class UpdateCompanyField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactWEmail",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EnableQr",
                table: "Company",
                nullable: false,
                defaultValue: true);

            migrationBuilder.AddColumn<string>(
                name: "Industries",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebsiteUrl",
                table: "Company",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContactWEmail",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "EnableQr",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Industries",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "WebsiteUrl",
                table: "Company");
        }
    }
}
