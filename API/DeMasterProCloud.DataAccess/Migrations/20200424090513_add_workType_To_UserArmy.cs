﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class add_workType_To_UserArmy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "WorkType",
                table: "UserArmy",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AlterColumn<short>(
                name: "WorkType",
                table: "User",
                nullable: true,
                oldClrType: typeof(short));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WorkType",
                table: "UserArmy");

            migrationBuilder.AlterColumn<short>(
                name: "WorkType",
                table: "User",
                nullable: false,
                oldClrType: typeof(short),
                oldNullable: true);
        }
    }
}
