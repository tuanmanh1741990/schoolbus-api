﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class Change_UserName_To_AccountId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserName",
                table: "MessageLog");

            migrationBuilder.AddColumn<int>(
                name: "AccountId",
                table: "MessageLog",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "MessageLog");

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "MessageLog",
                nullable: true);
        }
    }
}
