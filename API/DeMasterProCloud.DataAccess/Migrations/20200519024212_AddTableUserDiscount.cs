﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddTableUserDiscount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
               name: "UserDiscount",
               columns: table => new
               {
                   Id = table.Column<int>(nullable: false)
                       .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                   UserId = table.Column<int>(nullable: true),
                   Amount = table.Column<decimal>(nullable: true)
               },
               constraints: table =>
               {
                   table.PrimaryKey("PK_UserDiscount", x => x.Id);
               });

            migrationBuilder.CreateIndex(
                name: "IX_UserDiscount_UserId",
                table: "UserDiscount",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDiscount");
        }
    }
}
