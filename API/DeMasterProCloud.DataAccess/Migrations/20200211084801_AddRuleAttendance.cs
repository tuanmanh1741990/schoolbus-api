﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class AddRuleAttendance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "WorkingTypeId",
                table: "User",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAttendanceOn",
                table: "Company",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "Attendance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "WorkingTime",
                table: "Attendance",
                type: "jsonb",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Attendance_CompanyId",
                table: "Attendance",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Attendance_Company_CompanyId",
                table: "Attendance",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Attendance_Company_CompanyId",
                table: "Attendance");

            migrationBuilder.DropIndex(
                name: "IX_Attendance_CompanyId",
                table: "Attendance");

            migrationBuilder.DropColumn(
                name: "UpdatedAttendanceOn",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Attendance");

            migrationBuilder.DropColumn(
                name: "WorkingTime",
                table: "Attendance");

            migrationBuilder.AlterColumn<int>(
                name: "WorkingTypeId",
                table: "User",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
