﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class UpdateTypeAttendance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "Start",
                table: "Attendance",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<double>(
                name: "End",
                table: "Attendance",
                nullable: true,
                oldClrType: typeof(float));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "Start",
                table: "Attendance",
                nullable: true,
                oldClrType: typeof(double));

            migrationBuilder.AlterColumn<float>(
                name: "End",
                table: "Attendance",
                nullable: true,
                oldClrType: typeof(double));
        }
    }
}
