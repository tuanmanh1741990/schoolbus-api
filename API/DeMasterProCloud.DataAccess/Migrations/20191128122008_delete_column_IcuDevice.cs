﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class delete_column_IcuDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HardAntiFlag",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "LastSyncTime",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "NetworkEnableFlag",
                table: "IcuDevice");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HardAntiFlag",
                table: "IcuDevice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastSyncTime",
                table: "IcuDevice",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "NetworkEnableFlag",
                table: "IcuDevice",
                nullable: false,
                defaultValue: false);
        }
    }
}
