﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class add_column_IcuDevice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OutCardReaderVersion",
                table: "IcuDevice",
                newName: "CardReaderVersion_1");

            migrationBuilder.RenameColumn(
                name: "LedOut",
                table: "IcuDevice",
                newName: "Role_Reader_1");

            migrationBuilder.RenameColumn(
                name: "LedIn",
                table: "IcuDevice",
                newName: "Role_Reader_0");

            migrationBuilder.RenameColumn(
                name: "InCardReaderVersion",
                table: "IcuDevice",
                newName: "CardReaderVersion_0");

            migrationBuilder.RenameColumn(
                name: "DoorStatusDelay",
                table: "IcuDevice",
                newName: "UseCardReader");

            migrationBuilder.RenameColumn(
                name: "Buzzer",
                table: "IcuDevice",
                newName: "SensorAlarm");

            migrationBuilder.AddColumn<bool>(
                name: "Buzzer_Reader_0",
                table: "IcuDevice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Buzzer_Reader_1",
                table: "IcuDevice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<short>(
                name: "Led_Reader_0",
                table: "IcuDevice",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "Led_Reader_1",
                table: "IcuDevice",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<int>(
                name: "SensorDuration",
                table: "IcuDevice",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Buzzer_Reader_0",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "Buzzer_Reader_1",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "Led_Reader_0",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "Led_Reader_1",
                table: "IcuDevice");

            migrationBuilder.DropColumn(
                name: "SensorDuration",
                table: "IcuDevice");

            migrationBuilder.RenameColumn(
                name: "UseCardReader",
                table: "IcuDevice",
                newName: "DoorStatusDelay");

            migrationBuilder.RenameColumn(
                name: "SensorAlarm",
                table: "IcuDevice",
                newName: "Buzzer");

            migrationBuilder.RenameColumn(
                name: "Role_Reader_1",
                table: "IcuDevice",
                newName: "LedOut");

            migrationBuilder.RenameColumn(
                name: "Role_Reader_0",
                table: "IcuDevice",
                newName: "LedIn");

            migrationBuilder.RenameColumn(
                name: "CardReaderVersion_1",
                table: "IcuDevice",
                newName: "OutCardReaderVersion");

            migrationBuilder.RenameColumn(
                name: "CardReaderVersion_0",
                table: "IcuDevice",
                newName: "InCardReaderVersion");
        }
    }
}
