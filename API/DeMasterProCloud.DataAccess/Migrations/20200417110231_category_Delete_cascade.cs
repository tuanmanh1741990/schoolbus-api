﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DeMasterProCloud.DataAccess.Migrations
{
    public partial class category_Delete_cascade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryOption_Category",
                table: "CategoryOption");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCategoryOption_CategoryOption",
                table: "UserCategoryOption");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCategoryOption_User",
                table: "UserCategoryOption");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryOption_Category",
                table: "CategoryOption",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCategoryOption_CategoryOption",
                table: "UserCategoryOption",
                column: "CategoryOptionId",
                principalTable: "CategoryOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCategoryOption_User",
                table: "UserCategoryOption",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CategoryOption_Category",
                table: "CategoryOption");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCategoryOption_CategoryOption",
                table: "UserCategoryOption");

            migrationBuilder.DropForeignKey(
                name: "FK_UserCategoryOption_User",
                table: "UserCategoryOption");

            migrationBuilder.AddForeignKey(
                name: "FK_CategoryOption_Category",
                table: "CategoryOption",
                column: "CategoryId",
                principalTable: "Category",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCategoryOption_CategoryOption",
                table: "UserCategoryOption",
                column: "CategoryOptionId",
                principalTable: "CategoryOption",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserCategoryOption_User",
                table: "UserCategoryOption",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
