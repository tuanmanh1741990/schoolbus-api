﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch.Internal;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Department Repository
    /// </summary>
    public interface IDeviceMessageRepository : IGenericRepository<DeviceMessage>
    {
        void AddDefaultDeviceMessage(int companyId = 0);
        IQueryable<DeviceMessage> GetByCompanyId(int? companyId);
    }
    public class DeviceMessageRepository : GenericRepository<DeviceMessage>, IDeviceMessageRepository
    {

        private readonly AppDbContext _dbContext;
        private readonly HttpContext _httpContext;
        public DeviceMessageRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            _httpContext = contextAccessor.HttpContext;
        }
        public IQueryable<DeviceMessage> GetByCompanyId(int? companyId)
        {
            if (companyId != null) return _dbContext.DeviceMessage.Where(x => x.CompanyId == companyId);
            return null;
        }

        /// <summary>
        /// Add default device message
        /// </summary>
        public void AddDefaultDeviceMessage(int companyId)
        {
            var deviceMessages = new List<DeviceMessage>();

            deviceMessages.Add(new DeviceMessage
            {
                //Id = 1,
                MessageId = 1,
                Content = "Welcome!",
                Remark = "Default Message",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 2,
                MessageId = 2,
                Content = "Thank you",
                Remark = "Default Message",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 3,
                MessageId = 3,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 4,
                MessageId = 4,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 5,
                MessageId = 5,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 6,
                MessageId = 6,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 7,
                MessageId = 7,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 8,
                MessageId = 8,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 9,
                MessageId = 9,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });
            deviceMessages.Add(new DeviceMessage
            {
                //Id = 10,
                MessageId = 10,
                Content = "",
                Remark = "",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                CompanyId = companyId,
            });

            foreach (var deviceMessage in deviceMessages)
            {
                Add(deviceMessage);
            }
        }
    }
}
