﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.User;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// User repository interface
    /// </summary>
    public interface ICardRepository : IGenericRepository<Card>
    {

        IQueryable<Card> GetByCompanyId(int companyId);
        Card GetById(int companyId, int id);
        List<Card> GetByIds(int companyId, List<int> ids);

        List<Card> GetByUserId(int companyId, int userId);
        List<Card> GetByVisitId(int companyId, int visitId);

        int GetCountByUserId(int companyId, int userId);

       // List<Card> GetByUserIdIncludeUser(int companyId, int userId);
        bool IsCardIdExist(int companyId, List<CardModel> cardList);
        
        bool IsCardIdByUserIdExist(int companyId, int userId, int cardId);
        bool IsCardIdExist(int companyId, string cardId);
        List<int> GetUserIdHasCards(int companyId);
        List<int> GetVisitIdHasCards(int companyId);

        IQueryable<Card> GetFilteredCards(int companyId, string filter);
        Card GetByCardId(int? companyId, string cardId);
        int GetCardCountByuserIds(int companyId, List<int> userIds);
        int GetCardStatusByUserIdAndCardId(int companyId, int userId,  string cardId);
        bool CheckIsExistedQrCode(string qrId);
        
        Card GetQrCode(int userId);
        Card GetRandomQrCode(int companyId);
        List<Card> GetCardQrByCompanyId(int companyId);

        bool GetQrCodeBelongToUser(int userId);
        
        Card GetPassCode(int userId, int companyId);
    }

    /// <summary>
    /// Card repository
    /// </summary>
    public class CardRepository : GenericRepository<Card>, ICardRepository
    {
        private readonly AppDbContext _dbContext;
        public CardRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get by company ID
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<Card> GetByCompanyId(int companyId)
        {
            return _dbContext.Card
                .Where(m => m.CompanyId == companyId && !m.IsDeleted);
        }

        /// <summary>
        /// Get by companyid and id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Card GetById(int companyId, int id)
        {
            return _dbContext.Card.Where(m =>
                m.Id == id && m.CompanyId == companyId && !m.IsDeleted).FirstOrDefault();
        }


        public List<Card> GetByIds(int companyId, List<int> ids)
        {
            return _dbContext.Card.Where(m =>
                ids.Contains(m.Id) && m.CompanyId == companyId && !m.IsDeleted).ToList();
        }

        /// <summary>
        /// Check whether if card id is existed in the company
        /// </summary>
        /// <param name="model"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool IsCardIdExist(int companyId, List<CardModel> cardList)
        {
            if (!cardList.Any())
            {
                return false;
            }

            return Get(m =>
                       cardList.Select(card => card.CardId).Contains(m.CardId) &&
                       m.CompanyId == companyId && !m.IsDeleted) != null;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userId">index number of user</param>
        /// <param name="cardId">index number of card, not cardId</param>
        /// <returns></returns>
        public bool IsCardIdByUserIdExist(int companyId, int userId, int cardId)
        {
            return Get(m => m.Id == cardId && m.CompanyId == companyId && !m.IsDeleted && m.UserId == userId) !=
                   null;
        }
        
        /// <summary>
        /// Check whether if card id is existed in the company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public bool IsCardIdExist(int companyId, string cardId)
        {
            if (string.IsNullOrEmpty(cardId))
            {
                return false;
            }

            return Get(m => m.CardId == cardId && m.CompanyId == companyId && !m.IsDeleted) != null;
        }

        /// <summary>
        /// Get list of card by user id
        /// </summary>
        /// <param name="companyId"> identifier of company </param>
        /// <param name="userId"> identifier of user </param>
        /// <returns></returns>
        public List<Card> GetByUserId(int companyId, int userId)
        {
            var data = _dbContext.Card.Where(m => m.UserId == userId && !m.IsDeleted);

            if(companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            return data.ToList();
        }

        /// <summary>
        /// Get list of card by visit id
        /// </summary>
        /// <param name="companyId"> identifier of company </param>
        /// <param name="visitId"> identifier of visit </param>
        /// <returns></returns>
        public List<Card> GetByVisitId(int companyId, int visitId)
        {
            var data = _dbContext.Card.Where(m => m.VisitId == visitId && !m.IsDeleted);

            if (companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            return data.ToList();
        }

        public int GetCountByUserId(int companyId, int userId)
        {
            var data = _dbContext.Card.Where(m => m.UserId == userId && m.CompanyId == companyId && !m.IsDeleted);

            if(companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            //return _dbContext.Card.Where(m =>
            //    m.UserId == userId && m.CompanyId == companyId && !m.IsDeleted).Count();

            return data.Count();
        }

        //public List<Card> GetByUserIdIncludeUser(int companyId, int userId)
        //{
        //    return _dbContext.Card.Include(c => c.User).Where(m =>
        //        m.UserId == userId && m.CompanyId == companyId && !m.IsDeleted).ToList();
        //}

        public List<int> GetUserIdHasCards(int companyId)
        {
            return _dbContext.Card.Where(m =>
                m.CompanyId == companyId && !m.IsDeleted && m.UserId != null).Select(c => c.UserId.Value).ToList();
        }

        public List<int> GetVisitIdHasCards(int companyId)
        {
            return _dbContext.Card.Where(m => m.CompanyId == companyId && !m.IsDeleted && m.VisitId != null).Select(c => c.VisitId.Value).ToList();
        }

        public IQueryable<Card> GetFilteredCards(int companyId, string filter)
        {
            return _dbContext.Card.Where(m => m.CardId.Contains(filter));
        }

        public Card GetByCardId(int? companyId, string cardId)
        {
            return _dbContext.Card.Where(m =>
                m.CardId.Equals(cardId) && m.CompanyId == companyId && !m.IsDeleted).FirstOrDefault();
        }

        public int GetCardStatusByUserIdAndCardId(int companyId, int userId, string cardId)
        {
            return _dbContext.Card.Where(m =>
                m.CardId.Equals(cardId) && m.UserId == userId && m.CompanyId == companyId && !m.IsDeleted).Select(m => m.CardStatus).FirstOrDefault();
        }
        public int GetCardCountByuserIds(int companyId, List<int> userIds)
        {
            return _dbContext.Card.Where(m => m.UserId != null &&
                userIds.Contains(m.UserId.Value) && m.CompanyId == companyId && !m.IsDeleted).Count();
        }
        ///// <summary>
        ///// Get valid user
        ///// </summary>
        ///// <returns></returns>
        //public IQueryable<User> GetValidUser(int companyId)
        //{
        //    return _dbContext.User.Include(m => m.Department).Where(m =>
        //        m.CompanyId == companyId && (m.ExpiredDate == null ||
        //                                     m.ExpiredDate.Value.Date.Subtract(DateTime.Now.Date).Days >= 0) && !m.IsDeleted);
        //}

        ///// <summary>
        ///// Get users by card id
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="cardIds"></param>
        ///// <returns></returns>
        //public List<User> GetUsersByCardId(int companyId, List<string> cardIds)
        //{
        //    return _dbContext.User.AsNoTracking().Where(c =>
        //            cardIds.Any(m => m == c.CardId) && c.CompanyId == companyId && !c.IsDeleted).ToList();
        //}

        ///// <summary>
        ///// Get user by list of user id in company
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="userIds"></param>
        ///// <returns></returns>
        //public List<User> GetByIds(int companyId, List<int> userIds)
        //{
        //    return _dbContext.User.Where(m =>
        //            userIds.Any(c => c == m.Id) && m.CompanyId == companyId && !m.IsDeleted)
        //        .Include(c => c.Department).Include(c => c.AccessGroup).ThenInclude(c => c.AccessGroupDevice)
        //        .ThenInclude(c => c.Icu).ToList();
        //}

        ///// <summary>
        ///// Get user by card id
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="cardId"></param>
        ///// <returns></returns>
        //public User GetByCardId(int companyId, string cardId)
        //{
        //    return _dbContext.User.FirstOrDefault(c =>
        //        c.CardId == cardId && c.CompanyId == companyId && !c.IsDeleted);
        //}

        ///// <summary>
        ///// Get user by card id
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="cardId"></param>
        ///// <returns></returns>
        //public User GetByCardIdIncludeDepartment(int companyId, string cardId)
        //{
        //    return _dbContext.User.Include(c => c.Department).FirstOrDefault(c =>
        //        c.CardId == cardId && c.CompanyId == companyId && !c.IsDeleted);
        //}

        ///// <summary>
        ///// Get user by keypad password
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="keyPadPw"></param>
        ///// <returns></returns>
        //public User GetByKeyPadPw(int companyId, string keyPadPw)
        //{
        //    return _dbContext.User.AsNoTracking().FirstOrDefault(c =>
        //        c.KeyPadPw == keyPadPw && c.CompanyId == companyId && !c.IsDeleted);
        //}

        ///// <summary>
        ///// Get users by card id
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="keyPadPws"></param>
        ///// <returns></returns>
        //public List<User> GetByKeyPadPws(int companyId, List<string> keyPadPws)
        //{
        //    return _dbContext.User.AsNoTracking().Where(c =>
        //            keyPadPws.Any(m => m == c.KeyPadPw) && c.CompanyId == companyId && !c.IsDeleted)
        //        .ToList();
        //}





        ///// <summary>
        ///// Check whether keypad password is existed in the company
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="userId"></param>
        ///// <param name="enctypedKeyPadPassword"></param>
        ///// <returns></returns>
        //public bool IsKeyPadPasswordExist(int companyId, int userId, string enctypedKeyPadPassword)
        //{
        //    if (!string.IsNullOrEmpty(enctypedKeyPadPassword))
        //    {
        //        return Get(m =>
        //                   m.KeyPadPw == enctypedKeyPadPassword && m.CompanyId == companyId &&
        //                   m.Id != userId && !m.IsDeleted) != null;
        //    }

        //    return false;
        //}

        ///// <summary>
        ///// Get user by access group
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="accessGroupId"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetAssignUsers(int companyId, int accessGroupId)
        //{
        //    return _dbContext.User.Include(m => m.Department)
        //        .Where(m => m.CompanyId == companyId &&
        //                    m.AccessGroupId == accessGroupId && !m.IsDeleted);
        //}

        ///// <summary>
        ///// Get user by access groups
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="accessGroupIds"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetAssignUsersByAccessGroupIds(int companyId, List<int> accessGroupIds)
        //{
        //    return _dbContext.User.Include(m => m.Department)
        //        .Where(
        //            u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted);
        //}

        ///// <summary>
        ///// Get user by access groups
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="accessGroupIds"></param>
        ///// <returns></returns>
        //public int GetCountUserMasterCard(int companyId, List<int> accessGroupIds)
        //{
        //    return _dbContext.User.Count(
        //            u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted && u.IsMasterCard);
        //}

        ///// <summary>
        ///// get user master card
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="accessGroupIds"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetUserMasterCard(int companyId, List<int> accessGroupIds)
        //{
        //    return _dbContext.User.Where(
        //        u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted && u.IsMasterCard);
        //}

        ///// <summary>
        ///// Get list user not contain list userids
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="accessGroupId"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetUnAssignUsers(int companyId, int accessGroupId)
        //{
        //    return _dbContext.User.Include(c => c.Department).Include(c => c.AccessGroup).Where(m =>
        //        m.CompanyId == companyId && m.AccessGroupId != accessGroupId && !m.IsDeleted && !m.IsMasterCard);
        //}

        ///// <summary>
        ///// Get unassign with mastercard
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="accessGroupId"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetUnAssignUsersForMasterCard(int companyId, int accessGroupId)
        //{
        //    return _dbContext.User.Include(c => c.Department).Include(c => c.AccessGroup).Where(m =>
        //        m.CompanyId == companyId && m.AccessGroupId != accessGroupId && !m.IsDeleted);
        //}

        ///// <summary>
        ///// Get by id and company
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetByCompanyId(int companyId)
        //{
        //    return _dbContext.User.Include(m => m.Department).Include(m => m.AccessGroup)
        //        .Where(m => m.CompanyId == companyId && m.CardStatus != (short)CardStatus.InValid && !m.IsDeleted);
        //}

        ///// <summary>
        ///// Get invalid user
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <returns></returns>
        //public IQueryable<User> GetUserByCompany(int companyId)
        //{
        //    return _dbContext.User.Include(m => m.Department).Include(m => m.AccessGroup)
        //        .Where(m => m.CompanyId == companyId && !m.IsDeleted);
        //}

        ///// <summary>
        ///// Get by companyid and userid
        ///// </summary>
        ///// <param name="companyId"></param>
        ///// <param name="userId"></param>
        ///// <returns></returns>
        //public User GetByUserId(int companyId, int userId)
        //{
        //    return _dbContext.User.Include(m => m.AccessGroup).FirstOrDefault(m =>
        //        m.Id == userId && m.CompanyId == companyId && !m.IsDeleted);
        //}

        public bool CheckIsExistedQrCode(string qrId)
        {
            var qr = _dbContext.Card.Where(m => !m.IsDeleted && m.CardId == qrId).Count();
            if (qr != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        public List<Card> GetCardQrByCompanyId(int companyId)
        {
            return GetMany(m => m.CompanyId == companyId && m.CardType == 2).ToList();
        }
        
        public Card GetQrCode(int userId)
        {
            var qr = _dbContext.Card.FirstOrDefault(m =>
                !m.IsDeleted && m.CardType == (short) CardType.QrCode && m.UserId == userId);
            return qr;
        }
        
        public bool GetQrCodeBelongToUser(int userId)
        {
            return GetMany(m => m.CardType == 1 && m.UserId == userId && !m.IsDeleted).ToList().Count != 1;
        }
        
        public Card GetRandomQrCode(int companyId)
        {
            return _dbContext.Card.FirstOrDefault(m => !m.IsDeleted && m.CardType == (short) CardType.QrCode && m.CompanyId == companyId);
        }

        public Card GetPassCode(int userId, int companyId)
        {
            return _dbContext.Card.FirstOrDefault(m => !m.IsDeleted && m.CardType == (short) CardType.PassCode && m.CompanyId == companyId && m.UserId == userId);
        }
    }
}