﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.DataAccess.Models;
using System.Linq.Dynamic.Core;
using DeMasterProCloud.DataModel.EventLog;
using System.Threading;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for EventLog repository
    /// </summary>
    public interface IEventLogRepository : IGenericRepository<EventLog>
    {
        long GetMaxIndexByIcu(int icuId);
        bool IsExisted(int icuId, DateTime? eventTime);
        bool IsDuplicated(EventLog eventLog);
        List<EventLog> GetAllEventLogNormalAccess(int companyId);

        List<EventLog> GetAllEventLogNormalAccessToday(int companyId);
        void AddEventLog(EventLog model);
        List<EventLog> GetFirstInOutLogNormalAccess(int companyId, DateTime firstTime, DateTime lastTime);
        List<EventLog> GetFirstInOutNormalAccessToday(int companyId);

        IEnumerable<EventLogReportListModel> GetPaginatedEventLog(IQueryable<EventLog> data, int pageNumber, int pageSize, string sortDirection, int sortColumn, string culture);
    }

    /// <summary>
    /// EventLog repository
    /// </summary>
    public class EventLogRepository : GenericRepository<EventLog>, IEventLogRepository
    {
        private readonly AppDbContext _dbContext;
        public EventLogRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get max event log index by icu
        /// </summary>
        /// <param name="icuId"></param>
        /// <returns></returns>
        public long GetMaxIndexByIcu(int icuId)
        {
            if (!_dbContext.EventLog.Any(c => c.IcuId == icuId))
            {
                return 0;
            }
            return _dbContext.EventLog.Where(c => c.IcuId == icuId).Max(c => c.Index);
        }

        /// <summary>
        /// Check if there is a event log is existed.
        /// </summary>
        /// <param name="icuId"></param>
        /// <param name="eventTime"></param>
        /// <returns></returns>
        public bool IsExisted(int icuId, DateTime? eventTime)
        {
            return _dbContext.EventLog.Any(c => c.IcuId == icuId && c.EventTime == eventTime);
        }

        public bool IsDuplicated(EventLog eventLog)
        {
            return _dbContext.EventLog.Any(e => e.IcuId == eventLog.IcuId
                                        && e.EventTime.ToSettingDateTimeString().Equals(eventLog.EventTime.ToSettingDateTimeString())
                                        && e.EventType == eventLog.EventType
                                        && e.CardId == eventLog.CardId
                                        && e.Antipass == eventLog.Antipass);
        }

        public List<EventLog> GetAllEventLogNormalAccess(int companyId)
        {
            return _dbContext.EventLog.Where(e => e.UserId != null
                                                  && e.CardId != null
                                                  && e.EventType == (short)EventType.NormalAccess
                                                  && e.Icu != null
                                                  && e.CompanyId == companyId).ToList();
        }

        public List<EventLog> GetAllEventLogNormalAccessToday(int companyId)
        {
            return _dbContext.EventLog.Where(e => e.UserId != null
                                                  && e.CardId != null
                                                  && e.EventType == (short)EventType.NormalAccess
                                                  && e.Icu != null
                                                  && e.CompanyId == companyId
                                                  && e.EventTime.Date == DateTime.Now.Date).ToList();
        }



        public List<EventLog> GetFirstInOutLogNormalAccess(int companyId, DateTime firstTime, DateTime lastTime)
        {


            var lst = _dbContext.EventLog.Where(e => e.UserId != null
                                                  && e.CardId != null
                                                  && e.EventType == (short)EventType.NormalAccess
                                                  && e.Icu != null
                                                  && e.Antipass.ToLower() == "in"
                                                  && e.CompanyId == companyId
                                                  && (e.EventTime.Date >= firstTime.Date && e.EventTime.Date <= lastTime.Date)).Select(grp => new
                                                  {
                                                      UserId = Convert.ToInt32(grp.UserId),
                                                      _Antipass = grp.Antipass,
                                                      EventTimeDay = grp.EventTime.Date,
                                                      EventTimeDateTime = grp.EventTime,
                                                      _IcuId = grp.IcuId,
                                                      _CompanyId = grp.CompanyId,
                                                  }).OrderBy(a => a.UserId).ToList();

            var result = lst.GroupBy(r => new { r.UserId, r._IcuId, r._CompanyId, r.EventTimeDay }).Select(grp => new EventLog
            {
                UserId = Convert.ToInt32(grp.Key.UserId),
                Antipass = "In",
                EventTime = grp.Min(d => d.EventTimeDateTime),
                IcuId = grp.Key._IcuId,
                CompanyId = grp.Key._CompanyId,
            }).ToList();


            var _lst = _dbContext.EventLog.Where(e => e.UserId != null
                                                  && e.CardId != null
                                                  && e.EventType == (short)EventType.NormalAccess
                                                  && e.Icu != null
                                                  && e.CompanyId == companyId
                                                  && (e.EventTime.Date >= firstTime.Date && e.EventTime.Date <= lastTime.Date)).Select(grp => new
                                                  {
                                                      UserId = Convert.ToInt32(grp.UserId),
                                                      _Antipass = grp.Antipass,
                                                      EventTimeDay = grp.EventTime.Date,
                                                      EventTimeDateTime = grp.EventTime,
                                                      _IcuId = grp.IcuId,
                                                      _CompanyId = grp.CompanyId,
                                                  }).OrderBy(a => a.UserId).ToList();


            var _result = _lst.GroupBy(r => new { r.UserId, r._IcuId, r._CompanyId, r.EventTimeDay }).Select(grp => new EventLog
            {
                UserId = Convert.ToInt32(grp.Key.UserId),
                EventTime = grp.Max(d => d.EventTimeDateTime),
                Antipass = _lst.Where(x=> x.UserId == grp.Key.UserId && x.EventTimeDateTime == grp.Max(d=>x.EventTimeDateTime)).Select(x=>x._Antipass).FirstOrDefault(),
                IcuId = grp.Key._IcuId,
                CompanyId = grp.Key._CompanyId,
            }).ToList();


            var lstEventLog = result.Concat(_result);

            return lstEventLog.ToList();
        }
        public List<EventLog> GetFirstInOutNormalAccessToday(int companyId)
        {

            var result = _dbContext.EventLog.Where(e => e.UserId != null
                                                  && e.CardId != null
                                                  && e.EventType == (short)EventType.NormalAccess
                                                  && e.Icu != null
                                                  && e.CompanyId == companyId
                                                  && e.Antipass.ToLower() == "in"
                                                  && e.EventTime.Date == DateTime.Now.Date).GroupBy(r => new { UserId = r.UserId, Day = r.EventTime.Date, Antipass = r.Antipass, IcuId = r.IcuId, CompanyId = r.CompanyId }).Select(grp => new EventLog
                                                  {
                                                      UserId = Convert.ToInt32(grp.Key.UserId),
                                                      Antipass = grp.Key.Antipass,
                                                      EventTime = grp.Min(d => d.EventTime.Date),
                                                      IcuId = grp.Key.IcuId,
                                                      CompanyId = grp.Key.CompanyId,
                                                  }).OrderBy(a => a.EventTime).ThenBy(b => b.EventTime).Distinct().ToList();


            var _result = _dbContext.EventLog.Where(e => e.UserId != null
                                                  && e.CardId != null
                                                  && e.EventType == (short)EventType.NormalAccess
                                                  && e.Icu != null
                                                  && e.CompanyId == companyId
                                                  && e.EventTime.Date == DateTime.Now.Date).GroupBy(r => new { UserId = r.UserId, Day = r.EventTime.Date, Antipass = r.Antipass, IcuId = r.IcuId, CompanyId = r.CompanyId }).Select(grp => new EventLog
                                                  {
                                                      UserId = Convert.ToInt32(grp.Key.UserId),
                                                      Antipass = grp.Key.Antipass,
                                                      EventTime = grp.Max(d => d.EventTime.Date),
                                                      IcuId = grp.Key.IcuId,
                                                      CompanyId = grp.Key.CompanyId,
                                                  }).OrderByDescending(a => a.EventTime).ThenBy(b => b.EventTime).Distinct().ToList();
            var lstEventLog = result.Concat(_result);

            return lstEventLog.ToList();
        }






        public void AddEventLog(EventLog model)
        {
            _dbContext.EventLog.Add(model);
            _dbContext.SaveChanges();
        }


        public IEnumerable<EventLogReportListModel> GetPaginatedEventLog(IQueryable<EventLog> data, int pageNumber, int pageSize, string sortDirection, int sortColumn, string culture)
        {
            int cultureCode = 0;

            switch (culture)
            {
                case "en-US":
                    cultureCode = 0;
                    break;
                case "ja-JP":
                    cultureCode = 1;
                    break;
                case "ko-KR":
                    cultureCode = 2;
                    break;
                case "vi-VN":
                    cultureCode = 3;
                    break;
                default:
                    cultureCode = 0;
                    break;
            }

            var result = (from A in data
                          join B in _dbContext.Event on A.EventType equals B.EventNumber
                          where B.Culture == cultureCode
                          orderby A.EventTime descending
                          select new EventLogReportListModel
                          {
                              AccessTime = A.EventTime.ToString(ApplicationVariables.Configuration[
                                           Constants.DateTimeServerFormat + ":" + Thread.CurrentThread.CurrentCulture.Name]),
                              Action = A.Antipass,
                              BirthDay = A.User != null && A.User.BirthDay != null ? A.User.BirthDay.ToSettingDateString() : "",
                              Building = A.Icu != null && A.Icu.Building != null ? A.Icu.Building.Name : "",
                              CardId = A.CardId,
                              CardStatus = ((CardStatus)A.CardStatus).GetDescription(),
                              CardType = ((CardType)A.CardType).GetDescription(),
                              Department = A.User != null && A.User.Department != null ? A.User.Department.DepartName : "",
                              DeviceAddress = A.Icu != null ? A.Icu.DeviceAddress : "",
                              DoorName = A.Icu != null ? A.Icu.Name : "",
                              EmployeeNumber = A.User != null ? A.User.EmpNumber : "",
                              EventDetail = B.EventName,
                              IcuId = A.IcuId,
                              InOut = Constants.AntiPass.Contains(A.Antipass) ? ((Antipass)Enum.Parse(typeof(Antipass), A.Antipass)).GetDescription() : "",
                              IssueCount = A.IssueCount,
                              UserCode = A.User != null ? A.User.UserCode : "",
                              UserId = A.UserId,
                              UserName = A.UserName,
                              VisitId = A.VisitId
                          });

            if (sortDirection.Equals("desc"))
            {
                switch (sortColumn)
                {
                    case 0:
                    case 1:
                        result = result.OrderByDescending(c => c.AccessTime);
                        break;
                    case 2:
                        result = result.OrderByDescending(c => c.UserName);
                        break;
                    case 3:
                        result = result.OrderByDescending(c => c.BirthDay);
                        break;
                    case 4:
                        result = result.OrderByDescending(c => c.UserCode);
                        break;
                    case 5:
                        result = result.OrderByDescending(c => c.Department);
                        break;
                    case 6:
                        result = result.OrderByDescending(c => c.CardId);
                        break;
                    case 7:
                        result = result.OrderByDescending(c => c.DeviceAddress);
                        break;
                    case 8:
                        result = result.OrderByDescending(c => c.DoorName);
                        break;
                    case 9:
                        result = result.OrderByDescending(c => c.Building);
                        break;
                    case 10:
                        result = result.OrderByDescending(c => c.InOut);
                        break;
                    case 11:
                        result = result.OrderByDescending(c => c.EventDetail);
                        break;
                    case 12:
                        result = result.OrderByDescending(c => c.IssueCount);
                        break;
                    case 13:
                        result = result.OrderByDescending(c => c.CardStatus);
                        break;
                    case 14:
                        result = result.OrderByDescending(c => c.CardType);
                        break;

                    default:
                        break;
                };
            }
            else if (sortDirection.Equals("asc"))
            {
                switch (sortColumn)
                {
                    case 0:
                    case 1:
                        result = result.OrderBy(c => c.AccessTime);
                        break;
                    case 2:
                        result = result.OrderBy(c => c.UserName);
                        break;
                    case 3:
                        result = result.OrderBy(c => c.BirthDay);
                        break;
                    case 4:
                        result = result.OrderBy(c => c.UserCode);
                        break;
                    case 5:
                        result = result.OrderBy(c => c.Department);
                        break;
                    case 6:
                        result = result.OrderBy(c => c.CardId);
                        break;
                    case 7:
                        result = result.OrderBy(c => c.DeviceAddress);
                        break;
                    case 8:
                        result = result.OrderBy(c => c.DoorName);
                        break;
                    case 9:
                        result = result.OrderBy(c => c.Building);
                        break;
                    case 10:
                        result = result.OrderBy(c => c.InOut);
                        break;
                    case 11:
                        result = result.OrderBy(c => c.EventDetail);
                        break;
                    case 12:
                        result = result.OrderBy(c => c.IssueCount);
                        break;
                    case 13:
                        result = result.OrderBy(c => c.CardStatus);
                        break;
                    case 14:
                        result = result.OrderBy(c => c.CardType);
                        break;

                    default:
                        break;
                };
            }

            result = result.Skip((pageNumber - 1) * pageSize).Take(pageSize);

            return result;
        }
    }
}