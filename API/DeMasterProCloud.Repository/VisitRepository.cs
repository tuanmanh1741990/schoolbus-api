﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Visit;
using AutoMapper;
using Newtonsoft.Json.Linq;
using DeMasterProCloud.DataModel.EventLog;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// User repository interface
    /// </summary>
    public interface IVisitRepository : IGenericRepository<Visit>
    {

        IQueryable<Visit> GetByCompanyId(int companyId);
        IQueryable<Visit> GetByAccessGroupId(int companyId, int accessGroupId);
        IQueryable<Visit> GetByFirstApprovalId(int companyId, int approval1Id);
        Visit GetByVisitId(int companyId, int visitId);
        List<Visit> GetByVisitIds(int companyId, List<int> visitIds);
        int GetMaxId(int companyId);
        Visit GetByCardId(int? companyId, string cardId);
        Visit GetByCardIdExceptThis(int companyId, int id, string cardId);
        void CreateDefaultVisitSetting(int companyId);

        VisitSetting GetVisitSetting(int companyId);

        void UpdateVisitSetting(VisitSetting setting);
        IEnumerable<EventLog> GetHistoryVisitor(int id, int pageNumber, int pageSize, out int totalRecords);
    }

    /// <summary>
    /// Visit repository
    /// </summary>
    public class VisitRepository : GenericRepository<Visit>, IVisitRepository
    {
        private readonly AppDbContext _dbContext;
        public VisitRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get by company ID
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<Visit> GetByCompanyId(int companyId)
        {
            return _dbContext.Visit
                .Where(m => m.CompanyId == companyId && !m.IsDeleted);
        }

        /// <summary>
        /// Get by approvalId
        /// </summary>
        /// <param name="companyId"> identifier of company </param>
        /// <param name="approval1Id"> identifier of first approval account </param>
        /// <returns></returns>
        public IQueryable<Visit> GetByFirstApprovalId(int companyId, int approval1Id)
        {
            var visits = _dbContext.Visit.Where(m => m.CompanyId == companyId && m.ApproverId1 == approval1Id);

            return visits;
        }

        /// <summary>
        /// Get by companyid and visitid
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="visitId"></param>
        /// <returns></returns>
        public Visit GetByVisitId(int companyId, int visitId)
        {
            return _dbContext.Visit.Where(m =>
                m.Id == visitId && m.CompanyId == companyId && !m.IsDeleted).FirstOrDefault();
        }


        public List<Visit> GetByVisitIds(int companyId, List<int> visitIds)
        {
            return _dbContext.Visit.Where(m =>
                visitIds.Contains(m.Id) && m.CompanyId == companyId && !m.IsDeleted).ToList();
        }

        public int GetMaxId(int companyId)
        {
            return _dbContext.Visit.Where(m =>
                m.CompanyId == companyId).Select(m => m.Id).Max();
        }



        public Visit GetByCardId(int? companyId, string cardId)
        {
            return _dbContext.Visit.FirstOrDefault(m =>
                m.CardId.Equals(cardId) && m.CompanyId == companyId && !m.IsDeleted && m.Status != (short)VisitStatus.Reclamation);
        }

        public Visit GetByCardIdExceptThis(int companyId, int id, string cardId)
        {
            return _dbContext.Visit.FirstOrDefault(m => m.Id != id && m.CardId.Equals(cardId) && m.CompanyId == companyId
                                                        && !m.IsDeleted && m.Status != (short)VisitStatus.Reclamation);
        }

        public void CreateDefaultVisitSetting(int companyId)
        {
            var setting = new VisitSetting
            {
                CompanyId = companyId,
                ApprovalStepNumber = (short)VisitSettingType.NoStep
            };
            _dbContext.VisitSetting.Add(setting);
        }

        public VisitSetting GetVisitSetting(int companyId)
        {
            return _dbContext.VisitSetting.FirstOrDefault(m => m.CompanyId == companyId);
        }

        /// <summary>
        /// Get visitor by access group
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public IQueryable<Visit> GetByAccessGroupId(int companyId, int accessGroupId)
        {
            return _dbContext.Visit.Where(m => m.CompanyId == companyId &&
                            m.AccessGroupId == accessGroupId && !m.IsDeleted);
        }

        public void UpdateVisitSetting(VisitSetting setting)
        {
            _dbContext.VisitSetting.Update(setting);
            _dbContext.SaveChanges();
        }

        public IEnumerable<EventLog> GetHistoryVisitor(int id, int pageNumber, int pageSize, out int totalRecords)
        {
            var device = _dbContext.EventLog.FirstOrDefault(c =>
               c.VisitId == id);

            if (device != null)
            {
                if (device != null && device.CompanyId != null)
                {
                    device.Company = _dbContext.Company.FirstOrDefault(m => m.Id == device.CompanyId && !m.IsDeleted);
                }


                var companyId = device.CompanyId;

                var data = _dbContext.EventLog.Where(c =>
                   c.CompanyId == companyId && c.IsVisit == true
                   && c.VisitId == id)
                       .Include(m => m.User)
                       .Include(m => m.User.Department)
                       .Include(m => m.Icu)
                       .Include(m => m.Icu.Building)
                       .Include(m => m.Company)
                       .Include(m => m.Visit)
                       .AsQueryable();



                totalRecords = data.Count();
                data = data.OrderByDescending(m => m.EventTime);
                data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize);
                return data;
            }
            totalRecords = 0;
            return new List<EventLog>();

        }

    }
}