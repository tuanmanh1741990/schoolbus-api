﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Migrations;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealEventLog;
using DeMasterProCloud.DataModel.MealSetting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.WebSockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Transactions;

namespace DeMasterProCloud.Repository
{
    public interface IMealEventLogRepository
    {
        ReponseStatus AddEventLog(int mealTypeId, decimal price, string cardId, int cornerId);
        ReponseStatus AddMealEventLogManual(MealEventLogAddModel model);
        ReponseStatus UpdateMealEventLogManual(MealEventLogUpdateModel model);
        List<MealEventLogModel> GetListMealEventLogManual(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, List<int> icuId, List<int> buildingId, List<int> cornerId, int isManual);
        MealEventLogModel GetMealEventLogManual(int id);
        ReponseStatus DeleteMeaEventLog(int id);
        List<MealEventLogReportModel> Report(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, string doorName, List<int> buildingId, int isReport);
        List<ReportCanteenManagementByBuildingModel> ListReportByBuilding(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> buildingId);
        List<ReportCanteenManagementByCornerModel> ListReportByCorner(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> cornerId);

    }
    public class MealEventLogRepository : GenericRepository<MealEventLog>, IMealEventLogRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public MealEventLogRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            _httpContext = contextAccessor.HttpContext;
        }

        public ReponseStatus AddEventLog(int mealTypeId, decimal price, string cardId, int cornerId)
        {
            try
            {
                var eventLogId = _dbContext.EventLog.OrderByDescending(x => x.Id).Select(x => x.Id).First();
                var userId = _dbContext.Card.Where(x => x.CardId == cardId && x.CardType == (short)CardType.NFC).Select(x => x.UserId).FirstOrDefault();
                var userDiscount = _dbContext.UserDiscount.Where(x => x.UserId == userId).FirstOrDefault();
                decimal priceDiscount = 0;
                if (userDiscount != null)
                {
                    priceDiscount = userDiscount.Amount;
                }

                MealEventLog model = new MealEventLog();
                var mealType = _dbContext.MealType.Where(x => x.Id == mealTypeId).FirstOrDefault();
                model.MealCode = mealType.Code;
                model.MealType = mealType.Name;
                model.Price = userId == null ? price.ToString() : (price - priceDiscount).ToString();
                model.EventLogId = eventLogId;
                model.CornerId = cornerId;
                Add(model);
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }
            return res;

        }

        public ReponseStatus AddMealEventLogManual(MealEventLogAddModel model)
        {
            decimal priceException = 0;
            decimal priceUserDiscount = 0;
            var mealSetting = (from a in _dbContext.MealSetting
                               join b in _dbContext.MealType on a.MealTypeId equals b.Id
                               where a.IcuDeviceId == model.icuId && b.Code == model.mealCode
                               select new { a.Price, a.CornerId, a.Id }).First();

            var exceptionalMeal = _dbContext.ExceptionalMeal.Where(x => (x.Start <= model.eventTime && model.eventTime <= x.End) && x.MealSettingId == mealSetting.Id).FirstOrDefault();
            if (exceptionalMeal != null)
            {
                priceException = exceptionalMeal.Price;
            }
            var userDiscount = _dbContext.UserDiscount.Where(x => x.UserId == model.userId).FirstOrDefault();
            if(userDiscount != null)
            {
                priceUserDiscount = userDiscount.Amount;
            }    

            _dbContext.Database.CreateExecutionStrategy().Execute(() =>
            {

                using (var transaction = new TransactionScope())
                {
                    var depId = _dbContext.User.Where(x => x.Id == model.userId).Select(x => x.DepartmentId).FirstOrDefault();
                    var cardId = _dbContext.Card.Where(x => x.UserId == model.userId).Select(x => x.CardId).FirstOrDefault();
                    EventLog eventLog = new EventLog();
                    eventLog.CompanyId = _httpContext.User.GetCompanyId();
                    eventLog.CreatedOn = DateTime.Now;
                    eventLog.IcuId = model.icuId;
                    eventLog.UserName = model.userName;
                    eventLog.UserId = model.userId;
                    eventLog.EventType = model.mealCode;
                    eventLog.DoorName = model.doorName;
                    eventLog.DeptId = depId;
                    eventLog.EventTime = model.eventTime;
                    eventLog.CardId = cardId;
                    AddEventLog(eventLog);

                    //add data to table MealEventLog
                    MealEventLog mealEventLog = new MealEventLog();
                    int eventLogId = _dbContext.EventLog.OrderByDescending(x => x.Id).Select(x => x.Id).First();

                    mealEventLog.MealCode = model.mealCode;
                    mealEventLog.MealType = model.mealType;
                    mealEventLog.EventLogId = eventLogId;
                    mealEventLog.Price = (priceException == 0 ? (mealSetting.Price - priceUserDiscount) : (priceException -priceUserDiscount)).ToString();
                    mealEventLog.IsManual = true;
                    mealEventLog.CornerId = mealSetting.CornerId;
                    Add(mealEventLog);

                    res.message = Constants.CornerSetting.AddSuccess;
                    res.statusCode = true;
                    res.data = new { eventLogId = eventLog.Id, mealEventLogId = mealEventLog.Id };
                    transaction.Complete();
                }

            });

            return res;
        }


        public ReponseStatus UpdateMealEventLogManual(MealEventLogUpdateModel model)
        {

            _dbContext.Database.CreateExecutionStrategy().Execute(() =>
            {

                using (var transaction = new TransactionScope())
                {
                    try
                    {
                        //Update table MealEventLog
                        var updateMealEventLog = _dbContext.MealEventLog.Find(model.mealEventLogId);
                        var mealSetting = (from a in _dbContext.MealSetting
                                           join b in _dbContext.MealType on a.MealTypeId equals b.Id
                                           where a.IcuDeviceId == model.icuId
                                           select new { a.Price, a.CornerId }).FirstOrDefault();
                        var depId = _dbContext.User.Where(x => x.Id == model.userId).Select(x => x.DepartmentId).FirstOrDefault();
                        updateMealEventLog.MealCode = model.mealCode;
                        updateMealEventLog.MealType = model.mealType;
                        updateMealEventLog.Price = mealSetting.Price.ToString();
                        updateMealEventLog.CornerId = mealSetting.CornerId;
                        Update(updateMealEventLog);
                        //Update table EventLog
                        var eventLog = _dbContext.MealEventLog.Find(model.mealEventLogId);
                        var updateEventLog = _dbContext.EventLog.Find(eventLog.EventLogId);
                        updateEventLog.EventTime = model.eventTime;
                        updateEventLog.IcuId = model.icuId;
                        updateEventLog.UserId = model.userId;
                        updateEventLog.UserName = model.userName;
                        updateEventLog.DoorName = model.doorName;
                        updateEventLog.DeptId = depId;
                        UpdateEventLog(updateEventLog);
                        res.message = Constants.CornerSetting.AddSuccess;
                        res.statusCode = true;
                        transaction.Complete();
                    }
                    catch (Exception ex)
                    {

                        res.message = Constants.CornerSetting.AddFailed;
                        res.statusCode = false;

                    }
                }

            });
            return res;


        }


        public List<MealEventLogModel> GetListMealEventLogManual(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, List<int> icuId, List<int> buildingId, List<int> cornerId, int isManual)
        {
            var firsTime = DateTimeHelper.ConvertIsoToDateTime(firstAccessTime);
            var lastTime = DateTimeHelper.ConvertIsoToDateTime(lastAccessTime);

            var data = (from a in _dbContext.MealEventLog
                        join b in _dbContext.EventLog on a.EventLogId equals b.Id
                        join c in _dbContext.IcuDevice on b.IcuId equals c.Id
                        join d in _dbContext.CornerSetting on a.CornerId equals d.Id
                        where b.CompanyId == _httpContext.User.GetCompanyId()
                        && (firsTime <= b.EventTime.Date && b.EventTime.Date <= lastTime)
                        select new MealEventLogModel
                        {
                            companyId = Convert.ToInt32(b.CompanyId),
                            eventTime = b.EventTime,
                            icuId = b.IcuId,
                            userName = b.UserName,
                            cornerId = d.Id,
                            mealCode = a.MealCode,
                            mealType = a.MealType,
                            Price = Convert.ToDecimal(a.Price),
                            mealEventLogId = a.Id,
                            userId = Convert.ToInt32(b.UserId),
                            eventLogId = b.Id,
                            isManual = a.IsManual,
                            deptId = Convert.ToInt32(b.DeptId),
                            cardId = b.CardId,
                            deviceName = c.Name,
                            buildingId = Convert.ToInt32(c.BuildingId),
                            deviceAddress = c.DeviceAddress,
                            cornerName = d.Name
                        }).ToList();
            if (icuId.Count() > 0)
            {
                data = data.Where(x => icuId.Contains(x.icuId)).ToList();
            }
            if (cornerId.Count() > 0)
            {
                data = data.Where(x => cornerId.Contains(x.cornerId)).ToList();
            }
            if (cardId != null || cardId == "")
            {
                data = data.Where(x => x.cardId == cardId).ToList();
            }
            if (departmentId != 0)
            {
                data = data.Where(x => x.deptId == departmentId).ToList();
            }
            if (userId != 0)
            {
                data = data.Where(x => x.userId == userId).ToList();
            }
            if (mealCode.Count() > 0)
            {
                data = data.Where(x => mealCode.Contains(x.mealCode)).ToList();
            }
            if (buildingId.Count() > 0)
            {
                data = data.Where(x => buildingId.Contains(x.buildingId)).ToList();
            }
            if (isManual != -1)
            {
                data = data.Where(x => x.isManual == Convert.ToBoolean(isManual)).ToList();
            }

            data = (from a in data
                    join c in _dbContext.User on a.userId equals c.Id
                    join d in _dbContext.Department on a.deptId equals d.Id
                    join e in _dbContext.Building on a.buildingId equals e.Id

                    select new MealEventLogModel
                    {
                        eventTime = a.eventTime,
                        userName = a.userName,
                        userCode = c.UserCode,
                        birthDay = c.BirthDay == null ? (DateTime?)null : Convert.ToDateTime(c.BirthDay),
                        departmentName = d.DepartName,
                        deviceAddress = a.deviceAddress,
                        deviceName = a.deviceName,
                        buildingName = e.Name,
                        mealType = a.mealType,
                        cardId = a.cardId,
                        buildingId = a.buildingId,
                        mealEventLogId = a.mealEventLogId,
                        eventLogId = a.eventLogId,
                        cornerName = a.cornerName,
                        isManual = a.isManual
                    }).ToList();

            totalRecords = data.Count();

            recordsFiltered = data.Count();

            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


            return data;
        }

        public MealEventLogModel GetMealEventLogManual(int id)
        {
            return (from a in _dbContext.MealEventLog
                    join b in _dbContext.EventLog on a.EventLogId equals b.Id
                    join c in _dbContext.IcuDevice on b.IcuId equals c.Id
                    where a.Id == id
                    select new MealEventLogModel
                    {
                        eventTime = b.EventTime,//time check in
                        icuId = b.IcuId,
                        userName = b.UserName,
                        mealCode = a.MealCode,
                        mealType = a.MealType,
                        Price = Convert.ToDecimal(a.Price),
                        mealEventLogId = a.Id,
                        eventLogId = b.Id,
                        deviceName = c.Name,
                        isManual = a.IsManual
                    }).FirstOrDefault();

        }

        public ReponseStatus DeleteMeaEventLog(int id)
        {


            _dbContext.Database.CreateExecutionStrategy().Execute(() =>
            {

                using (var transaction = new TransactionScope())
                {
                    try
                    {

                        var mealEventLog = _dbContext.MealEventLog.Where(x => x.Id == id).FirstOrDefault();
                        if (mealEventLog.IsManual == true)
                        {
                            Remove(mealEventLog);
                            int eventLogId = mealEventLog.EventLogId;
                            var eventLog = _dbContext.EventLog.Where(x => x.Id == eventLogId).FirstOrDefault();
                            RemoveEventLog(eventLog);
                            res.message = Constants.CornerSetting.DeleteSuccess;
                            res.statusCode = true;
                            transaction.Complete();
                        }
                        else
                        {
                            res.message = "";
                            res.statusCode = true;
                        }

                    }
                    catch (Exception ex)
                    {
                        ex.ToString();
                        res.message = Constants.CornerSetting.DeleteFailed;
                        res.statusCode = false;
                    }

                }

            });

            return res;
        }

        public List<MealEventLogReportModel> Report(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, string cardId, int departmentId, List<int> mealCode, int userId, string doorName, List<int> buildingId, int isReport)
        {
            var firsTime = DateTimeHelper.ConvertIsoToDateTime(firstAccessTime);
            var lastTime = DateTimeHelper.ConvertIsoToDateTime(lastAccessTime);
            var data = (from a in _dbContext.EventLog
                        join b in _dbContext.MealEventLog on a.Id equals b.EventLogId
                        join c in _dbContext.IcuDevice on a.IcuId equals c.Id
                        where
                          a.CompanyId == _httpContext.User.GetCompanyId()
                          && firsTime.TimeOfDay <= a.EventTime.TimeOfDay && a.EventTime.TimeOfDay <= lastTime.TimeOfDay
                        select new MealEventLogModel
                        {
                            userId = Convert.ToInt32(a.UserId),
                            deptId = Convert.ToInt32(a.DeptId),
                            Price = Convert.ToDecimal(b.Price),
                            doorName = c.Name,
                            cardId = a.CardId,
                            mealCode = b.MealCode,
                            buildingId = Convert.ToInt32(c.BuildingId)
                        }).ToList();

            if (doorName != null || doorName == "")
            {
                data = data.Where(x => x.doorName.Trim().ToLower() == doorName.Trim().ToLower()).ToList();
            }
            if (cardId != null || cardId == "")
            {
                data = data.Where(x => x.cardId.Trim().ToLower() == cardId.Trim().ToLower()).ToList();
            }
            if (departmentId != 0)
            {
                data = data.Where(x => x.deptId.Equals(departmentId)).ToList();
            }
            if (userId != 0)
            {
                data = data.Where(x => x.userId == userId).ToList();
            }
            if (mealCode.Count > 0)
            {
                data = data.Where(x => mealCode.Contains(x.mealCode)).ToList();
            }
            if (buildingId.Count > 0)
            {
                data = data.Where(x => buildingId.Contains(x.buildingId)).ToList();
            }

            var dataGroupBy = data.GroupBy(x => new { x.userId, x.deptId, x.cardId }).Select(x1 => new MealEventLogModel
            {
                userId = x1.First().userId,
                deptId = x1.First().deptId,
                cardId = x1.First().cardId,
                Price = x1.Sum(c => c.Price)
            }).ToList();

            var allData = (from a in dataGroupBy
                           join b in _dbContext.User on a.userId equals b.Id
                           join c in _dbContext.Department on a.deptId equals c.Id
                           select new MealEventLogReportModel
                           {
                               userName = b.FirstName + "" + b.LastName,
                               userId = b.Id,
                               departmentName = c.DepartName,
                               userCode = b.UserCode,
                               cardId = a.cardId,
                               totalPrice = a.Price,
                               employeeNumber = b.EmpNumber,
                               birthDay = b.BirthDay == null ? (DateTime?)null : Convert.ToDateTime(b.BirthDay),
                           }).ToList();
            totalRecords = allData.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                allData = allData.Where(x => x.userName.ToLower().Contains(filter.ToLower())).ToList();
            }
            recordsFiltered = allData.Count();
            if (isReport == 0)
            {
                allData = allData.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            }

            return allData;
        }

        public List<ReportCanteenManagementByBuildingModel> ListReportByBuilding(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> buildingId)
        {
            var firsTime = DateTimeHelper.ConvertIsoToDateTime(firstAccessTime);
            var lastTime = DateTimeHelper.ConvertIsoToDateTime(lastAccessTime);
            var data = (from a in _dbContext.IcuDevice
                        from b in _dbContext.MealEventLog
                        join c in _dbContext.EventLog on new { A = a.Id, B = b.EventLogId } equals new { A = c.IcuId, B = c.Id }
                        where
                         c.CompanyId == _httpContext.User.GetCompanyId()
                        && (firsTime <= c.EventTime && c.EventTime <= lastTime)
                        select new
                        {
                            icuId = a.Id,
                            accessTime = c.EventTime.Date.ToString("dd/MM/yyyy"),
                            Building = Convert.ToInt32(a.BuildingId),
                            EventLogId = c.Id,
                            EvenType = c.EventType
                        }).Distinct().ToList();
            if (buildingId.Count > 0)
            {
                data = data.Where(x => buildingId.Contains(x.Building)).ToList();
            }

            var dataGroupBy = data.GroupBy(x => new { x.accessTime, x.Building, x.EvenType }).Select(x1 => new ReportCanteenGroupBy
            {


                BuildingId = Convert.ToInt32(x1.First().Building),
                AccessTime = x1.First().accessTime.ToString(),
                EvenType = x1.First().EvenType,
                TotalCount = x1.Count()
            }).ToList();

            dataGroupBy = dataGroupBy.GroupBy(x => new { x.AccessTime, x.EvenType, x.BuildingId, x.TotalCount }).Select(x1 => new ReportCanteenGroupBy
            {

                AccessTime = x1.First().AccessTime.ToString(),
                BuildingId = x1.First().BuildingId,
                EvenType = x1.First().EvenType,
                TotalCount = x1.First().TotalCount,
            }).Distinct().ToList();

            List<ReportCanteenManagementByBuildingModel> alldata = new List<ReportCanteenManagementByBuildingModel>();
            List<int> numbers = new List<int>();
            List<string> _stringDay = new List<string>();
            foreach (var item in dataGroupBy)
            {


                bool checkbuidingId = numbers.Contains(item.BuildingId);
                bool checkAccessTime = _stringDay.Contains(item.AccessTime.ToLower());
                if (checkbuidingId && checkAccessTime)
                {


                }
                else
                {
                    ReportCanteenManagementByBuildingModel ab = new ReportCanteenManagementByBuildingModel();

                    ab.Day = item.AccessTime;
                    ab.LstBuilding = new
                    {
                        BuildingId = _dbContext.Building.Where(x => x.Id == item.BuildingId).Select(x => x.Name).FirstOrDefault().ToString(),
                        data = new
                        {
                            data = ListMealTypeByBuildingId(dataGroupBy, item.AccessTime, item.BuildingId)
                        }
                    };
                    alldata.Add(ab);
                }
                numbers.Add(item.BuildingId);
                _stringDay.Add(item.AccessTime.ToLower());
            }
            List<ReportCanteenManagementByBuildingModel> _alldata = new List<ReportCanteenManagementByBuildingModel>();
            List<string> stringDay = new List<string>();
            foreach (var item in alldata)
            {
                bool checkDay = stringDay.Contains(item.Day.ToLower());
                if (!checkDay)
                {
                    ReportCanteenManagementByBuildingModel ab = new ReportCanteenManagementByBuildingModel();
                    ab.Day = item.Day;
                    ab.LstBuilding = alldata.Where(x => x.Day == item.Day).ToList();
                    _alldata.Add(ab);
                    stringDay.Add(item.Day.ToLower());
                }


            }
            totalRecords = _alldata.Count();

            recordsFiltered = _alldata.Count();
            _alldata = _alldata.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return _alldata;

        }

        public List<ReportCanteenManagementByCornerModel> ListReportByCorner(string filter, int pageNumber, int pageSize, int sortColumn,
              string sortDirection, out int totalRecords, out int recordsFiltered, string firstAccessTime, string lastAccessTime, List<int> cornerId)
        {
            var firsTime = DateTimeHelper.ConvertIsoToDateTime(firstAccessTime);
            var lastTime = DateTimeHelper.ConvertIsoToDateTime(lastAccessTime);
            var lstData = (from a in _dbContext.EventLog
                           join b in _dbContext.MealEventLog on a.Id equals b.EventLogId
                           join c in _dbContext.CornerSetting on b.CornerId equals c.Id
                           join d in _dbContext.MealType on a.EventType equals d.Code
                           join e in _dbContext.IcuDevice on a.IcuId equals e.Id
                           where a.CompanyId == _httpContext.User.GetCompanyId()
                             && (firsTime <= a.EventTime && a.EventTime <= lastTime)
                           select new ReportMealEventLogByCornerModel
                           {
                               EventLogId = a.Id,
                               AccessTime = a.EventTime.Date.ToString("dd/MM/yyyy"),
                               CornerId = c.Id,
                               MealTypeId = d.Id
                           }).Distinct().ToList();
            if (cornerId.Count() > 0)
            {
                lstData = lstData.Where(x => cornerId.Contains(x.CornerId)).ToList();
            }

            var dataGroupBy = lstData.GroupBy(x => new { x.AccessTime, x.CornerId, x.MealTypeId }).Select(x1 => new ReportMealEventLogByCornerModel
            {

                AccessTime = x1.First().AccessTime.ToString(),
                CornerId = x1.First().CornerId,
                MealTypeId = x1.First().MealTypeId,
                TotalCount = x1.Count()
            }).ToList();

            List<ReportCanteenManagementByBuildingModel> alldata = new List<ReportCanteenManagementByBuildingModel>();
            List<int> numbers = new List<int>();
            List<string> _stringDay = new List<string>();
            foreach (var item in dataGroupBy)
            {
                bool checkbuidingId = numbers.Contains(item.CornerId);
                bool checkAccessTime = _stringDay.Contains(item.AccessTime.ToLower());
                if (checkbuidingId && checkAccessTime)
                {


                }
                else
                {
                    ReportCanteenManagementByBuildingModel ab = new ReportCanteenManagementByBuildingModel();

                    ab.Day = item.AccessTime;
                    ab.LstBuilding = new
                    {
                        CornerName = _dbContext.CornerSetting.Where(x => x.Id == item.CornerId).Select(x => x.Name).FirstOrDefault().ToString(),
                        data = new
                        {
                            data = ListMealTypeByCornerIdId(dataGroupBy, item.AccessTime, item.CornerId)
                        }
                    };
                    alldata.Add(ab);

                }
                numbers.Add(item.CornerId);
                _stringDay.Add(item.AccessTime);
            }

            List<ReportCanteenManagementByCornerModel> _alldata = new List<ReportCanteenManagementByCornerModel>();
            List<string> stringDay = new List<string>();
            foreach (var item in alldata)
            {
                bool checkDay = stringDay.Contains(item.Day.ToLower());
                if (!checkDay)
                {
                    ReportCanteenManagementByCornerModel ab = new ReportCanteenManagementByCornerModel();
                    ab.Day = item.Day;
                    ab.LstCorner = alldata.Where(x => x.Day == item.Day).ToList();
                    _alldata.Add(ab);
                    stringDay.Add(item.Day.ToLower());
                }
            }

            totalRecords = _alldata.Count();

            recordsFiltered = _alldata.Count();

            _alldata = _alldata.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return _alldata;

        }


        public List<ReportCornerGroupBy> ListMealTypeByCornerIdId(List<ReportMealEventLogByCornerModel> listReport, string accessTime, int cornerId)
        {

            var data = listReport.Where(x => x.AccessTime == accessTime && x.CornerId == cornerId).Select(x => new { x.CornerId, x.MealTypeId, x.TotalCount }).Distinct().ToList();
            var alldata = data.Distinct().GroupBy(x => new { x.CornerId, x.MealTypeId, x.TotalCount }).Select(x => new ReportCornerGroupBy
            {
                MealTypeId = x.Key.MealTypeId,
                CornerId = x.Key.CornerId,
                TotalCount = x.Key.TotalCount
            }).Distinct().ToList();
            var lstdata = (from a in alldata
                           join b in _dbContext.MealType on a.MealTypeId equals b.Id
                           join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                           select new ReportCornerGroupBy
                           {
                               MealTypeName = b.Name,
                               CornerId = a.CornerId,
                               MealTypeId = a.MealTypeId,
                               CornerName = c.Name,
                               TotalCount = a.TotalCount
                           }).ToList();
            return lstdata;
        }


        public List<MealTypeByBuildingModel> ListMealTypeByBuildingId(List<ReportCanteenGroupBy> listReport, string accessTime, int buildingId)
        {

            var data = listReport.Where(x => x.AccessTime == accessTime && x.BuildingId == buildingId).Select(x => new { x.IcuId, x.EvenType, x.TotalCount }).Distinct().ToList();
            var alldata = data.Distinct().GroupBy(x => new { x.IcuId, x.EvenType, x.TotalCount }).Select(x => new ReportCanteenGroupBy
            {
                IcuId = x.Key.IcuId,
                EvenType = x.Key.EvenType,
                TotalCount = x.Key.TotalCount
            }).Distinct().ToList();
            var lstdata = (from a in alldata
                           join b in _dbContext.MealType on a.EvenType equals b.Code
                           select new MealTypeByBuildingModel
                           {
                               mealTypeName = b.Name,
                               mealTypeId = b.Id,
                               totalCount = a.TotalCount
                           }).ToList();
            return lstdata;
        }

        public void Add(MealEventLog model)
        {
            _dbContext.MealEventLog.Add(model);
            _dbContext.SaveChanges();

        }

        public void Update(MealEventLog model)
        {
            _dbContext.MealEventLog.Update(model);
            _dbContext.SaveChanges();

        }

        public void AddEventLog(EventLog model)
        {
            _dbContext.EventLog.Add(model);
            _dbContext.SaveChanges();

        }

        public void UpdateEventLog(EventLog model)
        {
            _dbContext.EventLog.Update(model);
            _dbContext.SaveChanges();

        }

        public void Remove(MealEventLog model)
        {
            _dbContext.MealEventLog.Remove(model);
            _dbContext.SaveChanges();
        }

        public void RemoveEventLog(EventLog model)
        {
            _dbContext.EventLog.Remove(model);
            _dbContext.SaveChanges();
        }
    }
}
