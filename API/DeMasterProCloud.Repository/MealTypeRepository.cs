﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealSetting;
using DeMasterProCloud.DataModel.MealType;
using Microsoft.AspNetCore.Http;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.WebSockets;
using System.Text;

namespace DeMasterProCloud.Repository
{
    public interface IMealTypeRepository
    {
        IQueryable<MealType> GetByCompanyId(int companyId);
        IQueryable<MealType> GetByCodeAndCompanyId(int mealTypeCode, int companyId);
    }

    public class MealTypeRepository : GenericRepository<MealTypeModel>, IMealTypeRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public MealTypeRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            _httpContext = contextAccessor.HttpContext;
        }

        public IQueryable<MealType> GetByCompanyId(int companyId)
        {
            var data = _dbContext.MealType.Where(m => m.CompanyId == companyId);

            return data;
        }

        public IQueryable<MealType> GetByCodeAndCompanyId(int mealTypeCode, int companyId)
        {
            var data = _dbContext.MealType.Where(m => m.Code == mealTypeCode && m.CompanyId == companyId);

            return data;
        }
    }
}
