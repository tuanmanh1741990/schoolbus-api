﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Linq;
using DeMasterProCloud.DataAccess.Models;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for Holiday repository
    /// </summary>
    public interface IPartTimeRepository : IGenericRepository<PartTime>
    {
        //IQueryable<Building> GetByCompanyId(int companyId);
        //Building GetByIdAndCompanyId(int companyId, int id);
        //List<Building> GetByIdsAndCompanyId(int companyId, List<int> ids);
        //string GetBuildingNameByRid(int companyId, string deviceAddress);
    }

    /// <summary>
    /// Holiday repository
    /// </summary>
    public class PartTimeRepository : GenericRepository<PartTime>, IPartTimeRepository
    {
        private readonly AppDbContext _dbContext;
        public PartTimeRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }


        //    /// <summary>
        //    /// Get by companyid
        //    /// </summary>
        //    /// <param name="companyId"></param>
        //    /// <returns></returns>
        //    public IQueryable<Building> GetByCompanyId(int companyId)
        //    {
        //        return _dbContext.Building.Where(x => x.CompanyId == companyId && !x.IsDeleted);
        //    }

        //    /// <summary>
        //    /// Get by company and id
        //    /// </summary>
        //    /// <param name="companyId"></param>
        //    /// <param name="id"></param>
        //    public Building GetByIdAndCompanyId(int companyId, int id)
        //    {
        //        return _dbContext.Building.FirstOrDefault(x => x.CompanyId == companyId && x.Id == id && !x.IsDeleted);
        //    }

        //    /// <summary>
        //    /// 
        //    /// </summary>
        //    /// <param name="companyId"></param>
        //    /// <param name="ids"></param>
        //    /// <returns></returns>
        //    public List<Building> GetByIdsAndCompanyId(int companyId, List<int> ids)
        //    {
        //        return _dbContext.Building.Where(x => ids.Contains(x.Id) && x.CompanyId == companyId && !x.IsDeleted)
        //            .ToList();
        //    }

        //    public string GetBuildingNameByRid(int companyId, string deviceAddress)
        //    {
        //        var device = _dbContext.IcuDevice.Where(x =>
        //            x.DeviceAddress.Equals(deviceAddress) && x.CompanyId == companyId && !x.IsDeleted).FirstOrDefault();
        //        if (device == null)
        //            return "";
        //        else
        //            return _dbContext.Building.Where(x =>
        //            x.Id == device.BuildingId && x.CompanyId == companyId && !x.IsDeleted).Select(x => x.Name).FirstOrDefault();
        //    }
    }
}