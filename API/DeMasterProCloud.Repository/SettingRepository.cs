﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Setting;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for Setting repository
    /// </summary>
    public interface ISettingRepository : IGenericRepository<Setting>
    {
        Setting GetByKey(string key, int companyId);
        List<Setting> GetByKeys(List<string> keys, int companyId);
        Setting GetLogo(int companyId);
        
        Setting GetLanguage(int companyId);
        bool IsKeyExist(SettingModel model);
        List<Setting> GetCompaniesWithAutoRenew();
        
        Setting GetCompaniesPeriodAutoRenew(int companyId);
    }

    /// <summary>
    /// Setting repository
    /// </summary>
    public class SettingRepository : GenericRepository<Setting>, ISettingRepository
    {
        private readonly AppDbContext _dbContext;
        public SettingRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get setting by key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public Setting GetByKey(string key, int companyId)
        {
            return Get(c => c.Key == key && c.CompanyId == companyId);
        }

        /// <summary>
        /// Get setting by keys
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public List<Setting> GetByKeys(List<string> keys, int companyId)
        {
            return GetMany(c => keys.Any(m =>m == c.Key) && c.CompanyId == companyId).ToList();
        }
        
        public Setting GetLogo(int companyId)
        {
            var logo = Get(c => c.Key == "logo" && c.CompanyId == companyId);
            return logo;
        }
        /// <summary>
        /// Check if key is exists
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsKeyExist(SettingModel model)
        {
            return _dbContext.Setting.Any(m => m.Key == model.Key && m.Id != model.Id);
        }

        public Setting GetLanguage(int companyId)
        {
            var setting = Get(c => c.Key == "language" && c.CompanyId == companyId);
            return setting;
        }
        
        public List<Setting> GetCompaniesWithAutoRenew()
        {
            var companies = GetMany(c => c.Key == "auto_renew_qr_code").ToList();
            return companies;
        }
        
        public Setting GetCompaniesPeriodAutoRenew(int companyId)
        {
            var companies = Get(c => c.Key == "qr_code_auto_renew_period" && c.CompanyId == companyId);
            return companies;
        }
    }
}