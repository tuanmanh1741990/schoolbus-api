﻿using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// UnregistedDevices repository interface
    /// </summary>
    public interface IUnregistedDeviceRepository : IGenericRepository<UnregistedDevice>
    {
        IQueryable<UnregistedDevice> GetByCompanyId(int companyId);
        UnregistedDevice GetByDeviceAddress(string deviceAddress);
        UnregistedDevice GetByMacAddress(string macAddr);
        UnregistedDevice GetByIdAndCompanyId(int companyId, int id);
    }

    /// <summary>
    /// UnregistedDevices repository
    /// </summary>
    public class UnregistedDeviceRepository : GenericRepository<UnregistedDevice>, IUnregistedDeviceRepository
    {
        private readonly AppDbContext _dbContext;
        public UnregistedDeviceRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get all unregisted devices by companyId
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<UnregistedDevice> GetByCompanyId(int companyId)
        {
            return _dbContext.UnregistedDevice.Where(x => x.CompanyId == companyId);
        }

        /// <summary>
        /// Get by device address and companyId
        /// </summary>
        /// <param name="deviceAddress"></param>
        /// <returns></returns>
        public UnregistedDevice GetByDeviceAddress(string deviceAddress)
        {
            return _dbContext.UnregistedDevice.FirstOrDefault(x =>
                x.DeviceAddress == deviceAddress);
        }

        /// <summary>
        /// Get by Mac address
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        public UnregistedDevice GetByMacAddress(string macAddr)
        {
            return _dbContext.UnregistedDevice.FirstOrDefault(x =>
                x.MacAddress == macAddr);
        }

        /// <summary>
        /// Get by id and companyId
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public UnregistedDevice GetByIdAndCompanyId(int companyId, int id)
        {
            return _dbContext.UnregistedDevice.FirstOrDefault(x =>
                x.Id == id && x.CompanyId == companyId);
        }
    }
}
