﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for Holiday repository
    /// </summary>
    public interface IHolidayRepository : IGenericRepository<Holiday>
    {
        List<Holiday> GetHolidayByCompany(int companyId);
        Holiday GetHolidayByIdAndCompany(int companyId, int holidayId);
        Holiday GetHolidayByNameAndCompany(int companyId, string name);
        int GetHolidayCount(int companyId);
    }

    /// <summary>
    /// Holiday repository
    /// </summary>
    public class HolidayRepository : GenericRepository<Holiday>, IHolidayRepository
    {
        public HolidayRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
        }

        /// <summary>
        /// Get holidays by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<Holiday> GetHolidayByCompany(int companyId)
        {
            if(companyId != 0)
            {
                return GetMany(c => c.CompanyId == companyId && !c.IsDeleted).ToList();
            }
            else
            {
                return GetMany(c => !c.IsDeleted).ToList();
            }
            
        }

        /// <summary>
        /// Get number of holiday in a company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public int GetHolidayCount(int companyId)
        {
            return GetMany(c => c.CompanyId == companyId && !c.IsDeleted)
                .Select(c => c.Id)
                .Count();
        }

        /// <summary>
        /// Get holiday by id and company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="holidayId"></param>
        /// <returns></returns>
        public Holiday GetHolidayByIdAndCompany(int companyId, int holidayId)
        {
            return Get(c => c.CompanyId == companyId && c.Id == holidayId
            && !c.IsDeleted);
        }

        /// <summary>
        /// Get holiday by name and company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public Holiday GetHolidayByNameAndCompany(int companyId, string name)
        {
            return Get(c => c.CompanyId == companyId && c.Name == name
                    && !c.IsDeleted);
        }
    }
}