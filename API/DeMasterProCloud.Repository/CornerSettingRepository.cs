﻿using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.CornerSetting;

namespace DeMasterProCloud.Repository
{
    public interface ICornerSettingRepository 
    {
        ReponseStatus AddCornerSetting(CornerSetting model);
        ReponseStatus UpdateCornerSetting(CornerSetting model);
        ReponseStatus DeleteCornerSetting(int id);
        List<CornerSettingModel> GetListCornerSetting(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered);
        CornerSettingModel GetCornerSetting(int id);

    }
    public class CornerSettingRepository : GenericRepository<CornerSettingModel>, ICornerSettingRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public CornerSettingRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            _httpContext = contextAccessor.HttpContext;
        }
        public ReponseStatus AddCornerSetting(CornerSetting model)
        {
            
            try
            {
                var lst = _dbContext.CornerSetting.Where(x => x.CompanyId == _httpContext.User.GetCompanyId() && x.Code == model.Code).FirstOrDefault();
                if(lst != null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }    

                CornerSetting cornerSetting = new CornerSetting();
                cornerSetting.Code = model.Code;
                cornerSetting.CompanyId = _httpContext.User.GetCompanyId();
                cornerSetting.Name = model.Name;
                cornerSetting.Description = model.Description;
                Add(cornerSetting);
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = cornerSetting.Id;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }

            return res;
        }
        public ReponseStatus UpdateCornerSetting(CornerSetting model)
        {
            
            try
            {
                var lst = _dbContext.CornerSetting.Where(x => x.CompanyId == _httpContext.User.GetCompanyId() && x.Code == model.Code && x.Id != model.Id).ToList();
                if (lst.Count > 0)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                CornerSetting cornerSetting = _dbContext.CornerSetting.Where(x => x.Id == model.Id).FirstOrDefault();
                cornerSetting.Code = model.Code;
                cornerSetting.Name = model.Name;
                cornerSetting.Description = model.Description;
                Update(cornerSetting);
                res.message = Constants.CornerSetting.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.UpdateFailed;
                res.statusCode = false;
            }

            return res;
        }
        public ReponseStatus DeleteCornerSetting(int id)
        {
           
            try
            {
                var cornerSetting = _dbContext.CornerSetting.Where(x => x.Id == id).FirstOrDefault();
                Remove(cornerSetting);
                res.message = Constants.CornerSetting.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.DeleteFailed;
                res.statusCode = false;
            }

            return res;
        }

        public List<CornerSettingModel> GetListCornerSetting(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var allData = _dbContext.CornerSetting.ToList();
            var data = (from a in allData join b in _dbContext.Company on a.CompanyId equals b.Id
                         select new CornerSettingModel
                         {
                             id = a.Id,
                             code = a.Code,
                             name =a.Name,
                             description = a.Description,
                             companyId = a.CompanyId,
                             companyName = b.Name
                         }).ToList();
            int companyId = _httpContext.User.GetCompanyId();
            if (_httpContext.User.GetAccountType() != (short)AccountType.SystemAdmin)
            {
                data = data.Where(m => m.companyId == companyId).ToList();
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                //data = data.Where(x => x.name.ToLower().Contains(filter.ToLower()) ||  x.description.ToLower().Contains(filter.ToLower())
                //               || x.companyName.ToLower().Contains(filter.ToLower()) || x.code.ToLower().Contains(filter.ToLower())).ToList();
                data = data.Where(x => x.name.ToLower().Contains(filter.ToLower())).ToList();
            }
            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.CornerSetting.Length - 1 ? 0 : sortColumn;

            data = data.AsQueryable().OrderBy($"{ColumnDefines.CornerSetting[sortColumn]} {sortDirection}").ToList();
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


            return data;
        }

        public CornerSettingModel GetCornerSetting(int id)
        {
            var cornerSetting = (from a in _dbContext.CornerSetting
                                 join b in _dbContext.Company on a.CompanyId equals b.Id
                                 select new CornerSettingModel
                                 {
                                     id = a.Id,
                                     companyId = a.CompanyId,
                                     name = a.Name,
                                     code = a.Code,
                                     description = a.Description,
                                     companyName = b.Name
                                 }).FirstOrDefault();
            return cornerSetting;
        }

        public void Add(CornerSetting model)
        {
            _dbContext.Add(model);
            _dbContext.SaveChanges();

        }
        public void Update(CornerSetting model)
        {
            _dbContext.Update(model);
            _dbContext.SaveChanges();

        }
        public void Remove(CornerSetting model)
        {
            _dbContext.CornerSetting.Remove(model);
            _dbContext.SaveChanges();
        }
    }
}
