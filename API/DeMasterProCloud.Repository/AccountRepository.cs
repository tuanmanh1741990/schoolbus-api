﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using System;
using Microsoft.Extensions.Options;
using DeMasterProCloud.DataModel.Api;

namespace DeMasterProCloud.Repository
{

    /// <summary>
    /// UserLogin repository interface
    /// </summary>
    public interface IAccountRepository : IGenericRepository<Account>
    {
        Account GetRootAccountByCompany(int companyId);
        Account AddOrUpdateDefaultAccount(Company company, string userName, string password);
        Account AddDefaultAccount(string userName, string password);
        bool IsExist(int id, string username, int companyId);
        Account GetByIdAndCompanyId(int companyId, int id);
        Account GetByUserNameAndCompanyId(int companyId, string username);
        void AddTokenAndRefreshToken( string refreshToken, Account model, int expiryRefreshToken);
        Account GetAccountByRefreshToken(string refreshToken);
        string GetRefreshTokenByUserName(string userName, int CompanyId);
    }

    /// <summary>
    /// UserLogin repository
    /// </summary>
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        private readonly AppDbContext _dbContext;
        public AccountRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get root account by company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Account GetRootAccountByCompany(int companyId)
        {
            return Get(c => c.CompanyId == companyId
            //&& c.RootFlag 
            && !c.IsDeleted);
        }

        /// <summary>
        /// Add or update a default account
        /// </summary>
        /// <param name="company"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public Account AddOrUpdateDefaultAccount(Company company, string userName, string password)
        {
            var account = Get(c => c.CompanyId == company.Id
            && c.RootFlag
            && !c.IsDeleted);
            if (account == null)
            {
                account = new Account
                {
                    Username = userName,
                    Password = SecurePasswordHasher.Hash(password),
                    CompanyId = company.Id,
                    Type = (short)AccountType.SuperAdmin,
                    RootFlag = true
                };
                Add(account);
            }
            else
            {
                if (!string.IsNullOrEmpty(password))
                {
                    account.Password = SecurePasswordHasher.Hash(password);
                }
                account.Username = userName;
            }

            return account;
        }

        /// <summary>
        /// Add or update a default account
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public Account AddDefaultAccount(string userName, string password)
        {
            var account = Get(c =>
            c.RootFlag
            && !c.IsDeleted);
            if (account == null)
            {
                account = new Account
                {
                    Username = userName,
                    Password = SecurePasswordHasher.Hash(password),
                    Type = (short)AccountType.SystemAdmin,
                    RootFlag = true
                };
                Add(account);
            }
            else
            {
                if (!string.IsNullOrEmpty(password))
                {
                    account.Password = SecurePasswordHasher.Hash(password);
                }
                account.Username = userName;
            }

            return account;
        }

        /// <summary>
        /// Get by account id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public new Account GetById(int accountId)
        {
            return _dbContext.Account.Include(c => c.Company)
                .FirstOrDefault(c => c.Id == accountId);
        }

        /// <summary>
        /// Check whether if account is exists
        /// </summary>
        /// <param name="id"></param>
        /// <param name="username"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public bool IsExist(int id, string username, int companyId)
        {
            var userLogin = Get(m =>
                m.Username == username && !m.IsDeleted &&
                m.CompanyId == companyId && m.Id != id);

            return userLogin != null;
        }

        /// <summary>
        /// Get by companyid and id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public Account GetByIdAndCompanyId(int companyId, int id)
        {
            return _dbContext.Account.AsNoTracking().FirstOrDefault(x => x.Id == id && x.CompanyId == companyId && !x.IsDeleted);
        }

        public Account GetByUserNameAndCompanyId(int companyId, string username)
        {
            return _dbContext.Account.AsNoTracking().FirstOrDefault(x => x.Username.Equals(username) && x.CompanyId == companyId && !x.IsDeleted);
        }

        public void AddTokenAndRefreshToken(string refreshToken, Account model, int expiryRefreshToken)
        {

            try
            {
                var accountLogin = _dbContext.Account.Where(x => x.CompanyId == model.CompanyId && x.Username == model.Username).Select(x => x.Id).FirstOrDefault();
                Account acc = _dbContext.Account.FirstOrDefault(item => item.Id == accountLogin);
                var createDateRefreshToken = acc.CreateDateRefreshToken;
                if(createDateRefreshToken == null)
                {
                    createDateRefreshToken = DateTime.Now;
                }    
                if(acc.RefreshToken == "" || acc.RefreshToken == null || createDateRefreshToken.AddMonths(expiryRefreshToken) < DateTime.Now)
                {
                    acc.RefreshToken = refreshToken;
                    acc.CreateDateRefreshToken = DateTime.Now;
                }    
               
                
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

        }

        public Account GetAccountByRefreshToken(string refreshToken)
        {
            return _dbContext.Account.AsNoTracking().FirstOrDefault(x => x.RefreshToken == refreshToken);
        }

        public string GetRefreshTokenByUserName(string userName,int CompanyId)
        {
            if(CompanyId == 0)
                return _dbContext.Account.AsNoTracking().FirstOrDefault(x => x.Username == userName && x.CompanyId == null).RefreshToken;
            else
                return _dbContext.Account.AsNoTracking().FirstOrDefault(x => x.Username == userName && x.CompanyId == CompanyId).RefreshToken;
        }
    }
}