﻿using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Repository
{
    public interface ICategoryOptionRepository : IGenericRepository<CategoryOption>
    {
        IQueryable<CategoryOption> GetByIds(List<int> optionsIds);
        IQueryable<CategoryOption> GetOptionByCategoryId(int categoryId);
        IQueryable<CategoryOption> GetOptionByCategoryIds(List<int> categoryIds);
        IQueryable<CategoryOption> GetOptionByParentId(int parentOptionId);
    }

    public class CategoryOptionRepository : GenericRepository<CategoryOption>, ICategoryOptionRepository
    {
        private readonly AppDbContext _dbContext;

        public CategoryOptionRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }

        public IQueryable<CategoryOption> GetByIds(List<int> optionsIds)
        {
            var options = _dbContext.CategoryOption.Where(m => optionsIds.Contains(m.Id));

            return options;
        }

        public IQueryable<CategoryOption> GetOptionByCategoryId(int categoryId)
        {
            var options = _dbContext.CategoryOption.Where(m => m.CategoryId == categoryId);

            return options;
        }

        public IQueryable<CategoryOption> GetOptionByCategoryIds(List<int> categoryIds)
        {
            var options = _dbContext.CategoryOption.Where(m => categoryIds.Contains(m.CategoryId));

            return options;
        }

        public IQueryable<CategoryOption> GetOptionByParentId(int parentOptionId)
        {
            var options = _dbContext.CategoryOption.Where(m => m.ParentOptionId == parentOptionId);

            return options;
        }
    }
}