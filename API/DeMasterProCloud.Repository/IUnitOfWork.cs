﻿using System;
using System.Threading.Tasks;
using DeMasterProCloud.DataAccess.Models;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Unit of work interface
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository UserRepository { get; }
        IAccountRepository AccountRepository { get; }
        IDepartmentRepository DepartmentRepository { get; }
        IIcuDeviceRepository IcuDeviceRepository { get; }
        ITimezoneRepository TimezoneRepository { get; }
        IHolidayRepository HolidayRepository { get; }
        ICompanyRepository CompanyRepository { get; }
        ISystemLogRepository SystemLogRepository { get; }
        IEventRepository EventRepository { get; }
        IEventLogRepository EventLogRepository { get; }
        ISettingRepository SettingRepository { get; }
        IAccessGroupRepository AccessGroupRepository { get; }
        IAccessGroupDeviceRepository AccessGroupDeviceRepository { get; }
        IBuildingRepository BuildingRepository { get; }
        IPartTimeRepository PartTimeRepository { get; }
        IUnregistedDeviceRepository UnregistedDevicesRepository { get; }

        

        IVisitRepository VisitRepository { get; }
        ICardRepository CardRepository { get; }
        IDeviceMessageRepository DeviceMessageRepository { get; }
        
        IWorkingRepository WorkingRepository { get; }
        
        IAttendanceRepository AttendanceRepository { get; }
        IPlugInRepository PlugInRepository { get; }

        ICategoryRepository CategoryRepository { get; }

        ICategoryOptionRepository CategoryOptionRepository { get; }

        IUserCategoryOptionRepository UserCategoryOptionRepository { get; }

        IUserArmyRepository UserArmyRepository { get; }
        IVisitArmyRepository VisitArmyRepository { get; }

        ICornerSettingRepository CornerSettingRepository { get; }
        IUserDiscountRepository UserDiscountRepository { get; }
        IMealSettingRepository MealSettingRepository { get; }
        IExceptionalMealRepository ExceptionalMealRepository { get; }
        IMealTypeRepository MealTypeRepository { get; }
        IMealEventLogRepository MealEventLogRepository { get; }
        AppDbContext AppDbContext { get; }
        void Save();
        Task SaveAsync();
    }
}