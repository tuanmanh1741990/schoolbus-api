﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Device;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// User repository interface
    /// </summary>
    public interface IUserRepository : IGenericRepository<User>
    {
        IQueryable<User> GetValidUser(int companyId);
        List<User> GetUsersByCardId(int? companyId, List<string> cardIds);
        IQueryable<User> GetAssignUsers(int companyId, int accessGroupId);
        IQueryable<User> GetAssignUsersByAccessGroupIds(int companyId, List<int> accessGroupIds);
        int GetCountUserMasterCard(int companyId, List<int> accessGroupIds);
        IQueryable<User> GetUserMasterCard(int companyId, List<int> accessGroupIds);
        IQueryable<User> GetUserMasterCardIncludeCard(int companyId, List<int> accessGroupIds);
        User GetRandomUser(int companyId);
        IQueryable<User> GetUnAssignUsers(int companyId, int accessGroupId);
        IQueryable<User> GetUnAssignUsersForMasterCard(int companyId, int accessGroupId);
        User GetByCardId(int? companyId, string cardId);
        User GetByKeyPadPw(int companyId, string keyPadPw);
        List<User> GetByIds(int companyId, List<int> userIds);
        List<User> GetByDepartmentId(int departmentId);

        //bool IsUserCodeExist(int companyId, UserModel model);
        bool IsKeyPadPasswordExist(int companyId, int userId, string enctypedKeyPadPassword);
        List<User> GetByKeyPadPws(int companyId, List<string> keyPadPws);
        User GetByCardIdIncludeDepartment(int? companyId, string cardId);
        IQueryable<User> GetByCompanyId(int companyId);

        
        IQueryable<User> GetUserByCompany(int companyId);
        User GetByUserId(int companyId, int userId);
        
        int GetUserById(int userId);
        int GetNewUserCode(int companyId);
        int GetMaxId(int companyId);

        User GetByUserCode(int companyId, string userCode);
        User GetByAccountId(int companyId, int accountId);
        List<User> GetAllUserInSystem();
        List<User> GetAllUserInSystemNoWorkingTime();
        int GetCountByDepartmentId(int companyId, int departmentId);

        User GetUserByEmail(int companyId, string emailAddress);

    }

    /// <summary>
    /// User repository
    /// </summary>
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _dbContext;
        public UserRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get valid user
        /// </summary>
        /// <returns></returns>
        public IQueryable<User> GetValidUser(int companyId)
        {
            return _dbContext.User.Include(m => m.Department).Where(m =>
                m.CompanyId == companyId && (m.ExpiredDate == null ||
                                             m.ExpiredDate.Value.Date.Subtract(DateTime.Now.Date).Days >= 0) && !m.IsDeleted);
        }

        /// <summary>
        /// Get users by card id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="cardIds"></param>
        /// <returns></returns>
        public List<User> GetUsersByCardId(int? companyId, List<string> cardIds)
        {
            var userIds = _dbContext.Card.Where(c => cardIds.Contains(c.CardId) && c.CompanyId == companyId && !c.IsDeleted).Select(u => u.UserId)
                .ToList();
            return _dbContext.User.AsNoTracking().Where(c =>
                userIds.Contains(c.Id) && c.CompanyId == companyId && !c.IsDeleted).ToList();
        }

        /// <summary>
        /// Get user by list of user id in company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public List<User> GetByIds(int companyId, List<int> userIds)
        {
            return _dbContext.User.Where(m =>
                    userIds.Any(c => c == m.Id) && m.CompanyId == companyId && !m.IsDeleted)
                .Include(c => c.Department).Include(c => c.AccessGroup).ThenInclude(c => c.AccessGroupDevice)
                .ThenInclude(c => c.Icu).ToList();
        }

        /// <summary>
        /// Get user by card id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public User GetByCardId(int? companyId, string cardId)
        {
            var userId = _dbContext.Card.Where(c => cardId.Equals(c.CardId) && c.CompanyId == companyId && !c.IsDeleted)
                .Select(u => u.UserId).FirstOrDefault();
                
            
            return _dbContext.User.FirstOrDefault(c =>
                c.Id == userId && c.CompanyId == companyId && !c.IsDeleted);
        }

        public User GetRandomUser(int companyId)
        {
            return _dbContext.User.FirstOrDefault(c =>c.CompanyId == companyId && !c.IsDeleted);
        }

        /// <summary>
        /// Get user by department id
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public List<User> GetByDepartmentId(int departmentId)
        {
            return _dbContext.User.Where(c => c.DepartmentId == departmentId && !c.IsDeleted).ToList();
        }

        /// <summary>
        /// Get user by card id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public User GetByCardIdIncludeDepartment(int? companyId, string cardId)
        {
            var userId = _dbContext.Card.Where(c => cardId.Equals(c.CardId) && c.CompanyId == companyId && !c.IsDeleted)
                .Select(u => u.UserId).ToList().FirstOrDefault();

            return _dbContext.User.Include(c => c.Department).FirstOrDefault(c =>
                c.Id == userId && c.CompanyId == companyId && !c.IsDeleted);
        }

        /// <summary>
        /// Get user by keypad password
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="keyPadPw"></param>
        /// <returns></returns>
        public User GetByKeyPadPw(int companyId, string keyPadPw)
        {
            return _dbContext.User.AsNoTracking().FirstOrDefault(c =>
                c.KeyPadPw == keyPadPw && c.CompanyId == companyId && !c.IsDeleted);
        }

        /// <summary>
        /// Get users by card id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="keyPadPws"></param>
        /// <returns></returns>
        public List<User> GetByKeyPadPws(int companyId, List<string> keyPadPws)
        {
            return _dbContext.User.AsNoTracking().Where(c =>
                    keyPadPws.Any(m => m == c.KeyPadPw) && c.CompanyId == companyId && !c.IsDeleted)
                .ToList();
        }

        

        /// <summary>
        /// Check whether keypad password is existed in the company
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <param name="enctypedKeyPadPassword"></param>
        /// <returns></returns>
        public bool IsKeyPadPasswordExist(int companyId, int userId, string enctypedKeyPadPassword)
        {
            if (!string.IsNullOrEmpty(enctypedKeyPadPassword))
            {
                return Get(m =>
                           m.KeyPadPw == enctypedKeyPadPassword && m.CompanyId == companyId &&
                           m.Id != userId && !m.IsDeleted) != null;
            }

            return false;
        }

        /// <summary>
        /// Get user by access group
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public IQueryable<User> GetAssignUsers(int companyId, int accessGroupId)
        {
            return _dbContext.User.Include(m => m.Department)
                .Where(m =>  m.CompanyId == companyId &&
                            m.AccessGroupId == accessGroupId && !m.IsDeleted);
        }

        /// <summary>
        /// Get user by access groups
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupIds"></param>
        /// <returns></returns>
        public IQueryable<User> GetAssignUsersByAccessGroupIds(int companyId, List<int> accessGroupIds)
        {
            var data = _dbContext.User.Include(m => m.Department)
                .Where(u => accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted);

            if(companyId != 0)
            {
                data = data.Where(u => u.CompanyId == companyId);
            }

            //return _dbContext.User.Include(m => m.Department)
            //    .Where(u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted);

            return data;
        }

        /// <summary>
        /// Get user by access groups
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupIds"></param>
        /// <returns></returns>
        public int GetCountUserMasterCard(int companyId, List<int> accessGroupIds)
        {
            return _dbContext.User.Count(
                    u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted && u.IsMasterCard);
        }

        /// <summary>
        /// get user master card
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupIds"></param>
        /// <returns></returns>
        public IQueryable<User> GetUserMasterCard(int companyId, List<int> accessGroupIds)
        {
            var data = _dbContext.User.Where(u => accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted && u.IsMasterCard);

            if(companyId != 0)
            {
                data = data.Where(u => u.CompanyId == companyId);
            }

            //return _dbContext.User.Where(
            //    u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted && u.IsMasterCard);

            return data;
        }


        public IQueryable<User> GetUserMasterCardIncludeCard(int companyId, List<int> accessGroupIds)
        {
            

            return _dbContext.User.Include(u => u.Card).Where(
                u => u.CompanyId == companyId && accessGroupIds.Any(a => a == u.AccessGroupId) && !u.IsDeleted && u.IsMasterCard);
        }

        

        /// <summary>
        /// Get list user not contain list userids
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public IQueryable<User> GetUnAssignUsers(int companyId, int accessGroupId)
        {
            return _dbContext.User.Include(c => c.Department).Include(c => c.AccessGroup).Where(m =>
                m.CompanyId == companyId && m.AccessGroupId != accessGroupId && !m.IsDeleted && !m.IsMasterCard);
        }

        /// <summary>
        /// Get unassign with mastercard
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public IQueryable<User> GetUnAssignUsersForMasterCard(int companyId, int accessGroupId)
        {
            return _dbContext.User.Include(c => c.Department).Include(c => c.AccessGroup).Where(m =>
                m.CompanyId == companyId && m.AccessGroupId != accessGroupId && !m.IsDeleted);
        }

        /// <summary>
        /// Get by id and company
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<User> GetByCompanyId(int companyId)
        {
            var data = _dbContext.User.Include(m => m.Department).Include(m => m.AccessGroup)
                .Where(m => m.Status != (short)UserStatus.NotUse && !m.IsDeleted);

            if (companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            //return _dbContext.User.Include(m => m.Department).Include(m => m.AccessGroup)
            //    .Where(m => m.CompanyId == companyId && m.Status != (short)UserStatus.NotUse && !m.IsDeleted);

            return data;
        }

        /// <summary>
        /// Get invalid user
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<User> GetUserByCompany(int companyId)
        {
            var data = _dbContext.User.Include(m => m.Department).Include(m => m.AccessGroup)
                .Where(m => !m.IsDeleted);

            if(companyId != 0)
            {
                data = data.Where(m => m.CompanyId == companyId);
            }

            //return _dbContext.User.Include(m => m.Department).Include(m => m.AccessGroup)
            //    .Where(m => m.CompanyId == companyId && !m.IsDeleted);

            return data;
        }

        /// <summary>
        /// Get by companyid and userid
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public User GetByUserId(int companyId, int userId)
        {
            return _dbContext.User.Include(m => m.AccessGroup).FirstOrDefault(m =>
                m.Id == userId && m.CompanyId == companyId && !m.IsDeleted);
        }

        public int GetNewUserCode(int companyId)
        {
            if (_dbContext.User.Count() == 0)
                return 1;
            if (_dbContext.User.Where(m => m.CompanyId == companyId && !m.IsDeleted).ToList().Count == 0)
                return 1;

            //int userCode = _dbContext.User.Where(m => m.CompanyId == companyId && !m.IsDeleted).Max(m => m.Id) + 1;

            int userCode = _dbContext.User.Where(m => m.CompanyId == companyId).Count() + 1;

            while (true) {
                string str_UserCode = String.Format("{0:000000}", userCode);

                var userCount = _dbContext.User.Where(m => m.CompanyId == companyId && m.UserCode.Equals(str_UserCode)).Count();

                if (userCount > 0)
                    userCode += 1;
                else
                    return userCode;

            }

            //return _dbContext.User.Where(m => m.CompanyId == companyId && !m.IsDeleted).Max(m => m.Id) + 1;
        }

        public int GetMaxId(int companyId)
        {
            return _dbContext.User.Where(m =>
                m.CompanyId == companyId).Select(m => m.Id).Max();
        }


        public User GetByUserCode(int companyId, string userCode)
        {
            //var userId = _dbContext.Card.Where(c => cardId.Equals(c.CardId) && c.CompanyId == companyId && !c.IsDeleted)
            //    .Select(u => u.UserId).ToList().FirstOrDefault();


            return _dbContext.User.FirstOrDefault(c =>
                c.UserCode.Equals(userCode) && c.CompanyId == companyId && !c.IsDeleted);
            
        }
        
        public int GetUserById(int userId)
        {
            Console.WriteLine(userId);
            return _dbContext.User.FirstOrDefault(c => c.Id == userId && !c.IsDeleted).CompanyId;
        }

        public List<User> GetAllUserInSystem()
        {
            return _dbContext.User.Include(m => m.WorkingType)
                .Where(m => !m.IsDeleted && m.WorkingTypeId != null).ToList();
        }

        public List<User> GetAllUserInSystemNoWorkingTime()
        {
            return _dbContext.User.Include(m => m.WorkingType)
                .Where(m => !m.IsDeleted && m.WorkingTypeId == null).ToList();
        }
        
        public User GetByAccountId(int companyId, int accountId)
        {
            //var userId = _dbContext.Card.Where(c => cardId.Equals(c.CardId) && c.CompanyId == companyId && !c.IsDeleted)
            //    .Select(u => u.UserId).ToList().FirstOrDefault();


            return _dbContext.User.FirstOrDefault(c => c.AccountId == accountId && c.CompanyId == companyId && !c.IsDeleted);
            
        }

        /// <summary>   Gets user count by department identifier. </summary>
        /// <remarks>   Edward, 2020-03-17. </remarks>
        /// <param name="companyId">    Identifier for the company. </param>
        /// <param name="departmentId"> Identifier for the department. </param>
        /// <returns>   The user count by department identifier. </returns>
        public int GetCountByDepartmentId(int companyId, int departmentId)
        {
            var count = _dbContext.User.Where(c => c.DepartmentId == departmentId && c.CompanyId == companyId && !c.IsDeleted).Count();

            return count;
        }

        /// <summary>   Gets user by email. </summary>
        /// <remarks>   Edward, 2020-03-17. </remarks>
        /// <param name="companyId">    Identifier for the company. </param>
        /// <param name="emailAddress"> The email address. </param>
        /// <returns>   The user by email. </returns>
        public User GetUserByEmail(int companyId, string emailAddress)
        {
            var user = _dbContext.User.Where(c => c.CompanyId == companyId && c.Email.Equals(emailAddress) && !c.IsDeleted).FirstOrDefault();

            return user;
        }
    }
}