using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.WorkingModel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DeMasterProCloud.Repository
{
    public interface IAttendanceRepository : IGenericRepository<Attendance>
    {
        void AddNewAttendance(int userId, int companyId, DateTime today, string workingTime, DateTime start, DateTime end, int type);

        Attendance GetAttendanceAlreadyCreated(int userId, int companyId, DateTime today);
        bool CheckAttendanceAlreadyCreated(int userId, int companyId, DateTime today);
        
        Attendance GetAttendanceByIdAndCompanyId(int companyId, int attendanceId);
    }

    public class AttendanceRepository : GenericRepository<Attendance>, IAttendanceRepository
    {
        private readonly AppDbContext _dbContext;
        public AttendanceRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }
        
        public void AddNewAttendance(int userId, int companyId, DateTime today, string workingTime, DateTime start, DateTime end, int type)
        {
            var attendance = new Attendance
            {
                UserId = userId,
                CompanyId = companyId,
                Date = today,
                WorkingTime = workingTime,
                StartD = start,
                EndD = end,
                Type = type
            };
            Add(attendance);
        }

        public bool CheckAttendanceAlreadyCreated(int userId, int companyId, DateTime today)
        {
            return Get(m =>
                       m.UserId == userId && m.Date == today && m.CompanyId ==  companyId) != null;
        }
        
        public Attendance GetAttendanceAlreadyCreated(int userId, int companyId, DateTime today)
        {
            return _dbContext.Attendance.Where(a => a.UserId == userId && a.CompanyId == companyId && a.Date == today).FirstOrDefault();
        }
        
        public Attendance GetAttendanceByIdAndCompanyId(int companyId, int attendanceId)
        {
            return Get(m =>
                       m.Id == attendanceId && m.CompanyId ==  companyId);
        }
        
    }
}
