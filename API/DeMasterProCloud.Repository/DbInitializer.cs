﻿using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// the class to seed date to database server when application start up.
    /// </summary>
    public static class DbInitializer
    {
        public static void Initialize(IUnitOfWork unitOfWork, IConfiguration configuration)
        {
            //Database migrate
            unitOfWork.AppDbContext.Database.Migrate();

            // Add a event list
            unitOfWork.EventRepository.AddDefaultEventList();

            unitOfWork.Save();

            // Look for any company.
            if (!unitOfWork.AppDbContext.Company.Any())
            {
                // Add a system admin as a default account.
                unitOfWork.AccountRepository.AddDefaultAccount(
                    configuration[Constants.Settings.DefaultAccountUsername],
                    configuration[Constants.Settings.DefaultAccountPassword]);



                unitOfWork.Save();
            }
        }
    }
}
