﻿using Microsoft.AspNetCore.Http;
using DeMasterProCloud.DataAccess.Models;
using System.Linq;
using System.Collections.Generic;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// UserArmy repository interface
    /// </summary>
    public interface IUserArmyRepository : IGenericRepository<UserArmy>
    {
        IQueryable<UserArmy> GetArmyByUserId(int userId);
        IQueryable<UserArmy> GetArmysByUserIds(List<int> userIds);
    }

    /// <summary>
    /// UserArmy repository
    /// </summary>
    public class UserArmyRepository : GenericRepository<UserArmy>, IUserArmyRepository
    {
        private readonly AppDbContext _dbContext;
        public UserArmyRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }
        
        /// <summary>
        /// Get UserArmy by UserId
        /// </summary>
        /// <param name="userId"> Identifier of user </param>
        /// <returns></returns>
        public IQueryable<UserArmy> GetArmyByUserId(int userId)
        {
            if(userId == 0)
            {
                return null;
            }

            var data = _dbContext.UserArmy.Where(m => m.UserId == userId);

            return data;
        }

        public IQueryable<UserArmy> GetArmysByUserIds(List<int> userIds)
        {
            if(userIds == null || !userIds.Any())
            {
                return null;
            }

            var data = _dbContext.UserArmy.Where(m => userIds.Contains(m.UserId));

            return data;
        }
    }
}