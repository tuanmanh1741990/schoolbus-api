﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Visit;
using AutoMapper;
using Newtonsoft.Json.Linq;
using DeMasterProCloud.DataModel.EventLog;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// User repository interface
    /// </summary>
    public interface IVisitArmyRepository : IGenericRepository<VisitArmy>
    {
        IQueryable<VisitArmy> GetArmyByVisitId(int visitId);
    }

    /// <summary>
    /// VisitArmy repository
    /// </summary>
    public class VisitArmyRepository : GenericRepository<VisitArmy>, IVisitArmyRepository
    {
        private readonly AppDbContext _dbContext;
        public VisitArmyRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }


        public IQueryable<VisitArmy> GetArmyByVisitId(int visitId)
        {
            if (visitId == 0)
            {
                return null;
            }

            var data = _dbContext.VisitArmy.Where(m => m.VisitId == visitId);

            return data;
        }
    }
}