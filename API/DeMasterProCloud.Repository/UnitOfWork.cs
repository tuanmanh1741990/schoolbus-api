﻿using System;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.Extensions.Options;
using DeMasterProCloud.DataModel.Api;

namespace DeMasterProCloud.Repository
{
    /// <inheritdoc />
    /// <summary>
    /// Unit of work class
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        #region inject field variables
        private readonly AppDbContext _appDbContext;
        #endregion

        #region data members
        private readonly IHttpContextAccessor _httpContext;
        private IUserRepository _userRepository;
        private IAccountRepository _accountRepository;
        private IDepartmentRepository _departmentRepository;
        private IIcuDeviceRepository _icuDeviceRepository;
        private ITimezoneRepository _timezoneRepository;
        private IHolidayRepository _holidayRepository;
        private ICompanyRepository _companyRepository;
        private ISystemLogRepository _systemLogRepository;
        private IEventRepository _eventRepository;
        private IEventLogRepository _eventLogRepository;
        private ISettingRepository _settingRepository;
        private IAccessGroupRepository _accessGroupRepository;
        private IAccessGroupDeviceRepository _accessGroupDeviceRepository;
        private IUnregistedDeviceRepository _unregistedDevicesRepository;
        private IBuildingRepository _buildingRepository;
        private IVisitRepository _visitRepository;
        private ICardRepository _cardRepository;
        private IDeviceMessageRepository _deviceMessageRepository;
        private IPartTimeRepository _partTimeRepository;
        private IWorkingRepository _workingRepository;
        private IAttendanceRepository _attendanceRepository;
        private IPlugInRepository _plugInRepository;
        private ICategoryRepository _categoryRepository;
        private ICategoryOptionRepository _categoryOptionRepository;
        private IUserCategoryOptionRepository _userCategoryOptionRepository;
        private IUserArmyRepository _userArmyRepository;
        private IVisitArmyRepository _visitArmyRepository;
        private ICornerSettingRepository _cornerSettingRepository;
        private IUserDiscountRepository _userDiscountRepository;
        private IMealSettingRepository _mealSettingRepository;
        private IExceptionalMealRepository _exceptionalMealRepository;
        private IMealTypeRepository _mealTypeRepository;
        private IMealEventLogRepository _mealEventLogRepository;
        #endregion

        /// <summary>
        /// Unit of work constructor
        /// </summary>
        /// <param name="appDbContext"></param>
        /// <param name="contextAccessor"></param>
        public UnitOfWork(AppDbContext appDbContext, IHttpContextAccessor contextAccessor)
        {
            _appDbContext = appDbContext;
            _httpContext = contextAccessor;
        }

        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        #region Properties
        /// <summary>
        /// Get AppDbContext
        /// </summary>
        public AppDbContext AppDbContext => _appDbContext;
        /// <summary>
        /// Get UserRepository
        /// </summary>
        public IUserRepository UserRepository
        {
            get
            {
                return _userRepository =
                    _userRepository ?? new UserRepository(_appDbContext, _httpContext);
            }
        }

        /// <summary>
        /// Get UserLoginRepository
        /// </summary>
        public IAccountRepository AccountRepository
        {
            get
            {
                return _accountRepository = _accountRepository ??
                                              new AccountRepository(
                                                  _appDbContext, _httpContext);
            }
        }

        public IDepartmentRepository DepartmentRepository
        {
            get
            {
                return _departmentRepository =
                    _departmentRepository ?? new DepartmentRepository(_appDbContext, _httpContext);
            }
        }

        public IIcuDeviceRepository IcuDeviceRepository
        {
            get
            {
                return _icuDeviceRepository =
                    _icuDeviceRepository ?? new IcuDeviceRepository(_appDbContext, _httpContext);
            }
        }

        public IUnregistedDeviceRepository UnregistedDevicesRepository
        {
            get
            {
                return _unregistedDevicesRepository =
                    _unregistedDevicesRepository ?? new UnregistedDeviceRepository(_appDbContext, _httpContext);
            }
        }

        public ITimezoneRepository TimezoneRepository
        {
            get
            {
                return _timezoneRepository =
                    _timezoneRepository ?? new TimezoneRepository(_appDbContext, _httpContext);
            }
        }

        public IHolidayRepository HolidayRepository
        {
            get
            {
                return _holidayRepository =
                    _holidayRepository ?? new HolidayRepository(_appDbContext, _httpContext);
            }
        }

        public ICompanyRepository CompanyRepository
        {
            get
            {
                return _companyRepository =
                    _companyRepository ?? new CompanyRepository(_appDbContext, _httpContext);
            }
        }

        public ISystemLogRepository SystemLogRepository
        {
            get
            {
                return _systemLogRepository =
                    _systemLogRepository ?? new SystemLogRepository(_appDbContext, _httpContext);
            }
        }

        public IEventRepository EventRepository
        {
            get
            {
                return _eventRepository =
                    _eventRepository ?? new EventRepository(_appDbContext, _httpContext);
            }
        }

        public IEventLogRepository EventLogRepository
        {
            get
            {
                return _eventLogRepository =
                    _eventLogRepository ?? new EventLogRepository(_appDbContext, _httpContext);
            }
        }
        public ISettingRepository SettingRepository
        {
            get
            {
                return _settingRepository =
                    _settingRepository ?? new SettingRepository(_appDbContext, _httpContext);
            }
        }

        public IAccessGroupRepository AccessGroupRepository
        {
            get
            {
                return _accessGroupRepository =
                    _accessGroupRepository ?? new AccessGroupRepository(_appDbContext, _httpContext);
            }
        }

        public IAccessGroupDeviceRepository AccessGroupDeviceRepository
        {
            get
            {
                return _accessGroupDeviceRepository =
                    _accessGroupDeviceRepository ?? new AccessGroupDeviceRepository(_appDbContext, _httpContext);
            }
        }


        public IBuildingRepository BuildingRepository
        {
            get
            {
                return _buildingRepository =
                    _buildingRepository ?? new BuildingRepository(_appDbContext, _httpContext);
            }
        }

        public IVisitRepository VisitRepository
        {
            get
            {
                return _visitRepository =
                    _visitRepository ?? new VisitRepository(_appDbContext, _httpContext);
            }
        }

        public IDeviceMessageRepository DeviceMessageRepository
        {
            get
            {
                return _deviceMessageRepository =
                    _deviceMessageRepository ?? new DeviceMessageRepository(_appDbContext, _httpContext);
            }
        }

        public ICardRepository CardRepository
        {
            get
            {
                return _cardRepository =
                    _cardRepository ?? new CardRepository(_appDbContext, _httpContext);
            }
        }

        public IPartTimeRepository PartTimeRepository
        {
            get
            {
                return _partTimeRepository =
                    _partTimeRepository ?? new PartTimeRepository(_appDbContext, _httpContext);
            }
        }

        public IWorkingRepository WorkingRepository
        {
            get
            {
                return _workingRepository =
                    _workingRepository ?? new WorkingRepository(_appDbContext, _httpContext);
            }
        }

        public IAttendanceRepository AttendanceRepository
        {
            get
            {
                return _attendanceRepository = _attendanceRepository ?? new AttendanceRepository(_appDbContext, _httpContext);
            }
        }

        public IPlugInRepository PlugInRepository
        {
            get
            {
                return _plugInRepository = _plugInRepository ?? new PlugInRepository(_appDbContext, _httpContext);
            }
        }

        public ICategoryRepository CategoryRepository
        {
            get
            {
                return _categoryRepository = _categoryRepository ?? new CategoryRepository(_appDbContext, _httpContext);
            }
        }

        public ICategoryOptionRepository CategoryOptionRepository
        {
            get
            {
                return _categoryOptionRepository = _categoryOptionRepository ?? new CategoryOptionRepository(_appDbContext, _httpContext);
            }
        }

        public IUserCategoryOptionRepository UserCategoryOptionRepository
        {
            get
            {
                return _userCategoryOptionRepository = _userCategoryOptionRepository ?? new UserCategoryOptionRepository(_appDbContext, _httpContext);
            }
        }

        public IUserArmyRepository UserArmyRepository
        {
            get
            {
                return _userArmyRepository = _userArmyRepository ?? new UserArmyRepository(_appDbContext, _httpContext);
            }
        }

        public IVisitArmyRepository VisitArmyRepository
        {
            get
            {
                return _visitArmyRepository = _visitArmyRepository ?? new VisitArmyRepository(_appDbContext, _httpContext);
            }
        }


        public IMealEventLogRepository MealEventLogRepository
        {
            get
            {
                return _mealEventLogRepository = _mealEventLogRepository ?? new MealEventLogRepository(_appDbContext, _httpContext);
            }
        }

        #endregion
        #region Methods


        public ICornerSettingRepository CornerSettingRepository
        {
            get
            {
                return _cornerSettingRepository = _cornerSettingRepository ?? new CornerSettingRepository(_appDbContext, _httpContext);
            }
        }

        public IUserDiscountRepository UserDiscountRepository
        {
            get
            {
                return _userDiscountRepository = _userDiscountRepository ?? new UserDiscountRepository(_appDbContext, _httpContext);
            }
        }

        public IMealSettingRepository MealSettingRepository
        {
            get
            {
                return _mealSettingRepository = _mealSettingRepository ?? new MealSettingRepository(_appDbContext, _httpContext);
            }
        }

        public IExceptionalMealRepository ExceptionalMealRepository
        {
            get
            {
                return _exceptionalMealRepository = _exceptionalMealRepository ?? new ExceptionalMealRepository(_appDbContext, _httpContext);
            }
        }

        public IMealTypeRepository MealTypeRepository
        {
            get
            {
                return _mealTypeRepository = _mealTypeRepository ?? new MealTypeRepository(_appDbContext, _httpContext);
            }
        }

        /// <summary>
        /// Save
        /// </summary>
        public void Save()
        {
            _appDbContext.SaveChanges();
        }
        /// <summary>
        /// Save Async
        /// </summary>
        public async Task SaveAsync()
        {
            await _appDbContext.SaveChangesAsync();
        }
        #endregion

        #region dispose
        private bool _disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _appDbContext.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}