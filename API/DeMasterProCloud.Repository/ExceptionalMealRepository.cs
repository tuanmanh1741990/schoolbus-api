﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.ExceptionalMeal;
using DeMasterProCloud.DataModel.MealSetting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;

namespace DeMasterProCloud.Repository
{
    public interface IExceptionalMealRepository
    {
        ReponseStatus AddExceptionalMeal(ExceptionalMeal model);
        ReponseStatus UpdateExceptionalMeal(ExceptionalMeal model);
        ReponseStatus DeleteExceptionalMeal(int id);
        List<ExceptionalMealModel> GetListExceptionalMeal(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered);
        ExceptionalMealModel GetExceptionalMeal(int id);
        ExceptionalMealModel ListExceptionalMealByDateTime(int companyId, int icuId, string accessTime);

    }
    public class ExceptionalMealRepository : GenericRepository<ExceptionalMealModel>, IExceptionalMealRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public ExceptionalMealRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            _httpContext = contextAccessor.HttpContext;
        }

        public ReponseStatus AddExceptionalMeal(ExceptionalMeal model)
        {

            try
            {
                var lst = _dbContext.ExceptionalMeal.Where(x => x.MealSettingId == model.MealSettingId).ToList();
                bool result = false;
                foreach (var item in lst)
                {
                    if ((item.Start <= model.Start && model.Start <= item.End) || (item.Start <= model.End && model.End <= item.End))
                    {
                        result = true;
                    }
                }
                if (result)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }

                ExceptionalMeal exceptionalMeal = new ExceptionalMeal();
                exceptionalMeal.Start = model.Start;
                exceptionalMeal.End = model.End;
                exceptionalMeal.Price = model.Price;
                exceptionalMeal.MealSettingId = model.MealSettingId;
                Add(exceptionalMeal);
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = exceptionalMeal.Id;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }

            return res;
        }


        public ReponseStatus UpdateExceptionalMeal(ExceptionalMeal model)
        {

            try
            {
                ExceptionalMeal exceptionalMeal = _dbContext.ExceptionalMeal.Where(x => x.Id == model.Id).FirstOrDefault();
                exceptionalMeal.Start = model.Start;
                exceptionalMeal.End = model.End;
                exceptionalMeal.Price = model.Price;
                Update(exceptionalMeal);
                res.message = Constants.CornerSetting.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.UpdateFailed;
                res.statusCode = false;
            }

            return res;
        }

        public ReponseStatus DeleteExceptionalMeal(int id)
        {
            try
            {
                var exceptionalMeal = _dbContext.ExceptionalMeal.Where(x => x.Id == id).FirstOrDefault();
                Remove(exceptionalMeal);
                res.message = Constants.CornerSetting.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.DeleteFailed;
                res.statusCode = false;
            }

            return res;
        }


        public List<ExceptionalMealModel> GetListExceptionalMeal(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            int companyId = _httpContext.User.GetCompanyId();
            var lstIcuDeviceByCompany = _dbContext.IcuDevice.Where(x => x.CompanyId == companyId).ToList();
            var allData = _dbContext.ExceptionalMeal.ToList();

            var lstMealSetting = (from a in _dbContext.MealSetting
                                  join b in _dbContext.IcuDevice on a.IcuDeviceId equals b.Id
                                  join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                                  join d in _dbContext.MealType on a.MealTypeId equals d.Id
                                  select new MealSettingModel
                                  {
                                      id = a.Id,
                                      start = a.Start,
                                      end = a.End,
                                      icuDeviceName = b.Name,
                                      cornerName = c.Name,
                                      mealTypeName = d.Name,
                                      price = a.Price,
                                      icuDevice = b.Id,
                                      mealSettingId = a.Id
                                  }).ToList();

            var data = (from a in allData
                        join b in lstMealSetting on a.MealSettingId equals b.id
                        select new ExceptionalMealModel
                        {
                            id = a.Id,
                            start = a.Start,
                            end = a.End,
                            icuDeviceName = b.icuDeviceName,
                            cornerName = b.cornerName,
                            mealTypeName = b.mealTypeName,
                            price = a.Price,
                            priceDiscount = b.price,
                            fromHout = b.start,
                            toHour = b.end,
                            icuDeviceId = b.icuDevice,
                            mealSettingId = b.mealSettingId
                        }).ToList();

            if (_httpContext.User.GetAccountType() != (short)AccountType.SystemAdmin)
            {
                data = (from a in data join b in lstIcuDeviceByCompany on a.icuDeviceId equals b.Id select a).ToList();
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.icuDeviceName.ToLower().Contains(filter.ToLower()) || x.mealTypeName.ToLower().Contains(filter.ToLower())
                               || x.cornerName.ToLower().Contains(filter.ToLower()) || x.price.ToString().ToLower().Contains(filter.ToLower())).ToList();
            }
            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.ExceptionalMeal.Length - 1 ? 0 : sortColumn;

            data = data.AsQueryable().OrderBy($"{ColumnDefines.ExceptionalMeal[sortColumn]} {sortDirection}").ToList();
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


            return data;
        }

        public ExceptionalMealModel ListExceptionalMealByDateTime(int companyId, int icuId, string accessTime)
        {

            var lstIcuDeviceByCompany = _dbContext.IcuDevice.Where(x => x.CompanyId == companyId && x.Id == icuId).ToList();
            //var allData = _dbContext.ExceptionalMeal.ToList();

            var data = (from a in _dbContext.MealSetting
                                  join b in _dbContext.IcuDevice on a.IcuDeviceId equals b.Id
                                  join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                                  join d in _dbContext.MealType on a.MealTypeId equals d.Id
                                  join e in _dbContext.ExceptionalMeal on a.Id equals e.MealSettingId
                                  select new ExceptionalMealModel
                                  {

                                      id = e.Id,
                                      start = e.Start,
                                      end = e.End,
                                      icuDeviceName = b.Name,
                                      cornerName = c.Name,
                                      mealTypeName = d.Name,
                                      price = e.Price,
                                      priceDiscount = a.Price,
                                      fromHout = a.Start,
                                      toHour = a.End,
                                      icuDeviceId = b.Id

                                  }).ToList();



            data = (from a in data join b in lstIcuDeviceByCompany on a.icuDeviceId equals b.Id select a).ToList();

            DateTime now = DateTimeHelper.ConvertAccessTime(accessTime);
            var firtData = (from a in data where a.start <= now && now <= a.end select a).FirstOrDefault();
            return firtData;
        }

        public ExceptionalMealModel GetExceptionalMeal(int id)
        {
            var lstMealSetting = (from a in _dbContext.MealSetting
                                  join b in _dbContext.IcuDevice on a.IcuDeviceId equals b.Id
                                  join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                                  join d in _dbContext.MealType on a.MealTypeId equals d.Id
                                  select new MealSettingModel
                                  {
                                      id = a.Id,
                                      start = a.Start,
                                      end = a.End,
                                      icuDeviceName = b.Name,
                                      cornerName = c.Name,
                                      mealTypeName = d.Name,
                                      price = a.Price,
                                      icuDevice = b.Id
                                  }).ToList();

            var exeptionalMeal = (from a in _dbContext.ExceptionalMeal
                                  where a.Id == id
                                  join b in lstMealSetting on a.MealSettingId equals b.id
                                  select new ExceptionalMealModel
                                  {
                                      id = a.Id,
                                      start = a.Start,
                                      end = a.End,
                                      icuDeviceName = b.icuDeviceName,
                                      cornerName = b.cornerName,
                                      mealTypeName = b.mealTypeName,
                                      price = a.Price,
                                      priceDiscount = b.price,
                                      fromHout = b.start,
                                      toHour = b.end,
                                      icuDeviceId = b.icuDevice
                                  }).FirstOrDefault();

            return exeptionalMeal;
        }

        public void Add(ExceptionalMeal model)
        {
            _dbContext.Add(model);
            _dbContext.SaveChanges();

        }
        public void Update(ExceptionalMeal model)
        {
            _dbContext.Update(model);
            _dbContext.SaveChanges();

        }
        public void Remove(ExceptionalMeal model)
        {
            _dbContext.ExceptionalMeal.Remove(model);
            _dbContext.SaveChanges();
        }
    }
}
