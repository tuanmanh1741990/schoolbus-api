﻿using System;
using DeMasterProCloud.Common.Infrastructure;
using Microsoft.AspNetCore.Http;
using DeMasterProCloud.DataAccess.Models;
using System.Globalization;
using System.Threading;
using System.Linq;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for Event repository
    /// </summary>
    public interface IEventRepository : IGenericRepository<Event>
    {
        void AddDefaultEventList();
    }

    /// <summary>
    /// EventLog repository
    /// </summary>
    public class EventRepository : GenericRepository<Event>, IEventRepository
    {
        private readonly AppDbContext _dbContext;
        public EventRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Add or update a default account
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void AddDefaultEventList()
        {
            var events = GetAll();

            if (events == null || events.Count() == 0)
            {
                // Add default events to DB
                foreach (var culture in Enum.GetValues(typeof(CultureCodes)))
                {
                    var cultureCode = culture.GetDescription();
                    var cultureCodeNumber = (int)culture;

                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureCode);

                    foreach (var eachEvent in Enum.GetValues(typeof(EventType)))
                    {
                        Event _event = new Event()
                        {
                            Culture = cultureCodeNumber,
                            EventNumber = (int)eachEvent,
                            EventName = eachEvent.GetDescription()
                        };

                        Add(_event);
                    }
                }
            }
        }

    }
}