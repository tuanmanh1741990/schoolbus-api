﻿using System;
using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for AccessGroup repository
    /// </summary>
    public interface IAccessGroupRepository : IGenericRepository<AccessGroup>
    {
        AccessGroup GetByIdAndCompanyId(int companyId, int id);
        AccessGroup GetDefaultAccessGroup(int companyId);
        List<AccessGroup> GetByIdsAndCompanyId(int companyId, List<int> accessGroupIds);
        List<AccessGroup> GetListAccessGroups(int companyId);
        List<AccessGroup> GetAccessGroupUnSetDefault(int companyId, int id);
        AccessGroup GetNoAccessGroup(int companyId);
        AccessGroup GetFullAccessGroup(int companyId);
        AccessGroup GetVisitAccessGroup(int companyId);
        AccessGroup GetByNameAndCompanyId(int companyId, string name);
        bool HasExistName(int accessGroupId, string name);
        void AddDefault(Company company, IConfiguration _configuration);
    }
    public class AccessGroupRepository : GenericRepository<AccessGroup>, IAccessGroupRepository
    {
        private readonly AppDbContext _dbContext;

        public AccessGroupRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get default access group
        /// </summary>
        /// <returns></returns>
        public AccessGroup GetDefaultAccessGroup(int companyId)
        {
            return _dbContext.AccessGroup.FirstOrDefault(x => x.IsDefault && x.CompanyId == companyId && !x.IsDeleted);
        }

        /// <summary>
        /// Get by ids and company
        /// </summary>
        /// <param name="accessGroupIds"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<AccessGroup> GetByIdsAndCompanyId(int companyId, List<int> accessGroupIds)
        {
            return _dbContext.AccessGroup.Include(m => m.User)
                .Where(m => accessGroupIds.Contains(m.Id) && m.CompanyId == companyId && !m.IsDeleted).ToList();
        }

        /// <summary>
        /// Get list access group is valid
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<AccessGroup> GetListAccessGroups(int companyId)
        {
            return _dbContext.AccessGroup.Where(x => x.CompanyId == companyId && !x.IsDeleted).ToList();
        }

        /// <summary>
        /// Get access group by id and companyid
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public AccessGroup GetByIdAndCompanyId(int companyId, int id)
        {
            return _dbContext.AccessGroup.FirstOrDefault(x => x.CompanyId == companyId && x.Id == id && !x.IsDeleted);
        }

        /// <summary>
        /// Get list access group unset default
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<AccessGroup> GetAccessGroupUnSetDefault(int companyId, int id)
        {
            return _dbContext.AccessGroup.Where(x => x.CompanyId == companyId && x.Id != id && !x.IsDeleted).ToList();
        }

        /// <summary>
        /// Get no access group
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public AccessGroup GetNoAccessGroup(int companyId)
        {
            return Get(x => x.CompanyId == companyId && x.Type == (short)AccessGroupType.NoAccess && !x.IsDeleted);
        }

        /// <summary>
        /// Get full access group
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public AccessGroup GetFullAccessGroup(int companyId)
        {
            return Get(x => x.CompanyId == companyId && x.Type == (short)AccessGroupType.FullAccess && !x.IsDeleted);
        }

        /// <summary>
        /// Get visit access group
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public AccessGroup GetVisitAccessGroup(int companyId)
        {
            return Get(x => x.CompanyId == companyId && x.Type == (short)AccessGroupType.VisitAccess && !x.IsDeleted);
        }

        /// <summary>
        /// Check name of access group name is already
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasExistName(int accessGroupId, string name)
        {
            return _dbContext.AccessGroup.Any(x => x.Id != accessGroupId && x.Name == name && !x.IsDeleted);
        }

        /// <summary>
        /// Get the AccessGroup by name and companyId
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public AccessGroup GetByNameAndCompanyId(int companyId, string name)
        {
            return _dbContext.AccessGroup.FirstOrDefault(x => x.CompanyId == companyId && x.Name == name && !x.IsDeleted);
        }


        /// <summary>
        /// Get the AccessGroup by companyId
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<AccessGroup> GetByCompanyId(int companyId)
        {
            return _dbContext.AccessGroup.Where(x => x.CompanyId == companyId && !x.IsDeleted).ToList() ;
        }

        public void AddDefault(Company company, IConfiguration _configuration)
        {
            var accessGroupFullAccess = new AccessGroup
            {
                Name = _configuration[Constants.Settings.DefaultAccessGroupFullAccess],
                IsDefault = true,
                Type = (short)AccessGroupType.FullAccess,
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                IsDeleted = false,
                CompanyId = company.Id,
            };
            var accessGroupNoAccess = new AccessGroup
            {
                Name = _configuration[Constants.Settings.DefaultAccessGroupNoAccess],
                IsDefault = false,
                Type = (short)AccessGroupType.NoAccess,
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                IsDeleted = false,
                CompanyId = company.Id,
            };
            var accessGroupVisitAccess = new AccessGroup
            {
                Name = _configuration[Constants.Settings.DefaultAccessGroupVisitAccess],
                IsDefault = false,
                Type = (short)AccessGroupType.VisitAccess,
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow,
                IsDeleted = false,
                CompanyId = company.Id,
            };

            Add(accessGroupFullAccess);
            Add(accessGroupNoAccess);
            Add(accessGroupVisitAccess);
        }

    }
}
