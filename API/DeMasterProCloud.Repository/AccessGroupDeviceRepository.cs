﻿using System.Collections.Generic;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for AccessGroupDevice repository
    /// </summary>
    public interface IAccessGroupDeviceRepository : IGenericRepository<AccessGroupDevice>
    {
        IQueryable<AccessGroupDevice> GetByAccessGroupId(int companyId, int accessGroupId);
        int GetUserCount(int icuId);
        List<User> GetUserList(int icuId);
        IQueryable<AccessGroupDevice> GetByUnAssignAccessGroupId(int companyId, int accessGroupId);
        bool HasData(int companyId, int accessGroupId);
        List<AccessGroupDevice> GetByAccessGroupIdAndDeviceIds(int companyId, int accessGroupId, List<int> deviceIds);
        AccessGroupDevice GetByAccessGroupIdAndDeviceId(int companyId, int accessGroupId, int deviceId);
        List<AccessGroupDevice> GetByTimezoneId(int companyId, int tzId);
        IQueryable<AccessGroupDevice> GetByIcuId(int companyId, int icuId);
        IQueryable<AccessGroupDevice> GetByIcuIdInOtherCompany(int companyId, int icuId);
        bool HasTimezone(int timezoneId);
    }
    public class AccessGroupDeviceRepository : GenericRepository<AccessGroupDevice>, IAccessGroupDeviceRepository
    {
        private readonly AppDbContext _dbContext;
        public AccessGroupDeviceRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get by access group id
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<AccessGroupDevice> GetByAccessGroupId(int companyId, int accessGroupId)
        {
            var data = _dbContext.AccessGroupDevice.Include(c => c.Icu).Include(c => c.Icu.Building).Include(c => c.Tz)
                .Where(c => c.AccessGroupId == accessGroupId /*&& c.AccessGroup.CompanyId == companyId*/ &&
                            c.Icu.Status == (short)Status.Valid && !c.Icu.IsDeleted);

            if(companyId != 0)
            {
                data = data.Where(c => c.AccessGroup.CompanyId == companyId);
            }

            //return _dbContext.AccessGroupDevice.Include(c => c.Icu).Include(c => c.Icu.Building).Include(c => c.Tz)
            //    .Where(c => c.AccessGroupId == accessGroupId && c.AccessGroup.CompanyId == companyId &&
            //                c.Icu.Status == (short) Status.Valid && !c.Icu.IsDeleted);

            return data;
        }

        /// <summary>
        /// Count number of user in a device
        /// </summary>
        /// <param name="icuId"></param>
        /// <returns></returns>
        public int GetUserCount(int icuId)
        {
            return _dbContext.AccessGroupDevice.Where(c => c.IcuId == icuId).Include(c => c.AccessGroup)
                .ThenInclude(c => c.User).Where(c => !c.AccessGroup.IsDeleted && !c.Icu.IsDeleted)
                .SelectMany(c => c.AccessGroup.User).Count();
        }

        /// <summary>
        /// List of user in a device
        /// </summary>
        /// <param name="icuId"></param>
        /// <returns></returns>
        public List<User> GetUserList(int icuId)
        {
            return _dbContext.AccessGroupDevice.Where(c => c.IcuId == icuId).Include(c => c.AccessGroup)
                .ThenInclude(c => c.User).Where(c => !c.AccessGroup.IsDeleted && !c.Icu.IsDeleted)
                .SelectMany(c => c.AccessGroup.User).ToList();
        }

        /// <summary>
        /// Get not by access group id
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<AccessGroupDevice> GetByUnAssignAccessGroupId(int companyId, int accessGroupId)
        {
            return _dbContext.AccessGroupDevice.Include(c => c.Icu).Include(c => c.Icu).Include(c => c.Tz)
                .Where(c => c.AccessGroupId != accessGroupId && c.AccessGroup.CompanyId == companyId && !c.Icu.IsDeleted);
        }

        /// <summary>
        /// Check if having any device was assigned to an access group
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupId"></param>
        /// <returns></returns>
        public bool HasData(int companyId, int accessGroupId)
        {
            return _dbContext.AccessGroupDevice.Include(c => c.AccessGroup).Include(c => c.Icu).Any(c =>
                c.AccessGroupId == accessGroupId && c.AccessGroup.CompanyId == companyId && !c.AccessGroup.IsDeleted &&
                !c.Icu.IsDeleted);
        }

        /// <summary>
        /// Get not by access group id
        /// </summary>
        /// <param name="accessGroupId"></param>
        /// <param name="deviceIds"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public List<AccessGroupDevice> GetByAccessGroupIdAndDeviceIds(int companyId, int accessGroupId, List<int> deviceIds)
        {
            return _dbContext.AccessGroupDevice.Include(c => c.Icu).Include(c => c.AccessGroup).ThenInclude(c => c.User)
                .Where(c => c.AccessGroupId == accessGroupId && deviceIds.Contains(c.IcuId) &&
                            c.AccessGroup.CompanyId == companyId && !c.AccessGroup.IsDeleted && !c.Icu.IsDeleted)
                .ToList();
        }

        /// <summary>
        /// Get by accessgrop and device id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="accessGroupId"></param>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public AccessGroupDevice GetByAccessGroupIdAndDeviceId(int companyId, int accessGroupId, int deviceId)
        {
            return _dbContext.AccessGroupDevice.Include(c => c.Tz)
                .FirstOrDefault(c => c.AccessGroupId == accessGroupId && deviceId == c.IcuId &&
                                     c.AccessGroup.CompanyId == companyId && !c.AccessGroup.IsDeleted &&
                                     !c.Icu.IsDeleted);
        }

        /// <summary>
        /// Get by timezone id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="tzId"></param>
        /// <returns></returns>
        public List<AccessGroupDevice> GetByTimezoneId(int companyId, int tzId)
        {
            return _dbContext.AccessGroupDevice.Include(x => x.AccessGroup).Include(x => x.Icu).Where(x =>
                    x.TzId == tzId && x.AccessGroup.CompanyId == companyId && !x.AccessGroup.IsDeleted && !x.Icu.IsDeleted).ToList();
        }

        /// <summary>
        /// Get by icuid
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="icuId"></param>
        /// <returns></returns>
        public IQueryable<AccessGroupDevice> GetByIcuId(int companyId, int icuId)
        {
            var data = _dbContext.AccessGroupDevice.Include(x => x.AccessGroup).Include(x => x.Icu).Include(x => x.Tz).Where(x =>
                x.IcuId == icuId && !x.AccessGroup.IsDeleted && !x.Icu.IsDeleted);

            if(companyId != 0)
            {
                data = data.Where(x => x.AccessGroup.CompanyId == companyId);
            }

            return data;
        }

        /// <summary>
        /// Get by icuid
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="icuId"></param>
        /// <returns></returns>
        public IQueryable<AccessGroupDevice> GetByIcuIdInOtherCompany(int companyId, int icuId)
        {
            return _dbContext.AccessGroupDevice.Include(x => x.AccessGroup).Include(x => x.Icu).Include(x => x.Tz).Where(x =>
                x.IcuId == icuId && x.AccessGroup.CompanyId != companyId && !x.AccessGroup.IsDeleted && !x.Icu.IsDeleted);
        }

        /// <summary>
        /// Check if door is assigned to a timezone
        /// </summary>
        /// <param name="timezoneId"></param>
        /// <returns></returns>
        public bool HasTimezone(int timezoneId)
        {
            return _dbContext.AccessGroupDevice.Any(c => c.TzId == timezoneId);
        }
    }
}
