﻿using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using DeMasterProCloud.DataModel.MealSetting;
using Microsoft.AspNetCore.Http;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net.WebSockets;
using System.Text;

namespace DeMasterProCloud.Repository
{
    public interface IMealSettingRepository
    {
        ReponseStatus AddMealSetting(MealSetting model);
        ReponseStatus UpdateMealSetting(MealSetting model);
        ReponseStatus DeleteMealSetting(int id);
        List<MealSettingModel> GetListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered, int companyId, short accountType);
        List<ListMealSettingModel> ListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
            string sortDirection, out int totalRecords, out int recordsFiltered, int companyId, short accountType);
        MealSettingModel GetMealSetting(int Id);
        MealSettingModel CheckAcessTime(string rId, string accessTime);
        List<MealSetting> GetMealSettingByIcuId(int icuDeviceId);
        List<MealSetting> GetMealSettingByIcuIdandMealTypId(int icuDeviceId, int mealTypeId);
    }
    public class MealSettingRepository : GenericRepository<MealSettingModel>, IMealSettingRepository
    {
        private readonly AppDbContext _dbContext;
        //private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public MealSettingRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            //_httpContext = contextAccessor.HttpContext;
        }
        public ReponseStatus AddMealSetting(MealSetting model)
        {

            try
            {
                var lst = _dbContext.MealSetting.Where(x => x.CornerId == model.CornerId && x.IcuDeviceId == model.IcuDeviceId && x.MealTypeId == model.MealTypeId).FirstOrDefault();
                var checkCornerAndIcu = _dbContext.MealSetting.Where(x => x.CornerId != model.CornerId && x.IcuDeviceId == model.IcuDeviceId).FirstOrDefault();
                if (lst != null || checkCornerAndIcu != null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }




                MealSetting mealSetting = new MealSetting();
                mealSetting.Price = model.Price;
                mealSetting.Start = model.Start;
                mealSetting.End = model.End;
                mealSetting.CornerId = model.CornerId;
                mealSetting.IcuDeviceId = model.IcuDeviceId;
                mealSetting.MealTypeId = model.MealTypeId;
                Add(mealSetting);
                res.message = Constants.CornerSetting.AddSuccess;
                res.statusCode = true;
                res.data = mealSetting.Id;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.AddFailed;
                res.statusCode = false;
            }

            return res;
        }
        public ReponseStatus UpdateMealSetting(MealSetting model)
        {

            try
            {

                MealSetting mealSetting = _dbContext.MealSetting.Where(x => x.Id == model.Id).FirstOrDefault();
                mealSetting.Price = model.Price;
                mealSetting.Start = model.Start;
                mealSetting.End = model.End;
                Update(mealSetting);
                res.message = Constants.CornerSetting.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.UpdateFailed;
                res.statusCode = false;
            }

            return res;
        }
        public ReponseStatus DeleteMealSetting(int id)
        {

            try
            {
                var mealSetting = _dbContext.MealSetting.Where(x => x.Id == id).FirstOrDefault();
                Remove(mealSetting);
                res.message = Constants.CornerSetting.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.CornerSetting.DeleteFailed;
                res.statusCode = false;
            }

            return res;
        }

        public List<MealSetting> GetMealSettingByIcuId(int icuDeviceId)
        {
            var lstMealSetting = _dbContext.MealSetting.Where(x => x.IcuDeviceId == icuDeviceId).ToList();
            return lstMealSetting;
        }

        public List<MealSetting> GetMealSettingByIcuIdandMealTypId(int icuDeviceId, int mealTypeId)
        {
            var lstMealSetting = _dbContext.MealSetting.Where(x => x.IcuDeviceId == icuDeviceId && x.MealTypeId != mealTypeId).ToList();
            return lstMealSetting;
        }

        public List<MealSettingModel> GetListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered, int companyId, short accountType)
        {
            //int companyId = _httpContext.User.GetCompanyId();
            var lstIcuDeviceByCompany = _dbContext.IcuDevice.Where(x => x.CompanyId == companyId).ToList();
            var allData = _dbContext.MealSetting.ToList();

            var data = (from a in allData
                        join b in _dbContext.IcuDevice on a.IcuDeviceId equals b.Id
                        join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                        join d in _dbContext.MealType on a.MealTypeId equals d.Id
                        select new MealSettingModel
                        {
                            id = a.Id,
                            start = a.Start,
                            end = a.End,
                            icuDeviceName = b.Name,
                            cornerName = c.Name,
                            mealTypeName = d.Name,
                            price = a.Price,
                            icuDevice = b.Id
                        }).ToList();

            if (accountType != (short)AccountType.SystemAdmin)
            {
                data = (from a in data join b in lstIcuDeviceByCompany on a.icuDevice equals b.Id select a).ToList();
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.icuDeviceName.ToLower().Contains(filter.ToLower()) || x.mealTypeName.ToLower().Contains(filter.ToLower())
                               || x.cornerName.ToLower().Contains(filter.ToLower()) || x.price.ToString().ToLower().Contains(filter.ToLower())).ToList();
            }
            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.MealSetting.Length - 1 ? 0 : sortColumn;

            data = data.AsQueryable().OrderBy($"{ColumnDefines.MealSetting[sortColumn]} {sortDirection}").ToList();
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


            return data;
        }



        public List<ListMealSettingModel> ListMealSetting(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered, int companyId, short accountType)
        {
            var allData = (from a in _dbContext.MealSetting
                           join b in _dbContext.CornerSetting on a.CornerId equals b.Id
                           where b.CompanyId == companyId
                           group a by new { a.IcuDeviceId, a.CornerId } into g

                           select new
                           {
                               IcuDeviceId = g.Key.IcuDeviceId,
                               CornerId = g.Key.CornerId
                           }).ToList();
            var data = (from a in allData
                        join b in _dbContext.IcuDevice on a.IcuDeviceId equals b.Id
                        join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                        select new ListMealSettingModel
                        {

                            icuDeviceName = b.Name,
                            cornerName = c.Name,
                            cornerId = c.Id,
                            icuDevice = b.Id,
                            rId = b.DeviceAddress,
                            data = new
                            {
                                data = ListMealTypeByRID(a.IcuDeviceId)
                            }

                        }).ToList();


            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.icuDeviceName.ToLower().Contains(filter.ToLower()) || x.cornerName.ToLower().Contains(filter.ToLower())).ToList();
            }
            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.ListMealSetting.Length - 1 ? 0 : sortColumn;

            data = data.AsQueryable().OrderBy($"{ColumnDefines.ListMealSetting[sortColumn]} {sortDirection}").Distinct().ToList();
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


            return data;
        }


        public List<ObjectMealSettingModel> ListMealTypeByRID(int icuDeviceId)
        {

            var lst = (from a in _dbContext.MealType
                       join b in _dbContext.MealSetting on a.Id equals b.MealTypeId
                       where b.IcuDeviceId == icuDeviceId
                       select new ObjectMealSettingModel
                       {
                           mealTypeName = a.Name,
                           mealTypeId = a.Id,
                           start = TimeSpan.FromHours(Convert.ToDouble(b.Start)).ToString("h\\:mm"),
                           end = TimeSpan.FromHours(Convert.ToDouble(b.End)).ToString("h\\:mm"),
                           mealSettingId = b.Id,
                           price = b.Price
                       }).ToList();
            return lst;
        }

        public MealSettingModel GetMealSetting(int Id)
        {
            var mealSetting = (from a in _dbContext.MealSetting
                               join b in _dbContext.MealType on a.MealTypeId equals b.Id
                               join c in _dbContext.CornerSetting on a.CornerId equals c.Id
                               join d in _dbContext.IcuDevice on a.IcuDeviceId equals d.Id
                               where a.Id == Id
                               select new MealSettingModel
                               {
                                   id = a.Id,
                                   icuDeviceName = d.Name,
                                   cornerName = c.Name,
                                   mealTypeName = b.Name,
                                   start = a.Start,
                                   end = a.End,
                                   price = a.Price
                               }).FirstOrDefault();
            return mealSetting;
        }

        public MealSettingModel CheckAcessTime(string rId, string accessTime)
        {
            MealSettingModel result = new MealSettingModel();
            int icuId = _dbContext.IcuDevice.Where(x => x.DeviceAddress == rId).Select(x => x.Id).FirstOrDefault();
            var checkIcuIsRes = (from a in _dbContext.MealSetting
                                 join b in _dbContext.MealType on a.MealTypeId equals b.Id
                                 where a.IcuDeviceId == icuId
                                 select new { a.IcuDeviceId, a.MealTypeId, a.Price, b.Code, a.Start, a.End, a.CornerId }).ToList();
            //var timeTouch = Convert.ToDecimal(DateTime.Parse(accessTime));
            var timeTouch = Convert.ToDecimal(TimeSpan.Parse(DateTimeHelper.ConvertAccessTime(accessTime).TimeOfDay.ToString()).TotalHours);
            if (checkIcuIsRes.Count > 0)
            {

                foreach (var item in checkIcuIsRes)
                {
                    if (item.Start <= timeTouch && timeTouch <= item.End)
                    {
                        result.icuDevice = icuId;
                        result.mealTypeId = item.MealTypeId;
                        result.price = item.Price;
                        result.Code = item.Code;
                        result.cornerId = item.CornerId;
                        return result;
                    }
                }
                result.Code = (short)EventType.OnlyAccessibleAtMealtime;

            }
            else
            {
                result.Code = (short)EventType.NoDoorActiveTime;
            }

            return result;
        }


        public void Add(MealSetting model)
        {
            _dbContext.Add(model);
            _dbContext.SaveChanges();

        }
        public void Update(MealSetting model)
        {
            _dbContext.Update(model);
            _dbContext.SaveChanges();

        }
        public void Remove(MealSetting model)
        {
            _dbContext.MealSetting.Remove(model);
            _dbContext.SaveChanges();
        }
    }
}
