﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using Newtonsoft.Json;
using DeMasterProCloud.DataModel.PlugIn;
using System.Reflection;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for Company repository
    /// </summary>
    public interface ICompanyRepository : IGenericRepository<Company>
    {
        string MakeCompanyCode();
        Company GetCompanyById(int? id);
        List<Company> GetExpiredCompaniesByDay(int numberOfDay = 0, bool bExactDay = true);
        void UpdateCompaniesToExpire(List<Company> companies);
        Company GetRootCompany();
        List<Company> GetByIds(List<int> ids);
        List<Company> GetCompanies();
        String GetCompanyCodeByCompanyId(int companyId);
        List<Company> GetCompaniesByPlugin(string pluginName);
        List<Company> GetCompanyByPlugin(string pluginName, int companyId);
    }

    /// <summary>
    /// Company repository
    /// </summary>
    public class CompanyRepository : GenericRepository<Company>, ICompanyRepository
    {
        private readonly AppDbContext _dbContext;
        public CompanyRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Get company by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Company GetCompanyById(int? id)
        {
            return _dbContext.Company/*.Include(c => c.Account)*/
                .FirstOrDefault(c =>
                    c.Id == id /*&& c.Account.FirstOrDefault(m => m.RootFlag).CompanyId == id*/ &&
                    !c.IsDeleted);
        }

        ///// <summary>
        ///// Get companies
        ///// </summary>
        ///// <param></param>
        ///// <returns></returns>
        //public List<Company> GetCompanies()
        //{
        //    return _dbContext.Company.Where(c => !c.IsDeleted).ToList();
        //}

        /// <summary>
        /// Get company by list of id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<Company> GetByIds(List<int> ids)
        {
            return GetMany(c => ids.Contains(c.Id) && !c.IsDeleted).ToList();
        }

        /// <summary>
        /// Make company code
        /// </summary>
        /// <returns></returns>
        public string MakeCompanyCode()
        {
            var companies = _dbContext.Company.ToList();
            var companyCode = Helpers.GenerateCompanyCode();
            while (companies.Any(c => c.Code == companyCode))
            {
                companyCode = Helpers.GenerateCompanyCode();
            }
            return companyCode;
        }

        /// <summary>
        /// Get company code
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public string GetCompanyCodeByCompanyId(int companyId)
        {
            var company = _dbContext.Company.FirstOrDefault(c => c.Id == companyId);
            
            return company.Code;
        }

        /// <summary>
        /// Get list of company that enabled specific plugin.
        /// </summary>
        /// <param name="pluginName"></param>
        /// <returns></returns>
        public List<Company> GetCompaniesByPlugin(string pluginName)
        {
            List<Company> enabledCompanies = new List<Company>();

            var plugInTuples = _dbContext.PlugIn.Include(m => m.Company).Where(m => !m.Company.IsDeleted);

            foreach (var plugin in plugInTuples)
            {
                var solution = JsonConvert.DeserializeObject<PlugIns>(plugin.PlugIns);

                foreach (PropertyInfo pi in solution.GetType().GetProperties())
                {
                    if (pi.Name == pluginName)
                    {
                        bool value = (bool)solution.GetType().GetProperty(pluginName).GetValue(solution, null);
                        if (value)
                            enabledCompanies.Add(plugin.Company);
                    }
                }
            }

            return enabledCompanies;
        }

        public List<Company> GetCompanyByPlugin(string pluginName, int companyId)
        {
            List<Company> enabledCompanies = new List<Company>();

            var plugInTuples = _dbContext.PlugIn.Include(m => m.Company).Where(m => !m.Company.IsDeleted && m.CompanyId == companyId);
            if(plugInTuples.Count() > 0)
            {
                foreach (var plugin in plugInTuples)
                {
                    var solution = JsonConvert.DeserializeObject<PlugIns>(plugin.PlugIns);

                    foreach (PropertyInfo pi in solution.GetType().GetProperties())
                    {
                        if (pi.Name == pluginName)
                        {
                            bool value = (bool)solution.GetType().GetProperty(pluginName).GetValue(solution, null);
                            if (value)
                                enabledCompanies.Add(plugin.Company);
                        }
                    }
                }
            }
            else
            {
                return new List<Company>();
            }
           

            return enabledCompanies;
        }



        /// <summary>
        /// Get expired companies by day
        /// </summary>
        /// <param name="numberOfDay"></param>
        /// <param name="bExactDay"></param>
        /// <returns></returns>
        public List<Company> GetExpiredCompaniesByDay(int numberOfDay, bool bExactDay)
        {
            var companies = _dbContext.Company.Include(c => c.Account)
                .Where(c => !c.RootFlag && !c.IsDeleted)
                .AsEnumerable();
            companies = bExactDay
                ? companies.Where(c => DateTime.Now.AddDays(numberOfDay).Date.Subtract(c.ExpiredTo.Date).Days == 0)
                : companies.Where(c => DateTime.Now.AddDays(numberOfDay).Date.Subtract(c.ExpiredTo.Date).Days >= 0);
            return companies.ToList();
        }

        /// <summary>
        /// Update companies to expired
        /// </summary>
        /// <param name="companies"></param>
        public void UpdateCompaniesToExpire(List<Company> companies)
        {
            //Update companies to expired
            foreach (var company in companies)
            {
                //company.IsD = (short)Status.Invalid;
                Update(company);
            }
        }

        /// <summary>
        /// Get root company
        /// </summary>
        /// <returns></returns>
        public Company GetRootCompany()
        {
            return Get(m => m.RootFlag);
        }

        /// <summary>
        /// Get all companies
        /// </summary>
        /// <returns></returns>
        public List<Company> GetCompanies()
        {
            return GetMany(c =>!c.IsDeleted).OrderBy(c => c.Id).ToList();
        }
    }
}