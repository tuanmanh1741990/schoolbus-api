using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.PlugIn;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace DeMasterProCloud.Repository
{
    public interface IPlugInRepository : IGenericRepository<PlugIn>
    {
        void AddPlugInDefault(int companyId);
        
        PlugIn GetPlugInByCompany(int companyId);
    }
    
    public class PlugInRepository : GenericRepository<PlugIn>, IPlugInRepository
    {
        private readonly AppDbContext _dbContext;
        
        public PlugInRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }
         
        public void AddPlugInDefault(int companyId)
        {
            var company = Get(m => m.CompanyId == companyId);
            if (company == null)
            {
                var plugIns = new PlugIns()
                {
                    AccessControl = Constants.PlugInValue.AccessControl,
                    
                    TimeAttendance = Constants.PlugInValue.TimeAttendance,
                    
                    VisitManagement = Constants.PlugInValue.VisitManagement,
                    
                    CanteenManagement = Constants.PlugInValue.CanteenManagement,
                    
                    CardIssuing = Constants.PlugInValue.CardIssuing,
                    
                    Common = Constants.PlugInValue.Common,
                    
                    QrCode = Constants.PlugInValue.QrCode,
                    
                    PassCode = Constants.PlugInValue.PassCode,
                    
                    ScreenMessage = Constants.PlugInValue.ScreenMessage,

                    ArmyManagement = Constants.PlugInValue.ArmyManagement,
                };
                var json = JsonConvert.SerializeObject(plugIns);
                
                var plugInsDescription = new PlugInsDescription()
                {
                    AccessControl = Constants.PlugInValue.AccessControlDescription,
                    
                    TimeAttendance = Constants.PlugInValue.TimeAttendanceDescription,
                    
                    VisitManagement = Constants.PlugInValue.VisitManagementDescription,
                    
                    CanteenManagement = Constants.PlugInValue.CanteenManagementDescription,
                    
                    CardIssuing = Constants.PlugInValue.CardIssuingDescription,
                    
                    Common = Constants.PlugInValue.CommonDescription,
                    
                    QrCode = Constants.PlugInValue.QrCodeDescription,
                    
                    PassCode = Constants.PlugInValue.PassCodeDescription,
                    
                    ScreenMessage = Constants.PlugInValue.ScreenMessageDescription,

                    ArmyManagement = Constants.PlugInValue.ArmyManagementDescription,
                };
                var jsonDescription = JsonConvert.SerializeObject(plugInsDescription);
                
                var plugIn = new PlugIn()
                {
                    CompanyId = companyId,
                    PlugIns = json,
                    PlugInsDescription = jsonDescription
                };
                Add(plugIn);
            }
        }

        public PlugIn GetPlugInByCompany(int companyId)
        {
            return Get(m => m.CompanyId == companyId);
        }
    }
}