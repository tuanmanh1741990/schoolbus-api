﻿using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq.Dynamic.Core;
using System.Linq;
using DeMasterProCloud.Common.Infrastructure;
using DeMasterProCloud.DataModel.UserDiscount;

namespace DeMasterProCloud.Repository
{
    public interface IUserDiscountRepository
    {
        ReponseStatus AddUserDiscount(UserDiscount model);
        ReponseStatus UpdateUserDiscount(UserDiscount model);
        ReponseStatus DeleteUserDiscount(int id);
        List<UserDiscountModel> GetListUserDiscount(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered);
        UserDiscountModel GetUserDiscount(int id);
    }
    public class UserDiscountRepository : GenericRepository<UserDiscountModel>, IUserDiscountRepository
    {
        private readonly AppDbContext _dbContext;
        private readonly HttpContext _httpContext;
        ReponseStatus res = new ReponseStatus();
        public UserDiscountRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
            _httpContext = contextAccessor.HttpContext;
        }
        public ReponseStatus AddUserDiscount(UserDiscount model)
        {

            try
            {
                var lst = _dbContext.UserDiscount.Where(x => x.UserId == model.UserId).FirstOrDefault();
                if (lst != null)
                {
                    res.message = null;
                    res.statusCode = true;
                    return res;
                }
                UserDiscount userDiscount = new UserDiscount();
                userDiscount.UserId = model.UserId;
                userDiscount.Amount = model.Amount;
                Add(userDiscount);
                res.message = Constants.UserDiscount.AddSuccess;
                res.statusCode = true;
                res.data = userDiscount.Id;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.UserDiscount.AddFailed;
                res.statusCode = false;
            }

            return res;
        }
        public ReponseStatus UpdateUserDiscount(UserDiscount model)
        {

            try
            {
                UserDiscount userDiscount = _dbContext.UserDiscount.Where(x => x.Id == model.Id).FirstOrDefault();
                userDiscount.Amount = model.Amount;
                Update(userDiscount);
                res.message = Constants.UserDiscount.UpdateSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.UserDiscount.UpdateFailed;
                res.statusCode = false;
            }

            return res;
        }
        public ReponseStatus DeleteUserDiscount(int id)
        {

            try
            {
                var userDiscount = _dbContext.UserDiscount.Where(x => x.Id == id).FirstOrDefault();
                Remove(userDiscount);
                res.message = Constants.UserDiscount.DeleteSuccess;
                res.statusCode = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                res.message = Constants.UserDiscount.DeleteFailed;
                res.statusCode = false;
            }

            return res;
        }

        public List<UserDiscountModel> GetListUserDiscount(string filter, int pageNumber, int pageSize, int sortColumn,
             string sortDirection, out int totalRecords, out int recordsFiltered)
        {
            var allData = _dbContext.UserDiscount.ToList();

            var companyId = _httpContext.User.GetCompanyId();

            var data = (from a in allData
                        join b in _dbContext.User on a.UserId equals b.Id
                        select new UserDiscountModel
                        {
                            id = a.Id,
                            amount = a.Amount,
                            userId = a.UserId,
                            userName = b.FirstName + "" + b.LastName
                        }).ToList();
            if (_httpContext.User.GetAccountType() != (short)AccountType.SystemAdmin)
            {
                var lstUserDiscountByCompany = _dbContext.Company.Where(c => c.Id == companyId).Select(x => x.User.ToList()).FirstOrDefault();
                data = (from a in allData
                        join b in lstUserDiscountByCompany on a.UserId equals b.Id
                        select new UserDiscountModel
                        {
                            userId = a.UserId,
                            userName = b.FirstName + "" + b.LastName,
                            amount = a.Amount,
                            id = a.Id
                        }).ToList();
            }

            totalRecords = data.Count();

            if (!string.IsNullOrEmpty(filter))
            {
                data = data.Where(x => x.userName.ToLower().Contains(filter.ToLower())).ToList();
            }
            recordsFiltered = data.Count();
            sortColumn = sortColumn > ColumnDefines.UserDiscount.Length - 1 ? 0 : sortColumn;

            data = data.AsQueryable().OrderBy($"{ColumnDefines.UserDiscount[sortColumn]} {sortDirection}").ToList();
            data = data.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();


            return data;
        }


        public UserDiscountModel GetUserDiscount(int id)
        {
            var userDiscount = (from a in _dbContext.UserDiscount
                                join b in _dbContext.User on a.UserId equals b.Id
                                select new UserDiscountModel
                                {
                                    id = a.Id,
                                    userId = a.UserId,
                                    amount = a.Amount,
                                    userName = b.FirstName + "" + b.LastName
                                }).FirstOrDefault();
            return userDiscount;
        }

        public void Add(UserDiscount model)
        {
            _dbContext.Add(model);
            _dbContext.SaveChanges();

        }
        public void Update(UserDiscount model)
        {
            _dbContext.Update(model);
            _dbContext.SaveChanges();

        }
        public void Remove(UserDiscount model)
        {
            _dbContext.UserDiscount.Remove(model);
            _dbContext.SaveChanges();
        }
    }
}
