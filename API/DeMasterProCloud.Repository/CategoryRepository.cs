﻿using DeMasterProCloud.DataAccess.Models;
using DeMasterProCloud.DataModel.Category;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Repository
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        Category GetCategoryByIdAndCompanyId(int id, int companyId);
        Category GetCategoryByNameAndCompanyId(string name, int companyId);
        IQueryable<Category> GetCategoriesByParentIdAndCompanyId(int parentId, int companyId);
        IQueryable<Category> GetCategoriesByCompanyId(int companyId);
    }

    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly AppDbContext _dbContext;

        public CategoryRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }

        public Category GetCategoryByIdAndCompanyId(int id, int companyId)
        {
            var category = _dbContext.Category.Where(m => m.Id == id && m.CompanyId == companyId);

            if(category.Count() != 0)
            {
                return category.First();
            }

            return null;
        }

        public Category GetCategoryByNameAndCompanyId(string name, int companyId)
        {
            var category = _dbContext.Category.Where(m => m.Name.Equals(name) && m.CompanyId == companyId);

            if(category.Count() != 0)
            {
                return category.First();
            }

            return null;
        }

        public IQueryable<Category> GetCategoriesByParentIdAndCompanyId(int parentId, int companyId)
        {
            var categories = _dbContext.Category.Where(m => m.ParentCategoryId == parentId && m.CompanyId == companyId);

            return categories;
        }

        public IQueryable<Category> GetCategoriesByCompanyId(int companyId)
        {
            var categories = _dbContext.Category
                .Where(m => m.CompanyId == companyId);

            return categories;
        }

    }
}