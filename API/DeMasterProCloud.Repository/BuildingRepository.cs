﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Linq;
using DeMasterProCloud.DataAccess.Models;
using System;

namespace DeMasterProCloud.Repository
{
    /// <summary>
    /// Interface for Holiday repository
    /// </summary>
    public interface IBuildingRepository : IGenericRepository<Building>
    {
        IQueryable<Building> GetByCompanyId(int companyId);
        Building GetByIdAndCompanyId(int companyId, int id);
        Building GetDefaultByCompanyId(int companyId);
        List<Building> GetByIdsAndCompanyId(int companyId, List<int> ids);
        string GetBuildingNameByRid(int companyId, string deviceAddress);
        Building GetByNameAndCompanyId(string companyName, int companyId);
        void AddDefault(Company company);
    }

    /// <summary>
    /// Holiday repository
    /// </summary>
    public class BuildingRepository : GenericRepository<Building>, IBuildingRepository
    {
        private readonly AppDbContext _dbContext;
        public BuildingRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext, contextAccessor)
        {
            _dbContext = dbContext;
        }


        /// <summary>
        /// Get by companyid
        /// </summary>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public IQueryable<Building> GetByCompanyId(int companyId)
        {
            return _dbContext.Building.Where(x => x.CompanyId == companyId && !x.IsDeleted);
        }

        /// <summary>
        /// Get by company and id
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="id"></param>
        public Building GetByIdAndCompanyId(int companyId, int id)
        {
            return _dbContext.Building.Where(x => x.CompanyId == companyId && x.Id == id && !x.IsDeleted).FirstOrDefault();
        }
        
        /// <summary>
        /// Get default building by company Id
        /// </summary>
        /// <param name="companyId"></param>
        public Building GetDefaultByCompanyId(int companyId)
        {
            return _dbContext.Building.Where(x => x.CompanyId == companyId && !x.IsDeleted).OrderBy(x => x.Id).FirstOrDefault();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="ids"></param>
        /// <returns></returns>
        public List<Building> GetByIdsAndCompanyId(int companyId, List<int> ids)
        {
            return _dbContext.Building.Where(x => ids.Contains(x.Id) && x.CompanyId == companyId && !x.IsDeleted)
                .ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="companyName"></param>
        /// <param name="companyId"></param>
        /// <returns></returns>
        public Building GetByNameAndCompanyId(string companyName, int companyId)
        {
            return Get(x => x.Name == companyName && x.CompanyId == companyId && !x.IsDeleted);
        }

        public string GetBuildingNameByRid(int companyId, string deviceAddress)
        {
            var device = _dbContext.IcuDevice.Where(x =>
                x.DeviceAddress.Equals(deviceAddress) && x.CompanyId == companyId && !x.IsDeleted).FirstOrDefault();
            if (device == null)
                return "";
            else
                return _dbContext.Building.Where(x =>
                x.Id == device.BuildingId && x.CompanyId == companyId && !x.IsDeleted).Select(x => x.Name).FirstOrDefault();
        }

        /// <summary>
        /// Add a default building by company
        /// </summary>
        /// <param name="company"></param>
        public void AddDefault(Company company)
        {
            Building building = new Building
            {
                CompanyId = company.Id,
                //Name = company.Name,
                Name = "Head Quarter",
                CreatedOn = DateTime.UtcNow,
                UpdatedOn = DateTime.UtcNow
            };
            Add(building);
        }
    }
}