﻿using DeMasterProCloud.DataAccess.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DeMasterProCloud.Repository
{
    public interface IUserCategoryOptionRepository : IGenericRepository<UserCategoryOption>
    {
        IQueryable<UserCategoryOption> GetByUserId(int userId);
        IQueryable<UserCategoryOption> GetByUserIdAndOptionId(int userId, int optionId);
    }

    public class UserCategoryOptionRepository : GenericRepository<UserCategoryOption>, IUserCategoryOptionRepository
    {
        private readonly AppDbContext _dbContext;

        public UserCategoryOptionRepository(AppDbContext dbContext, IHttpContextAccessor contextAccessor) : base(dbContext,
            contextAccessor)
        {
            _dbContext = dbContext;
        }

        public IQueryable<UserCategoryOption> GetByUserId(int userId)
        {
            var userOption = _dbContext.UserCategoryOption.Where(m => m.UserId == userId);

            return userOption;
        }

        public IQueryable<UserCategoryOption> GetByUserIdAndOptionId(int userId, int optionId)
        {
            var userOption = _dbContext.UserCategoryOption.Where(m => m.UserId == userId && m.CategoryOptionId == optionId);

            return userOption;
        }
    }
}