## Overview
Custom docker image for backup data

## Quickstart 
<dl>
<dt>Build image</dt>
Go to deployment/ folder and build 

    $ cd deployment/
    $ docker build -t docker-backup docker-backup/. 

Run backup manually

    $ docker-compose run backup --backup

Run restore manally

    $ docker-compose run backup --restore /var/restore-result

## Customize
Custom docker-backup variable in file docker-backup.env
For store backup in local

    DEST_FILE=var/backup-result

For store backup with AWS S3, create a storage in in AWS s3 and add follow config:

    AWS_S3_BUCKET=s3_bucket_name
    AWS_ACCESS_KEY_ID=aws_key_id
    AWS_SECRET_ACCESS_KEY=aws_access_key
    AWS_LOCATION=aws_location

## How it work
<dl>
<dt>Backup</dt>
Normally,  backup run an incremental backup each 10 minutes and create new full backup each day. Change CRON_SCHEDULE in `docker-backup.env` to update schedule

    CRON_SCHEDULE=*/10 * * * * # Run incremental backup each 10 minutes
    FULL_BACKUP_SCHEDULE=0 0 * * * # Run full backup at 0h every day 
For show all the backup sets:

    $ docker-compose run backup --collection-status

<dt>Restore procedure</dt>
Step 1: Change config to get new new backup by create new volumn and mount with /var/restore-result :

      backup:
        << : *default
        image: 009795078640.dkr.ecr.ap-southeast-1.amazonaws.com/docker-backup:latest
        restart: always
        env_file:
          - docker-backup.env
        volumes:
          - /path/to/postgres-data:/var/backup
          - /path/to/dmpw-backup:/var/backup-result
          - /path/to/postgres-data__restore_YYMMDD:/var/restore-result # Change this line

Step 2: Restore backup with specific time

    docker-compose run backup --restore /var/restore-result --time time

The **-t** or  **--time** options take a time string, which can be given in any of several formats:

 1. the string "now" (refers to the current time)
 2. a sequences of digits, like "123456890" (indicating the time in seconds after the epoch)
 3. A string like "2002-01-25T07:00:00+02:00" in datetime format
 4. An interval, which is a number followed by one of the characters s, m, h, D, W, M, or Y (indicating seconds, minutes, hours, days, weeks, months, or years respectively), or a series of such pairs. In this case the string refers to the time that preceded the current time by the length of the interval. For instance, "1h78m" indicates the time that was one hour and 78 minutes ago. The calendar here is unsophisticated: a month is always 30 days, a year is always 365 days, and a day is always 86400 seconds.
 5. A date format of the form YYYY/MM/DD, YYYY-MM-DD, MM/DD/YYYY, or MM-DD-YYYY, which indicates midnight on the day in question, relative to the current time zone settings. For instance, "2002/3/5", "03-05-2002", and "2002-3-05" all mean March 5th, 2002.

Step 3: Mount data volumn of database to new backup folder:

    postgres-db:
        << : *default
        image: "postgres"
        volumes:
          - /path/to/postgres-data__restore_YYMMDD:/var/lib/postgresql/data
        environment:
          - POSTGRES_USER=postgres
          - POSTGRES_PASSWORD=postgres
          - POSTGRES_DB=demasterpro
        ports:
          - "5432:5432"
        healthcheck:
          test: ["CMD-SHELL", "pg_isready -U postgres"]
          interval: 10s
          timeout: 5s
          retries: 5
Step 4: Restart database.

    docker-compose down && docker-compose up -d 


