


## Quickstart
<dl>
    <dt>Run deploymet local</dt>
    

    cd deployment && docker-compose down && docker-compose up -d 

   <dt>Run deployment in other environment</dt>
    For example, run deployment in staging environment

    cd deployment && docker-compose down && docker-compose -f docker-compose.yml -f docker-compose.staging.yml up -d
    

## ### Installation

  <dt>Config SSL for public domain</dt>
  Clone this repo to install SSL
  
     git clone https://github.com/wmnnd/nginx-certbot.git 
Change domain in file init-letsencrypt.sh and run to get config SSL
Change fields in nginx/app.config to enable nginx with SSL: server_name, ssl_certificate, ssl_certificate_key, proxy_pass

    server {
        listen 80;
        server_name test.domain.com;
        server_tokens off;
    
        location /.well-known/acme-challenge/ {
            root /var/www/certbot;
        }
    
        location / {
            return 301 https://$host$request_uri;
        }
    }
        server {
        listen 443 ssl;
        server_name testapp.demasterpro.com;
        server_tokens off;
        
        ssl_certificate /etc/letsencrypt/live/test.domain.com/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/test.domain.com/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        
        location / {
            proxy_pass  http://webapp:80;
            proxy_set_header    Host                $http_host;
            proxy_set_header    X-Real-IP           $remote_addr;
            proxy_set_header    X-Forwarded-For     $proxy_add_x_forwarded_for;
        }
    
    }
Run init-letsencrypt.sh 

    $ chmod +x init-letsencrypt.sh  
    $ ./init-letsencrypt.sh

<dt>Enable SSL for Web MQTT in RabbitMQ</dt>
Mount config SSL from nginx to use with RabbitMQ

In rabbitmq.conf add config and change path to folder store certificate. 

    #Web MQTT SSl
    web_mqtt.ssl.port       = 15673
    web_mqtt.ssl.backlog    = 1024
    web_mqtt.ssl.cacertfile = /etc/letsencrypt/live/test.domain.com/fullchain.pem
    web_mqtt.ssl.certfile   = /etc/letsencrypt/live/test.domain.com/cert.pem
    web_mqtt.ssl.keyfile    = /etc/letsencrypt/live/test.domain.com/privkey.pem
   
   Reference: [https://www.rabbitmq.com/ssl.html](https://www.rabbitmq.com/ssl.html)

</dl>

<dt>Enable SSL for AMQP and MQTT in RabbitMQ</dt>
Note: with connections between RabbitMQ server and client use AMQP and MQTT, we should use a different certificate with certificate of letsencrypt, self signed certificate, to create server certificate and client certificate for peer verification .

To create self signed certificate, clone this repository and run 

    git clone https://github.com/michaelklishin/tls-gen.git
    cd tls_gen/basic
    make regen PASSWORD=random-password CN=demasterpro.com

For config SSL with RabbitMQ server, change file rabbitmq.conf

    ...
    #AMQP SSL
    ssl_options.verify               = verify_peer
    
    listeners.ssl.default = 5671
    
    ssl_options.cacertfile = /path/to/tls-gen/basic/result/ca_certificate.pem
    ssl_options.certfile   = /path/to/tls-gen/basic/result/server_certificate.pem
    ssl_options.keyfile    = /path/to/tls-gen/basic/result/server_key.pem
    ssl_options.verify     = verify_peer
    ssl_options.fail_if_no_peer_cert = true
    
    #MQTT SSL use same config with AMQP
    mqtt.listeners.ssl.default = 8883
    ...

For client, to connect with RabbitMQ server, client have to use client certificate and key to authenticate with RabbitMQ server. 
For example, with python:

    ca_certs='/path/to/tls-gen/basic/result/ca_certificate.pem'
    certfile='/path/to/tls-gen/basic/result/client_certificate.pem'
    keyfile='/path/to/tls-gen/basic/result/client_key.pem'
    
    client = mqtt.Client()
    client.tls_set(ca_certs=ca_certs, certfile=certfile, keyfile=keyfile)
    client.tls_insecure_set(True)
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set("username", "password")
    
    client.connect("localhost", 8883, 30)
Note, with another language, some SSL lib need verify trusted certificate, so to use with self signed certificate, we need disable this feature, for example with C#:

    factory  =  new  ConnectionFactory
    
    {
        #config for connection
        ...
    };
    sslOption  =  new  SslOption
    {
        Enabled  =  true,
        
        // disable verify trusted certificate 
        AcceptablePolicyErrors  =  SslPolicyErrors.RemoteCertificateNameMismatch  |
        
        SslPolicyErrors.RemoteCertificateChainErrors,
        
        CertPath  =  configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsCertPath),
        
        CertPassphrase  =  configuration.GetValue<string>(Constants.Settings.QueueConnectionSettingsCertPassphrase)
    };
    factory.Ssl = sslOption

Reference: [https://www.rabbitmq.com/ssl.html](https://www.rabbitmq.com/ssl.html)

For get `client_certificate.pem`,  `client_key.pem`, `ca_certificate.pem` please contact with Mr Cuong or Hung:

 - Cuong: cuong@duali.com
 - Hung: hung@duali.com
