
## Build and deploy backend for run local
<dl>
    <dt>Build image:</dt>
    
    docker build -f docker/builder.Dockerfile -t dmpw_api:builder .
    docker build -f docker/staging.Dockerfile -t dmpw_api:lastest .
   <dt>Run image</dt>

    docker run --name dmpw_api_production --env ASPNETCORE_ENVIRONMENT=Local dmpw_api:lastest

</dl>

## Build and deloy RabbitMQ for run local
<dl>
    <dt>Build image:</dt>
    
    docker build -f docker/rabbitmq.Dockerfile -t dmpw_rabbit:lastest
   <dt>Run image</dt>
    
    docker run -p 5672:5672 -p 5673:5673 -p 15672:15672 -p 15675:15675 -p 1883:1883 -d --name rabbitmq dmpw_rabbit:lastest
</dl>     