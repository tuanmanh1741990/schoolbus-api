FROM rabbitmq:3.7-management

WORKDIR /
RUN rabbitmq-plugins enable rabbitmq_mqtt
RUN rabbitmq-plugins enable rabbitmq_web_mqtt
RUN rabbitmq-plugins enable rabbitmq_auth_mechanism_ssl

ENTRYPOINT ["rabbitmq-server"]