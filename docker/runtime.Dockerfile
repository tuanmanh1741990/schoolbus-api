FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app

# install dependencies
RUN apt-get update \
    && apt-get install -y python python-pip \
    && apt-get install -y libgdiplus libc6-dev  \
    && python -m pip install --upgrade pip \
    && pip install requests paho-mqtt \
    && rm -rf /var/lib/apt/lists/*
