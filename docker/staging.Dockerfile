#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat

FROM microsoft/dotnet:2.2-sdk AS build-env
WORKDIR /app
COPY ./*.sln ./

# Copy csproj and restore as distinct layers
COPY ./API/DeMasterProCloud.Api/DeMasterProCloud.Api.csproj ./API/DeMasterProCloud.Api/
COPY ./API/DeMasterProCloud.Common/DeMasterProCloud.Common.csproj ./API/DeMasterProCloud.Common/
COPY ./API/DeMasterProCloud.DataAccess/DeMasterProCloud.DataAccess.csproj ./API/DeMasterProCloud.DataAccess/
COPY ./API/DeMasterProCloud.DataModel/DeMasterProCloud.DataModel.csproj ./API/DeMasterProCloud.DataModel/
COPY ./API/DeMasterProCloud.Repository/DeMasterProCloud.Repository.csproj ./API/DeMasterProCloud.Repository/
COPY ./API/DeMasterProCloud.Service/DeMasterProCloud.Service.csproj ./API/DeMasterProCloud.Service/
COPY ./API/DeMasterProCloud.UnitTest/DeMasterProCloud.UnitTest.csproj ./API/DeMasterProCloud.UnitTest/
RUN dotnet restore --force --ignore-failed-sources

COPY . ./
#RUN dotnet test /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:Exclude=[xunit.*]* ./API/DeMasterProCloud.UnitTest/DeMasterProCloud.UnitTest.csproj -c Release
RUN dotnet publish ./API/DeMasterProCloud.Api/DeMasterProCloud.Api.csproj -c Release -o out

# Build runtime image
FROM 009795078640.dkr.ecr.ap-southeast-1.amazonaws.com/dmpw/dmpw-api:runtime AS runtime
WORKDIR /app
COPY --from=build-env /app/API/DeMasterProCloud.Api/out ./
COPY --from=build-env /app/API/DeMasterProCloud.Api/DeMasterProCloud.Api.xml ./
    
COPY --from=build-env /app/API/DeMasterProCloud.Api/update_version.sh ./
RUN chmod +x update_version.sh

ENTRYPOINT ["dotnet", "DeMasterProCloud.Api.dll"]	